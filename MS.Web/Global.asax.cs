﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.App_Start;
using Cobranca.Web.Rotinas;
using DevExpress.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Cobranca.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);


            SimpleInjectorInitializer.Initialize();
            //if (new Rotinas.WebRotinas.junixToken().atualizarParametros())
            //{
            //    throw new Exception("Atenção, não foi possivel verificar o acesso, entre em contato com o Administrador do sistema. Erro Code: 1001");
            //}

        }
        public override void Init()
        {
            this.PostAuthenticateRequest += MvcApplication_PostAuthenticateRequest;

            

            base.Init();
        }

        void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {            
            System.Web.HttpContext.Current.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.Required);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {

                // Get the forms authentication ticket.
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                int userData = Cobranca.Db.Rotinas.DbRotinas.ConverterParaInt(((FormsIdentity)(Context.User.Identity)).Ticket.UserData);//get user id

                if (!Context.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage("msg=expirado");
                }
                
            }
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            //PUBLICADO
            //Exception exception = Server.GetLastError();
            //Session.Add("_getLastError", exception);
            //Server.ClearError();
            //Response.Redirect("/Error/ServerError");



            //if (!HttpContext.Current.Request.IsLocal)
            //{
            //    Exception exception = Server.GetLastError();
            //    Session.Add("_getLastError", exception);
            //    Server.ClearError();
            //    Response.Redirect("/Error/ServerError");
            //}

        }
        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            DevExpressHelper.Theme = "MetropolisBlue";
        }
    }
}
