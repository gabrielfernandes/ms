﻿using Cobranca.Domain.BoletoContext.Handlers;
using Cobranca.Domain.BoletoContext.Repositories;
using Cobranca.Domain.CobrancaContext.Handlers;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Infra;
using Cobranca.Infra.BoletoContext.Repositories;
using Cobranca.Infra.CobrancaContext.Repositories;
using SimpleInjector;

namespace Cobranca.Web.App_Start
{
    public static class DIContainer
    {
        public static Container container;

        public static void RegisterDependencies(Container containerToInject)
        {
            containerToInject.Register<DbContext>(Lifestyle.Scoped);

            //REPOSITORIEs
            containerToInject.Register<IClienteRepository, ClienteRepository>();
            containerToInject.Register<IContratoRepository, ContratoRepository>();
            containerToInject.Register<IHistoricoRepository, HistoricoRepository>();
            containerToInject.Register<ISinteseRepository, SinteseRepository>();
            containerToInject.Register<IContratoParcelaRepository, ContratoParcelaRepository>();
            containerToInject.Register<IParametroAtendimentoRepository, ParametroAtendimentoRepository>();
            containerToInject.Register<IParametroSinteseRepository, ParametroSinteseRepository>();
            containerToInject.Register<IBoletoHistoricoRepository, BoletoHistoricoRepository>();
            containerToInject.Register<IBoletoRepository, BoletoRepository>();
            containerToInject.Register<IBancoPortadorRepository, BancoPortadorRepository>();
            containerToInject.Register<IBancoPortadorLogArquivoRepository, BancoPortadorLogArquivoRepository>();
            containerToInject.Register<ITabelaBancoRepository, TabelaBancoRepository>();
            containerToInject.Register<ITabelaGrupoRepository, TabelaGrupoRepository>();
            containerToInject.Register<ICartaCobrancaRepository, CartaCobrancaRepository>();


            //HANDLERs
            containerToInject.Register<ParametroAtendimentoHandler, ParametroAtendimentoHandler>();
            containerToInject.Register<BoletoHandler, BoletoHandler>();
            containerToInject.Register<BancoPortadorHandler, BancoPortadorHandler>();
            containerToInject.Register<ClienteHandler, ClienteHandler>();
            containerToInject.Register<CartaCobrancaHandler, CartaCobrancaHandler>();
        }
    }
}