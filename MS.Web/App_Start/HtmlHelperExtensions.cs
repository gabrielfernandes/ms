﻿#region Using

using System;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;


#endregion

namespace Cobranca.Web
{
    public static class HtmlHelperExtensions
    {




        /// <summary>
        ///     Returns an unordered list (ul element) of validation messages that utilizes bootstrap markup and styling.
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="alertType">The alert type styling rule to apply to the summary element.</param>
        /// <param name="heading">The optional value for the heading of the summary element.</param>
        /// <returns></returns>
        public static HtmlString ValidationBootstrap(this HtmlHelper htmlHelper, string alertType = "danger",
            string heading = "", string animate = "")
        {
            if (htmlHelper.ViewData.ModelState.IsValid)
                return new HtmlString(string.Empty);

            var sb = new StringBuilder();

            sb.AppendFormat("<div class=\"alert alert-{0} alert-block {1}\">", alertType, animate);
            sb.Append("<button class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>");

            if (!string.IsNullOrEmpty(heading))
            {
                sb.AppendFormat("<h4 class=\"alert-heading\"><i class=\"fa fa-warning\" >{0}</h4>", heading);
            }

            sb.Append(htmlHelper.ValidationSummary());
            sb.Append("</div>");

            return new HtmlString(sb.ToString());
        }
        public static HtmlString ValidationBootstrapModal(this HtmlHelper htmlHelper, string alertType = "danger",
            string heading = "")
        {
            if (htmlHelper.ViewData.ModelState.IsValid)
                return new HtmlString(string.Empty);

            var sb = new StringBuilder();


            if (!htmlHelper.ViewData.ModelState.IsValid)
            {
                sb.AppendLine("<div class='modal modal-dados-invalidos' style='display:none;'>");
                sb.AppendLine("<div class='modal-dialog'>");
                sb.AppendLine("<div class='modal-content'>");

                sb.AppendLine("<div class='modal-header text-danger'>");
                sb.AppendLine("<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>");
                sb.AppendLine(" <h4 class='modal-title'><i class='fa fa-warning'></i>&nbsp;Dados inválidos!</h4>");
                sb.AppendLine("</div>");

                sb.AppendLine("<div class='modal-body modal-msg'>");
                sb.Append(htmlHelper.ValidationSummary());
                sb.AppendLine("</div>");

                sb.AppendLine("<div class='modal-footer'>");
                sb.AppendLine("<div class='col-md-9'></div><div class='col-md-3'><button type='button' class='btn btn-danger btn-block' data-dismiss='modal'>OK</button></div>");
                sb.AppendLine("</div>");

                sb.AppendLine("</div>");
                sb.AppendLine("</div>");
                sb.AppendLine("</div>");
            }

            return new HtmlString(sb.ToString());
        }


        public static HtmlString Modal(this HtmlHelper htmlHelper, string alertType = "danger",
       string mensagem = "", string animate = "", string titulo = "Mensagem")
        {
            if (string.IsNullOrEmpty(mensagem) && htmlHelper.ViewBag.MensagemOK == null)
                return new HtmlString(string.Empty);

            var sb = new StringBuilder();

            if (htmlHelper.ViewBag.MensagemOK != null && mensagem == string.Empty)
                mensagem = htmlHelper.ViewBag.MensagemOK;


            if (!string.IsNullOrEmpty(mensagem))
            {
                sb.AppendFormat("<div class='modal {0} modal-dados-invalidos' style='display:none;'>", animate);
                sb.AppendLine("<div class='modal-dialog'>");
                sb.AppendLine("<div class='modal-content'>");

                sb.AppendLine("<div class='modal-header text-primary'>");
                sb.AppendLine("<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button>");
                sb.AppendLine(" <h4 class='modal-title'><i class='fa fa-info'></i>&nbsp;Mensagem</h4>");
                sb.AppendLine("</div>");

                sb.AppendLine("<div class='modal-body'>");
                sb.Append(mensagem);
                sb.AppendLine("</div>");

                sb.AppendLine("<div class='modal-footer'>");

                if (htmlHelper.ViewBag.RedirecionarUrl != null)
                {
                    sb.AppendFormat("<div class='col-md-9'></div><div class='col-md-3'><button onclick='document.location = \"{0}\"; ' type='button' class='btn btn-primary btn-block' data-dismiss='modal'>OK</button></div>", htmlHelper.ViewBag.RedirecionarUrl);
                }
                else
                {
                    if (htmlHelper.ViewBag.PageReload == true)
                    {
                        sb.AppendLine("<div class='col-md-9'></div><div class='col-md-3'><button onclick='goReload()' type='button' class='btn btn-primary btn-block' data-dismiss='modal'>OK</button></div>");
                    }
                    else
                    {
                        sb.AppendLine("<div class='col-md-9'></div><div class='col-md-3'><button onclick='goBack()' type='button' class='btn btn-primary btn-block' data-dismiss='modal'>OK</button></div>");
                    }
                }

                
                

                sb.AppendLine("</div>");

                sb.AppendLine("</div>");
                sb.AppendLine("</div>");
                sb.AppendLine("</div>");
            }

            return new HtmlString(sb.ToString());
        }

    }
}