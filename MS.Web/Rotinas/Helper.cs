﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Rotinas;

namespace Cobranca.Web.Rotinas
{
    public class Helper
    {
        internal OperacaoRetorno GerarArquivo(string nomeArquivo, IEnumerable<object> dados, List<ConverterListaParaExcel.CollumnExcel> columns)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                string arquivo = WebRotinas.ObterArquivoTemp();
                FileInfo file = new FileInfo(arquivo);
                ConverterListaParaExcel.GerarCSVDeEspecList(dados.ToList(), arquivo, columns);
                op.OK = true;
                op.Dados = $"/Home/downloadCSV/?guid={file.Name}&nm={nomeArquivo}.csv";
            }
            catch (Exception ex)
            {
                op.Mensagem = $"Erro ao gerar csv {ex.Message}.";
            }
            return op;
        }
    }
}