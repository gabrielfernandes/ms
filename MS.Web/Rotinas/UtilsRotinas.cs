﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using Cobranca.Web.Models;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas;
using System.Linq.Dynamic;
using System.Web.UI.WebControls;
using Cobranca.Db.Models.Pesquisa;

namespace Cobranca.Web.Rotinas
{

    public static class UtilsRotinas
    {
        private static Repository<Usuario> objUsuairoDb = new Repository<Usuario>();
        private static Repository<UsuarioRegional> objUsuarioRegionalDb = new Repository<UsuarioRegional>();
        private static Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        private static Repository<TabelaProduto> _repProduto = new Repository<TabelaProduto>();
        private static Repository<TabelaAging> _repAging = new Repository<TabelaAging>();
        private static Repository<Usuario> _repUsuario = new Repository<Usuario>();
        private static Repository<ParametroAtendimentoProduto> _dbParametroAtendimentoProduto = new Repository<ParametroAtendimentoProduto>();
        public static List<int> ObterRegionailIdsDoUsuarioLogado()
        {
            var objUsr = WebRotinas.ObterUsuarioLogado();
            List<int> Ids = new List<int>();
            Ids = objUsuarioRegionalDb.All().Where(x => x.CodUsuario == objUsr.Cod).Select(x => x.CodRegional).ToList();
            return Ids;
        }
        /// <summary>GET SelectListItem Atendente (usuario regional)</summary>
        /// <param name="where"><see cref="x.Cod == 1 ..."/></param>
        /// <param name="checkeds"><see cref="[1,2,3...]"/></param>
        public static List<SelectListItem> GetSelectListItemAtendenteAdmFilialAnalistaSupervisor(string where = default(string), string[] checkeds = default(string[]))
        {
            List<SelectListItem> Itens = new List<SelectListItem>();
            var _where = $@"1 = 1 {where}";

            var qry = (_repUsuario.FindAll(x => x.PerfilTipo == (short)Enumeradores.PerfilTipo.Regional || x.PerfilTipo == (short)Enumeradores.PerfilTipo.RegionalAdmin || x.PerfilTipo == (short)Enumeradores.PerfilTipo.Analista || x.PerfilTipo == (short)Enumeradores.PerfilTipo.SupervisorAnalista) );

            var lista = qry.Where(_where).ToList();
            foreach (var item in lista.OrderBy(x=> x.Nome))
            {
                Itens.Add(new SelectListItem()
                {
                    Value = item.Cod.ToString(),
                    Text = item.Nome,
                    Selected = false
                });
            }
            if (checkeds != null)
            {
                foreach (var i in checkeds)
                {
                    if (Itens.Where(x => x.Value == i).FirstOrDefault() != null)
                    {
                        Itens.Where(x => x.Value == i).FirstOrDefault().Selected = true;
                    }
                }
            }
            return Itens;
        }

        /// <summary>GET SelectListItem Atendente (usuario regional)</summary>
        /// <param name="where"><see cref="x.Cod == 1 ..."/></param>
        /// <param name="checkeds"><see cref="[1,2,3...]"/></param>
        public static List<SelectListItem> GetSelectListItemAtendenteAdmFilial(string where = default(string), string[] checkeds = default(string[]))
        {
            List<SelectListItem> Itens = new List<SelectListItem>();
            var _where = $@"1 = 1 {where}";

            var qry = (_repUsuario.FindAll(x => x.PerfilTipo == (short)Enumeradores.PerfilTipo.Regional || x.PerfilTipo == (short)Enumeradores.PerfilTipo.RegionalAdmin));

            var lista = qry.Where(_where).ToList();
            foreach (var item in lista)
            {
                Itens.Add(new SelectListItem()
                {
                    Value = item.Cod.ToString(),
                    Text = item.Nome,
                    Selected = false
                });
            }
            if (checkeds != null)
            {
                foreach (var i in checkeds)
                {
                    if (Itens.Where(x => x.Value == i).FirstOrDefault() != null)
                    {
                        Itens.Where(x => x.Value == i).FirstOrDefault().Selected = true;
                    }
                }
            }
            return Itens;
        }

        /// <summary>GET SelectListItem Atendente (usuario regional)</summary>
        /// <param name="where"><see cref="x.Cod == 1 ..."/></param>
        /// <param name="checkeds"><see cref="[1,2,3...]"/></param>
        public static List<SelectListItem> GetSelectListItemAtendente(string where = default(string), string[] checkeds = default(string[]))
        {
            List<SelectListItem> Itens = new List<SelectListItem>();
            var _where = $@"1 = 1 {where}";

            var qry = (_repUsuario.FindAll(x=> x.PerfilTipo == (short)Enumeradores.PerfilTipo.Regional));

            var lista = qry.Where(_where).ToList();
            foreach (var item in lista)
            {
                Itens.Add(new SelectListItem()
                {
                    Value = item.Cod.ToString(),
                    Text = item.Nome,
                    Selected = false
                });
            }
            if (checkeds != null)
            {
                foreach (var i in checkeds)
                {
                    if (Itens.Where(x => x.Value == i).FirstOrDefault() != null)
                    {
                        Itens.Where(x => x.Value == i).FirstOrDefault().Selected = true;
                    }
                }
            }
            return Itens;
        }

        /// <summary>GET SelectListItem TabelaRegional</summary>
        /// <param name="where"><see cref="x.Cod == 1 ..."/></param>
        /// <param name="checkeds"><see cref="[1,2,3...]"/></param>
        public static List<SelectListItem> GetSelectListItemRegional(string where = default(string), string[] checkeds = default(string[]))
        {
            List<SelectListItem> Itens = new List<SelectListItem>();
            var _where = $@"1 = 1 {where}";
            var usuario = WebRotinas.ObterUsuarioLogado();
            var UsuarioRegionais = WebRotinas.ObterRegionailIdsDoUsuarioLogado();
            _dbRegional = new Repository<TabelaRegional>();
            var qry = (_dbRegional.FindAll());
            if (UsuarioRegionais.Count() > 0) qry.DynamicContains("Cod", UsuarioRegionais);

            var lista = qry.Where(_where).ToList();
            if (UsuarioRegionais.Count() > 0) lista = lista.Where(x => UsuarioRegionais.Contains(x.Cod)).ToList();
            foreach (var item in lista)
            {
                item.Regional = DbRotinas.Descriptografar(item.Regional);
            }
            foreach (var item in lista.OrderBy(x=> x.Regional))
            {
                Itens.Add(new SelectListItem()
                {
                    Value = item.Cod.ToString(),
                    Text = item.Regional,
                    Group = new SelectListGroup() { Name = String.Join(", ", item.TabelaRegionalGrupoes.Where(x=> !x.DataExcluido.HasValue) != null ? item.TabelaRegionalGrupoes.Where(x=> !x.DataExcluido.HasValue).Select(s => s.CodRegionalCliente).Distinct().ToList() : null) },
                    Selected = false
                });
            }
            if (checkeds != null)
            {
                foreach (var i in checkeds)
                {
                    if (Itens.Where(x => x.Value == i).FirstOrDefault() != null)
                    {
                        Itens.Where(x => x.Value == i).FirstOrDefault().Selected = true;
                    }
                }
            }
            return Itens;
        }

        /// <summary>GET SelectListItem TabelaRegional</summary>
        /// <param name="where"><see cref="x.Cod == 1 ..."/></param>
        /// <param name="checkeds"><see cref="[1,2,3...]"/></param>
        public static List<SelectListItem> GetSelectListItemProduto(string where = default(string), string[] checkeds = default(string[]))
        {
            List<SelectListItem> Itens = new List<SelectListItem>();
            var _where = $@"1 = 1 {where}";
            var usuario = WebRotinas.ObterUsuarioLogado();
            var UsuarioRegionais = WebRotinas.ObterRegionailIdsDoUsuarioLogado();
            _repProduto = new Repository<TabelaProduto>();
            var qry = (_repProduto.FindAll());
            //FILTRA OS PRODUTOS QUE O USUARIO DA REGIONAL PARTICIPA
            if (UsuarioRegionais.Count() > 0 && usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional)
            {
                //CRIA UMA LISTA DE PRODUTOS DEFINIDAS PARA O USUARIO
                List<int> produtosIds = _dbParametroAtendimentoProduto.All().Where(x => x.ParametroAtendimento.CodUsuario == usuario.Cod).Select(p => (int)p.CodProduto).ToList();
                qry.DynamicContains("Cod", produtosIds);
            }

            var lista = qry.Where(_where).ToList();
            foreach (var item in lista)
            {
                item.Descricao = DbRotinas.Descriptografar(item.Descricao);
            }
            foreach (var item in lista)
            {
                Itens.Add(new SelectListItem()
                {
                    Value = item.Cod.ToString(),
                    Text = item.Descricao,
                    Selected = false
                });
            }
            if (checkeds != null)
            {
                foreach (var i in checkeds)
                {
                    if (Itens.Where(x => x.Value == i).FirstOrDefault() != null)
                    {
                        Itens.Where(x => x.Value == i).FirstOrDefault().Selected = true;
                    }
                }
            }
            return Itens;
        }

        /// <summary>GET SelectListItem TabelaRegional</summary>
        /// <param name="where"><see cref="x.Cod == 1 ..."/></param>
        /// <param name="checkeds"><see cref="[1,2,3...]"/></param>
        public static List<SelectListItem> GetSelectListItemAging(string where = default(string), string[] checkeds = default(string[]))
        {
            List<SelectListItem> Itens = new List<SelectListItem>();
            var _where = $@"1 = 1 {where}";

            var qry = (_repAging.FindAll());

            var lista = qry.Where(_where).ToList();
            foreach (var item in lista)
            {
                Itens.Add(new SelectListItem()
                {
                    Value = item.Cod.ToString(),
                    Text = item.Aging,
                    Selected = false
                });
            }
            if (checkeds != null)
            {
                foreach (var i in checkeds)
                {
                    if (Itens.Where(x => x.Value == i).FirstOrDefault() != null)
                    {
                        Itens.Where(x => x.Value == i).FirstOrDefault().Selected = true;
                    }
                }
            }
            return Itens;
        }

        public static IQueryable<T> DynamicContains<T, TProperty>(this IQueryable<T> query, string property, IEnumerable<TProperty> items)
        {
            var pe = Expression.Parameter(typeof(T));
            var me = Expression.Property(pe, property);
            var ce = Expression.Constant(items);
            var call = Expression.Call(typeof(Enumerable), "Contains", new[] { me.Type }, ce, me);
            var lambda = Expression.Lambda<Func<T, bool>>(call, pe);
            return query.Where(lambda);
        }

    }
}