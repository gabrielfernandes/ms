﻿using Cobranca.Db.Models;
using Cobranca.Db.Models.Pesquisa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace Cobranca.Web.Models
{
    [Serializable]
    public class HomePesquisaViewModel
    {
        public long ClienteId { get; set; }
        public long UsuarioCopiaId { get; set; }
        public long TrabalhoTipoId { get; set; }
        public long ResponsavelId { get; set; }
        public long FaseId { get; set; }
        public int CodCampanha { get; set; }
        public int CodTipoCobranca { get; set; }
        public int CodEmpreendimento { get; set; }
        public int CodConstrutora { get; set; }
        public int CodRegional { get; set; }
        public int CodProduto { get; set; }
        public int CodSintese { get; set; }
        public short Status { get; set; }
        public int CodEmpresaVenda { get; set; }
        public int CodBloco { get; set; }
        public DateTime? DataDe { get; set; }
        public DateTime? DataAte { get; set; }
        public int CodEscritorio { get; set; }
        public int CodAging { get; set; }
        public short CodPerfilTipo { get; set; }

        public Nullable<short> TipoFluxo { get; set; }


        public string CodProdutos { get; set; }
        public string[] produtosIds { get; set; }
        public string CodRegionais { get; set; }
        public string[] regionalIds { get; set; }
        public string CodConstrutoras { get; set; }
        public string CodAgings { get; set; }
        public string[] agingIds { get; set; }
        public string[] construtorasIds { get; set; }
        public string CodUsuarios { get; set; }
        public string[] usuarioIds { get; set; }

        [XmlIgnoreAttribute]
        public List<Contrato> ListaContratoes { get; internal set; }
        [XmlIgnoreAttribute]
        public List<ContratoDados> ListaContratoesDados { get; internal set; }
        [XmlIgnoreAttribute]
        public List<HistoricoDados> HistoricoesCliente { get; set; }
        [XmlIgnoreAttribute]

        public List<HistoricoDados> HistoricoesParcela { get; set; }
        [XmlIgnoreAttribute]

        public List<HistoricoDados> HistoricoesContrato { get; set; }

        public List<HistoricoDados> HistoricoesBoleto { get; set; }
        [XmlIgnoreAttribute]
        public List<Proposta> ListaPropostas { get; internal set; }

        [XmlIgnoreAttribute]
        public List<TabelaFase> ListaFase { get; internal set; }
        public List<TabelaFase> ListaFaseBoleto { get; internal set; }
        [XmlIgnoreAttribute]
        public SelectList SelectListClientes { get; internal set; }
        [XmlIgnoreAttribute]
        public SelectList SelectListUsuarios { get; internal set; }
        [XmlIgnoreAttribute]
        public SelectList SelectListTrabalhos { get; internal set; }
        [XmlIgnoreAttribute]
        public SelectList SelectListFase { get; internal set; }


        public int QtdUnidade { get; set; }
        public int QtdReserva { get; set; }
        public int QtdVenda { get; set; }
        public decimal PercentualConversao { get; set; }
        public int QtdAtiva { get; set; }
        public int QtdEstoque { get; set; }
        public DateTime? PeriodoDe { get; set; }
        public DateTime? PeriodoAte { get; set; }
        public string CodRotas { get; set; }


        public int CodUsuarioAtendimento { get; set; }



        public short tabSelecionada { get; set; }
    }
}