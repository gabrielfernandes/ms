﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class RelatorioViewModel
    {

        public int CodEscritorio { get; set; }
        public int CodConstrutora { get; set; }
        public int CodRegional { get; set; }
        public int CodProduto { get; set; }

        public short? RelatorioFiltroTipo { get; set; }

    }

    public class RelatorioPesquisaViewModel
    {
        public RelatorioPesquisaViewModel()
        {
            ListaTipoSolicitacao = new List<DictionaryEntry>();
            ListaStatusBoleto = new List<DictionaryEntry>();
            ListaTipoPeriodo = new List<DictionaryEntry>();
        }

        public string AN8 { get; set; }
        public string CNPJ { get; set; }
        public string Contrato { get; set; }
        public DateTime? DataEmissao { get; set; }
        public DateTime? DataVencimento { get; set; }
        public string NumeroDocumento { get; set; }
        public string NumeroFatura { get; set; }
        public decimal? ValorDe { get; set; }
        public decimal? ValorAte { get; set; }
        public decimal? ValorAbertoAte { get; set; }
        public decimal? ValorAbertoDe { get; set; }
        public string Regionais { get; set; }
        public string[] regionalIds { get; set; }
        public string AtendenteIds { get; set; }
        public string[] usuarioIds { get; set; }

        public string NumeroParcela { get; set; }
        public string CodArea { get; set; }
        public string NomeFilial { get; set; }
        public DateTime? DataFechamento { get; set; }
        public DateTime? DataPagamentoPCF { get; set; }
        public string Status { get; set; }
        public string Fase { get; set; }
        public string Sintese { get; set; }
        public string Observacao { get; set; }
        public DateTime? DataAgendamentoDe { get; set; }
        public DateTime? DataAgendamentoAte { get; set; }
        public string CodRegionais { get; set; }
        public string CodContrutoras { get; set; }
        public string CodProdutos { get; set; }
        public string CodRotas { get; set; }
        public string CodAgins { get; set; }
        public DateTime? DataPeriodoDe { get; set; }
        public DateTime? DataPeriodoAte { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public Nullable<int> PerfilTipo { get; set; }
        public Nullable<short> TipoReport { get; set; }

        public Nullable<int> CodUsuarioAtendente { get; set; }
        public Nullable<int> CodRegional { get; set; }
        public string codigoRelatorio { get; set; }
        public short? ReportCod { get; set; }
        public string Regional { get; set; }
        public string CondicaoQuery { get; set; }

        public int? ContratoId { get; set; }
        public int? ClienteId { get; set; }

        public bool? Aberto { get; set; }
        public bool? Vencido { get; set; }
        public bool? Pago { get; set; }

        public List<DictionaryEntry> ListaTipoSolicitacao { get; set; }
        public List<DictionaryEntry> ListaStatusBoleto { get; set; }
        public List<DictionaryEntry> ListaTipoPeriodo { get; set; }

        public string SolicitacaoTipo { get; set; }
        public string[] solicitacaoTipoIds { get; set; }

        public string StatusBoleto { get; set; }
        public string[] statusBoletoIds { get; set; }

        public int? TipoPeriodo { get; set; }

        public string Solicitadores { get; set; }
        public string[] usuarioSolicitaIds { get; set; }

        public string Aprovadores { get; set; }
        public string[] usuarioAprovaIds { get; set; }
    }
}