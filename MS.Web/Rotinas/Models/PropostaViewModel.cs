﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    [Serializable]
    public class PropostaViewModel
    {
        public int CodUnidade { get; set; }
        public string NomeCliente { get; set; }
        public string CPF { get; set; }
        public string CNPJ { get; set; }
        public string Email { get; set; }
        public string TelefoneCelular { get; set; }
        public DateTime DataNascimentoCliente { get; set; }
        public decimal ValorFGTS { get; set; }
        public decimal ValorRendaFamiliar { get; set; }
        public decimal ValorMaximoFinanciamento { get; set; }
        public decimal ValorMaximoRepasse { get; set; }
        public decimal ValorEntradaDireta { get; set; }
        public decimal ValorFinanciamentoTotal { get; set; }
        public int CodTabelaVenda { get; set; }
        public int CodSintese { get; set; }
        public int CodProposta { get; set; }
        public short Status { get; set; }
        public int CodProspect { get; set; }
        public int CodEmpreendimento { get; set; }
        public int CodBloco { get; set; }
        public int CodRegional { get; set; }
        public decimal valorCalculadoFinanciamento { get; set; }
        public decimal ValorUnidade { get; set; }
        public string AndamentoDaObra { get; set; }

        public decimal ValorFalta { get;  set; }        
        public decimal ValorTotalGeral { get;  set; }        
        public DateTime? DataInicioRepasse { get;  set; }
        public decimal ValorTotalRepasse { get;  set; }
        public decimal ValorTotalSubsidio { get;  set; }
    }
}