﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class ContaViewModel
    {
    }
    public class LoginViewModel
    {
        [Required]
        public string Login { get; set; }
        [Required]
        public string Senha { get; set; }
        public bool ManterConectado { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class CadastreViewModel
    {
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Empresa { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(4, ErrorMessage = "A senha deve ter no mínimo 4 caracteres")]
        public string Senha { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Senha")]
        public string ConfirmarSenha { get; set; }


    }

    public class LoginClienteViewModel
    {
        [Required]
        public string CPF { get; set; }
        [Required]
        public string Senha { get; set; }
        public bool ManterConectado { get; set; }
        public string ReturnUrl { get; set; }
    }
}