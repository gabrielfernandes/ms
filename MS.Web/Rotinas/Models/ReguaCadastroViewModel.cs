﻿using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recebiveis.Web.Models
{
    [Serializable]
    public class ReguaCadastroViewModel
    {
        public string Acoes { get; set; }
        public int CodTipoCobranca { get; set; }
        public List<int> CodAcoes
        {
            get
            {
                List<int> l = new List<int>();

                if (!string.IsNullOrEmpty(Acoes))
                {
                    var arr = Acoes.Split(',');
                    foreach (var item in arr)
                    {
                        l.Add(DbRotinas.ConverterParaInt(item));
                    }
                }

                return l;
            }
        }
    }
}