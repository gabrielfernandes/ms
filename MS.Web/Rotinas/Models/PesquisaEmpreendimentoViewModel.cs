﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class PesquisaEmpreendimentoViewModel
    {

        public int CodDonoCarteira { get; set; }
        public int CodRegional { get; set; }
        public short StatusUnidade { get; set; }
        public int CodEmpreendimento { get; set; }
        public int CodBloco { get; set; }
        public int CodUnidade { get; set; }
        public string NumeroUnidade { get; set; }

        public string AndamentoDaObra { get; set; }
    }
}