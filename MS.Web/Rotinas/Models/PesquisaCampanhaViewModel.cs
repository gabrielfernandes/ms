﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class PesquisaCampanhaViewModel
    {
        public string CodigoVenda { get; set; }        
        public string NomeComprador { get; set; }
        public string CPF_CNPJ { get; set; }
        public int CodEscritorio { get; set; }
        public int CodUsuarioEscritorio { get; set; }
        public int CodUsuarioCadastrou { get; set; }
        public int CodBanco { get; set; }
        public int CodFaseCampanha { get; set; }
    }
}