﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recebiveis.Web.Models
{
    public class DataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public object data { get; set; }

    }
    
}