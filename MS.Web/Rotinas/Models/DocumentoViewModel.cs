﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class DocumentoViewModel
    {
        public List<String> Documentos { get; set; }
        public long Id { get; set; }
    }
}