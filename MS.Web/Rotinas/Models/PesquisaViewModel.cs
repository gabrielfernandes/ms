﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class PesquisaViewModel
    {
        //public int? CodigoVenda { get; set; }
        public string CodigoCliente { get; set; }
        public string CodProduto { get; set; }
        public string NomeComprador { get; set; }
        public string PrimeiroNome { get; set; }
        public string CPF_CNPJ { get; set; }
        public string CodigoVenda { get; set; }
        public int CodConstrutora { get; set; }
        public int CodDonoCarteira { get; set; }
        public int CodRegional { get; set; }
        public int CodTipoCobranca { get; set; }
        public int CodCobrancaAcao { get; set; }
        public int CodModeloAcao { get; set; }
        public int CodTipoCarteira { get; set; }
        public int CodGestao { get; set; }
        public int CodSintese { get; set; }
        public int CodEscritorio { get; set; }
        public int CodAging { get; set; }
        public short StatusUnidade { get; set; }
        public int CodEmpreendimento { get; set; }
        public int CodBloco { get; set; }
        public int CodUnidade { get; set; }
        public bool Bloqueado { get; set; }
        public string CodAgingCliente { get; set; }
        public string AndamentoDaObra { get; set; }

        public string NumeroContrato { get; set; }
        public int TipoAcaoCobranca { get; set; }
        public int CodAcaoCobranca { get; set; }
        public string NumeroUnidade { get; set; }
        public short? SituacaoTipo { get; set; }

        public int CodFase { get; set; }
        public int CodGrupo { get; set; }
        public int? CodTipoBloqueio { get; set; }
        public int CodContratoParcela { get; set; }

        public string[] RegionalIds { get; set; }

        public string Regionais { get; set; }

        public string Condicao { get; set; }


        public int CodSinteseParcela { get; set; }

        public int? ClienteId { get; set; }
        public int? CodUsuarioLogado { get; set; }
        public int? CodPerfilTipo { get; set; }

        public string NumeroFatura { get; set; }
        public string NotaFiscal { get; set; }
    }
}