﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace Cobranca.Web.Rotinas
{
    [Serializable]
    public class OperacaoRetorno
    {


        public OperacaoRetorno()
        {
            this.OK = false;
            this.CamposInvalidos = new List<OperacaoRetornoCampo>();
        }

        public OperacaoRetorno(Exception ex)
        {
            this.OK = false;
            this.Mensagem = ex.Message;
        }

        public bool OK { get; set; }
        public string Mensagem { get; set; }
        public object Dados { get; set; }
        public List<OperacaoRetornoCampo> CamposInvalidos { get; set; }

        public void addCampoErro(string campo)
        {
            this.addCampoErro(campo, "");
        }
        public void addCampoErro(string campo, string mensagem)
        {
            this.CamposInvalidos.Add(new OperacaoRetornoCampo()
            {
                CampoId = campo,
                Mensagem = mensagem
            });
            this.OK = false;
            this.Mensagem = "Dados inválidos";
        }
    }
    public class OperacaoRetornoCampo
    {
        public string CampoId { get; set; }
        public string Mensagem { get; set; }
    }
}