﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using Cobranca.Web.Models;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas;
using System.Linq.Dynamic;
using System.Web.UI.WebControls;
using Cobranca.Db.Models.Pesquisa;
using static Cobranca.Web.Models.ContratoParcelaBoletoViewModel;

namespace Cobranca.Web.Rotinas
{

    public static class BoletoRotinasUtils
    {
        public static int CalcularPeriodo(DateTime DataVencimento, DateTime DataMoratoria)
        {
            return (int)DbRotinas.DiferencaMes((DateTime)DataVencimento, (DateTime)DataMoratoria);
        }

        public static decimal? ValorAtualizado(decimal ValorPresente, decimal Taxa, int Periodo)
        {
            return (ValorPresente * (decimal)Math.Pow(1.0 + (DbRotinas.ConverterParaDouble(Taxa) / 100), Periodo));
        }

        public static decimal? CalcularValorJurosSimples(decimal ValorPresente, decimal Taxa, DateTime DataVencimento, DateTime DataBoleto)
        {
            var DtNow = DateTime.Now;
            var QtdDay = (DataBoleto - DataVencimento).Days;
            if (QtdDay <= 0)
            {
                return 0;
            }
            else
            {
                return decimal.Round(((((Taxa / 30) * QtdDay) / 100) * ValorPresente), 2, MidpointRounding.AwayFromZero);
            }
        }

        public static decimal? CalcularValorJurosComposto(decimal ValorPresente, decimal Taxa, int Periodo)
        {
            return ((ValorPresente * (decimal)Math.Pow(1.0 + (DbRotinas.ConverterParaDouble(Taxa) / 100), Periodo)) - ValorPresente);
        }

        internal static decimal? CalcularValorMulta(decimal? Valor, decimal? MultaAtraso, DateTime DataVencimento, DateTime DataBoleto)
        {
            
            if (DataBoleto.Date <= DataVencimento.Date)
            {
                return 0;
            }
            else if (DataBoleto.Date > DataVencimento.Date)
            {
                return decimal.Round(DbRotinas.ConverterParaDecimal(Valor * (MultaAtraso / 100)), 2, MidpointRounding.AwayFromZero); 
            }
            else
            {
                return decimal.Round(DbRotinas.ConverterParaDecimal(Valor * (MultaAtraso / 100)), 2, MidpointRounding.AwayFromZero);
            }
        }

        public static Nullable<int> ObterSinteseEtapa(Enumeradores.EtapaBoleto tipo)
        {
            Repository<TabelaParametroSintese> _parametroSintese = new Repository<TabelaParametroSintese>();
            var sinteseSolcitar = (from p in _parametroSintese.All()
                                   where p.Etapa == (short)tipo
                                   select p.CodSintesePara).ToList();
            if (sinteseSolcitar.Count() > 0)
            {
                return sinteseSolcitar.First();
            }
            else return null;
        }

        public static Enumeradores.EtapaBoleto ObterEtapaDaSintese(int CodSintese)
        {

            Repository<TabelaParametroSintese> _parametroSintese = new Repository<TabelaParametroSintese>();
            var Etapa = (from p in _parametroSintese.All()
                         where p.CodSintesePara == (int)CodSintese && p.Etapa.HasValue
                         select p.Etapa).ToList();
            if (Etapa.Count() > 0)
            {
                return (Enumeradores.EtapaBoleto)Etapa.First();
            }
            else return 0;
        }


        public static Enumeradores.EtapaBoleto VerificarTipoEtapaSolicitacao(ContratoParcelaBoleto Boleto)
        {
            Enumeradores.EtapaBoleto tipoSolicitacao = Enumeradores.EtapaBoleto.AtendenteSolicitaComDesconto;
            //SE O BOLETO FOR APENAS PRORROGAÇÂO DO VENCIMENTO O MESMO SERA APROVADO DIRETO
            if (Boleto.JurosAoMesPorcentagemPrevisto == Boleto.JurosAoMesPorcentagem && Boleto.MultaAtrasoPorcentagemPrevisto == Boleto.MultaAtrasoPorcentagem)
            {
                tipoSolicitacao = Enumeradores.EtapaBoleto.AtendenteSolicitaProrrogacao;
            }
            else
            {
                tipoSolicitacao = Enumeradores.EtapaBoleto.AtendenteSolicitaComDesconto;
            }
            return tipoSolicitacao;
        }

        public static Nullable<int> VerificarTipoEtapaSolicitacaoSintese(ContratoParcelaBoleto Boleto)
        {

            Enumeradores.EtapaBoleto tipoSolicitacao = Enumeradores.EtapaBoleto.AtendenteSolicitaComDesconto;
            //SE O BOLETO FOR APENAS PRORROGAÇÂO DO VENCIMENTO O MESMO SERA APROVADO DIRETO
            if (Boleto.JurosAoMesPorcentagemPrevisto == Boleto.JurosAoMesPorcentagem && Boleto.MultaAtrasoPorcentagemPrevisto == Boleto.MultaAtrasoPorcentagem && Boleto.ValorDocumentoPrevisto == Boleto.ValorDocumento)
            {
                tipoSolicitacao = Enumeradores.EtapaBoleto.AtendenteSolicitaProrrogacao;
            }
            Repository<TabelaParametroSintese> _parametroSintese = new Repository<TabelaParametroSintese>();
            var sinteseSolcitar = (from p in _parametroSintese.All()
                                   where p.Etapa == (short)tipoSolicitacao
                                   select p.CodSintesePara).ToList();
            if (sinteseSolcitar.Count() > 0)
            {
                return sinteseSolcitar.First();
            }
            else return null;
        }
        public static string ObterObservacaoEtapa(Enumeradores.EtapaBoleto tipo)
        {

            Repository<TabelaParametroSintese> _parametroSintese = new Repository<TabelaParametroSintese>();
            var sinteseSolcitar = (from p in _parametroSintese.All()
                                   where p.Etapa == (short)tipo
                                   select p.Observacao).ToList();
            if (sinteseSolcitar.Count() > 0)
            {
                return sinteseSolcitar.First();
            }
            else return null;
        }

        public static List<TabelaFase> ObterListaFaseBoleto()
        {
            var user = WebRotinas.ObterUsuarioLogado();
            var tpFluxo = 0;
            if (user.PerfilTipo == Enumeradores.PerfilTipo.Analista)
            {
                tpFluxo = (short)Enumeradores.TipoFluxo.AnalistaBoleto;
            }
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista)
            {
                tpFluxo = (short)Enumeradores.TipoFluxo.SupervisorBoleto;
            }
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional || user.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                tpFluxo = (short)Enumeradores.TipoFluxo.FilialBoleto;
            }

            Repository<TabelaFase> _fase = new Repository<TabelaFase>();
            var fasesBoleto = (from p in _fase.All()
                               where p.TipoHistorico == (short)Enumeradores.TipoHistorico.Boleto
                               && (p.TabelaTipoCobranca.TipoFluxo == tpFluxo || tpFluxo == 0)
                               select p).ToList();
            return fasesBoleto;
        }
        public static List<TabelaSintese> ObterListaSinteseBoleto()
        {
            Repository<TabelaFase> _fase = new Repository<TabelaFase>();
            Repository<TabelaSintese> _sintese = new Repository<TabelaSintese>();
            var faseIds = ObterListaFaseBoleto().Select(x => x.Cod).ToList();
            var SinteseBoleto = (from p in _sintese.All()
                                 where faseIds.Contains(p.CodFase)
                                 select p).ToList();
            return SinteseBoleto;
        }

        /// <summary>
        /// Metodo para obter dados do banco portador pelo codigo companhia
        /// </summary>
        public static TabelaBancoPortador ObterParametrosBancoPortador(string _companhiaFiscal)
        {
            Repository<TabelaBancoPortador> _dbBancoPortador = new Repository<TabelaBancoPortador>();
            return _dbBancoPortador.All().Where(x => x.CompanhiaFiscal == _companhiaFiscal).FirstOrDefault();
        }

        public static int? VerificarSintesePara(int? codSintese)
        {
            Repository<TabelaParametroSintese> _parametroSintese = new Repository<TabelaParametroSintese>();
            int? SintesePara = 0;
            var ListaSintesePara = _parametroSintese.All().Where(x => x.CodSinteseDe == codSintese).ToList();
            SintesePara = ListaSintesePara.Count > 0 ? ListaSintesePara.First().CodSintesePara : 0;

            return SintesePara;

        }


        public static ContratoParcelaBoleto CalcularValoresPrevisto(ContratoParcelaBoleto boleto, TabelaBancoPortador _parametroBanco)
        {
            try
            {
                boleto.JurosAoMesPorcentagemPrevisto = _parametroBanco.JurosAoMesPorcentagem;
                boleto.MultaAtrasoPorcentagemPrevisto = _parametroBanco.MultaAtraso;

                boleto.ValorJurosPrevisto = CalcularValorJurosSimples(
                                                    DbRotinas.ConverterParaDecimal(boleto.ValorDocumentoOriginal),//VALOR PRESENTE
                                                    (decimal)_parametroBanco.JurosAoMesPorcentagem,//TAXA
                                                    boleto.ContratoParcela.DataVencimento,//DAT VENCIMENTO
                                                    (DateTime)boleto.DataVencimento//DATA BOLETO
                                                    );

                boleto.ValorMultaPrevisto = CalcularValorMulta(
                                                    DbRotinas.ConverterParaDecimal(boleto.ValorDocumentoOriginal),//VALOR
                                                    _parametroBanco.MultaAtraso,//MULTA
                                                    boleto.ContratoParcela.DataVencimento,//VENCIMENTO PARCELA
                                                    (DateTime)boleto.DataVencimento //DATA VENCIMENTO BOLETO
                                                    );

                boleto.ValorDocumentoPrevisto = DbRotinas.ConverterParaDecimal(boleto.ValorJurosPrevisto) 
                                                    + DbRotinas.ConverterParaDecimal(boleto.ValorMultaPrevisto) 
                                                    + DbRotinas.ConverterParaDecimal(boleto.ValorDocumentoOriginal);

                return boleto;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao calcular valores previstos.", ex);
            }
        }

        public static DateTime proximoDiaUtil(DateTime dt)
        {
            while (true)
            {
                if (dt.DayOfWeek == DayOfWeek.Saturday)
                {
                    dt = dt.AddDays(2);
                    return proximoDiaUtil(dt);
                }
                else if (dt.DayOfWeek == DayOfWeek.Sunday)
                {
                    dt = dt.AddDays(1);
                    return proximoDiaUtil(dt);
                }
                else if (Feriado(dt) == true)
                {
                    dt = dt.AddDays(1);
                    return proximoDiaUtil(dt);
                }
                else return dt;
            }
        }

        public static bool Feriado(DateTime dt)
        {
            Repository<ParametroFeriado> _dbFeriado = new Repository<ParametroFeriado>();
            var _listFeriados = _dbFeriado.FindAll(x => x.Data == dt && x.Ativo == true && !x.DataExcluido.HasValue).ToList();

            if (_listFeriados.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}