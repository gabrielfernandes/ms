﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using Cobranca.Web.Models;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas;
using System.Linq.Dynamic;
using System.Web.UI.WebControls;
using Cobranca.Db.Models.Pesquisa;

namespace Cobranca.Web.Rotinas
{

    public static class WebRotinas
    {
        private static Repository<Usuario> objUsuairoDb = new Repository<Usuario>();
        private static Repository<UsuarioRegional> objUsuarioRegionalDb = new Repository<UsuarioRegional>();
        private static Repository<ParametroPlataforma> objParametroDb = new Repository<ParametroPlataforma>();

        static readonly string PasswordHash = "P@@Sw0rd";
        static readonly string SaltKey = "S@LT&KEYS";
        static readonly string VIKey = "@1B2c3D4e5F6g7H8";

        public static long GetCurrentUserId()
        {
            long userid = 0;

            if (HttpContext.Current.Request.IsAuthenticated)
            {
                var cookie = HttpContext.Current.Request
                    .RequestContext
                    .HttpContext
                    .Request
                    .Cookies[FormsAuthentication.FormsCookieName];

                if (null == cookie)
                    return 0;

                var decrypted = FormsAuthentication.Decrypt(cookie.Value);

                if (!string.IsNullOrEmpty(decrypted.UserData))
                    long.TryParse(decrypted.UserData, out userid);
            }
            else
            {
                EncerrarSessao();
            }

            return userid;
        }

        public static void EncerrarSessao()
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();

        }

        internal static void AutenticarUsuario(Usuario user)
        {

            UsuarioLogadoInfo objUser = ConverterObjUsuario(user);


            //// Clear any lingering authencation data
            //FormsAuthentication.SignOut();
            //// Write the authentication cookie
            //FormsAuthentication.SetAuthCookie(objUser.Token, false);

            //HttpCookie cookie = FormsAuthentication.GetAuthCookie(objUser.Token, true);
            //FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
            //FormsAuthenticationTicket newTicket = new FormsAuthenticationTicket(
            //     ticket.Version, objUser.Token, DateTime.Now, DateTime.Now.AddDays(360)
            //    , true, objUser.Cod.ToString(), ticket.CookiePath
            //);

            //string encTicket = FormsAuthentication.Encrypt(newTicket);
            //cookie.Value = encTicket;
            //System.Web.HttpContext.Current.Response.Cookies.Add(cookie);

            //HttpContext.Current.Session[objUser.Token] = objUser;
            string formsCookieStr = string.Empty;
            string userRoleData = "";
            int timeExpire = 20;
            if (!HttpContext.Current.Request.IsLocal) timeExpire = 180;

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket
            (
                    1,                              // version
                    objUser.Token,                  // user name
                    DateTime.Now,                   // issue time
                    DateTime.Now.AddMinutes(timeExpire),    // expires
                    false,                          // Persistence
                    userRoleData                    // user data

            );
            formsCookieStr = FormsAuthentication.Encrypt(ticket);
            HttpCookie FormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, formsCookieStr);
            HttpContext.Current.Response.Cookies.Add(FormsCookie);


            WebRotinas.AtualizarIdiomaCache(user.IdiomaTipo ?? 1);
        }

        internal static string GerarSenhaTemporaria()
        {
            //return Membership.GeneratePassword(6, 1);

            string guid = Guid.NewGuid().ToString();
            guid = guid.Substring(0, 6);
            return guid;

        }

        public static void CookieGravar(string p, string dadosSerializado)
        {
            HttpContext.Current.Session[p] = dadosSerializado;// CriptografarString(dadosSerializado);
        }

        public static string CriptografarString(string dados)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(dados);

            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }

        internal static void CookieRemover(string id)
        {
            HttpContext.Current.Session[id] = null;
        }

        public static string DescriptografarString(string dados)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(dados);
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());

        }

        internal static string ObterIP()
        {
            return HttpContext.Current.Request.UserHostAddress;
        }

        private static string SerializarDados(object dados)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(dados.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, dados);
                return textWriter.ToString();
            }
        }
        private static T DeserializarDados<T>(string dados)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            var info = (T)Activator.CreateInstance(typeof(T));
            var reader = XmlReader.Create(dados.Trim().ToStream(), new XmlReaderSettings() { ConformanceLevel = ConformanceLevel.Document });
            info = (T)serializer.Deserialize(reader);

            //using (Stream reader = ToStream(dados))
            //{   
            //    info = (T)serializer.Deserialize(reader);
            //    reader.Close();
            //}
            return info;

        }

        public static T ParseXML<T>(this string @this) where T : class
        {
            var reader = XmlReader.Create(@this.Trim().ToStream(), new XmlReaderSettings() { ConformanceLevel = ConformanceLevel.Document });
            return new XmlSerializer(typeof(T)).Deserialize(reader) as T;
        }

        public static Stream ToStream(this string @this)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(@this);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
        public static string CookieLer(string p)
        {
            string resultado = string.Empty;
            var dados = HttpContext.Current.Session[p];
            if (dados != null)
            {
                resultado = dados.ToString();
            }
            return resultado;
        }
        public static T CookieObjetoLer<T>(string id)
        {
            var info = (T)Activator.CreateInstance(typeof(T));

            object dados = HttpContext.Current.Session[id];
            if (dados != null)
            {
                info = (T)dados;
            }
            return info;
        }

        public static UsuarioLogadoInfo ObterUsuarioLogado()
        {
            var objUsr = new UsuarioLogadoInfo();

            if (HttpContext.Current != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                string[] dados = HttpContext.Current.User.Identity.Name.Split('|');

                objUsr.Cod = DbRotinas.ConverterParaInt(dados[0]);
                objUsr.Nome = DbRotinas.ConverterParaString(dados[1]);
                objUsr.Email = DbRotinas.ConverterParaString(dados[2]);
                objUsr.PerfilTipo = (Enumeradores.PerfilTipo)DbRotinas.ConverterParaShort(dados[3]);
                objUsr.TipoFluxo = (Enumeradores.TipoFluxo)DbRotinas.ConverterParaShort(dados[4]); ;
                objUsr.Modulo = (Enumeradores.SistemaModulo)DbRotinas.ConverterParaShort(dados[5]); ;
                objUsr.DataInicioSessao = DbRotinas.ConverterParaDatetime(dados[6]);
                objUsr.Login = DbRotinas.ConverterParaString(dados[7]);
                objUsr.CodEmpresaVenda = DbRotinas.ConverterParaInt(dados[8]);
                objUsr.CodEscritorio = DbRotinas.ConverterParaInt(dados[9]);
                objUsr.Admin = DbRotinas.ConverterParaBool(dados[10]);
                objUsr.CodRegional = DbRotinas.ConverterParaInt(dados[11]);
                if (dados.Length > 12)
                {
                    objUsr.Idioma = (Enumeradores.IdiomaTipo)DbRotinas.ConverterParaShort(dados[12]);
                }
                else
                {
                    objUsr.Idioma = Enumeradores.IdiomaTipo.pt_BR;
                }

            }
            return objUsr;
        }

        public static List<int> ObterRegionailIdsDoUsuarioLogado()
        {
            var objUsr = WebRotinas.ObterUsuarioLogado();
            List<int> Ids = new List<int>();
            Ids = objUsuarioRegionalDb.All().Where(x => x.CodUsuario == objUsr.Cod).Select(x => x.CodRegional).ToList();
            return Ids;
        }

        private static UsuarioLogadoInfo ConverterObjUsuario(Usuario user)
        {

            Enumeradores.SistemaModulo modulo = Enumeradores.SistemaModulo.Cobranca;
            Enumeradores.TipoFluxo tipofluxo = Enumeradores.TipoFluxo.Cobranca;

            if (user.SistemaCobranca == true)
            {
                modulo = Enumeradores.SistemaModulo.Cobranca;
                tipofluxo = Enumeradores.TipoFluxo.Cobranca;
            }



            return new UsuarioLogadoInfo()
            {
                Cod = user.Cod,
                Nome = user.Nome,
                Email = user.Email,
                PerfilTipo = (Enumeradores.PerfilTipo)user.PerfilTipo,
                Modulo = modulo,
                DataInicioSessao = DateTime.Now,
                TipoFluxo = tipofluxo,
                Login = user.Login,
                CodEmpresaVenda = user.CodEmpresaVenda,
                CodEscritorio = user.CodEscritorio,
                Admin = user.Admin,
                CodRegional = user.CodRegional,
                Idioma = ((Enumeradores.IdiomaTipo)(user.IdiomaTipo ?? 1))
            };



        }

        internal static void AutenticarCliente(Cliente user)
        {
            // Clear any lingering authencation data
            FormsAuthentication.SignOut();
            // Write the authentication cookie
            FormsAuthentication.SetAuthCookie(user.Nome, false);


            HttpCookie cookie = FormsAuthentication.GetAuthCookie(user.Nome, true);
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
            FormsAuthenticationTicket newTicket = new FormsAuthenticationTicket(
                 ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration
                , ticket.IsPersistent, user.Cod.ToString(), ticket.CookiePath
            );

            string encTicket = FormsAuthentication.Encrypt(newTicket);
            cookie.Value = encTicket;
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);

            HttpContext.Current.Session[string.Format("user_{0}", user.Cod)] = user;
        }




        #region Conversoes
        private static long ConverterParaLong(object p)
        {
            if (p != null)
            {
                return WebRotinas.ConverterParaLong(p.ToString());
            }
            else
                return 0;
        }

        private static long ConverterParaLong(string p)
        {
            long valor = 0;

            long.TryParse(p, out valor);

            return valor;
        }
        #endregion


        public static bool UsuarioAutenticado
        {
            get
            {
                return HttpContext.Current.User.Identity.IsAuthenticated;

                //return WebRotinas.ObterUsuarioLogado().Cod > 0;

            }
        }




        public static string _userCookieKey { get { return string.Format("user_{0}", GetCurrentUserId()); } }

        public static void CookieObjetoGravar(string codigo, object dados)
        {
            HttpContext.Current.Session[codigo] = dados;

        }
        public static Enumeradores.VersaoEmpresa ObterVersaoEmpresa()
        {
            int valor = DbRotinas.ConverterParaInt(System.Configuration.ConfigurationManager.AppSettings["VersaoEmpresa"]);
            return (Enumeradores.VersaoEmpresa)valor;
        }

        public static void ParametroEmpresaGravar(ParametroPlataforma parametro)
        {
            string valor = string.Format("{1}|{0}|{2}", parametro.LayoutTipo, parametro.CaminhoLogo, parametro.Icone);
            CookieGravar("_parametroDoSistemaInfo", valor);
        }

        public static ParametroDoSistemaInfo ObterParametro()
        {
            var obj = new ParametroDoSistemaInfo();

            string valor = CookieLer("_parametroDoSistemaInfo");

            if (!string.IsNullOrEmpty(valor))
            {
                string[] dados = valor.Split('|');
                obj.LogoEmpresa = DbRotinas.ConverterParaString(dados[0]);
                obj.LayoutSistema = (Enumeradores.LayoutSistema)DbRotinas.ConverterParaShort(dados[1]);
                obj.Icone = DbRotinas.ConverterParaString(dados[2]);
            }
            else
            {
                Repository<ParametroPlataforma> objParam = new Repository<ParametroPlataforma>();
                var parametro = objParam.All().FirstOrDefault();
                ParametroEmpresaGravar(parametro);
                return ObterParametro();
            }
            return obj;
        }

        public static string ObterLayoutSistema()
        {
            Enumeradores.LayoutSistema tp = ObterParametro().LayoutSistema;
            switch (tp)
            {
                case Enumeradores.LayoutSistema.Green:
                    return "<link href='~/Content/App-green.css' rel='stylesheet'/>";
                case Enumeradores.LayoutSistema.Purple:
                    return "<link href='~/Content/App-purple.css' rel='stylesheet'/>";
                case Enumeradores.LayoutSistema.Blue:
                    return "<link href='~/Content/App-blue.css' rel='stylesheet'/>";
                default:
                    return string.Empty;
            }
        }

        public static void AtualizarIdiomaCache(short tp)
        {
            WebRotinas.CookieObjetoGravar("idioma", tp);

            Idioma.LimparCache();
        }

        public static string ObterArquivoTemp(string fileTemp = null)
        {
            string fullPath = HttpContext.Current.Server.MapPath("~/temp");

            WebRotinas.CriarDiretorioSemErro(fullPath);

            if (string.IsNullOrEmpty(fileTemp)) { fileTemp = Path.GetTempFileName(); };

            return Path.Combine(fullPath, Path.GetFileName(fileTemp));
        }
        public static void GravarArquivo(string caminhoPasta, string texto)
        {
            try
            {
                var caminhoArquivoLog = Path.Combine(caminhoPasta);
                try
                {
                    if (!Directory.Exists(caminhoPasta))
                        Directory.CreateDirectory(caminhoPasta);
                }
                catch { }

                FileStream fs = null;
                if (!File.Exists(caminhoArquivoLog))
                    fs = new FileStream(caminhoArquivoLog, FileMode.OpenOrCreate, FileAccess.Write);
                else if (File.Exists(caminhoArquivoLog) && File.GetLastWriteTime(caminhoArquivoLog) < DateTime.Today)
                    fs = new FileStream(caminhoArquivoLog, FileMode.OpenOrCreate, FileAccess.Write);
                else
                    fs = new FileStream(caminhoArquivoLog, FileMode.Append);

                StreamWriter sw = new StreamWriter(fs);

                sw.WriteLine(texto);

                sw.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao escrever as linhas do arquivo", ex);
            }
        }

        public static void CriarDiretorioSemErro(string fullPath)
        {
            try
            {
                if (!Directory.Exists(fullPath))
                    Directory.CreateDirectory(fullPath);
            }
            catch (Exception ex)
            {
            }
        }

        public class junixToken
        {
            private readonly string PasswordHash = "CrP@@w0rd";
            private readonly string SaltKey = "1@70&CRP$$";
            private readonly string VIKey = "@2C2d7D9c6F78e7C9";
            private string CriptografarString(string dados)
            {
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(dados);

                byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
                var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
                var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

                byte[] cipherTextBytes;

                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                        cryptoStream.FlushFinalBlock();
                        cipherTextBytes = memoryStream.ToArray();
                        cryptoStream.Close();
                    }
                    memoryStream.Close();
                }
                return Convert.ToBase64String(cipherTextBytes);
            }
            private string DescriptografarString(string dados)
            {
                byte[] cipherTextBytes = Convert.FromBase64String(dados);
                byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
                var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

                var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
                var memoryStream = new MemoryStream(cipherTextBytes);
                var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                memoryStream.Close();
                cryptoStream.Close();
                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());

            }
            public bool atualizarParametros()
            {
                return registrar();
            }

            private bool registrar()
            {
                var _param = objParametroDb.All().First();
                var now = this.CriptografarString(DbRotinas.ConverterParaString(DateTime.Now.Date));
                if (_param.TmNow != now)
                {
                    _param.TmNow = now;
                    objParametroDb.Update(_param);

                    if (DbRotinas.ConverterParaDatetime(this.DescriptografarString(_param.TmOff)).Date < DateTime.Now.Date)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public class DTRotinas
        {

            public static List<object> DTGetResult(DTParameters param, IQueryable<object> dtResult)
            {
                if (param.Length > 0)
                {
                    return DTFilterResult(dtResult, param).Skip(param.Start).Take(param.Length).ToList();
                }
                else
                {
                    return DTFilterResult(dtResult, param).Skip(param.Start).ToList();
                }
            }
            public static List<object> DTGetResultExport(DTParameters param, IQueryable<object> dtResult)
            {
                return DTFilterResult(dtResult, param).Skip(param.Start).ToList();
            }
            public static int DTCount(DTParameters param, IQueryable<object> dtResult)
            {
                return DTFilterResult(dtResult, param).ToList().Count();
            }
            public static IEnumerable<object> DTFilterResult(IQueryable<object> dtResult, DTParameters param)
            {
                var _where = "1 = 1";
                if (param.Columns != null)
                {
                    foreach (var col in param.Columns)
                    {
                        if (col.Search.Value != null)
                        {
                            var search = col.Search.Value;
                            string[] expression = { ">", "<", "<>", "=", ">=", "<=", "=>", "=<" };
                            string[] expressionLinq = { "!=", "==" };
                            string[] expressionDate = { "/" };
                            string[] expressionNumber = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
                            if (expression.Any(x => search.Contains(x)))
                            {
                                search = search.Replace(",", ".");
                                if (search.Contains("<>"))
                                {
                                    _where += $@" && {col.Name} != {search.Replace("<>", "")}";
                                }
                                else if (search.Contains("="))
                                {

                                    if (search.Contains(">="))
                                    {
                                        _where += $@" && {col.Name} {search}";
                                    }
                                    else if (search.Contains("<="))
                                    {
                                        _where += $@" && {col.Name} {search}";
                                    }
                                    else if (search.Contains("=>"))
                                    {
                                        _where += $@" && {col.Name} >= {search.Replace("=>", "")}";
                                    }
                                    else if (search.Contains("=<"))
                                    {
                                        _where += $@" && {col.Name} <= {search.Replace("=<", "")}";
                                    }
                                    else
                                    {
                                        _where += $@" && {col.Name} == {search.Replace("=", "")}";
                                    }
                                }
                                else
                                {
                                    _where += $@" && {col.Name} {search}";
                                }
                            }
                            else if (expressionDate.Any(x => search.Contains(x)))
                            {
                                _where += $@" && {col.Name} == Convert.ToDateTime(""{ DbRotinas.ConverterParaDatetime(search) }"")";
                            }
                            else if (expressionNumber.Any(x => search.Contains(x)))
                            {
                                _where += $@" && {col.Name}.ToString().Contains(""{search}"")";
                            }
                            else
                            {
                                _where += $@" && {col.Data}.ToLower().Contains(""{col.Search.Value.Replace("*", "").ToLower()}"")";
                            }

                        }
                    }
                }
                var _order = "";
                if (param.Order != null)
                {
                    foreach (var o in param.Order)
                    {
                        if (o != null)
                        {
                            _order += $@"{param.Columns[o.Column].Name} {o.Dir}";
                        }
                        if (_order == " ASC" || _order == " DESC")
                        {
                            _order = "Cod ASC";
                        }
                    }
                }
                var teste = (dtResult.AsEnumerable().Where(_where));
                if (param.Order != null)
                {
                    teste = teste.OrderBy(_order);
                }
                return teste;
            }


            public static DTResult DTResult(DTParameters param, IQueryable<object> dtResult)
            {
                var data = DTGetResult(param, dtResult);
                int count = DTCount(param, dtResult);
                DTResult result = new DTResult
                {
                    draw = param.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return result;
            }

            public static DTResult DTResult<T>(DTParameters param, List<T> dtResult)
            {
                var list = dtResult.Cast<object>();
                var data = DTGetResult(param, list.AsQueryable());
                int count = dtResult.Count();
                DTResult result = new DTResult
                {
                    draw = param.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return result;
            }
        }

        public static HtmlString HTMLIif(bool ok, string html)
        {
            return ok ? new HtmlString(html) : new HtmlString(string.Empty);
        }


        public static string _filtroGeralSessaoKey { get { return string.Format("filter_all_{0}", GetCurrentUserId()); } }

        internal static void FiltroGeralGravar(FiltroGeralDados filter)
        {
            HttpContext.Current.Session[_filtroGeralSessaoKey] = filter;
        }
        public static FiltroGeralDados FiltroGeralObter()
        {
            if (HttpContext.Current.Session[_filtroGeralSessaoKey] == null)
            {
                return new FiltroGeralDados();
            }
            FiltroGeralDados Filtro = (FiltroGeralDados)HttpContext.Current.Session[_filtroGeralSessaoKey];
            Filtro = FiltroGeralTratarUsuarioAFiltrar(Filtro);
            return Filtro;
        }
        internal static void FiltroGeralRemover()
        {
            HttpContext.Current.Session[_filtroGeralSessaoKey] = null;
        }

        private static FiltroGeralDados FiltroGeralTratarUsuarioAFiltrar(FiltroGeralDados model)
        {
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            Usuario UsuarioFiltrar = new Usuario();
            if (usuarioLogado.PerfilTipo == Enumeradores.PerfilTipo.Atendimento || usuarioLogado.PerfilTipo == Enumeradores.PerfilTipo.Regional)
            {
                //SE O USUARIO FOR ATENDIMENTO DEVERA PEGAR O USUARIO LOGADO
                UsuarioFiltrar = objUsuairoDb.FindById(usuarioLogado.Cod);
            }
            else
            {
                //SE O USUARIO FILTRAR ALGUM ATENDENTE 
                if (model.CodUsuarioAtendimento > 0)
                {
                    UsuarioFiltrar = objUsuairoDb.FindById((int)model.CodUsuarioAtendimento);
                }
                //CASO USUARIO NAO FILTRAR NINGUEM
                else
                {
                    UsuarioFiltrar = objUsuairoDb.FindById(usuarioLogado.Cod);
                }
            }
            model.CodUsuarioAtendimento = UsuarioFiltrar.Cod;
            model.CodPerfilTipo = UsuarioFiltrar.PerfilTipo;
            return model;
        }

        internal static FiltroGeralDados AplicarFiltros(FiltroGeralDados dados)
        {

            FiltroGeralDados filtro = dados;    //new FiltroGeralDados(dados.AgingIds,dados.Bloqueado,dados.CPF_CNPJ,dados.ClienteId,dados.CodContratos,dados.CodAging,dados.CodigoCliente,dados.CodTipoCobranca,dados.CodConstrutora,dados.CodRegional,dados.CodProduto,dados.CodSintese,dados.CodPerfilTipo,dados.CodProduto,dados.CodRegionais,dados.CodConstrutoras,dados.CodAging,dados.CodRotas,dados.CodUsuarioAtendimento,dados.CodFase,dados.CodGrupo,dados.CodTipoBloqueio,dados.CodContrato,);
            FiltroGeralGravar(filtro);
            return filtro;
        }
    }
}