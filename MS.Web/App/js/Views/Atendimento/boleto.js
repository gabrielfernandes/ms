﻿var _today = new Date();


function emitirBoleto(url) {
    var height = $(window).height();
    height = (height - 420);

    $.ajax({
        url: url,
        type: 'post'
    }).success(function (data) {
        var win = window.open(url, "_blank", "width=350, toolbar=no, scrollbars=yes, resizable=no");
        with (win.document) {
            open();
            write(data.Html);
            close();
        }

        var html = $("#div-boleto");
        var div = $(document.createElement("div"));
        div.addClass("form-group-sm")
        html.append(div.html(data.Html));
        win.window.print();
        // We'll make our own renderer to skip this editor
        var specialElementHandlers = {
            '#editor': function (element, renderer) {
                return true;
            }
        };
        var doc = new jsPDF();

        // All units are in the set measurement for the document
        // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
        doc.fromHTML($('#div-boleto').get(0), 15, 15, {
            'width': 170,
            'elementHandlers': specialElementHandlers
        });
    });

}
function createPDF(html) {
    getCanvas().then(function (canvas) {
        var
            img = canvas.toDataURL("image/png"),
            doc = new jsPDF({
                unit: 'px',
                format: 'a4'
            });
        var specialElementHandlers = {
            '#editor': function (element, renderer) {
                return true;
            }
        };
        doc.fromHTML($('#render_me').get(0), 15, 15, {
            'width': 170,
            'elementHandlers': specialElementHandlers
        });
        doc.addImage(img, 'JPEG', 20, 20);
        doc.save('techumber-html-to-pdf.pdf');
        form.width(cache_width);
    });
}
function getCanvas() {
    form.width((a4[0] * 1.33333) - 80).css('max-width', 'none');
    return html2canvas(form, {
        imageTimeout: 2000,
        removeContainer: true
    });
}

function openBoleto() {
    var _ticked = "&t=" + _today.getSeconds();
    var rowcollection = _tb.$("input[type='checkbox']:checked", { "page": "all" });
    var cod = 0;
    var dados = [], total = [];

    rowcollection.each(function (index, elem) {
        var data = moment($(elem).attr("data-vencimento"), 'DD/MM/YYYY').format();
        var Today = moment().format();
        total.push(index);
        cod = $(elem).attr("data-id");
        dados.push(cod);
        //if (moment(data).isAfter(Today) || moment(Today).isSame(data, 'd')) {
        //    cod = $(elem).attr("data-id");
        //    dados.push(cod);
        //}
    });
    $.get('/ContratoParcelaBoleto/ValidarBoleto/?p=' + dados + _ticked, function (r) {
        if (r.OK) {
            modalBoleto(dados);
        } else {
            jx.mensagem(r.Titulo, r.Mensagem, 'warning', 'fa fa-exclamation ');
        }
    });

    //if (cod > 0) {
    //    if (total.length < dados.length) {
    //        jx.mensagem('Faturas!', 'Serão gerados somente as faturas à vencer.', 'warning', 'fa fa-check');
    //    }
    //} else if (total.length > 0) {
    //    jx.mensagem('Faturas!', 'Só poderão emitir boletos original de faturas à vencer.', 'warning', 'fa fa-check');
    //} else {
    //    jx.mensagem('Faturas!', 'Selecione uma fatura à vencer.', 'warning', 'fa fa-check');
    //}
}

function SolicitarAvulso() {
    var cliente = $("#CodCliente").val();
    var listaContratos = $("#filtContrato").val();
    $.ajax({
        url: '/ContratoParcelaBoleto/ValidarSolicitacaoAvulso',
        type: 'post',
        dataType: 'json',
        cache: false,
        data: { ClienteId: cliente, Contratos: listaContratos }
    }).success(function (r) {
        if (r.Success) {
            AbrirSolicitacaoAvulso(cliente, listaContratos);
        }
        else {
            jx.mensagem("Boleto Avulso", r.Message, 'warning', 'fa fa-exclamation ');
            debugger;
            for (var i = 0; i < r.Data.length; i++) {
                jx.mensagem(r.Data[i].Property, r.Data[i].Message, 'warning', 'fa fa-exclamation ');
            }

        }
    });
}

function AbrirSolicitacaoAvulso(c, contratos) {
    var $form = $("#modal-form-solicitacao");
    var $formTitle = $("#modal-form-solicitacao .modal-title");
    var $formBody = $("#modal-form-solicitacao .modal-body");
    var $formFooter = $(".modal-footer");
    $formBody.empty();
    $formFooter.empty();
    $formTitle.empty();
    $formTitle.text("BOLETO AVULSO");
    $formBody.load("/ContratoParcelaBoleto/CarregarAvulso/?cliente=" + c + "&contratos=" + contratos, function () {
        $formBody.css("padding-top", "0px")
        $formFooter.append('<button type="button" class="btn btn-sm btn-modal-save" onclick="SaveBoletoAvulso()"><i class="fa fa-save"></i> Salvar</button>')
        $formFooter.append('<button type="button" class="btn btn-sm btn-modal-cancel" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>')
        $formFooter.show();
        $form.modal("show");
        var dataPadrao = $form.find("#DataPadrao").val();
        $form.find("input[data-boleto-valida]")
            .mask("00/00/0000")
            .focusout(function (event) {
                var target, phone, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                value = target.value.replace(/\D/g, '');
                element = $(target);
                element.unmask();
                if (value.length = 8) {
                    var DataAgenda = moment(value, 'DD/MM/YYYY').format();
                    var dtPadrao = moment(dataPadrao, 'DD/MM/YYYY').format();
                    $(this).removeClass("date-red animated shake");
                    if (!moment(DataAgenda).isAfter(dtPadrao)) {
                        $(this).addClass("date-red animated shake", "easeOutBounce");
                    }
                }
            });
        $formFooter.draggable({ handle: ".modal-header" });
    });
}
function SaveBoletoAvulso() {
    var $form = $("#form-solicitacao-avulso");
    $.ajax({
        url: '/ContratoParcelaBoleto/SalvarBoletoAvulso/',
        type: 'post',
        dataType: 'json',
        cache: false,
        data: jx.formRotinas.obterDadosForm($form)
    }).success(function (r) {
        if (r.Success) {
            $("#modal-form-solicitacao").modal('toggle');
            jx.mensagem("Boleto Avulso", r.Message, 'success', 'fa fa-check');
        }
        else {
            r.Data.forEach(function (value, chave) {
                jx.mensagem("Boleto Avulso", value.Message, 'warning', 'fa fa-exclamation ');
            });
        }
    });
}

function myfunction() {
    console.log('teste');
}

function modalBoleto(dados) {
    var idCliente = $("#CodCliente").val();
    $("#modal-form-historico .modal-body").empty();
    $(".modal-footer").empty();
    $("#modal-form-historico .modal-title").empty().text("Envio Boleto");
    $("#modal-form-historico .modal-body").load("/Atendimento/EnvioBoleto/?idCliente=" + idCliente + "&idParcela=" + dados + " #form-envio-boleto", function () {
        $(".modal-footer").hide()
        $("#modal-form-historico").modal("show");
        $("#btn-submit").off("click").click(function () { $("#form-envio-boleto").submit() });

        $("#TipoEmail").selectpicker('refresh');
        $("#TipoEmail").on('changed.bs.select', function () {
            MontarModeloEmail(dados, $("#TipoEmail").val());
        });
        $('.editor-html').summernote({ height: '280px' });
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
    });
};

function MontarModeloEmail(parcelas, modeloEmail) {
    $.ajax({
        url: '/ContratoParcelaBoleto/MontarModeloEmail',
        type: 'post',
        dataType: 'json',
        cache: false,
        data: { Parcelas: parcelas, CodModeloAcao: modeloEmail }
    }).success(function (r) {
        if (r.Success) {
            $("#Mensagem").val(r.Data);
            $('.editor-html').summernote({ height: '280px' });
        }
        else {
            console.log(r.Message);
        }
    });

}

function EnviarPorEmail() {
    var clienteId = $("#CodCliente").val();
    var parcelaIds = $("#modal-form-historico").find("#ParcelaIds").val();
    var emailDestinho = $("#modal-form-historico").find("#Email").val();
    var emailAssunto = $("#modal-form-historico").find("#Assunto").val();
    var msg = $("#modal-form-historico").find("#Mensagem").val();
    $.ajax({
        url: '/Atendimento/EnviarBoletoEmail',
        type: 'post',
        dataType: 'json',
        data: { CodCliente: clienteId, parcelas: parcelaIds, email: emailDestinho, assunto: emailAssunto, mensagem: msg }
    }).success(function (r) {
        if (r.OK) {
            $('#modal-form-historico').modal('toggle');
            alert("E-mail enviado!");
        }
        else {
            console.log(r.Mensagem);
        }
    });
}

var objSolicitacao = function () {
    this.NumeroDocumento = undefined;
    this.ValorDocumento = undefined;
    this.JurosAoMesPorcentagem = undefined;
    this.DataVencimento = undefined;
    this.MultaAtraso = undefined;
    this.ValorAtualizado = undefined;
    this.DataAntiga = undefined;
};
function atualizarBoleto() {
    var valido = ValidarDataVencimentos();
    if ($.inArray(false, valido) == -1) {
        var objButton = {
            text: '<i class="fa fa-refresh"></i>', className: 'dt-button-filter', action: function (e, dt, node, config) {
                $table.block({
                    message: '', css: {
                        border: 'none', padding: '15px', backgroundColor: '#fff', '-webkit-border-radius': '10px', '-moz-border-radius': '10px', opacity: .5, color: '#fff', width: '50%'
                    }, overlayCSS: { backgroundColor: '#FFF' }
                }); atualizarBoleto(); dt.ajax.reload();
            }
        }
        var _dtBoleto = $('#tbBoleto').DataTable({
            "buttons": [objButton],
        });
        var $form = $("#form-solicitacao"), $table = $('#tbBoleto');
        _dtBoleto.destroy();
        _dtBoleto = $table.DataTable({
            "order": [[1, 'desc']],
            "deferRender": true,
            "serverSide": true,
            "processing": true,
            "paging": false,
            "sScrollY": "250px",
            "bCollapse": true,
            "ajax": {
                "url": "/ContratoParcelaBoleto/ComputarValores/",
                "data": jx.formRotinas.obterDadosForm($form),
                "cache": false,
                "type": "POST"
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(0),td:eq(1),td:eq(2),td:eq(3)', nRow).addClass("text-center");
                $('td:eq(4),td:eq(5)', nRow).addClass("text-right");
            },
            'initComplete': function (settings, json) {
                var tb = _dtBoleto.data();
                tb.inicializar($table);
                tb.sum();
            },
            "fnDrawCallback": function (oSettings) {
                _dtBoleto.data().sum();
                $table.unblock();
                $table.removeClass('reloading');
            },
            "buttons": [objButton],
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "buttons": {
                    "selectAll": "Marcar Todos",
                    "selectNone": "Desmarcar Todos"
                },
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                oAria: {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "dom": '<"row"<"col-xs-12 col-md-12"B>>' +
                '<"row"<"col-xs-12 col-md-12"t>>',
        });
    }
    else {
        jx.mensagem('Informações Incorretas!', 'Data de Vencimento Inválida.', 'warning', 'fa fa-check');
    }

}

function SolicitarAlteracao() {
    var _ticked = "&t=" + _today.getSeconds();
    var rowcollection = _tb.$("input[type='checkbox']:checked", { "page": "all" });
    var cod = 0;
    var dados = [];
    rowcollection.each(function (index, elem) {
        cod = $(elem).attr("data-id");
        dados.push(cod);
    });
    $.get('/ContratoParcelaBoleto/ValidarSolicitacaoAlterarExistente/?p=' + dados + _ticked, function (r) {
        if (r.Success) {
            AlterarBoleto(dados);
        } else {
            console.log(r);
            jx.mensagem("Validação", r.Message, 'warning', 'fa fa-exclamation ');
        }
    });
}

function AlterarBoleto(dados) {
    var _ticked = "&t=" + _today.getSeconds();
    $("#modal-form-solicitacao .modal-body").empty();
    $(".modal-footer").empty();
    $("#modal-form-solicitacao .modal-title").empty().text("ALTERAR BOLETO");
    $("#modal-form-solicitacao .modal-body").load("/ContratoParcelaBoleto/FormModificar/?p=" + dados.toString(), function () {
        //$(".modal-footer").hide();
        $("#modal-form-solicitacao .modal-body").css("padding-top", "0px")

        $(".modal-footer").append('<button type="button" class="btn btn-sm btn-modal-save" onclick="SaveBoletoModificar()"><i class="fa fa-save"></i> Salvar</button>')
        $(".modal-footer").append('<button type="button" class="btn btn-sm btn-modal-cancel" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>')
        $(".modal-footer").show();
        $("#modal-form-solicitacao").modal("show");
        $("#btn-submit").off("click").click(function () { SalvarSolcitacao(); });

        $("#modal-form-solicitacao").find("[data-validate='multa']")
            .mask("##0,00", { reverse: true })
            .focusout(function (event) {
                var target, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                value = target.value.replace(',', '.');
                element = $(target);
                if (value.length > 0) {
                    $(this).removeClass("date-red animated shake");
                    $.get('/ContratoParcelaBoleto/ValidarMulta/?multa=' + parseFloat(value) + _ticked, function (r) {
                        if (r.OK) {
                            console.log('multa OK');
                        } else {
                            $("#tabSolicitacao").click();
                            element.addClass("date-red animated shake", "easeOutBounce");
                            jx.mensagem(r.Titulo, r.Mensagem, 'warning', 'fa fa-check ');
                        }
                    });

                }
            });
        $("#modal-form-solicitacao").find("[data-validate='juros']")
            .mask("##0,00", { reverse: true })
            .focusout(function (event) {
                var target, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                value = target.value.replace(',', '.');
                element = $(target);
                if (value.length > 0) {
                    $(this).removeClass("date-red animated shake");
                    $.get('/ContratoParcelaBoleto/ValidarJuros/?juros=' + parseFloat(value) + _ticked, function (r) {
                        if (r.OK) {
                            console.log('Juros OK');
                        } else {
                            $("#tabSolicitacao").click();
                            element.addClass("date-red animated shake", "easeOutBounce");
                            jx.mensagem(r.Titulo, r.Mensagem, 'warning', 'fa fa-check ');
                        }
                    });

                }
            });

        $("input[data-boleto-valida]")
            .mask("00/00/0000")
            .focusout(function (event) {
                var target, phone, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                value = target.value.replace(/\D/g, '');
                element = $(target);
                element.unmask();
                if (value.length = 8) {
                    var DataAgenda = moment(value, 'DD/MM/YYYY').format();
                    var Today = moment().format();
                    $(this).removeClass("date-red animated shake");
                    if (!moment(DataAgenda).isAfter(Today)) {
                        $(this).addClass("date-red animated shake", "easeOutBounce");
                    }
                }
            });

        $data_table_boleto = $('#tbBoleto');
        jx.dtableFooter($data_table_boleto);
        $('#tbBoleto').DataTable({
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(0),td:eq(1),td:eq(2),td:eq(3)', nRow).addClass("text-center");
            },
            "buttons": [{
                text: '<i class="fa fa-refresh"></i>', className: 'dt-button-filter', action: function (e, dt, node, config) {
                    $('#tbBoleto').block({
                        message: '', css: {
                            border: 'none', padding: '15px', backgroundColor: '#fff', '-webkit-border-radius': '10px', '-moz-border-radius': '10px', opacity: .5, color: '#fff', width: '50%'
                        }, overlayCSS: { backgroundColor: '#FFF' }
                    }); atualizarBoleto(); dt.ajax.reload();
                }
            }],
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                paging: false,
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",

                "buttons": {
                    "selectAll": "Marcar Todos",
                    "selectNone": "Desmarcar Todos"
                },
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                oAria: {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "dom": '<"row"<"col-xs-12 col-md-12"B>>' +
                '<"row"<"col-xs-12 col-md-12"t>>',
        });
    });
}

function SolicitarBoleto() {
    var _ticked = "&t=" + _today.getSeconds();
    var rowcollection = _tb.$("input[type='checkbox']:checked", { "page": "all" });
    var cod = 0;
    var dados = [];
    rowcollection.each(function (index, elem) {
        cod = $(elem).attr("data-id");
        dados.push(cod);
    });
    $.get('/ContratoParcelaBoleto/ValidarSolicitacaoNovo/?p=' + dados + _ticked, function (r) {
        if (r.OK) {
            SolicitarNovoB(dados);
        } else {
            jx.mensagem(r.Titulo, r.Mensagem, 'warning', 'fa fa-exclamation ');
        }
    });

}

function SolicitarNovoB(dados) {
    var _ticked = "&t=" + _today.getSeconds();
    $("#modal-form-solicitacao .modal-body").empty();
    $(".modal-footer").empty();
    $("#modal-form-solicitacao .modal-title").empty().text("SOLICITAÇÃO NOVO BOLETO");
    $("#modal-form-solicitacao .modal-body").load("/ContratoParcelaBoleto/FormSolicitacao/?p=" + dados.toString() + _ticked, function () {
        //$(".modal-footer").hide();
        $("#modal-form-solicitacao .modal-body").css("padding-top", "0px")

        $(".modal-footer").append('<button type="button" class="btn btn-sm btn-modal-save" onclick="SaveBoletoSocitacao()"><i class="fa fa-save"></i> Salvar</button>')
        $(".modal-footer").append('<button type="button" class="btn btn-sm btn-modal-cancel" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>')
        $(".modal-footer").show();
        $.ajax({
            type: "POST",
            url: "/ContratoParcelaBoleto/ObterObservacao",
            success: function (resultado) {
                if (resultado.OK) {
                    $("#Observacao").val(resultado.obs);
                }
            }
        });

        $("#modal-form-solicitacao").modal("show");
        $("#modal-form-solicitacao").find("[data-validate='multa']")
            .mask("##0,00", { reverse: true })
            .focusout(function (event) {
                var target, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                value = target.value.replace(',', '.');
                element = $(target);
                if (value.length > 0) {
                    $(this).removeClass("date-red animated shake");
                    $.get('/ContratoParcelaBoleto/ValidarMulta/?multa=' + parseFloat(value) + _ticked, function (r) {
                        if (r.OK) {
                            console.log('multa OK');
                        } else {
                            $("#tabSolicitacao").click();
                            element.addClass("date-red animated shake", "easeOutBounce");
                            jx.mensagem(r.Titulo, r.Mensagem, 'warning', 'fa fa-exclamation ');
                        }
                    });

                }
            });
        $("#modal-form-solicitacao").find("[data-validate='juros']")
            .mask("##0,00", { reverse: true })
            .focusout(function (event) {
                var target, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                value = target.value.replace(',', '.');
                element = $(target);
                if (value.length > 0) {
                    $(this).removeClass("date-red animated shake");
                    $.get('/ContratoParcelaBoleto/ValidarJuros/?juros=' + parseFloat(value) + _ticked, function (r) {
                        if (r.OK) {
                            console.log('Juros OK');
                        } else {
                            $("#tabSolicitacao").click();
                            element.addClass("date-red animated shake", "easeOutBounce");
                            jx.mensagem(r.Titulo, r.Mensagem, 'warning', 'fa fa-exclamation ');
                        }
                    });

                }
            });
        $("#btn-submit").off("click").click(function () { $("#form-cadastro-historico").submit() });

        $("input[data-boleto-valida]")
            .mask("00/00/0000")
            .focusout(function (event) {
                var target, phone, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                value = target.value.replace(/\D/g, '');
                element = $(target);
                element.unmask();
                if (value.length = 8) {
                    var DataAgenda = moment(value, 'DD/MM/YYYY').format();
                    var Today = moment().format();
                    $(this).removeClass("date-red animated shake");
                    if (!moment(DataAgenda).isAfter(Today)) {

                        $(this).addClass("date-red animated shake", "easeOutBounce");
                    }
                }
            });

        $data_table_boleto = $('#tbBoleto');
        jx.dtableFooter($data_table_boleto);
        $('#tbBoleto').DataTable({
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td:eq(0),td:eq(1),td:eq(2),td:eq(3)', nRow).addClass("text-center");
            },
            "buttons": [{
                text: '<i class="fa fa-refresh"></i>', className: 'dt-button-filter', action: function (e, dt, node, config) {
                    $('#tbBoleto').block({
                        message: '', css: {
                            border: 'none', padding: '15px', backgroundColor: '#fff', '-webkit-border-radius': '10px', '-moz-border-radius': '10px', opacity: .5, color: '#fff', width: '50%'
                        }, overlayCSS: { backgroundColor: '#FFF' }
                    }); atualizarBoleto(); dt.ajax.reload();
                }
            }],
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                paging: false,
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",

                "buttons": {
                    "selectAll": "Marcar Todos",
                    "selectNone": "Desmarcar Todos"
                },
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                oAria: {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "dom": '<"row"<"col-xs-12 col-md-12"B>>' +
                '<"row"<"col-xs-12 col-md-12"t>>',
        });
    });
}


function ValidarDataVencimentos() {
    var validos = []
    $("input[data-boleto-valida]").each(function (i, e) {
        var $el = $(e);
        var $el = moment($el.val(), 'DD/MM/YYYY').format();
        var Today = moment().format();
        $(e).removeClass("date-red animated shake");
        if (!moment($el).isAfter(Today)) {
            $(e).addClass("date-red animated shake");
            validos.push(false);
        } else {
            validos.push(true);
        }
    })
    return validos;
}


function SaveBoletoSocitacao() {
    var $form = $("#form-solicitacao");
    var $elemObs = $("#Observacao");
    $elemObs.removeClass("date-red animated focus");

    //FOI SOLICITADO RETIRAR A OBRIGATORIEDADE DE JUSTIFICATIVA
    //if (!$elemObs.val()) {
    if (false) {
        jx.mensagem("Solicitação!", "É obrigatório justificar o pedido!", 'warning', 'fa fa-exclamation ');
        $("#tabObs").click();
        $elemObs.addClass("animated shake");
    } else {
        $.ajax({
            url: '/ContratoParcelaBoleto/ValidarSolicitacao/',
            type: 'post',
            dataType: 'json',
            cache: false,
            data: jx.formRotinas.obterDadosForm($form)
        }).success(function (resultado) {
            if (resultado.OK) {
                $.ajax({
                    type: "POST",
                    url: $form.attr('action'),
                    data: jx.formRotinas.obterDadosForm($form),
                    success: function (r) {
                        if (r.Success) {
                            jx.mensagem("Solicitação", r.Message, 'success', 'fa fa-check ');
                            setTimeout(function () {
                                $('#modal-form-solicitacao').modal('toggle');
                            }, 1500)
                        }
                    }
                });
            } else {

                $("#tabSolicitacao").click();
                jx.mensagem(resultado.Titulo, resultado.Mensagem, 'warning', 'fa fa-exclamation ');
            }
        });
    }
}




function SaveBoletoModificar() {
    var $form = $("#form-solicitacao");
    $.ajax({
        url: '/ContratoParcelaBoleto/ValidarSolicitacao/',
        type: 'post',
        dataType: 'json',
        data: jx.formRotinas.obterDadosForm($form)
    }).success(function (resultado) {
        if (resultado.OK) {
            $.ajax({
                type: "POST",
                url: $form.attr('action'),
                data: jx.formRotinas.obterDadosForm($form),
                success: function (r) {
                    if (r.Success) {
                        jx.mensagem("Solicitação", r.Message, 'success', 'fa fa-check ');
                        setTimeout(function () {
                            $('#modal-form-solicitacao').modal('toggle');
                        }, 1500)
                    }
                }
            });
        } else {

            $("#tabSolicitacao").click();
            jx.mensagem(resultado.Titulo, resultado.Mensagem, 'warning', 'fa fa-exclamation ');
        }
    });
}

function validarMulta() {
    var retorno = undefined;
    $("#modal-form-solicitacao input[data-validate='multa']").each(function (i, e) {
        var target = $(e).val();
        value = target.replace(',', '.');
        element = $(e);
        if (value.length > 0) {
            $(this).removeClass("date-red animated shake");
            $.get('/ContratoParcelaBoleto/ValidarMulta/?multa=' + parseFloat(value) + _ticked, function (r) {
                if (r.OK) {
                    console.log('multa OK');
                    retorno = r.OK;
                } else {
                    element.addClass("date-red animated shake", "easeOutBounce");
                    jx.mensagem(r.Titulo, r.Mensagem, 'warning', 'fa fa-exclamation ');
                    $("#tabSolicitacao").click();
                    return Boolean(false);
                }
            });
        }
    });
}
function validarJuros() {
    var _ticked = "&t=" + _today.getSeconds();
    $("#modal-form-solicitacao input[data-validate='juros']").each(function (i, e) {
        var target = $(e).val();
        value = target.replace(',', '.');
        element = $(e);
        if (value.length > 0) {
            $(this).removeClass("date-red animated shake");
            $.get('/ContratoParcelaBoleto/ValidarJuros/?juros=' + parseFloat(value) + _ticked, function (r) {
                if (r.OK) {
                    console.log('Juros OK');
                    return r.OK;
                } else {
                    element.addClass("date-red animated shake", "easeOutBounce");
                    jx.mensagem(r.Titulo, r.Mensagem, 'warning', 'fa fa-exclamation');
                    $("#tabSolicitacao").click();
                    return r.OK;
                }
            });
        }
    });
}
