﻿var $data_table = $('#grid-documento');
jx.dtableSearch($data_table);
jx.dtableFooter($data_table);
var _today = new Date();
var _ticked = "&t=" + _today.getSeconds();
var table = $('#grid-documento').DataTable({
    "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        paging: false,
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",

        "buttons": {
            "selectAll": "Marcar Todos",
            "selectNone": "Desmarcar Todos"
        },
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        oAria: {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    "sDom": '<"col-xs-12 col-md-12 col-lg-12">' +
    '<"col-xs-12 col-md-12"t>' +
    '<"col-xs-12 col-md-6 pull-left"i><"col-xs-12 col-md-6 pull-right"p>'
});
$(document).ready(function () {
    ajustarAlturaIframe();
    obterDados();
});

function selDoc(guid) {
    var url = '/Pesquisa/VisualizaDocumento/' + '?GUID=' + guid;
    $("#visualiza-doc").attr("src", url);
    $("iframe").load(function () {
        height = $("iframe").contents().find("body").height();
        if (height > 0) {
            $("iframe").height(height)//ajusta a altura do iframe;
            $("iframe").contents().find("body").css('text-align', 'center');//centraliza imagem;
        }
        else {
            $("iframe").height($(window).height() - 200)//ajusta a altura do iframe;
        }
    });
}
function downDoc(guid) {
    window.location.href = '/Pesquisa/DownloadSeguro/' + '?cd=' + guid;
}
function ajustarAlturaIframe() {
    $("iframe").height($("iframe").contents().find("body").height() + 40)//ajusta a altura do iframe;
}
function exibirForm() {
    $(".form-exibir").show();
    $("#btn-adcionar").hide();
}
function cancelar() {
    document.location.reload(false);
}
function definirUpload() {
    $(".form-upload").fileupload({
        dataType: 'json',
        url: '/Upload/UploadHandler.ashx',
        maxFileSize: 5368709120, // 5GB
        //sequentialUploads: true,
        limitConcurrentUploads: 1,
        //maxChunkSize: 204800, // 200KB
        //autoUpload: true,//tr
        dropZone: "documentos",
        formData: {
            Id: $("#modal-form-documento #Id").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Ocorreu um erro no upload: " + textStatus);
            $(".progress-bar").removeClass("progress-bar-warning").addClass("progress-bar-danger");
            return;
        },
        done: function (e, data) {
            debugger;
            var file = data.files[0];
            $("#modal-form-documento #btn-file").removeAttr("disabled");
            $("#modal-form-documento #ArquivoNome").val(file.name);
            $(".progress-bar").removeClass("progress-bar-warning").addClass("progress-bar-success");
        },
        add: function (e, data) {
            debugger;
            var id = $(this).attr("id");
            var tr = $(this).parent().parent().parent();
            var file = data.files[0];
            $(".progress-bar").removeClass("progress-bar-danger").removeClass("progress-bar-success").addClass("progress-bar-warning");
            data.submit();
        },
        start: function () {
        },
        progress: function (e, data) {
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $(".progress").show();
            $(".progress-bar").css('width', progress + '%');
            $(".progress-bar").html(progress + '%');
        }
    });
}
function removerDocumento(cod) {
    if (confirm("Deseja excluir?")) {
        $.ajax({
            url: '/Pesquisa/DocumentoRemover/',
            type: 'post',
            data: { cod: cod }
        }).success(function (o) {
            if (o.OK) {
                cancelar();
            } else {
                alert(o.Mensagem);
            }
        });
    }
}

function openFormDocumento() {
    var dados = $("#CodCliente").val();
    $("#modal-form-documento .modal-body").empty();
    $("#modal-form-documento .modal-title").empty().text("Documento");
    $("#modal-form-documento .modal-body").load("/Documento/Modal/?idCliente=" + dados, function () {
        $("#modal-form-documento").modal("show");
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
        definirUpload();

        $("#modal-confirm").off("click").click(function () {
            var $form = $("#form-cadastro-documento");
            $.ajax({
                url: $form.attr('action'),
                type: 'post',
                dataType: 'json',
                data: $form.serialize()
            }).success(function (resultado) {
                if (resultado.OK) {
                    jx.mensagem('Operação Realizada!', 'Documento Salvo.', 'success', 'fa fa-check');
                    $('#modal-form-documento').modal('toggle');
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                } else {
                    jx.mensagem('Data de Agendamento!', resultado.Mensagem, 'danger', 'fa fa-exclamation');
                }
            });
        });
    });
};


function obterDados() {
    var $data_table = $('#grid-documento');
    table.destroy();
    table = $data_table.DataTable({
        "rowId": 'Cod',
        "order": [[1, 'desc']],
        "deferRender": true,
        "serverSide": true,
        "processing": true,
        "ajax": {
            "url": "/Pesquisa/ObterDocDadosPorServer",
            "data": { cdCliente: $("#CodCliente").val() },
            "type": "POST"
        },
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "Filtrados de _MAX_ registros",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "<h2><i class='fa fa-spin fa-spinner text-silver'></i> Carregando os dados..</h2>",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            oAria: {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        "columnDefs": [
            {
                targets: 1,
                searchable: false,
                orderable: false,
                className: '',
                render: function (data, type, row, index) {
                    return '<label class="label pull-left label-success" style="margin-left:4px; font-size:100%;">' + row.TipoDocumento + '</label>'
                }
            },
            {
                targets: 4,
                searchable: false,
                orderable: false,
                className: '',
                render: function (data, type, row, index) {
                    var $div = $("<div></div>");
                    $divBtnGroup = $(document.createElement("div")).addClass('btn-group-xs').attr('role', 'group');
                    var $btnRemove = $(document.createElement("buttom"))
                        .attr("type", "button")
                        .addClass("btn btn-xs btn-danger")
                        .attr("onclick", "removerDocumento('" + row.ArquivoGuid + "')")
                        .append("<i class='fa fa-trash'></i>");
                    var $btnLook = $(document.createElement("buttom"))
                        .attr("type", "button")
                        .addClass("btn btn-xs btn-default")
                        .attr("onclick", "selDoc('" + row.ArquivoGuid + "')")
                        .append("<i class='fa fa-search-plus'></i>");
                    var $btnDownload = $(document.createElement("buttom"))
                        .attr("type", "button")
                        .addClass("btn btn-xs btn-success")
                        .attr("onclick", "downDoc('" + row.ArquivoGuid + "')")
                        .append("<i class='fa fa-download'></i>");
                    $divBtnGroup.append($btnRemove).append($btnLook).append($btnDownload);
                    $div.append($divBtnGroup);
                    return $div.html();

                }
            }],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td:eq(1),td:eq(2),td:eq(3),td:eq(4)', nRow).addClass("text-center");
        },
        "sDom": '<"col-xs-12 col-md-12 col-lg-12">' +
        '<"col-xs-12 col-md-12"t>' +
        '<"col-xs-12 col-md-6 pull-left"i><"col-xs-12 col-md-6 pull-right"p>',
        'initComplete': function (settings, json) {
            var tb = table.data();
            tb.inicializar($data_table);
            tb.addSearch($data_table);
            jx.dTablesUnblock($data_table);
            $("#tb-loading").hide();
        },
        "fnDrawCallback": function (oSettings) {
            jx.dTablesUnblock($data_table);
        }

    }).on('order.dt', function () {
        jx.dTablesBlock($data_table);
    });
}