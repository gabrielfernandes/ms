﻿//#region GRID CONTRATO
var $data_table_acao = $('#acao-cobranca');
jx.dtableSearch($data_table_acao);
function acaoCobrancaGrid() {
    //_tbAcao.destroy();
    _tbAcao = $('#acao-cobranca').DataTable({
        "rowId": 'codCliente',
        "order": [[10, 'desc']],
        "deferRender": true,
        "serverSide": true,
        "processing": true,
        "ajax": {
            "url": "/Atendimento/AcaoCobrancaDados/",
            "data": function (d) {
                d.clienteId = $("#CodCliente").val();
            },
            "type": "POST"
        },
        "columnDefs": [{
            targets: 0,
            searchable: false,
            orderable: false,
            data: 'Status',
            render: function (data, type, row) {
                if (row.Status == "Finalizado") {
                    return '<label class="label pull-left label-success" style="margin-left:4px; font-size:100%;">' + row.Status + '</label>'
                } else {
                    return '<label class="label pull-left label-warning" style="margin-left:4px; font-size:100%;">' + row.Status + '</label>'
                }
            }
        }],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td:eq(9),td:eq(10)', nRow).addClass("text-right");
            $('td:eq(0),td:eq(1),td:eq(2),td:eq(3),td:eq(4),td:eq(5),td:eq(6),td:eq(7),td:eq(12),td:eq(13)', nRow).addClass("text-center");
        },
        'initComplete': function (settings, json) {
            var tb = _tbAcao.data();
            tb.inicializar($data_table_acao);
            tb.addSearch($data_table_acao);
        },
        "fnDrawCallback": function (oSettings) {
            $data_table_acao.unblock();
            $data_table_acao.removeClass('reloading');
        },
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "buttons": {
                "selectAll": "Marcar Todos",
                "selectNone": "Desmarcar Todos"
            },
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            oAria: {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        "dom": '<"row"<"col-xs-12 col-md-6"l>>' +
        '<"row"<"col-xs-12 col-md-12"t>>' + 
        '<"row"<"col-xs-12 col-md-6"i><"col-xs-12 col-md-6"p>>',
    })
}