﻿//#region eventos click no botao editar Cliente
$('body').on('click', '.widget-panel-sortable > .widget-panel .handle > div.jnxwidget-ctrls > a[data-rel="edit"]', function (ev) {
    ev.preventDefault();

    var $this = $(this).closest('.panel');
    $this.block({
        message: '',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',
            width: '50%'
        },
        overlayCSS: { backgroundColor: '#FFF' }
    });
    $this.addClass('reloading');
    openCliente();
    setTimeout(function () {
        $this.unblock();
        $this.removeClass('reloading');
    }, 900);
});
//#endregion eventos click no botao editar Cliente

function openCliente() {
    var _clienteId = $("#CodCliente").val();
    $("#modal-form-historico .modal-title").empty().text("Cadastro Cliente");
    $("#modal-form-historico .modal-body").empty();
    $("#modal-form-historico .modal-footer").hide();


    var url = "/Pesquisa/AlterarCliente/?idCliente=" + _clienteId;
    //#region modal Cliente
    $("#modal-form-historico .modal-body").append($("<div>")).load(url, function () {

        $("#modal-form-historico").find(".panel-body.fadeIn").css("max-height", "400px").css("overflow-x", "hidden").css("overflow-y", "scroll");


        $("#modal-form-historico").modal("show");

        //#region criar elemento com botoes no topo
        var $divButtons = $("#modal-form-historico .modal-body").find("#rowButtons");
        var header = '<header class="bs-docs-nav navbar navbar-static-top" style="background-color:#E8E8E8; margin: -16px -15px 7px -15px; height:50px !important; border-top: 1px solid" id="top"><div class="row" style="margin:10px;"><div class="col-md-2 pull-left"><button type="button" class="btn btn-block btn-default" data-dismiss="modal" style="margin-left:-10px"><i class="fa fa-times"></i> Cancelar</button></div><div class="col-md-2 pull-left"><button type="button" data-btn="salvarContato" class="btn btn-block btn-primary" style="margin-left:-10px"><i class="fa fa-save"></i> Salvar</button></div><div class="col-md-2" style="border-left: 1px solid #CCC"><button type="button" class="btn btn-block btn-info" data-btn="NovoContato" style="display: inline-block;"><i class="fa fa-plus"></i> Novo</button></div></div></header>';
        $divButtons.append(header);
        $divButtons.removeClass("hide");
        //#endregion comentarios
        $("#btnNovoContato").hide();
        $('[data-btn="NovoContato"]').hide();
        //#region evento de trocar de abas no cliente
        $("#modal-form-historico").find('.nav-tabs a').on('shown.bs.tab', function (e) {
            $('[data-btn="NovoContato"]').hide();
            var tb = e.target.hash.replace("#tab-", "");
            if (tb == "contatos") {
                $('[data-btn="NovoContato"]').show();
            }
        })
        //#endregion comentarios

        //#region criar novo contato
        $('[data-btn="NovoContato"]').click(function () {
            var countDiv = $(".addFormContato > div").length + 1;
            $(".addFormContato").append("<div class='append_" + countDiv + "'></div>");
            $('.append_' + countDiv).load('/Pesquisa/AdicionarContatoForm/?idCliente=' + $("#CodCliente").val());
            $('.append_' + countDiv).focus();
        });
        $('[data-btn="salvarContato"]').click(function () {
            var form = $("#form-cadastro-cliente-contato");
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: jx.formRotinas.obterDadosForm($("#form-cadastro-cliente-contato")),
                success: function (response) {
                    jx.mensagem('Operação Realizada!', 'Contato Salvo.', 'success', 'fa fa-check');
                    $('#modal-form-historico').modal('toggle');
                    setTimeout(function () {
                        location.reload();
                    }, 1500);

                }
            });
        });
        //#endregion comentario

        //#region remover contato
        $(".removeClone").click(function () {
            var contId = $(this).attr('data-contrato');
            var panel = $(this).parent().closest("div[data-elem-clone='contato']");
            if (contId > 0) {
                $.ajax({
                    type: "POST",
                    url: '/Cliente/ExcluirContato/',
                    data: { id: contId },
                    success: function (response) {
                        jx.mensagem('Operação Realizada!', 'Contato Excluido.', 'success', 'fa fa-check');
                        panel.addClass('fadeOut');
                        setTimeout(function () {
                            panel.remove();
                        }, 500);
                    }
                });
            } else {
                panel.addClass('fadeOut');
                setTimeout(function () {
                    panel.remove();
                }, 500);
            }
        });
        //#endregion comentarios
    });

    //#endregion modal Cliente
    $("#modal-form-historico").modal("show");
}


