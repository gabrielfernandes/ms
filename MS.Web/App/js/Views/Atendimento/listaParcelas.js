﻿var $data_table_parcela = $('#tbParcelas')
jx.dtableSearch($data_table_parcela);
jx.dtableFooter($data_table_parcela);
var _tb = $('#tbParcelas').DataTable({
    "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        paging: false,
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",

        "buttons": {
            "selectAll": "Marcar Todos",
            "selectNone": "Desmarcar Todos"
        },
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        oAria: {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    "dom": '<"row"<"col-xs-12 col-md-12 dtable-reponsive"t>>'
});

function parcelaGrid(tp) {
    if ($("#filtContrato").val() != '' && $("#filtContrato").val() != undefined) {
        var filter = {
            contratoId: $("#filtContrato").val(),
            CodSinteseContrato: $("#sinteseContratoId").val(),
            CodSinteseParcela: $("#sinteseParcelaId").val(),
            tipo: parseInt($("#parcelaTab").find("li.active>a").attr('data-tipo'))
        }
        if (tp > 0) {
            filter.tipo = parseInt(tp);
        }
        _tb.destroy();
        _tb = $('#tbParcelas').DataTable({
            "columnDefs": [{
                targets: 0,
                searchable: false,
                orderable: false,
                data: 'ValorParcela',
                className: 'td-checkbox',
                scrollCollapse: true,
                render: function (data, type, row) {
                    if (row.IsPago) {
                        return ''
                    } else {
                        if (row.IsRegionalUsuario) {
                            return '<div class="checkbox checkbox-inline checkbox-default" style="margin-bottom:4px;font-size:14px;  padding-left: 0px; padding-right: 0px; margin:0; margin-top:2px;"><input data-tipo="parcela" style="left: 2px; width: 17px; height: 16px; margin-top:2px" data-vr="' + row.ValorParcela + '" data-id="' + row.CodParcela + '" data-vencimento="' + row.DataVencimento + '" type="checkbox"><label></label></div>'
                        } else {
                            return ''
                        }

                    }
                }
            },
            {
                targets: 29,
                searchable: false,
                orderable: false,
                className: '',
                render: function (data, type, row) {
                    return '<button onclick="listHistParc(' + row.CodParcela + ')" type="button" class="btn btn-xs btn-block btn-default" style=""><i class="fa fa-history"></i></button>'
                }
            }],
            "rowId": 'codParcela',
            "order": [[1, 'desc']],
            "deferRender": true,
            "serverSide": true,
            "processing": true,
            "ajax": {
                "url": "/Atendimento/ParcelaDados/",
                "data": filter,
                "type": "POST"
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $('td:eq(29)', nRow).css('padding', '1');
                    $('td:eq(13),td:eq(18),td:eq(21),td:eq(22),td:eq(25),td:eq(28)', nRow).addClass("text-left");
                    $('td:eq(1),td:eq(2),td:eq(3),td:eq(4),td:eq(5),td:eq(6),td:eq(7),td:eq(8),td:eq(9),td:eq(10),td:eq(11),td:eq(12),td:eq(14),td:eq(15),td:eq(16),td:eq(17),td:eq(19),td:eq(20),td:eq(23),td:eq(24),td:eq(27)', nRow).addClass("text-center");
            },
            "buttons": [{
                text: '<i class="fa fa-cloud-download"></i>',
                extend: 'csvHtml5',
                className: 'dt-button-excel',
                title: 'teste',
                exportOptions: {
                    columns: ':visible'
                },
                action: function (e, dt, node, config) {
                    $data_table.block({
                        message: '',
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#fff',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff',
                            width: '50%'
                        },
                        overlayCSS: { backgroundColor: '#FFF' }
                    });
                    $data_table.addClass('carregando');
                    var params = { param: dt.ajax.params(), contratoId: dt.ajax.params().contratoId, CodSinteseContrato: dt.ajax.params().CodSinteseContrato, CodSinteseParcela: dt.ajax.params().CodSinteseParcela, tipo : dt.ajax.params().tipo };
                    console.log(params);
                    $.post("/Atendimento/ExportarExcelParcela", $.param(params), function (d) {
                        if (d.OK) {
                            window.location.href = d.Dados;
                            //var opnner = window.open(d.Dados, '_blank');
                        }
                    });
                    dt.ajax.reload();
                }
            }],
            'initComplete': function (settings, json) {
                var tb = _tb.data();
                tb.inicializar($data_table_parcela);
                tb.addSearch($data_table_parcela);
                tb.sum();

                $data_table_parcela.removeClass('carregando');
            },
            "fnDrawCallback": function (oSettings) {
                _tb.data().sum();
                $data_table_parcela.unblock();

                $data_table_parcela.removeClass('carregando');
                $data_table_parcela.removeClass('reloading');
            },
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "buttons": {
                    "selectAll": "Marcar Todos",
                    "selectNone": "Desmarcar Todos"
                },
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                oAria: {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "dom": '<"row"<"col-xs-12 col-md-12"Bl>>' +
                '<"row"<"col-xs-12 col-md-12 dtable-reponsive"t>>' +
                '<"row"<"col-xs-12 col-md-6 pull-left"i><"col-xs-12 col-md-6 pull-right"p>>'
        });
    }

}

function listHistParc(codParcela) {
    $("#modal-form-historico .modal-body").empty();
    $("#modal-form-historico .modal-title").empty().text("Histórico");
    $("#modal-form-historico .modal-body").load("/Historico/getList/?idCliente=0&idContrato=0&idParcela=" + codParcela + "&tpHistorico=3" + " #form-list-historico", function () {
        $(".modal-footer").hide()
        $("#modal-form-historico").modal("show");

        abrirListaHistoricos(codParcela);
    });

};

function abrirListaHistoricos(id) {
    var $data_table_hist = $('#tbHistorico');
    var _table = $('#tbHistorico').DataTable({
        "rowId": 'Cod',
        "deferRender": true,
        "serverSide": true,
        "processing": true,
        paging: false,
        "ajax": {
            "url": "/Historico/GetHistoricoParcela/",
            "data": function (d) {
                d.idParcela = id;
            },
            "type": "POST"
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td:eq(1),td:eq(2),td:eq(3),td:eq(4),td:eq(5),td:eq(6),td:eq(7)', nRow).addClass("text-center");
        },
        'initComplete': function (settings, json) {
            var tb = _table.data();
            tb.inicializar($data_table_hist);
            tb.addSearch($data_table_hist);

        },
        "fnDrawCallback": function (oSettings) {
            _table.data().sum();
            $data_table_hist.unblock();
            $data_table_hist.removeClass('carregando');
        },
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "buttons": {
                "selectAll": "Marcar Todos",
                "selectNone": "Desmarcar Todos"
            },
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            oAria: {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        "dom": '<"row"<"col-xs-12 col-md-12 dtable-reponsive"t>>' +
            '<"row"<"col-xs-12 col-md-12 pull-left"i>>'
    });
    table.draw();
}
