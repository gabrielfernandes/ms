﻿var $data_table_contrato = $('#contratos');
jx.dtableSearch($data_table_contrato);
jx.dtableFooter($data_table_contrato);;
var _tbContrato = $('#contratos').DataTable({
    "language": {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "buttons": {
            "selectAll": "Marcar Todos",
            "selectNone": "Desmarcar Todos"
        },
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        oAria: {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    "dom": '<"row"<"col-xs-12 col-md-12"t>>',
}).on('processing.dt', function (e, settings, processing) {
    if (processing) {
        $("#tb-dados").find("tr").addClass("loading");
    } else {
        $("#tb-dados").find("tr").removeClass("loading");
    }
});
function contratoGrid() {
    _tbContrato.destroy();
    _tbContrato = $('#contratos').DataTable({
        "rowId": 'codContrato',
        "order": [[1, 'desc']],
        "deferRender": true,
        "serverSide": true,
        "processing": true,
        "paging": false,
        "sScrollY": "250px",
        "bCollapse": true,
        "ajax": {
            "url": "/Atendimento/ContratoDados/",
            "data": function (d) {
                d.clienteId = $("#CodCliente").val();
            },
            "type": "POST"
        },
        "columnDefs": [{
            targets: 0,
            searchable: false,
            orderable: false,
            data: 'ContratoId',
            className: 'td-checkbox',
            render: function (data, type, row) {
                return '<div class="checkbox checkbox-inline checkbox-default" style="margin-bottom:4px;font-size:14px;  padding-left: 0px; padding-right: 0px; margin:0; margin-top:2px;"><input data-tipo="contrato" style="left: 2px; width: 17px; height: 16px; margin-top:2px;" data-id="' + row.CodContrato + '" type="checkbox"><label></label></div>'
            }
        }],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td:eq(6),td:eq(7),td:eq(8)', nRow).addClass("text-right text-bold");
            $('td:eq(1),td:eq(4)', nRow).addClass("text-center text-bold");
            $('td:eq(2),td:eq(3),td:eq(5),td:eq(9)', nRow).addClass("text-center");
        },
        "buttons": [jx.dtableButtons($('#contratos'), _tbContrato, ['excel', 'filter', 'length'])],
        'initComplete': function (settings, json) {
            var tb = _tbContrato.data();
            tb.inicializar($data_table_contrato);
            tb.addSearch($data_table_contrato);
            $data_table_contrato.parent().parent().find("input[type='checkbox']").click(function (event) {
                if ($data_table_contrato.DataTable().$("input[type='checkbox']:checked", { "page": "all" }).length > 0) {
                    FiltrarContrato();
                } else {
                    $("#filtContrato").val(0);
                    parcelaGrid();
                }
            });
            tb.sum();
        },
        "fnDrawCallback": function (oSettings) {
            _tbContrato.data().sum();
            $data_table_contrato.unblock();
            $data_table_contrato.removeClass('reloading');
        },
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "buttons": {
                "selectAll": "Marcar Todos",
                "selectNone": "Desmarcar Todos"
            },
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            oAria: {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        "dom": '<"row"<"col-xs-12 col-md-12"B>>' +
            '<"row"<"col-xs-12 col-md-12"t>>',
    })
}
function FiltrarContrato() {
    var rowcollection = _tbContrato.$("input[type='checkbox']:checked", { "page": "all" });
    var cod = 0;
    var dados = [];
    rowcollection.each(function (index, elem) {
        cod = $(elem).attr("data-id");
        dados.push(cod);
    });
    $("#filtContrato").val(dados);
    $.ajax({
        url: '/Atendimento/VerificaRegionalContrato/',
        type: 'post',
        data: { id: dados }
    }).success(function (resultado) {
        _contratoRegional = resultado.OK;
        if (_contratoRegional) {
            $('#btn-historico').prop("disabled", false);
        } else {
            $('#btn-historico').prop("disabled", true);
        }

    });
    $('#btn-mensagem').prop("disabled", false);
    consultarMsgContrato(dados);
    parcelaGrid();
}
function DownloadExcel($dTables, e, dt, node, config) {
    $dTables.block({
        message: '',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#fff',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',
            width: '50%'
        },
        overlayCSS: { backgroundColor: '#FFF' }
    });
    $dTables.addClass('carregando');
    window.location.href = "/Atendimento/DownloadExcelContrato/?ClienteId=" + $("#CodCliente").val();
    dt.ajax.reload();
}
