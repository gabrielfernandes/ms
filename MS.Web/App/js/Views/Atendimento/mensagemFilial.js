﻿function popUpMensagem() {
    var cod = $("#filtContrato").val();
    mensagemFilial(cod);
}

function mensagemFilial(codContrato) {
    $("#modal-form-historico .modal-body").empty();
    $("#modal-form-historico .modal-title").empty().text("Mensagem");
    $("#modal-form-historico .modal-body").load("/Atendimento/getMensagem/?idContrato=" + codContrato + " #form-envio-mensagem", function () {
        $(".modal-footer").hide();
        $('.editor-html').summernote({ height: '250px' });
        $("#modal-form-historico").modal("show");
        $("#btn-submit").off("click").click(function () { $("#form-cadastro-historico").submit() });

    });
};
function obterParametrosMensagem() {
    return {
        CodContrato: $("#filtContrato").val(),
        Assunto: $("#modal-form-historico #Assunto").val(),
        Mensagem: $("#modal-form-historico #Mensagem").val(),
        EmailDestinario: $("#modal-form-historico #EmailDestinatario").val(),
        NomeDestinario: $("#modal-form-historico #Responsavel").val()
    }
}


function EnviarMensagemPorEmail() {
    var obj = obterParametrosMensagem();
    $.ajax({
        url: '/Atendimento/EnviarMensagemEmail/',
        type: 'post',
        dataType: 'json',
        data: obj
    }).success(function (r) {
        if (r.OK) {
            $('#modal-form-historico').modal('toggle');
            alert("E-mail enviado!");
        }
        else {
            $('#modal-form-historico').modal('toggle');
            alert(r.Mensagem);
        }
    });
}


function openCarta() {
    var _ticked = "&t=" + _today.getSeconds();
    var rowcollection = _tb.$("input[type='checkbox']:checked", { "page": "all" });
    var cod = 0;
    var dados = [], total = [];

    rowcollection.each(function (index, elem) {
        var data = moment($(elem).attr("data-vencimento"), 'DD/MM/YYYY').format();
        var Today = moment().format();
        total.push(index);
        cod = $(elem).attr("data-id");
        dados.push(cod);
      
    });
    $.post('/Atendimento/ValidarCarta/?Parcelas=' + dados + _ticked, function (r) {
        if (r.Success) {
            montarCarta(dados);
        } else {
            for (var i = 0; i < r.Data.length; i++) {
                jx.mensagem(r.Data[i].Property, r.Data[i].Message, 'warning', 'fa fa-exclamation ');
            }
        }
    });

}

function montarCarta(dados) {
    var idCliente = $("#CodCliente").val();
    $("#modal-form-solicitacao .modal-body").empty();
    $(".modal-footer").empty();
    $("#modal-form-solicitacao .modal-title").empty().text("Envio e Carta");
    $("#modal-form-solicitacao .modal-body").load("/Atendimento/EnvioCarta/?idCliente=" + idCliente + "&idParcela=" + dados + " #form-envio-boleto", function () {
        $(".modal-footer").hide()
        $("#modal-form-solicitacao").modal("show");
        $("#btn-submit").off("click").click(function () { $("#form-envio-boleto").submit() });

        $("#TipoEmail").selectpicker('refresh');
        $("#TipoEmail").on('changed.bs.select', function () {
            MontarModeloEmail(dados, $("#TipoEmail").val());
        });
        $('.editor-html').summernote({ height: '380px' });
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
    });
};