﻿function popUpHistorico() {
    var cod = $("#filtContrato").val();
    historicoContrato(cod);
}
function historicoContrato(codContrato) {
    $("#modal-form-historico .modal-body").empty();
    $("#modal-form-historico .modal-title").empty().text("Histórico Contrato");
    $("#modal-form-historico .modal-body").load("/Historico/getHistorico/?idCliente=0&idContrato=" + codContrato + "&idParcela=0&tpHistorico=1" + " #form-cadastro-historico", function () {
        //$("#form-cadastro-historico").find("#form-comandos").hide();
        $(".modal-footer").show()
        $("#modal-form-historico").modal("show");
        $("#form-cadastro-historico #CodCliente").val($("#CodCliente").val());
        $("#btn-submit").off("click").click(function () { $("#form-cadastro-historico").submit() });
        $("#CodFase").change(function () {
            var cod = $("#CodFase").val();
            $.ajax({
                url: '/Sintese/ListarSintesePorFase/',
                type: 'post',
                dataType: 'json',
                data: { fase: cod }
            }).success(function (resultado) {
                popularDropoDownList($("#CodSintese"), resultado);
            });
        });
        //DRAGGABLE MODAL ARRASTAR
        $('.modal-dialog').draggable({
            handle: ".modal-header"
        });
        $("input[data-formato]")
            .mask("00/00/0000")
            .focusout(function (event) {
                var target, phone, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                value = target.value.replace(/\D/g, '');
                element = $(target);
                element.unmask();
                if (value.length = 8) {
                    var DataAgenda = moment(value, 'DD/MM/YYYY').format();
                    var Today = moment().format();
                    $(this).removeClass("date-red animated shake");
                    if (!moment(DataAgenda).isAfter(Today)) {
                        $(this).addClass("date-red animated shake", "easeOutBounce");
                    }
                }
            });
    });
};
function historicoParcela(dados) {
    $("#modal-form-historico .modal-footer").empty();
    $("#modal-form-historico .modal-body").empty();
    $("#modal-form-historico .modal-title").empty().text("Histórico Parcela");
    $("#modal-form-historico .modal-body").load("/Historico/getHistorico/?idCliente=0&idContrato=0&idParcela=" + dados + "&tpHistorico=3" + " #form-cadastro-historico", function () {
        jx.carregando(true);
        //$("#form-cadastro-historico").find("#form-comandos").hide();
        $("#form-cadastro-historico #CodCliente").val($("#CodCliente").val());
        $("#modal-form-historico").find("#divfaseSintese").css("max-height", "400px").css("overflow-x", "hidden").css("overflow-y", "scroll");
        $("#modal-form-historico .modal-footer").append('<button type="button" class="btn btn-sm btn-modal-save" onclick="salvarHistorico()"><i class="fa fa-save"></i> Salvar</button>')
        $("#modal-form-historico .modal-footer").append('<button type="button" class="btn btn-sm btn-modal-cancel" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>')

        $(".modal-footer").show()
        $("#modal-form-historico").modal("show");
        $("#btn-submit").off("click").click(function () { $("#form-cadastro-historico").submit() });
        $("input[data-formato]")
            .mask("00/00/0000")
            .focusout(function (event) {
                var target, phone, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                value = target.value.replace(/\D/g, '');
                element = $(target);
                element.unmask();
                if (value.length = 8) {
                    var DataAgenda = moment(value, 'DD/MM/YYYY').format();
                    var Today = moment().format();
                    $(this).removeClass("date-red animated shake");
                    if (!moment(DataAgenda).isAfter(Today)) {
                        $(this).addClass("date-red animated shake", "easeOutBounce");
                    }
                }
            });
        $("#CodFase").change(function () {
            var cod = $("#CodFase").val();
            $.ajax({
                url: '/Sintese/ListarSintesePorFase/',
                type: 'post',
                dataType: 'json',
                data: { fase: cod }
            }).success(function (resultado) {
                popularDropoDownList($("#CodSintese"), resultado);
            });
        });

        jx.carregando(false);
    });
};


function salvarHistorico() {
    $("#CodSintese").val($("#hfCodSintese").val());
    $("#CodMotivoAtraso").val($("#hfCodMotivo").val());
    var formData = $('#form-cadastro-historico');
    var retornoVerificacaoDataAgenda = ValidaDataAgenda();
    if (retornoVerificacaoDataAgenda) {
        $.ajax({
            url: '/Historico/Salvar/',
            type: 'post',
            dataType: 'json',
            data: formData.serialize()
        }).success(function (resultado) {
            if (resultado.OK) {
                jx.mensagem('Operação Realizada!', 'Seu histórico foi registrado', 'success', 'fa fa-check');
                $('#modal-form-historico').modal('toggle');
                FiltrarContrato();
                finalizarFila();
            } else {
                jx.mensagem('Verificação!', resultado.Mensagem, 'warning', 'fa fa-warning');
            }
        });
    } else {
        jx.mensagem('Data de Agendamento!', 'Está data não é permitida', 'danger', 'fa fa-exclamation');
    }

}

function selecionaFase(value) {
    var cod = $("#hfCodFase").val();
    mudaCorFase(value, cod);
    $("#hfCodFase").val(value);
    $.ajax({
        url: '/Sintese/ListarSintesePorFaseAtendimento/',
        type: 'post',
        dataType: 'json',
        data: { fase: value }
    }).success(function (resultado) {
        alteraModal($("#sinteses"), resultado);
    })
}

function mudaCorFase(value, ultimaValue) {
    $("#fase-" + value).removeAttr("class", "btn btn-sm btn-success");
    $("#fase-" + value).attr("class", "btn btn-sm btn-warning");
    if (ultimaValue != value) {
        $("#fase-" + ultimaValue).removeAttr("class", "btn btn-sm btn-warning");
        $("#fase-" + ultimaValue).attr("class", "btn btn-sm btn-success");
    }
}
function mudaCorMotivo(a) {
    var ultValue = $("#hfCodMotivo").val();
    var cod = $(a).attr("data-id");
    $("#motivo-" + cod).removeAttr("class", "btn btn-sm btn-default");
    $("#motivo-" + cod).attr("class", "btn btn-sm btn-warning");
    if (ultValue != cod) {
        $("#motivo-" + ultValue).removeAttr("class", "btn btn-sm btn-warning");
        $("#motivo-" + ultValue).attr("class", "btn btn-sm btn-default");

    }
    $("#hfCodMotivo").val(cod);
}
function selecionarSintese(a) {
    var ultValue = $("#hfCodSintese").val();
    var cod = $(a).attr("data-id");
    mudaCorSintase(cod, ultValue);
    $("#hfCodSintese").val(cod);
}
function mudaCorSintase(value, ultimaValue) {
    $("#sintese-" + value).removeAttr("class", "btn btn-sm btn-success");
    $("#sintese-" + value).attr("class", "btn btn-sm btn-warning");
    if (ultimaValue != value) {
        $("#sintese-" + ultimaValue).removeAttr("class", "btn btn-sm btn-warning");
        $("#sintese-" + ultimaValue).attr("class", "btn btn-sm btn-success");
    }
}