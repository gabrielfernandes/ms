﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class TabelaAnaliseDocumentalController : BasicController<TabelaAnaliseDocumental>
    {
        public Repository<TabelaTipoDocumento> _dbTabelaTipoDocumento = new Repository<TabelaTipoDocumento>();
        ListasRepositorio _dbListas = new ListasRepositorio();
        public override void carregarCustomViewBags()
        {
            ViewBag.ListaTipoDocumento = _dbListas.TipoDocumento().OrderBy(x => x.Value);
            base.carregarCustomViewBags();
        }
    }
}