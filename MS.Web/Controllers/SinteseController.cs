﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class SinteseController : BasicController<TabelaSintese>
    {

        public Repository<TabelaTipoCobranca> _dbTipoCobranca = new Repository<TabelaTipoCobranca>();
        public Repository<TabelaFase> _dbFase = new Repository<TabelaFase>();
        public override void carregarCustomViewBags()
        {
            var listaTiposDeCobranca = _dbTipoCobranca.All();
            var listaFase = _dbFase.All();

            ViewBag.ListaTipoCobranca = from p in listaTiposDeCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);
            ViewBag.ListaFase = from p in listaFase.OrderBy(x => x.Fase).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.Fase), p.Cod);


            base.carregarCustomViewBags();
        }
        public override ActionResult Cadastro(TabelaSintese dados)
        {
            dados.Sintese = DbRotinas.Criptografar(dados.Sintese);
            return base.Cadastro(dados);
        }
        public override void Ok(TabelaSintese dados, HttpRequestBase Request)
        {
            ViewBag.RedirecionarUrl = Url.Action("Index", "TipoCobranca");
            base.Ok(dados, Request);
        }
        [HttpPost]
        public JsonResult ListarSintesePorFase(int fase = 0)
        {
            var lista = _repository.All().Where(x => x.CodFase == fase).ToList();
            foreach (var item in lista)
            {
                item.Sintese = Idioma.Traduzir(DbRotinas.Descriptografar(item.Sintese));
            }
            lista = lista.OrderBy(x => x.Sintese).ToList();
            lista.Insert(0, new TabelaSintese());
            var selectList = from p in lista select new { Cod = p.Cod, Text = p.Sintese };

            return Json(selectList);
        }
        [HttpPost]
        public JsonResult ListarSintesePorFaseAtendimento(int fase = 0)
        {
            var lista = _repository.All().Where(x => x.CodFase == fase).ToList();
            foreach (var item in lista)
            {
                item.Sintese = Idioma.Traduzir(DbRotinas.Descriptografar(item.Sintese));
            }
            lista = lista.OrderBy(x => x.Sintese).ToList();
            var selectList = from p in lista select new { Cod = p.Cod, Text = p.Sintese };

            return Json(selectList);
        }

        [Authorize]
        [HttpPost]
        public JsonResult SalvarSinteseBoleto(TabelaSintese dados)
        {
            try
            {
                dados.Sintese = DbRotinas.Criptografar(dados.Sintese);
                validacaoCustom(dados);
                carregarCustomViewBags();

                dados = _repository.CreateOrUpdate(dados);
                return Json(new { OK = true, Titulo = "", Mensagem = "Operacao realizada com sucesso" });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
    }
}