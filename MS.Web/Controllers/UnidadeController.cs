﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class UnidadeController : BasicController<TabelaUnidade>
    {
        [HttpPost]
        public JsonResult Pesquisar(int id = 0)
        {
            var lista = _repository.All().Where(x => x.CodBloco == id).ToList();
            var dados = from p in lista select new { Cod = p.Cod, Text = p.NumeroUnidade };
            return Json(dados);
        }
    }
}