﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class TabelaVendaFluxoController : BasicController<TabelaVendaFluxo>
    {
        private Repository<TabelaVenda> _dbTabelaVenda = new Repository<TabelaVenda>();
        private Repository<TabelaVendaFluxo> _dbTabelaVendaFluxo = new Repository<TabelaVendaFluxo>();
        private Repository<TabelaTipoParcela> _dbTipoParcela = new Repository<TabelaTipoParcela>();


        public override ActionResult Cadastro(int id = 0)
        {
            int tabelaId = Cobranca.Db.Rotinas.DbRotinas.ConverterParaInt(Request.QueryString["tabela"]);
            ViewBag.ListaFluxoPorTabela = _repository.All().Where(x => x.CodTabelaVenda == tabelaId).OrderBy(x => x.Ordem).ToList();
            return base.Cadastro(id);
        }
        public override ActionResult Cadastro(TabelaVendaFluxo dados)
        {
            ViewBag.ListaFluxoPorTabela = _repository.All().Where(x => x.CodTabelaVenda == dados.CodTabelaVenda).OrderBy(x => x.Ordem).ToList();
            ViewBag.RedirecionarUrl = Url.Action("Cadastro", "TabelaVendaFluxo", new { tabela = dados.CodTabelaVenda });
            return base.Cadastro(dados);
        }

        public override void carregarCustomViewBags()
        {


            var listaTabelaVenda = _dbTabelaVenda.All().ToList();
            var listaTipoParcela = _dbTipoParcela.All().ToList();
            listaTabelaVenda.Insert(0, new TabelaVenda());
            listaTipoParcela.Insert(0, new TabelaTipoParcela());
            ViewBag.ListaTabelaVenda = from p in listaTabelaVenda.OrderBy(x => x.Nome).ToList() select new DictionaryEntry(p.Nome, p.Cod);
            ViewBag.ListaTipoParcela = from p in listaTipoParcela.OrderBy(x => x.TipoParcela).ToList() select new DictionaryEntry(p.TipoParcela, p.Cod);

            base.carregarCustomViewBags();
        }

        public JsonResult AlteraFluxo(TabelaVendaFluxo dadosTabela)
        {
            //_dbTabelaVendaFluxo.FindById(dadosTabela.Cod);
            //Proposta item = new Db.Models.Proposta();
            var entity = _dbTabelaVendaFluxo.CreateOrUpdate(dadosTabela);

            try
            {
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

    }
}