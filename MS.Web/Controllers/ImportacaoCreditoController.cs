﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.IO;
using System.Web.UI;
using System.Data.OleDb;
using System.Data.SqlClient;
using Recebiveis.Web.Models;
using Cobranca.Rotinas;
using Excel;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Web.Controllers
{
    public class ImportacaoCreditoController : Controller
    {
        private Repository<Contrato>
        _dbContrato = new Repository<Contrato>();

        private Repository<Importacao>
        _dbImportacao = new Repository<Importacao>();

        public Dados _qry = new Dados();
        public const string _filtroSessionId = "FiltroPesquisaId";




        //string[] arquivo;
        List<string> arquivo = new List<string>();

        [HttpGet]
        public ActionResult Import()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ImportLista()
        {
            var processamentos = _qry.ListarImportacao((short)Enumeradores.TipoImportacao.Credito);
            return View(processamentos);
        }
        [HttpPost]
        public JsonResult LerArquivo(string[] file)
        {

            try
            {
                CarregarArquivoParaBanco(file);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        private FileInfo GetFile(string file)
        {
            string pathUpload = Server.MapPath("~/Content/files");
            string pathDefinitivo = Server.MapPath(string.Format("~/Content/files/{0}", file));
            return new FileInfo(pathDefinitivo);
        }


        public void CarregarArquivoParaBanco(string[] files)
        {
            foreach (var file in files)
            {
                FileInfo File = GetFile(file);
                var dt = ConvertTXTtoDataTable(File.Name, File.FullName);

                var colunasDePara = new List<ColunaDePara>();
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numero do documento", NomeColunaTabela = "NumeroDocumento" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "nr cliente", NomeColunaTabela = "NumeroCliente" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "descrição", NomeColunaTabela = "Descricao" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "data do credito", NomeColunaTabela = "DataCredito" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "valor do credito", NomeColunaTabela = "ValorCredito" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "obs", NomeColunaTabela = "Observacao" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "saldo do credito", NomeColunaTabela = "SaldoCredito" });

                ImportacaoDb.BulkCopy(dt, "ImportacaoCredito", colunasDePara);

            }
            var listaResultado = ImportacaoDb.ImportacaoCreditoObterStatus(WebRotinas.ObterUsuarioLogado().Cod, 0);
        }
        public static DataTable ConvertTXTtoDataTable(string strFileName, string strFilePath)
        {
            DataTable dt = new DataTable();
            Int32 c = 0;
            try
            {
                using (StreamReader sr = new StreamReader(strFilePath, System.Text.Encoding.Default, false, 512))
                {
                    string[] headers = new string[] { };
                    string linhaHader = sr.ReadLine();
                    if (linhaHader.Split('\t').Length <= 1)
                    {
                        headers = linhaHader.Split(new Char[] { ';' });
                    }
                    else
                    {
                        headers = linhaHader.Split(new Char[] { '\t' });
                    }
                    foreach (string header in headers)
                    {
                        if (dt.Columns.Count <= 18)
                        {
                            dt.Columns.Add(header.Trim());
                        }
                    }
                    var text = sr.ReadToEnd();
                    string[] stringSeparators = new string[] { "\r\n" };
                    var rows = text.Split(stringSeparators, StringSplitOptions.None);
                    foreach (var r in rows)
                    {
                        DataRow dr = dt.NewRow();

                        string linhatexto = r.ToString();
                        if (linhatexto == null || String.IsNullOrEmpty(linhatexto))
                        {
                            break;
                        }

                        string[] quebra = new string[] { };
                        if (linhatexto.Split('\t').Length <= 1)
                        {
                            quebra = linhatexto.Split(new Char[] { ';' });
                        }
                        else
                        {
                            quebra = linhatexto.Split(new Char[] { '\t' });
                        }
                        for (int i = 0; i < quebra.Count(); i++)
                        {
                            quebra[i] = quebra[i].Replace(";", "").Replace("\t", "").Replace("\"", "");
                            //VERIFICA OS CAMPOS PARA OFUSCAR OS DADOS ANTES DA IMPORTACAO
                            if (i == 1)
                            {
                                dr[i] = DbRotinas.Criptografar(quebra[i].Trim());
                            }
                            else
                            {
                                if (i == 3)
                                {
                                    dr[i] = DbRotinas.ConverterParaDatetimeOuNull(quebra[i].Trim());
                                }
                                else
                                {
                                    dr[i] = quebra[i].Trim();
                                }

                            }
                        }
                        c = c + 1;

                        dt.Rows.Add(dr);
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
       
        [HttpPost]
        public JsonResult Excluir(int id)
        {
            try
            {
                _dbImportacao.Delete(id);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        public FileResult ExtrairResultadoImportacaoCredito(int CodImportacao = 0, short Status = 0)
        {
            var lista = ImportacaoDb.ListarResultadoPorCredito(CodImportacao, Status);

            string pathFile = Path.Combine(Path.GetTempPath(), Path.GetTempFileName());
            using (TextWriter tw = System.IO.File.CreateText(pathFile))
            {
                tw.WriteLine(";numero do documento;nr cliente;descrição;data do credito;valor do credito;obs;Resultado");
                foreach (var item in lista)
                {
                    tw.WriteLine(string.Format("{0};{1};{2};{3};{4};{5};{6};{7}",
                        item.Cod
                      , item.NumeroDocumento
                      , DbRotinas.Descriptografar(item.NumeroCliente)
                      , item.Descricao
                      , item.DataCredito
                      , item.ValorCredito
                      , item.Observacao
                      , item.Resultado
                    ));
                }
                tw.Close();
            }

            return File(pathFile, "text/csv", "resultado.csv");

        }

        public FileResult LayoutCredito(int CodImportacao = 0, short Status = 0)
        {


            StringBuilder sb = new StringBuilder();
            sb.AppendLine("numero do documento;nr cliente;descrição;data do credito;valor do credito;");

            return File(new System.Text.UTF8Encoding().GetBytes(sb.ToString()), "text/csv", "layout-cliente.csv");

        }

        [HttpPost]
        public JsonResult Resultado(int CodImportacao, short Status)
        {
            var lista = ImportacaoDb.ListarResultadoPorCliente(CodImportacao, Status);
            return Json(lista);
        }

        [HttpPost]
        public async Task<JsonResult> Processar(int CodImportacao)
        {
            ImportacaoDb.ProcessarCredito(CodImportacao);
            return Json(new { OK = true });
        }
    }

}
