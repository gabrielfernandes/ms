﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.IO;
using System.Web.UI;
using System.Data.OleDb;
using System.Data.SqlClient;
using Recebiveis.Web.Models;
using Cobranca.Rotinas;
using Excel;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Cobranca.Web.Controllers
{
    public class ImportacaoClienteEnderecoController : Controller
    {
        private Repository<Contrato>
        _dbContrato = new Repository<Contrato>();

        private Repository<Importacao>
        _dbImportacao = new Repository<Importacao>();

        public Dados _qry = new Dados();
        public const string _filtroSessionId = "FiltroPesquisaId";




        //string[] arquivo;
        List<string> arquivo = new List<string>();

        [HttpGet]
        public ActionResult Import()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ImportLista()
        {
            var processamentos = _dbImportacao.All().Where(x => x.Tipo == (short)Enumeradores.TipoImportacao.ClienteEndereco).ToList();
            return View(processamentos);
        }
        [HttpPost]
        public JsonResult LerArquivo(string[] file)
        {
            try
            {
                CarregarArquivoParaBanco(file);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        private FileInfo GetFile(string file)
        {
            string pathUpload = Server.MapPath("~/Content/files");
            string pathDefinitivo = Server.MapPath(string.Format("~/Content/files/{0}", file));
            return new FileInfo(pathDefinitivo);
        }


        public void CarregarArquivoParaBanco(string[] files)
        {
            foreach (var file in files)
            {
                FileInfo File = GetFile(file);
                var dt = ConvertTXTtoDataTable(File.Name, File.FullName);

                var colunasDePara = new List<ColunaDePara>();
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Contrato", NomeColunaTabela = "Contrato" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "enderecocob", NomeColunaTabela = "EnderecoCob" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numerocob", NomeColunaTabela = "NumeroCob" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "complementocob", NomeColunaTabela = "ComplementoCob" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "bairrocob", NomeColunaTabela = "BairroCob" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cepcob", NomeColunaTabela = "CEPCob" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cidadecob", NomeColunaTabela = "CidadeCob" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "estadocob", NomeColunaTabela = "EstadoCob" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "enderecoobra", NomeColunaTabela = "EnderecoObra" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numeroobra", NomeColunaTabela = "NumeroObra" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "complementoobra", NomeColunaTabela = "ComplementoObra" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "bairroobra", NomeColunaTabela = "BairroObra" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cepobra", NomeColunaTabela = "CepObra" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cidadeobra", NomeColunaTabela = "CidadeObra" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "estadoobra", NomeColunaTabela = "EstadoObra" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "mensagemcontrato", NomeColunaTabela = "MensagemContrato" });
                ImportacaoDb.BulkCopy(dt, "ImportacaoClienteEndereco", colunasDePara);
            }
            int codUsuarioLogado = WebRotinas.ObterUsuarioLogado().Cod;
            var op = ImportacaoDb.ImportacaoClienteEnderecoObterStatus(codUsuarioLogado, 0, String.Join("/", files));
            if (op.OK)
            {
                int CodImportacao = DbRotinas.ConverterParaInt(op.Dados);
                new Thread(delegate ()
                {
                    ImportacaoDb.ProcessarClienteEndereco(CodImportacao);
                }).Start();
            }
        }
        public static DataTable ConvertTXTtoDataTable(string strFileName, string strFilePath)
        {
            DataTable dt = new DataTable();
            Int32 c = 0;
            try
            {
                using (StreamReader sr = new StreamReader(strFilePath, System.Text.Encoding.Default, false, 512))
                {
                    string[] headers = sr.ReadLine().Split(new Char[] { '\t' });

                    foreach (string header in headers)
                    {
                        if (dt.Columns.Count <= 17)
                        {
                            dt.Columns.Add(header.Trim());
                        }
                    }

                    string[] rows = sr.ReadLine().Split('\t');

                    while (true)
                    {
                        DataRow dr = dt.NewRow();

                        string linhatexto = sr.ReadLine();
                        if (linhatexto == null)
                        {
                            break;
                        }
                        string[] quebra = linhatexto.Split(new Char[] { '\t' });
                        for (int i = 0; i < quebra.Count(); i++)
                        {
                            quebra[i] = quebra[i].Replace(";", "");
                            //VERIFICA OS CAMPOS PARA OFUSCAR OS DADOS ANTES DA IMPORTACAO
                            if (i == 0)
                            {
                                dr[i] = DbRotinas.Criptografar(quebra[i].Trim());
                            }
                            else
                            {
                                dr[i] = quebra[i].Trim();
                            }

                        }
                        c = c + 1;

                        dt.Rows.Add(dr);
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public static DataTable RemoveLinhasDuplicadasRows(DataTable dTable, List<string> colunas)
        {
            Hashtable hTable = new Hashtable();
            ArrayList listDuplicada = new ArrayList();
            DataColumn Col = dTable.Columns.Add("Duplicado", Type.GetType("System.Boolean"));
            Col.SetOrdinal(14);// to put the column in position 0;

            foreach (DataRow drow in dTable.Rows)
            {
                var texto = string.Empty;
                foreach (var c in colunas)
                {
                    texto = texto + (string)drow[c];
                }

                if (hTable.Contains(texto))
                {
                    //listDuplicada.Add(drow);  ADICIONA NA LISTA DE DUPLICADOS
                    drow[14] = true;
                }
                else
                {
                    hTable.Add(texto, string.Empty);
                    drow[14] = false;
                }
            }


            ////Remove da lista datatable os itens duplicados.
            //foreach (DataRow dRow in listDuplicada)
            //    dTable.Rows.Remove(dRow);

            //Datatable which contains unique records will be return as output.
            return dTable;
        }
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(strFilePath, System.Text.Encoding.Default, false, 512))
            {
                string[] headers = sr.ReadLine().Split(new Char[] { '\t' });

                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(';');
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        [HttpPost]
        public JsonResult Excluir(int id)
        {
            try
            {
                _dbImportacao.Delete(id);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        public FileResult ExtrairResultadoImportacaoCliente(int CodImportacao = 0, short Status = 0)
        {
            var lista = ImportacaoDb.ListarResultadoPorClienteEndereco(CodImportacao, Status);

            string pathFile = Path.Combine(Path.GetTempPath(), Path.GetTempFileName());
            using (TextWriter tw = System.IO.File.CreateText(pathFile))
            {
                tw.WriteLine("Cod;Contrato;enderecocob;numerocob;complementocob;bairrocob;cepcob;cidadecob;estadocob;enderecoobra;numeroobra;complementoobra;bairroobra;cepobra;cidadeobra;estadoobra;Resultado");
                foreach (var item in lista)
                {
                    tw.WriteLine(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15}",
                        item.Cod
                        , item.Contrato
                        , item.EnderecoCob
                        , item.NumeroCob
                        , item.ComplementoCob
                        , item.BairroCob
                        , item.CEPCob
                        , item.CidadeCob
                        , item.EstadoCob
                        , item.EnderecoObra
                        , item.NumeroObra
                        , item.ComplementoObra
                        , item.BairroObra
                        , item.CepObra
                        , item.CidadeObra
                        , item.EstadoObra
                        , item.Mensagem
                    ));
                }
                tw.Close();
            }

            return File(pathFile, "text/csv", "resultado.csv");

        }

        public FileResult LayoutCliente(int CodImportacao = 0, short Status = 0)
        {


            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Contrato;enderecocob;numerocob;complementocob;bairrocob;cepcob;cidadecob;estadocob;enderecoobra;numeroobra;complementoobra;bairroobra;cepobra;cidadeobra;estadoobra;");

            return File(new System.Text.UTF8Encoding().GetBytes(sb.ToString()), "text/csv", "layout-cliente-endereco.csv");

        }

        [HttpPost]
        public JsonResult Resultado(int CodImportacao, short Status)
        {
            var lista = ImportacaoDb.ListarResultadoPorCliente(CodImportacao, Status);
            return Json(lista);
        }

        [HttpPost]
        public async Task<JsonResult> Processar(int CodImportacao)
        {
            ImportacaoDb.ProcessarCliente(CodImportacao);
            return Json(new { OK = true });
        }

        [HttpPost]
        public JsonResult ObterTotais(int[] IdImportacao)
        {

            try
            {
                var processamentos = _dbImportacao.All().Where(x => x.Tipo == (short)Enumeradores.TipoImportacao.ClienteEndereco).ToList();
                var objRetorno = from r in processamentos
                                 select new
                                 {
                                     r.Cod,
                                     r.TotalRegistros,
                                     r.TotalRegistrosProcessado,
                                     r.TotalRegistrosErro,
                                     r.TotalRegistrosOK,
                                     r.Mensagem
                                 };

                return Json(new { OK = true, Dados = objRetorno.ToList() });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
    }

}
