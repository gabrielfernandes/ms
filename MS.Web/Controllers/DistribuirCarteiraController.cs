﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class DistribuirCarteiraController : Controller
    {
        public Repository<TabelaAging> _dbAging = new Repository<TabelaAging>();
        public Repository<TabelaTipoCobranca> _dbCluster = new Repository<TabelaTipoCobranca>();
        public Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();
        public Repository<TabelaEscritorio> _dbEscritorio = new Repository<TabelaEscritorio>();
        public Repository<TabelaEscritorio> _dbTabelaEscritorio = new Repository<TabelaEscritorio>();
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        [HttpGet]
        public ActionResult Index()
        {
            var listaEscritorio = _dbTabelaEscritorio.All().OrderBy(x => x.DataCadastro).ToList();
            ViewBag.ListaEscritorio = (from p in listaEscritorio select new DictionaryEntry(p.Escritorio, p.Cod)).ToList();

            return View();
        }
        [HttpGet]
        public ActionResult Pendentes()
        {
            var listaEscritorio = _dbTabelaEscritorio.All().OrderBy(x => x.DataCadastro).ToList();
            ViewBag.ListaEscritorio = (from p in listaEscritorio select new DictionaryEntry(p.Escritorio, p.Cod)).ToList();

            return View();
        }
        [HttpGet]
        public ActionResult SelecionarEscritorio()
        {
            var dados = _dbEscritorio.All().ToList();
            return View(dados);
        }
        [HttpGet]
        public ActionResult DistribuicaoPasso1()
        {
            var dados = _dbEscritorio.All();
            return View(dados.ToList());
        }

        [HttpPost]
        public ActionResult DistribuicaoPasso1(string ids)
        {
            var all = _dbEscritorio.All();

            var allContratos = _dbContrato.All();

            //totais
            ViewBag.TotalProcessos = allContratos.Count();
            ViewBag.TotalPendete = allContratos.Where(x => !x.CodEscritorio.HasValue).Count();
            ViewBag.TotalDistribuido = allContratos.Where(x => x.CodEscritorio.HasValue).Count();

            if (!string.IsNullOrEmpty(ids))
            {
                string[] valores = ids.Split(',');
                all = all.Where(x => ids.Contains(x.Cod.ToString()));
            }
            else
            {
                return View(new List<TabelaEscritorio>());
            }

            return View(all.ToList());
        }

        [HttpGet]
        public JsonResult CarregarTiposCobranca()
        {
            List<DictionaryEntry> dados = new List<DictionaryEntry>();
            if (WebRotinas.UsuarioAutenticado)
            {
                dados = (from p in _dbCluster.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList()
                         select new DictionaryEntry() { Key = p.Cod, Value = p.TipoCobranca }).ToList();
            }
            return Json(dados, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ConsultarQueryCondicao(int Cod, string Query)
        {
            try
            {
                Dados d = new Dados();
                var resultado = d.DistribuicaoExecutarQuery(Cod, Query);
                return Json(new
                {
                    OK = true,
                    Dados = resultado
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    OK = true,
                    Mensagem = ex.Message
                });
            }
        }
        [HttpPost]
        public JsonResult DistribuirPorEscritorio(int Cod, string sql)
        {
            try
            {
                Dados d = new Dados();
                var resultado = d.DistribuicaoProcessarPorEscritorio(Cod, sql);
                return Json(new
                {
                    OK = true,
                    Dados = resultado
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    OK = true,
                    Mensagem = ex.Message
                });
            }
        }
        
        [HttpPost]
        public JsonResult PreparBaseDistribuicao(int Cod)
        {
            try
            {
                Dados d = new Dados();
                d.DistribuicaoPrepararContratos(WebRotinas.ObterUsuarioLogado().Cod);
                return Json(new
                {
                    OK = true
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    OK = false,
                    Mensagem = ex.Message
                });
            }
        }
    }
}