﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class ParametroSinteseController : BasicController<TabelaParametroSintese>
    {
        Repository<TabelaTipoCobranca> _dbTipoCobranca = new Repository<TabelaTipoCobranca>();
        Repository<TabelaFase> _dbFase = new Repository<TabelaFase>();
        Repository<TabelaSintese> _dbSintese = new Repository<TabelaSintese>();
        Repository<ContratoParcelaBoletoHistorico> _dbBoletoHistorico = new Repository<ContratoParcelaBoletoHistorico>();

        [HttpGet]
        public ActionResult List()
        {

            ViewBag.ListaEtapaBoleto = EnumeradoresLista.EtapaBoleto();
            ViewBag.ListaFase = EnumeradoresLista.PreecherDropdownEmBranco();
            ViewBag.ListaSintese = EnumeradoresLista.PreecherDropdownEmBranco();

            var dados = _repository.FindAll(x => !x.DataExcluido.HasValue).ToList();
            return PartialView("_list", dados);
        }
        [HttpGet]
        public ActionResult ListDePara()
        {
            var dados = _repository.FindAll(x => !x.DataExcluido.HasValue && !x.Etapa.HasValue).ToList();
            ViewBag.ListaDePara = (from p in dados
                                   select new DictionaryEntry() { Key = p.Cod, Value = p.CodSinteseDe > 0 && p.CodSintesePara > 0 ? $"{DbRotinas.Descriptografar(p.TabelaSinteseDe.TabelaFase.TabelaTipoCobranca.TipoCobranca)} / {DbRotinas.Descriptografar(p.TabelaSinteseDe.TabelaFase.Fase)} / {DbRotinas.Descriptografar(p.TabelaSinteseDe.Sintese)} >> {DbRotinas.Descriptografar(p.TabelaSintesePara.TabelaFase.TabelaTipoCobranca.TipoCobranca)} / {DbRotinas.Descriptografar(p.TabelaSintesePara.TabelaFase.Fase)} / {DbRotinas.Descriptografar(p.TabelaSintesePara.Sintese)}" : "" }).ToList();
            ((List<DictionaryEntry>)ViewBag.ListaDePara).Insert(0, new DictionaryEntry() { Key = "0", Value = "--" });
            ((List<DictionaryEntry>)ViewBag.ListaDePara).Insert(1, new DictionaryEntry() { Key = "-1", Value = "Novo" });
            return PartialView("_listDePara", dados);
        }
        [HttpGet]
        public ActionResult FormParametro(int etapaId = 0)
        {
            int _codTipoCobranca = 0;
            int _codFase = 0;
            ViewBag.ListaEtapaBoleto = EnumeradoresLista.EtapaBoleto();
            ViewBag.ListaTipoCobranca = PreecherListaTipoCobranca();

            var dados = _repository.FindAll(x => !x.DataExcluido.HasValue && (short)x.Etapa == etapaId).FirstOrDefault();

            if (dados == null)
            {
                dados = Activator.CreateInstance<TabelaParametroSintese>();
                dados.DataCadastro = DateTime.Now;
            }
            else
            {
                _codTipoCobranca = dados.CodSintesePara > 0 ? dados.TabelaSintesePara.CodFase > 0 ? (int)dados.TabelaSintesePara.TabelaFase.CodTipoCobranca : 0 : 0;
                _codFase = dados.CodSintesePara > 0 ? (int)dados.TabelaSintesePara.CodFase : 0;
            }
            ViewBag.CodTipoCobranca = _codTipoCobranca;
            ViewBag.CodFase = _codFase;

            var fases = _dbFase.All().Where(x => x.CodTipoCobranca == _codTipoCobranca && x.TipoHistorico == (short)Enumeradores.TipoHistorico.Boleto).ToList();
            var sinteses = _dbSintese.All().Where(x => x.CodFase == _codFase).ToList();

            ViewBag.ListaFase = (from p in fases
                                 select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.Fase), Value = p.Cod }).ToList();
            ((List<DictionaryEntry>)ViewBag.ListaFase).Insert(0, new DictionaryEntry() { Key = "--", Value = "0" });

            ViewBag.ListaSintese = (from p in sinteses
                                    select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.Sintese), Value = p.Cod }).ToList();


            return PartialView("_form", dados);
        }
        [HttpGet]
        public ActionResult FormDePara(int id = 0)
        {
            int _codTipoCobrancaDe = 0;
            int _codTipoCobrancaPara = 0;
            int _codFaseDe = 0;
            int _codFasePara = 0;
            ViewBag.ListaTipoCobranca = PreecherListaTipoCobranca();

            var dados = _repository.FindById(id);

            if (dados == null)
            {
                dados = Activator.CreateInstance<TabelaParametroSintese>();
                dados.DataCadastro = DateTime.Now;
            }
            else
            {
                _codTipoCobrancaDe = dados.CodSinteseDe > 0 ? dados.TabelaSinteseDe.CodFase > 0 ? (int)dados.TabelaSinteseDe.TabelaFase.CodTipoCobranca : 0 : 0;
                _codFaseDe = dados.CodSinteseDe > 0 ? (int)dados.TabelaSinteseDe.CodFase : 0;

                _codTipoCobrancaPara = dados.CodSintesePara > 0 ? dados.TabelaSintesePara.CodFase > 0 ? (int)dados.TabelaSintesePara.TabelaFase.CodTipoCobranca : 0 : 0;
                _codFasePara = dados.CodSintesePara > 0 ? (int)dados.TabelaSintesePara.CodFase : 0;
            }

            var fasesDe = _dbFase.All().Where(x => x.CodTipoCobranca == _codTipoCobrancaDe && x.TipoHistorico == (short)Enumeradores.TipoHistorico.Boleto).ToList();
            var sintesesDe = _dbSintese.All().Where(x => x.CodFase == _codFaseDe).ToList();

            var fasesPara = _dbFase.All().Where(x => x.CodTipoCobranca == _codTipoCobrancaPara && x.TipoHistorico == (short)Enumeradores.TipoHistorico.Boleto).ToList();
            var sintesesPara = _dbSintese.All().Where(x => x.CodFase == _codFasePara).ToList();

            ViewBag.ListaFaseDe = (from p in fasesDe
                                   select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.Fase), Value = p.Cod }).ToList();
            ((List<DictionaryEntry>)ViewBag.ListaFaseDe).Insert(0, new DictionaryEntry() { Key = "--", Value = "0" });

            ViewBag.ListaFasePara = (from p in fasesPara
                                     select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.Fase), Value = p.Cod }).ToList();
            ((List<DictionaryEntry>)ViewBag.ListaFasePara).Insert(0, new DictionaryEntry() { Key = "--", Value = "0" });

            ViewBag.ListaSinteseDe = (from p in sintesesDe
                                      select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.Sintese), Value = p.Cod }).ToList();

            ViewBag.ListaSintesePara = (from p in sintesesPara
                                        select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.Sintese), Value = p.Cod }).ToList();


            return PartialView("_formDePara", dados);
        }
        [Authorize]
        [HttpPost]
        public JsonResult SalvarParametroBoleto(TabelaParametroSintese dados)
        {
            try
            {
                validacaoCustom(dados);
                carregarCustomViewBags();

                var usuario = WebRotinas.ObterUsuarioLogado();
                dados.CodUsuario = usuario.Cod;

                dados = _repository.CreateOrUpdate(dados);
                return Json(new { OK = true, Titulo = "", Mensagem = "Operacao realizada com sucesso" });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        private List<DictionaryEntry> PreecherListaTipoCobranca()
        {
            List<DictionaryEntry> dados = new List<DictionaryEntry>();

            var tipoCobranca = _dbTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.AnalistaBoleto || x.TipoFluxo == (short)Enumeradores.TipoFluxo.SupervisorBoleto || x.TipoFluxo == (short)Enumeradores.TipoFluxo.FilialBoleto).OrderBy(x => x.Cod).ToList();
            dados = (from p in tipoCobranca
                     select new DictionaryEntry() { Key = p.Cod, Value = DbRotinas.Descriptografar(p.TipoCobranca) }).ToList();

            dados.Insert(0, new DictionaryEntry() { Key = 0, Value = "--" });

            return dados;

        }
        [HttpPost]
        public JsonResult ExcluirDePara(int parametroId)
        {
            var retorno = new
            {
                Ok = true,
                Mensagem = ""
            };
            try
            {
                _repository.Delete(parametroId);
            }
            catch (Exception ex)
            {

                retorno = new
                {
                    Ok = false,
                    Mensagem = ex.Message
                };
            }


            return Json(retorno);
        }
    }
}