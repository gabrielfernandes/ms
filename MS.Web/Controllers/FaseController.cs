﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class FaseController : BasicController<TabelaFase>
    {

        public Repository<TabelaTipoCobranca> _dbTipoCobranca = new Repository<TabelaTipoCobranca>();
        public Repository<TabelaCampanha> _dbCampanha = new Repository<TabelaCampanha>();

        public override ActionResult Cadastro(TabelaFase dados)
        {
            dados.Fase = DbRotinas.Criptografar(dados.Fase);
            ViewBag.TipoHistorico = EnumeradoresLista.TipoHistorico();
            return base.Cadastro(dados);
        }
        public override void carregarCustomViewBags()
        {
            var list = _dbTipoCobranca.All().Where(x=> x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
            foreach (var item in list)
            {
                item.TipoCobranca = Idioma.Traduzir(DbRotinas.Descriptografar(item.TipoCobranca));
            }
            var listaTiposDeCobranca = list.AsQueryable();
            ViewBag.ListaTipoCobranca = from p in listaTiposDeCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(p.TipoCobranca, p.Cod);

            ViewBag.TipoHistorico = EnumeradoresLista.TipoHistorico();


            var listaCampanha = _dbCampanha.All();
            ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);


            base.carregarCustomViewBags();
        }
        public override void Ok(TabelaFase dados, HttpRequestBase Request)
        {
            ViewBag.RedirecionarUrl = Url.Action("Index", "TipoCobranca");
            base.Ok(dados, Request);
        }
        [HttpPost]
        public JsonResult ListarFasePorTipoCobranca(int CodTipoCobranca = 0)
        {
            var lista = _repository.All().Where(x => x.CodTipoCobranca == CodTipoCobranca).ToList();
            lista.Insert(0, new TabelaFase());
            var selectList = from p in lista select new { Cod = p.Cod, Text = Idioma.Traduzir(p.Fase) };

            return Json(selectList);
        }


        [Authorize]
        [HttpGet]
        public ActionResult CadastroFluxoBoleto(int id = 0)
        {
            var dados = _repository.FindById(id);
            if (id == 0)
            {
                dados = Activator.CreateInstance<TabelaFase>();
                dados.DataCadastro = DateTime.Now;
            }

            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoHistorico((short)Enumeradores.TipoHistorico.Boleto), Value = (short)Enumeradores.TipoHistorico.Boleto });
            ViewBag.TipoHistorico = lista;

            var list = _dbTipoCobranca.All().Where(x=> x.TipoFluxo == (short)Enumeradores.TipoFluxo.AnalistaBoleto || x.TipoFluxo == (short)Enumeradores.TipoFluxo.SupervisorBoleto || x.TipoFluxo == (short)Enumeradores.TipoFluxo.FilialBoleto).ToList();
            foreach (var item in list)
            {
                item.TipoCobranca = Idioma.Traduzir(DbRotinas.Descriptografar(item.TipoCobranca));
            }
            var listaTiposDeCobranca = list.AsQueryable();
            ViewBag.ListaTipoCobranca = from p in listaTiposDeCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(p.TipoCobranca, p.Cod);



            var listaCampanha = _dbCampanha.All();
            ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);
            
            return View("Cadastro", dados);
        }

        [Authorize]
        [HttpPost]
        public JsonResult SalvarFaseBoleto(TabelaFase dados)
        {
            try
            {
                bool fgSistemaCobranca = WebRotinas.ObterUsuarioLogado().SistemaCobranca == true;
                dados.Fase = DbRotinas.Criptografar(dados.Fase);
                validacaoCustom(dados);
                carregarCustomViewBags();

                dados = _repository.CreateOrUpdate(dados);
                return Json(new { OK = true, Titulo = "", Mensagem = "Operacao realizada com sucesso" });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

    }
}