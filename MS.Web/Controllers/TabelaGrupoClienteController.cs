﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class TabelaGrupoClienteController : BasicController<TabelaGrupoCliente>
    {
        public Repository<TabelaGrupo> _dbGrupo = new Repository<TabelaGrupo>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<TabelaGrupoCliente> _dbGrupoCliente = new Repository<TabelaGrupoCliente>();

        public override void carregarCustomViewBags()
        {
            var listaGrupo = _dbGrupo.All();
            ViewBag.ListaGrupo = from p in listaGrupo.OrderBy(x => x.Nome).ToList() select new DictionaryEntry(p.Nome, p.Cod);

            ViewBag.ListaGrupoClientes = _dbGrupoCliente.All().OrderByDescending(x => x.TabelaGrupo.Nome).ToList();
            base.carregarCustomViewBags();

            ViewBag.ListaCliente = _dbCliente.All().OrderBy(x => x.Nome).ToList();

        }

        public ActionResult Teste(TabelaGrupoCliente dados)
        {
            return base.Cadastro(dados);
        }

        public override void Ok(TabelaGrupoCliente dados, HttpRequestBase Request)
        {
            ViewBag.RedirecionarUrl = Url.Action("Cadastro");
            base.Ok(dados, Request);
        }

        [HttpPost]
        public JsonResult SalvarVinculo(int codGrupo, string[] cnpjs)
        {
            try
            {

                foreach (var item in cnpjs)
                {
                    if (!_dbGrupoCliente.All().Any(x => x.CNPJ == item.ToString()))
                    {
                        TabelaGrupoCliente dados = new TabelaGrupoCliente();

                        dados.CodGrupo = codGrupo;
                        dados.CNPJ = item.ToString();
                        _dbGrupoCliente.CreateOrUpdate(dados);
                    }
                    
                }
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        [HttpGet]
        public JsonResult PesquisarGrupos(string q)
        {
            var lista = _dbGrupo.All().Where(x => x.Nome.Contains(q)).Take(30).ToList();

            return Json(new
            {
                items = from p in lista select new { id = p.Cod, text = p.Nome }
            }, JsonRequestBehavior.AllowGet);
        }

    }
}