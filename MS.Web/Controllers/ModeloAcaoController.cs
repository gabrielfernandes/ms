﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class ModeloAcaoController : BasicController<TabelaModeloAcao>
    {
        public Repository<TabelaAcaoCobranca> _dbAcaoCobranca = new Repository<TabelaAcaoCobranca>();

        [ValidateInput(false)]
        public override ActionResult Cadastro(TabelaModeloAcao dados)
        {
            return base.Cadastro(dados);
        }

        public override ActionResult Cadastro(int id = 0)
        {
            return base.Cadastro(id);
        }

        public override void carregarCustomViewBags()
        {
            var listaAcaoCobranca = _dbAcaoCobranca.All();
            ViewBag.ListaAcaoCobranca = from p in listaAcaoCobranca.OrderBy(x => x.AcaoCobranca).ToList() select new DictionaryEntry(p.AcaoCobranca, p.Cod);
            base.carregarCustomViewBags();
        }


        [HttpGet]
        public JsonResult GetChave(int acao = 0)
        {
            var AcaoCobranca = _dbAcaoCobranca.FindById(acao);
            if ((Enumeradores.TipoAcaoCobranca)AcaoCobranca.Tipo == Enumeradores.TipoAcaoCobranca.EMAIL_SENHA_USUARIO_NOVO || (Enumeradores.TipoAcaoCobranca)AcaoCobranca.Tipo == Enumeradores.TipoAcaoCobranca.EMAIL_SENHA_USUARIO_REENVIAR)
            {
                return Json(new { OK = true });
            }
            else
            {
                return Json(new { OK = false });
            }
        }

    }
}