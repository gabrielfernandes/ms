﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Rotinas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Recebiveis.Web.Controllers
{
    public class ContratoController : Controller
    {
        Repository<Contrato> _dbContrato = new Repository<Contrato>();


        [HttpPost]
        public JsonResult AlterarEmpreendimento(int CodContrato, int CodEmpreendimento, int CodBloco, int CodEmpresa, int CodUnidade, DateTime DataChaves, int CodUsuarioDonoCarteira, bool DAF, bool Chaves)
        {
            try
            {
                var contrato = _dbContrato.FindById(CodContrato);

                contrato.CodUnidade = CodUnidade;
                contrato.CodEmpresa = CodEmpresa;
                contrato.DataChaves = DataChaves;
                contrato.CodUsuarioDonoCarteira = CodUsuarioDonoCarteira;
                contrato.DAF = DAF;
                contrato.Chaves = Chaves;

                _dbContrato.CreateOrUpdate(contrato);

                return Json(new { OK = true });

            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult AlterarNotificacao(int CodContrato, short? Notificacao)
        {
            try
            {
                var contrato = _dbContrato.FindById(CodContrato);

                contrato.NotificacaoTipo = Notificacao;
                contrato.CodUsuarioNotificacao = WebRotinas.ObterUsuarioLogado().Cod;
                contrato.DataNotificacao = DateTime.Now;

                _dbContrato.CreateOrUpdate(contrato);

                return Json(new { OK = true });

            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult AlterarTipoCobranca(int CodContrato, int CodTipoCobranca)
        {
            try
            {
                var contrato = _dbContrato.FindById(CodContrato);

                contrato.CodTipoCobranca = CodTipoCobranca;
                contrato.CodUsuarioTipoCobranca = WebRotinas.ObterUsuarioLogado().Cod;
                contrato.DataTipoCobranca = DateTime.Now;

                _dbContrato.CreateOrUpdate(contrato);

                return Json(new { OK = true });

            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult AlterarCondominio(int CodContrato, decimal? ValorIPTU, DateTime? DataIPTU, decimal? ValorCondominio, DateTime? DataCondominio)
        {
            try
            {
                var contrato = _dbContrato.FindById(CodContrato);

                contrato.ValorIPTU = ValorIPTU;
                contrato.DataIPTU = DataIPTU;
                contrato.ValorCondominio = ValorCondominio;
                contrato.DataCondominio = DataCondominio;

                //contrato.CodUsuarioC = WebRotinas.ObterUsuarioLogado().Cod;
                contrato.DataTipoCobranca = DateTime.Now;

                _dbContrato.CreateOrUpdate(contrato);

                return Json(new { OK = true });

            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
    }
}
