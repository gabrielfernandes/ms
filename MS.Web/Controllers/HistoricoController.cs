﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Threading.Tasks;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Rotinas;
using Cobranca.Db.Suporte;

namespace Cobranca.Web.Controllers
{
    public class HistoricoController : BasicController<Historico>
    {
        public const string _filtroCodCliente = "FiltroCodCliente";

        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<ContratoParcela> _dbParcela = new Repository<ContratoParcela>();
        public Repository<Historico> _dbHistorico = new Repository<Historico>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<TabelaFase> _dbFase = new Repository<TabelaFase>();
        public Repository<TabelaSintese> _dbSintese = new Repository<TabelaSintese>();
        public Repository<TabelaMotivoAtraso> _dbMotivoAtraso = new Repository<TabelaMotivoAtraso>();
        public Repository<TabelaResolucao> _dbResolucao = new Repository<TabelaResolucao>();
        ListasRepositorio _dbListas = new ListasRepositorio();
        public Repository<TabelaSintese> _TabelaSintese = new Repository<TabelaSintese>();
        public Repository<ContratoAcaoCobrancaHistorico> _dbContratoAcaoCobrancaHistorico = new Repository<ContratoAcaoCobrancaHistorico>();
        public Dados _qry = new Dados();
        public override void carregarCustomViewBags()
        {
            base.carregarCustomViewBags();
        }

        [Authorize]
        [HttpGet]
        public ActionResult getList(int idCliente = 0, int idContrato = 0, int idParcela = 0, short tpHistorico = 1)
        {
            var model = GetAllHistorico(idCliente, idContrato, idParcela, tpHistorico);
            return PartialView("list", model.ToList());
        }
        private List<HistoricoDados> GetAllHistorico(int idCliente = 0, int idContrato = 0, int idParcela = 0, short tpHistorico = 1)
        {
            if (idCliente == 0) { idCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }

            var dados = _qry.ListarHistoricoCliente(idCliente);

            if (tpHistorico == (short)Enumeradores.TipoHistorico.Cliente)
            {
                dados = dados.Where(x => !x.CodParcela.HasValue && !x.CodContrato.HasValue).ToList();
            }
            if (tpHistorico == (short)Enumeradores.TipoHistorico.Contrato)
            {
                dados = dados.Where(x => x.CodContrato > 0 && !x.CodParcela.HasValue && x.CodContrato == idContrato).ToList();
            }
            if (tpHistorico == (short)Enumeradores.TipoHistorico.Parcela)
            {
                dados = dados.Where(x => x.CodParcela > 0 && x.CodParcela == idParcela).ToList();
            }

            return dados;

        }

        public JsonResult GetHistoricoParcela(DTParameters param, int idParcela = 0)
        {
            try
            {
                var model = _dbHistorico.FindAll(x => !x.DataExcluido.HasValue && x.CodParcela == idParcela).ToList();
                var lista = (from q in model
                             select new
                             {
                                 Cod = q.Cod,
                                 Data = q.DataCadastro,
                                 DataDesc = $"{q.DataCadastro:dd/MM/yyyy HH:mm}",
                                 Fase = q.CodSintese.HasValue ? DbRotinas.Descriptografar(q.TabelaSintese.TabelaFase.Fase) : "",
                                 Sintese = q.CodSintese.HasValue ? q.TabelaSintese.CodigoSinteseCliente + " - " + DbRotinas.Descriptografar(q.TabelaSintese.Sintese) : "",
                                 Motivo = q.CodMotivoAtraso,
                                 Usuario = q.CodUsuario.HasValue ? q.Usuario.Nome : "",
                                 Observacao = DbRotinas.Descriptografar(q.Observacao),
                                 DataAgenda = q.DataAgenda,
                                 DataAgendaDesc = q.DataAgenda.HasValue ? $"{q.DataAgenda:dd/MM/yyyy}" : ""
                             }).AsQueryable();
                var result = WebRotinas.DTRotinas.DTResult(param, lista);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    error = ex.Message
                });
            }
        }

        [HttpGet]
        public async Task<ActionResult> getHistorico(int idCliente = 0, int idContrato = 0, string idParcela = null, short tpHistorico = 1)
        {
            var model = await this.GetFullAndPartial(idCliente, idContrato, idParcela, tpHistorico);
            return PartialView("modal", model);
        }
        private async Task<HistoricoViewModel> GetFullAndPartial(int idCliente = 0, int idContrato = 0, string idParcela = null, short tpHistorico = 1)
        {
            var cdCliente = idCliente;
            if (cdCliente == 0) { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }

            var historicosCliente = _qry.ObterHistoricoPorCliente(cdCliente, 0).ToList();
            List<Historico> clienteHistorico = new List<Historico>();
            List<int> historicosClienteId = historicosCliente.Select(x => x.Cod).ToList();
            clienteHistorico = _dbHistorico.FindAll(x => !x.DataExcluido.HasValue && historicosClienteId.Contains(x.Cod)).ToList();


            ViewBag.historicosCLiente = clienteHistorico.ToList();

            // var CodTipoCobranca = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).FirstOrDefault().CodTipoCobranca;
            var historico = clienteHistorico.FirstOrDefault();

            if (historico == null) { historico = new Historico(); }

            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).ToList();
            contratos.Insert(0, new Contrato());
            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.Contratos = listaContratos.ToList();

            var listaFases = Fases(0, tpHistorico);
            ViewBag.ListaMotivoAtraso = MotivoAtraso();
            ViewBag.ListaResolucao = _dbListas.Resulucao();
            List<TabelaSintese> listaTabelaSintese = new List<TabelaSintese>();
            listaTabelaSintese = _dbSintese.All().ToList();

            List<int> CodParcelas = idParcela.Split(',').Select(x => DbRotinas.ConverterParaInt(x)).ToList();

            List<Historico> ParcelaHistorico = _dbParcela.All().Where(x => CodParcelas.Contains(x.Cod)).ToList().Select(x => x.Historico).ToList();
            if (ParcelaHistorico != null)
            {
                if (ParcelaHistorico[0] != null)
                {
                    listaTabelaSintese = validarSintese(listaTabelaSintese, ParcelaHistorico);
                }

            }

            var listaFasesIds = listaTabelaSintese.Select(x => x.CodFase).ToList();
            ViewBag.ListaFases = listaFases.Where(x => listaFasesIds.Contains(x.Cod)).ToList();
            foreach (var item in listaTabelaSintese)
            {
                item.Sintese = DbRotinas.Descriptografar(item.Sintese);
            }
            listaTabelaSintese = listaTabelaSintese.OrderBy(x => x.Sintese).ToList();
            ViewBag.ListaSintese = listaTabelaSintese.Where(c => !c.DataExcluido.HasValue).ToList();
            HistoricoViewModel vm = new HistoricoViewModel();
            vm.CodCliente = cdCliente;
            vm.CodContrato = historico.CodContrato;
            vm.CodMotivoAtraso = historico.CodMotivoAtraso;
            vm.CodParcela = historico.CodParcela;
            vm.CodResolucao = historico.CodResolucao;
            vm.CodSintese = historico.CodSintese;
            vm.CodUsuario = historico.CodUsuario;
            vm.DataAgenda = historico.DataAgenda;
            vm.Observacao = historico.Observacao;
            vm.ParcelaIds = idParcela;
            vm.DataCadastro = DateTime.Now;

            if (tpHistorico == (short)Enumeradores.TipoHistorico.Contrato) { vm.CodContrato = idContrato; vm.CodParcela = 0; }
            if (tpHistorico == (short)Enumeradores.TipoHistorico.Parcela) { vm.CodContrato = 0; vm.CodParcela = 0; vm.ParcelaIds = idParcela; }
            historico.CodCliente = cdCliente;
            return vm;
        }

        private List<TabelaSintese> validarSintese(List<TabelaSintese> listaTabelaSintese, List<Historico> historico)
        {
            try
            {
                List<int> sintesesBloqueadasMudanca = new List<int>();
                var C6 = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "C6").FirstOrDefault();
                if (C6 != null)
                {
                    sintesesBloqueadasMudanca.Add(C6.Cod);
                }
                var PC = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "PC").FirstOrDefault();
                if (PC != null)
                {
                    sintesesBloqueadasMudanca.Add(PC.Cod);
                }

                var PE = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "PE").FirstOrDefault();
                if (PE != null)
                {
                    sintesesBloqueadasMudanca.Add(PE.Cod);
                }

                

                List<int> sintese = historico.Select(x => (int)x.CodSintese).ToList();
                var _admin = WebRotinas.ObterUsuarioLogado().Admin;
                if (sintese.Any(item => sintesesBloqueadasMudanca.Contains(item)) && !(bool)_admin)
                {
                    var codSintese = sintese.Intersect(sintesesBloqueadasMudanca).ToList();
                    listaTabelaSintese = listaTabelaSintese.Where(x => codSintese.Contains(x.Cod)).ToList();
                }


                //CASO EXISTA ALGUMA PARCELA COMO LIQUIDADO DEVERÁ MANTER COMO LIQUIDADO OU FAZER CONTATO
                var LQ = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "LQ").FirstOrDefault();
                if (LQ != null)
                {
                    if (historico.Where(x => x.CodSintese == LQ.Cod).Count() > 0 && !(bool)_admin)
                    {
                        var fazerContato = listaTabelaSintese.Where(x => x.CodigoSinteseCliente == "" || x.CodigoSinteseCliente == " ").Select(s => s.Cod).FirstOrDefault();
                        listaTabelaSintese = listaTabelaSintese.Where(x => x.Cod == LQ.Cod || x.Cod == fazerContato).ToList();
                    }
                    else if (!(bool)_admin)
                    {
                        listaTabelaSintese = listaTabelaSintese.Where(x => x.Cod != LQ.Cod).ToList();
                    }
                }
                return listaTabelaSintese;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //public List<DictionaryEntry> Fases(int CodTipoCobranca = 0, short tpHistorico = 0)
        //{
        //
        //    List<TabelaFase> lista = new List<TabelaFase>();
        //    lista = _dbFase.All().OrderBy(x => x.Ordem).Where(x => /*x.CodTipoCobranca == CodTipoCobranca &&*/ x.TipoHistorico == tpHistorico).ToList();
        //    lista.Insert(0, new TabelaFase());
        //    var list = from p in lista select new DictionaryEntry(DbRotinas.Descriptografar(p.Fase), p.Cod);
        //    return list.ToList();
        //}
        public List<TabelaFase> Fases(int CodTipoCobranca = 0, short tpHistorico = 0)
        {

            List<TabelaFase> lista = new List<TabelaFase>();
            lista = _dbFase.All().Where(x => /*x.CodTipoCobranca == CodTipoCobranca &&*/ x.TipoHistorico == tpHistorico).ToList();
            foreach (var item in lista)
            {
                item.Fase = DbRotinas.Descriptografar(item.Fase);
            }
            lista = lista.OrderBy(x => x.Ordem).ToList();
            return lista.ToList();
        }

        //public List<DictionaryEntry> Sintese(int CodFase = 0)
        //{
        //
        //    List<TabelaSintese> lista = new List<TabelaSintese>();
        //    lista = _dbSintese.All().OrderBy(x => x.Ordem).Where(x => x.CodFase == CodFase).ToList();
        //    lista.Insert(0, new TabelaSintese());
        //    var list = from p in lista select new DictionaryEntry(DbRotinas.Descriptografar(p.Sintese), p.Cod);
        //    return list.ToList();
        //}

        public List<TabelaSintese> Sintese(int CodFase = 0)
        {

            List<TabelaSintese> lista = new List<TabelaSintese>();
            lista = _dbSintese.All().OrderBy(x => x.Ordem).Where(x => x.CodFase == CodFase).ToList();
            return lista.ToList();
        }

        [HttpPost]
        public JsonResult Salvar(HistoricoViewModel dados)
        {
            List<Historico> listHistorico = new List<Historico>();
            string[] parcelas = dados.ParcelaIds.Split(',');
            try
            {
                for (int i = 0; i < parcelas.Length; i++)
                {
                    int ParcelaId = Convert.ToInt32(parcelas[i]);

                    Historico hist = new Historico();
                    Historico ultimoHist = null;
                    List<Historico> listaHistoricosParcela = _dbHistorico.All().Where(x => x.CodParcela == ParcelaId).ToList().OrderByDescending(o => o.DataCadastro).ToList();
                    var _parcela = _dbParcela.FindById(ParcelaId);
                    if (listaHistoricosParcela.Count() > 0)
                    {
                        ultimoHist = listaHistoricosParcela.First();
                    }
                    hist.CodCliente = dados.CodCliente;
                    if (dados.CodContrato == 0) dados.CodContrato = null;
                    hist.CodContrato = dados.CodContrato;
                    hist.CodMotivoAtraso = dados.CodMotivoAtraso;

                    //DEFINE AS REGRAS
                    RegrasClassificacoes(dados, ultimoHist, _parcela);

                    //CASO O USUARIO NAO PREENCHER A SINTESE IRA COLOCAR A ULTIMA COLOCADA
                    if (!dados.CodSintese.HasValue && ultimoHist != null)
                    {
                        hist.CodSintese = ultimoHist.CodSintese;
                    }
                    else
                    {
                        hist.CodSintese = dados.CodSintese;
                    }
                    //CASO O USUARIO NAO PREENCHER A RESOLUÇÃO IRA COLOCAR A ULTIMA COLOCADA
                    if (!dados.CodResolucao.HasValue && ultimoHist != null)
                    {
                        hist.CodResolucao = ultimoHist.CodResolucao;
                    }
                    else
                    {
                        hist.CodResolucao = dados.CodResolucao;
                    }
                    hist.CodUsuario = dados.CodUsuario;
                    hist.DataAgenda = dados.DataAgenda;
                    hist.Observacao = DbRotinas.Criptografar(dados.Observacao);
                    hist.DataCadastro = DateTime.Now;
                    hist.CodParcela = ParcelaId;
                    listHistorico.Add(hist);
                }
                carregarCustomViewBags();

                foreach (var historico in listHistorico)
                {
                    if (historico.CodParcela == 0) historico.CodParcela = null;
                    var entityHistorico = _repository.CreateOrUpdate(historico);

                    //VERIFICA SE O HISTORICO FOR DE CONTRATO ELE REGISTRA NO CONTRATO SEU CODIGO
                    if (entityHistorico.CodContrato > 0)
                    {
                        Contrato contrato = _dbContrato.FindById((int)entityHistorico.CodContrato);
                        contrato.CodHistorico = (int)entityHistorico.Cod;
                        _dbContrato.CreateOrUpdate(contrato);

                        //OBTEM TODAS PARCELAS DO CONTRATO E VERIFICA AS AÇÔES
                        List<ContratoParcela> parcelasContrato = _dbParcela.All().Where(x => x.CodContrato == entityHistorico.CodContrato).ToList();
                        foreach (var p in parcelasContrato)
                        {
                            //VERIFICA AS ACOES DA PARCELA
                            var objSintese = _dbSintese.FindById((int)entityHistorico.CodSintese);
                            string obs = $"{DateTime.Now} - usuario:{WebRotinas.ObterUsuarioLogado().Nome} - fase: {DbRotinas.Descriptografar(objSintese.TabelaFase.Fase)} - sintese: {DbRotinas.Descriptografar(objSintese.Sintese)} - obs: {entityHistorico.Observacao}";

                            OperacaoDbRetorno op = new ReguaRotinas().FinalizarAtendimentoPorParcela(p.Cod, historico.DataAgenda, obs);
                        }
                    }
                    //VERIFICA SE O HISTORICO FOR DE PARCELA ELE REGISTRA NA PARCELA SEU CODIGO
                    if (entityHistorico.CodParcela > 0)
                    {
                        ContratoParcela parcela = _dbParcela.FindById((int)entityHistorico.CodParcela);
                        parcela.CodHistorico = (int)entityHistorico.Cod;
                        _dbParcela.CreateOrUpdate(parcela);

                        #region MARCA NO HISTORICO O ID DA ACAO
                        //ENCONTRA UM ACAO PENDENTE DESTA PARCELA E REGISTRA NO HISTORICO SEU ID PARA QTOS HISTORICOS FORAM REAGENDAMENTO DE UMA ACAO
                        var acaoCobranca = _dbContratoAcaoCobrancaHistorico
                                                .FindAll(x => x.CodParcela == parcela.Cod
                                                    && !x.DataEnvio.HasValue
                                                    && x.TabelaTipoCobrancaAcao.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.CONTATO)
                                                .FirstOrDefault();
                        if (acaoCobranca != null)
                        {
                            entityHistorico.CodAcaoCobranca = acaoCobranca.Cod;
                            _dbHistorico.Update(entityHistorico);
                        }
                        #endregion

                        //VERIFICA AS ACOES DA PARCELA
                        var objSintese = _dbSintese.FindById((int)entityHistorico.CodSintese);
                        string obs = $"{DateTime.Now} - usuario:{WebRotinas.ObterUsuarioLogado().Nome} - fase: {DbRotinas.Descriptografar(objSintese.TabelaFase.Fase)} - sintese: {DbRotinas.Descriptografar(objSintese.Sintese)} - agendamento: {entityHistorico.DataAgenda} - obs: {DbRotinas.Criptografar(entityHistorico.Observacao)}";

                        OperacaoDbRetorno op = new ReguaRotinas().FinalizarAtendimentoPorParcela(entityHistorico.CodParcela, entityHistorico.DataAgenda, obs);
                    }
                }


                ViewBag.MensagemOK = "Opercao realizada com sucesso";
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        private void RegrasClassificacoes(HistoricoViewModel dados, Historico ultimoHist, ContratoParcela _parcela)
        {
            //DEFINIÇÂO DE CLASSIFICACOES
            //Classificações que não podem ser reclassificadas no Junix > PC,PE,C6 | BNS,FAZ 
            //Classificações que não podem ser Feitas no Junix > EX,C6,C4,PC,PE

            #region Regra EX
            //Esta classificação so podera ser feita dentro do JD
            var _codSinteseEX = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "EX").FirstOrDefault();
            if (_codSinteseEX != null)
            {
                if (dados.CodSintese == (int)_codSinteseEX.Cod)
                {
                    throw new Exception($@"{Idioma.Traduzir("Está classificasão so podera ser feita no JDE.")}");
                }
            }
            #endregion

            #region Regra C6
            //Esta classificação só podera ser trocada dentro do JD, más podera ser mantida no Junix
            var _codSinteseC6 = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "C6").FirstOrDefault();
            if (_codSinteseC6 != null && ultimoHist != null)
            {
                //VERIFICA SE A ULTIMA SINTESE É C6 E FOI MODIFICADA
                if (ultimoHist.CodSintese == _codSinteseC6.Cod && (dados.CodSintese != _codSinteseC6.Cod && dados.CodSintese > 0))
                {
                    throw new Exception($@"{Idioma.Traduzir("Está classificasão so podera ser feita no JDE.")}");
                }
            }
            #endregion

            #region Regra LQ
            //Esta classificação só podera ser trocada dentro do JD, más podera ser mantida no Junix
            var _codSinteseLQ = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "LQ").FirstOrDefault();
            var _codSinteseFazerContato = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "" || x.CodigoSinteseCliente == " ").FirstOrDefault();
            if (_codSinteseLQ != null && ultimoHist != null)
            {
                List<int> SintesePermitida = new List<int>();
                SintesePermitida.Add(_codSinteseLQ.Cod);
                SintesePermitida.Add(_codSinteseFazerContato.Cod);

                //VERIFICA SE A ULTIMA SINTESE É LQ E FOI MODIFICADA
                if (ultimoHist.CodSintese == _codSinteseLQ.Cod && dados.CodSintese > 0)
                {
                    if (!SintesePermitida.Contains((int)dados.CodSintese))
                        throw new Exception($@"{Idioma.Traduzir("A reclassificação de LQ - Liquidado só poderá ser feita no JDE.")}");
                }
            }
            #endregion

            #region Regra C4
            //Esta classificação so podera ser feita dentro do JD
            var _codSinteseC4 = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "C4").FirstOrDefault();
            if (_codSinteseC4 != null)
            {
                if (dados.CodSintese == _codSinteseC4.Cod)
                {
                    throw new Exception($@"{Idioma.Traduzir("Está classificasão so podera ser feita no JDE.")}");
                }

            }
            #endregion

            #region Regra Empresa Cobrança
            //Não pode ser alterada classificacao de faturas com cobrança externas...(BNS e FAS)
            var _cdEmpresaCobranca = _parcela.CodigoEmpresaCobranca;
            if ((_cdEmpresaCobranca == "BNS" || _cdEmpresaCobranca == "FAS"))
            {
                if (ultimoHist != null)
                {
                    if (ultimoHist.CodSintese != dados.CodSintese)
                    {
                        var msg = $"Fatura {ultimoHist.ContratoParcela.FluxoPagamento} não pode ser reclassificada pois a cobrança pertence há {_cdEmpresaCobranca}";
                        throw new Exception(msg);
                    }
                }
            }
            #endregion

            #region Regra PC
            //Esta classificação só podera ser trocada dentro do JD
            var _codSintesePC = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "PC").FirstOrDefault();
            if (_codSintesePC != null)
            {
                if (ultimoHist != null && dados.CodSintese != null)
                {
                    //CASO ULTIMO HISTORICO FOR PC NAO PODE SER TROCADO OU NAO PODE SER CLASSIFICADO COMO PC
                    if (ultimoHist.CodSintese == _codSintesePC.Cod && dados.CodSintese != _codSintesePC.Cod)
                    {
                        throw new Exception($@"{Idioma.Traduzir($"A reclassificação de PC podera ser feita apenas no JDE.")}");
                    }
                }
            }
            #endregion

            #region Regra PE
            //Esta classificação só podera ser trocada dentro do JD
            var _codSintesePE = _dbSintese.All().Where(x => x.CodigoSinteseCliente == "PE").FirstOrDefault();
            if (_codSintesePE != null)
            {
                if (dados.CodSintese != null)
                {
                    if (ultimoHist != null)
                    {
                        //CASO ULTIMO HISTORICO FOR PE NAO PODE SER TROCADO OU NAO PODE SER CLASSIFICADO COMO PE
                        if (dados.CodSintese == _codSintesePE.Cod || (ultimoHist.CodSintese == _codSintesePE.Cod && dados.CodSintese != _codSintesePE.Cod))
                        {
                            throw new Exception($@"{Idioma.Traduzir("Está classificasão so podera ser feita no JDE.")}");
                        }
                    }

                }
            }
            #endregion
        }

        [HttpPost]
        public ActionResult DadosHistoricoPorCliente(int ClienteId = 0, short tpHistorico = (short)Enumeradores.TipoHistorico.Cliente, int CodSintese = 0, int CodSinteseParcela = 0)
        {
            if (ClienteId == 0) { ClienteId = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }

            var query_dados = _qry.ListarHistoricoCliente(ClienteId);

            if (tpHistorico == (short)Enumeradores.TipoHistorico.Cliente)
            {
                query_dados = query_dados.Where(x => !x.CodParcela.HasValue).ToList();
            }
            if (tpHistorico == (short)Enumeradores.TipoHistorico.Parcela)
            {
                query_dados = query_dados.Where(x => x.CodParcela > 0).ToList();
                if (CodSinteseParcela > 0)
                {
                    query_dados = query_dados.Where(x => x.CodSintese == CodSinteseParcela).ToList();
                }
            }
            if (tpHistorico == (short)Enumeradores.TipoHistorico.Contrato)
            {
                query_dados = query_dados.Where(x => x.CodContrato > 0).ToList();
                if (CodSintese > 0)
                {
                    query_dados = query_dados.Where(x => x.CodSintese == CodSintese).ToList();
                }
            }

            return Json(new { OK = true, Data = query_dados.OrderByDescending(x => x.DataCadastro).ToList() });
        }

        public List<TabelaFase> Fases()
        {
            List<TabelaFase> lista = new List<TabelaFase>();
            lista = _dbFase.All().ToList();
            foreach (var item in lista)
            {
                item.Fase = DbRotinas.Descriptografar(item.Fase);
            }
            lista = lista.OrderBy(x => x.Ordem).ToList();
            return lista.ToList();
        }
        public List<TabelaResolucao> Resulucao()
        {
            List<TabelaResolucao> lista = new List<TabelaResolucao>();
            lista = _dbResolucao.All().ToList();
            foreach (var item in lista)
            {
                item.Resolucao = DbRotinas.Descriptografar(item.Resolucao);
            }
            lista = lista.OrderBy(x => x.Resolucao).ToList();
            return lista.ToList();
        }
        public List<TabelaMotivoAtraso> MotivoAtraso()
        {
            List<TabelaMotivoAtraso> lista = new List<TabelaMotivoAtraso>();
            lista = _dbMotivoAtraso.All().ToList();
            foreach (var item in lista)
            {
                item.MotivoAtraso = DbRotinas.Descriptografar(item.MotivoAtraso);
            }
            lista = lista.OrderBy(x => x.MotivoAtraso).ToList();
            return lista.ToList();
        }

    }
}