﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Text;
using Cobranca.Rotinas;
using System.IO;

namespace Cobranca.Web.Controllers
{
    public class ParametroFeriadoController : BasicController<ParametroFeriado>
    {
        public Repository<ParametroFeriado> _dbFeriado = new Repository<ParametroFeriado>();

        [Authorize]
        public override ActionResult Index()
        {
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();

            if (WebRotinas.ObterUsuarioLogado().PerfilTipo != Enumeradores.PerfilTipo.Admin)
            {
                return RedirectToAction("Home", new { msg = "Acesso Negado" });
            }

            return base.Index();
        }


        [Authorize]
        [HttpGet]
        public override ActionResult Cadastro(int id = 0)
        {
            return base.Cadastro(id);
        }

        public override ActionResult Cadastro(ParametroFeriado dados)
        {
            return base.Cadastro(dados);
        }
        public override void Ok(ParametroFeriado dados, HttpRequestBase Request)
        {

        }

        [HttpPost]
        public JsonResult GetListData(DTParameters param)
        {
            try
            {
                var dtsource = _dbFeriado.All().ToList();
                IQueryable<object> data = null;
                data = dtsource
                            .Select(x => new
                            {
                                Cod = $"{DbRotinas.ConverterParaString(x.Cod)}",
                                Data = x.Data,
                                DataDesc = $"{x.Data:dd/MM/yyyy}",
                                Feriado = x.Feriado,
                                Ativo = EnumeradoresDescricao.SimNao((short)(x.Ativo == true ? Enumeradores.SimNao.Sim : Enumeradores.SimNao.Nao)),
                            }).AsQueryable();
                var result = WebRotinas.DTRotinas.DTResult(param, data);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        public FileResult DownloadFile(DTParameters param)
        {
            string filename = WebRotinas.ObterArquivoTemp();
            FileInfo info = new FileInfo(filename);
            var dtsource = _dbFeriado.All().OrderBy(x=> x.Data).ToList();
            IQueryable<object> data = null;
            data = dtsource
                        .Select(x => new
                        {
                            Data = $"{x.Data:dd/MM/yyyy}",
                            FeriadoDescricao = $"{x.Feriado}",
                            Ativo = $"{EnumeradoresDescricao.SimNao((short)(x.Ativo == true ? Enumeradores.SimNao.Sim : Enumeradores.SimNao.Nao))}"
                        }).AsQueryable();
            ConverterListaParaExcel.GerarCSVDeGenericList(data.ToList(), filename);
            var nmarquivo = "Feriado" + DateTime.Now.ToString("ddMMyy") + ".csv";
            return File(info.FullName, "application/force-download", nmarquivo);
        }

        public IQueryable<object> DataList()
        {
            var dados = _dbFeriado.All().ToList();
            IQueryable<object> data = null;
            data = dados
                        .Select(x => new
                        {
                            Cod = $"{DbRotinas.ConverterParaString(x.Cod)}",
                            Data = x.Data,
                            Feriado = x.Feriado,
                            Email = EnumeradoresDescricao.SimNao((short)(x.Ativo == true ? Enumeradores.SimNao.Sim : Enumeradores.SimNao.Nao)),
                        }).AsQueryable();

            return data;
        }

    }
}