﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

namespace Cobranca.Web.Controllers
{
    public class ImportacaoController : Controller
    {
        private Repository<Contrato>
      _dbContrato = new Repository<Contrato>();
        public Dados _qry = new Dados();

        [HttpGet]
        public ActionResult Contrato()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Devedor()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Parcela()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ImportContrato(FormCollection formCollection)
        {
            HttpPostedFileBase file = Request.Files["UploadedFile"];
            if (file.FileName != String.Empty)
            {
                InsereContrato(formCollection);
            }
            else
            {
                TempData["resultado"] = "Selecione um arquivo";
            }

            return RedirectToAction("Contrato");
        }

        [HttpPost]
        public JsonResult Teste()
        {
            var dados = _qry.QtdeImportacao();
            return Json(new { OK = true, lista = dados });

        }

        private void InsereContrato(FormCollection formCollection)
        {

            try
            {
                string ArquivoNome = "";
                HttpPostedFileBase file = Request.Files["UploadedFile"];

                string StrFileName = file.FileName.Substring(file.FileName.LastIndexOf("\\") + 1);
                ArquivoNome = file.FileName;

                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string ArquivoTipo = file.ContentType;
                    //if (!ArquivoTipo.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
                    if (!ArquivoTipo.Equals("application/vnd.ms-excel"))
                    {
                        ArquivoNome = "Favor anexar arquivo em formato CSV.";
                    }
                    else
                    {
                        string caminho = Server.MapPath("~/Documentos/ImportacaoExcel/" + string.Concat(StrFileName));
                        file.SaveAs(@caminho);

                        Int32 c = 0;
                        DateTime Data = DateTime.Now;

                        if (file.ContentLength > 0)
                        {
                            using (StreamReader fluxotexto = new StreamReader(caminho, System.Text.Encoding.Default, false, 512))
                                while (true)
                                {
                                    string linhatexto = fluxotexto.ReadLine();
                                    if (linhatexto == null)
                                    {
                                        break;
                                    }
                                    string[] quebra = linhatexto.Split(new Char[] { ';' });
                                    if (c > 0)
                                    {
                                        _qry.executarImportacaoContrato(
                                              quebra[0] //FILIAL
                                            , quebra[1] //PRODUTO
                                            , quebra[2] //NUMERO CONTRATO
                                            , quebra[3] //NUMERO CLIENTE
                                            , quebra[4] //NOME CLIENTE
                                            , quebra[5] //CPF
                                            , quebra[6] //NUMERO FATURA
                                            , DbRotinas.ConverterParaDecimal(quebra[7]) //VALOR FATURA
                                            , DbRotinas.ConverterParaDecimal(quebra[8]) //VALOR ABERTO
                                            , DbRotinas.ConverterParaDatetimeOuNull(quebra[9]) //DATA VENCIMENTO
                                            , DbRotinas.ConverterParaInt(quebra[10]) //NUMERO PARCELA
                                            , DbRotinas.ConverterParaDecimal(quebra[11]) //VALOR PAGO
                                            , DbRotinas.ConverterParaDatetimeOuNull(quebra[12]) //DATA PAGAMENTO
                                            , DbRotinas.ConverterParaShort(quebra[13]) //STATUS
                                            , (quebra[14]) //EMAIL
                                            , (quebra[15]) //TELEFONE RESIDENCIAL
                                            , (quebra[16]) //TELEFONE COMERCIAL
                                            , (quebra[17]) //TELEFONE CELULAR
                                            , (quebra[18]) //CEP
                                            , (quebra[19]) //ENDERECO
                                            , (quebra[20]) //BAIRRO
                                            , (quebra[21]) //UF
                                            , (quebra[22]) //CIDADE
                                            );
                                    }
                                    c = c + 1;
                                    TempData["QuantidadeInserido"] = c;
                                }
                        }
                        ArquivoNome = "Arquivo inserido com sucesso.";
                    }
                }
                TempData["resultado"] = ArquivoNome;
            }
            catch (Exception ex)
            {
                TempData["resultado"] = ex.Message;
            }
        }

        //private void InsereContrato(FormCollection formCollection)
        //{
        //    try
        //    {
        //        string ArquivoNome = "";
        //        HttpPostedFileBase file = Request.Files["UploadedFile"];

        //        string StrFileName = file.FileName.Substring(file.FileName.LastIndexOf("\\") + 1);
        //        ArquivoNome = file.FileName;

        //        if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
        //        {
        //            string ArquivoTipo = file.ContentType;
        //            //if (!ArquivoTipo.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
        //            if (!ArquivoTipo.Equals("application/vnd.ms-excel"))
        //            {
        //                ArquivoNome = "Favor anexar arquivo em formato CSV.";
        //            }
        //            else
        //            {
        //                string caminho = Server.MapPath("~/Documentos/ImportacaoExcel/" + string.Concat(StrFileName));
        //                file.SaveAs(@caminho);

        //                Int32 c = 0;
        //                DateTime Data = DateTime.Now;

        //                if (file.ContentLength > 0)
        //                {
        //                    using (StreamReader fluxotexto = new StreamReader(caminho, System.Text.Encoding.Default, false, 512))
        //                        while (true)
        //                        {
        //                            string linhatexto = fluxotexto.ReadLine();
        //                            if (linhatexto == null)
        //                            {
        //                                break;
        //                            }
        //                            string[] quebra = linhatexto.Split(new Char[] { ';' });
        //                            if (c > 0)
        //                            {
        //                                _qry.executarImportacaoContrato(
        //                                      quebra[3] //NUMERO CONTRATO
        //                                    , quebra[9] //NUMERO FATURA
        //                                    , quebra[1] //FILIAL 
        //                                    , quebra[5] //PRODUTO
        //                                    , quebra[8] //DESCRICAO CLIENTE
        //                                    , quebra[7] //CPF
        //                                    , quebra[6] //NUMERO CLIENTE
        //                                    , DbRotinas.ConverterParaDecimal(quebra[10]) //VALOR FATURA
        //                                    , DbRotinas.ConverterParaDecimal(quebra[11]) //VALOR ABERTO
        //                                    , DbRotinas.ConverterParaDatetimeOuNull(quebra[12]) //DATA VENCIMENTO
        //                                    , null //DATA PAGAMENTO
        //                                    , 1 //STATUS
        //                                    );
        //                            }
        //                            c = c + 1;
        //                        }
        //                }
        //                ArquivoNome = "Arquivo inserido com sucesso.";
        //            }
        //        }
        //        TempData["resultado"] = ArquivoNome;
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["resultado"] = ex.Message;
        //    }
        //}

        //[HttpPost]
        //public ActionResult ImportCliente(FormCollection formCollection)
        //{
        //    HttpPostedFileBase file = Request.Files["UploadedFile"];
        //    if (file.FileName != String.Empty)
        //    {
        //        InsereCliente(formCollection);
        //    }
        //    else
        //    {
        //        TempData["resultado"] = "Selecione um arquivo";
        //    }

        //    return RedirectToAction("Devedor");
        //}

        //private void InsereCliente(FormCollection formCollection)
        //{
        //    try
        //    {
        //        string ArquivoNome = "";
        //        HttpPostedFileBase file = Request.Files["UploadedFile"];

        //        string StrFileName = file.FileName.Substring(file.FileName.LastIndexOf("\\") + 1);
        //        ArquivoNome = file.FileName;

        //        if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
        //        {
        //            string ArquivoTipo = file.ContentType;
        //            //if (!ArquivoTipo.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
        //            if (!ArquivoTipo.Equals("application/vnd.ms-excel"))
        //            {
        //                ArquivoNome = "Favor anexar arquivo em formato CSV.";
        //            }
        //            else
        //            {
        //                string caminho = Server.MapPath("~/Documentos/ImportacaoExcel/" + string.Concat(StrFileName));
        //                file.SaveAs(@caminho);

        //                Int32 c = 0;
        //                DateTime Data = DateTime.Now;

        //                if (file.ContentLength > 0)
        //                {
        //                    using (StreamReader fluxotexto = new StreamReader(caminho, System.Text.Encoding.Default, false, 512))
        //                        while (true)
        //                        {
        //                            string linhatexto = fluxotexto.ReadLine();
        //                            if (linhatexto == null)
        //                            {
        //                                break;
        //                            }
        //                            string[] quebra = linhatexto.Split(new Char[] { ';' });
        //                            if (c > 0)
        //                            {
        //                                _qry.executarImportacaoContrato(
        //                                      quebra[3] //NUMERO CONTRATO
        //                                    , quebra[9] //NUMERO FATURA
        //                                    , quebra[1] //FILIAL 
        //                                    , quebra[5] //PRODUTO
        //                                    , quebra[8] //DESCRICAO CLIENTE
        //                                    , quebra[7] //CPF
        //                                    , quebra[6] //NUMERO CLIENTE
        //                                    , DbRotinas.ConverterParaDecimal(quebra[10]) //VALOR FATURA
        //                                    , DbRotinas.ConverterParaDecimal(quebra[11]) //VALOR ABERTO
        //                                    , DbRotinas.ConverterParaDatetimeOuNull(quebra[12]) //DATA VENCIMENTO
        //                                    , null //DATA PAGAMENTO
        //                                    , 1 //STATUS
        //                                    );
        //                            }
        //                            c = c + 1;
        //                        }
        //                }
        //                ArquivoNome = "Arquivo inserido com sucesso.";
        //            }
        //        }
        //        TempData["resultado"] = ArquivoNome;
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["resultado"] = ex.Message;
        //    }
        //}

    }
}