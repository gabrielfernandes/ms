﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class TabelaVendaController : BasicController<TabelaVenda>
    {

        public Repository<TabelaVenda> _dbTabelaVenda = new Repository<TabelaVenda>();

        [HttpPost]
        public JsonResult ListarTabelaVenda(int CodTabelaVenda = 0)
        {
            try
            {
                var venda = _dbTabelaVenda.FindAll(x => x.Cod == CodTabelaVenda).FirstOrDefault();
                return Json(new
                 {
                     OK = true,
                     InfoTabelaVenda = new
                     {
                         Cod = venda.Cod,
                         Nome = venda.Nome,
                         VigenciaDe = string.Format("{0:dd/MM/yyyy}", venda.VigenciaDe),
                         VigenciaAte = string.Format("{0:dd/MM/yyyy}", venda.VigenciaAte),
                         PercentualMinimoEntrada = string.Format("{0:P2}.", (venda.PercentualMinimoEntrada/100)),
                         PercentualComissao = string.Format("{0:P2}.", (venda.PercentualComissao/100))
                     }
                 });



            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
    }
}