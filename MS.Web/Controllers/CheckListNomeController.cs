﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Cobranca.Web.Controllers
{
    public class CheckListNomeController : BasicController<TabelaCheckListNome>
    {
        public Repository<TabelaCheckListNome> _dbTabelaCheckListNome = new Repository<TabelaCheckListNome>();



        [HttpPost]
        public JsonResult ObterTipoCheckList(int CodTabelaCheckListNome = 0)
        {
            try
            {
                var checkListNome = _dbTabelaCheckListNome.FindAll(x => x.Cod == CodTabelaCheckListNome).FirstOrDefault();
                return Json(new
                {
                    OK = true,
                    InfoTabelaCheckListNome = new
                    {
                        tipo = checkListNome.TipoCheckList
                    }
                });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
     

    }
}