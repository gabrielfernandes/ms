﻿using Cobranca.Db.Repositorio;
using Cobranca.Db.Models;
using Cobranca.Db.Rotinas;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Cobranca.Web.Controllers
{

    public class HomePropostaController : Controller
    {
        private Repository<Usuario>
         _repUsuario = new Repository<Usuario>();
        private Repository<TabelaFase>
        _repFase = new Repository<TabelaFase>();
        private Repository<Contrato>
        _repContrato = new Repository<Contrato>();
        private Repository<TabelaCampanha>
       _repCampanha = new Repository<TabelaCampanha>();
        private Repository<TabelaTipoCobranca>
     _repTipoCobranca = new Repository<TabelaTipoCobranca>();
        private Repository<ContratoParcela>
     _repContratoParcela = new Repository<ContratoParcela>();
        private Repository<TabelaMotivoAtraso>
     _repTabelaMotivoAtraso = new Repository<TabelaMotivoAtraso>();
        private Repository<TabelaResolucao>
     _repTabelaResolucao = new Repository<TabelaResolucao>();
        private Repository<Proposta>
  _repProposta = new Repository<Proposta>();
        private Repository<TabelaEmpresaVenda>
_repEmpresaVenda = new Repository<TabelaEmpresaVenda>();
        private Repository<TabelaUnidade>
_repUnidade = new Repository<TabelaUnidade>();
        private Repository<Proposta>
_Proposta = new Repository<Proposta>();
        private Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();

        [HttpGet]
        public ActionResult Index()
        {



            HomePesquisaViewModel model = new HomePesquisaViewModel();
            bool fgSistemaCobranca = WebRotinas.ObterUsuarioLogado().SistemaCobranca == true;
            var listaCampanha = _repCampanha.All().ToList();
            listaCampanha.Insert(0, new TabelaCampanha());
            ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);

            var listaCobranca = _repTipoCobranca.All().ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaCobranca = from p in listaCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);


            var listaEmpresaVenda = _repEmpresaVenda.All().ToList();
            listaEmpresaVenda.Insert(0, new TabelaEmpresaVenda());
            ViewBag.ListaEmpresaVenda = from p in listaEmpresaVenda.OrderBy(x => x.EmpresaVendas).ToList() select new DictionaryEntry(p.EmpresaVendas, p.Cod);

            //model.QtdUnidades = 

            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Login", null);
            }
            model.ListaFase = _repFase.All().ToList();
            model.ListaPropostas = FiltrarProposta(model, _repProposta.All());

            if (fgSistemaCobranca)
            {
                listaCobranca = listaCobranca.Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
                model.ListaFase = model.ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
            }
            else
            {
                listaCobranca = listaCobranca.Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Proposta).ToList();
                model.ListaFase = model.ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.Proposta).ToList();
            }

            var dados = FiltrarUnidade(model, _repUnidade.All());

            model.QtdUnidade = dados.Count();

            Repository<TabelaUnidadeReserva> hReserva = new Repository<TabelaUnidadeReserva>();
            int idUsuario = WebRotinas.ObterUsuarioLogado().Cod;
            int quantidadeReserva = hReserva.All().Where(x => x.CodUsuario == idUsuario && x.StatusReserva == (short)Enumeradores.ReservaStatus.Reservado).Count();

            model.QtdReserva = quantidadeReserva;// dados.Count(x => x.Status == (short)Enumeradores.StatusUnidade.Reservada);
            model.QtdVenda = dados.Count(x => x.Status == (short)Enumeradores.StatusUnidade.Vendida);
            model.PercentualConversao = _Proposta.All().Where(x => !x.DataExcluido.HasValue).Count();
            model.QtdAtiva = dados.Count(x => x.Status == (short)Enumeradores.StatusUnidade.Reservada);
            model.QtdEstoque = dados.Count(x => x.Status == (short)Enumeradores.StatusUnidade.Disponivel);

            return View(model);
        }

        [HttpGet]
        public ActionResult MapsFrame()
        {

            HomePesquisaViewModel filtro = new HomePesquisaViewModel();
            CarregarViewBags(filtro);
            return View(filtro);
        }


        [HttpPost]
        public ActionResult MapsFrame(HomePesquisaViewModel filtro)
        {
            CarregarViewBags(filtro);
            return View(filtro);
        }

        [HttpPost]
        public ActionResult Index(HomePesquisaViewModel model)
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Login", null);
            }
            bool fgSistemaCobranca = WebRotinas.ObterUsuarioLogado().SistemaCobranca == true;
            var listaCobranca = _repTipoCobranca.All().ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaCobranca = from p in listaCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);
            var listaEmpresaVenda = _repEmpresaVenda.All().ToList();
            listaEmpresaVenda.Insert(0, new TabelaEmpresaVenda());
            ViewBag.ListaEmpresaVenda = from p in listaEmpresaVenda.OrderBy(x => x.EmpresaVendas).ToList() select new DictionaryEntry(p.EmpresaVendas, p.Cod);
            model.ListaFase = _repFase.All().ToList();
            if (fgSistemaCobranca)
            {
                listaCobranca = listaCobranca.Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
                model.ListaFase = model.ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
            }
            else
            {
                listaCobranca = listaCobranca.Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Proposta).ToList();
                model.ListaFase = model.ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.Proposta).ToList();
            }

            model.ListaPropostas = FiltrarProposta(model, _repProposta.All());

            model.QtdUnidade = model.ListaPropostas.Count();

            Repository<TabelaUnidadeReserva> hReserva = new Repository<TabelaUnidadeReserva>();
            int idUsuario = WebRotinas.ObterUsuarioLogado().Cod;
            int quantidadeReserva = hReserva.All().Where(x => x.CodUsuario == idUsuario && x.StatusReserva == (short)Enumeradores.ReservaStatus.Reservado).Count();

            model.QtdReserva = quantidadeReserva;// dados.Count(x => x.Status == (short)Enumeradores.StatusUnidade.Reservada);
            model.QtdVenda = model.ListaPropostas.Count(x => x.Status == (short)Enumeradores.StatusUnidade.Vendida);
            model.PercentualConversao = (model.QtdUnidade * model.QtdVenda) / 100;
            model.QtdAtiva = model.ListaPropostas.Count(x => x.Status == (short)Enumeradores.StatusUnidade.Reservada);
            model.QtdEstoque = model.ListaPropostas.Count(x => x.Status == (short)Enumeradores.StatusUnidade.Disponivel);

            return View(model);
        }
        private List<Proposta> FiltrarProposta(HomePesquisaViewModel model, IQueryable<Proposta> queryable)
        {
            var Admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;
            List<Proposta> resultado = queryable.ToList();
            if (WebRotinas.ObterUsuarioLogado().CodEmpresaVenda > 0)
            {
                resultado = resultado.Where(c => c.CodUsuario == WebRotinas.ObterUsuarioLogado().Cod).ToList();
            }
            if (model.CodEmpreendimento > 0)
            {
                resultado = resultado.Where(c => c.TabelaUnidade.TabelaBloco.CodEmpreend == model.CodEmpreendimento).ToList();
            }
            if (model.CodBloco > 0)
            {
                resultado = resultado.Where(c => c.TabelaUnidade.CodBloco == model.CodBloco).ToList();
            }
            if (model.CodEmpresaVenda > 0)
            {
                resultado = resultado.Where(c => c.CodEmpresaVenda == model.CodEmpresaVenda).ToList();
            }
            if (model.Status > 0)
            {
                resultado = resultado.Where(x => x.Status == model.Status).ToList();
            }
            return resultado.Take(20).ToList();
        }

        private List<TabelaUnidade> FiltrarUnidade(HomePesquisaViewModel model, IQueryable<TabelaUnidade> queryable)
        {
            var Admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;
            List<TabelaUnidade> resultado = queryable.ToList();
            if (model.CodEmpreendimento > 0)
            {
                resultado = resultado.Where(c => c.TabelaBloco.CodEmpreend == model.CodEmpreendimento).ToList();
            }
            if (model.CodBloco > 0)
            {
                resultado = resultado.Where(c => c.CodBloco == model.CodBloco).ToList();
            }
            return resultado.ToList();
        }

        [HttpPost]
        public JsonResult ObterDadosGrafico(HomePesquisaViewModel model)
        {
            var propostas = FiltrarProposta(model, _repProposta.All());
            //if (model.CodCampanha > 0)
            //{
            //    propostas = propostas.Where(x => x.PropostaValores).ToList();
            //}
            var dadosAgrupados = propostas.GroupBy(x => x.Status);

            var dados = from p in dadosAgrupados
                        select new
                        {
                            name = EnumeradoresDescricao.StatusProposta(p.Key),
                            y = p.Count()
                        };
            return Json(dados.OrderByDescending(x => x.y));
        }

        [HttpPost]
        public JsonResult ObterDadosGraficoUnidade(HomePesquisaViewModel model)
        {
            var unidades = FiltrarUnidade(model, _repUnidade.All());
            //if (model.CodCampanha > 0)
            //{
            //    propostas = propostas.Where(x => x.PropostaValores).ToList();
            //}
            var dadosAgrupados = unidades.GroupBy(x => x.Status);
            var dados = from p in dadosAgrupados select new { name = EnumeradoresDescricao.StatusUnidade(p.Key), y = p.Count() };
            return Json(dados);
        }

        [HttpPost]
        public JsonResult QtdProposta(HomePesquisaViewModel model)
        {
            var propostas = FiltrarProposta(model, _repProposta.All());
            //if (model.CodEmpreendimento > 0)
            //{
            //    propostas = propostas.Where(x => x.ta).ToList();
            //}
            var dadosAgrupados = propostas.GroupBy(x => x.Status);
            var qtdProposta = propostas.GroupBy(x => x.Cod);
            var dados = propostas.Count();
            return Json(new { qtd = dados });
        }


        [HttpGet]
        public ActionResult Propostas()
        {
            HomePesquisaViewModel filtro = new HomePesquisaViewModel();

            filtro.ListaPropostas = _Proposta.All().ToList();
            CarregarViewBags(filtro);
            return View(filtro);
        }

        private void CarregarViewBags(HomePesquisaViewModel filtro)
        {

            var dbLista = new ListasRepositorio();
            ViewBag.ListaRegional = dbLista.Regional();
            ViewBag.ListaEmpreendimento = new List<DictionaryEntry>();
            ViewBag.ListaBloco = new List<DictionaryEntry>();
            if (filtro.CodRegional > 0)
            {
                ViewBag.ListaEmpreendimento = dbLista.Empreendimentos(filtro.CodRegional);
            }
            if (filtro.CodEmpreendimento > 0)
            {
                ViewBag.ListaBloco = dbLista.Bloco(filtro.CodEmpreendimento);
            }

        }

        [HttpPost]
        public ActionResult Propostas(HomePesquisaViewModel filtro)
        {

            var all = _Proposta.All();

            if (filtro.CodEmpreendimento > 0)
            {
                all = all.Where(x => x.TabelaUnidade.TabelaBloco.CodEmpreend == filtro.CodEmpreendimento);
            }
            if (filtro.CodBloco > 0)
            {
                all = all.Where(x => x.TabelaUnidade.CodBloco == filtro.CodBloco);
            }
            if (filtro.DataDe.HasValue)
            {
                all = all.Where(x => x.DataCadastro.Date >= filtro.DataDe);
            }
            if (filtro.DataAte.HasValue)
            {
                all = all.Where(x => x.DataCadastro.Date <= filtro.DataAte);
            }

            filtro.ListaPropostas = all.ToList();

            CarregarViewBags(filtro);

            return View(filtro);
        }

        [HttpPost]
        public JsonResult ListarEmpreendimento(int CodRegional)
        {
            var lista = _dbEmpreendimento.All().Where(x => x.CodRegional == CodRegional).ToList();
            lista.Insert(0, new TabelaEmpreendimento());
            var dados = from p in lista select new { Cod = p.Cod, Text = p.NomeEmpreendimento };
            return Json(dados);
        }

        [HttpPost]
        public JsonResult ListarCEPs(int? CodEmpreendimento, int? CodBloco, int? CodRegional, DateTime? DataInicio, DateTime? DataFim)
        {
            var propostas = _Proposta.All().Where(x => x.Status.HasValue);


            var empreendimentos = _dbEmpreendimento.All();


            if (CodEmpreendimento > 0)
            {
                empreendimentos = empreendimentos.Where(x => x.Cod == CodEmpreendimento);
                propostas = propostas.Where(x => x.TabelaUnidade.TabelaBloco.CodEmpreend == CodEmpreendimento);
            }
            if (CodRegional> 0)
            {
                empreendimentos = empreendimentos.Where(x => x.CodRegional == CodRegional);
                propostas = propostas.Where(x => x.TabelaUnidade.TabelaBloco.TabelaEmpreendimento.CodRegional == CodRegional);
            }
            if (CodBloco > 0)
            {   
                propostas = propostas.Where(x => x.TabelaUnidade.CodBloco == CodBloco);
            }


            var lEmpreendimentos = from p in empreendimentos
                                   select new
                                   {
                                       cep = p.TabelaEndereco.CEP,
                                       nome = p.NomeEmpreendimento
                                   };


            var lProposta = from p in propostas
                        select new
                        {
                            cep = p.PropostaProspects.FirstOrDefault().CEP,
                            numero = p.Cod,
                            status = "",//EnumeradoresDescricao.StatusProposta(p.Status),
                            icone = p.Status.HasValue ? p.Status : 0
                        };

            return Json(new {
                ListaProposta = lProposta, ListaEmpreendimentos = lEmpreendimentos
            });
        }

    }
}