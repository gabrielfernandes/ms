﻿using Cobranca.Db.Models;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas;
using Cobranca.Rotinas.BoletoGerar;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Recebiveis.Web.Models;
using Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Cobranca.Web.Controllers
{
    [Authorize]
    public class PesquisaController : Controller
    {
        DateTime? DataNascimento = System.DateTime.Today;
        decimal valor;
        public const string _filtroSessionId = "FiltroPesquisaId";
        public const string _FiltroHomeSession = "FiltroHome";
        public const string _filtroCodClienteAtendimento = "FiltroCodClienteAtendimento";
        public const string _filtroCodContrato = "FiltroCodContrato";
        public const string _filtroCodCliente = "FiltroCodCliente";



        public Repository<TabelaAcaoCobranca> _dbTabelaAcaoCobranca = new Repository<TabelaAcaoCobranca>();
        public Repository<AcordoContratoParcela> _dbAcordoContratoParcela = new Repository<AcordoContratoParcela>();
        public Repository<TabelaTipoBloqueio> _dbTabelaTipoBloqueio = new Repository<TabelaTipoBloqueio>();
        public Repository<ContratoParcela> _dbContratoParcela = new Repository<ContratoParcela>();
        public Repository<TabelaTipoCobrancaAcao> _dbTipoCobrancaAcao = new Repository<TabelaTipoCobrancaAcao>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<ContratoCliente> _dbContratoCliente = new Repository<ContratoCliente>();
        public Repository<TabelaParametroAtendimento> _dbParametroAtendimento = new Repository<TabelaParametroAtendimento>();
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<Contrato> _repoContrato = new Repository<Contrato>();
        public Repository<ContratoHistorico> _dbContratoHistorico = new Repository<ContratoHistorico>();
        public Repository<Historico> _dbHistorico = new Repository<Historico>();
        public Repository<ContratoDocumento> _dbContratoDocumento = new Repository<ContratoDocumento>();
        public Repository<ContratoDespesa> _dbContratoDespesa = new Repository<ContratoDespesa>();
        public Repository<TabelaAging> _dbAging = new Repository<TabelaAging>();
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();
        public Repository<TabelaBloco> _dbBloco = new Repository<TabelaBloco>();
        public Repository<TabelaParametroTaxa> _dbParametroTaxa = new Repository<TabelaParametroTaxa>();
        public Repository<TabelaUnidade> _dbUnidade = new Repository<TabelaUnidade>();
        public Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        public Repository<TabelaEscritorio> _dbEscritorio = new Repository<TabelaEscritorio>();
        public Repository<TabelaFase> _dbFase = new Repository<TabelaFase>();
        public Repository<TabelaSintese> _dbSintese = new Repository<TabelaSintese>();
        public Repository<TabelaTipoCobranca> _dbTipoCobranca = new Repository<TabelaTipoCobranca>();
        public Repository<Usuario> _dbUsuario = new Repository<Usuario>();
        public Repository<TabelaGestao> _dbGestao = new Repository<TabelaGestao>();
        public Repository<ContratoAvalista> _dbContratoAvalista = new Repository<ContratoAvalista>();
        public Repository<TabelaEmpresa> _dbEmpresa = new Repository<TabelaEmpresa>();
        public Repository<TabelaProduto> _dbProduto = new Repository<TabelaProduto>();
        public Repository<ContratoParcelaAcordo> _dbContratoParcelaAcordo = new Repository<ContratoParcelaAcordo>();
        public Repository<TabelaEstado> _dbEstado = new Repository<TabelaEstado>();
        public Repository<ContratoParcelaAcordo> _dbContratoAcordo = new Repository<ContratoParcelaAcordo>();
        public Repository<ContratoTipoCobrancaHistorico> _dbContratoTipoCobrancaHistorico = new Repository<ContratoTipoCobrancaHistorico>();
        public Repository<TabelaGrupo> _dbGrupo = new Repository<TabelaGrupo>();
        public Repository<TabelaBancoPortador> _dbParametroBoleto = new Repository<TabelaBancoPortador>();
        public Repository<TabelaMotivoAtraso> _dbMotivoAtraso = new Repository<TabelaMotivoAtraso>();
        public Repository<ClienteParcelaCredito> _dbClienteCredito = new Repository<ClienteParcelaCredito>();
        public Repository<TabelaSintese> _TabelaSintese = new Repository<TabelaSintese>();
        public Repository<TabelaFase> _TabelaFase = new Repository<TabelaFase>();
        public Repository<TabelaTipoCobranca> _TabelaTipoCobranca = new Repository<TabelaTipoCobranca>();
        public Repository<ContratoAcaoCobrancaHistorico> _dbContratoAcaoCobrancaHistorico = new Repository<ContratoAcaoCobrancaHistorico>();
        public Repository<ClienteAtendimento> atendimento = new Repository<ClienteAtendimento>();
        public Repository<ParametroAtendimentoRota> _dbParametroAtendimentoRota = new Repository<ParametroAtendimentoRota>();
        public Repository<TabelaTipoCadastro> _dbTabelaTipoCadastro = new Repository<TabelaTipoCadastro>();
        public Repository<Acordo> _dbAcordo = new Repository<Acordo>();
        public Repository<ContratoAcaoCobrancaHistorico> _dbAcaoCobrancaHistorico = new Repository<ContratoAcaoCobrancaHistorico>();
        private Repository<UsuarioRegional> _dbUsuarioRegional = new Repository<UsuarioRegional>();
        private Repository<ContratoParcelaBoletoHistorico> _dbBoletoHistorico = new Repository<ContratoParcelaBoletoHistorico>();
        private Repository<ContratoParcelaBoleto> _dbBoleto = new Repository<ContratoParcelaBoleto>();

        public Dados _qry = new Dados();
        public FiltroGeral _filtroRotinas = new FiltroGeral();

        ListasRepositorio _dbListas = new ListasRepositorio();

        [HttpGet]
        public ActionResult Index()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            //return RedirectToAction("Consulta");
            return RedirectToAction("Consulta");

        }

        [HttpGet]
        public ActionResult Consulta()
        {
            PesquisaViewModel model = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            if (model == null)
                model = new PesquisaViewModel();

            PreencherViewBags(model);

            return View(model);
        }

        [HttpGet]
        public ActionResult ConsultaPorCliente()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            PesquisaViewModel model = new PesquisaViewModel();
            model.NomeComprador = "";
            PreencherViewBags(model);
            return View(model);
        }



        private void PreencherViewBags(PesquisaViewModel model)
        {


            List<TabelaBloco> listaBloco = new List<TabelaBloco>();
            listaBloco = _dbBloco.All().OrderBy(x => x.NomeBloco).ToList();
            listaBloco.Insert(0, new TabelaBloco());

            List<Usuario> listaUsuario = new List<Usuario>();
            listaUsuario = _dbUsuario.All().OrderBy(x => x.Nome).ToList();
            listaUsuario.Insert(0, new Usuario());

            List<TabelaRegional> listaRegional = new List<TabelaRegional>();
            listaRegional = _dbRegional.All().ToList();

            foreach (var item in listaRegional)
            {
                item.Regional = DbRotinas.Descriptografar(item.Regional);
            }
            listaRegional.OrderBy(x => x.Regional).ToList();
            listaRegional.Insert(0, new TabelaRegional());

            List<TabelaUnidade> listaUnidade = new List<TabelaUnidade>();
            // listaUnidade = _dbUnidade.All().OrderBy(x => x.NumeroUnidade).ToList();
            listaUnidade.Insert(0, new TabelaUnidade());

            List<TabelaTipoCobranca> listaTipoCobranca = new List<TabelaTipoCobranca>();
            listaTipoCobranca = _dbTipoCobranca.All().Where(x => x.TipoFluxo == 2).ToList();
            foreach (var item in listaTipoCobranca)
            {
                item.TipoCobranca = DbRotinas.Descriptografar(item.TipoCobranca);
            }
            listaTipoCobranca.OrderBy(x => x.TipoCobranca).ToList();
            listaTipoCobranca.Insert(0, new TabelaTipoCobranca());

            List<TabelaEscritorio> listaEscritorio = new List<TabelaEscritorio>();
            listaEscritorio = _dbEscritorio.All().OrderBy(x => x.Escritorio).ToList();
            listaEscritorio.Insert(0, new TabelaEscritorio());

            List<TabelaTipoBloqueio> listaTipoBloqueio = new List<TabelaTipoBloqueio>();
            listaTipoBloqueio = _dbTabelaTipoBloqueio.All().OrderBy(x => x.TipoBloqueio).ToList();
            listaTipoBloqueio.Insert(0, new TabelaTipoBloqueio());

            List<TabelaAging> listaAging = new List<TabelaAging>();
            listaAging = _dbAging.All().OrderBy(x => x.Cod).ToList();
            listaAging.Insert(0, new TabelaAging());

            List<TabelaConstrutora> listaConstrutora = new List<TabelaConstrutora>();
            listaConstrutora = _dbConstrutora.All().OrderBy(x => x.Cod).ToList();
            listaConstrutora.Insert(0, new TabelaConstrutora());

            List<TabelaRegional> listaFilial = new List<TabelaRegional>();
            listaFilial = _dbRegional.All().OrderBy(x => x.Cod).ToList();
            listaFilial.Insert(0, new TabelaRegional());

            List<TabelaProduto> listaProduto = new List<TabelaProduto>();
            listaProduto = _dbProduto.All().OrderBy(x => x.Cod).ToList();
            foreach (var item in listaProduto)
            {
                item.Produto = DbRotinas.Descriptografar(item.Produto);
                item.Descricao = DbRotinas.Descriptografar(item.Descricao);
            }
            listaProduto.OrderBy(x => x.Descricao).ToList();

            ViewBag.ListaBloqueio = from p in listaTipoBloqueio.ToList() select new DictionaryEntry(p.TipoBloqueio, p.Cod);

            listaProduto.Insert(0, new TabelaProduto());

            ViewBag.ListaRegional = from p in listaFilial.ToList() select new DictionaryEntry(p.Regional, p.Cod);

            ViewBag.ListaConstrutora = _dbListas.Construtoras();

            ViewBag.ListaProduto = from p in listaProduto.ToList() select new DictionaryEntry($"{p.Produto} - {p.Descricao}", p.Cod);
            //ViewBag.ListaEmpreendimento = _dbListas.Empreendimentos().Take(20);
            // ViewBag.ListaBloco = _dbListas.Bloco();
            ViewBag.ListaAging = from p in listaAging select new DictionaryEntry(p.Aging, p.Cod);
            // ViewBag.ListaUnidade = from p in listaUnidade select new DictionaryEntry(p.NumeroUnidade, p.Cod);
            ViewBag.ListaUsuario = from p in listaUsuario select new DictionaryEntry(p.Nome, p.Cod);
            ViewBag.ListaRegional = from p in listaRegional select new DictionaryEntry(p.Regional, p.Cod);
            ViewBag.ListaEscritorio = from p in listaEscritorio select new DictionaryEntry(p.Escritorio, p.Cod);
            ViewBag.ListaTipoCobranca = from p in listaTipoCobranca select new DictionaryEntry(p.TipoCobranca, p.Cod);
            ViewBag.Lista = new List<DictionaryEntry>();

            List<TabelaGrupo> listaGrupo = new List<TabelaGrupo>();
            listaGrupo = _dbGrupo.All().OrderBy(x => x.Nome).ToList();
            listaGrupo.Insert(0, new TabelaGrupo());
            ViewBag.ListaGrupo = from p in listaGrupo.OrderBy(x => x.Nome).ToList() select new DictionaryEntry(p.Nome, p.Cod);

        }


        [HttpGet]
        public ActionResult ResultadoCliente()
        {
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);

            var resultado = new List<ContratoDados>();

            return View(resultado);
        }


        [HttpPost]
        public ActionResult ResultadoCliente(FiltroGeralDados filtro)
        {

            filtro = _filtroRotinas.TratarFiltroGeral(filtro);
            if (filtro.NumeroContrato != null)
            {
                filtro.CodContratos = _dbContrato.All().Where(x => filtro.NumerosContratosCriptografadoArray.Any(key => x.NumeroContrato.Contains(key))).Select(x => x.Cod).ToList();
            }
            if (filtro.NumeroFatura != null)
            {
                filtro.CodParcelas = _dbContratoParcela.All().Where(x => filtro.NumerosFaturasArray.Any(key => x.FluxoPagamento.Contains(key))).Select(x => x.Cod).ToList();
            }
            if (filtro.NotaFiscal != null)
            {
                filtro.CodParcelas = _dbContratoParcela.All().Where(x => filtro.NotaFiscaisArray.Any(key => x.NumeroNotaFiscal.Contains(key))).Select(x => x.Cod).ToList();
            }
            WebRotinas.FiltroGeralGravar(filtro);

            var resultado = new List<ContratoDados>();

            return View(resultado);
        }

        public ActionResult ResultadoParcela()
        {
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);

            var resultado = new List<ContratoDados>();

            return View(resultado);
        }
        [HttpPost]
        public ActionResult ResultadoParcela(FiltroGeralDados filtro)
        {
            var filter = WebRotinas.FiltroGeralObter();
            filtro.CodUsuarioAtendimento = filter.CodUsuarioAtendimento;
            WebRotinas.FiltroGeralGravar(filtro);
            var resultado = new List<ContratoDados>();

            return View(resultado);
        }

        private List<Contrato> CalcularAging(List<Contrato> dados)
        {
            //foreach (var item in dados)
            //{
            //    int diasAtraso = item.ContratoParcelas.Sum(x => x.DiasAtraso);
            //    item.Aging = _dbAging.FindOne(x => diasAtraso >= x.AtrasoDe && diasAtraso <= x.AtrasoAte);
            //    if (item.Aging == null)
            //    {
            //        item.Aging = new TabelaAging();
            //    }

            //}
            return dados;

        }

        private List<Contrato> FiltrarDados(IQueryable<Contrato> dados, PesquisaViewModel filtro)
        {
            var resultado = dados.ToList();

            if (filtro.CodConstrutora > 0)
            {
                resultado = resultado.Where(x => x.TabelaUnidade.TabelaBloco.TabelaEmpreendimento.CodConstrutora == filtro.CodConstrutora).ToList();
            }
            if (filtro.CodEmpreendimento > 0)
            {
                resultado = resultado.Where(x => x.TabelaUnidade.TabelaBloco.CodEmpreend == filtro.CodEmpreendimento).ToList();
            }
            if (filtro.CodRegional > 0)
            {
                resultado = resultado.Where(x => x.TabelaUnidade.TabelaBloco.TabelaEmpreendimento.CodRegional == filtro.CodRegional).ToList();
            }
            if (filtro.CodBloco > 0)
            {
                resultado = resultado.Where(x => x.TabelaUnidade.CodBloco == filtro.CodBloco).ToList();
            }
            if (filtro.CodUnidade > 0)
            {
                resultado = resultado.Where(x => x.CodUnidade == filtro.CodUnidade).ToList();
            }
            if (filtro.CodDonoCarteira > 0)
            {
                resultado = resultado.Where(x => x.CodUsuarioDonoCarteira == filtro.CodDonoCarteira).ToList();
            }
            if (filtro.CodTipoCobranca > 0)
            {
                resultado = resultado.Where(x => x.CodTipoCobranca == filtro.CodTipoCobranca).ToList();
            }
            if (filtro.CodGestao > 0)
            {
                resultado = resultado.Where(x => x.CodGestao == filtro.CodGestao).ToList();
            }
            if (filtro.CodSintese > 0)
            {
                resultado = resultado.Where(c => c.ContratoHistoricoes.Any() && c.ContratoHistoricoes.LastOrDefault().CodSintese == filtro.CodSintese).ToList();
            }
            if (filtro.CodEscritorio > 0)
            {
                resultado = resultado.Where(x => x.CodEscritorio == filtro.CodEscritorio).ToList();
            }
            if (filtro.CodAging > 0)
            {
                resultado = resultado.Where(x => x.CodAging == filtro.CodAging).ToList();
            }

            if (filtro.Bloqueado == true)
            {
                resultado = resultado.Where(x => x.Bloqueado == filtro.Bloqueado).ToList();
            }
            if (!String.IsNullOrEmpty(filtro.NomeComprador))
            {
                resultado = resultado.Where(x => x.ContratoClientes.Any(c => !c.DataExcluido.HasValue && c.Cliente.Nome.ToUpper().Contains(filtro.NomeComprador.ToUpper()))).ToList();
            }
            if (!String.IsNullOrEmpty(filtro.CPF_CNPJ))
            {
                resultado = resultado.Where(x => x.ContratoClientes.Any(c => !c.DataExcluido.HasValue && c.Cliente.ClienteDocumentoes.Any(d => d.NumeroDocumento.Contains(filtro.CPF_CNPJ)))).ToList();
            }
            if (!String.IsNullOrEmpty(filtro.CodigoVenda))
            {
                resultado = resultado.Where(x => x.NumeroContrato.Contains(filtro.CodigoVenda)).ToList();
            }
            if (filtro.StatusUnidade > 0)
            {
                resultado = resultado.Where(x => x.TabelaUnidade.Status == filtro.StatusUnidade).ToList();
            }

            return resultado.Take(150).ToList();
        }
        [HttpGet]
        public ActionResult Contrato(int id = 0, bool scopeInframe = false)
        {
            ViewBag.Iframe = scopeInframe;
            List<TabelaConstrutora> listaConstrutora = new List<TabelaConstrutora>();
            listaConstrutora = _dbConstrutora.All().OrderBy(x => x.NomeConstrutora).ToList();
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            List<TabelaRegional> listaRegional = new List<TabelaRegional>();
            listaRegional = _dbRegional.All().OrderBy(x => x.Regional).ToList();
            listaRegional.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaRegional.ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.Regional), p.Cod);

            List<TabelaProduto> listaProduto = new List<TabelaProduto>();
            listaProduto = _dbProduto.All().OrderBy(x => x.Cod).ToList();
            listaProduto.Insert(0, new TabelaProduto());
            ViewBag.ListaProduto = from p in listaProduto.ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.Descricao), p.Cod);

            List<TabelaGestao> listaGestao = new List<TabelaGestao>();
            listaGestao = _dbGestao.All().OrderBy(x => x.NomeGestao).ToList();
            listaGestao.Insert(0, new TabelaGestao());
            ViewBag.ListaGestao = from p in listaGestao select new DictionaryEntry(p.NomeGestao, p.Cod);

            List<TabelaEmpresa> listaEmpresa = new List<TabelaEmpresa>();
            listaEmpresa = _dbEmpresa.All().OrderBy(x => x.Empresa).ToList();
            listaEmpresa.Insert(0, new TabelaEmpresa());
            ViewBag.ListaEmpresa = from p in listaEmpresa select new DictionaryEntry(p.Empresa, p.Cod);

            List<Usuario> listaUsuarioDonoCarteira = new List<Usuario>();
            listaUsuarioDonoCarteira = _dbUsuario.All().Where(x => x.DonoDeCarteira == true).OrderBy(x => x.Nome).ToList();
            listaUsuarioDonoCarteira.Insert(0, new Usuario());
            ViewBag.ListaUsuarioDonoCarteira = from p in listaUsuarioDonoCarteira select new DictionaryEntry(p.Nome, p.Cod);

            List<TabelaEscritorio> listaEscritorio = new List<TabelaEscritorio>();
            listaEscritorio = _dbEscritorio.All().OrderBy(x => x.Escritorio).ToList();
            listaEscritorio.Insert(0, new TabelaEscritorio());

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Em processamento", Value = "3" });
            items.Add(new SelectListItem { Text = "Positiva", Value = "1" });
            items.Add(new SelectListItem { Text = "Negativa", Value = "2" });
            ViewBag.Notificacao = from p in items select new DictionaryEntry(p.Text, p.Value);

            ViewBag.ListaTipoCobranca = _dbListas.TipoCobranca();
            ViewBag.ListaTipoBloqueio = _dbListas.TipoBloqueio();
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato);
                if (id > 0)
                {
                    return RedirectToAction("Contrato", new { id = id });
                }
            }

            var dados = _dbContrato.FindById(id);

            if (dados == null)
                dados = new Db.Models.Contrato();

            return View(dados);
        }
        [HttpGet]
        public ActionResult ContratoPorCliente(int id = 0)
        {
            var cdCliente = 0;
            if (cdCliente == 0) { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }
            if (cdCliente == 0) { return RedirectToAction("ConsultaPorCliente"); }
            ViewBag.Cliente = _dbCliente.FindById(cdCliente);



            if (id == 0) { id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato); }

            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).ToList();

            foreach (var item in contratos)
            {
                item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
            }

            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var codSintese = filtro.CodSintese;
            if (codSintese > 0)
            {
                contratos = contratos.Where(x => x.CodHistorico.HasValue).ToList();
                contratos = (from p in contratos
                             join h in contratos.Select(x => x.Historico).ToList() on p.CodHistorico equals h.Cod
                             where (h.CodSintese == codSintese || codSintese == 0)
                             select p).ToList();
            }
            var codSinteseParcela = filtro.CodSinteseParcela;
            ViewBag.CodSinteseParcela = filtro.CodSinteseParcela;
            if (codSinteseParcela > 0)
            {
                List<int> contratoIds = contratos.Select(x => x.Cod).ToList();
                List<ContratoParcela> parcelas = _dbContratoParcela.FindAll(x => !x.DataExcluido.HasValue && contratoIds.Contains((int)x.CodContrato) && x.CodHistorico > 0).ToList();
                contratos = parcelas.Where(x => x.Historico.CodSintese == codSinteseParcela).GroupBy(x => x.Contrato).Select(x => x.Key).ToList();

                foreach (var item in contratos)
                {
                    item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
                }
            }

            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            if (usuarioLogado.CodRegional > 0)
            {
                contratos = (from p in contratos
                             where p.TabelaRegionalProduto.CodRegional == usuarioLogado.CodRegional
                             select p).ToList();
            }


            contratos.Insert(0, new Contrato());
            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.Contratos = listaContratos.ToList();

            List<TabelaConstrutora> listaConstrutora = new List<TabelaConstrutora>();
            listaConstrutora = _dbConstrutora.All().OrderBy(x => x.NomeConstrutora).ToList();
            foreach (var item in listaConstrutora)
            {
                item.NomeConstrutora = DbRotinas.Descriptografar(item.NomeConstrutora);
                item.CNPJ = DbRotinas.Descriptografar(item.CNPJ);
            }
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            List<TabelaGestao> listaGestao = new List<TabelaGestao>();
            listaGestao = _dbGestao.All().OrderBy(x => x.NomeGestao).ToList();
            listaGestao.Insert(0, new TabelaGestao());
            ViewBag.ListaGestao = from p in listaGestao select new DictionaryEntry(p.NomeGestao, p.Cod);

            List<TabelaEmpresa> listaEmpresa = new List<TabelaEmpresa>();
            listaEmpresa = _dbEmpresa.All().OrderBy(x => x.Empresa).ToList();
            listaEmpresa.Insert(0, new TabelaEmpresa());
            ViewBag.ListaEmpresa = from p in listaEmpresa select new DictionaryEntry(p.Empresa, p.Cod);

            List<TabelaRegional> lRegional = new List<TabelaRegional>();
            lRegional = _dbRegional.All().ToList();
            foreach (var item in lRegional)
            {
                item.Regional = DbRotinas.Descriptografar(item.Regional);
            }
            var listaRegional = lRegional.OrderBy(x => x.Regional).ToList();
            listaRegional.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaRegional select new DictionaryEntry(p.Regional, p.Cod);

            List<TabelaProduto> lProduto = new List<TabelaProduto>();
            lProduto = _dbProduto.All().ToList();
            foreach (var item in lProduto)
            {
                item.Produto = DbRotinas.Descriptografar(item.Produto);
                item.Descricao = DbRotinas.Descriptografar(item.Descricao);
            }
            var listaProduto = lProduto.OrderBy(x => x.Descricao).ToList();
            listaProduto.Insert(0, new TabelaProduto());
            ViewBag.ListaProduto = from p in listaProduto select new DictionaryEntry(p.Descricao, p.Cod);

            List<Usuario> listaUsuarioDonoCarteira = new List<Usuario>();
            listaUsuarioDonoCarteira = _dbUsuario.All().Where(x => x.DonoDeCarteira == true).OrderBy(x => x.Nome).ToList();
            listaUsuarioDonoCarteira.Insert(0, new Usuario());
            ViewBag.ListaUsuarioDonoCarteira = from p in listaUsuarioDonoCarteira select new DictionaryEntry(p.Nome, p.Cod);

            List<TabelaEscritorio> listaEscritorio = new List<TabelaEscritorio>();
            listaEscritorio = _dbEscritorio.All().OrderBy(x => x.Escritorio).ToList();
            listaEscritorio.Insert(0, new TabelaEscritorio());

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Em processamento", Value = "3" });
            items.Add(new SelectListItem { Text = "Positiva", Value = "1" });
            items.Add(new SelectListItem { Text = "Negativa", Value = "2" });
            ViewBag.Notificacao = from p in items select new DictionaryEntry(p.Text, p.Value);

            ViewBag.ListaTipoCobranca = _dbListas.TipoCobranca();
            ViewBag.ListaTipoBloqueio = _dbListas.TipoBloqueio();

            Contrato dados = new Contrato();
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato);
                if (id > 0)
                {
                    return RedirectToAction("Contrato", new { id = id });
                }
                else
                {
                    dados = _dbContrato.FindById(_dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).First().Cod);
                }
            }
            else
            {
                dados = _dbContrato.FindById(id);
                WebRotinas.CookieObjetoGravar(_filtroCodContrato, id);
            }

            return View(dados);
        }

        [HttpPost]
        public ActionResult ContratoPorCliente(Contrato dados)
        {

            var baseEntity = _dbContrato.FindById(dados.Cod);
            baseEntity.TabelaUnidade.TabelaBloco.CodEmpreend = dados.TabelaUnidade.TabelaBloco.CodEmpreend;
            _dbContrato.CreateOrUpdate(baseEntity);
            return View(dados);
        }

        [HttpGet]
        public ActionResult ClienteGeral(int id = 0, int CodCliente = 0)
        {
            Repository<Contrato> contrato = new Repository<Contrato>();
            Repository<Cliente> cliente = new Repository<Cliente>();
            DeParaModeloAcao depara = new DeParaModeloAcao();
            Dados _sql = new Dados();

            if (CodCliente == 0)
            {
                CodCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente);
                if (CodCliente > 0)
                {
                    return RedirectToAction("ClienteGeral", new { id = id, CodCliente = CodCliente });
                }
                else
                {
                    return RedirectToAction("ConsultaPorCliente");
                }
            }
            var dados = _dbCliente.FindById(CodCliente);
            ViewBag.ClienteContatos = dados.ClienteContatoes.Select(x => new ClienteContato
            {
                Cod = x.Cod,
                Contato = DbRotinas.Descriptografar(x.Contato),
                Tipo = x.Tipo,
                DataCadastro = x.DataCadastro,
                DataExcluido = x.DataExcluido,
                DataAlteracao = x.DataAlteracao,
                CodCliente = x.CodCliente,
                ContatoHOT = x.ContatoHOT,
                ContatoHOTCodUsuario = x.ContatoHOTCodUsuario,
                ContatoHOTData = x.ContatoHOTData,
                NomeContato = x.NomeContato,
                CodImportacao = x.CodImportacao,
                CodTipoCadastro = x.CodTipoCadastro,
                Descricao = x.Descricao

            }).Where(x => !x.DataExcluido.HasValue).ToList();
            ViewBag.ClienteTabelaEnderecos = dados.ClienteEnderecoes.Where(x => x.CodEndereco.HasValue).Select(x => new ClienteEndereco
            {
                Cod = x.TabelaEndereco.Cod,
                TipoEndereco = x.TipoEndereco,
                TabelaEndereco = x.TabelaEndereco,
                Contrato = x.Contrato

            }).ToList();
            var lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = null, Value = null });
            var ListaContatoCadastro = _dbTabelaTipoCadastro.All().ToList();
            foreach (var item in ListaContatoCadastro)
            {
                lista.Add(new DictionaryEntry() { Key = item.Cod, Value = item.Descricao });
            }
            ViewBag.dropTipoCadastro = lista.ToList();
            ViewBag.dropTipoContato = EnumeradoresLista.ContatoTipo();

            if (dados == null)
                dados = new Db.Models.Cliente();

            return View(dados);
        }
        [HttpGet]
        public ActionResult AlterarCliente(int idCliente = 0)
        {
            Repository<Contrato> contrato = new Repository<Contrato>();
            Repository<Cliente> cliente = new Repository<Cliente>();
            DeParaModeloAcao depara = new DeParaModeloAcao();
            Dados _sql = new Dados();
            if (idCliente == 0)
            {
                idCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente);
            }
            var dados = _dbCliente.FindById(idCliente);
            ViewBag.ClienteContatos = dados.ClienteContatoes.Select(x => new ClienteContato
            {
                Cod = x.Cod,
                Contato = DbRotinas.Descriptografar(x.Contato),
                Tipo = x.Tipo,
                DataCadastro = x.DataCadastro,
                DataExcluido = x.DataExcluido,
                DataAlteracao = x.DataAlteracao,
                CodCliente = x.CodCliente,
                ContatoHOT = x.ContatoHOT,
                ContatoHOTCodUsuario = x.ContatoHOTCodUsuario,
                ContatoHOTData = x.ContatoHOTData,
                NomeContato = x.NomeContato,
                CodImportacao = x.CodImportacao,
                CodTipoCadastro = x.CodTipoCadastro,
                Descricao = x.Descricao

            }).Where(x => !x.DataExcluido.HasValue).ToList();
            ViewBag.ClienteTabelaEnderecos = dados.ClienteEnderecoes.Where(x => x.CodEndereco.HasValue).Select(x => new ClienteEndereco
            {
                Cod = x.TabelaEndereco.Cod,
                TipoEndereco = x.TipoEndereco,
                TabelaEndereco = x.TabelaEndereco,
                Contrato = x.Contrato

            }).ToList();
            var lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = null, Value = null });
            var ListaContatoCadastro = _dbTabelaTipoCadastro.All().ToList();
            foreach (var item in ListaContatoCadastro)
            {
                lista.Add(new DictionaryEntry() { Key = item.Cod, Value = item.Descricao });
            }
            ViewBag.dropTipoCadastro = lista.ToList();
            ViewBag.dropTipoContato = EnumeradoresLista.ContatoTipo();

            if (dados == null)
                dados = new Db.Models.Cliente();
            dados.CPF = DbRotinas.Descriptografar(dados.CPF);
            dados.Nome = DbRotinas.Descriptografar(dados.Nome);
            dados.CodCliente = DbRotinas.Descriptografar(dados.CodCliente);
            return PartialView("_cliente", dados);
        }

        [HttpGet]
        public ActionResult AdicionarContatoForm(int idCliente = 0)
        {
            ClienteContato model = new ClienteContato();
            model.CodCliente = idCliente;
            var lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = null, Value = null });
            var ListaContatoCadastro = _dbTabelaTipoCadastro.All().ToList();
            foreach (var item in ListaContatoCadastro)
            {
                lista.Add(new DictionaryEntry() { Key = item.Cod, Value = item.Descricao });
            }
            ViewBag.dropTipoCadastro = lista.ToList();
            ViewBag.dropTipoContato = EnumeradoresLista.ContatoTipo();
            return PartialView("_contato", model);
        }
        //[HttpGet]
        //public ActionResult Cliente(int CodCliente = 0)
        //{
        //    Repository<Cliente> cliente = new Repository<Cliente>();
        //    var dados = _dbCliente.FindById(CodCliente);
        //    ViewBag.ClienteContatos = dados.ClienteContatoes.Select(x => new ClienteContato
        //    {
        //        Cod = x.Cod,
        //        Contato = DbRotinas.Descriptografar(x.Contato),
        //        Tipo = x.Tipo,
        //        DataCadastro = x.DataCadastro,
        //        DataExcluido = x.DataExcluido,
        //        DataAlteracao = x.DataAlteracao,
        //        CodCliente = x.CodCliente,
        //        ContatoHOT = x.ContatoHOT,
        //        ContatoHOTCodUsuario = x.ContatoHOTCodUsuario,
        //        ContatoHOTData = x.ContatoHOTData,
        //        NomeContato = x.NomeContato,
        //        CodImportacao = x.CodImportacao,
        //        CodTipoCadastro = x.CodTipoCadastro,
        //        Descricao = x.Descricao

        //    }).Where(x => !x.DataExcluido.HasValue).ToList();
        //    ViewBag.ClienteTabelaEnderecos = dados.ClienteEnderecoes.Where(x => x.CodEndereco.HasValue).Select(x => new ClienteEndereco
        //    {
        //        Cod = x.TabelaEndereco.Cod,
        //        TipoEndereco = x.TipoEndereco,
        //        TabelaEndereco = x.TabelaEndereco,
        //        Contrato = x.Contrato

        //    }).ToList();
        //    var lista = new List<DictionaryEntry>();
        //    lista.Add(new DictionaryEntry() { Key = null, Value = null });
        //    var ListaContatoCadastro = _dbTabelaTipoCadastro.All().ToList();
        //    foreach (var item in ListaContatoCadastro)
        //    {
        //        lista.Add(new DictionaryEntry() { Key = item.Cod, Value = item.Descricao });
        //    }
        //    ViewBag.dropTipoCadastro = lista.ToList();
        //    ViewBag.dropTipoContato = EnumeradoresLista.ContatoTipo();

        //    if (dados == null)
        //        dados = new Db.Models.Cliente();
        //    dados.CPF = Cobranca.Db.Rotinas.DbRotinas.Descriptografar(dados.CPF);
        //    dados.Nome = Cobranca.Db.Rotinas.DbRotinas.Descriptografar(dados.Nome);
        //    dados.CodCliente = Cobranca.Db.Rotinas.DbRotinas.Descriptografar(dados.CodCliente);
        //    dados.Email = Cobranca.Db.Rotinas.DbRotinas.Descriptografar(dados.Email);
        //    dados.TelefoneComercial = Cobranca.Db.Rotinas.DbRotinas.Descriptografar(dados.TelefoneComercial);
        //    dados.TelefoneResidencial = Cobranca.Db.Rotinas.DbRotinas.Descriptografar(dados.TelefoneResidencial);
        //    dados.Celular = Cobranca.Db.Rotinas.DbRotinas.Descriptografar(dados.Celular);
        //    dados.CelularComercial = Cobranca.Db.Rotinas.DbRotinas.Descriptografar(dados.CelularComercial);
        //    dados.CelularPrincipal = Cobranca.Db.Rotinas.DbRotinas.Descriptografar(dados.CelularPrincipal);
        //    return PartialView("_cliente", dados);
        //}

        [HttpGet]
        public ActionResult CobrancaAcao()
        {
            _qry.executarProcessarAcaoCobranca(); // Processa o historico Acoes Cobrança

            var listaCobrancaAcao = _dbTabelaAcaoCobranca.All().ToList();//Lista de Tipos de Ação ex: Não acusamos o recebimento (leve)
            var listaCobranca = _dbTipoCobranca.All().ToList(); // Lista de Tipos de Cobrança ex: 01 até 30 dias
            listaCobrancaAcao.Insert(0, new TabelaAcaoCobranca() { AcaoCobranca = "--Todos--" });
            listaCobranca.Insert(0, new TabelaTipoCobranca() { TipoCobranca = "--Todos--" });

            var selectListCobrancaAcao = from p in listaCobrancaAcao select new { Cod = p.Cod, Text = p.AcaoCobranca };
            var selectListCobranca = from p in listaCobranca select new { Cod = p.Cod, Text = DbRotinas.Descriptografar(p.TipoCobranca) };

            ViewBag.ListaTipoAcaoCobranca = EnumeradoresLista.TipoAcaoCobranca();
            ViewBag.ListaTipoSituacaoStatus = EnumeradoresLista.SituacaoHistoricoAcaoCobranca();
            ViewBag.ListaTipoCobranca = selectListCobranca;
            ViewBag.ListaTipoCobrancaAcao = selectListCobrancaAcao;

            ViewBag.ListaTipoBloqueio = _dbListas.TipoBloqueio();

            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var resultado = new List<Db.Models.Pesquisa.ContratoDados>();
            return View(resultado);
        }

        [HttpPost]
        public ActionResult CobrancaAcao(PesquisaViewModel filtro)
        {

            var listaCobrancaAcao = _dbTabelaAcaoCobranca.All().Where(x => x.Tipo == filtro.TipoAcaoCobranca).ToList();//Lista de Tipos de Ação ex: Não acusamos o recebimento (leve)
            var listaCobranca = _dbTipoCobranca.All().ToList(); // Lista de Tipos de Cobrança ex: 01 até 30 dias
            listaCobrancaAcao.Insert(0, new TabelaAcaoCobranca() { AcaoCobranca = "--Todos--" });
            listaCobranca.Insert(0, new TabelaTipoCobranca() { TipoCobranca = "" });

            var selectListCobrancaAcao = from p in listaCobrancaAcao select new { Cod = p.Cod, Text = p.AcaoCobranca };
            var selectListCobranca = from p in listaCobranca select new { Cod = p.Cod, Text = DbRotinas.Descriptografar(p.TipoCobranca) };

            ViewBag.ListaTipoAcaoCobranca = EnumeradoresLista.TipoAcaoCobranca();
            ViewBag.ListaTipoSituacaoStatus = EnumeradoresLista.SituacaoHistoricoAcaoCobranca();
            ViewBag.ListaTipoCobranca = selectListCobranca;
            ViewBag.ListaTipoCobrancaAcao = selectListCobrancaAcao;

            ViewBag.ListaTipoBloqueio = _dbListas.TipoBloqueio();

            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            //ObterDadosSession(true);
            var resultado = new List<Db.Models.Pesquisa.ContratoDados>();


            ViewBag.Filtro = filtro;

            return View(resultado);
        }

        [HttpPost]
        public JsonResult ListarAcaoPorCobranca(int CodTipoCobrancaAcao = 0)
        {

            var listaAcaoCobranca = _dbTipoCobranca.All().Where(x => x.TabelaTipoCobrancaAcaos.Any(a => a.CodAcaoCobranca == CodTipoCobrancaAcao || CodTipoCobrancaAcao == 0)).ToList();
            listaAcaoCobranca.Insert(0, new TabelaTipoCobranca() { TipoCobranca = "" });
            var selectListAcao = from p in listaAcaoCobranca select new { Cod = p.Cod, Text = DbRotinas.Descriptografar(p.TipoCobranca) };


            return Json(selectListAcao);
        }

        //Carreagar Tipos de Ação por Tipo (Sms,carta,Email ... etc)
        [HttpPost]
        public JsonResult ListarAcaoPorTipo(int TipoAcaoCobranca = 0)
        {
            var listaCobrancaAcao = _dbTabelaAcaoCobranca.All().Where(x => x.Tipo == TipoAcaoCobranca || TipoAcaoCobranca == 0).ToList();
            var selectListCobrancaAcao = from p in listaCobrancaAcao select new { Cod = p.Cod, Text = p.AcaoCobranca };
            listaCobrancaAcao.Insert(0, new TabelaAcaoCobranca() { AcaoCobranca = "--Todos--" });

            return Json(selectListCobrancaAcao);
        }

        [HttpPost]
        public JsonResult ListarTipoAcaoCobranca(int CodTipoAcaoCobranca = 0)
        {
            var listaAcaoCobranca = _dbTabelaAcaoCobranca.All().Where(x => x.Cod == CodTipoAcaoCobranca).ToList();
            var selectListAcao = from p in listaAcaoCobranca select new { tipo = p.Tipo, Text = p.AcaoCobranca };
            return Json(selectListAcao);
        }

        public List<DictionaryEntry> Fases(int CodTipoCobranca = 0)
        {

            //int CodTipoCobranca .Where(x=>x.CodTipoCobranca == CodTipoCobranca)
            List<TabelaFase> lista = new List<TabelaFase>();
            lista = _dbFase.All().Where(x => x.CodTipoCobranca == CodTipoCobranca).ToList();
            foreach (var item in lista)
            {
                item.Fase = DbRotinas.Descriptografar(item.Fase);
            }
            lista.OrderBy(x => x.Ordem).ToList();
            lista.Insert(0, new TabelaFase());
            var list = from p in lista select new DictionaryEntry(p.Fase, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> Sintese(int CodFase = 0)
        {

            List<TabelaSintese> lista = new List<TabelaSintese>();
            lista = _dbSintese.All().Where(x => x.CodFase == CodFase).ToList();
            foreach (var item in lista)
            {
                item.Sintese = DbRotinas.Descriptografar(item.Sintese);
            }
            lista = lista.OrderBy(x => x.Ordem).ToList();
            lista.Insert(0, new TabelaSintese());
            var list = from p in lista select new DictionaryEntry(p.Sintese, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> SiglaUF()
        {
            List<TabelaEstado> lista = new List<TabelaEstado>();
            lista = _dbEstado.All().OrderBy(x => x.Sigla).ToList();
            lista.Insert(0, new TabelaEstado());
            var list = from p in lista select new DictionaryEntry(p.Sigla, p.Cod);
            return list.ToList();
        }

        [HttpGet]
        public ActionResult HistoricoCliente(int id = 0)
        {
            var cdCliente = 0;
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente);
                if (id > 0)
                {
                    return RedirectToAction("HistoricoCliente", new { id = id });
                }
            }
            else
            {
                WebRotinas.CookieObjetoGravar(_filtroCodCliente, id);
            }
            if (cdCliente == 0)
            {

                if (id > 0)
                {
                    cdCliente = id;
                }
                else
                {
                    cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente);
                }
            }
            var dados = _dbCliente.FindById(cdCliente);



            var contratoAcaoCobrancaHistorico = _dbContratoAcaoCobrancaHistorico.All().Where(x =>
            x.Contrato.ContratoClientes.Any(c =>
            !c.DataExcluido.HasValue
            && c.CodCliente == cdCliente) && x.DataEnvio.HasValue && x.SituacaoTipo != 2).ToList();
            ViewBag.historicosAcaoCobrancaCLiente = contratoAcaoCobrancaHistorico;

            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var codSintese = filtro.CodSintese;
            ViewBag.FiltroSintese = filtro.CodSintese;

            var CodSinteseParcela = filtro.CodSinteseParcela;
            ViewBag.FiltroSinteseParcela = filtro.CodSinteseParcela;
            var historicosCliente = _qry.ObterHistoricoPorCliente(cdCliente, codSintese).ToList();
            List<ContratoHistorico> contratoHistorico = new List<ContratoHistorico>();
            foreach (var hItem in historicosCliente)
            {
                contratoHistorico.Add(_dbContratoHistorico.FindById(hItem.Cod));
            }

            ViewBag.historicosCLiente = contratoHistorico.ToList();

            var CodTipoCobranca = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).FirstOrDefault().CodTipoCobranca;
            var historico = contratoHistorico.FirstOrDefault();

            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).ToList();
            foreach (var item in contratos)
            {
                item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
            }
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();




            if (codSintese > 0)
            {
                contratos = contratos.Where(x => x.CodHistorico.HasValue).ToList();
                contratos = (from p in contratos
                             join h in contratos.Select(x => x.Historico).ToList() on p.CodHistorico equals h.Cod
                             where (h.CodSintese == codSintese || codSintese == 0)
                             select p).ToList();
            }

            var codSinteseParcela = filtro.CodSinteseParcela;
            ViewBag.CodSinteseParcela = filtro.CodSinteseParcela;
            if (codSinteseParcela > 0)
            {
                List<int> contratoIds = contratos.Select(x => x.Cod).ToList();
                List<ContratoParcela> parcelas = _dbContratoParcela.FindAll(x => !x.DataExcluido.HasValue && contratoIds.Contains((int)x.CodContrato) && x.CodHistorico > 0).ToList();
                contratos = parcelas.Where(x => x.Historico.CodSintese == codSinteseParcela).GroupBy(x => x.Contrato).Select(x => x.Key).ToList();

                foreach (var item in contratos)
                {
                    item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
                }
            }

            if (usuarioLogado.CodRegional > 0)
            {
                contratos = (from p in contratos
                             where p.TabelaRegionalProduto.CodRegional == usuarioLogado.CodRegional
                             select p).ToList();
            }
            contratos.Insert(0, new Contrato());
            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.Contratos = listaContratos.ToList();

            ViewBag.ListaFases = Fases(Convert.ToInt32(CodTipoCobranca));
            ViewBag.ListaMotivoAtraso = _dbListas.MotivoAtraso();
            ViewBag.ListaResolucao = _dbListas.Resulucao();
            if (historico != null)
            {
                var sintese = _TabelaSintese.FindById((int)historico.CodSintese);
                ViewBag.CodSintese = sintese.Cod;
                ViewBag.CodFase = sintese.CodFase;
                ViewBag.ListaSintese = Sintese(sintese.CodFase);

            }
            else
            {
                ViewBag.ListaSintese = new List<DictionaryEntry>();
            }

            var listHistorico = _qry.ListarHistoricoCliente(cdCliente);
            ViewBag.HitoricosTimeline = listHistorico.OrderByDescending(x => x.DataCadastro).ToList();
            if (dados == null)
                dados = new Db.Models.Cliente();

            return View(dados);
        }

        [HttpGet]
        public ActionResult ParcelaClienteAtrasada(int id = 0)
        {
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente);
                if (id > 0)
                {
                    return RedirectToAction("ParcelaClienteAtrasada", new { id = id });
                }
            }
            else { WebRotinas.CookieObjetoGravar(_filtroCodCliente, id); }
            var cdCliente = 0;
            if (id > 0) { cdCliente = id; } else { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }
            ViewBag.Cliente = _dbCliente.FindById(cdCliente);

            var dados = _dbCliente.FindById(cdCliente);

            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).ToList();
            foreach (var item in contratos)
            {
                item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
            }
            ViewBag.Contratos = contratos.ToList();
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            if (usuarioLogado.CodRegional > 0)
            {
                contratos = (from p in contratos
                             where p.TabelaRegionalProduto.CodRegional == usuarioLogado.CodRegional
                             select p).ToList();
            }
            contratos.Insert(0, new Contrato() { NumeroContrato = "--Todos--" });
            //FILTRAS OS CONTRATOS DA DETERMINADA SINTESE
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var codSintese = filtro.CodSintese;
            ViewBag.CodSinteseContrato = codSintese;
            if (codSintese > 0)
            {
                contratos = contratos.Where(x => x.CodHistorico.HasValue).ToList();
                contratos = (from p in contratos
                             join h in contratos.Select(x => x.Historico).ToList() on p.CodHistorico equals h.Cod
                             where (h.CodSintese == codSintese || codSintese == 0)
                             select p).ToList();
            }
            var codSinteseParcela = filtro.CodSinteseParcela;
            ViewBag.CodSinteseParcela = filtro.CodSinteseParcela;
            if (codSinteseParcela > 0)
            {
                List<int> contratoIds = contratos.Select(x => x.Cod).ToList();
                List<ContratoParcela> parcelas = _dbContratoParcela.FindAll(x => !x.DataExcluido.HasValue && contratoIds.Contains((int)x.CodContrato) && x.CodHistorico > 0).ToList();
                contratos = parcelas.Where(x => x.Historico.CodSintese == codSinteseParcela).GroupBy(x => x.Contrato).Select(x => x.Key).ToList();

                foreach (var item in contratos)
                {
                    item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
                }
            }
            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.dropContratos = listaContratos.ToList();

            var parcelaCliente = _qry.ObterParcelaPorCliente(cdCliente, 0, 0).ToList();
            List<ContratoParcela> contratoParcela = new List<ContratoParcela>();
            foreach (var parcelaItem in parcelaCliente)
            {
                contratoParcela.Add(_dbContratoParcela.FindById(parcelaItem.Cod));
            }

            ViewBag.parcelasCliente = contratoParcela.ToList();

            if (dados != null)
            {
                foreach (var item in contratoParcela)
                {
                    _qry.executarAtualizaValorParcela(item.Cod);
                }
            }


            return View(dados);
        }


        [HttpGet]
        public ActionResult ParcelaClienteAvencer(int id = 0)
        {
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente);
                if (id > 0)
                {
                    return RedirectToAction("ParcelaClienteAvencer", new { id = id });
                }
                else
                {
                    return RedirectToAction("ConsultaPorCliente");
                }
            }
            else { WebRotinas.CookieObjetoGravar(_filtroCodCliente, id); }
            var cdCliente = 0;
            if (id > 0) { cdCliente = id; } else { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }
            ViewBag.Cliente = _dbCliente.FindById(cdCliente);

            var dados = _dbCliente.FindById(cdCliente);

            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).ToList();
            foreach (var item in contratos)
            {
                item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
            }
            ViewBag.Contratos = contratos.ToList();
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            if (usuarioLogado.CodRegional > 0)
            {
                contratos = (from p in contratos
                             where p.TabelaRegionalProduto.CodRegional == usuarioLogado.CodRegional
                             select p).ToList();
            }
            contratos.Insert(0, new Contrato() { NumeroContrato = "--Todos--" });

            //FILTRAS OS CONTRATOS DA DETERMINADA SINTESE
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var codSintese = filtro.CodSintese;
            ViewBag.CodSinteseContrato = codSintese;
            ViewBag.CodSinteseParcela = filtro.CodSinteseParcela;
            if (codSintese > 0)
            {
                contratos = contratos.Where(x => x.CodHistorico.HasValue).ToList();
                contratos = (from p in contratos
                             join h in contratos.Select(x => x.Historico).ToList() on p.CodHistorico equals h.Cod
                             where (h.CodSintese == codSintese || codSintese == 0)
                             select p).ToList();
            }

            var codSinteseParcela = filtro.CodSinteseParcela;
            ViewBag.CodSinteseParcela = filtro.CodSinteseParcela;
            if (codSinteseParcela > 0)
            {
                List<int> contratoIds = contratos.Select(x => x.Cod).ToList();
                List<ContratoParcela> parcelas = _dbContratoParcela.FindAll(x => !x.DataExcluido.HasValue && contratoIds.Contains((int)x.CodContrato) && x.CodHistorico > 0).ToList();
                contratos = parcelas.Where(x => x.Historico.CodSintese == codSinteseParcela).GroupBy(x => x.Contrato).Select(x => x.Key).ToList();

                foreach (var item in contratos)
                {
                    item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
                }
            }

            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.dropContratos = listaContratos.ToList();




            return View(dados);
        }

        [HttpGet]
        public ActionResult ParcelaClientePagas(int id = 0)
        {
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente);
                if (id > 0)
                {
                    return RedirectToAction("ParcelaClientePagas", new { id = id });
                }
                else
                {
                    return RedirectToAction("ConsultaPorCliente");
                }
            }
            else { WebRotinas.CookieObjetoGravar(_filtroCodCliente, id); }
            var cdCliente = 0;
            if (id > 0) { cdCliente = id; } else { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }
            ViewBag.Cliente = _dbCliente.FindById(cdCliente);

            var dados = _dbCliente.FindById(cdCliente);

            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).ToList();
            foreach (var item in contratos)
            {
                item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
            }
            ViewBag.Contratos = contratos.ToList();
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            if (usuarioLogado.CodRegional > 0)
            {
                contratos = (from p in contratos
                             where p.TabelaRegionalProduto.CodRegional == usuarioLogado.CodRegional
                             select p).ToList();
            }
            contratos.Insert(0, new Contrato() { NumeroContrato = "--Todos--" });

            //FILTRAS OS CONTRATOS DA DETERMINADA SINTESE
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var codSintese = filtro.CodSintese;
            ViewBag.CodSinteseContrato = codSintese;
            ViewBag.CodSinteseParcela = filtro.CodSinteseParcela;
            if (codSintese > 0)
            {
                contratos = contratos.Where(x => x.CodHistorico.HasValue).ToList();
                contratos = (from p in contratos
                             join h in contratos.Select(x => x.Historico).ToList() on p.CodHistorico equals h.Cod
                             where (h.CodSintese == codSintese || codSintese == 0)
                             select p).ToList();
            }

            var codSinteseParcela = filtro.CodSinteseParcela;
            ViewBag.CodSinteseParcela = filtro.CodSinteseParcela;
            if (codSinteseParcela > 0)
            {
                List<int> contratoIds = contratos.Select(x => x.Cod).ToList();
                List<ContratoParcela> parcelas = _dbContratoParcela.FindAll(x => !x.DataExcluido.HasValue && contratoIds.Contains((int)x.CodContrato) && x.CodHistorico > 0).ToList();
                contratos = parcelas.Where(x => x.Historico.CodSintese == codSinteseParcela).GroupBy(x => x.Contrato).Select(x => x.Key).ToList();

                foreach (var item in contratos)
                {
                    item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
                }
            }

            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.dropContratos = listaContratos.ToList();

            return View(dados);
        }



        [HttpGet]
        public ActionResult ClienteCredito(int id = 0)
        {
            if (id == 0) { id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato); }
            var cdCliente = 0;
            if (cdCliente == 0) { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }
            if (cdCliente == 0) { return RedirectToAction("ConsultaPorCliente"); }

            var dados = _dbCliente.FindById(cdCliente);

            var listaCredito = _dbClienteCredito.All().Where(x => x.CodCliente == cdCliente).ToList();
            ViewBag.ListaCreditoCliente = listaCredito.ToList();

            if (dados == null)
                dados = new Cliente();

            return View(dados);
        }

        [HttpGet]
        public ActionResult SimulacaoProposta(int id = 0)
        {
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato);
            }

            var dados = _dbContrato.FindById(id);

            if (dados == null)
                dados = new Db.Models.Contrato();

            return View(dados);
        }

        [HttpGet]
        public ActionResult Servicos(int id = 0)
        {
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato);
            }

            var dados = _dbContrato.FindById(id);
            var clientePrincipal = _dbCliente.FindById(dados.CodClientePrincipal.Value);
            if (dados == null)
                dados = new Db.Models.Contrato();


            //ServicoFinanceiroTenda host = new ServicoFinanceiroTenda(
            //    DbRotinas.ConverterParaString(clientePrincipal.CodCliente),
            //    dados.TabelaUnidade.TabelaBloco.TabelaEmpreendimento.CodEmpreendimentoCliente,
            //    dados.TabelaUnidade.TabelaBloco.TabelaEmpreendimento.NomeEmpreendimento,
            //    dados.TabelaUnidade.TabelaBloco.CodBlocoCliente,
            //    dados.TabelaUnidade.NumeroUnidade,
            //    dados.TabelaUnidade.CodUnidadeCliente,
            //    dados.CodEmpresa.HasValue ? dados.TabelaEmpresa.CodEmpresaCliente : "",
            //    "99999999999",
            //    "JUNIX",
            //    WebRotinas.ObterUsuarioLogado().Nome,
            //    WebRotinas.ObterUsuarioLogado().Email
            //    );

            //ViewBag.PosicaoFinanceira = host.AbaPosicaoFinanceira;
            //ViewBag.SegundaViaBoleto = host.AbaSegundaViaBoleto;
            //ViewBag.AntecipacaoParcelas = host.AbaAntecipacaoParcelas;
            //ViewBag.ParcelasEmAtraso = host.AbaParcelasEmAtraso;
            //ViewBag.MemoriaCalculo = host.AbaMemoriaCalculo;
            //ViewBag.ParcelaServico = host.AbaParcelaServico;

            return View(dados);
        }

        [HttpGet]
        public ActionResult DocumentosCliente(int id = 0)
        {
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente);
                if (id > 0)
                {
                    return RedirectToAction("DocumentosCliente", new { id = id });
                }
            }
            else { WebRotinas.CookieObjetoGravar(_filtroCodCliente, id); }
            var cdCliente = 0;
            if (id > 0) { cdCliente = id; } else { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }
            var dados = _dbCliente.FindById(cdCliente);

            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).ToList();
            foreach (var item in contratos)
            {
                item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
            }

            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            if (usuarioLogado.CodRegional > 0)
            {
                contratos = (from p in contratos
                             where p.TabelaRegionalProduto.CodRegional == usuarioLogado.CodRegional
                             select p).ToList();
            }
            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.Contratos = listaContratos.ToList();

            var lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = null, Value = null });

            var clientes = dados.ContratoClientes.Where(x => !x.DataExcluido.HasValue).ToList();
            foreach (var item in clientes)
            {
                lista.Add(new DictionaryEntry() { Key = Db.Rotinas.DbRotinas.Descriptografar(item.Cliente.Nome), Value = item.CodCliente });
            }

            ViewBag.ListaTipoDocumento = _dbListas.TipoDocumento();
            ViewBag.ListaCliente = lista;

            return View(dados);

        }


        [HttpGet]
        public ActionResult Documentos(int id = 0)
        {
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato);
            }

            var dados = _dbContrato.FindById(id);

            if (dados == null)
                dados = new Db.Models.Contrato();

            var lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = null, Value = null });

            var clientes = dados.ContratoClientes.Where(x => !x.DataExcluido.HasValue).ToList();


            foreach (var item in clientes)
            {
                lista.Add(new DictionaryEntry() { Key = item.Cliente.Nome, Value = item.CodCliente });
            }

            ViewBag.ListaTipoDocumento = _dbListas.TipoDocumento();
            ViewBag.ListaCliente = lista;

            return View(dados);
        }


        [HttpGet]
        public ActionResult Despesas(int id = 0)
        {
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato);
            }

            var dados = _dbContrato.FindById(id);

            if (dados == null)
                dados = new Db.Models.Contrato();


            ViewBag.ListaTipoDespesa = _dbListas.TipoDespesa();

            return View(dados);
        }

        [HttpGet]
        public ActionResult DespesasCliente(int id = 0)
        {
            var cdCliente = 0;
            if (cdCliente == 0) { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }
            if (id == 0) { id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato); }
            var dados = _dbCliente.FindById(cdCliente);

            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).ToList();
            foreach (var item in contratos)
            {
                item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
            }
            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.Contratos = listaContratos.ToList();

            var contratosCliente = _dbContratoCliente.All().Where(x => x.CodCliente == cdCliente).ToList();
            List<ContratoDespesa> despesas = new List<ContratoDespesa>();
            foreach (var contrato in contratosCliente)
            {
                var despesaContrato = _dbContratoDespesa.All().Where(x => x.CodContrato == contrato.CodContrato).ToList();
                foreach (var item in despesaContrato)
                {
                    despesas.Add(_dbContratoDespesa.FindById((int)item.Cod));
                }
            }
            ViewBag.despesasCliente = despesas.ToList();

            if (dados == null)
                dados = new Db.Models.Cliente();


            ViewBag.ListaTipoDespesa = _dbListas.TipoDespesa();

            return View(dados);
        }

        [HttpPost]
        public ActionResult DespesasCliente(ContratoDespesa despesa)
        {
            var dados = _dbContrato.FindById(despesa.CodContrato.Value);

            if (ModelState.IsValid)
            {
                if (despesa.CodTipoDespesa == 0) { despesa.CodTipoDespesa = null; }
                _dbContratoDespesa.CreateOrUpdate(despesa);
                ViewBag.MensagemOK = Language.MensagemOK;
                ViewBag.RedirecionarUrl = Url.Action("DespesasCliente", "Pesquisa", new { id = dados.Cod });
            }

            ViewBag.ListaTipoDespesa = _dbListas.TipoDespesa();

            var cdCliente = 0;
            if (cdCliente == 0) { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }
            var dadosCliente = _dbCliente.FindById(cdCliente);

            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).ToList();
            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.Contratos = listaContratos.ToList();

            return View(dadosCliente);
        }

        [HttpPost]
        public ActionResult Despesas(ContratoDespesa despesa)
        {
            var dados = _dbContrato.FindById(despesa.CodContrato.Value);

            if (ModelState.IsValid)
            {
                if (despesa.CodTipoDespesa == 0) { despesa.CodTipoDespesa = null; }
                _dbContratoDespesa.CreateOrUpdate(despesa);
                ViewBag.MensagemOK = Language.MensagemOK;
                ViewBag.RedirecionarUrl = Url.Action("Despesas", "Pesquisa", new { id = dados.Cod });
            }

            ViewBag.ListaTipoDespesa = _dbListas.TipoDespesa();

            return View(dados);
        }

        [HttpPost]
        public JsonResult SelecionarContrato(int id = 0)
        {
            try
            {
                WebRotinas.CookieObjetoGravar(_filtroCodContrato, id);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        [HttpPost]
        public JsonResult SelecionarCliente(int id = 0)
        {
            try
            {
                WebRotinas.CookieObjetoGravar(_filtroCodCliente, id);

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpPost]
        public JsonResult SelecionarClienteParcela(int id = 0)
        {
            try
            {
                WebRotinas.CookieObjetoGravar(_filtroCodCliente, id);

                var filtro = WebRotinas.FiltroGeralObter();
                var query_dados = _qry.ListarClienteParcelaDadosPorParcela(
                    id,
                    filtro.NomeClienteEncrypt,
                    filtro.PrimeiroNomeEncrypt,
                    filtro.CPF_CNPJEncrypt,
                    filtro.CodConstrutora,
                    filtro.CodRegional,
                    filtro.CodTipoCobranca,
                    filtro.Bloqueado,
                    filtro.CodAgingCliente,
                    filtro.AndamentoDaObra,
                    null,//filtro.NumeroContratoEncrypt,
                    filtro.CodSintese,
                    filtro.CodProduto,
                    filtro.CodFase,
                    filtro.CodGrupo,
                    filtro.CodTipoBloqueio,
                    filtro.CodSinteseParcela,
                    filtro.CodUsuarioAtendimento,
                    filtro.CodPerfilTipo
                    );
                List<int> parcelasIds = query_dados.Select(x => x.CodContratoParcela).ToList();
                var parcelas = _dbContratoParcela.All().Where(x => parcelasIds.Contains(x.Cod)).ToList();
                filtro.NumeroFatura = String.Join(",", parcelas.Select(x => x.FluxoPagamento));
                List<int> contratoIds = parcelas.Select(x => (int)x.CodContrato).ToList();

                var contratos = _dbContrato.All().Where(x => contratoIds.Contains(x.Cod)).ToList();
                filtro.NumeroContrato = String.Join(",", contratos.Select(x => DbRotinas.Descriptografar(x.NumeroContrato)));

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        [HttpGet]
        public FileStreamResult DownloadSeguro(string cd)
        {


            var doc = _dbContratoDocumento.All().Where(x => x.ArquivoGuid == cd).First();

            string caminhoReal = Server.MapPath(doc.ArquivoCaminho);

            FileInfo info = new FileInfo(caminhoReal);
            return File(info.OpenRead(), "application/force-download", doc.ArquivoNome);


        }


        [HttpPost]
        public JsonResult DocumentoRemover(string cod)
        {
            try
            {
                var doc = _dbContratoDocumento.All().Where(x => x.ArquivoGuid == cod).FirstOrDefault();
                //string caminhoReal = Server.MapPath(doc.ArquivoCaminho);
                //FileInfo info = new FileInfo(caminhoReal);
                //if (info.Exists)
                //{
                //    try
                //    {
                //        System.IO.File.Delete(caminhoReal);
                //    }
                //    catch { }
                //}
                _dbContratoDocumento.Delete(doc.Cod);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {

                return Json(new { OK = false, Mensagem = ex.Message });
            }


        }

        [HttpPost]
        public ActionResult paginarDadosParcela(int draw, int start, int length, Dictionary<string, string> search, List<Dictionary<string, string>> order, List<Dictionary<string, string>> columns, string Variavel, string Multa, string Juros, string Honorario, string Desconto, int? tipo, string qtdParcela, short? TipoAcordo, string Obs)
        {
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var searchString = Request["search[value]"];
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"]);
            var sortColumnsName = Request[string.Format("columns[{0}][name]", sortColumnIndex)];
            var sortDirection = Request["order[0][dir]"]; // asc or desc

            var query_dados = _qry.ListarParcelaAcordo(Variavel, Juros, Multa, Honorario, Desconto, tipo, qtdParcela);

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count();
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SalvarAcordo(string Variavel, string Multa, string Juros, string Honorario, string Desconto, int? Tipo, string qtdParcela)
        {
            try
            {
                var query_dados = _qry.ListarParcelaAcordo(Variavel, Juros, Multa, Honorario, Desconto, Tipo, qtdParcela);
                return Json(new { OK = true, Acordo = query_dados.FirstOrDefault().CodAcordo });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpPost]
        public ActionResult paginarDados(int draw, int start, int length, Dictionary<string, string> search, List<Dictionary<string, string>> order, List<Dictionary<string, string>> columns)
        {
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var searchString = Request["search[value]"];
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"]);
            var sortColumnsName = Request[string.Format("columns[{0}][name]", sortColumnIndex)];
            var sortDirection = Request["order[0][dir]"]; // asc or desc

            var query_dados = _qry.ListarContratosDados(
                filtro.CodigoVenda,
                filtro.CodigoCliente,
                filtro.NomeComprador,
                 filtro.PrimeiroNome,
                filtro.CPF_CNPJ,
                filtro.CodConstrutora,
                filtro.CodDonoCarteira,
                filtro.CodRegional,
                filtro.CodTipoCobranca,
                filtro.CodTipoCarteira,
                filtro.CodGestao,
                filtro.CodEscritorio,
                filtro.CodEmpreendimento,
                filtro.CodBloco,
                filtro.CodUnidade,
                filtro.Bloqueado,
                filtro.CodAgingCliente,
                filtro.AndamentoDaObra,
                filtro.NumeroContrato,
                filtro.CodSintese,
                filtro.CodProduto,
                filtro.CodFase,
                filtro.CodGrupo,
                filtro.CodTipoBloqueio
                ).OrderBy(string.Format("{0} {1}", sortColumnsName, sortDirection));

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count();
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }



        //[HttpPost]
        //public ActionResult paginarDadosCliente(int draw, int start, int length, Dictionary<string, string> search, List<Dictionary<string, string>> order, List<Dictionary<string, string>> columns)
        //{
        //    var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
        //    var searchString = Request["search[value]"];
        //    var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"]);
        //    var sortColumnsName = Request[string.Format("columns[{0}][name]", sortColumnIndex)];
        //    var sortDirection = Request["order[0][dir]"]; // asc or desc

        //    var query_dados = _qry.ListarContratosDadosCliente(
        //        filtro.CodigoVenda,
        //        filtro.CodigoCliente,
        //        filtro.NomeComprador,
        //        filtro.PrimeiroNome,
        //        filtro.CPF_CNPJ,
        //        filtro.CodConstrutora,
        //        filtro.CodDonoCarteira,
        //        filtro.CodRegional,
        //        filtro.CodTipoCobranca,
        //        filtro.CodTipoCarteira,
        //        filtro.CodGestao,
        //        filtro.CodEscritorio,
        //        filtro.CodEmpreendimento,
        //        filtro.CodBloco,
        //        filtro.CodUnidade,
        //        filtro.Bloqueado,
        //        filtro.CodAgingCliente,
        //        filtro.AndamentoDaObra,
        //        filtro.NumeroContrato,
        //        filtro.CodSintese,
        //        filtro.CodProduto,
        //        filtro.CodFase,
        //        filtro.CodGrupo,
        //        filtro.CodTipoBloqueio,
        //        filtro.CodSinteseParcela,
        //        filtro.ClienteId
        //        ).OrderBy(string.Format("{0} {1}", sortColumnsName, sortDirection));

        //    DataTableData d = new DataTableData();

        //    d.draw = draw;
        //    d.recordsTotal = length;
        //    int recordsFiltered = query_dados.Count();
        //    d.data = query_dados.Skip(start).Take(length);
        //    d.recordsFiltered = recordsFiltered;

        //    return Json(d, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public ActionResult ObterDadosCliente()
        //{
        //    var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);

        //    var query_dados = _qry.ListarContratosDadosCliente(
        //       filtro.CodigoVenda,
        //       DbRotinas.Criptografar(filtro.CodigoCliente),
        //       DbRotinas.Criptografar(filtro.NomeComprador),
        //       DbRotinas.Criptografar(filtro.PrimeiroNome),
        //       DbRotinas.Criptografar(filtro.CPF_CNPJ),
        //       filtro.CodConstrutora,
        //       filtro.CodDonoCarteira,
        //       filtro.CodRegional,
        //       filtro.CodTipoCobranca,
        //       filtro.CodTipoCarteira,
        //       filtro.CodGestao,
        //       filtro.CodEscritorio,
        //       filtro.CodEmpreendimento,
        //       filtro.CodBloco,
        //       filtro.CodUnidade,
        //       filtro.Bloqueado,
        //       filtro.CodAgingCliente,
        //       filtro.AndamentoDaObra,
        //       DbRotinas.Criptografar(filtro.NumeroContrato),
        //       filtro.CodSintese,
        //       filtro.CodProduto,
        //       filtro.CodFase,
        //       filtro.CodGrupo,
        //       filtro.CodTipoBloqueio,
        //       filtro.CodSinteseParcela,
        //        filtro.ClienteId
        //       );

        //    var jsonResult = Json(new { OK = true, Data = query_dados.ToList() }, JsonRequestBehavior.AllowGet);
        //    jsonResult.MaxJsonLength = int.MaxValue;
        //    return jsonResult;
        //}
        public IQueryable<object> ListaDadosParcela(FiltroGeralDados filtro)
        {
            var query_dados = new List<ContratoDados>();
            query_dados = _qry.ListarClienteDadosPorParcela(
               filtro.CodigoClienteEncrypt,
               filtro.NomeClienteEncrypt,
               filtro.PrimeiroNomeEncrypt,
               filtro.CPF_CNPJEncrypt,
               filtro.CodConstrutora,
               filtro.CodRegional,
               filtro.CodTipoCobranca,
               filtro.Bloqueado,
               filtro.CodAgingCliente,
               filtro.AndamentoDaObra,
               filtro.NumeroContratoEncrypt,
               filtro.CodSintese,
               filtro.CodProduto,
               filtro.CodFase,
               filtro.CodGrupo,
               filtro.CodTipoBloqueio,
               filtro.CodSinteseParcela,
               filtro.CodUsuarioAtendimento,
               filtro.CodPerfilTipo
               );
            var data = query_dados
                               .Select(x => new
                               {
                                   Cod = x.Cod,
                                   CodCliente = DbRotinas.Descriptografar(x.CodCliente),
                                   Cliente = DbRotinas.Descriptografar(x.Cliente),
                                   CPF = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(x.CPF)),
                                   QtdContrato = x.QtdContrato,
                                   QtdParcela = x.QtdParcela,
                                   QtdCliente = x.QtdCliente,
                                   ValorAtraso = x.ValorAtraso,
                                   ValorPago = x.ValorPago,
                                   ValorAVencer = x.ValorAVencer,
                                   ValorAtrasoDesc = $"{x.ValorAtraso:C}",
                                   ValorPagoDesc = $"{x.ValorPago:C}",
                                   ValorAVencerDesc = $"{x.ValorAVencer:C}"
                                   //DataCadastro = $"{x.DataCadastro:dd/MM/yyyy}",
                                   //NomeUsuario = x.NomeUsuario,
                                   //Fase = DbRotinas.Descriptografar(x.Fase),
                                   //Sintese = DbRotinas.Descriptografar(x.Sintese)
                               }).AsQueryable();
            return data;
        }

        public IQueryable<object> ListaDadosCliente(FiltroGeralDados filtro, DTParameters param)
        {
            var query_dados = new List<ContratoDados>();
            try
            {
                if (filtro.CodSintese == 0)
                {
                    query_dados = _qry.ListarContratosDadosCliente(
                    filtro.CodigoClienteEncrypt,
                    filtro.NomeClienteEncrypt,
                    filtro.PrimeiroNomeEncrypt,
                    filtro.CPF_CNPJEncrypt,
                    filtro.CodConstrutora,
                    filtro.CodRegional,
                    filtro.CodTipoCobranca,
                    filtro.Bloqueado,
                    filtro.CodAgingCliente,
                    filtro.AndamentoDaObra,
                    filtro.NumeroContratoEncrypt,
                    filtro.CodSintese,
                    filtro.CodProduto,
                    filtro.CodFase,
                    filtro.CodGrupo,
                    filtro.CodTipoBloqueio,
                    filtro.CodSinteseParcela,
                    filtro.ClienteId,
                    filtro.CodContratos != null ? filtro.CodContratos.Count > 0 ? DbRotinas.StringJoinSemErro(filtro.CodContratos) : "0" : "",
                    filtro.CodParcelas != null ? filtro.CodParcelas.Count > 0 ? DbRotinas.StringJoinSemErro(filtro.CodParcelas) : "0" : ""
                    );
                }
                else
                {
                    query_dados = _qry.ListarClienteContratoPorSintese(
                    filtro.CodigoClienteEncrypt,
                    filtro.NomeClienteEncrypt,
                    filtro.PrimeiroNomeEncrypt,
                    filtro.CPF_CNPJEncrypt,
                    filtro.CodConstrutora,
                    filtro.CodRegional,
                    filtro.CodTipoCobranca,
                    filtro.Bloqueado,
                    filtro.CodAgingCliente,
                    filtro.AndamentoDaObra,
                    DbRotinas.Criptografar(filtro.NumeroContrato),
                    filtro.CodSintese,
                    filtro.CodProduto,
                    filtro.CodFase,
                    filtro.CodGrupo,
                    filtro.CodTipoBloqueio,
                    filtro.CodSinteseParcela,
                    filtro.ClienteId,
                    filtro.CodUsuarioLogado,
                    DbRotinas.StringJoinSemErro(filtro.CodContratos),
                    DbRotinas.StringJoinSemErro(filtro.CodParcelas)
                    );
                }
            }
            catch (Exception ex)
            {

                new Exception("Erro (SP_CONTRATO_DADOS_CLIENTE) -  ", ex);
            }
            IQueryable<object> data = null;
            try
            {
                data = query_dados
                                 .Select(x => new
                                 {
                                     Cod = x.Cod,
                                     CodCliente = DbRotinas.Descriptografar(x.CodCliente),
                                     Cliente = DbRotinas.Descriptografar(x.Cliente),
                                     CPF = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(x.CPF)),
                                     QtdContrato = DbRotinas.ConverterParaString(x.QtdContrato),
                                     QtdParcela = DbRotinas.ConverterParaString(x.QtdParcela),
                                     QtdCliente = DbRotinas.ConverterParaString(x.QtdCliente),
                                     ValorAtraso = x.ValorAtraso,
                                     ValorPago = x.ValorPago,
                                     ValorAVencer = x.ValorAVencer,
                                     ValorAtrasoDesc = $"{x.ValorAtraso:C}",
                                     ValorPagoDesc = $"{x.ValorPago:C}",
                                     ValorAVencerDesc = $"{x.ValorAVencer:C}",
                                     DataCadastro = x.DataCadastro,
                                     DataCadastroDesc = $"{x.DataCadastro:dd/MM/yyyy}",
                                     NomeUsuario = x.NomeUsuario,
                                     Fase = DbRotinas.Descriptografar(x.Fase),
                                     Observacao = DbRotinas.Descriptografar(x.Observacoes),
                                     Status = $"{x.CodigoSinteseCliente} - {DbRotinas.Descriptografar(x.Sintese)}",
                                     Aging = $"{x.Aging}"
                                 }).AsQueryable();
                data.ToList();
            }
            catch (Exception ex)
            {

                new Exception($"erro ao processar os dados {ex.InnerException}", ex);
            }

            return data.AsQueryable();
        }

        [HttpPost]
        public JsonResult ObterSolicitacaoDadosPorServer(DTParameters param, int? CodSintese, int? CodFase, int? CodSinteseHome)
        {
            try
            {
                var filter = WebRotinas.FiltroGeralObter();
                if (CodSintese == null || CodSintese == 0)
                {
                    CodSintese = filter.CodSinteseBoleto;
                }
                var dtsource = ListaDadosSolicitacao(filter, CodFase, CodSintese, CodSinteseHome);

                var result = WebRotinas.DTRotinas.DTResult(param, dtsource);

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }


        public IQueryable<object> ListaDadosSolicitacao(FiltroGeralDados filtro, int? CodFase, int? CodSintese, int? CodSinteseHome)
        {
            //CASO O USUARIO FOR DA FILIAL DEVE FILTRAR APENAS SEUS BOLETOS SOLICITADOS
            var usuario = WebRotinas.ObterUsuarioLogado();
            Nullable<int> usuarioId = null;
            string filiais = "";
            if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional)
            {
                usuarioId = usuario.Cod;
                var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == usuario.Cod && !x.DataExcluido.HasValue);
                filiais = string.Join(",", regionais.Select(x => x.CodRegional).ToArray());
            }
            else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == usuario.Cod && !x.DataExcluido.HasValue);
                filiais = string.Join(",", regionais.Select(x => x.CodRegional).ToArray());
            }

            //QRY PARA TRAZER TODOS BOLETOS
            var query_dados = new List<BoletoSolicitacaoDados>();
            query_dados = _qry.ListarDadosBoleto(usuarioId, CodFase, CodSintese, filiais);

            //VERIFICAR O TIPO DE FLUXO E VERIRICAR SE O USUARIO PODE EDITAR
            IQueryable<object> data = null;
            var fluxo = (short)0;
            if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Analista)
            {
                fluxo = (short)Enumeradores.TipoFluxo.AnalistaBoleto;
            }
            else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista)
            {
                fluxo = (short)Enumeradores.TipoFluxo.SupervisorBoleto;
            }
            else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional || usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                fluxo = (short)Enumeradores.TipoFluxo.FilialBoleto;
            }
            data = query_dados
                         .Select(x => new
                         {
                             cpId = x.CodParcela,
                             bId = x.CodBoleto,
                             CodigoCliente = DbRotinas.Descriptografar(x.CodigoCliente),
                             Cliente = DbRotinas.Descriptografar(x.Nome),
                             Regional = DbRotinas.Descriptografar(x.Regional),
                             CodContrato = x.CodContrato,
                             CodHistorico = x.CodHistorico,
                             CodCliente = x.CodClientePrincipal,
                             TipoCobranca = DbRotinas.Descriptografar(x.TipoCobranca),
                             Fase = DbRotinas.Descriptografar(x.Fase),
                             Sintese = DbRotinas.Descriptografar(x.Sintese),
                             NumeroDocumento = $"{x.NumeroDocumento}",
                             DataVencimento = x.DataVencimento,
                             DataVencimentoDesc = $"{x.DataVencimento:dd/MM/yyyy}",
                             DataDocumento = x.DataDocumento,
                             DataDocumentoDesc = $"{x.DataDocumento:dd/MM/yyyy}",
                             JurosAoMesPorcentagem = x.JurosAoMesPorcentagem,
                             JurosAoMesPorcentagemDesc = $"{x.JurosAoMesPorcentagem:N2}",
                             JurosAoMesPorcentagemPrevisto = x.JurosAoMesPorcentagemPrevisto,
                             JurosAoMesPorcentagemPrevistoDesc = $"{x.JurosAoMesPorcentagemPrevisto:N2}",
                             ValorJuros = x.ValorJuros,
                             ValorJurosDesc = $"{x.ValorJuros:C}",
                             ValorJurosPrevisto = x.ValorJurosPrevisto,
                             ValorJurosPrevistoDesc = $"{x.ValorJurosPrevisto:C}",
                             MultaAtrasoPorcentagem = x.MultaAtrasoPorcentagem,
                             MultaAtrasoPorcentagemDesc = $"{x.MultaAtrasoPorcentagem:N2}",
                             MultaAtrasoPorcentagemPrevisto = x.MultaAtrasoPorcentagemPrevisto,
                             MultaAtrasoPorcentagemPrevistoDesc = $"{x.MultaAtrasoPorcentagemPrevisto:N2}",
                             ValorMulta = x.ValorMulta,
                             ValorMultaDesc = $"{x.ValorMulta:C}",
                             ValorMultaPrevisto = x.ValorMultaPrevisto,
                             ValorMultaPrevistoDesc = $"{x.ValorMultaPrevisto:C}",
                             ValorDocumentoOriginal = x.ValorDocumentoOriginal,
                             ValorDocumentoOriginalDesc = $"{x.ValorDocumentoOriginal:C}",
                             ValorDocumentoPrevisto = x.ValorDocumentoPrevisto,
                             ValorDocumentoPrevistoDesc = $"{x.ValorDocumentoPrevisto:C}",
                             ValorDocumento = x.ValorDocumento,
                             ValorDocumentoDesc = $"{x.ValorDocumento:C}",
                             Observacao = x.Observacao,
                             Solicitante = x.Usuario,
                             DataCadastroDesc = $"{x.DataCadastro:dd/MM/yyyy HH: mm}",
                             DataCadastro = x.DataCadastro,
                             DataAprovacaoDesc = $"{x.DataAprovacao:dd/MM/yyyy HH:mm}",
                             DataAprovacao = x.DataAprovacao,
                             IsPerfil = x.TipoFluxo == fluxo ? true : false,
                             IsAprovado = x.Status == (short)Enumeradores.StatusBoleto.Aprovado ? true : false,
                             IsReprovado = x.Status == (short)Enumeradores.StatusBoleto.Reprovado ? true : false
                         }).AsQueryable();

            if (CodFase > 0)
            {

            }

            return data;
        }

        #region Excel Export
        [HttpPost]
        public JsonResult ExportarExcel(DTParameters param, int? CodSintese, int? CodFase, int? CodSinteseHome)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            var filter = WebRotinas.FiltroGeralObter();
            try
            {
                string arquivo = WebRotinas.ObterArquivoTemp($"boletos{DateTime.Now:ddMMyyyyss}.csv");

                var dtsource = ListaDadosSolicitacao(filter, CodFase, CodSintese, CodSinteseHome);
                var result = WebRotinas.DTRotinas.DTFilterResult(dtsource, param);

                List<ConverterListaParaExcel.CollumnExcel> columns = new List<ConverterListaParaExcel.CollumnExcel>();
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Regional", Text = "Filial" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CodCliente", Text = "AN8" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Cliente", Text = "Cliente" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NumeroDocumento", Text = "Nr Documento" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataVencimentoDesc", Text = "Dt.Vencimento" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataDocumentoDesc", Text = "Dt.Documento" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ValorDocumentoPrevistoDesc", Text = "Vr.Doc.Prev." });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ValorDocumentoDesc", Text = "Vr.Doc" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataCadastroDesc", Text = "Dt.Solicitação" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataAprovacaoDesc", Text = "Dt.Aprovação" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Solicitante", Text = "Solicitante" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "TipoCobranca", Text = "Fluxo" });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Observacao", Text = "Observação" });

                ConverterListaParaExcel.ConvertListToExcelFile(result.ToList(), arquivo, columns);

                ConverterListaParaExcel.GerarCSVDeEspecList(result.ToList(), arquivo, columns);
                //ConverterListaParaExcel.ConvertListToExcelFile(result.ToList(), arquivo, columns);
                FileInfo f = new FileInfo(arquivo);

                op.OK = true;
                op.Dados = Url.Content($"~/temp/{f.Name}");
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }

        

        [HttpPost]
        public JsonResult ExportarExcelCliente(DTParameters param, int? CodSintese, int? CodFase, int? CodSinteseHome)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            var filtro = WebRotinas.FiltroGeralObter();
            try
            {
                Helper _rotina = new Helper();

                List<ContratoDados> Dados = new List<ContratoDados>();
                List<ConverterListaParaExcel.CollumnExcel> columns = new List<ConverterListaParaExcel.CollumnExcel>();
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CodCliente", Text = Idioma.Traduzir("An8") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CPF", Text = Idioma.Traduzir("CPF") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "QtdContrato", Text = Idioma.Traduzir("QtdContrato") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ValorAtrasoDesc", Text = Idioma.Traduzir("Vr.Atraso") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ValorPagoDesc", Text = Idioma.Traduzir("Vr.Pago") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ValorAVencerDesc", Text = Idioma.Traduzir("Á Vencer") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataCadastroDesc", Text = Idioma.Traduzir("Dt.Cadastro") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Usuario", Text = Idioma.Traduzir("Usuario") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Status", Text = Idioma.Traduzir("Status") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Observacao", Text = Idioma.Traduzir("Observacoes") });
                var dtsource = ListaDadosCliente(filtro, param);
                var result = WebRotinas.DTRotinas.DTFilterResult(dtsource, param);

                var nomeArquivo = Idioma.Traduzir("ListaCliente");
                op = _rotina.GerarArquivo(nomeArquivo, result, columns);
            }
            catch (Exception ex)
            {

                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }
        #endregion

        public JsonResult ObterDadosClienteServer(DTParameters param)
        {
            try
            {
                var filter = WebRotinas.FiltroGeralObter();
                var dtsource = ListaDadosCliente(filter, param);
                var result = WebRotinas.DTRotinas.DTResult(param, dtsource);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        public FileResult DonwloadExcelParcela()
        {
            string filename = WebRotinas.ObterArquivoTemp();
            FileInfo info = new FileInfo(filename);
            var filtro = WebRotinas.FiltroGeralObter();
            var dtsource = ListaDadosParcela(filtro);
            ConverterListaParaExcel.GerarCSVDeGenericList(dtsource.ToList(), filename);
            var nmarquivo = "pesquisa" + DateTime.Now.ToString("ddMMyy") + ".csv";
            return File(info.FullName, "application/force-download", nmarquivo);
        }

        private void Teste()
        {
            var boletos = _dbBoleto.All();
            IQueryable<object> data = null;
            var usuario = WebRotinas.ObterUsuarioLogado();
            data = boletos
                         .ToList()
                         .Select(x => new
                         {
                             Regional = x?.ContratoParcela?.Contrato?.TabelaRegionalProduto?.TabelaRegional?.Regional ?? "Algum objeto nullo",
                         }).AsQueryable();
        }

        public FileResult DownloadExcel()
        {
            string filename = WebRotinas.ObterArquivoTemp();
            FileInfo info = new FileInfo(filename);
            var filtro = WebRotinas.FiltroGeralObter();
            var data = new List<ContratoDados>();
            if (filtro.CodSintese == 0)
            {
                var query_dados = _qry.ListarContratosDadosCliente(
                DbRotinas.Criptografar(filtro.CodigoCliente),
                DbRotinas.Criptografar(filtro.NomeCliente),
                DbRotinas.Criptografar(filtro.PrimeiroNome),
                DbRotinas.Criptografar(filtro.CPF_CNPJ),
                filtro.CodConstrutora,
                filtro.CodRegional,
                filtro.CodTipoCobranca,
                filtro.Bloqueado,
                filtro.CodAgingCliente,
                filtro.AndamentoDaObra,
                DbRotinas.Criptografar(filtro.NumeroContrato),
                filtro.CodSintese,
                filtro.CodProduto,
                filtro.CodFase,
                filtro.CodGrupo,
                filtro.CodTipoBloqueio,
                filtro.CodSinteseParcela,
                filtro.ClienteId,
                DbRotinas.StringJoinSemErro(filtro.CodContratos),
                DbRotinas.StringJoinSemErro(filtro.CodParcelas)
                );
                data = query_dados.ToList();
            }
            else
            {
                var query_dados = _qry.ListarClienteContratoPorSintese(
                DbRotinas.Criptografar(filtro.CodigoCliente),
                DbRotinas.Criptografar(filtro.NomeCliente),
                DbRotinas.Criptografar(filtro.PrimeiroNome),
                DbRotinas.Criptografar(filtro.CPF_CNPJ),
                filtro.CodConstrutora,
                filtro.CodRegional,
                filtro.CodTipoCobranca,
                filtro.Bloqueado,
                filtro.CodAgingCliente,
                filtro.AndamentoDaObra,
                DbRotinas.Criptografar(filtro.NumeroContrato),
                filtro.CodSintese,
                filtro.CodProduto,
                filtro.CodFase,
                filtro.CodGrupo,
                filtro.CodTipoBloqueio,
                filtro.CodSinteseParcela,
                filtro.ClienteId,
                filtro.CodUsuarioAtendimento,
                DbRotinas.StringJoinSemErro(filtro.CodContratos),
                DbRotinas.StringJoinSemErro(filtro.CodParcelas)
               );
                data = query_dados.ToList();
            }
            var listaPaginada = data
                                .Select(x => new
                                {
                                    CodCliente = DbRotinas.Descriptografar(x.CodCliente),
                                    Cliente = DbRotinas.Descriptografar(x.Cliente),
                                    CPF = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(x.CPF)),
                                    QtdContrato = x.QtdContrato,
                                    Atraso = $"{x.ValorAtraso:C}",
                                    Pago = $"{x.ValorPago:C}",
                                    Vencer = $"{x.ValorAVencer:C}",
                                    Data = $"{x.DataCadastro:dd/MM/yyyy}",
                                    Usuario = x.NomeUsuario,
                                    Observacoes = DbRotinas.Descriptografar(x.Fase),
                                    Status = DbRotinas.Descriptografar(x.Sintese)
                                });
            ConverterListaParaExcel.GerarCSVDeGenericList(listaPaginada.ToList(), filename);
            var nmarquivo = "pesquisa" + DateTime.Now.ToString("ddMMyy") + ".csv";
            return File(info.FullName, "application/force-download", nmarquivo);
        }

        [HttpPost]
        public JsonResult ObterDadosPorParcelaServer(DTParameters param)
        {
            try
            {
                var filter = WebRotinas.FiltroGeralObter();

                var dtsource = ListaDadosParcela(filter);
                var result = WebRotinas.DTRotinas.DTResult(param, dtsource);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult ResultadoSolicitacao()
        {
            var filtro = WebRotinas.FiltroGeralObter();
            var user = WebRotinas.ObterUsuarioLogado();
            Nullable<int> usuarioId = null;
            string filiais = null;
            short? tipoFluxo = null;
            if (user.PerfilTipo == Enumeradores.PerfilTipo.Analista)
            {
                tipoFluxo = (short)Enumeradores.TipoFluxo.AnalistaBoleto;
            }
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista)
            {
                tipoFluxo = (short)Enumeradores.TipoFluxo.SupervisorBoleto;
            }
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional || user.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                {
                    usuarioId = user.Cod;
                }
                tipoFluxo = (short)Enumeradores.TipoFluxo.FilialBoleto;
                var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == user.Cod && !x.DataExcluido.HasValue);
                filiais = string.Join(",", regionais.Select(x => x.CodRegional).ToArray());
            }

            var resultadoParcelaBoleto = _qry.ObterTotaisBoletoSolicitacao(usuarioId, tipoFluxo, filiais);
            return View(resultadoParcelaBoleto);
        }
        [HttpPost]
        public ActionResult ResultadoSolicitacao(FiltroGeralDados filtro)
        {
            WebRotinas.FiltroGeralGravar(filtro);
            var user = WebRotinas.ObterUsuarioLogado();
            Nullable<int> usuarioId = null;
            string filiais = null;
            short? tipoFluxo = null;
            if (user.PerfilTipo == Enumeradores.PerfilTipo.Analista)
            {
                tipoFluxo = (short)Enumeradores.TipoFluxo.AnalistaBoleto;
            }
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista)
            {
                tipoFluxo = (short)Enumeradores.TipoFluxo.SupervisorBoleto;
            }
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional || user.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                {
                    usuarioId = user.Cod;
                }
                tipoFluxo = (short)Enumeradores.TipoFluxo.FilialBoleto;
                var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == user.Cod && !x.DataExcluido.HasValue);
                filiais = string.Join(",", regionais.Select(x => x.CodRegional).ToArray());
            }
            var resultadoParcelaBoleto = _qry.ObterTotaisBoletoSolicitacao(usuarioId, tipoFluxo, filiais);
            return View(resultadoParcelaBoleto);
        }
        [HttpPost]
        public ActionResult paginarDadosAcaoCobranca(int draw, int start, int length)
        {
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var query_dados = _qry.ListarContratosAcaoCobrancaHistoricoDados(filtro.TipoAcaoCobranca, filtro.CodTipoCobranca, filtro.CodCobrancaAcao, filtro.SituacaoTipo);

            //foreach (var i in query_dados)
            //{
            //    if (!i.SituacaoTipo.HasValue)
            //    {
            //        i.SituacaoTipoNome = "Prontos a Enviar";
            //    }
            //    else
            //    {
            //        i.SituacaoTipoNome = "Inconsistente";
            //    }

            //}

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count;
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult paginarDadosAll()
        {
            var query_dados = _qry.ListarContratosPaginadas(0, 1000);
            var contratos = ObterDadosSession();



            DataTableData d = new DataTableData();

            //d.draw = draw;
            //d.recordsTotal = length;
            int recordsFiltered = 67000;
            d.data = query_dados;
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult dadosRemessaAcaoCobranca(int draw, int start, int length)
        {
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var query_dados = _qry.ListarContratosAcaoCobrancaHistoricoDadosEmitidos(filtro.TipoAcaoCobranca, filtro.CodTipoCobranca, filtro.CodCobrancaAcao, filtro.SituacaoTipo);

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count;
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }
        private List<Contrato> ObterDadosSession(bool forceReload = false)
        {
            if (forceReload)
            {
                Session["filtro_dados"] = null;
            }
            List<Contrato> dados = new List<Db.Models.Contrato>();
            if (Session["filtro_dados"] != null)
                dados = WebRotinas.CookieObjetoLer<List<Contrato>>("filtro_dados");
            else
            {
                var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
                var contratos = _dbContrato.All();
                var resultado = FiltrarDados(contratos, filtro);
                WebRotinas.CookieObjetoGravar("filtro_dados", resultado);
            }
            return dados;
        }

        [HttpPost]
        public JsonResult SalvarBloqueados(int codTipoBloqueio, string descricao, int[] cheksIds)
        {
            try
            {
                foreach (var item in cheksIds)
                {
                    var entity = _dbContrato.FindById(item);
                    //entity.
                    entity.CodUsuarioBloqueio = WebRotinas.ObterUsuarioLogado().Cod;
                    entity.Bloqueado = true;
                    entity.CodTipoBloqueio = codTipoBloqueio;
                    entity.DataBloqueio = DateTime.Now;
                    entity.Justificativa = descricao;
                    _dbContrato.CreateOrUpdate(entity);
                }
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpPost]
        public JsonResult ParcelaAcordo(int CodAcordo, int[] parcelas)
        {
            try
            {
                foreach (var item in parcelas)
                {
                    AcordoContratoParcela contratoparcela = new AcordoContratoParcela();
                    contratoparcela.CodAcordo = CodAcordo;
                    contratoparcela.CodContratoParcela = item;
                    _dbAcordoContratoParcela.CreateOrUpdate(contratoparcela);

                }
                var dados = _dbContratoParcela.FindById(parcelas[0]);
                var acordo = _dbAcordo.FindById(CodAcordo);
                acordo.CodContratoPrincipal = dados.CodContrato;
                acordo.FluxoPagamento = Convert.ToString(GeraFluxoPagamento());
                _dbAcordo.CreateOrUpdate(acordo);

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        public int GeraFluxoPagamento()
        {
            Random number = new Random();
            return number.Next(99999999);
        }

        private void ConsultaHistoricoRepasse(string CodSAP)
        {
            SqlConnection conexao = new SqlConnection(ConfigurationManager.ConnectionStrings["tenda_imobflowContext"].ConnectionString.ToString());
            SqlCommand cmd = null;
            SqlDataReader dr;
            cmd = new SqlCommand("SP_HISTORICO_COBRANCA", conexao);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CODSAP", CodSAP);
            conexao.Open();
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                List<HistoricoRepasseDados> lHistorico = new List<HistoricoRepasseDados>();
                while (dr.Read())
                {
                    HistoricoRepasseDados historico = new HistoricoRepasseDados();
                    historico.Data = dr["Data"].ToString();
                    historico.Fase = dr["Fase"].ToString();
                    historico.Sintese = dr["Sintese"].ToString();
                    historico.Obs = dr["Obs"].ToString();
                    historico.Login = dr["LoginCadastro"].ToString();
                    lHistorico.Add(historico);
                }
                ViewBag.ListaHistorico = lHistorico;
            }
            conexao.Close();

        }

        //[HttpGet]
        //public ActionResult Atendimento(int id = 0, int cdHistorico = 0, int CodCliente = 0)
        //{
        //    Repository<Contrato> contrato = new Repository<Contrato>();

        //    var hsitorico = _dbContrato.All().Where(x => x.ContratoHistorico.CodSintese == cdHistorico && !x.DataAtendimento.HasValue);


        //    Dados obj = new Dados();

        //    var lista = obj.ConsultaCasosAtendimento();

        //    if (lista.Count() > 0)
        //    {
        //        id = (int)lista.FirstOrDefault().CodCliente;
        //    }
        //    if (id == 0)
        //    {
        //        id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato);
        //    }
        //    if (id == 0)
        //    {
        //        return RedirectToAction("FilaFinalizada");
        //    }

        //    Dados _sql = new Dados();
        //    var script = _sql.ConsultaScript(id);

        //    DeParaModeloAcao depara = new DeParaModeloAcao();
        //    ViewBag.ScriptAtendimento = script.Count() > 0 ? depara.DeParaCampos(script.FirstOrDefault().Modelo, id) : "Não há script para este tipo de cobrança ou faixa de atraso";



        //    var dados = _dbCliente.FindById(id);
        //    if (dados == null)
        //    {
        //        dados = new Cliente();
        //    }

        //    var taxas = _dbParametroTaxa.All().ToList().FirstOrDefault();

        //    ViewBag.MultaDe = (int)taxas.MultaDe;
        //    ViewBag.MultaAte = (int)taxas.MultaAte;

        //    ViewBag.JurosDe = (int)taxas.JurosDe;
        //    ViewBag.JurosAte = (int)taxas.JurosAte;

        //    ViewBag.HonorarioPercentualDe = (int)taxas.HonorarioPercentualDe;
        //    ViewBag.HonorarioPercentualAte = (int)taxas.HonorarioPercentualAte;

        //    ViewBag.DescontoDe = 0;
        //    ViewBag.DescontoAte = (int)taxas.DescontoPercentualValor;

        //    var sintese = contrato.FindAll(x => x.Cod == id).OrderByDescending(x => x.DataCadastro);
        //    // sintese.FirstOrDefault().LoginAtendimento = WebRotinas.ObterUsuarioLogado().Cod;
        //    sintese.FirstOrDefault().DataAtendimento = DateTime.Now;
        //    contrato.CreateOrUpdate(sintese.FirstOrDefault());

        //    var clientePrincipal = _dbCliente.FindById(id);
        //    ViewBag.ClientePrincipal = clientePrincipal.Nome;
        //    var CodTipoCobranca = _dbContrato.FindById(id).CodTipoCobranca;

        //    ViewBag.ListaFases = Fases(Convert.ToInt32(CodTipoCobranca));
        //    ViewBag.ListaMotivoAtraso = _dbListas.MotivoAtraso();
        //    ViewBag.ListaResolucao = _dbListas.Resulucao();

        //    //if (dados.ContratoHistoricoes.LastOrDefault() != null)
        //    //{
        //    //    ViewBag.ListaSintese = Sintese(dados.ContratoHistoricoes.LastOrDefault().TabelaSintese.CodFase);
        //    //}
        //    //else
        //    //{
        //    //    ViewBag.ListaSintese = new List<DictionaryEntry>();
        //    //}
        //    List<TabelaSintese> listaTabelaSintese = new List<TabelaSintese>();
        //    listaTabelaSintese = _dbSintese.All().ToList();
        //    ViewBag.ListaTodasSintese = listaTabelaSintese.OrderBy(x => x.Sintese).ToList();
        //    List<TabelaMotivoAtraso> listaTabelaMotivoAtraso = new List<TabelaMotivoAtraso>();
        //    listaTabelaMotivoAtraso = _dbMotivoAtraso.All().ToList();
        //    ViewBag.ListaTodosMotivos = listaTabelaMotivoAtraso.OrderBy(x => x.MotivoAtraso).ToList();


        //    //TELEFONES TESTE
        //    List<ClienteContato> t = new List<ClienteContato>();
        //    //t.Add(new ClienteContato {
        //    //    Contato = "",
        //    //    Tipo = (short)Enumeradores.ContatoTipo.TelefoneComercial    
        //    //});
        //    var listaAcordo = new List<ContratoParcelaAcordo>();
        //    if (_dbContratoAcordo.FindAll(x => x.CodContrato == id).Count() > 0)
        //    {
        //        listaAcordo = _dbContratoAcordo.FindAll(x => x.CodContrato == id).ToList();
        //    }
        //    ViewBag.ListaParcelaAcordo = listaAcordo;


        //    var parcelaCliente = _qry.ObterParcelaPorCliente(CodCliente).ToList();
        //    List<ContratoParcela> contratoParcela = new List<ContratoParcela>();
        //    foreach (var parcelaItem in parcelaCliente)
        //    {
        //        contratoParcela.Add(_dbContratoParcela.FindById(parcelaItem.Cod));
        //    }

        //    ViewBag.parcelasCliente = contratoParcela.ToList();

        //    if (dados != null)
        //    {
        //        foreach (var item in contratoParcela)
        //        {
        //            _qry.executarAtualizaValorParcela(item.Cod);
        //        }
        //    }

        //    return View(dados);
        //}



        [HttpPost]
        public JsonResult SalvarHistorico(Historico dados)
        {
            try
            {
                dados.CodMotivoAtraso = dados.CodMotivoAtraso > 0 ? dados.CodMotivoAtraso : null;
                dados.CodResolucao = dados.CodResolucao > 0 ? dados.CodResolucao : null;
                dados.Observacao = DbRotinas.Criptografar(dados.Observacao);
                _dbHistorico.CreateOrUpdate(dados);
                if (dados.CodContrato > 0)
                {
                    _qry.computarHistoricoPorContrato((int)dados.CodContrato);
                }
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpPost]
        public JsonResult InformacaoContrato(int CodContrato = 0)
        {
            try
            {
                var dados = _dbContrato.FindById(CodContrato);



                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpPost]
        public JsonResult BloquearContrato(int Cod = 0, bool Status = false)
        {
            try
            {
                var dados = _dbContrato.FindById(Cod);
                dados.Bloqueado = Status;
                _dbContrato.CreateOrUpdate(dados);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpGet]
        public ActionResult AcordoCliente(int id = 0)
        {
            if (id == 0) { id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato); }
            var cdCliente = 0;
            if (cdCliente == 0) { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }
            if (cdCliente == 0) { return RedirectToAction("ConsultaPorCliente"); }
            ViewBag.Cliente = _dbCliente.FindById(cdCliente);
            var dados = _dbCliente.FindById(cdCliente);

            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == cdCliente).ToList();
            ViewBag.Contratos = contratos.ToList();

            var parcelasAcordo = _qry.ObterParcelaAcordoPorCliente(cdCliente).ToList();
            List<Acordo> listaAcordo = new List<Acordo>();
            foreach (var i in parcelasAcordo)
            {
                var acordo = _dbAcordoContratoParcela.FindAll(x => x.CodAcordo == (int)i.Cod);
                if (acordo.Count() > 0)
                {
                    listaAcordo.Add(_dbAcordo.FindById((int)i.Cod));
                }
            }

            ViewBag.ListaAcordo = listaAcordo;



            var taxas = _dbParametroTaxa.All().ToList().FirstOrDefault();

            ViewBag.MultaDe = (int)taxas.MultaDe;
            ViewBag.MultaAte = (int)taxas.MultaAte;

            ViewBag.JurosDe = (int)taxas.JurosDe;
            ViewBag.JurosAte = (int)taxas.JurosAte;

            ViewBag.HonorarioPercentualDe = (int)taxas.HonorarioPercentualDe;
            ViewBag.HonorarioPercentualAte = (int)taxas.HonorarioPercentualAte;

            ViewBag.DescontoDe = 0;
            ViewBag.DescontoAte = (int)taxas.DescontoPercentualValor;



            contratos.Insert(0, new Contrato());
            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.dropContratos = listaContratos.ToList();

            return View(dados);
        }
        [HttpPost]
        public JsonResult SalvarTipoBloqueio(int Cod = 0, int Bloqueio = 0)
        {
            try
            {
                var dados = _dbContrato.FindById(Cod);
                if (Bloqueio == 0)
                {
                    dados.CodTipoBloqueio = null;
                }
                else
                {
                    dados.CodTipoBloqueio = Bloqueio;
                }
                _dbContrato.CreateOrUpdate(dados);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpPost]
        public JsonResult FilaAtendimento(int CodCliente = 0)
        {
            Repository<Cliente> cliente = new Repository<Cliente>();
            Dados obj = new Dados();

            var lista = obj.ConsultaCasosAtendimentoPorCliente(CodCliente);

            if (lista.Count() > 0)
            {
                CodCliente = (int)lista.FirstOrDefault().CodCliente;
            }

            WebRotinas.CookieObjetoGravar(_filtroCodClienteAtendimento, CodCliente);
            var clienteAtendimento = cliente.FindById(CodCliente);
            clienteAtendimento.DataAtendimento = DateTime.Now;
            cliente.CreateOrUpdate(clienteAtendimento);

            ClienteAtendimento atendimentoCliente = new ClienteAtendimento();
            atendimentoCliente.CodCliente = CodCliente;
            //atendimentoCliente.CodUsuario = WebRotinas.ObterClienteLogado().Cod;
            atendimentoCliente.DataAtendimento = DateTime.Now;
            atendimento.CreateOrUpdate(atendimentoCliente);
            return Json(new { OK = true });
        }

        [Authorize]
        [HttpGet]
        public ActionResult Negociacao(int id = 0, int CodCliente = 0, int sintese = 0, string p = null, string u = null)
        {
            Repository<Contrato> contrato = new Repository<Contrato>();
            Repository<Cliente> cliente = new Repository<Cliente>();
            DeParaModeloAcao depara = new DeParaModeloAcao();
            Dados _sql = new Dados();

            if (CodCliente == 0)
            {
                CodCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente);
                if (CodCliente > 0)
                {
                    return RedirectToAction("Negociacao", new { id = id, CodCliente = CodCliente });
                }
                else
                {
                    return RedirectToAction("ConsultaPorCliente");
                }
            }
            if (sintese == 0)
            {
                var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
                if (filtro.CodSinteseParcela > 0)
                {
                    return RedirectToAction("Negociacao", new
                    {
                        id = id,
                        CodCliente = CodCliente,
                        sintese = filtro.CodSinteseParcela,
                        p = DbRotinas.Descriptografar(filtro.CodPerfilTipo.ToString()),
                        u = DbRotinas.Descriptografar(filtro.CodUsuarioLogado.ToString())
                    });
                }
            }


            var script = _sql.ConsultaScript(CodCliente);

            //ATUALIZA OS DADOS DO CLIENTE.
            //_sql.ComputaValoresCliente(CodCliente);

            ViewBag.ScriptAtendimento = script.Count() > 0 ? depara.DeParaCampos(script.FirstOrDefault().Modelo, CodCliente) : "Não há script para este tipo de cobrança ou faixa de atraso";
            var dados = _dbCliente.FindById(CodCliente);
            if (dados == null) { dados = new Cliente(); }
            else
            {
                FiltroGeralDados Filtro = WebRotinas.FiltroGeralObter();

                //CASO O USUÁRIO FILTRAR UM CONTRATO DEVOLVE OS ENDEREÇOS DAQUELE CONTRATO DETERMINADO
                if (Filtro.NumeroContrato != null)
                {
                    var nrContratos = Filtro.NumerosContratosCriptografadoArray;
                    List<int> listaContratos = new List<int>();
                    listaContratos = _dbContratoCliente
                                        .FindAll(x => !x.DataExcluido.HasValue
                                                 && x.CodCliente == dados.Cod
                                                 && nrContratos.Any(key => x.Contrato.NumeroContrato.Contains(key))
                                                 ).Select(x => (int)x.CodContrato)
                                                 .ToList();


                    // ENCONTRA O ENDEREÇO APENAS DE DETERMINADO CONTRATO FILTRADO
                    var enderecos = dados.ClienteEnderecoes.Where(x => listaContratos.Contains((int)x.CodContrato)).ToList();

                    // DEVOLVE OS ENDERECOS FILTRADOS
                    dados.ClienteEnderecoes = dados.ClienteEnderecoes.Intersect(enderecos).ToList();
                }
            }


            var taxas = _dbParametroTaxa.All().ToList().FirstOrDefault();

            try
            {
                List<TabelaFase> listaTabelaFase = new List<TabelaFase>();
                listaTabelaFase = _dbFase.FindAll(x => x.TabelaSintese.Any(c => c.TarefaAtendimento == true && !c.DataExcluido.HasValue)).ToList();
                ViewBag.ListaFase = listaTabelaFase;

                ViewBag.ListaMotivoAtraso = _dbListas.MotivoAtraso();
                ViewBag.ListaResolucao = _dbListas.Resulucao();

                List<TabelaSintese> listaTabelaSintese = new List<TabelaSintese>();
                listaTabelaSintese = _dbSintese.All().ToList();
                ViewBag.ListaTodasSintese = listaTabelaSintese.Where(c => c.TarefaAtendimento == true && !c.DataExcluido.HasValue).OrderBy(x => x.Sintese).ToList();

                List<TabelaMotivoAtraso> listaTabelaMotivoAtraso = new List<TabelaMotivoAtraso>();
                listaTabelaMotivoAtraso = _dbMotivoAtraso.All().ToList();
                ViewBag.ListaTodosMotivos = listaTabelaMotivoAtraso.OrderBy(x => x.MotivoAtraso).ToList();

                var usuarioLogado = WebRotinas.ObterUsuarioLogado();
                //CRIA UMA LISTA DE ROTAS DEFINIDAS PARA O USUARIO
                List<Rota> listaRotas = new List<Rota>();
                List<ParametroAtendimentoRota> ParametroRotasQueryable = _dbParametroAtendimentoRota.All().Where(x => x.ParametroAtendimento.CodUsuario == usuarioLogado.Cod).ToList();
                if (ParametroRotasQueryable.Count > 0)
                {
                    listaRotas = (List<Rota>)ParametroRotasQueryable.ToList().Select(x => x.Rota).ToList();
                }
                List<string> rotas = listaRotas.Select(x => x.Nome).ToList();
                List<Contrato> listaContratos = new List<Contrato>();
                listaContratos = _dbContrato.FindAll(x => !x.DataExcluido.HasValue && x.CodClientePrincipal == CodCliente).ToList();



                //MUDANÇA NA REGRA -- USUARIO PODE VER CONTRATOS DE OUTRA REGIONAL
                //if (usuarioLogado.CodRegional > 0)
                //{
                //   
                //    listaContratos = (from p in listaContratos
                //                      where p.TabelaRegionalProduto.CodRegional == usuarioLogado.CodRegional
                //                      select p).ToList();
                //}
                var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
                var codSintese = filtro.CodSintese;
                if (codSintese > 0)
                {
                    //if (usuarioLogado.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                    //{
                    //    listaContratos = listaContratos.Where(x => rotas.Contains(x.RotaArea)).ToList();
                    //}
                    listaContratos = listaContratos.Where(x => x.CodHistorico.HasValue).ToList();
                    listaContratos = (from c in listaContratos
                                      join h in listaContratos.Select(x => x.Historico).ToList() on c.CodHistorico equals h.Cod
                                      where (h.CodSintese == codSintese || codSintese == 0)
                                      && c.ValorAtraso > 0
                                      select c).ToList();
                }

                var codSinteseParcela = filtro.CodSinteseParcela;
                ViewBag.CodSinteseContrato = filtro.CodSintese;
                ViewBag.CodSinteseParcela = filtro.CodSinteseParcela;

                List<int> contratoIds = listaContratos.Select(c => c.Cod).ToList();
                if (codSinteseParcela > 0)
                {
                    //if (usuarioLogado.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                    //{
                    //    listaContratos = listaContratos.Where(x => rotas.Contains(x.RotaArea)).ToList();
                    //}
                    contratoIds = listaContratos.Select(x => x.Cod).ToList();
                    List<ContratoParcela> parcelas = _dbContratoParcela.FindAll(x => !x.DataExcluido.HasValue && contratoIds.Contains((int)x.CodContrato) && x.CodHistorico > 0).ToList();
                    listaContratos = parcelas.Where(x => x.Historico.CodSintese == codSinteseParcela).GroupBy(x => x.Contrato).Select(x => x.Key).ToList();
                }

                ViewBag.ListaContratos = listaContratos.ToList();
                ViewBag.contratoids = listaContratos.Select(x => x.Cod).ToList();
                var listaCredito = _dbClienteCredito.All().Where(x => x.CodCliente == CodCliente).ToList();
                ViewBag.ListaCreditoCliente = listaCredito.ToList();


                List<ContratoAcaoCobrancaHistorico> listaAcaoCobrancaHistorico = new List<ContratoAcaoCobrancaHistorico>();
                listaAcaoCobrancaHistorico = _dbAcaoCobrancaHistorico.FindAll(x => x.DataExcluido.HasValue && contratoIds.Contains((int)x.CodContrato)).ToList();
                ViewBag.acaoCobranca = listaAcaoCobrancaHistorico.ToList();
                //var parcelaCliente = _qry.ObterParcelaPorCliente(CodCliente).ToList();
                //List<ContratoParcela> contratoParcela = new List<ContratoParcela>();
                //foreach (var parcelaItem in parcelaCliente)
                //{
                //    contratoParcela.Add(_dbContratoParcela.FindById(parcelaItem.Cod));
                //}

                //ViewBag.parcelasCliente = contratoParcela.ToList();

                //if (dados != null)
                //{
                //    foreach (var item in contratoParcela)
                //    {
                //        _qry.executarAtualizaValorParcela(item.Cod);
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
            return View(dados);
        }

        [HttpGet]
        public ActionResult Acordo(int id = 0)
        {
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato);
            }
            var dados = _dbContrato.FindById(id);
            var taxas = _dbParametroTaxa.All().ToList().FirstOrDefault();

            ViewBag.MultaDe = (int)taxas.MultaDe;
            ViewBag.MultaAte = (int)taxas.MultaAte;

            ViewBag.JurosDe = (int)taxas.JurosDe;
            ViewBag.JurosAte = (int)taxas.JurosAte;

            ViewBag.HonorarioPercentualDe = (int)taxas.HonorarioPercentualDe;
            ViewBag.HonorarioPercentualAte = (int)taxas.HonorarioPercentualAte;

            ViewBag.DescontoDe = 0;
            ViewBag.DescontoAte = (int)taxas.DescontoPercentualValor;

            var listaAcordo = new List<ContratoParcelaAcordo>();

            if (_dbContratoAcordo.FindAll(x => x.CodContrato == id).Count() > 0)
            {
                listaAcordo = _dbContratoAcordo.FindAll(x => x.CodContrato == id).ToList();
            }
            ViewBag.ListaParcelaAcordo = listaAcordo;

            return View(dados);
        }
        [HttpPost]
        public JsonResult ExcluirParcelaAcordo(int Cod)
        {
            try
            {
                var dados = _dbAcordoContratoParcela.FindAll(x => x.CodAcordo == Cod).ToList();
                foreach (var item in dados)
                {
                    _dbAcordoContratoParcela.Delete(item.Cod);
                }

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpPost]
        public JsonResult AlterarDataVencimento(string DataPagamento, int Cod = 0)
        {
            try
            {
                var dados = _dbAcordo.FindById(Cod);
                dados.DataVencimento = Convert.ToDateTime(DataPagamento);
                _dbAcordo.CreateOrUpdate(dados);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpGet]
        public FileResult VisualizaDocumento(string GUID)
        {
            //var doc = _dbPropostaDocumento.All().Where(x => x.ArquivoGuid == GUID).First();
            //string caminhoReal = Server.MapPath(doc.ArquivoCaminho);
            //return File(caminhoReal, "application/unknown", doc.ArquivoNome);


            var doc = _dbContratoDocumento.All().Where(x => x.ArquivoGuid == GUID).First();

            string pathTemp = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            if (!Directory.Exists(pathTemp))
                Directory.CreateDirectory(pathTemp);
            string caminhoTemp = Path.Combine(pathTemp, doc.ArquivoNome);

            string caminhoReal = Server.MapPath(doc.ArquivoCaminho);
            System.IO.File.Copy(caminhoReal, caminhoTemp);
            FileInfo f = new FileInfo(caminhoTemp);

            var ct = MimeMapping.GetMimeMapping(doc.ArquivoNome);
            FileStream stream = new FileStream(caminhoTemp, FileMode.Open);
            FileStreamResult result = new FileStreamResult(stream, ct);

            return result;
        }

        [HttpGet]
        public ActionResult FiltroGeralShwitcher()
        {
            FiltroGeralDados filtro = WebRotinas.FiltroGeralObter();
            PreencherListasFiltro(filtro);
            return PartialView("_filter", filtro);
        }
        [HttpPost]
        public ActionResult FiltroGeralShwitcher(FiltroGeralDados Dados)
        {
            if (Dados.AgingIds != null)
            {
                Dados.CodAgingCliente = DbRotinas.StringJoinSemErro(Dados.AgingIds.Select(key => DbRotinas.ConverterParaInt(key)).ToList());
            }
            if (Dados.ProdutosIds != null)
            {
                Dados.CodProduto = DbRotinas.StringJoinSemErro(Dados.ProdutosIds.Select(key => DbRotinas.ConverterParaInt(key)).ToList());
            }
            if (Dados.RegionalIds != null)
            {
                Dados.CodRegionais = DbRotinas.StringJoinSemErro(Dados.RegionalIds.Select(key => DbRotinas.ConverterParaInt(key)).ToList());
            }

            FiltroGeralDados filtro = WebRotinas.AplicarFiltros(Dados);
            PreencherListasFiltro(filtro);
            return PartialView("_filter", filtro);
        }
        [HttpGet]
        public ActionResult ClearFiltroGeralShwitcher(FiltroGeralDados dados)
        {
            dados = new FiltroGeralDados();
            FiltroGeralDados filtro = WebRotinas.AplicarFiltros(dados);
            PreencherListasFiltro(filtro);

            return PartialView("_filter", filtro);
        }


        private void PreencherListasFiltro(FiltroGeralDados filtro)
        {
            var where = String.Empty;
            UsuarioLogadoInfo usuario = WebRotinas.ObterUsuarioLogado();

            //PREENCHER SelectListRegional
            ViewBag.ListaRegional = new SelectList(UtilsRotinas.GetSelectListItemRegional(where, filtro.CodRegionaisArray));

            ViewBag.ListaProduto = new SelectList(UtilsRotinas.GetSelectListItemProduto(where, filtro.CodProdutoArray));

            where = String.Empty;
            ViewBag.ListaAging = new SelectList(UtilsRotinas.GetSelectListItemAging(where, filtro.CodAgingClienteArray));

            bool admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;
            var cdReginal = WebRotinas.ObterUsuarioLogado().CodRegional;
            ViewBag.ListaUsuarioAtendimento = _dbUsuarioRegional.FindAll(x => x.CodRegional == (int)cdReginal || admin).Select(x => x.Usuario).ToList();
            ViewBag.ListaUsuarioAtendimento.Insert(0, new Usuario() { Cod = 0, Nome = "--" });
        }

        [HttpPost]
        public JsonResult ObterDocDadosPorServer(int cdCliente, DTParameters param)
        {
            try
            {
                var CodCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente);
                if (cdCliente > 0)
                {
                    CodCliente = cdCliente;
                }
                var dtsource = ListaDocumentosDados(CodCliente);

                var result = WebRotinas.DTRotinas.DTResult(param, dtsource);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public IQueryable<object> ListaDocumentosDados(int filtro)
        {
            var UsuarioId = WebRotinas.ObterUsuarioLogado().Cod;
            var dados = _dbCliente.FindById(filtro);
            IQueryable<object> data = null;
            data = dados.ContratoDocumentoes
                        .Where(w => !w.DataExcluido.HasValue)
                        .Select(x => new
                        {
                            DataDocumento = x.DataCadastro.Date,
                            DataDocumentoDesc = $"{x.DataCadastro:dd/MM/yyyy}",
                            ArquivoGuid = x.ArquivoGuid,
                            ArquivoNome = x.ArquivoNome,
                            TipoDocumento = x.TabelaTipoDocumento.TipoDocumento,
                            Observacao = $"{x.ArquivoObservacao}",
                            Usuario = x.CodUsuario > 0 ? $"{x.Usuario.Nome}" : "",
                            IsRemove = x.CodUsuario == UsuarioId ? true : false
                        }).AsQueryable();

            return data;
        }

        [HttpGet]
        public ActionResult CarregarMenuSolicitacao()
        {
            var user = WebRotinas.ObterUsuarioLogado();
            Nullable<int> usuarioId = null;
            short? tipoFluxo = null;
            string filiais = null;
            if (user.PerfilTipo == Enumeradores.PerfilTipo.Analista)
            {
                tipoFluxo = (short)Enumeradores.PerfilTipo.Analista;
            }
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista)
            {
                tipoFluxo = (short)Enumeradores.PerfilTipo.SupervisorAnalista;
            }
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional || user.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                {
                    usuarioId = user.Cod;
                }
                tipoFluxo = (short)Enumeradores.TipoFluxo.FilialBoleto;
                var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == user.Cod && !x.DataExcluido.HasValue);
                filiais = string.Join(",", regionais.Select(x => x.CodRegional).ToArray());
            }
            var resultadoParcelaBoleto = _qry.ObterTotaisBoletoSolicitacao(usuarioId, tipoFluxo, filiais);

            return PartialView("_menuSolicitacao", resultadoParcelaBoleto);
        }


    }

}
