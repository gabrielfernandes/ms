﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cobranca.Web.Controllers
{
    public class ErrorController : Controller
    {

        public ActionResult Index()
        {
            return RedirectToAction("ServerError");
        }
        // GET: Error
        [HttpGet,HandleError]
        public ActionResult ServerError()//
        {
            Exception exception = Server.GetLastError();
            if(exception == null)
            {
                if(Session["_getLastError"] != null)
                {
                    exception = (Exception)Session["_getLastError"];
                }
            }
            string Mensagem = exception.Message;            
            return View(exception);
        }


        [HttpPost]
        public ActionResult GetError(string msg = null)
        {
            var model = new Exception();
            ViewBag.Mensagem = msg;
            string Mensagem = model.Message;

            return PartialView("PopupError", model);
        }
    }
}