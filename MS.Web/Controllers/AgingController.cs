﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class AgingController : BasicController<TabelaAging>
    {

        public override void validacaoCustom(TabelaAging entity)
        {

            if(entity.AtrasoDe > entity.AtrasoDe)
            {
                ModelState.AddModelError("AtrasoDe", "Valor é superidor ao atraso até");
            }


            base.validacaoCustom(entity);
        }

    }
}