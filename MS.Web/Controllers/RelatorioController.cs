﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using System.Collections;
using Cobranca.Db.Rotinas;
using Recebiveis.Web.Models;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Web.Views.Relatorio;
using Cobranca.Web.DataSet.Cobranca;
using Cobranca.Web.DataSet.Cobranca.DataSet_CobrancaTableAdapters;
using DevExpress.XtraPrinting.InternalAccess;
using DevExpress.XtraReports.UI;
using System.Data;
using DevExpress.Web.Mvc;
using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Domain.CobrancaContext.Enums;

namespace Cobranca.Web.Controllers
{

    [Authorize]
    public class RelatorioController : Controller
    {
        public const string _filtroSessionId = "FiltroRelatorio";
        public const string _filtroAtendimentoAgendadosSessionId = "FiltroAtendimentoAgendadosSessionId";

        public Repository<TabelaEscritorio> _dbEscritorio = new Repository<TabelaEscritorio>();
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        public Repository<TabelaProduto> _dbProduto = new Repository<TabelaProduto>();
        public Repository<TabelaAcaoCobranca> _dbAcaoCobranca = new Repository<TabelaAcaoCobranca>();
        private Repository<Usuario> _dbUsuario = new Repository<Usuario>();
        private Repository<UsuarioRegional> _dbUsuarioRegional = new Repository<UsuarioRegional>();
        Repository<TabelaGrupo> _dbTabelaGrupo = new Repository<TabelaGrupo>();
        Repository<Cliente> _dbCliente = new Repository<Cliente>();
        Repository<TabelaTipoCobrancaAcao> _dbTipoCobrancaAcao = new Repository<TabelaTipoCobrancaAcao>();

        public Dados _qryDados = new Dados();

        [HttpGet]
        public ActionResult Index()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            return RedirectToAction("AtendimentoAgendados");

        }

        #region Historico Acoes
        [HttpGet]
        public ActionResult HistoricoAcoes()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();

            List<TabelaAcaoCobranca> listaAcoesCobranca = new List<TabelaAcaoCobranca>();
            listaAcoesCobranca = _dbAcaoCobranca.All().OrderBy(x => x.AcaoCobranca).ToList();
            listaAcoesCobranca.Insert(0, new TabelaAcaoCobranca());
            ViewBag._listaAcaoCobranca = from p in listaAcoesCobranca.ToList() select new DictionaryEntry(p.AcaoCobranca, p.Cod);


            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult HistoricoAcoes(RelatorioViewModel filtro)
        {
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            List<TabelaAcaoCobranca> listaAcoesCobranca = new List<TabelaAcaoCobranca>();
            listaAcoesCobranca = _dbAcaoCobranca.All().OrderBy(x => x.AcaoCobranca).ToList();
            listaAcoesCobranca.Insert(0, new TabelaAcaoCobranca());
            ViewBag._listaAcaoCobranca = from p in listaAcoesCobranca.ToList() select new DictionaryEntry(p.AcaoCobranca, p.Cod);


            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Produtividade
        [HttpGet]
        public ActionResult Produtividade()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();

            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult Produtividade(RelatorioViewModel filtro)
        {
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Cliente Contatados
        [HttpGet]
        public ActionResult ClienteContatado()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();

            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult ClienteContatado(RelatorioViewModel filtro)
        {
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Acordos Não Realizados
        [HttpGet]
        public ActionResult AcordosNaoRealizado()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();

            model.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.AcordosNaoRealizado;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);

            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult AcordosNaoRealizado(RelatorioViewModel filtro)
        {
            filtro.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.AcordosNaoRealizado;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Acordos Realizados
        [HttpGet]
        public ActionResult AcordosRealizado()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();

            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult AcordosRealizado(RelatorioViewModel filtro)
        {
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Cliente Credito
        [HttpGet]
        public ActionResult ClienteCredito()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();

            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult ClienteCredito(RelatorioViewModel filtro)
        {
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Parcelas Atrasada
        [HttpGet]
        public ActionResult ParcelaAtrasada()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();
            model.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ParcelaEmAtraso;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);

            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult ParcelaAtrasada(RelatorioViewModel filtro)
        {
            filtro.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ParcelaEmAtraso;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Parcela Paga

        [HttpGet]
        public ActionResult ParcelaPaga()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();
            model.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ParcelaPaga;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);


            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult ParcelaPaga(RelatorioViewModel filtro)
        {
            filtro.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ParcelaPaga;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Parcela Aberta
        [HttpGet]
        public ActionResult ParcelaAberta()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();

            model.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ParcelaAberto;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);


            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult ParcelaAberta(RelatorioViewModel filtro)
        {
            filtro.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ParcelaAberto;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Cliente Telefone
        [HttpGet]
        public ActionResult ClienteTelefone()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();

            model.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ClienteSemContato;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);

            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult ClienteTelefone(RelatorioViewModel filtro)
        {

            filtro.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ClienteSemContato;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Cliente Email
        [HttpGet]
        public ActionResult ClienteEmail()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();

            model.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ClienteSemEmail;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);

            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult ClienteEmail(RelatorioViewModel filtro)
        {
            filtro.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ClienteSemEmail;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Cliente CPF / CNPJ
        [HttpGet]
        public ActionResult ClienteCpfCnpj()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            if (model == null)
                model = new RelatorioViewModel();

            model.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ClienteSemCpfCnpj;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);

            PreencherViewBags(model);

            return View(model);

        }
        [HttpPost]
        public ActionResult ClienteCpfCnpj(RelatorioViewModel filtro)
        {
            filtro.RelatorioFiltroTipo = (short)Enumeradores.TipoRelatorio.ClienteSemEmail;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            RelatorioViewModel model = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);

            PreencherViewBags(model);
            return View(model);
        }
        #endregion

        #region Parcelas sem acoes de cobranca
        [HttpGet]
        public ActionResult ParcelaAcaoCobranca()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioPesquisaViewModel model = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroAtendimentoAgendadosSessionId);
            if (model == null)
                model = new RelatorioPesquisaViewModel();
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);


            return View(model);

        }

        [HttpPost]
        public ActionResult ParcelaAcaoCobranca(RelatorioPesquisaViewModel filtro)
        {
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            return View(filtro);
        }
        #endregion ReguaSemAcoes

        private void PreencherViewBags(RelatorioViewModel model)
        {

            List<TabelaEscritorio> listaEscritorio = new List<TabelaEscritorio>();
            listaEscritorio = _dbEscritorio.All().OrderBy(x => x.Escritorio).ToList();
            listaEscritorio.Insert(0, new TabelaEscritorio());
            ViewBag.ListaEscritorio = from p in listaEscritorio.ToList() select new DictionaryEntry(p.Escritorio, p.Cod);

            List<TabelaConstrutora> listaConstrutora = new List<TabelaConstrutora>();
            listaConstrutora = _dbConstrutora.All().OrderBy(x => x.Cod).ToList();
            foreach (var item in listaConstrutora)
            {
                item.NomeConstrutora = DbRotinas.Descriptografar(item.NomeConstrutora);
                item.CNPJ = DbRotinas.Descriptografar(item.CNPJ);
            }
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora.ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            List<TabelaProduto> listaProduto = new List<TabelaProduto>();
            listaProduto = _dbProduto.All().OrderBy(x => x.Cod).ToList();
            foreach (var item in listaProduto)
            {
                item.Produto = DbRotinas.Descriptografar(item.Produto);
                item.Descricao = DbRotinas.Descriptografar(item.Descricao);
            }
            listaProduto.Insert(0, new TabelaProduto());
            ViewBag.ListaProduto = from p in listaProduto.ToList() select new DictionaryEntry(p.Produto, p.Cod);

            List<TabelaRegional> listaFilial = new List<TabelaRegional>();
            listaFilial = _dbRegional.All().OrderBy(x => x.Cod).ToList();
            foreach (var item in listaFilial)
            {
                item.Regional = DbRotinas.Descriptografar(item.Regional);
            }
            listaFilial.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaFilial.ToList() select new DictionaryEntry(p.Regional, p.Cod);

        }

        [HttpPost]
        public ActionResult paginarDados(int draw, int start, int length, Dictionary<string, string> search, List<Dictionary<string, string>> order, List<Dictionary<string, string>> columns)
        {
            var filtro = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            var searchString = Request["search[value]"];
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"]);
            var sortColumnsName = Request[string.Format("columns[{0}][name]", sortColumnIndex)];
            var sortDirection = Request["order[0][dir]"]; // asc or desc


            var query_dados = _qryDados.ListarRelatorioDados(
                filtro.CodConstrutora,
                filtro.CodRegional,
                filtro.CodEscritorio,
                filtro.CodProduto
                ).OrderBy(string.Format("{0} {1}", sortColumnsName, sortDirection));

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count();
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult paginarContatado(int draw, int start, int length, Dictionary<string, string> search, List<Dictionary<string, string>> order, List<Dictionary<string, string>> columns)
        {
            var filtro = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            var searchString = Request["search[value]"];
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"]);
            var sortColumnsName = Request[string.Format("columns[{0}][name]", sortColumnIndex)];
            var sortDirection = Request["order[0][dir]"]; // asc or desc


            var query_dados = _qryDados.ListarRelatorioContatado(
                filtro.CodConstrutora,
                filtro.CodRegional,
                filtro.CodEscritorio,
                filtro.CodProduto
                );

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count();
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult creditoDados(int draw, int start, int length, Dictionary<string, string> search, List<Dictionary<string, string>> order, List<Dictionary<string, string>> columns)
        {
            var filtro = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            var searchString = Request["search[value]"];
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"]);
            var sortColumnsName = Request[string.Format("columns[{0}][name]", sortColumnIndex)];
            var sortDirection = Request["order[0][dir]"]; // asc or desc


            var query_dados = _qryDados.ListarRelatorioCredito(
                filtro.CodConstrutora,
                filtro.CodRegional,
                filtro.CodEscritorio,
                filtro.CodProduto
                ).OrderBy(string.Format("{0} {1}", sortColumnsName, sortDirection));

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count();
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult acordoDados(int draw, int start, int length, Dictionary<string, string> search, List<Dictionary<string, string>> order, List<Dictionary<string, string>> columns)
        {
            var filtro = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            var searchString = Request["search[value]"];
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"]);
            var sortColumnsName = Request[string.Format("columns[{0}][name]", sortColumnIndex)];
            var sortDirection = Request["order[0][dir]"]; // asc or desc


            var query_dados = _qryDados.ListarRelatorioAcordos(
                filtro.CodConstrutora,
                filtro.CodRegional,
                filtro.CodEscritorio,
                filtro.CodProduto
                ).OrderBy(string.Format("{0} {1}", sortColumnsName, sortDirection));

            if (filtro.RelatorioFiltroTipo == (short)Enumeradores.TipoRelatorio.AcordosNaoRealizado)
            {
                query_dados = query_dados.Where(x => x.TotalParcelaAtrasada > 0).ToList();

            }

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count();
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult acoesCobrancaDados(int draw, int start, int length, Dictionary<string, string> search, List<Dictionary<string, string>> order, List<Dictionary<string, string>> columns)
        {
            var filtro = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            var searchString = Request["search[value]"];
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"]);
            var sortColumnsName = Request[string.Format("columns[{0}][name]", sortColumnIndex)];
            var sortDirection = Request["order[0][dir]"]; // asc or desc


            var query_dados = _qryDados.ListarRelatorioAcoesCobranca(
                filtro.CodConstrutora,
                filtro.CodRegional,
                filtro.CodEscritorio,
                filtro.CodProduto
                ).OrderBy(string.Format("{0} {1}", sortColumnsName, sortDirection));

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count();
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult parcelaDados(int draw, int start, int length, Dictionary<string, string> search, List<Dictionary<string, string>> order, List<Dictionary<string, string>> columns)
        {
            var filtro = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            var searchString = Request["search[value]"];
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"]);
            var sortColumnsName = Request[string.Format("columns[{0}][name]", sortColumnIndex)];
            var sortDirection = Request["order[0][dir]"]; // asc or desc


            var query_dados = _qryDados.ListarRelatorioParcela(
                filtro.CodConstrutora,
                filtro.CodRegional,
                filtro.CodEscritorio,
                filtro.CodProduto
                ).OrderBy(string.Format("{0} {1}", sortColumnsName, sortDirection));

            if (filtro.RelatorioFiltroTipo == (short)Enumeradores.TipoRelatorio.ParcelaEmAtraso)
            {
                query_dados = query_dados.Where(x => x.ValorTotalAtraso > 0).ToList();

            }
            if (filtro.RelatorioFiltroTipo == (short)Enumeradores.TipoRelatorio.ParcelaAberto)
            {
                query_dados = query_dados.Where(x => x.ValorTotalEmAberto > 0).ToList();
            }
            if (filtro.RelatorioFiltroTipo == (short)Enumeradores.TipoRelatorio.ParcelaPaga)
            {
                query_dados = query_dados.Where(x => x.ValorTotalPago > 0).ToList();
            }

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count();
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult clienteDados(int draw, int start, int length, Dictionary<string, string> search, List<Dictionary<string, string>> order, List<Dictionary<string, string>> columns)
        {
            var filtro = WebRotinas.CookieObjetoLer<RelatorioViewModel>(_filtroSessionId);
            var searchString = Request["search[value]"];
            var sortColumnIndex = Convert.ToInt32(Request["order[0][column]"]);
            var sortColumnsName = Request[string.Format("columns[{0}][name]", sortColumnIndex)];
            var sortDirection = Request["order[0][dir]"]; // asc or desc


            var query_dados = _qryDados.ListarRelatorioCliente(
                filtro.CodConstrutora,
                filtro.CodRegional,
                filtro.CodEscritorio,
                filtro.CodProduto
                ).OrderBy(string.Format("{0} {1}", sortColumnsName, sortDirection));

            if (filtro.RelatorioFiltroTipo == (short)Enumeradores.TipoRelatorio.ClienteSemEmail)
            {
                query_dados = query_dados.Where(x => x.TotalEmail == 0).ToList();
            }
            if (filtro.RelatorioFiltroTipo == (short)Enumeradores.TipoRelatorio.ClienteSemCpfCnpj)
            {
                query_dados = query_dados.Where(x => x.CPF == null).ToList();
            }
            if (filtro.RelatorioFiltroTipo == (short)Enumeradores.TipoRelatorio.ClienteSemContato)
            {
                query_dados = query_dados.Where(x => x.TotalCelular == 0 && x.TotalComercial == 0).ToList();
            }

            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count();
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Atendimento()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioPesquisaViewModel model = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroAtendimentoAgendadosSessionId);
            if (model == null)
                model = new RelatorioPesquisaViewModel();
            var usuario = WebRotinas.ObterUsuarioLogado();
            bool admin = usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin;

            var listaRegional = _dbRegional.All().ToList();
            listaRegional.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaRegional.OrderBy(x => x.Regional).ToList() select new { key = p.Cod, value = DbRotinas.Descriptografar(p.Regional) };

            List<int> codUsuarios = _dbUsuarioRegional.FindAll(x => x.CodUsuario == (int)usuario.Cod).Select(x => x.CodRegional).ToList();

            ViewBag.ListaAtendentes = new SelectList(UtilsRotinas.GetSelectListItemAtendenteAdmFilial("", null));
           
            model.ReportCod = (short)Enumeradores.TipoRelatorio.Atendimento;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);

            return View(model);
        }

        [HttpGet]
        public ActionResult SemAtendimento()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioPesquisaViewModel model = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroAtendimentoAgendadosSessionId);
            if (model == null)
                model = new RelatorioPesquisaViewModel();
            var usuario = WebRotinas.ObterUsuarioLogado();
            bool admin = usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin;
            List<int> codUsuarios = _dbUsuarioRegional.FindAll(x => x.CodUsuario == (int)usuario.Cod).Select(x => x.CodRegional).ToList();

            var Regionais = _dbRegional.FindAll(x => codUsuarios.Contains(x.Cod) || admin).ToList();
            if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional)
            {
                Regionais = Regionais.Where(x => codUsuarios.Contains(x.Cod) || admin).ToList();
            }
            ViewBag.ListaRegionais = (from p in Regionais
                                      select new { key = p.Cod, value = DbRotinas.Descriptografar(p.Regional) }).ToList();
            if (usuario.PerfilTipo != Enumeradores.PerfilTipo.Regional)
            {
                ViewBag.ListaRegionais.Insert(0, new { key = 0, value = "--" });
            }
            model.ReportCod = (short)Enumeradores.TipoRelatorio.ParcelaSemAtendimento;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);

            var acaoCobranca = _dbAcaoCobranca.All().Where(x => x.Tipo == (short)Enumeradores.TipoAcaoCobranca.CONTATO).FirstOrDefault();
            model.CondicaoQuery = acaoCobranca.TabelaTipoCobrancaAcaos.Where(x => !x.DataExcluido.HasValue && x.PeriodoEmDias > 0 && x.PeriodoEmDias < 30 && x.CondicaoQuery != null).FirstOrDefault().CondicaoQuery;
            return View(model);

        }

        [HttpGet]
        public ActionResult AgingFilial()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioPesquisaViewModel model = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroAtendimentoAgendadosSessionId);
            if (model == null)
                model = new RelatorioPesquisaViewModel();
            var usuario = WebRotinas.ObterUsuarioLogado();
            bool admin = usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin;
            List<int> codUsuarios = _dbUsuarioRegional.FindAll(x => x.CodUsuario == (int)usuario.Cod).Select(x => x.CodRegional).ToList();

            var Regionais = _dbRegional.FindAll(x => codUsuarios.Contains(x.Cod) || admin).ToList();
            if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional)
            {
                Regionais = Regionais.Where(x => codUsuarios.Contains(x.Cod) || admin).ToList();
            }
            ViewBag.ListaRegionais = (from p in Regionais
                                      select new { key = p.Cod, value = DbRotinas.Descriptografar(p.Regional) }).ToList();
            if (usuario.PerfilTipo != Enumeradores.PerfilTipo.Regional)
            {
                ViewBag.ListaRegionais.Insert(0, new { key = 0, value = "--" });
            }
            model.ReportCod = (short)Enumeradores.TipoRelatorio.AgingFilial;
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);

            var acaoCobranca = _dbAcaoCobranca.All().Where(x => x.Tipo == (short)Enumeradores.TipoAcaoCobranca.CONTATO).FirstOrDefault();
            model.CondicaoQuery = acaoCobranca.TabelaTipoCobrancaAcaos.Where(x => !x.DataExcluido.HasValue && x.PeriodoEmDias > 0 && x.PeriodoEmDias < 30 && x.CondicaoQuery != null).FirstOrDefault().CondicaoQuery;
            return View(model);

        }

        [HttpGet]
        public ActionResult AtendimentoAgendados()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioPesquisaViewModel model = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroAtendimentoAgendadosSessionId);
            if (model == null)
                model = new RelatorioPesquisaViewModel();
            var usuario = WebRotinas.ObterUsuarioLogado();
            bool admin = usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin;
            List<int> codUsuarios = _dbUsuarioRegional.FindAll(x => x.CodUsuario == (int)usuario.Cod).Select(x => x.CodRegional).ToList();

            ViewBag.ListaAtendentes = new SelectList(UtilsRotinas.GetSelectListItemAtendenteAdmFilial("", null));

            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);
            model.ReportCod = (short)Enumeradores.TipoRelatorio.AtendimentoAgendados;

            return View(model);
        }

        [HttpPost]
        public ActionResult AtendimentoAgendados(RelatorioPesquisaViewModel filtro)
        {
            WebRotinas.CookieObjetoGravar(_filtroAtendimentoAgendadosSessionId, filtro);
            bool admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;
            var cdReginal = WebRotinas.ObterUsuarioLogado().CodRegional;

            ViewBag.ListaUsuarioAtendimento = _dbUsuarioRegional.FindAll(x => x.CodRegional == (int)cdReginal || admin).Select(x => x.Usuario).ToList();
            //ViewBag.ListaUsuarioAtendimento.Insert(0, new Usuario() { Cod = 0, Nome = "--" });
            return View(filtro);

        }

        [HttpGet]
        public ActionResult GestaoBoleto()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioPesquisaViewModel model = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroAtendimentoAgendadosSessionId);
            if (model == null)
                model = new RelatorioPesquisaViewModel();
            var usuario = WebRotinas.ObterUsuarioLogado();
            bool admin = usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin;
            List<int> codUsuarios = _dbUsuarioRegional.FindAll(x => x.CodUsuario == (int)usuario.Cod).Select(x => x.CodRegional).ToList();

            ViewBag.ListaAtendentes = new SelectList(UtilsRotinas.GetSelectListItemAtendenteAdmFilialAnalistaSupervisor("", null));
            ViewBag.ListaRegionais = new SelectList(UtilsRotinas.GetSelectListItemRegional("", null));

            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);
            model.ReportCod = (short)Enumeradores.TipoRelatorio.GestaoBoleto;

            model.DataPeriodoDe = DateTime.Now.AddDays(-7).Date;
            model.DataPeriodoAte = DateTime.Now.Date;

            List<DictionaryEntry> listaTipoSolicitacao = new List<DictionaryEntry>();
            listaTipoSolicitacao.Add(new DictionaryEntry() { Key = (short)EBoletoSolicitacaoTipo.Alterar, Value = EBoletoSolicitacaoTipoDescricao.EBoletoSolicitacaoTipoDesc(EBoletoSolicitacaoTipo.Alterar) });
            listaTipoSolicitacao.Add(new DictionaryEntry() { Key = (short)EBoletoSolicitacaoTipo.Avulso, Value = EBoletoSolicitacaoTipoDescricao.EBoletoSolicitacaoTipoDesc(EBoletoSolicitacaoTipo.Avulso) });
            listaTipoSolicitacao.Add(new DictionaryEntry() { Key = (short)EBoletoSolicitacaoTipo.Novo, Value = EBoletoSolicitacaoTipoDescricao.EBoletoSolicitacaoTipoDesc(EBoletoSolicitacaoTipo.Novo) });
            model.ListaTipoSolicitacao = listaTipoSolicitacao;

            List<DictionaryEntry> listaStatusBoleto = new List<DictionaryEntry>();
            listaStatusBoleto.Add(new DictionaryEntry() { Key = (short)EBoletoStatusType.Aprovado, Value = EBoletoStatusTypeDescricao.EBoletoStatusTypeDescricaoDesc(EBoletoStatusType.Aprovado) });
            listaStatusBoleto.Add(new DictionaryEntry() { Key = (short)EBoletoStatusType.Reprovado, Value = EBoletoStatusTypeDescricao.EBoletoStatusTypeDescricaoDesc(EBoletoStatusType.Reprovado) });
            model.ListaStatusBoleto = listaStatusBoleto;


            List<DictionaryEntry> listTipoPeriodo = new List<DictionaryEntry>();
            listTipoPeriodo.Add(new DictionaryEntry() { Key = (short)ERelatorioBoletoDetalhadoPeriodoTipo.Solicitacao, Value = ERelatorioBoletoDetalhadoPeriodoTipoDescricao.ERelatorioBoletoDetalhadoPeriodoTipoDesc(ERelatorioBoletoDetalhadoPeriodoTipo.Solicitacao) });
            listTipoPeriodo.Add(new DictionaryEntry() { Key = (short)ERelatorioBoletoDetalhadoPeriodoTipo.Status, Value = ERelatorioBoletoDetalhadoPeriodoTipoDescricao.ERelatorioBoletoDetalhadoPeriodoTipoDesc(ERelatorioBoletoDetalhadoPeriodoTipo.Status) });
            listTipoPeriodo.Add(new DictionaryEntry() { Key = (short)ERelatorioBoletoDetalhadoPeriodoTipo.Vencimento, Value = ERelatorioBoletoDetalhadoPeriodoTipoDescricao.ERelatorioBoletoDetalhadoPeriodoTipoDesc(ERelatorioBoletoDetalhadoPeriodoTipo.Vencimento) });
            model.ListaTipoPeriodo = listTipoPeriodo;

            return View(model);
        }

        [HttpPost]
        public ActionResult GestaoBoleto(RelatorioPesquisaViewModel filtro)
        {
            WebRotinas.CookieObjetoGravar(_filtroAtendimentoAgendadosSessionId, filtro);
            bool admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;
            var cdReginal = WebRotinas.ObterUsuarioLogado().CodRegional;

            ViewBag.ListaUsuarioAtendimento = _dbUsuarioRegional.FindAll(x => x.CodRegional == (int)cdReginal || admin).Select(x => x.Usuario).ToList();

            List<DictionaryEntry> listaTipoSolicitacao = new List<DictionaryEntry>();
            listaTipoSolicitacao.Add(new DictionaryEntry() { Key = (short)EBoletoSolicitacaoTipo.Alterar, Value = EBoletoSolicitacaoTipoDescricao.EBoletoSolicitacaoTipoDesc(EBoletoSolicitacaoTipo.Alterar) });
            listaTipoSolicitacao.Add(new DictionaryEntry() { Key = (short)EBoletoSolicitacaoTipo.Avulso, Value = EBoletoSolicitacaoTipoDescricao.EBoletoSolicitacaoTipoDesc(EBoletoSolicitacaoTipo.Avulso) });
            listaTipoSolicitacao.Add(new DictionaryEntry() { Key = (short)EBoletoSolicitacaoTipo.Novo, Value = EBoletoSolicitacaoTipoDescricao.EBoletoSolicitacaoTipoDesc(EBoletoSolicitacaoTipo.Novo) });
            filtro.ListaTipoSolicitacao = listaTipoSolicitacao;

            List<DictionaryEntry> listaStatusBoleto = new List<DictionaryEntry>();
            listaStatusBoleto.Add(new DictionaryEntry() { Key = (short)EBoletoStatusType.Aprovado, Value = EBoletoStatusTypeDescricao.EBoletoStatusTypeDescricaoDesc(EBoletoStatusType.Aprovado) });
            listaStatusBoleto.Add(new DictionaryEntry() { Key = (short)EBoletoStatusType.Reprovado, Value = EBoletoStatusTypeDescricao.EBoletoStatusTypeDescricaoDesc(EBoletoStatusType.Reprovado) });
            filtro.ListaStatusBoleto = listaStatusBoleto;


            List<DictionaryEntry> listTipoPeriodo = new List<DictionaryEntry>();
            listTipoPeriodo.Add(new DictionaryEntry() { Key = (short)ERelatorioBoletoDetalhadoPeriodoTipo.Solicitacao, Value = ERelatorioBoletoDetalhadoPeriodoTipoDescricao.ERelatorioBoletoDetalhadoPeriodoTipoDesc(ERelatorioBoletoDetalhadoPeriodoTipo.Solicitacao) });
            listTipoPeriodo.Add(new DictionaryEntry() { Key = (short)ERelatorioBoletoDetalhadoPeriodoTipo.Status, Value = ERelatorioBoletoDetalhadoPeriodoTipoDescricao.ERelatorioBoletoDetalhadoPeriodoTipoDesc(ERelatorioBoletoDetalhadoPeriodoTipo.Status) });
            listTipoPeriodo.Add(new DictionaryEntry() { Key = (short)ERelatorioBoletoDetalhadoPeriodoTipo.Vencimento, Value = ERelatorioBoletoDetalhadoPeriodoTipoDescricao.ERelatorioBoletoDetalhadoPeriodoTipoDesc(ERelatorioBoletoDetalhadoPeriodoTipo.Vencimento) });
            filtro.ListaTipoPeriodo = listTipoPeriodo;

            //ViewBag.ListaUsuarioAtendimento.Insert(0, new Usuario() { Cod = 0, Nome = "--" });
            return View(filtro);

        }
        [HttpGet]
        public ActionResult GestaoCarta()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioPesquisaViewModel model = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroAtendimentoAgendadosSessionId);
            if (model == null)
                model = new RelatorioPesquisaViewModel();
            var usuario = WebRotinas.ObterUsuarioLogado();
            bool admin = usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin;
            List<int> codUsuarios = _dbUsuarioRegional.FindAll(x => x.CodUsuario == (int)usuario.Cod).Select(x => x.CodRegional).ToList();

            ViewBag.ListaAtendentes = new SelectList(UtilsRotinas.GetSelectListItemAtendenteAdmFilial("", null));
            ViewBag.ListaRegionais = new SelectList(UtilsRotinas.GetSelectListItemRegional("", null));

            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);
            model.ReportCod = (short)Enumeradores.TipoRelatorio.GestaoCarta;


            model.DataPeriodoDe = DateTime.Now.AddDays(-7).Date;
            model.DataPeriodoAte = DateTime.Now.Date ;

            return View(model);
        }

        [HttpPost]
        public ActionResult GestaoCarta(RelatorioPesquisaViewModel filtro)
        {
            WebRotinas.CookieObjetoGravar(_filtroAtendimentoAgendadosSessionId, filtro);
            bool admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;
            var cdReginal = WebRotinas.ObterUsuarioLogado().CodRegional;

            ViewBag.ListaUsuarioAtendimento = _dbUsuarioRegional.FindAll(x => x.CodRegional == (int)cdReginal || admin).Select(x => x.Usuario).ToList();
            ViewBag.ListaRegionais = new SelectList(UtilsRotinas.GetSelectListItemRegional("", null));

     

            return View(filtro);

        }
        [HttpGet]
        public ActionResult GestaoFilial()
        {
            WebRotinas.CookieRemover(_filtroSessionId);
            RelatorioPesquisaViewModel model = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroAtendimentoAgendadosSessionId);
            if (model == null)
                model = new RelatorioPesquisaViewModel();
            var usuario = WebRotinas.ObterUsuarioLogado();
            bool admin = usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin;
            List<int> codUsuarios = _dbUsuarioRegional.FindAll(x => x.CodUsuario == (int)usuario.Cod).Select(x => x.CodRegional).ToList();

            ViewBag.ListaAtendentes = new SelectList(UtilsRotinas.GetSelectListItemAtendenteAdmFilial("", null));
            ViewBag.ListaRegionais = new SelectList(UtilsRotinas.GetSelectListItemRegional("", null));

            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);
            model.ReportCod = (short)Enumeradores.TipoRelatorio.GestaoFilial;

            model.DataPeriodoDe = DateTime.Now.AddDays(-7).Date;
            model.DataPeriodoAte = DateTime.Now.Date;

            return View(model);
        }

        [HttpPost]
        public ActionResult GestaoFilial(RelatorioPesquisaViewModel filtro)
        {
            WebRotinas.CookieObjetoGravar(_filtroAtendimentoAgendadosSessionId, filtro);
            bool admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;
            var cdReginal = WebRotinas.ObterUsuarioLogado().CodRegional;

            ViewBag.ListaUsuarioAtendimento = _dbUsuarioRegional.FindAll(x => x.CodRegional == (int)cdReginal || admin).Select(x => x.Usuario).ToList();
            //ViewBag.ListaUsuarioAtendimento.Insert(0, new Usuario() { Cod = 0, Nome = "--" });
            return View(filtro);

        }

        private string verificarStatus(DateTime? dtVencimento, DateTime? dtFechamento, decimal? vrAberto)
        {
            var resultado = String.Empty;
            if (dtVencimento > DateTime.Now)
            {
                resultado = EnumeradoresDescricao.NegociacaoTipoParcela((short)Enumeradores.NegociacaoTipoParcela.aVencer);
            }
            if (dtVencimento < DateTime.Now)
            {
                resultado = EnumeradoresDescricao.NegociacaoTipoParcela((short)Enumeradores.NegociacaoTipoParcela.Atrasada);
            }
            if (dtFechamento.HasValue && vrAberto == 0)
            {
                resultado = EnumeradoresDescricao.NegociacaoTipoParcela((short)Enumeradores.NegociacaoTipoParcela.Paga);
            }
            return resultado;

        }
        private IQueryable<RelatorioAtendimentoDados> filtrarAgendamentos(IQueryable<RelatorioAtendimentoDados> query_dados, RelatorioPesquisaViewModel filtro)
        {
            if (!string.IsNullOrEmpty(filtro.Contrato))
            {
                filtro.Contrato = DbRotinas.Criptografar(filtro.Contrato);
                query_dados = query_dados.Where(x => x.NumeroContrato.Contains(filtro.Contrato));
            }
            if (filtro.DataEmissao.HasValue)
            {
                query_dados = query_dados.Where(x => x.DataEmissaoFatura == filtro.DataEmissao.Value.Date);
            }
            if (!string.IsNullOrEmpty(filtro.CNPJ))
            {
                filtro.CNPJ = DbRotinas.Criptografar(DbRotinas.RemoverTracosPontos(filtro.CNPJ));
                query_dados = query_dados.Where(x => x.CNPJ.Contains(filtro.CNPJ));
            }
            if (!string.IsNullOrEmpty(filtro.CodArea))
            {
                filtro.CodArea = DbRotinas.Criptografar(filtro.CodArea);
                query_dados = query_dados.Where(x => x.RotaArea.Contains(filtro.CodArea));
            }

            if (filtro.DataAgendamentoDe.HasValue)
                query_dados = query_dados.Where(x => x.DataAgenda >= filtro.DataAgendamentoDe);

            if (filtro.DataAgendamentoAte.HasValue)
                query_dados = query_dados.Where(x => x.DataAgenda <= filtro.DataAgendamentoAte);

            if (filtro.DataVencimento.HasValue)
                query_dados = query_dados.Where(x => x.DataVencimento >= filtro.DataVencimento);

            if (!string.IsNullOrEmpty(filtro.NumeroDocumento))
            {
                //filtro.NumeroDocumento = DbRotinas.Criptografar(filtro.NumeroDocumento);
                query_dados = query_dados.Where(x => x.NumeroDocumento.Contains(filtro.NumeroDocumento));
            }

            if (!string.IsNullOrEmpty(filtro.NumeroFatura))
            {
                //filtro.NumeroFatura = DbRotinas.Criptografar(filtro.NumeroFatura);
                query_dados = query_dados.Where(x => x.NumeroFatura.Contains(filtro.NumeroFatura));
            }
            if (filtro.ValorAbertoDe.HasValue)
                query_dados = query_dados.Where(x => x.ValorAberto >= filtro.ValorAbertoDe);

            if (filtro.ValorAbertoAte.HasValue)
                query_dados = query_dados.Where(x => x.VarlorParcela <= filtro.ValorAbertoAte);

            if (filtro.ValorAbertoAte.HasValue)
                query_dados = query_dados.Where(x => x.VarlorParcela <= filtro.ValorAbertoAte);

            if (!string.IsNullOrEmpty(filtro.NomeFilial))
            {
                filtro.NomeFilial = DbRotinas.Criptografar(filtro.NomeFilial);
                query_dados = query_dados.Where(x => x.Filial.Contains(filtro.NomeFilial));
            }

            if (!string.IsNullOrEmpty(filtro.Status))
            {
                if (filtro.Status == "vencida")
                {
                    query_dados = query_dados.Where(x => x.DataVencimento < DateTime.Now.Date && !x.DataFechamento.HasValue);
                }
                if (filtro.Status.ToLower() == "pago")
                {
                    query_dados = query_dados.Where(x => x.DataFechamento.HasValue);
                }
                if (filtro.Status.ToLower().Contains("atras"))
                {
                    query_dados = query_dados.Where(x => x.DataVencimento >= DateTime.Now.Date && !x.DataFechamento.HasValue);
                }
            }
            if (!string.IsNullOrEmpty(filtro.Fase))
            {
                filtro.Fase = DbRotinas.Criptografar(filtro.Fase);
                query_dados = query_dados.Where(x => x.Fase.Contains(filtro.Fase));
            }
            if (!string.IsNullOrEmpty(filtro.Sintese))
            {
                filtro.Sintese = DbRotinas.Criptografar(filtro.Sintese);
                query_dados = query_dados.Where(x => x.Sintese.Contains(filtro.Sintese));
            }
            return query_dados;
        }

        #region DevExpress DocumentViewer (ReportCallbackRoutValues | ExportDocumentViewer)
        public ActionResult ReportCallbackRouteValues(string tipoReport = null)
        {
            RelatorioPesquisaViewModel filtro = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroSessionId);
            XtraReport report = GetReport(filtro);
            return PartialView("ReportViewerPartial", report);
        }
        public ActionResult ExportDocumentViewer()
        {
            RelatorioPesquisaViewModel filtro = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroSessionId);
            XtraReport report = GetReport(filtro);
            return DevExpress.Web.Mvc.DocumentViewerExtension.ExportTo(report);
        }

        #endregion

        XtraReport GetReport(RelatorioPesquisaViewModel filtro)
        {
            switch ((Enumeradores.TipoRelatorio)filtro.TipoReport)
            {
                case Enumeradores.TipoRelatorio.ReguaParcelaSemAcoes:
                    {
                        return ReportParcelaReguaSemAcoes(filtro);
                    }
                case Enumeradores.TipoRelatorio.Teste:
                    {
                        return CreateProductsReport();
                    }
                default:
                    {
                        return new XtraReport();
                    }
            }
        }
        #region Relatorio Parcelas Sem Acao Cobranca
        public ActionResult ReportParcelaSemAcao()
        {
            RelatorioPesquisaViewModel filtro = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroSessionId);
            if (filtro.TipoReport == null)
            {
                filtro.TipoReport = (short)Enumeradores.TipoRelatorio.ReguaParcelaSemAcoes;
            }
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            XtraReport report = GetReport(filtro);
            return View("ReportViewerPartial", report);
        }

        static XtraReport CreateProductsReport()
        {
            Repository<TabelaProduto> _dbProduto = new Repository<TabelaProduto>();
            XtraReport report = new rpt_partial_teste();
            ReportHeaderBand headerBand = new ReportHeaderBand()
            {
                HeightF = 80
            };
            report.Bands.Add(headerBand);

            headerBand.Controls.Add(new XRLabel()
            {
                Text = "Product Report"
            });
            XRTable table = new XRTable();
            table.Borders = DevExpress.XtraPrinting.BorderSide.All;
            table.BeginInit();

            var produtos = _dbProduto.All().ToList();

            int rowsCount = 3;
            float rowHeight = 25f;
            for (int i = 0; i < rowsCount; i++)
            {
                XRTableRow row = new XRTableRow();
                row.HeightF = rowHeight;
                foreach (var collmns in produtos)
                {
                    XRTableCell cell = new XRTableCell();
                    cell.Text = DbRotinas.Descriptografar(collmns.Descricao);
                    row.Cells.Add(cell);
                }
                table.Rows.Add(row);
            }
            table.AdjustSize();
            table.EndInit();

            DetailBand detailBand = new DetailBand();
            report.Bands.Add(detailBand);
            detailBand.Controls.Add(table);

            //report.DataSource = GetCategories();

            return report;
        }

        public static List<TabelaProduto> GetCategories()
        {
            Repository<TabelaProduto> _prod = new Repository<TabelaProduto>();
            List<TabelaProduto> catData = _prod.All().ToList();

            return catData;
        }

        XtraReport ReportParcelaReguaSemAcoes(RelatorioPesquisaViewModel filtro)
        {
            //rpt_cobranca_regua_sem_acoes report = new rpt_cobranca_regua_sem_acoes();
            rpt_partial_teste report = new rpt_partial_teste();
            //DataSet_Cobranca ds = new DataSet_Cobranca();
            //SP_RELATORIO_REGUA_PARCELAS_SEM_ACOESTableAdapter tableAdapter = new SP_RELATORIO_REGUA_PARCELAS_SEM_ACOESTableAdapter();
            //
            //tableAdapter.Fill(ds.SP_RELATORIO_REGUA_PARCELAS_SEM_ACOES
            //     , 0
            //    , filtro.CodContrutoras
            //    , filtro.CodRegionais
            //    , filtro.CodProdutos
            //    , filtro.CodRotas
            //    , filtro.CodAgins
            //    , filtro.DataPeriodoDe
            //    , filtro.DataPeriodoAte
            //    , filtro.CodUsuario
            //    , filtro.PerfilTipo
            //    );
            //
            //foreach (DataRow r in ds.Tables[0].Rows)
            //{
            //    r["Filial"] = DbRotinas.Descriptografar(r["Filial"].ToString());
            //    r["NumeroContrato"] = DbRotinas.Descriptografar(r["NumeroContrato"].ToString());
            //    r["RotaArea"] = DbRotinas.Descriptografar(r["RotaArea"].ToString());
            //    r["Fase"] = DbRotinas.Descriptografar(r["Fase"].ToString());
            //    r["Sintese"] = DbRotinas.Descriptografar(r["Sintese"].ToString());
            //    r["Observacao"] = DbRotinas.Descriptografar(r["Observacao"].ToString());
            //}
            //report.DataSource = ds;
            report.CreateDocument();
            return report;
        }
        #endregion

        #region Relatorio Acoes Agendadas
        public ActionResult ReportAntendimentoAgendado()
        {
            RelatorioPesquisaViewModel filtro = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroSessionId);
            if (filtro.TipoReport == null)
            {
                filtro.TipoReport = (short)Enumeradores.TipoRelatorio.AtendimentoAgendados;
            }
            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            XtraReport report = GetReport(filtro);
            return View("ReportViewerPartial", report);
        }
        #endregion


        [HttpGet, ValidateInput(false)]
        public JsonResult ObterFiltrosQueryBuilder()
        {
            OperacaoRetorno op = new OperacaoRetorno();


            try
            {
                var all = _dbTabelaGrupo.All();


                FaseSinteseDb _objFaseDb = new FaseSinteseDb();
                var classificacoes = _objFaseDb.ListarHistorico(Enumeradores.TipoHistorico.Parcela).OrderBy(x => x.CodigoSinteseCliente);

                var itensClassificacao = new List<string>();

                foreach (var p in classificacoes)
                {
                    itensClassificacao.Add(string.Format(@"{{""{0}"":""{3} - {2} - {1}""}}", p.Cod, p.Sintese.ToUpper(), p.TabelaFase.Fase.ToUpper(), p.CodigoSinteseCliente != null ? p.CodigoSinteseCliente.ToUpper() : ""));
                }

                var itens = new List<string>();

                foreach (var p in all.ToList())
                {
                    itens.Add(string.Format(@"{{""{0}"":""{1}""}}", p.Cod, p.Nome));
                }


                var corporativoAll = _dbCliente.All().Where(x => !String.IsNullOrEmpty(x.Corporativo)).Select(a => a.Corporativo).Distinct().ToList();
                var itensCorporativo = new List<string>();
                foreach (var p in corporativoAll.ToList())
                {
                    itensCorporativo.Add(string.Format(@"{{""{0}"":""{1}""}}", p, p));
                }


                op.Dados = new
                {
                    listaClassificacaoParcela = itensClassificacao,
                    listaGruposEmpresa = itens,
                    listaCorporativo = itensCorporativo
                };

                op.OK = true;

            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }

            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ReportMvc()
        {
            return View();
        }


        public ActionResult DocumentViewerPartial()
        {
            RelatorioPesquisaViewModel filtro = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroSessionId);
            filtro.TipoReport = (short)Enumeradores.TipoRelatorio.ReguaParcelaSemAcoes;
            XtraReport report = GetReport(filtro);
            report = CreateProductsReport();
            return PartialView("_DocumentViewerPartial", report);
        }

        public ActionResult DocumentViewerExport()
        {
            //XtraReport report = CreateProductsReport();
            RelatorioPesquisaViewModel filtro = WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroSessionId);
            filtro.TipoReport = (short)Enumeradores.TipoRelatorio.ReguaParcelaSemAcoes;
            XtraReport report = GetReport(filtro);
            report = CreateProductsReport();
            return ReportViewerExtension.ExportTo(report);
        }
    }


}