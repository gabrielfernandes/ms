﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using Cobranca.Web.Rotinas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cobranca.Web.Controllers
{
    public class ChatClienteController : Controller
    {
        private Repository<Cliente>
        _repoCliente = new Repository<Cliente>();

        private Repository<Usuario>
        _repoUsuario = new Repository<Usuario>();

        private Repository<ChatAtendimentoTipo>
        _repoAtendimentoTipo = new Repository<ChatAtendimentoTipo>();

        private Repository<ChatAtendimento>
        _repoAtendimento = new Repository<ChatAtendimento>();

        private Repository<ChatAtendimentoMensagem>
        _repoAtendimentoMensagem = new Repository<ChatAtendimentoMensagem>();

        private Repository<ChatAtendimentoResultado>
        _repoResultados = new Repository<ChatAtendimentoResultado>();

        private const string _filtroAtendimentoKeyId = "atendimentoKeyId";
        [HttpGet]
        public ActionResult index()
        {
            string chave = Request.QueryString["crypt"];
            long? clienteId = obterParametro(chave);

            long? EmpresaId = 0;

            //if (clienteId.HasValue)
            //{
            //    EmpresaId = _repoCliente.FindById(clienteId.Value).EmpresaId;
            //}

            bool online = false;
            online = _repoUsuario.FindAll(
                    x => !x.DataExcluido.HasValue
                        && x.PerfilTipo == (short)Enumeradores.PerfilTipo.Atendimento
                        //&& x.ChatStatus == (short)Enumeradores.ChatStatus.Online
                        //&& x.ChatStatusData.HasValue
                        //&& x.EmpresaId == EmpresaId
                        ).Any();


            ViewBag.OperadoresOnline = online;


            if (EmpresaId.HasValue)
            {
                string _pathChatOff = System.IO.Path.Combine(string.Format("~/Documentos/Empresas/chatOff/{0}/", EmpresaId.Value));
                //ViewBag.ImgOff = System.IO.Path.Combine(_pathChatOff, System.IO.Path.GetFileName(System.IO.Directory.GetFiles(Server.MapPath(_pathChatOff)).FirstOrDefault()));
                ViewBag.ImgOff = null;
                string _pathChatOn = System.IO.Path.Combine(string.Format("~/Documentos/Empresas/chatOn/{0}/", EmpresaId.Value));
                //ViewBag.ImgOnline = System.IO.Path.Combine(_pathChatOn, System.IO.Path.GetFileName(System.IO.Directory.GetFiles(Server.MapPath(_pathChatOn)).FirstOrDefault()));
                ViewBag.ImgOnline = null;
            }


            ViewBag.Codigo = chave;



            return View();
        }

        private int? obterParametro(string chave)
        {
            return 0;
            if (chave == string.Empty)
                return null;

            string url = HttpUtility.UrlDecode(chave);

            string decrip = WebRotinas.DescriptografarString(url);

            //return DbRotinas.ConverterParaLong(decrip);



        }
        [HttpGet]
        public ActionResult NovoAtendimento(string key)
        {
            ChatAtendimento atendimento = new ChatAtendimento();
            //atendimento.c = key;
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            foreach (var item in _repoAtendimentoTipo.All().ToList())
            {
                lista.Add(new DictionaryEntry()
                {
                    Key = item.Nome,
                    Value = item.Cod
                });

            }

            ViewBag.ListaTiposSuporte = new SelectList(lista, "value", "key", 0);
            return View(atendimento);
        }
        [HttpPost]
        public ActionResult NovoAtendimento(ChatAtendimento dados)
        {
            if (ModelState.IsValid)
            {

                dados.IP = Request.ServerVariables["REMOTE_ADDR"];
                //dados.ClienteId = obterParametro(dados.CodigoCliente);
                dados = _repoAtendimento.CreateOrUpdate(dados);

            }
            ViewBag.ListaTiposSuporte = new SelectList(_repoAtendimentoTipo.SelectList(x => !x.DataExcluido.HasValue), "value", "key", 0);
            return View(dados);
        }
        public ActionResult Atendimento(int id = 0)
        {

            var dados = _repoAtendimento.FindById(id);

            return View(dados);
        }
        public ActionResult Chat(int id = 0)
        {

            var dados = _repoAtendimento.FindById(id);

            return View(dados);
        }
        [HttpPost]
        public JsonResult PosicaoFilaEsperaAtendimento(int id = 0)
        {

            var atendimento = _repoAtendimento.FindById(id);
            int posicao = _repoAtendimento.All().Count(x => x.Cod < atendimento.Cod && !x.DataAtendimento.HasValue);

            return Json(new { posicao = posicao, atendimento = atendimento.DataAtendimento.HasValue });
        }
        [HttpPost]
        public JsonResult EnivarMensagem(int id, string texto, short tp = 0)
        {
            ChatAtendimentoMensagem msg = new ChatAtendimentoMensagem();
            int? usuarioId = null;

            msg.CodigoAtentimento = id;
            msg.Descricao = texto;
            msg.Nome = texto;
            msg.CodUsuario = usuarioId;
            msg.Tipo = (short)Enumeradores.ChatMensagemTipo.Cliente;
            msg.DataCadastro = DateTime.Now;
            if (tp == 2)
            {
                msg.Arquivo = texto;
                msg.ArquivoNome = Path.GetFileName(texto);
            }


            msg = _repoAtendimentoMensagem.CreateOrUpdate(msg);



            if (tp == 2)
            {
                vincularDocumentoMensagem(msg);
            }
            return Json(new
            {
                Mensagem = msg.Descricao,
                Id = msg.Cod,
                Hora = string.Format("{0:HH:mm}h", msg.DataCadastro)
            });
        }
        private void vincularDocumentoMensagem(ChatAtendimentoMensagem msg)
        {
            string pathUpload = Server.MapPath("~/Content/files");

            if (!Directory.Exists(pathUpload))
            {
                Directory.CreateDirectory(pathUpload);
            }

            string pathConteudoMsg = Path.Combine("~/Documentos/chats", msg.CodigoAtentimento.ToString(), msg.Cod.ToString());

            string serverPath = Server.MapPath(pathConteudoMsg);

            if (!Directory.Exists(serverPath))
            {
                Directory.CreateDirectory(serverPath);
            }
            try
            {
                FileInfo f = new FileInfo(Path.Combine(pathUpload, msg.ArquivoNome));
                if (f.Exists)
                {
                    System.IO.File.Move(f.FullName, Path.Combine(serverPath, msg.ArquivoNome));
                }

            }
            catch (Exception ex)
            {

            }

        }
        [HttpPost]
        public JsonResult CarregarMensagem(int id = 0, int ultimaMsgId = 0)
        {
            var lista = _repoAtendimentoMensagem.FindAll(x => x.CodigoAtentimento == id && x.Cod > ultimaMsgId).OrderBy(x => x.DataCadastro).ToList();

            var l = from p in lista
                    select new
                    {
                        Mensagem = p.Descricao,
                        Data = string.Format("{0:dd/MM/yyyy} as {0:hh:mm}", p.DataCadastro),
                        Usuario = p.CodUsuario > 0 ? p.Usuario.Nome : p.ChatAtendimento.Nome,
                        Id = p.Cod,
                        AtendimentoId = p.CodigoAtentimento,
                        Tipo = p.Tipo,
                        Hora = string.Format("{0:HH:mm}h", p.DataCadastro),
                        ArquivoNome = p.ArquivoNome,
                        pathArquivo = string.IsNullOrEmpty(p.ArquivoNome) ? null : Path.Combine("~/documentos/chats/", p.CodigoAtentimento.ToString(), p.Cod.ToString(), p.Arquivo)
                    };

            return Json(l);
        }
        public JsonResult FinalizarAtendimento(int id = 0)
        {
            var dados = _repoAtendimento.FindById(id);
            dados.DataEncerrado = DateTime.Now;

            ChatAtendimentoMensagem msg = new ChatAtendimentoMensagem();
            int? usuarioId = null;

            msg.CodigoAtentimento = id;
            msg.Descricao = string.Format("Atendimento encerrado pelo cliente {0} as {1}.", dados.Nome, DateTime.Now);
            msg.Nome = msg.Descricao;
            msg.CodUsuario = usuarioId;
            msg.Tipo = (short)Enumeradores.ChatMensagemTipo.Automatica;
            msg.DataCadastro = DateTime.Now;
            _repoAtendimentoMensagem.CreateOrUpdate(msg);

            _repoAtendimento.CreateOrUpdate(dados);
            return Json(new { OK = true });
        }
        [HttpGet]
        public ActionResult AvaliacaoAtendimento(int id = 0)
        {
            var dados = _repoAtendimento.FindById(id);

            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            foreach (var item in _repoResultados.All())
            {
                lista.Add(new DictionaryEntry() {
                    Key = item.Nome,
                    Value = item.Cod
                });
            }

            ViewBag.ListaResultados = new SelectList(lista, "value", "key", 0);
            return View(dados);
        }
        [HttpGet]
        public ActionResult AtendimentoEncerrado(int id = 0)
        {
            var dados = _repoAtendimento.FindById(id);
            return View(dados);
        }
        [HttpPost]
        public JsonResult RegistrarOpniao(int id = 0, int RespostaId = 0, string Descricao = "")
        {
            var dados = _repoAtendimento.FindById(id);
            dados.CodResultado = RespostaId;
            dados.Descricao = Descricao;
            _repoAtendimento.CreateOrUpdate(dados);

            return Json(new { OK = true });
        }
        //
    }
}