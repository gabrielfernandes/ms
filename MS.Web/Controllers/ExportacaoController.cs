﻿using BoletoNet;
using Cobranca.Db.Models;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs;
using Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Outputs;
using Cobranca.Domain.BoletoContext.Handlers;
using Cobranca.Notificacao.Email;
using Cobranca.Notificacao.EMAIL;
using Cobranca.Rotinas;
using Cobranca.Rotinas.BoletoGerar;
using Cobranca.Shared.Commands;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Recebiveis.Web.Models;
using Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Cobranca.Web.Controllers
{
    public class ExportacaoController : Controller
    {
        public const string _filtroSessionId = "FiltroPesquisaId";
        public const string _filtroCodContrato = "FiltroCodContrato";
        private ParametroPlataforma _objParametro;
        private EmailRotinas _objEmailRotinas;
        public Repository<TabelaTipoCobrancaAcao> _dbTipoCobrancaAcao = new Repository<TabelaTipoCobrancaAcao>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<ContratoCliente> _dbContratoCliente = new Repository<ContratoCliente>();
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<ContratoParcela> _dbParcela = new Repository<ContratoParcela>();
        public Repository<ContratoAcaoCobrancaHistorico> _dbContratoAcaoCobrancaHistorico = new Repository<ContratoAcaoCobrancaHistorico>();
        public Repository<ContratoHistorico> _dbContratoHistorico = new Repository<ContratoHistorico>();
        public Repository<ContratoDocumento> _dbContratoDocumento = new Repository<ContratoDocumento>();
        public Repository<ContratoDespesa> _dbContratoDespesa = new Repository<ContratoDespesa>();
        public Repository<TabelaAging> _dbAging = new Repository<TabelaAging>();
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();
        public Repository<TabelaBloco> _dbBloco = new Repository<TabelaBloco>();
        public Repository<TabelaUnidade> _dbUnidade = new Repository<TabelaUnidade>();
        public Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        public Repository<TabelaEscritorio> _dbEscritorio = new Repository<TabelaEscritorio>();
        public Repository<TabelaFase> _dbFase = new Repository<TabelaFase>();
        public Repository<TabelaTipoCobranca> _dbTipoCobranca = new Repository<TabelaTipoCobranca>();
        public Repository<Usuario> _dbUsuario = new Repository<Usuario>();
        public Repository<TabelaGestao> _dbGestao = new Repository<TabelaGestao>();
        public Repository<TabelaEmpresa> _dbEmpresa = new Repository<TabelaEmpresa>();
        public Repository<ContratoParcelaBoleto> _dbBoleto = new Repository<ContratoParcelaBoleto>();
        public Repository<ContratoTipoCobrancaHistorico> _dbContratoTipoCobrancaHistorico = new Repository<ContratoTipoCobrancaHistorico>();
        private Repository<TabelaBancoPortador> _dbParametroBoleto = new Repository<TabelaBancoPortador>();
        Repository<TabelaModeloAcao> _dbTabelaModeloAcao = new Repository<TabelaModeloAcao>();
        public Repository<TabelaRegional> _dbFilial = new Repository<TabelaRegional>();
        public Repository<ParametroPlataforma> _dbParametro = new Repository<ParametroPlataforma>();
        public Repository<LogEmail> _dbLogEmail = new Repository<LogEmail>();
        public Repository<Historico> _dbHistorico = new Repository<Historico>();

        private Repository<ContratoParcelaBoletoLote> _dbBoletoLote = new Repository<ContratoParcelaBoletoLote>();
        public Dados _qry = new Dados();

        ListasRepositorio _dbListas = new ListasRepositorio();


        private readonly BoletoHandler _boletoHandler;

        public ExportacaoController(BoletoHandler boletoHandler)
        {
            _boletoHandler = boletoHandler;
        }

        private void PreencherViewBags(PesquisaViewModel model)
        {


            List<Usuario> listaUsuario = new List<Usuario>();
            listaUsuario = _dbUsuario.All().OrderBy(x => x.Nome).ToList();
            listaUsuario.Insert(0, new Usuario());

            List<TabelaRegional> listaRegional = new List<TabelaRegional>();
            listaRegional = _dbRegional.All().OrderBy(x => x.Regional).ToList();
            listaRegional.Insert(0, new TabelaRegional());

            List<TabelaTipoCobranca> listaTipoCobranca = new List<TabelaTipoCobranca>();
            listaTipoCobranca = _dbTipoCobranca.All().OrderBy(x => x.TipoCobranca).ToList();
            listaTipoCobranca.Insert(0, new TabelaTipoCobranca());

            List<TabelaAging> listaAging = new List<TabelaAging>();
            listaAging = _dbAging.All().OrderBy(x => x.Cod).ToList();
            listaAging.Insert(0, new TabelaAging());

            ViewBag.ListaConstrutora = _dbListas.Construtoras();
            //ViewBag.ListaEmpreendimento = _dbListas.Empreendimentos().Take(20);
            // ViewBag.ListaBloco = _dbListas.Bloco();
            ViewBag.ListaAging = from p in listaAging select new DictionaryEntry(p.Aging, p.Cod);
            // ViewBag.ListaUnidade = from p in listaUnidade select new DictionaryEntry(p.NumeroUnidade, p.Cod);
            ViewBag.ListaUsuario = from p in listaUsuario select new DictionaryEntry(p.Nome, p.Cod);
            ViewBag.ListaRegional = from p in listaRegional select new DictionaryEntry(p.Regional, p.Cod);
            ViewBag.ListaTipoCobranca = from p in listaTipoCobranca select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);
            ViewBag.Lista = new List<DictionaryEntry>();


            List<TabelaFase> lista = new List<TabelaFase>();
            lista = _dbFase.All().OrderBy(x => x.Ordem).Where(x => x.TipoHistorico == (short)Enumeradores.TipoHistorico.Parcela).ToList();
            lista.Insert(0, new TabelaFase());
            var listFase = from p in lista select new DictionaryEntry(DbRotinas.Descriptografar(p.Fase), p.Cod);
            ViewBag.ListaFases = listFase.ToList();
            ViewBag.ListaSintese = new List<DictionaryEntry>();
            ViewBag.ListaMotivoAtraso = _dbListas.MotivoAtraso();
            ViewBag.ListaResolucao = _dbListas.Resulucao();

        }

        [HttpGet]
        public ActionResult Sms()
        {
            var listaCobranca = _dbTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            var selectList = from p in listaCobranca select new { Cod = p.Cod, Text = DbRotinas.Descriptografar(p.TipoCobranca) };
            ViewBag.ListaTipoCobranca = selectList;
            ViewBag.ListaTipoCobrancaAcao = from p in new List<TabelaTipoCobrancaAcao>() select new { Cod = p.Cod, Text = p.TabelaAcaoCobranca.AcaoCobranca };

            WebRotinas.CookieRemover(_filtroSessionId);
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var resultado = new List<Db.Models.Pesquisa.ContratoDados>();

            return View(resultado);


        }


        [Authorize]
        [HttpGet]
        public ActionResult ClassificacaoParcela()
        {
            List<TabelaFase> lista = new List<TabelaFase>();
            lista = _dbFase.All().OrderBy(x => x.Ordem).Where(x => x.TipoHistorico == (short)Enumeradores.TipoHistorico.Parcela).ToList();
            lista.Insert(0, new TabelaFase());
            var listFase = from p in lista select new DictionaryEntry(DbRotinas.Descriptografar(p.Fase), p.Cod);
            ViewBag.ListaFases = listFase.ToList();
            ViewBag.ListaSintese = new List<DictionaryEntry>();
            ViewBag.ListaMotivoAtraso = _dbListas.MotivoAtraso();
            ViewBag.ListaResolucao = _dbListas.Resulucao();
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Negativacao()
        {
            List<TabelaFase> lista = new List<TabelaFase>();
            lista = _dbFase.All().OrderBy(x => x.Ordem).Where(x => x.TipoHistorico == (short)Enumeradores.TipoHistorico.Parcela).ToList();
            lista.Insert(0, new TabelaFase());
            var listFase = from p in lista select new DictionaryEntry(DbRotinas.Descriptografar(p.Fase), p.Cod);
            ViewBag.ListaFases = listFase.ToList();
            ViewBag.ListaSintese = new List<DictionaryEntry>();
            ViewBag.ListaMotivoAtraso = _dbListas.MotivoAtraso();
            ViewBag.ListaResolucao = _dbListas.Resulucao();
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Personalizada()
        {
            List<TabelaFase> lista = new List<TabelaFase>();
            lista = _dbFase.All().OrderBy(x => x.Ordem).Where(x => x.TipoHistorico == (short)Enumeradores.TipoHistorico.Parcela).ToList();
            lista.Insert(0, new TabelaFase());
            var listFase = from p in lista select new DictionaryEntry(DbRotinas.Descriptografar(p.Fase), p.Cod);
            ViewBag.ListaFases = listFase.ToList();
            ViewBag.ListaSintese = new List<DictionaryEntry>();
            ViewBag.ListaMotivoAtraso = _dbListas.MotivoAtraso();
            ViewBag.ListaResolucao = _dbListas.Resulucao();

            return View();
        }
        [HttpGet, Authorize]
        public ActionResult Email()
        {
            var objAcao = _dbTabelaModeloAcao.FindAll(x => !x.DataExcluido.HasValue && x.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.EMAIL_BOLETO_SOLICITADO).FirstOrDefault();
            ViewBag.Mensagem = objAcao.Modelo;
            return View();
        }
        [Authorize]
        [HttpGet]
        public ActionResult Remessa()
        {
            var boletos = _dbParametroBoleto.All().ToList();
            ViewBag.ListaCompanhia = from p in boletos.OrderBy(x => x.Companhia).ToList() select new DictionaryEntry(p.Companhia, p.Companhia);

            return View();
        }
        [HttpPost]
        public ActionResult Sms(PesquisaViewModel filtro)
        {
            var listaCobranca = _dbTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            var selectList = from p in listaCobranca select new { Cod = p.Cod, Text = DbRotinas.Descriptografar(p.TipoCobranca) };
            var listaCobrancaAcao = ListarTipoCobrancaAcao(filtro.CodTipoCobranca);
            var selectListCobrancaAcao = from p in listaCobrancaAcao select new { Cod = p.Cod, Text = p.TabelaAcaoCobranca.AcaoCobranca };

            ViewBag.ListaTipoCobranca = selectList;
            ViewBag.ListaTipoCobrancaAcao = selectListCobrancaAcao;

            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            //ObterDadosSession(true);
            var resultado = new List<Db.Models.Pesquisa.ContratoDados>();
            return View(resultado);
        }

        [HttpGet]
        public ActionResult Serasa()
        {
            var listaCobranca = _dbTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            var selectList = from p in listaCobranca select new { Cod = p.Cod, Text = DbRotinas.Descriptografar(p.TipoCobranca) };
            ViewBag.ListaTipoCobranca = selectList;
            ViewBag.ListaTipoCobrancaAcao = from p in new List<TabelaTipoCobrancaAcao>() select new { Cod = p.Cod, Text = p.TabelaAcaoCobranca.AcaoCobranca };

            WebRotinas.CookieRemover(_filtroSessionId);
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var resultado = new List<Db.Models.Pesquisa.ContratoDados>();
            return View(resultado);
        }
        [HttpPost]
        public ActionResult Serasa(PesquisaViewModel filtro)
        {
            var listaCobranca = _dbTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            var selectList = from p in listaCobranca select new { Cod = p.Cod, Text = DbRotinas.Descriptografar(p.TipoCobranca) };
            var listaCobrancaAcao = ListarTipoCobrancaAcao(filtro.CodTipoCobranca);
            var selectListCobrancaAcao = from p in listaCobrancaAcao select new { Cod = p.Cod, Text = p.TabelaAcaoCobranca.AcaoCobranca };

            ViewBag.ListaTipoCobranca = selectList;
            ViewBag.ListaTipoCobrancaAcao = selectListCobrancaAcao;

            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            //ObterDadosSession(true);
            var resultado = new List<Db.Models.Pesquisa.ContratoDados>();
            return View(resultado);
        }

        public ActionResult CobrancaAcao()
        {
            var listaCobranca = _dbTipoCobranca.All().ToList();
            var selectList = from p in listaCobranca select new { Cod = p.Cod, Text = DbRotinas.Descriptografar(p.TipoCobranca) };
            ViewBag.ListaTipoCobranca = selectList;

            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var resultado = new List<Db.Models.Pesquisa.ContratoDados>();
            return View(resultado);
        }
        [HttpPost]
        public ActionResult CobrancaAcao(PesquisaViewModel filtro)
        {
            var listaCobranca = _dbTipoCobranca.All().ToList();
            var selectList = from p in listaCobranca select new { Cod = p.Cod, Text = DbRotinas.Descriptografar(p.TipoCobranca) };
            ViewBag.ListaTipoCobranca = selectList;

            WebRotinas.CookieObjetoGravar(_filtroSessionId, filtro);
            //ObterDadosSession(true);
            var resultado = new List<Db.Models.Pesquisa.ContratoDados>();
            return View(resultado);
        }

        private List<Contrato> FiltrarDados(IQueryable<Contrato> dados, PesquisaViewModel filtro)
        {
            var resultado = dados.ToList();

            if (filtro.CodConstrutora > 0)
            {
                resultado = resultado.Where(x => x.TabelaUnidade.TabelaBloco.TabelaEmpreendimento.CodConstrutora == filtro.CodConstrutora).ToList();
            }
            if (filtro.CodEmpreendimento > 0)
            {
                resultado = resultado.Where(x => x.TabelaUnidade.TabelaBloco.CodEmpreend == filtro.CodEmpreendimento).ToList();
            }
            if (filtro.CodRegional > 0)
            {
                resultado = resultado.Where(x => x.TabelaUnidade.TabelaBloco.TabelaEmpreendimento.CodRegional == filtro.CodRegional).ToList();
            }
            if (filtro.CodBloco > 0)
            {
                resultado = resultado.Where(x => x.TabelaUnidade.CodBloco == filtro.CodBloco).ToList();
            }
            if (filtro.CodUnidade > 0)
            {
                resultado = resultado.Where(x => x.CodUnidade == filtro.CodUnidade).ToList();
            }
            if (filtro.CodDonoCarteira > 0)
            {
                resultado = resultado.Where(x => x.CodUsuarioDonoCarteira == filtro.CodDonoCarteira).ToList();
            }
            if (filtro.CodTipoCobranca > 0)
            {
                resultado = resultado.Where(x => x.CodTipoCobranca == filtro.CodTipoCobranca).ToList();
            }
            if (filtro.CodGestao > 0)
            {
                resultado = resultado.Where(x => x.CodGestao == filtro.CodGestao).ToList();
            }
            if (filtro.CodSintese > 0)
            {
                resultado = resultado.Where(c => c.ContratoHistoricoes.Any() && c.ContratoHistoricoes.LastOrDefault().CodSintese == filtro.CodSintese).ToList();
            }
            if (filtro.CodEscritorio > 0)
            {
                resultado = resultado.Where(x => x.CodEscritorio == filtro.CodEscritorio).ToList();
            }
            if (filtro.Bloqueado == true)
            {
                resultado = resultado.Where(x => x.Bloqueado == filtro.Bloqueado).ToList();
            }
            if (!String.IsNullOrEmpty(filtro.NomeComprador))
            {
                resultado = resultado.Where(x => x.ContratoClientes.Any(c => !c.DataExcluido.HasValue && c.Cliente.Nome.ToUpper().Contains(filtro.NomeComprador.ToUpper()))).ToList();
            }
            if (!String.IsNullOrEmpty(filtro.CPF_CNPJ))
            {
                resultado = resultado.Where(x => x.ContratoClientes.Any(c => !c.DataExcluido.HasValue && c.Cliente.ClienteDocumentoes.Any(d => d.NumeroDocumento.Contains(filtro.CPF_CNPJ)))).ToList();
            }
            if (!String.IsNullOrEmpty(filtro.CodigoVenda))
            {
                resultado = resultado.Where(x => x.NumeroContrato.Contains(filtro.CodigoVenda)).ToList();
            }
            if (filtro.StatusUnidade > 0)
            {
                resultado = resultado.Where(x => x.TabelaUnidade.Status == filtro.StatusUnidade).ToList();
            }

            return resultado.Take(150).ToList();
        }

        [HttpPost]
        public JsonResult ListarAcaoPorCobranca(int CodTipoCobranca = 0)
        {
            var listaAcoes = ListarTipoCobrancaAcao(CodTipoCobranca);
            var selectListAcao = from p in listaAcoes select new { Cod = p.Cod, Text = p.TabelaAcaoCobranca.AcaoCobranca };

            return Json(selectListAcao);
        }

        public List<TabelaTipoCobrancaAcao> ListarTipoCobrancaAcao(int CodTipoCobranca = 0)
        {
            var listaAcaoCobranca = _dbTipoCobrancaAcao.All().Where(x => x.CodTipoCobranca == CodTipoCobranca && x.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.SMS).ToList();
            return listaAcaoCobranca.ToList();
        }
        public List<DictionaryEntry> Fases(int CodTipoCobranca = 0)
        {

            //int CodTipoCobranca .Where(x=>x.CodTipoCobranca == CodTipoCobranca)
            List<TabelaFase> lista = new List<TabelaFase>();
            lista = _dbFase.All().OrderBy(x => x.Ordem).Where(x => x.CodTipoCobranca == CodTipoCobranca).ToList();
            lista.Insert(0, new TabelaFase());
            var list = from p in lista select new DictionaryEntry(DbRotinas.Descriptografar(p.Fase), p.Cod);
            return list.ToList();
        }


        [HttpGet]
        public ActionResult Servicos(int id = 0)
        {
            if (id == 0)
            {
                id = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato);
            }

            var dados = _dbContrato.FindById(id);

            if (dados == null)
                dados = new Db.Models.Contrato();

            return View(dados);
        }

        private void AnexarDocumento(ref ContratoDocumento doc, HttpRequestBase request)
        {
            string pathUpload = Server.MapPath("~/Content/files");
            string pathDefinitivo = Server.MapPath(string.Format("~/Documentos/Contrato/", doc.Cod));
            doc.ArquivoCaminho = String.Format("~/Documentos/Contrato/{0}", doc.ArquivoGuid);
            string file = doc.ArquivoNome;

            if (!Directory.Exists(pathDefinitivo))
                Directory.CreateDirectory(pathDefinitivo);
            if (file != null)
            {
                string pathDoc = Path.Combine(pathUpload, file);
                FileInfo fileInfo = new FileInfo(pathDoc);
                if (fileInfo.Exists)
                {
                    fileInfo.MoveTo(Path.Combine(pathDefinitivo, doc.ArquivoGuid));
                }
            }
        }


        [HttpPost]
        public ActionResult paginarDados(int draw, int start, int length)
        {
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var query_dados = new List<ContratoDados>();
            if (filtro.CodTipoCobranca > 0)
            {
                query_dados = _qry.ListarContratosDados(
                filtro.CodigoVenda,
                filtro.CodigoCliente,
                filtro.NomeComprador,
                filtro.PrimeiroNome,
                filtro.CPF_CNPJ,
                filtro.CodConstrutora,
                filtro.CodDonoCarteira,
                filtro.CodRegional,
                filtro.CodTipoCobranca,
                filtro.CodTipoCarteira,
                filtro.CodGestao,
                filtro.CodEscritorio,
                filtro.CodEmpreendimento,
                filtro.CodBloco,
                filtro.CodUnidade,
                filtro.Bloqueado,
                filtro.CodAgingCliente,
                filtro.AndamentoDaObra,
                filtro.NumeroContrato,
                filtro.CodSintese,
                filtro.CodProduto,
                filtro.CodFase,
                filtro.CodGrupo,
                filtro.CodTipoBloqueio);
            }


            DataTableData d = new DataTableData();

            d.draw = draw;
            d.recordsTotal = length;
            int recordsFiltered = query_dados.Count;
            d.data = query_dados.Skip(start).Take(length);
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult paginarDadosAll()
        {
            var query_dados = _qry.ListarContratosPaginadas(0, 1000);
            var contratos = ObterDadosSession();

            DataTableData d = new DataTableData();

            //d.draw = draw;
            //d.recordsTotal = length;
            int recordsFiltered = 67000;
            d.data = query_dados;
            d.recordsFiltered = recordsFiltered;

            return Json(d, JsonRequestBehavior.AllowGet);
        }

        private List<Contrato> ObterDadosSession(bool forceReload = false)
        {
            if (forceReload)
            {
                Session["filtro_dados"] = null;
            }
            List<Contrato> dados = new List<Db.Models.Contrato>();
            if (Session["filtro_dados"] != null)
                dados = WebRotinas.CookieObjetoLer<List<Contrato>>("filtro_dados");
            else
            {
                var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
                var contratos = _dbContrato.All();
                var resultado = FiltrarDados(contratos, filtro);
                WebRotinas.CookieObjetoGravar("filtro_dados", resultado);
            }
            return dados;
        }

        [HttpGet]
        public FileStreamResult GerarLayoutSms()
        {

            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var query_dados = new List<ExportarSms>();
            if (filtro.CodTipoCobranca > 0)
            {
                query_dados = _qry.ListarExportarSms(
              filtro.CodigoVenda,
              filtro.CodigoCliente,
              filtro.NomeComprador,
              filtro.CPF_CNPJ,
              filtro.CodConstrutora,
              filtro.CodDonoCarteira,
              filtro.CodRegional,
              filtro.CodTipoCobranca,
              filtro.CodTipoCarteira,
              filtro.CodGestao,
              filtro.CodEscritorio,
              filtro.CodEmpreendimento,
              filtro.CodBloco,
              filtro.CodUnidade,
              filtro.Bloqueado,
              filtro.CodAgingCliente,
              filtro.AndamentoDaObra);
            }

            string pathTemp = Path.GetTempFileName() + ".xls";

            // Gerar Arquivo Excel
            CreateExcelFile.CreateExcelDocument(query_dados, pathTemp);
            //string caminhoReal = Server.MapPath(pathTemp);
            FileInfo info = new FileInfo(pathTemp);

            return File(info.OpenRead(), "application/force-download", "sms.xls");
        }

        [HttpGet]
        public FileStreamResult GerarLayoutCarta()
        {

            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var dados = _qry.ListarContratosAcaoCobrancaHistoricoDados(filtro.TipoAcaoCobranca, filtro.CodTipoCobranca, filtro.CodAcaoCobranca, filtro.SituacaoTipo);
            var carta = _qry.ListarExportarCarta(filtro.TipoAcaoCobranca, filtro.CodTipoCobranca, filtro.CodAcaoCobranca);

            string pathTemp = Path.GetTempFileName() + ".xls";

            // Gerar Arquivo Excel
            CreateExcelFile.CreateExcelDocument(carta, pathTemp);
            //string caminhoReal = Server.MapPath(pathTemp);
            FileInfo info = new FileInfo(pathTemp);
            foreach (var d in dados)
            {
                var acao = _dbContratoAcaoCobrancaHistorico.FindById((int)d.CodContratoAcaoCobrancaHistorico);

                acao.DataEnvio = DateTime.Now;
                acao.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                acao.EnvioAutomatico = false;
                _dbContratoAcaoCobrancaHistorico.CreateOrUpdate(acao);
            }
            return File(info.OpenRead(), "application/force-download", "carta.xls");
        }

        [HttpGet]
        public FileStreamResult GerarLayoutEmail()
        {
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var dados = _qry.ListarContratosAcaoCobrancaHistoricoDados(filtro.TipoAcaoCobranca, filtro.CodTipoCobranca, filtro.CodAcaoCobranca, filtro.SituacaoTipo);
            var carta = _qry.ListarExportarEmail(filtro.TipoAcaoCobranca, filtro.CodTipoCobranca, filtro.CodAcaoCobranca);

            string pathTemp = Path.GetTempFileName() + ".xls";

            // Gerar Arquivo Excel
            CreateExcelFile.CreateExcelDocument(carta, pathTemp);
            //string caminhoReal = Server.MapPath(pathTemp);
            FileInfo info = new FileInfo(pathTemp);
            foreach (var d in dados)
            {
                var acao = _dbContratoAcaoCobrancaHistorico.FindById((int)d.CodContratoAcaoCobrancaHistorico);

                acao.DataEnvio = DateTime.Now;
                acao.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                acao.EnvioAutomatico = false;
                _dbContratoAcaoCobrancaHistorico.CreateOrUpdate(acao);
            }

            return File(info.OpenRead(), "application/force-download", "email.xls");
        }

        // [HttpGet]
        //public FileStreamResult GerarLayoutSmsTxt()
        //{
        //
        //    var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
        //    var dados = _qry.ListarContratosAcaoCobrancaHistoricoDados(filtro.TipoAcaoCobranca, filtro.CodTipoCobranca, filtro.CodAcaoCobranca, filtro.SituacaoTipo);
        //
        //    string pathTemp = Path.GetTempFileName() + ".txt";
        //    // Append text to an existing file named "WriteLines.txt".
        //    using (StreamWriter outputFile = new StreamWriter(pathTemp, true))
        //    {
        //        outputFile.WriteLine(string.Format("1{0:yyyyMMdd}Gafisa", DateTime.Now));
        //
        //        string menagem = string.Empty;
        //        foreach (var i in dados)
        //        {
        //            var acao = _dbContratoAcaoCobrancaHistorico.FindById((int)i.CodContratoAcaoCobrancaHistorico);
        //            if (string.IsNullOrEmpty(menagem))
        //            {
        //                menagem = acao.Conteudo;
        //                //Caso nao possua a mensagem ele escreve msg em branco 
        //                if (string.IsNullOrEmpty(menagem))
        //                {
        //                    menagem = "Mensagem em branco";
        //                }
        //                outputFile.WriteLine(string.Format("2{0}", menagem));
        //            }
        //            if (!string.IsNullOrEmpty(i.TelefoneSMS))
        //            {
        //                //Verifica se a expressão contem caracter invalidos ... config para somente numeros
        //                outputFile.WriteLine(string.Format("3{0}", Regex.Replace(i.TelefoneSMS, @"[^\d]", "")));
        //
        //                //Rotinas de Carimbar o Historico
        //                acao.DataEnvio = DateTime.Now;
        //                acao.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
        //                acao.EnvioAutomatico = false;
        //                _dbContratoAcaoCobrancaHistorico.CreateOrUpdate(acao);
        //            }
        //        }
        //        outputFile.WriteLine(string.Format("9{0:000000000}", dados.Count));
        //    }
        //    FileInfo info = new FileInfo(pathTemp);
        //
        //    //foreach (var item in query_contatos)
        //    //{
        //    //    //dadosHistoricoAcaoCobranca.CodAcaoCobranca = filtro.CodCobrancaAcao;
        //    //    dadosHistoricoAcaoCobranca.Cod = 0;
        //    //    dadosHistoricoAcaoCobranca.CodContrato = 0;
        //    //    dadosHistoricoAcaoCobranca.CodTipoCobrancaAcao = 0;
        //    //    dadosHistoricoAcaoCobranca.CodUsuario = 0;
        //    //    dadosHistoricoAcaoCobranca.Conteudo = null;
        //    //    dadosHistoricoAcaoCobranca.DataAlteracao = DateTime.Now;
        //    //    //dadosHistoricoAcaoCobranca.DataCadastro = null;
        //    //    //dadosHistoricoAcaoCobranca.DataEnvio = null;
        //    //    //dadosHistoricoAcaoCobranca.DataEnvioAgendado = null;
        //    //    dadosHistoricoAcaoCobranca.EnvioAutomatico = null;
        //    //    dadosHistoricoAcaoCobranca.Status = "Enviado";
        //
        //    //    dadosHistoricoAcaoCobranca.CodContrato = item.CodContrato;
        //    //    dadosHistoricoAcaoCobranca.DataCadastro = Convert.ToDateTime(item.DataProcessamento);
        //    //    _dbContratoAcaoCobrancaHistorico.CreateOrUpdate(dadosHistoricoAcaoCobranca);
        //    //}
        //    return File(info.OpenRead(), "application/force-download", "sms.txt");
        //}

        [HttpGet]
        public FileStreamResult GerarLayoutSerasa()
        {

            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var dados = _qry.ListarContratosAcaoCobrancaHistoricoDados(filtro.TipoAcaoCobranca, filtro.CodTipoCobranca, filtro.CodAcaoCobranca, filtro.SituacaoTipo);
            var serasa = _qry.ListarExportarSerasa(filtro.TipoAcaoCobranca, filtro.CodTipoCobranca, filtro.CodAcaoCobranca);


            string pathTemp = Path.GetTempFileName() + ".txt";

            // Append text to an existing file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(pathTemp, true))
            {
                foreach (var i in serasa)
                {
                    outputFile.WriteLine(i.Linha);
                }
                foreach (var d in dados)
                {
                    var acao = _dbContratoAcaoCobrancaHistorico.FindById((int)d.CodContratoAcaoCobrancaHistorico);

                    acao.DataEnvio = DateTime.Now;
                    acao.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                    acao.EnvioAutomatico = false;
                    _dbContratoAcaoCobrancaHistorico.CreateOrUpdate(acao);
                }
            }
            FileInfo info = new FileInfo(pathTemp);
            return File(info.OpenRead(), "application/force-download", "serasa.txt");
        }

        [HttpPost]
        public FilePathResult ExtrairStatus(int CodFase = 0, int CodSintese = 0, string De = null, string Ate = null)
        {
            DateTime DataDe = DbRotinas.ConverterParaDatetime(De);
            DateTime DataAte = DbRotinas.ConverterParaDatetime(Ate);

            var parcelas = _qry.ExtrairClassificacaoParcela(CodFase, CodSintese, DataDe, DataAte);
            string pathTemp = Path.GetTempFileName() + ".txt";
            using (TextWriter tw = System.IO.File.CreateText(pathTemp))
            {
                foreach (var item in parcelas)
                {
                    tw.WriteLine(string.Format("{0};{1};{2};{3};{4};{5};{6}",
                        DbRotinas.Descriptografar(item.CodCliente)
                      , item.FluxoPagamento
                      , DbRotinas.Descriptografar(item.NumeroContrato)
                      , item.NumeroParcela
                      , item.CodRegionalCliente
                      , item.CodSinteseCliente
                      , item.NossoNumero
                    ));
                }
                tw.Close();
            }
            FileInfo info = new FileInfo(pathTemp);
            var nmarquivo = "junixclassificacaofatura.txt";
            return File(info.FullName, "application/force-download", nmarquivo);

        }

        [HttpPost]
        public FilePathResult ExtrairNegativacao(int CodFase = 0, int CodSintese = 0, string De = null, string Ate = null)
        {
            DateTime DataDe = DbRotinas.ConverterParaDatetime(De);
            DateTime DataAte = DbRotinas.ConverterParaDatetime(Ate);

            var parcelas = _qry.ExtrairNegativacao(CodFase, CodSintese, DataDe, DataAte);
            string pathTemp = Path.GetTempFileName() + ".txt";
            using (TextWriter tw = System.IO.File.CreateText(pathTemp))
            {
                tw.WriteLine($"Extração de Negativados: {parcelas.Count()} parcelas | Periodo De:{DataDe:dd/MM/yyyy} Ate:{DataAte:dd/MM/yyyy}");
                tw.WriteLine($"");
                tw.WriteLine($"An8;Contrato;Status;NrFatura;NrParcela;VrAberto;Filial;Classificação;DtAcao;");
                foreach (var item in parcelas)
                {
                    tw.WriteLine($"{DbRotinas.Descriptografar(item.CodCliente)}" +
                        $";{DbRotinas.Descriptografar(item.NumeroContrato)};" +
                        $"{item.StatusContrato};" +
                        $"{item.FluxoPagamento};" +
                        $"{item.NumeroParcela};" +
                        $"{item.ValorAberto:N2};" +
                        $"{item.CodRegionalCliente};" +
                        $"{item.CodSinteseCliente};" +
                        $"{item.AcaoDtGerada:dd/MM/yyyy};"
                    );
                }
                tw.Close();
            }
            FileInfo info = new FileInfo(pathTemp);
            var nmarquivo = "junixNegativacaoFatura.txt";
            return File(info.FullName, "application/force-download", nmarquivo);

        }

        [HttpPost]
        public FilePathResult ExtrairPersonalizado(int CodFase = 0, int CodSintese = 0, string De = null, string Ate = null)
        {
            DateTime DataDe = DbRotinas.ConverterParaDatetime(De);
            DateTime DataAte = DbRotinas.ConverterParaDatetime(Ate);

            var parcelas = _qry.ExtrairPersonalizada(CodFase, CodSintese, DataDe, DataAte);
            string pathTemp = Path.GetTempFileName() + ".txt";
            using (TextWriter tw = System.IO.File.CreateText(pathTemp))
            {
                foreach (var item in parcelas)
                {
                    var arrayRow = item.Linha.Split(';').ToList();
                    var strRow = "";
                    for (int i = 0; i < arrayRow.Count; i++)
                    {
                        var strPositon = arrayRow[i];
                        if (!String.IsNullOrEmpty(strPositon))
                        {
                            if (strPositon[0].ToString() == "@")
                            {

                                strRow = $"{strRow}{DbRotinas.Descriptografar(arrayRow[i].Replace("@", "").ToString())};";
                            }
                            else
                            {
                                strRow = $"{strRow}{arrayRow[i].ToString()};";
                            }
                        }
                    }
                    tw.WriteLine($"{strRow}");
                }
                tw.Close();
            }
            FileInfo info = new FileInfo(pathTemp);
            var nmarquivo = "ExtrairCustomizada.csv";
            return File(info.FullName, "application/force-download", nmarquivo);

        }



        #region Processar E-Mails em Massa
        [HttpPost, ValidateInput(false)]
        public JsonResult DispararEmails(string parcelas = null, string enderecoEmail = null, string mensagem = null, string assunto = null)
        {

            OperacaoRetorno op = new OperacaoRetorno();
            List<ContratoParcela> listaParcelas = new List<ContratoParcela>();
            List<EmailInfo> Emails = new List<EmailInfo>();
            int TotalEmail = 0;
            int TotalEmailComErro = 0;
            int TotalRegistro = 0;
            int TotalParcelaErro = 0;
            int TotalParcelaProcessada = 0;

            try
            {
                int[] ParcelaIds = Array.ConvertAll(parcelas.Split(','), delegate (string p) { return int.Parse(p); });
                foreach (var p in ParcelaIds)
                {
                    var parcela = _dbParcela.FindById(p);
                    if (parcela != null) listaParcelas.Add(parcela);
                }
                //Quantidade de Parcelas Encontradas
                TotalRegistro = ParcelaIds.Count();

                foreach (var item in listaParcelas.GroupBy(x => x.Contrato?.ContratoClientes?.FirstOrDefault()))
                {
                    var cdCliente = item.Select(x => x.Contrato?.ContratoClientes?.FirstOrDefault().CodCliente).FirstOrDefault();
                    var Parcelas = item.Select(x => x).ToList();
                    var QtdeParaEsteCliente = parcelas.Count();
                    try
                    {
                        Emails.Add(ProcessarinformacoesEmail((int)cdCliente, Parcelas, assunto, enderecoEmail, mensagem));

                    }
                    catch (Exception)
                    {
                        //Adiciona a lista com erros;
                        TotalParcelaErro += parcelas.Count();
                    }
                    finally
                    {
                        TotalParcelaProcessada += Parcelas.Count();
                    }

                }
                TotalEmail = Emails.Count();
                foreach (var email in Emails)
                {
                    Enumeradores.LogEmailStatus status = Enumeradores.LogEmailStatus.Enviado;

                    var NotificacaoRetorno = _objEmailRotinas.Enivar(email);
                    if (!NotificacaoRetorno.OK)
                    {
                        status = Enumeradores.LogEmailStatus.FalhaNoEnvio;
                        TotalEmailComErro++;
                    }

                    //Registra Historico no logEmail
                    var obj = RegistraLogEmail(email, status, NotificacaoRetorno.Mensagem);

                }

                return Json(new { OK = true, Mensagem = $"Processo Finalizado ... {Environment.NewLine}Total de argumentos enviados: {TotalRegistro} | Total de parcelas encontradas: {TotalParcelaProcessada}{Environment.NewLine}Emails: {TotalEmail} | Emails não enviados: {TotalEmailComErro} {Environment.NewLine}" });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = $"Processo Finalizado ... {Environment.NewLine}Total enviada:{TotalRegistro} | Total de parcelas encontradas: {TotalParcelaProcessada}{Environment.NewLine}Emails:{TotalEmail} | Emails não enviados: {TotalEmailComErro} {Environment.NewLine}{Environment.NewLine}{ex.Message}" });
            }

        }

        public EmailInfo ProcessarinformacoesEmail(int CodCliente, List<ContratoParcela> Parcelas, string _assunto, string emailDestinatario, string mensagem)
        {
            try
            {
                var objAcao = _dbTabelaModeloAcao.FindAll(x => !x.DataExcluido.HasValue && x.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.EMAIL_SEGUNDA_VIA_BOLETO).FirstOrDefault();
                BoletoUtils gerarBoleto = new BoletoUtils();
                LogEmail email = new LogEmail();
                StringBuilder sb = new StringBuilder();
                int codRegional = 0;
                if (Parcelas != null)
                {
                    Contrato contrato = Parcelas.Select(x => x.Contrato).First();
                    if (contrato.CodRegionalProduto > 0)
                    {
                        codRegional = (int)contrato.TabelaRegionalProduto.CodRegional;
                    }
                    if (CodCliente == 0)
                    {
                        CodCliente = (int)contrato.CodClientePrincipal;
                    }
                }
                codRegional = codRegional > 0 ? codRegional : (int)WebRotinas.ObterUsuarioLogado().CodRegional;
                var objRegional = _dbFilial.FindById(codRegional);
                string emailRegional = WebRotinas.ObterUsuarioLogado().Email ?? "cobflow@junix.com.br";

                var objCliente = _dbCliente.FindById(CodCliente);
                email.Assunto = "";

                _objParametro = _dbParametro.FindAll().FirstOrDefault();
                _objEmailRotinas = new EmailRotinas(_objParametro.SMTPPorta, _objParametro.SMTPServidor, _objParametro.SMTPUsuario, _objParametro.SMTPSenha, _objParametro.fgSSL == true);

                sb.Append(mensagem);
                if (_assunto == null)
                {
                    email.Assunto = objAcao.CodAcaoCobranca > 0 ? objAcao.TabelaAcaoCobranca.AcaoCobranca : "Otis - 2ª via de boleto";
                }
                else
                {
                    email.Assunto = _assunto;
                }


                string tbParcelas = MontarTabelaHtml(Parcelas);
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.TabelaParcelas), tbParcelas);
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NomeComprador), DbRotinas.Descriptografar(objCliente.Nome));
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.CPF_CPNPJ_Comprador), DbRotinas.Descriptografar(objCliente.CPF));
                if (objRegional != null)
                {
                    //DADOS DA REGIONAL PRODUTO;
                    sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.DescricaoFilial), DbRotinas.Descriptografar(objRegional.Regional));
                    sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NumeroFilial), objRegional.Telefone);
                    sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.ResponsavelFilial), objRegional.Responsavel);
                    emailRegional = objRegional.Email;
                    email.NomeRemetente = objRegional.Responsavel;
                }
                email.Conteudo = sb.ToString();

                email.HTML = true;
                email.EmailRemetente = emailRegional;


                email.EmailDestinatario = !String.IsNullOrEmpty(emailDestinatario) ? emailDestinatario : DbRotinas.Descriptografar(objCliente.Email);
                email.NomeDestinatario = DbRotinas.Descriptografar(objCliente.Nome);
                var objEmail = new EmailInfo();
                foreach (var p in Parcelas)
                {
                    EmailInfoArquivo arquivo = new EmailInfoArquivo();
                    if (p.LinhaDigitavel != null)
                    {
                        BoletoUtils.BoletoRetorno objRetornoBoleto = gerarBoleto.GerarBoletoPorLinhaDigitavel(p);

                        var boletoByte = gerarBoleto.MontaBoletoEmBytePDF(objRetornoBoleto.Boleto, "", "", p.FluxoPagamento, false, false);
                        //var boletoByte = objRetornoBoleto.Boleto.MontaBytesPDF();
                        arquivo.Dados = boletoByte;
                        arquivo.NomeArquivo = p.FluxoPagamento + ".pdf";
                        objEmail.Arquivos.Add(arquivo);
                    }
                }

                EmailInfo _email = new EmailInfo()
                {
                    Assunto = email.Assunto,
                    Conteudo = email.Conteudo,
                    EmailRemetente = email.EmailRemetente,
                    EmailDestinatario = email.EmailDestinatario,
                    NomeDestinatario = email.NomeDestinatario != null ? email.NomeDestinatario : "Destinatario",
                    NomeRemetente = email.NomeRemetente != null ? email.NomeRemetente : WebRotinas.ObterUsuarioLogado().Nome,
                    Arquivos = objEmail.Arquivos,
                    HTML = true
                };

                return _email;
            }
            catch (Exception ex)
            {
                throw new Exception($"Erro processar as informações : {CodCliente} ", ex);
            }


        }
        public byte[] GetPDF(string pHTML)
        {
            byte[] bPDF = null;

            MemoryStream ms = new MemoryStream();
            TextReader txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class
            Document doc = new Document(PageSize.A4, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file
            PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document
            HTMLWorker htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document
            doc.Open();
            htmlWorker.StartDocument();

            // 5: parse the html into the document
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }
        public OperacaoRetorno RegistraLogEmail(EmailInfo email, Enumeradores.LogEmailStatus status, string mensagem)
        {
            OperacaoRetorno objOp = new OperacaoRetorno();
            try
            {
                //*******REGISTRA NA TABELA LOGEMAIL******//
                LogEmail _logEmail = new LogEmail();
                _logEmail.EmailRemetente = email.EmailRemetente;
                _logEmail.EmailDestinatario = email.EmailDestinatario;
                _logEmail.NomeDestinatario = email.NomeDestinatario;
                _logEmail.NomeRemetente = email.NomeRemetente;
                _logEmail.Assunto = email.Assunto;
                _logEmail.Conteudo = email.Conteudo;
                _logEmail.HTML = email.HTML;
                if (status == Enumeradores.LogEmailStatus.FalhaNoEnvio)
                {
                    _logEmail.TentativaEnvio = 3; //Para o Serviço despresar a tentativas.
                }
                _logEmail.Status = (short)status;
                _logEmail.Mensagem = mensagem;
                _dbLogEmail.CreateOrUpdate(_logEmail);
                objOp.OK = true;
            }
            catch (Exception ex)
            {
                objOp.OK = false;
                objOp.Mensagem = $"Erro - {ex.Message}";
            }
            return objOp;

        }
        public static string StripHTML(string inputHTML)
        {
            string noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
            //noHTML = Regex.Replace(noHTML, @"[\r\n]", " ").Trim();
            noHTML = DbRotinas.Criptografar(noHTML);
            return noHTML;

        }
        private string MontarTabelaHtml(List<ContratoParcela> parcelas)
        {
            StringBuilder sbTabelaParcelas = new StringBuilder();

            sbTabelaParcelas.AppendFormat($"<table border='1'>");
            sbTabelaParcelas.AppendFormat($"<tr stype='border-bottom:solid 1px #808080;'>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Contrato</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Fatura</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Nº Parcela</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Vencimento</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Valor</th>");
            sbTabelaParcelas.AppendFormat($"</tr>");



            foreach (var parcela in parcelas)
            {

                //emailRegional = parcela.Contrato.TabelaRegionalProduto.TabelaRegional.Email;

                sbTabelaParcelas.AppendFormat($"<tr style='border-bottom:solid 1px #808080'>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{DbRotinas.Descriptografar(parcela.Contrato.NumeroContrato)}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.FluxoPagamento}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.NumeroParcela}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.DataVencimento:dd/MM/yyyy}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.ValorAberto:C2}</td>");
                sbTabelaParcelas.AppendFormat($"</tr>");

            }
            sbTabelaParcelas.AppendFormat($"</table>");

            return sbTabelaParcelas.ToString();
        }
        #endregion


        [HttpPost]
        public JsonResult ObterDadosBoletoRemessa(DTParameters param, DateTime? PeriodoDe, DateTime? PeriodoAte, int? CdPortador)
        {
            try
            {
                var dtsource = ListaDadosBoleto(PeriodoDe, PeriodoAte, CdPortador);

                var result = WebRotinas.DTRotinas.DTResult(param, dtsource);

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }


        public IQueryable<object> ListaDadosBoleto(DateTime? _de, DateTime? _ate, int? _codPortador)
        {
            //QRY PARA TRAZER TODOS BOLETOS
            var query_dados = new List<BoletoSolicitacaoDados>();
            query_dados = _qry.ListarDadosBoletoRemessa(_de, _ate, _codPortador);

            var data = query_dados
                         .Select(x => new
                         {
                             CodBoleto = x.CodBoleto,
                             CodigoCliente = DbRotinas.Descriptografar(x.CodigoCliente),
                             Cliente = DbRotinas.Descriptografar(x.Nome),
                             Regional = DbRotinas.Descriptografar(x.Regional),
                             CodContrato = x.CodContrato,
                             CodHistorico = x.CodHistorico,
                             CodCliente = x.CodClientePrincipal,
                             TipoCobranca = DbRotinas.Descriptografar(x.TipoCobranca),
                             Fase = DbRotinas.Descriptografar(x.Fase),
                             Sintese = DbRotinas.Descriptografar(x.Sintese),
                             NumeroDocumento = $"{x.NumeroDocumento}",
                             DataVencimento = x.DataVencimento,
                             DataVencimentoDesc = $"{x.DataVencimento:dd/MM/yyyy}",
                             DataDocumento = x.DataDocumento,
                             DataDocumentoDesc = $"{x.DataDocumento:dd/MM/yyyy}",
                             JurosAoMesPorcentagem = x.JurosAoMesPorcentagem,
                             JurosAoMesPorcentagemDesc = $"{x.JurosAoMesPorcentagem:N2}",
                             JurosAoMesPorcentagemPrevisto = x.JurosAoMesPorcentagemPrevisto,
                             JurosAoMesPorcentagemPrevistoDesc = $"{x.JurosAoMesPorcentagemPrevisto:N2}",
                             ValorJuros = x.ValorJuros,
                             ValorJurosDesc = $"{x.ValorJuros:C}",
                             ValorJurosPrevisto = x.ValorJurosPrevisto,
                             ValorJurosPrevistoDesc = $"{x.ValorJurosPrevisto:C}",
                             MultaAtrasoPorcentagem = x.MultaAtrasoPorcentagem,
                             MultaAtrasoPorcentagemDesc = $"{x.MultaAtrasoPorcentagem:N2}",
                             MultaAtrasoPorcentagemPrevisto = x.MultaAtrasoPorcentagemPrevisto,
                             MultaAtrasoPorcentagemPrevistoDesc = $"{x.MultaAtrasoPorcentagemPrevisto:N2}",
                             ValorMulta = x.ValorMulta,
                             ValorMultaDesc = $"{x.ValorMulta:C}",
                             ValorMultaPrevisto = x.ValorMultaPrevisto,
                             ValorMultaPrevistoDesc = $"{x.ValorMultaPrevisto:C}",
                             ValorDocumentoOriginal = x.ValorDocumentoOriginal,
                             ValorDocumentoOriginalDesc = $"{x.ValorDocumentoOriginal:C}",
                             ValorDocumentoPrevisto = x.ValorDocumentoPrevisto,
                             ValorDocumentoPrevistoDesc = $"{x.ValorDocumentoPrevisto:C}",
                             ValorDocumento = x.ValorDocumento,
                             ValorDocumentoDesc = $"{x.ValorDocumento:C}",
                             Observacao = x.Observacao,
                             Solicitante = x.Usuario,
                             DataCadastroDesc = $"{x.DataCadastro:dd/MM/yyyy HH: mm}",
                             DataCadastro = x.DataCadastro,
                             DataAprovacaoDesc = $"{x.DataAprovacao:dd/MM/yyyy HH:mm}",
                             DataAprovacao = x.DataAprovacao,
                             x.NossoNumero,
                             BoletoTipo = x.Alterar == true ? "Alteração" : "Novo",
                             x.DataRemessa,
                             StatusTransmissao = x.DataRemessa == null ? "Pendente" : x.Erro == null ? "Emitido" : "Erro",
                             IsValid = x.Status == (short)Enumeradores.StatusBoleto.Aprovado ? true : false,
                             IsEmitido = x.DataRemessa == null ? false : true,
                             RemessaId = x.CodLogArquivo,
                             DataRemessaDesc = $"{x.DataRemessa:dd/MM/yyyy HH: mm}"
                         }).AsQueryable();


            return data;
        }
        #region Excel Export

        [HttpPost]
        public JsonResult ExportarExcelRemessa(DTParameters param, DateTime? PeriodoDe, DateTime? PeriodoAte, int? CdPortador)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                Helper _rotina = new Helper();

                List<ContratoDados> Dados = new List<ContratoDados>();
                List<ConverterListaParaExcel.CollumnExcel> columns = new List<ConverterListaParaExcel.CollumnExcel>();
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "StatusTransmissao", Text = Idioma.Traduzir("Status") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Regional", Text = Idioma.Traduzir("Filial") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CodigoCliente", Text = Idioma.Traduzir("An8") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Cliente", Text = Idioma.Traduzir("Cliente") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "BoletoTipo", Text = Idioma.Traduzir("Tipo") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NumeroDocumento", Text = Idioma.Traduzir("Nr.Documento") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NossoNumero", Text = Idioma.Traduzir("NossoNumero") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataVencimentoDesc", Text = Idioma.Traduzir("Dt.Vencimento") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataDocumentoDesc", Text = Idioma.Traduzir("Dt.Documento") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ValorDocumentoPrevistoDesc", Text = Idioma.Traduzir("Vr.Doc.Prev") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ValorDocumentoDesc", Text = Idioma.Traduzir("Vr.Doc") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataCadastroDesc", Text = Idioma.Traduzir("Dt.Solicitação") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataAprovacaoDesc", Text = Idioma.Traduzir("Dt.Aprovação") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataRemessaDesc", Text = Idioma.Traduzir("Dt.Remessa") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Solicitante", Text = Idioma.Traduzir("Solicitante") });

                var dtsource = ListaDadosBoleto(PeriodoDe, PeriodoAte, CdPortador);
                var result = WebRotinas.DTRotinas.DTFilterResult(dtsource, param);

                var nomeArquivo = Idioma.Traduzir("ListaRemessa");
                op = _rotina.GerarArquivo(nomeArquivo, result, columns);
            }
            catch (Exception ex)
            {

                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }
        #endregion

        [HttpPost]
        public JsonResult GerarRemessa(List<int> ids)
        {
            BoletoUtils BoletoRotina = new BoletoUtils();
            Cobranca.Rotinas.Service.RemessaService RemessaService = new Cobranca.Rotinas.Service.RemessaService();
            Repository<TabelaBancoPortadorGrupo> _repParamGrupo = new Repository<TabelaBancoPortadorGrupo>();
            try
            {
                var usuarioId = WebRotinas.ObterUsuarioLogado().Cod;

                //var response = RemessaService.CriarArquivoRemessa(null, null, ids, paramGrupo);
                var resultado = (CriarRemessaCommandResult)_boletoHandler.Handle(new CriarRemessaCommand() { BoletoIds = ids, CodUsuario =  usuarioId });
                if (resultado.Success)
                {
                    //RemessaService.RegistrarBoletosProcessados(ref response);
                    //arquivosIdsLog.Add(response.CodLogArquivo);
                }
                
                var parametroGrupos = _repParamGrupo.All().Where(x => !x.DataExcluido.HasValue).ToList();
                List<int> arquivosIdsLog = new List<int>();


                //BoletoUtils.RemessaRetorno op = new BoletoUtils.RemessaRetorno();
                //foreach (var paramGrupo in parametroGrupos)
                //{
                //    if (paramGrupo.NumeroLoteAtual == null) paramGrupo.NumeroLoteAtual = paramGrupo.NumeroLote;

                //    var usuarioId = WebRotinas.ObterUsuarioLogado().Cod;

                //    //var response = RemessaService.CriarArquivoRemessa(null, null, ids, paramGrupo);
                //    var resultado = (CriarRemessaCommandResult)_boletoHandler.Handle(new CriarRemessaCommand() { BoletoIds = ids, CodUsuario =  });

                //    if (resultado.Success)
                //    {
                //        var nomeArquivo = paramGrupo.NomeArquivo;
                //        nomeArquivo = nomeArquivo.Replace("@NUMERO_LOTE_ATUAL", DbRotinas.FormatCode(DbRotinas.ConverterParaString(paramGrupo.NumeroLoteAtual), "0", 8, true));
                //        nomeArquivo = $"{nomeArquivo}.rem";
                //        RemessaService.RegistrarLogRemessa(nomeArquivo, resultado.Arquivo, resultado.ListaBoletoOK.Count(), usuarioId);
                //        RemessaService.RegistrarBoletosProcessados(ref response);
                //        //arquivosIdsLog.Add(response.CodLogArquivo);

                //        op.OK = true;
                //        op.Mensagem = $"{Idioma.Traduzir("Operação realizada com sucesso!")}";
                //    }
                //}

                return Json(new { resultado.Success, resultado.Message, remessa = resultado.ArquivoLogIds });
            }
            catch (Exception ex)
            {

                return Json(new OperacaoRetorno(ex));
            }
        }


        [HttpGet]
        public FileStreamResult DownloadRemessa(int cd, bool copia)
        {
            OperacaoRetorno op = new OperacaoRetorno();

            Repository<TabelaBancoPortadorLogArquivo> _dbLogArquivo = new Repository<TabelaBancoPortadorLogArquivo>();
            var doc = _dbLogArquivo.FindById(cd);

            string filename = WebRotinas.ObterArquivoTemp();
            FileInfo info = new FileInfo(filename);

            using (var sw = new StreamWriter(filename, false, Encoding.UTF8))
            {
                if (copia == true)
                    sw.Write($"**ARQUIVO TRANSMITIDO NA DATA DE {doc.DataCadastro: dd/MM/yyyy HH:mm}**{Environment.NewLine}");
                sw.Write(Encoding.UTF8.GetString(doc.Arquivo));
            }
            return File(info.OpenRead(), "application/force-download", doc.NomeDocumento);
        }



    }
}
