﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using BoletoNet;
using System.IO;
using Cobranca.Rotinas.BoletoGerar;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using Cobranca.Rotinas.Remessa;
using Recebiveis.Web.Models;
using Cobranca.Rotinas.Retorno;
using System.Threading.Tasks;
using Cobranca.Rotinas.BNet;

namespace Cobranca.Web.Controllers
{
    public class BancoController : BasicController<TabelaBanco>
    {
        public const string _filtroAcaoCobranca = "FiltroAcaoCobranca";
        public Repository<TabelaBanco> _dbBanco = new Repository<TabelaBanco>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<ContratoCliente> _dbContratoCliente = new Repository<ContratoCliente>();
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        private Repository<ContratoCliente> _ContratoCliente = new Repository<ContratoCliente>();
        private Repository<ContratoParcela> _ContratoParcela = new Repository<ContratoParcela>();
        public Repository<TabelaBancoPortador> _dbParametroBoleto = new Repository<TabelaBancoPortador>();
        private Repository<ContratoParcelaBoleto> _dbContratoParcelaBoleto = new Repository<ContratoParcelaBoleto>();
        public Repository<Acordo> _dbAcordo = new Repository<Acordo>();
        public Repository<ContratoHistorico> _dbContratoHistorico = new Repository<ContratoHistorico>();
        public Repository<Historico> _dbHistorico = new Repository<Historico>();
        public Repository<AcordoParcela> _dbAcordoParcela = new Repository<AcordoParcela>();
        public Repository<AcordoContratoParcela> _dbAcordoContratoParcela = new Repository<AcordoContratoParcela>();

        public Dados _qry = new Dados();

        [Authorize]
        [HttpGet]
        public override ActionResult Cadastro(int id = 0)
        {
            var model = _repository.FindById(id);
            if (model == null)
                model = new TabelaBanco();

            carregarCustomViewBags();

            return View("Cadastro", model);
        }


        public override void carregarCustomViewBags()
        {


            var listaCodigoBanco = new List<DictionaryEntry>();
            listaCodigoBanco.Add(new DictionaryEntry() { Key = null, Value = null });
            listaCodigoBanco.Add(new DictionaryEntry(EnumBoletoDescricao.BancoCodigo(EnumBoleto.BancoCodigo.BancodoBrasil), (short)EnumBoleto.BancoCodigo.BancodoBrasil));
            listaCodigoBanco.Add(new DictionaryEntry(EnumBoletoDescricao.BancoCodigo(EnumBoleto.BancoCodigo.Bradesco), (short)EnumBoleto.BancoCodigo.Bradesco));
            listaCodigoBanco.Add(new DictionaryEntry(EnumBoletoDescricao.BancoCodigo(EnumBoleto.BancoCodigo.Caixa), (short)EnumBoleto.BancoCodigo.Caixa));
            listaCodigoBanco.Add(new DictionaryEntry(EnumBoletoDescricao.BancoCodigo(EnumBoleto.BancoCodigo.Itau), (short)EnumBoleto.BancoCodigo.Itau));
            listaCodigoBanco.Add(new DictionaryEntry(EnumBoletoDescricao.BancoCodigo(EnumBoleto.BancoCodigo.Santander), (short)EnumBoleto.BancoCodigo.Santander));
            ViewBag.dropBanco = listaCodigoBanco.OrderBy(x => x.Value);



            base.carregarCustomViewBags();
        }

        [HttpPost]
        public FilePathResult ArquivoRemessa(string pIds)
        {
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroAcaoCobranca);
            //var dados = _qry.ListarContratosParcelaRemessa(filtro.TipoAcaoCobranca, filtro.CodTipoCobranca, filtro.CodAcaoCobr0anca, filtro.SituacaoTipo);
            List<ContratoParcelaViewModel> dados = new List<ContratoParcelaViewModel>();
            List<ContratoParcelaBoleto> parcelaBoletos = new List<ContratoParcelaBoleto>();

            #region Antigo
            //foreach (var item in pIds)
            //{
            //    parcelaBoletos.Add(_dbContratoParcelaBoleto.FindById(item));
            //}
            //// var parcelas = _ContratoParcela.All().Where(x => x.DataEmissao.HasValue).ToList();

            //foreach (var boleto in parcelaBoletos)
            //{
            //    dados.Add(new ContratoParcelaViewModel
            //    {
            //        CodBoleto = boleto.Cod,
            //        CodParcela = boleto.ContratoParcela.Cod,
            //        NumeroContrato = boleto.ContratoParcela.Contrato.NumeroContrato,
            //        NumeroParcela = boleto.ContratoParcela.NumeroParcela,
            //        DataVencimento = (DateTime)boleto.DataVencimento,
            //        strDataVencimento = string.Format("{0:dd/MM/yyyy}", boleto.DataVencimento),
            //        ValorParcela = string.Format("{0:0,0.00}", boleto.ValorDocumento),
            //        ValorAtualizado = string.Format("{0:0,0.00}", boleto.ValorDocumento), // atualizar quando for criado o campo valorAtualizado
            //        ValorTotal = boleto.ValorDocumento, // atualizar quando for criado o campo valorAtualizado
            //        StatusParcela = (boleto.DataVencimento < DateTime.Now ? (short)Enumeradores.StatusFatura.Vencida : (short)Enumeradores.StatusFatura.EmDia),
            //        Status = EnumeradoresDescricao.StatusFatura(boleto.DataVencimento < DateTime.Now ? (short)Enumeradores.StatusFatura.Vencida : (short)Enumeradores.StatusFatura.EmDia)
            //    });
            //}
            #endregion

            ArquivoRemessa arquivo = new ArquivoRemessa(TipoArquivo.CNAB400);

            RemessaCustom remessa = new RemessaCustom();
            string pathTemp = Path.GetTempFileName() + ".rem";
            FileStream fs = new FileStream(pathTemp, FileMode.Create);

            Boletos boletos = new Boletos();
            var arr = pIds.Split(',');

            foreach (var item in arr)
            {
                boletos.Add(montarDadosItau(DbRotinas.ConverterParaInt(item)));
            }

            var cedente = MontarCedente();
            var banco = montarBanco();

            string vMsgRetorno = string.Empty;

            remessa.GerarArquivoRemessa("0", banco, cedente, boletos, fs, 1, TipoArquivo.CNAB400);

            //ATUALIZA O BOLETO COLOCANDO A DATA DO ARQUIVO REMESSA
            foreach (var item in arr)
            {
                var boleto = _dbContratoParcelaBoleto.FindById(DbRotinas.ConverterParaInt(item));
                boleto.DataRemessa = DateTime.Now;
                boleto.CodUsuarioRemessa = WebRotinas.ObterUsuarioLogado().Cod;
                _dbContratoParcelaBoleto.Update(boleto);
            }

            FileInfo info = new FileInfo(pathTemp);
            var nmarquivo = "remessa-" + DateTime.Now.ToString("dd-MM-yy") + ".rem";
            return File(info.FullName, "application/force-download", nmarquivo);

        }

        [HttpPost]
        public JsonResult ValidarBanco(int id = 0, short banco = 0, bool padrao = false)
        {
            OperacaoRetorno retorno = new OperacaoRetorno();
            retorno.OK = true;
            try
            {
                var JaExiste = (bool)(_dbBanco.All().Where(x => x.Numero == banco && x.Cod != id).Count() > 0);
                if (JaExiste)
                {
                    retorno.OK = false;
                    retorno.Mensagem = $"Já existe um registro para este banco";
                }
                else if (padrao)
                {
                    var bancoPadrao = (bool)(_dbBanco.All().Where(x => x.BancoPadrao == true && x.Cod != id).Count() > 0);
                    if (bancoPadrao)
                    {
                        retorno.OK = false;
                        retorno.Mensagem = $"Já existe um registro como banco padrão";
                    }

                }
            }
            catch (Exception ex)
            {
                retorno = new OperacaoRetorno(ex);
            }
            return Json(retorno);
        }

        private List<ContratoParcelaViewModel> obterParcelaPorCliente(int codCliente = 0)
        {
            var contratos = _ContratoCliente.All().Where(x => x.CodCliente == codCliente).ToList();
            List<ContratoParcelaViewModel> parcelas = new List<ContratoParcelaViewModel>();

            foreach (var c in contratos)
            {
                var cParcelas = _ContratoParcela.All().Where(x => x.CodContrato == c.CodContrato).ToList();
                foreach (var i in cParcelas) { _qry.executarAtualizaValorParcela(i.Cod); }
                cParcelas = _ContratoParcela.All().Where(x => x.CodContrato == c.CodContrato).ToList();
                cParcelas = cParcelas.OrderByDescending(x => x.DataVencimento).ToList();
                foreach (var p in cParcelas)
                {

                    parcelas.Add(new ContratoParcelaViewModel
                    {
                        CodParcela = p.Cod,
                        NumeroContrato = p.Contrato.NumeroContrato,
                        NumeroParcela = p.NumeroParcela,
                        DataVencimento = p.DataVencimento,
                        strDataVencimento = string.Format("{0:dd/MM/yyyy}", p.DataVencimento),
                        ValorParcela = string.Format("{0:0,0.00}", p.ValorParcela),
                        ValorAtualizado = string.Format("{0:0,0.00}", p.ValorAtualizado), // atualizar quando for criado o campo valorAtualizado
                        ValorTotal = p.ValorParcela, // atualizar quando for criado o campo valorAtualizado
                        StatusParcela = (p.DataVencimento < DateTime.Now ? (short)Enumeradores.StatusFatura.Vencida : (short)Enumeradores.StatusFatura.EmDia),
                        Status = EnumeradoresDescricao.StatusFatura(p.DataVencimento < DateTime.Now ? (short)Enumeradores.StatusFatura.Vencida : (short)Enumeradores.StatusFatura.EmDia)
                    });
                }
            }
            return parcelas;
        }

        [HttpPost]
        public JsonResult SalvarBanco(TabelaBanco dados)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                _dbBanco.CreateOrUpdate(dados);
                op.OK = true;
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }

        [HttpPost]
        public JsonResult BoletoPorLinhaDigitavel(string DataBoleto, int CodParcela = 0, bool isCliente = false)
        {
            try
            {
                BoletoUtils gerarBoleto = new BoletoUtils();
                _qry.executarAtualizaValorParcela(CodParcela);

                var parcela = _ContratoParcela.FindById(CodParcela);

                BoletoUtils.BoletoRetorno objRetornoBoleto = gerarBoleto.GerarBoletoPorLinhaDigitavel(parcela);

                var html = objRetornoBoleto.Boleto.MontaHtmlEmbedded();
                //ALINHAR ALINHA VALORES A DIREITA
                html = html.Replace("</style>", " .BB { text-align: right !important; } .c { text-align: right !important; } </style> ");

                return Json(new { OK = true, Html = html, Mensagem = objRetornoBoleto.Mensagem });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Html = ex.Message, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public FilePathResult BoletoPorLinhaDigitavelPdf(string DataBoleto, int CodParcela = 0, bool isCliente = false)
        {
            BoletoUtils gerarBoleto = new BoletoUtils();
            _qry.executarAtualizaValorParcela(CodParcela);

            var parcela = _ContratoParcela.FindById(CodParcela);

            string pathTemp = Path.GetTempFileName() + ".pdf";
            BoletoUtils.BoletoRetorno objRetornoBoleto = gerarBoleto.GerarBoletoPorLinhaDigitavel(parcela);

            byte[] _ByteArray = objRetornoBoleto.Boleto.MontaBytesPDF();
            FileStream fs = new FileStream(pathTemp, FileMode.Create);
            fs.Write(_ByteArray, 0, _ByteArray.Length);
            fs.Close();
            FileInfo info = new FileInfo(pathTemp);
            return File(info.FullName, "application/force-download", info.Name);

        }

        public BoletoNet.Boleto montarDadosItau(int codBoleto)
        {
            var boleto = _dbContratoParcelaBoleto.FindById(codBoleto);
            var cedente = _dbParametroBoleto.All().FirstOrDefault();
            var contrato = _dbContrato.FindById((int)boleto.CodContrato);
            var sacador = _dbCliente.FindById(contrato.CodClientePrincipal.Value);
            var c = MontarCedente();


            BoletoNet.Boleto b = new BoletoNet.Boleto((DateTime)boleto.DataVencimento, (Decimal)boleto.ValorDocumento, c.Carteira, Convert.ToString(boleto.Cod), c);

            //b.DataDocumento = (DateTime)boleto.DataDocumento;
            //b.DataVencimento = (contrato.ContratoParcelas.FirstOrDefault().DataVencimento);
            b.ValorBoleto = ((decimal)boleto.ValorDocumento);
            b.Banco = new Banco((int)cedente.TabelaBanco.Numero);

            b.DataDocumento = (DateTime)boleto.DataCadastro;

            // VERIFICA E COLOCA A INSTRUÇÂO DO CLIENTE
            Remessa rem = new Remessa();
            b.Remessa = rem;
            //SE NAO POSSUIR O CODIGO DA OCORRENCIA COLOCA COMO 01 - REMESSA 
            if (string.IsNullOrEmpty(boleto.CodOcorrencia) == true)
            {
                rem.CodigoOcorrencia = "01";
            }
            else
            {
                rem.CodigoOcorrencia = boleto.CodOcorrencia.ToString();
            }




            /////////NOSSO NUMERO //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //*Escriturais: é enviado zerado pela empresa e retornado pelo Banco Itaú na confirmação de entrada, com exceção da carteira 115 e 138 cuja faixa de Nosso Número é de livre utilização pelo cedente seguindo as regras das carteiras Diretas abaixo;
            //*Diretas: é de livre utilização pelo cedente, não podendo ser repetida se o número ainda estiver registrado no Banco Itaú ou se transcorridos menos de 45 dias de sua baixa / liquidação no Banco Itaú.Dependendo da carteira de cobrança utilizada a faixa de Nosso Número pode ser definida pelo Banco.
            //*Para todas as movimentações envolvendo o título, o “Nosso Número” deve ser informado.

            //b.NossoNumero = ""; devera ser o codBoleto
            /// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            b.NossoNumero = Convert.ToString(boleto.Cod);
            b.NumeroDocumento = Convert.ToString(boleto.NumeroDocumento);

            Instrucao_Itau item1 = new Instrucao_Itau(9, 5);
            //Instrucao_Itau item2 = new Instrucao_Itau(81, 10);
            //item2.Descricao += item2.QuantidadeDias.ToString() + " dias corridos do vencimento.";
            //b.Instrucoes.Add(item1); ESTA COM ERRO ! CORRETO SERIA 05 E ESTA RETORNANDO 5
            //b.Instrucoes.Add(item2);
            //JUROS DE 1 dia
            //DESCONTO DADO ATE A DATA TAL
            //VALOR IOF NOTA 14
            //VALOR ABATIMENTO CONCEDIDO NOTA 13
            //IDENTIFICAÇÂO DE INSCRITAO DO PAGADOR 01 = CPF 02 = CNPJ
            b.Sacado = new Sacado(sacador.CPF, sacador.Nome);
            b.Cedente = MontarCedente();

            b.Sacado.Endereco.End = sacador.Endereco != null ? sacador.Endereco.ToUpper() : "";
            b.Sacado.Endereco.Bairro = sacador.Bairro != null ? sacador.Bairro.ToUpper() : "";
            b.Sacado.Endereco.Cidade = sacador.Cidade != null ? sacador.Cidade.ToUpper() : "";
            b.Sacado.Endereco.CEP = sacador.CEP != null ? sacador.CEP.ToUpper() : "";
            b.Sacado.Endereco.UF = sacador.UF != null ? sacador.UF.ToUpper() : "";

            return b;

        }

        public Banco montarBanco()
        {
            var cedente = _dbParametroBoleto.All().FirstOrDefault();

            var boletoInfo = new Banco(cedente.CodBanco.HasValue ? (short)cedente.TabelaBanco.Numero.Value : (short)0);

            return boletoInfo;
        }

        public Cedente MontarCedente()
        {
            var cedente = _dbParametroBoleto.All().FirstOrDefault();

            Cedente cedenteInfo = new Cedente();
            cedenteInfo.Nome = cedente.Portador != null ? cedente.Portador.ToUpper() : "";
            cedenteInfo.CPFCNPJ = cedente.CNPJ;
            //cedenteInfo.Codigo = cedente.CodigoCedente;
            cedenteInfo.Carteira = "109"; // Direta eletronica sem emissao - simples

            //informações do banco
            var contaCedente = new ContaBancaria(DbRotinas.FormatarText(cedente.Agencia, 4), cedente.AgenciaDigito, cedente.Conta, cedente.ContaDigito);
            cedenteInfo.ContaBancaria = contaCedente;

            return cedenteInfo;

        }


        public int GeraFluxoPagamento()
        {
            Random number = new Random();
            return number.Next(99999999);
        }

        [HttpPost]
        public JsonResult ListDataServer(DTParameters param)
        {
            try
            {
                var dtsource = ListaDados();
                var result = WebRotinas.DTRotinas.DTResult(param, dtsource);

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        public IQueryable<object> ListaDados()
        {
            var Dados = _repository.All();
            IQueryable<object> data = null;

            data = Dados.ToList()
                        .Select(x => new
                        {
                            Cod = x.Cod,
                            x.Numero,
                            x.NomeBanco,
                            x.CodCliente,
                            x.NossoNumero,
                            x.NossoNumeroAtual,
                            x.Lote,
                            x.LoteAtual
                        }).AsQueryable();
            return data;
        }

        private bool verificaSaldoCliente(int codCliente)
        {

            Cliente cliente = _dbCliente.FindById(codCliente);
            var contratos = _dbContratoCliente.All().Where(x => x.CodCliente == cliente.Cod).ToList();
            List<ContratoParcela> parcelas = new List<ContratoParcela>();
            foreach (var c in contratos)
            {
                var p = _ContratoParcela.All().Where(x => x.CodContrato == c.CodContrato && !x.AcordoContratoParcelas.Any(ac => !ac.DataExcluido.HasValue)).ToList();
                foreach (var i in p.Where(x => x.DataPagamento == null))
                {
                    parcelas.Add(i);
                }
            }

            //VERIFICA SE O CLIENTE TEM ALGUMA PARCELA COM A DATA DE PAGAMENTO NULL
            if (parcelas.Count == 0)
            {
                return true;
            }
            else
            {

                return false;
            }
        }



    }
}