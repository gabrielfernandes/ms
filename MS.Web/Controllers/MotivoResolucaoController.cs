﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class MotivoResolucaoController : BasicController<TabelaResolucao>
    {
        public override ActionResult Cadastro(TabelaResolucao dados)
        {
            dados.Resolucao = DbRotinas.Criptografar(dados.Resolucao);
            return base.Cadastro(dados);
        }


    }
}