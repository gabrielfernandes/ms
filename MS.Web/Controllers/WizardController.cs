﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Text;

namespace Cobranca.Web.Controllers
{
    public class WizardController : Controller
    {
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();
        public Repository<TabelaEmpresa> _dbEmpresa = new Repository<TabelaEmpresa>();
        public Repository<TabelaBloco> _dbBloco = new Repository<TabelaBloco>();
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<TabelaUnidade> _dbUnidade = new Repository<TabelaUnidade>();
        public Repository<TabelaVenda> _dbTabelaVenda = new Repository<TabelaVenda>();
        public Repository<Proposta> _dbProposta = new Repository<Proposta>();
        public Repository<PropostaProspect> _dbPropostaProspect = new Repository<PropostaProspect>();
        public Repository<PropostaValore> _dbPropostaValore = new Repository<PropostaValore>();
        public Repository<ProspectFGT> _dbProspectFGTS = new Repository<ProspectFGT>();
        public Repository<TabelaBancoTipoOperacao> _dbBancoTipoOperacao = new Repository<TabelaBancoTipoOperacao>();
        public Repository<TabelaCheckList> _dbCheckList = new Repository<TabelaCheckList>();
        public Repository<TabelaCheckList> _dbTabelaCheckList = new Repository<TabelaCheckList>();

        public Repository<PropostaDocumento> _dbPropostaDocumento = new Repository<PropostaDocumento>();
        public Repository<PropostaDocumentoChecklist> _dbPropostaDocumentoChecklist = new Repository<PropostaDocumentoChecklist>();


        [HttpGet]
        public ActionResult Proposta(int codUnidade = 0, int cdProposta = 0)
        {
            ViewBag.CPF = "";
            var dadosProspect = _dbPropostaProspect.FindAll(x => x.CodProposta == cdProposta);
            if (dadosProspect.Count() > 0)
            {
                ViewBag.CPF = dadosProspect.FirstOrDefault().CPF;
            }
            Contrato dados = new Db.Models.Contrato();
            List<TabelaConstrutora> listaConstrutora = new List<TabelaConstrutora>();
            listaConstrutora = _dbConstrutora.All().Take(50).OrderBy(x => x.NomeConstrutora).ToList();
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            if (codUnidade > 0)
            {
                var unidade = _dbUnidade.FindById(codUnidade);
                ViewBag.CodUnidade = unidade.Cod;
                ViewBag.CodBloco = unidade.TabelaBloco.Cod;
                ViewBag.CodEmpreendimento = unidade.TabelaBloco.CodEmpreend;
            }

            List<TabelaEmpreendimento> listaEmpreendimento = new List<TabelaEmpreendimento>();
            listaEmpreendimento = _dbEmpreendimento.All().Take(50).OrderBy(x => x.NomeEmpreendimento).ToList();
            listaEmpreendimento.Insert(0, new TabelaEmpreendimento());
            ViewBag.ListaEmpreendimento = from p in listaEmpreendimento select new DictionaryEntry(p.NomeEmpreendimento, p.Cod);

            List<TabelaEmpresa> listaEmpresa = new List<TabelaEmpresa>();
            listaEmpresa = _dbEmpresa.All().Take(50).OrderBy(x => x.Empresa).ToList();
            listaEmpresa.Insert(0, new TabelaEmpresa());
            ViewBag.ListaEmpresa = from p in listaEmpresa select new DictionaryEntry(p.Empresa, p.Cod);


            return View(dados);
        }
        [HttpGet]
        public ActionResult InformacoesCadastrais(int id = 0)
        {
            var dados = _dbProposta.FindById(id);
            return View(dados);
        }

        [HttpPost]
        public JsonResult PesquisarBlocoPorEmpreendimento(int CodEmpreendimento = 0)
        {
            var lista = _dbBloco.All().Where(x => x.CodEmpreend == CodEmpreendimento).ToList();
            var dados = from p in lista select new { Cod = p.Cod, Text = p.NomeBloco };
            return Json(dados);
        }

        [HttpPost]
        public JsonResult ObterDadosUnidade(int CodUnidade = 0)
        {
            var dados = _dbUnidade.FindById(CodUnidade);
            var listaTabelasVendas = _dbTabelaVenda.All().Where(x => x.TabelaVendaEmpreendimentoes.Any(t => t.CodEmpreendimento == dados.TabelaBloco.CodEmpreend)).ToList();
            var unidade = new
            {
                CodUnidade = dados.Cod,
                Unidade = dados.NumeroUnidade,
                Valor = dados.Valor,
                ValorFormatada = string.Format("{0:n2}", dados.Valor),
                Status = dados.Status,
                ListaTabelaVenada = from p in listaTabelasVendas select new { Cod = p.Cod, Text = p.Nome },
                //ValorMaximoFinanciamento = (dados.Valor * (dados.TabelaBloco.TabelaEmpreendimento.PercentualFinanciamento / 100)),
                //ValorMaximoFinanciamentoFormatado = string.Format("{0:n2}", (dados.Valor * (dados.TabelaBloco.TabelaEmpreendimento.PercentualFinanciamento / 100)))
            };

            return Json(unidade);
        }

        [HttpPost]
        public JsonResult ObterFluxoTabelaVenda(int CodTabelaVenda = 0, int CodUnidade = 0)
        {
            var dados = _dbTabelaVenda.FindById(CodTabelaVenda);
            var unidade = _dbUnidade.FindById(CodUnidade);


            decimal? valorUnidade = unidade.Valor;
            DateTime? dt = unidade.TabelaBloco.DataHabitese;
            DateTime dtNow = DateTime.Now;
            DateTime dtInicio = dtNow;
            int totalMeses = 0;

            if (dt.HasValue)
            {
                totalMeses = DbRotinas.DiferencaMes(dt.Value, dtNow);
            }




            var listaFluxo = dados.TabelaVendaFluxoes.Where(x => !x.DataExcluido.HasValue).OrderBy(x => x.Ordem);

            var fluxo = from p in listaFluxo
                        select new
                        {
                            Cod = p.Cod,
                            CodTipoParcela = p.CodTipoParcela,
                            TipoParcela = p.TabelaTipoParcela.TipoParcela,
                            PercentualMinimo = p.PercentualMinimo,
                            ValorMinimo = p.Valor,
                            TipoPeriodo = p.TabelaTipoParcela.TipoPeriodo,
                            Ordem = p.Ordem,
                            QtdMes = p.TabelaTipoParcela.QtdMes,
                            Qtd = CalcularQuantidadeParcelas(p, totalMeses),
                            ValorPrevisto = CalcularValorPrevisto(valorUnidade.Value, p, totalMeses),
                            DataInicio = CalcularDataInicioParcela(p, ref dtInicio, totalMeses),
                            DataHabitese = string.Format("{0:dd/MM/yyyy}", dt),
                            DataInicioGeral = string.Format("{0:dd/MM/yyyy}", DateTime.Now),
                            ValorTotalSerie = string.Format("{0:n2}", CalcularValorPrevisto(valorUnidade.Value, p, totalMeses) * CalcularQuantidadeParcelas(p, totalMeses))
                        };


            return Json(fluxo);
        }

        private object CalcularDataInicioParcela(TabelaVendaFluxo p, ref DateTime dtInicio, int totalMeses)
        {

            string dtDescricao = string.Format("{0:MM/yyyy}", dtInicio);

            switch ((Enumeradores.TipoPeriodo)p.TabelaTipoParcela.TipoPeriodo)
            {
                case Enumeradores.TipoPeriodo.Unica:
                    {
                        dtInicio = dtInicio.AddMonths(1);
                        break;
                    }
                case Enumeradores.TipoPeriodo.Mensal:
                    {
                        dtInicio = dtInicio.AddMonths(totalMeses);
                        break;
                    }
                case Enumeradores.TipoPeriodo.Trimestral:
                    {
                        dtInicio = dtInicio.AddMonths(Math.Abs(totalMeses / 3));
                        break;
                    }
                case Enumeradores.TipoPeriodo.Semestral:
                    {
                        dtInicio = dtInicio.AddMonths(Math.Abs(totalMeses / 6));
                        break;
                    }
                case Enumeradores.TipoPeriodo.Anual:
                    {
                        dtInicio = dtInicio.AddMonths(Math.Abs(totalMeses / 12));
                        break;
                    }
                default:
                    break;
            }

            return dtDescricao;

        }

        private int CalcularQuantidadeParcelas(TabelaVendaFluxo p, int totalMeses)
        {
            int quantidade = 1;
            switch ((Enumeradores.TipoPeriodo)p.TabelaTipoParcela.TipoPeriodo)
            {
                case Enumeradores.TipoPeriodo.Unica:
                    {
                        quantidade = 1;
                        break;
                    }
                case Enumeradores.TipoPeriodo.Mensal:
                    {
                        return totalMeses;
                    }
                case Enumeradores.TipoPeriodo.Trimestral:
                    {
                        return Math.Abs(totalMeses / 3);
                    }
                case Enumeradores.TipoPeriodo.Semestral:
                    {
                        return Math.Abs(totalMeses / 6);
                    }
                case Enumeradores.TipoPeriodo.Anual:
                    {
                        return Math.Abs(totalMeses / 12);
                    }
                default:
                    break;
            }
            return quantidade;
        }

        private decimal CalcularValorPrevisto(decimal valorUnidade, TabelaVendaFluxo p, int totalMeses)
        {

            decimal valor = 0;
            decimal vlMinimo = 0;

            if (p.PercentualMinimo > 0)
            {
                vlMinimo = valorUnidade * (p.PercentualMinimo.Value / 100);
            }
            if (p.Valor > 0)
            {
                vlMinimo = p.Valor.Value;
            }

            switch ((Enumeradores.TipoPeriodo)p.TabelaTipoParcela.TipoPeriodo)
            {
                case Enumeradores.TipoPeriodo.Unica:
                    {
                        valor = vlMinimo;
                        break;
                    }
                case Enumeradores.TipoPeriodo.Mensal:
                    {
                        valor = vlMinimo / totalMeses;
                        break;
                    }
                case Enumeradores.TipoPeriodo.Trimestral:
                    {
                        valor = vlMinimo / (totalMeses / 3m);
                        break;
                    }
                case Enumeradores.TipoPeriodo.Semestral:
                    {
                        valor = vlMinimo / (totalMeses / 6m);
                        break;
                    }
                case Enumeradores.TipoPeriodo.Anual:
                    {
                        valor = vlMinimo / (totalMeses / 12m);
                        break;
                    }
                default:
                    break;
            }
            return valor;
        }

        [HttpPost]
        public JsonResult SalvarProposta(PropostaViewModel proposta)
        {
            try
            {
                proposta.valorCalculadoFinanciamento = DbRotinas.ConverterParaDecimal(Request.Form["valorCalculadoFinanciamento"]);
                proposta.ValorEntradaDireta = DbRotinas.ConverterParaDecimal(Request.Form["ValorEntradaDireta"]);
                proposta.ValorFGTS = DbRotinas.ConverterParaDecimal(Request.Form["ValorFGTS"]);
                proposta.ValorFinanciamentoTotal = DbRotinas.ConverterParaDecimal(Request.Form["ValorFinanciamentoTotal"]);
                proposta.ValorMaximoFinanciamento = DbRotinas.ConverterParaDecimal(Request.Form["ValorMaximoFinanciamento"]);
                proposta.ValorMaximoRepasse = DbRotinas.ConverterParaDecimal(Request.Form["ValorMaximoRepasse"]);
                proposta.ValorRendaFamiliar = DbRotinas.ConverterParaDecimal(Request.Form["ValorRendaFamiliar"]);
                proposta.ValorUnidade = DbRotinas.ConverterParaDecimal(Request.Form["ValorUnidade"]);


                proposta.ValorFalta = DbRotinas.ConverterParaDecimal(Request.Form["valorFalta"]);
                proposta.ValorTotalGeral = DbRotinas.ConverterParaDecimal(Request.Form["valorTotalGerar"]);
                //proposta.ValorCalculadoFinanciamento = DbRotinas.ConverterParaDecimal(Request.Form["valorCalculadoFinanciamento"]);
                proposta.DataInicioRepasse = DbRotinas.ConverterParaDatetimeOuNull(Request.Form["dataInicioRepasse"]);
                proposta.ValorTotalRepasse = DbRotinas.ConverterParaDecimal(Request.Form["valorTotalRepasse"]);
                proposta.ValorTotalSubsidio = DbRotinas.ConverterParaDecimal(Request.Form["valorTotalSubsidio"]);


                Proposta item = new Db.Models.Proposta();
                item.Cod = proposta.CodProposta;
                //item.Cod = 0;
                item.Status = (short)Enumeradores.StatusProposta.PropostaCadastrada;
                item.ValorUnidade = proposta.ValorUnidade;
                item.CodUnidade = proposta.CodUnidade;
                item.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                item.CodTabelaVenda = proposta.CodTabelaVenda;
                item.DataProposta = DateTime.Now;
                item.ValorFGTS = proposta.ValorFGTS;
                item.ValorRendaFamiliar = proposta.ValorRendaFamiliar;
                item.ValorUnidade = proposta.ValorUnidade;
                item.ValorMaximoRepasse = proposta.ValorMaximoRepasse;
                item.ValorMaximoFinanciamento = proposta.ValorMaximoFinanciamento;
                item.ValorFinanciamentoTotal = proposta.ValorFinanciamentoTotal;
                item.PrazoFinanciamento = 360;

                item.ValorFalta = proposta.ValorFalta;
                item.ValorTotalGeral = proposta.ValorTotalGeral;
                item.DataInicioRepasse = proposta.DataInicioRepasse;
                item.ValorTotalRepasse = proposta.ValorTotalRepasse;
                item.ValorTotalSubsidio = proposta.ValorTotalSubsidio;


                PropostaProspect cliente = new PropostaProspect();
                cliente.Cod = proposta.CodProspect;
                cliente.CodProposta = proposta.CodProposta;
                cliente.Nome = proposta.NomeCliente;
                cliente.CPF = proposta.CPF;
                cliente.DataNascimento = proposta.DataNascimentoCliente;
                cliente.Email = proposta.Email;
                cliente.TelefoneResidencial = proposta.TelefoneCelular;
                cliente.Celular = proposta.TelefoneCelular;
                cliente.TipoComprador = (short)Enumeradores.CompradorTipo.Comprador;
                cliente.Ordem = 1;
                cliente.DataCadastro = DateTime.Now;

                PropostaHistorico historico = new PropostaHistorico();
                historico.CodSintese = 122;
                historico.CodProposta = proposta.CodProposta;
                historico.DataCadastro = DateTime.Now;
                historico.Observacoes = "Proposta cadastrada.";
                historico.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;

                item.PropostaHistoricoes.Add(historico);
                item.PropostaProspects.Add(cliente);

                var entity = _dbProposta.CreateOrUpdate(item);

                GravarFluxo(Request, entity.Cod);

                PropostaStatusDb objPropostaStatusDb = new PropostaStatusDb();

                objPropostaStatusDb.AlterarStatusProposta(item.Cod, Enumeradores.StatusProposta.PropostaCadastrada, WebRotinas.ObterUsuarioLogado().Cod, "");

                WebRotinas.CookieObjetoGravar("FiltroCodProposta", entity.Cod);

                return Json(new { OK = true, Dados = entity.Cod });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }


        }

        private List<PropostaValore> GravarFluxo(HttpRequestBase request, int CodProposta)
        {
            List<PropostaValore> lista = new List<PropostaValore>();


            var arr = request.Form.GetValues("CodTipoParcela");
            var qtdParcela = request.Form.GetValues("qtdParcela");
            var valorParcela = request.Form.GetValues("valorParcela");
            var dataInicio = request.Form.GetValues("dataInicio");
            var valorTotal = request.Form.GetValues("valorTotal");

            for (int i = 0; i < arr.Length; i++)
            {
                int id = DbRotinas.ConverterParaInt(arr[i]);
                if (id > 0)
                {
                    lista.Add(new PropostaValore()
                    {
                        CodTipoParcela = DbRotinas.ConverterParaInt(arr[i]),
                        CodProposta = CodProposta,
                        DataInicio = DbRotinas.ConverterParaDatetimeOuNull(dataInicio[i]),
                        Quantidade = DbRotinas.ConverterParaInt(qtdParcela[i]),
                        ValorParcela = DbRotinas.ConverterParaDecimal(valorParcela[i]),
                        ValorTotal = DbRotinas.ConverterParaInt(qtdParcela[i]) * DbRotinas.ConverterParaDecimal(valorParcela[i]),
                        DataCadastro = DateTime.Now

                    });
                }

            }
            foreach (var item in lista)
            {
                _dbPropostaValore.CreateOrUpdate(item);
            }

            return lista;
        }

        [HttpPost, ValidateInput(false)]
        public JsonResult SalvarCliente(PropostaProspect cliente)
        {
            try
            {
                if (cliente.CodProposta.HasValue)
                {
                    var proposta = _dbProposta.FindById(cliente.CodProposta.Value);
                    var entity = _dbPropostaProspect.CreateOrUpdate(cliente);

                    //Contrato contrato = new Contrato();
                    //contrato = Activator.CreateInstance<Contrato>();
                    //contrato.CodProposta = cliente.CodProposta.Value;
                    //contrato.DataCadastro = System.DateTime.Today;
                    //contrato.CodUnidade = cliente.Proposta.CodUnidade;
                    //contrato.ValorVenda = cliente.Proposta.ValorUnidade;
                    //_dbContrato.CreateOrUpdate(contrato);
                }

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {

                return Json(new { OK = false, Mensagem = ex.Message });
            }


        }

        [HttpPost]
        public JsonResult CalcularPotencialFinanciamento(decimal ValorRendaFamiliar, DateTime DataNascimentoCliente, int CodUnidade, int nCompradores)
        {
            var unidade = _dbUnidade.FindById(CodUnidade);

            nCompradores = nCompradores == 0 ? 1 : nCompradores;//default 1

            var tpOperacao = _dbBancoTipoOperacao.FindOne(c => !c.DataExcluido.HasValue && c.CodBanco == unidade.TabelaBloco.TabelaEmpreendimento.CodBancoFinanciador);
            int Prazo = DbRotinas.ConverterParaInt(tpOperacao.PrazoMaximo);
            decimal PercentualFinanciamento = DbRotinas.ConverterParaDecimal(tpOperacao.PercentualAvaliacao);
            decimal TaxaJuros = DbRotinas.ConverterParaDecimal(tpOperacao.TaxaJuros);
            int idadeMaxima = DbRotinas.ConverterParaInt(tpOperacao.IdadeMaxima);
            decimal percentualComprometimentoRenda = DbRotinas.ConverterParaDecimal(tpOperacao.PercentualComprometimentoRenda);
            decimal percentualFinanciamentoRepasse = 96;
            decimal valor = CalculoImob.CalcularCapacidadeFinanciamento(ValorRendaFamiliar, DataNascimentoCliente, idadeMaxima, Prazo, TaxaJuros, percentualComprometimentoRenda, percentualFinanciamentoRepasse);
            decimal valorMaximoParcela = CalculoImob.CalcularPercentualComprometimentoRenda(ValorRendaFamiliar, percentualComprometimentoRenda);

            var valorSubsidio = CalculoImob.CalcularSubsidioCAIXA(nCompradores, ValorRendaFamiliar, 26000);

            decimal valorMaximoFinanciamentoUnidade = DbRotinas.ConverterParaDecimal(unidade.ValorAvaliacao) * (PercentualFinanciamento / 100m);
            if (valor > valorMaximoFinanciamentoUnidade)
            {
                valor = valorMaximoFinanciamentoUnidade;
            }

            return Json(new
            {
                capacidadeFinanciamento = valor,
                valorMaximoFinanciamentoUnidade = valorMaximoFinanciamentoUnidade,
                valorMaximoParcela = valorMaximoParcela,
                valorSubsidio = valorSubsidio
            });
        }
        [HttpPost]
        public JsonResult LocalizarPorCPF(string CPF)
        {
            var prospect = _dbPropostaProspect.All().Where(x => x.CPF == CPF);
            if (prospect.Count() > 0)
            {
                var cli = prospect.ToList().LastOrDefault();
                var dados = new
                {
                    CodProposta = cli.CodProposta,
                    CodProspect = cli.Cod,
                    NomeCliente = cli.Nome,
                    DataNascimentoCliente = string.Format("{0:dd/MM/yyyy}", cli.DataNascimento),
                    TelefoneFixo = cli.TelefoneResidencial,
                    TelefoneCelular = cli.Celular,
                    Email = cli.Email
                };

                return Json(new { OK = true, Dados = dados });
            }
            return Json(new { OK = false, Dados = "" });
        }
        [HttpPost]
        public JsonResult EnivarProposta(int CodProposta, string Observacao)
        {
            var proposta = _dbProposta.FindById(CodProposta);
            if (proposta != null)
            {
                PropostaStatusDb objPropostaStatusDb = new PropostaStatusDb();
                objPropostaStatusDb.AlterarStatusProposta(proposta.Cod, Enumeradores.StatusProposta.Solicitada, WebRotinas.ObterUsuarioLogado().Cod, Observacao);

                return Json(new { OK = true, Dados = "" });
            }
            return Json(new { OK = false, Dados = "" });
        }

        [HttpPost]
        public JsonResult ReprovarProposta(int CodProposta, string obs)
        {
            var proposta = _dbProposta.FindById(CodProposta);
            if (proposta != null)
            {
                PropostaStatusDb objPropostaStatusDb = new PropostaStatusDb();
                objPropostaStatusDb.AlterarStatusProposta(proposta.Cod, Enumeradores.StatusProposta.Negada, WebRotinas.ObterUsuarioLogado().Cod, obs);

                return Json(new { OK = true, Dados = "" });
            }
            return Json(new { OK = false, Dados = "" });
        }
        [HttpPost]
        public JsonResult EnviarParaAnalise(int CodProposta)
        {

            var usuariologado = WebRotinas.ObterUsuarioLogado();

            if (usuariologado.PerfilTipo == Enumeradores.PerfilTipo.SupervisorVenda && (usuariologado.SistemaCobranca == false))
            {
                var proposta = _dbProposta.FindById(CodProposta);
                if (proposta != null)
                {
                    PropostaStatusDb objPropostaStatusDb = new PropostaStatusDb();
                    objPropostaStatusDb.AlterarStatusProposta(proposta.Cod, Enumeradores.StatusProposta.EnviadoParaAnaliseCredito, WebRotinas.ObterUsuarioLogado().Cod, "");

                    return Json(new { OK = true, Dados = "" });
                }
            }
            else
            {
                return Json(new { OK = false, Mensagem = "Permissão negada" });
            }

            return Json(new { OK = false, Dados = "" });
        }


        [HttpPost]
        public JsonResult AnalisarCritica(int CodProposta)
        {

            var usuariologado = WebRotinas.ObterUsuarioLogado();
            List<TabelaCheckList> listaCheckListProspect = new List<TabelaCheckList>();
            List<PropostaDocumentoChecklist> listaPropostaDocumentoChecklist = new List<PropostaDocumentoChecklist>();
            List<String[]> criticas = new List<String[]>();
            StringBuilder validacao = new StringBuilder();

            var propostaProspect = _dbPropostaProspect.All().Where(x => x.CodProposta == CodProposta).ToList();

            foreach (var p in propostaProspect)
            {
                var listaPropostaProspect = _dbPropostaProspect.All().Where(x => x.CodConjuge == p.Cod).ToList();

                if (p.EstadoCivil == (short)Cobranca.Db.Models.Enumeradores.EstadoCivil.Casado)
                {
                    validacao.Append("<div style='border-bottom: 1px solid #fff'><i class='fa fa-exclamation-circle'></i> INFORMAÇÕES CADASTRAIS</div>");
                    validacao.Append(p.Nome); // PROSPECT
                    validacao.Append("<small>");
                    validacao.Append("<br/>");
                    validacao.Append("<br/>");
                    if (listaPropostaProspect.Count() == 0)
                    {
                        validacao.Append("- Conjuge não cadastrado!"); // MENSAGEM
                    }
                    validacao.Append("</small>");

                    criticas.Add(new String[] { validacao.ToString() });
                    validacao.Clear();
                }

            

            }

            foreach (var p in propostaProspect)
            {
                
                if (p.Proposta.ValorFGTS > 0 && !_dbProspectFGTS.All().Any(x => x.CodProspect == p.Cod) && p.TipoComprador == (short)Enumeradores.CompradorTipo.Comprador && p.Ordem == 1 ) //VERIFICA SE FGTS DO PROMIERO COMPRADOR FOR DECLARADO NA PROPOSTA VERIFICA SE JA FOI CADASTRADO
                {
                    var listaPropostaProspect = _dbPropostaProspect.All().Where(x => x.CodConjuge == p.Cod).ToList();

                    validacao.Append("<div style='border-bottom: 1px solid #fff'><i class='fa fa-exclamation-circle'></i> FGTS</div>");
                    validacao.Append(p.Nome); // PROSPECT
                    validacao.Append("<small>");
                    validacao.Append("<br/>");
                    validacao.Append("<br/>");
                    validacao.Append("- Valor FGTS não cadastrado!"); // MENSAGEM
                    validacao.Append("</small>");

                    criticas.Add(new String[] { validacao.ToString() });
                    validacao.Clear();
                }
               
            }


            listaCheckListProspect = _dbTabelaCheckList.All().ToList();

            foreach (var p in propostaProspect)
            {
                listaPropostaDocumentoChecklist = _dbPropostaDocumentoChecklist.All().Where(x => x.PropostaDocumento.CodProspect == p.Cod && (x.StatusAnalise == (short)Cobranca.Db.Models.Enumeradores.DocumentoStatus.Aprovado || x.StatusAnalise == null)).ToList();

                listaCheckListProspect = listaCheckListProspect.Where(x => x.CodCheckListNome == p.Proposta.TabelaUnidade.TabelaBloco.TabelaEmpreendimento.CodCheckListNome
                && (x.CodBanco == p.Proposta.TabelaUnidade.TabelaBloco.TabelaEmpreendimento.CodBancoFinanciador)
                && (x.EstadoCivil == p.EstadoCivil || x.EstadoCivil == 0)
                && (x.CategoriaProfissional == p.CategoriaProfissional || x.CategoriaProfissional == 0)).ToList();

                validacao.Append("<div style='border-bottom: 1px solid #fff'><i class='fa fa-exclamation-circle'></i> DOCUMENTOS</div>");
                 validacao.Append(p.Nome); // PROSPECT
                validacao.Append("<small>");
                validacao.Append("<br/>");
                validacao.Append("<br/>");
                for (int i = 0; i < listaCheckListProspect.Count; i++)
                {
                    var c = listaCheckListProspect[i];
                    if (c.Obrigatorio == true && !listaPropostaDocumentoChecklist.Any(x => x.CodChecklist == c.Cod))
                    {
                        validacao.Append("- " + c.TabelaTipoDocumento.TipoDocumento); // MENSAGEM
                        validacao.Append("<br/>");
                    }
                }
                validacao.Append("</small>");
                criticas.Add(new String[] { validacao.ToString() });
                validacao.Clear();
            }

            return Json(new { OK = true, listaCriticas = criticas });

        }
        //
    }
}