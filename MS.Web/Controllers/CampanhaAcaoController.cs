﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class CampanhaAcaoController : BasicController<TabelaCampanhaAcao>
    {

        public Repository<TabelaCampanha> _dbCampanha = new Repository<TabelaCampanha>();
        public Repository<TabelaAcaoCobranca> _dbAcao= new Repository<TabelaAcaoCobranca>();

        public override void carregarCustomViewBags()
        {
            var listaCampanha = _dbCampanha.All();
            var listaAcao = _dbAcao.All();

            ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);           
            ViewBag.ListaAcao = from p in listaAcao.OrderBy(x => x.AcaoCobranca).ToList() select new DictionaryEntry(p.AcaoCobranca, p.Cod);
            base.carregarCustomViewBags();
        }


    }
}