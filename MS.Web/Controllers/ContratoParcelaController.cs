﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using Recebiveis.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Recebiveis.Web.Controllers
{
    public class ContratoParcelaController : Controller
    {
        public Dados _qry = new Dados();
        public const string _filtroCodContrato = "FiltroCodContrato";
        public const string _filtroCodCliente = "FiltroCodCliente";

        Repository<Contrato> _dbContrato = new Repository<Contrato>();
        Repository<ContratoCliente> _dbContratoCliente = new Repository<ContratoCliente>();
        Repository<ContratoParcela> _dbContratoParcela = new Repository<ContratoParcela>();


        [HttpPost]
        public ActionResult DadosParcela(int contratoId = 0, int tipo = 0, int CodSinteseContrato = 0, int CodSinteseParcela = 0)
        {
            var cdCliente = 0;
            if (contratoId == 0) { contratoId = WebRotinas.CookieObjetoLer<int>(_filtroCodContrato); }
            if (cdCliente == 0) { cdCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }

            //TRAZ TODAS PARCELAS DO CLIENTE
            var parcelaCliente = _qry.ObterParcelaPorCliente(cdCliente, CodSinteseContrato, CodSinteseParcela);

            //FILTRA POR CodContrato
            if (contratoId > 0)
            {
                parcelaCliente = parcelaCliente.Where(x => x.CodContrato == contratoId).ToList();
            }
            else
            {
                var usuarioLogado = WebRotinas.ObterUsuarioLogado();
                if (usuarioLogado.CodRegional > 0)
                {
                    List<Contrato> contratoCliente = _dbContratoCliente.FindAll(x => x.CodCliente == cdCliente).Select(c => c.Contrato).ToList();
                    List<int> idsContratos = (from p in contratoCliente
                                              where p.TabelaRegionalProduto.CodRegional == usuarioLogado.CodRegional
                                              select p.Cod).ToList();
                    parcelaCliente = parcelaCliente.Where(x => idsContratos.Contains((int)x.CodContrato)).ToList();
                }
            }

            List<ContratoParcela> parcelas = new List<ContratoParcela>();
            foreach (var parcelaItem in parcelaCliente) { parcelas.Add(_dbContratoParcela.FindById(parcelaItem.Cod)); }

            if (tipo == (int)Enumeradores.PesquisaTipoParcela.EmAtraso) { parcelas = parcelas.Where(x => !x.AcordoContratoParcelas.Any(a => !a.DataExcluido.HasValue) && !x.DataFechamento.HasValue && x.DataVencimento.Date < DateTime.Now.Date).ToList(); }
            if (tipo == (int)Enumeradores.PesquisaTipoParcela.aVencer) { parcelas = parcelas.Where(x => !x.AcordoContratoParcelas.Any(a => !a.DataExcluido.HasValue) && !x.DataFechamento.HasValue && x.DataVencimento.Date > DateTime.Now.Date).ToList(); }
            if (tipo == (int)Enumeradores.PesquisaTipoParcela.Pago) { parcelas = parcelas.Where(x => !x.AcordoContratoParcelas.Any(a => !a.DataExcluido.HasValue) && (x.DataPagamento.HasValue || x.DataFechamento.HasValue) ).ToList(); }

            var result = parcelas.Select(t => new ContratoParcelaViewModel
            {
                CodParcela = t.Cod,
                NumeroContrato = Cobranca.Db.Rotinas.DbRotinas.Descriptografar(t.Contrato.NumeroContrato),
                DataVencimento = t.DataVencimento,
                QtdDiasAtraso = DateTime.Now.Subtract(t.DataVencimento).Days.ToString(),
                strDataVencimento = string.Format("{0:dd/MM/yyyy}", t.DataVencimento),
                FluxoPagamento = t.FluxoPagamento,
                Natureza = t.CodNatureza.HasValue ? t.TabelaNatureza.Natureza : "",
                ValorParcela = string.Format("{0:0,0.00}", t.ValorParcela),
                ValorAtualizado = string.Format("{0:0,0.00}", t.ValorAtualizado),
                Juros = t.Juros,
                Multa = t.Multa,
                NumeroParcela = t.NumeroParcela,
                TipoParcela = t.CodAcordo.HasValue ? "Parcela Acordo" : "Parcela",
                PossuiLinhaDigitavel = t.LinhaDigitavel != null ? true : false,
                NotaFiscal = t.NumeroNotaFiscal,
                TipoFatura = t.TipoFatura


            });

            return Json(new { OK = true, Data = result.ToList() }, JsonRequestBehavior.AllowGet);
        }


    }
}
