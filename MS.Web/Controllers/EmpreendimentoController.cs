﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Cobranca.Web.Controllers
{
    public class EmpreendimentoController : BasicController<TabelaEmpreendimento>
    {

        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaBloco> _dbBloco = new Repository<TabelaBloco>();
        public Repository<TabelaEndereco> _dbEndereco = new Repository<TabelaEndereco>();
        public Repository<Usuario> _dbUsuario = new Repository<Usuario>();
        public Repository<Proposta> _dbProposta = new Repository<Proposta>();
        public Repository<TabelaBanco> _dbBanco = new Repository<TabelaBanco>();


        public override void carregarCustomViewBags()
        {
            //var teste = _dbConstrutora.All().ToList().OrderBy(x=>x.NomeConstrutora);
            //ViewBag.Teste = new SelectList(teste, "Cod", "NomeConstrutora");

            var listaContrutoras = _dbConstrutora.All();
            var listaUsuario = _dbUsuario.All();
            var listaBanco = _dbBanco.All();
            ViewBag.ListaConstrutora = from p in listaContrutoras.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);
            ViewBag.ListaUsuarioDonoCarteira = from p in listaUsuario.Where(x => x.DonoDeCarteira == true).ToList() select new DictionaryEntry(p.Nome, p.Cod);
            ViewBag.ListaUsuarioDonoNegocio = from p in listaUsuario.Where(x => x.DonoDeNegocio == true).ToList() select new DictionaryEntry(p.Nome, p.Cod);
            ViewBag.ListaUsuarioResponsavel = from p in listaUsuario.Where(x => x.Responsavel == true).ToList() select new DictionaryEntry(p.Nome, p.Cod);
            ViewBag.ListaBanco = from p in listaBanco.OrderBy(x => x.NomeBanco).ToList() select new DictionaryEntry(p.NomeBanco, p.Cod);
            base.carregarCustomViewBags();
        }
        public override void Ok(TabelaEmpreendimento dados, HttpRequestBase Request)
        {
            base.Ok(dados, Request);
        }

        public override void validacaoCustom(TabelaEmpreendimento entity)
        {
            if (entity.CodEndereco > 0)
            {
                _dbEndereco.CreateOrUpdate(entity.TabelaEndereco);
            }
            base.validacaoCustom(entity);
        }

        [HttpPost]
        public JsonResult Pesquisar(int id = 0)
        {
            var lista = _repository.All().Where(x => x.CodConstrutora == id).ToList();
            var dados = from p in lista select new { Cod = p.Cod, Text = p.NomeEmpreendimento };
            return Json(dados);
        }

        [HttpGet]
        public JsonResult PesquisarEmpreendimentos(string q)
        {
            var lista = _repository.All().Where(x => x.NomeEmpreendimento.Contains(q)).Take(10).ToList();

            return Json(new {
                items = from p in lista select new { id = p.Cod, text= p.NomeEmpreendimento }
            },JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SelecionaEmpreendimento(int Cod = 0)
        {
            try
            {
                List<String[]> Empreendimento = new List<String[]>();
                var cliente = _dbProposta.FindAll(x => x.Cod == Cod).FirstOrDefault();
                if (cliente != null)
                {
                    Empreendimento.Add(new String[] { Convert.ToString(cliente.TabelaUnidade.TabelaBloco.TabelaEmpreendimento.NomeEmpreendimento) });
                    Empreendimento.Add(new String[] { Convert.ToString(cliente.TabelaUnidade.TabelaBloco.NomeBloco) });
                    Empreendimento.Add(new String[] { Convert.ToString(cliente.TabelaUnidade.NumeroUnidade) });

                }

                return Json(new { OK = true, InfoCliente = Empreendimento });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ListarBlocoPorEmpreendimento(int CodEmpreendimento)
        {
            var lista = _dbBloco.All().Where(x => x.CodEmpreend == CodEmpreendimento).ToList();
            lista.Insert(0, new TabelaBloco());
            var dados = from p in lista select new { Cod = p.Cod, Text = p.NomeBloco };
            return Json(dados);
        }
    }
}