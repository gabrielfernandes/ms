﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas.BoletoGerar;
using BoletoNet;
using System.Configuration;
using System.IO;
using Cobranca.Rotinas.BoletoGerar;
using Cobranca.Rotinas.BNet;
using System.Text;
using Cobranca.Infra.BoletoContext.Repositories;
using Cobranca.Domain.BoletoContext.Handlers;
using Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs;
using Cobranca.Domain.BoletoContext.Enums;
using JunixValidator;
using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Shared.Commands;
using Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Outputs;
using Cobranca.Db.Models.Pesquisa;

namespace Cobranca.Web.Controllers
{
    public class ContratoParcelaBoletoController : BasicController<ContratoParcelaBoleto>
    {
        Repository<TabelaTipoCobranca> _dbTipoCobranca = new Repository<TabelaTipoCobranca>();
        public Repository<ContratoParcelaBoleto> _dbRepository = new Repository<ContratoParcelaBoleto>();
        public Repository<ContratoParcela> _dbParcela = new Repository<ContratoParcela>();
        public Repository<TabelaBancoPortador> _dbBancoPortador = new Repository<TabelaBancoPortador>();
        public Repository<TabelaParametroSintese> _parametroSintese = new Repository<TabelaParametroSintese>();
        Repository<ContratoParcelaBoleto> _dbBoleto = new Repository<ContratoParcelaBoleto>();
        Repository<ContratoParcelaBoletoHistorico> _dbBoletoHistorico = new Repository<ContratoParcelaBoletoHistorico>();
        Repository<ContratoCliente> _dbContratoCliente = new Repository<ContratoCliente>();
        Repository<Cliente> _dbCliente = new Repository<Cliente>();
        Repository<TabelaSintese> _dbSintese = new Repository<TabelaSintese>();
        Repository<TabelaModeloAcao> _dbTabelaModeloAcao = new Repository<TabelaModeloAcao>();
        Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        Repository<TabelaRegionalGrupo> _dbRegionalGrupo = new Repository<TabelaRegionalGrupo>();
        Repository<Cobranca.Db.Models.Contrato> _dbContrato = new Repository<Cobranca.Db.Models.Contrato>();
        private Repository<UsuarioRegional> _dbUsuarioRegional = new Repository<UsuarioRegional>();
        public Dados _qry = new Dados();

        private readonly BoletoRepository _repBoleto;
        private readonly BoletoHandler _boletoHandler;
        private readonly BancoPortadorRepository _repBancoPortador;
        private readonly ClienteRepository _repCliente;
        private readonly ContratoRepository _repContrato;

        public ContratoParcelaBoletoController(
            BoletoRepository repBoleto,
            BoletoHandler boletoHandler,
            BancoPortadorRepository portadorRepository,
            ClienteRepository repCliente,
            ContratoRepository repContrato)
        {
            _boletoHandler = boletoHandler;
            _repBoleto = repBoleto;
            _repBancoPortador = portadorRepository;
            _repCliente = repCliente;
            _repContrato = repContrato;
        }

        [HttpGet]
        public ActionResult FormSolicitacao(string p = null)
        {
            carregarCustomViewBags();

            string[] idParcelas = p.Split(',');
            List<ContratoParcelaBoleto> boletos = new List<ContratoParcelaBoleto>();
            List<ClienteHeaderViewModel> ListHeader = new List<ClienteHeaderViewModel>();
            foreach (var item in idParcelas)
            {
                var id = DbRotinas.ConverterParaInt(item);
                var dados = _dbParcela.FindById(id);
                var paramBanco = _repBancoPortador.GetByParcelaId(id);
                boletos.Add(new ContratoParcelaBoleto
                {
                    CodParcela = dados.Cod,
                    DataVencimento = dados.DataVencimento,
                    ValorDocumento = dados.ValorAberto == dados.ValorParcela ? (dados.ValorAberto - DbRotinas.ConverterParaDecimal(dados.Imposto)) : dados.ValorAberto,
                    NumeroDocumento = dados.FluxoPagamento,
                    JurosAoMesPorcentagem = paramBanco.JurosAoMesPorcentagem,
                    MultaAtrasoPorcentagem = paramBanco.MultaAtraso,
                    DataCadastro = DateTime.Now
                });
                ListHeader.Add(new ClienteHeaderViewModel
                {
                    CodCliente = DbRotinas.Descriptografar(dados.CodCliente),
                    Banco = paramBanco.NomeBanco,
                    DataVencimento = dados.DataVencimento,
                    ValorAberto = dados.ValorAberto,
                    NossoNumero = dados.NossoNumero,
                    NumeroDocumento = dados.FluxoPagamento,
                    NumeroContrato = DbRotinas.Descriptografar(dados.Contrato.NumeroContrato),
                    TipoFatura = dados.TipoFatura,
                    CodigoSinteseCliente = dados.CodHistorico > 0 ? dados.Historico.CodSintese > 0 ? dados.Historico.TabelaSintese.CodigoSinteseCliente : "--" : "--"

                });

            }
            ViewBag.ClienteHeader = ListHeader.ToList();

            return PartialView("_formBoleto", boletos);
        }
        [HttpGet]
        public ActionResult FormModificar(string p = null)
        {
            carregarCustomViewBags();

            string[] idParcelas = p.Split(',');
            List<ContratoParcelaBoleto> boletos = new List<ContratoParcelaBoleto>();
            List<ClienteHeaderViewModel> ListHeader = new List<ClienteHeaderViewModel>();
            foreach (var item in idParcelas)
            {
                var id = DbRotinas.ConverterParaInt(item);
                var dados = _dbParcela.FindById(id);
                var paramBanco = _repBancoPortador.GetByParcelaId(id);
                boletos.Add(new ContratoParcelaBoleto
                {
                    CodParcela = dados.Cod,
                    DataVencimento = dados.DataVencimento,
                    ValorDocumento = (dados.ValorAberto - DbRotinas.ConverterParaDecimal(dados.Imposto)),
                    NumeroDocumento = dados.FluxoPagamento,
                    JurosAoMesPorcentagem = paramBanco.JurosAoMesPorcentagem,
                    MultaAtrasoPorcentagem = paramBanco.MultaAtraso,
                    NossoNumero = dados.NossoNumero,
                    DataCadastro = DateTime.Now
                });
                ListHeader.Add(new ClienteHeaderViewModel
                {
                    CodCliente = DbRotinas.Descriptografar(dados.CodCliente),
                    Banco = paramBanco.NomeBanco,
                    DataVencimento = dados.DataVencimento,
                    ValorAberto = dados.ValorAberto,
                    NossoNumero = dados.NossoNumero,
                    NumeroDocumento = dados.FluxoPagamento,
                    NumeroContrato = DbRotinas.Descriptografar(dados.Contrato.NumeroContrato),
                    TipoFatura = dados.TipoFatura,
                    CodigoSinteseCliente = dados.CodHistorico > 0 ? dados.Historico.CodSintese > 0 ? dados.Historico.TabelaSintese.CodigoSinteseCliente : "--" : "--"

                });
            }
            ViewBag.ClienteHeader = ListHeader.ToList();
            return PartialView("_formBoletoAlterar", boletos);
        }
        [HttpGet]
        public ActionResult FormHistorico(int id = 0)
        {
            var user = WebRotinas.ObterUsuarioLogado();
            var dados = _dbBoletoHistorico.FindById(id);

            var fases = BoletoRotinasUtils.ObterListaFaseBoleto();

            var tipoCobranca = _dbTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.AnalistaBoleto || x.TipoFluxo == (short)Enumeradores.TipoFluxo.SupervisorBoleto || x.TipoFluxo == (short)Enumeradores.TipoFluxo.FilialBoleto).OrderBy(x => x.Cod).ToList();
            ViewBag.TipoCobrancaDe = (from p in tipoCobranca
                                      select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.TipoCobranca), Value = p.Cod }).ToList();


            ViewBag.ListaFase = (from p in fases
                                 select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.Fase), Value = p.Cod }).ToList();
            ((List<DictionaryEntry>)ViewBag.ListaFase).Insert(0, new DictionaryEntry() { Key = "--", Value = "0" });
            var sinteses = BoletoRotinasUtils.ObterListaSinteseBoleto();
            if (dados.CodSintese > 0)
            {
                sinteses = sinteses.Where(x => x.CodFase == dados.TabelaSintese.CodFase).ToList();
            }

            ViewBag.ListaSintese = (from p in sinteses
                                    select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.Sintese), Value = p.Cod }).ToList();
            ((List<DictionaryEntry>)ViewBag.ListaSintese).Insert(0, new DictionaryEntry() { Key = "--", Value = "0" });


            var historicos = _dbBoletoHistorico.All().Where(x => x.CodBoleto == dados.CodBoleto && !x.DataExcluido.HasValue).ToList();
            var Mensagens = new List<string>();

            if (historicos.Count > 0)
            {
                var agrupados = (from p in historicos
                                 group p by new { codboleto = p.CodBoleto, codusuario = p.CodUsuario } into g
                                 select g.Select(x => x).LastOrDefault()
                                ).ToList();

                if (agrupados.Count() > 0)
                {
                    Mensagens = agrupados.OrderByDescending(x => x.DataCadastro)
                                    .Select(x => TratarMensagemHistorico(x))
                                    .ToList();
                }
                if (Mensagens.Count() > 0)
                {
                    dados.Observacao = $"{string.Join("\n\n", Mensagens)}";
                }

            }
            //var Mensagens = (from p in historicos
            //                 group p by new { codBoleto = p.CodBoleto, codUsuario = p.CodUsuario } into g
            //                 select g.Select(x => x).LastOrDefault()
            //                )
            //                .OrderByDescending(x => x.DataCadastro)
            //                .Select(x => TratarMensagemHistorico(x))
            //                .ToList();

            dados.Observacao = $"{string.Join("\n\n", Mensagens)}";
            var header = new ClienteHeaderViewModel();
            var boleto = _repBoleto.FindById((int)dados.CodBoleto);
            header = PreecherDadosHeader(boleto);
            ViewBag.ClienteHeader = header; ;

            return PartialView("_formBoletoHistorico", dados);
        }

        private string TratarMensagemHistorico(ContratoParcelaBoletoHistorico historico)
        {
            var msg = "";
            if (historico != null)
            {
                msg = $"{historico.DataCadastro:dd/MM/yyyy HH:mm}{Environment.NewLine}{historico.Usuario?.Nome?.ToUpper()} : {historico.Observacao}";
                //msg = $"{Environment.NewLine}{historico.Observacao}";
            }
            return msg;
        }



        [HttpPost]
        public ActionResult ComputarValores(DTParameters param = null, ContratoParcelaBoletoViewModel.BoletoViewModelViewModel models = null)
        {
            try
            {
                ContratoParcelaBoletoViewModel Solicitacao = new ContratoParcelaBoletoViewModel();
                Solicitacao.ParcelasBoletos = new List<ContratoParcelaBoletoViewModel.ParcelaBoleto>();
                for (int i = 0; i < models.Cod.Length; i++)
                {
                    Solicitacao.ParcelasBoletos.Add(new ContratoParcelaBoletoViewModel.ParcelaBoleto(models.Cod[i], models.CodParcela[i], models.NumeroDocumento[i], models.DataVencimento[i], models.DataMoratoria[i], models.ValorDocumento[i], models.MultaAtrasoPorcentagem[i], models.JurosAoMesPorcentagem[i], models.ValorDesconto[i]));
                }

                List<ClienteContato> _ClienteContato = new List<ClienteContato>();
                var dataSet = (from p in Solicitacao.ParcelasBoletos
                               select new
                               {
                                   NumeroDocumento = p.NumeroDocumento,
                                   DataVencimento = $"{p.DataVencimento:dd/MM/yyyy}",
                                   DataMoratoria = $"{p.DataMoratoria:dd/MM/yyyy}",
                                   MultaAtrasoPercento = $"{p.MultaAtrasoPercento:n2}",
                                   MultaAtrasoValor = $"{p.MultaAtrasoValor:C}",
                                   JurosAoMesPorcentagem = $"{p.JurosAoMesPorcentagem:n2}",
                                   JurosAoMeValor = $"{p.JurosAoMeValor:C}",
                                   ValorDocumento = $"{p.ValorDocumento:C}",
                                   DiasAtraso = $"{p.QtdDias:D}",
                                   ValorAtualizado = $"{p.ValorAtualizado:C}",
                                   ValorDesconto = $"{p.ValorDesconto:C}"
                               }).AsQueryable();

                var result = WebRotinas.DTRotinas.DTResult(param, dataSet);
                return Json(result, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult SalvarSolicitacao(ContratoParcelaBoletoViewModel.BoletoViewModelViewModel models)
        {
            try
            {
                var justificativa = models.Observacao.First();
                var msgImpressa = models.InfoImpressa.First();
                var usuario = WebRotinas.ObterUsuarioLogado();
                ContratoParcelaBoletoViewModel Solicitacao = ConverterArrayBoletoVMinListBoletoVM(models);

                List<ContratoParcelaBoleto> _ParcelaBoleto = new List<ContratoParcelaBoleto>();

                List<RegisterBoletoCommand> listCommands = new List<RegisterBoletoCommand>();
                foreach (var solicitado in Solicitacao.ParcelasBoletos)
                {
                    #region parametros
                    ContratoParcela parcela = _dbParcela.FindById(solicitado.CodParcela);
                    var cdCliente = _dbContratoCliente.FindAll(x => !x.DataExcluido.HasValue && x.CodContrato == parcela.CodContrato).FirstOrDefault().CodCliente;
                    Cliente cliente = _dbCliente.FindById((int)cdCliente);
                    #endregion

                    listCommands.Add(new RegisterBoletoCommand()
                    {
                        //Info
                        CodParcela = parcela.Cod,
                        CodBoleto = DbRotinas.ConverterParaInt(solicitado.Cod),
                        ClienteId = cliente.Cod,
                        CodigoCliente = DbRotinas.Descriptografar(cliente.CodCliente),
                        CodUsuario = WebRotinas.ObterUsuarioLogado().Cod,
                        FluxoPagamento = parcela.FluxoPagamento,
                        ParcelaDataVencimento = DbRotinas.ConverterParaDatetime(parcela.DataVencimento),
                        ParcelaValorAberto = DbRotinas.ConverterParaDecimal(parcela.ValorAberto),
                        ParcelaValorParcela = DbRotinas.ConverterParaDecimal(parcela.ValorParcela),
                        ParcelaValorImposto = DbRotinas.ConverterParaDecimal(parcela.Imposto),
                        ParcelaDataEmissaoFatura = DbRotinas.ConverterParaDatetime(parcela.DataEmissaoFatura),
                        ParcelaCodigoEmpresaCobranca = parcela.CodigoEmpresaCobranca,
                        ParcelaLinhaDigitavel = parcela.LinhaDigitavel,
                        ParcelaNossoNumero = parcela.NossoNumero,
                        Companhia = parcela.Companhia,
                        CompanhiaFiscal = parcela.CompanhiaFiscal,
                        InstrucaoPagamento = parcela.InstrucaoPagamento,
                        //Solicitado
                        TipoSolicitacao = EBoletoSolicitacaoTipo.Novo, // TIPO DE SOLICITACAO
                        NumeroDocumento = solicitado.NumeroDocumento,
                        ValorDocumento = DbRotinas.ConverterParaDecimal(solicitado.ValorDocumento),
                        DataVencimento = DbRotinas.ConverterParaDatetime(solicitado.DataMoratoria),
                        DataDocumento = DbRotinas.ConverterParaDatetime(solicitado.DataVencimento),
                        JurosAoMesPorcentagem = DbRotinas.ConverterParaDecimal(solicitado.JurosAoMesPorcentagem),
                        MultaAtrasoPorcentagem = DbRotinas.ConverterParaDecimal(solicitado.MultaAtrasoPercento),
                        MensagemImpressa = msgImpressa, //Mensagem a ser Impressa
                        ValorDesconto = DbRotinas.ConverterParaDecimal(solicitado.ValorDesconto),
                        Justificativa = justificativa
                    });
                }
                List<CommandResult> retornos = new List<CommandResult>();
                foreach (var command in listCommands)
                {
                    _boletoHandler.Handle(command);
                }
                //TRATAR O RETORNO
                if (retornos.Any(x => x.Success == false))
                {
                    StringBuilder sb = new StringBuilder();
                    retornos.Where(x => x.Success == false).ToList().ForEach(x => sb.Append($"{Environment.NewLine}{x.Message}"));
                    return Json(new CommandResult(false, $"Algun(s) boleto(s) não foram registrado corretamente!{sb.ToString()}"));
                }
                //CASO TODOS FOREM EXECUTADOS COM SUCESSO
                return Json(new CommandResult(true, $"Operação realizada com sucesso!"));
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        private ContratoParcelaBoletoHistorico RegistraHistoricoDePara(ContratoParcelaBoletoHistorico Historico)
        {
            var sintesePara = BoletoRotinasUtils.VerificarSintesePara(Historico.CodSintese);
            ContratoParcelaBoleto boleto = new ContratoParcelaBoleto();
            ContratoParcelaBoletoHistorico bHistorico = new ContratoParcelaBoletoHistorico();
            boleto = _repository.FindById((int)Historico.CodBoleto);
            if ((Historico.CodSintese != sintesePara && sintesePara > 0))
            {
                bHistorico.CodBoleto = Historico.CodBoleto;
                bHistorico.CodSintese = sintesePara;
                bHistorico.CodUsuario = Historico.CodUsuario;
                bHistorico.Observacao = Historico.Observacao;

                return _dbBoletoHistorico.CreateOrUpdate(bHistorico);

            }
            return Historico;

        }


        [HttpPost]
        public JsonResult ObterObservacao()
        {
            try
            {
                var observacao = BoletoRotinasUtils.ObterObservacaoEtapa(Enumeradores.EtapaBoleto.AtendenteSolicitaComDesconto);
                return Json(new { OK = true, obs = observacao });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    OK = false,
                    Mensagem = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult DadosParcela(int parcelaId = 0, DTParameters param = null)
        {
            var userLogado = WebRotinas.ObterUsuarioLogado();
            var CodUsuarioLogado = userLogado.Cod;
            var parcela = _dbParcela.FindById(parcelaId);
            var query_dados = _qry.ListarParcelasPorContrato(DbRotinas.ConverterParaString(parcela.CodContrato), 0, 0).ToList();
            query_dados = query_dados.Where(x => x.CodParcela == parcelaId).ToList();
            var lista = (from q in query_dados
                         select new
                         {
                             CodCliente = DbRotinas.Descriptografar(q.CodCliente),
                             CodParcela = q.CodParcela,
                             DataEmissao = $"{q.DataEmissaoFatura:dd/MM/yyyy}",
                             DataVencimento = $"{q.DataVencimento:dd/MM/yyyy}",
                             FluxoPagamento = q.FluxoPagamento,
                             NotaFiscal = q.NF,
                             ValorParcela = q.ValorParcela,
                             ValorAberto = q.ValorAberto,
                             ValorParcelaDesc = $"{q.ValorParcela:C}",
                             ValorAbertoDesc = $"{q.ValorAberto:C}",
                             NumeroParcela = q.NumeroParcela,
                             RotaArea = DbRotinas.Descriptografar(q.RotaArea),
                             CodRegionalCliente = q.CodRegionalCliente,
                             Filial = DbRotinas.Descriptografar(q.Filial),
                             DataFechamento = $"{q.DataFechamento:dd/MM/yyyy}",
                             DataPagamento = $"{q.DataPagamento:dd/MM/yyyy}",
                             Status = verificarStatus(q.DataVencimento, q.DataFechamento, q.ValorAberto),
                             Fase = DbRotinas.Descriptografar(q.Fase),
                             NaturezaCliente = q.NaturezaCliente,
                             Sintese = $"{q.CodigoSinteseCliente} - {DbRotinas.Descriptografar(q.Sintese)}",
                             HistoricoObs = DbRotinas.Descriptografar(q.HistoricoObs),
                             IsPago = q.DataFechamento.HasValue && q.ValorAberto == 0 ? true : false,
                             PossuiLinhaDigitavel = q.LinhaDigitavel != null ? true : false,
                             TipoFatura = q.TipoFatura,
                             Cia = q.Companhia,
                             Governo = q.Governo,
                             InstrucaoPagamento = q.InstrucaoPagamento,
                             NossoNumero = $"{q.NossoNumero}",
                             Corporativo = q.Corporativo,
                             CodigoEmpresaCobranca = q.CodigoEmpresaCobranca,
                             NumeroContrato = DbRotinas.Descriptografar(q.NumeroContrato),
                             DiasAtraso = q.DiasAtraso
                         }).AsQueryable();


            var result = WebRotinas.DTRotinas.DTResult(param, lista);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        private string verificarStatus(DateTime? dtVencimento, DateTime? dtFechamento, decimal? vrAberto)
        {
            var resultado = String.Empty;
            if (dtVencimento > DateTime.Now)
            {
                resultado = EnumeradoresDescricao.NegociacaoTipoParcela((short)Enumeradores.NegociacaoTipoParcela.aVencer);
            }
            if (dtVencimento < DateTime.Now)
            {
                resultado = EnumeradoresDescricao.NegociacaoTipoParcela((short)Enumeradores.NegociacaoTipoParcela.Atrasada);
            }
            if (dtFechamento.HasValue && vrAberto == 0)
            {
                resultado = EnumeradoresDescricao.NegociacaoTipoParcela((short)Enumeradores.NegociacaoTipoParcela.Paga);
            }
            return resultado;

        }


        public IQueryable<object> ListaDadosSolicitacao(int CodBoleto)
        {
            IQueryable<object> data = null;
            data = _dbBoletoHistorico
                         .All()
                         .Where(x => x.CodBoleto == CodBoleto)
                         .ToList()
                         .Select(x => new
                         {
                             Cod = x.Cod,
                             CodParcela = x.ContratoParcelaBoleto.CodParcela,
                             CodContrato = x.ContratoParcelaBoleto.CodParcela > 0 ? x.ContratoParcelaBoleto.ContratoParcela.CodContrato : 0,
                             CodCliente = x.ContratoParcelaBoleto.ContratoParcela.Contrato.CodClientePrincipal,
                             Sintese = x.CodSintese > 0 ? DbRotinas.Descriptografar(x.TabelaSintese.Sintese) : "",
                             NumeroDocumento = $"{x.ContratoParcelaBoleto.NumeroDocumento}",
                             DataVencimento = $"{x.ContratoParcelaBoleto.DataVencimento:dd/MM/yyyy}",
                             DataDocumento = $"{x.ContratoParcelaBoleto.DataDocumento:dd/MM/yyyy}",
                             JurosAoMesPorcentagem = x.ContratoParcelaBoleto.JurosAoMesPorcentagem,
                             JurosAoMesPorcentagemDesc = $"{x.ContratoParcelaBoleto.JurosAoMesPorcentagem:N2}",
                             JurosAoMesPorcentagemPrevisto = x.ContratoParcelaBoleto.JurosAoMesPorcentagemPrevisto,
                             JurosAoMesPorcentagemPrevistoDesc = $"{x.ContratoParcelaBoleto.JurosAoMesPorcentagemPrevisto:N2}",
                             ValorJuros = x.ContratoParcelaBoleto.ValorJuros,
                             ValorJurosDesc = $"{x.ContratoParcelaBoleto.ValorJuros:C}",
                             ValorJurosPrevisto = x.ContratoParcelaBoleto.ValorJurosPrevisto,
                             ValorJurosPrevistoDesc = $"{x.ContratoParcelaBoleto.ValorJurosPrevisto:C}",
                             MultaAtrasoPorcentagem = x.ContratoParcelaBoleto.MultaAtrasoPorcentagem,
                             MultaAtrasoPorcentagemDesc = $"{x.ContratoParcelaBoleto.MultaAtrasoPorcentagem:N2}",
                             MultaAtrasoPorcentagemPrevisto = x.ContratoParcelaBoleto.MultaAtrasoPorcentagemPrevisto,
                             MultaAtrasoPorcentagemPrevistoDesc = $"{x.ContratoParcelaBoleto.MultaAtrasoPorcentagemPrevisto:N2}",
                             ValorMulta = x.ContratoParcelaBoleto.ValorMulta,
                             ValorMultaDesc = $"{x.ContratoParcelaBoleto.ValorMulta:C}",
                             ValorMultaPrevisto = x.ContratoParcelaBoleto.ValorMultaPrevisto,
                             ValorMultaPrevistoDesc = $"{x.ContratoParcelaBoleto.ValorMultaPrevisto:C}",
                             ValorDocumentoOriginal = x.ContratoParcelaBoleto.ValorDocumentoOriginal,
                             ValorDocumentoOriginalDesc = $"{x.ContratoParcelaBoleto.ValorDocumentoOriginal:C}",
                             ValorDocumentoPrevisto = x.ContratoParcelaBoleto.ValorDocumentoPrevisto,
                             ValorDocumentoPrevistoDesc = $"{x.ContratoParcelaBoleto.ValorDocumentoPrevisto:C}",
                             ValorDocumento = x.ContratoParcelaBoleto.ValorDocumento,
                             ValorDocumentoDesc = $"{x.ContratoParcelaBoleto.ValorDocumento:C}",
                             Observacao = x.Observacao
                         }).AsQueryable();

            return data;
        }



        //[HttpPost]
        //public JsonResult SalvarHistorico(ContratoParcelaBoletoHistorico models, string MensagemImpressa)
        //{
        //    BoletoRotinas bRotinas = new BoletoRotinas();
        //    try
        //    {

        //        // Registra o Historico
        //        var id = DbRotinas.ConverterParaInt(models.Cod);
        //        var user = WebRotinas.ObterUsuarioLogado();
        //        models.CodUsuario = user.Cod;
        //        if (models.Observacao == null)
        //        {
        //            var lastmodel = _dbBoletoHistorico.FindAll(x => x.CodBoleto == models.CodBoleto).ToList().LastOrDefault();
        //            models.Observacao = $"{lastmodel.Observacao}";
        //        }


        //        var entity = _dbBoletoHistorico.CreateOrUpdate(models);

        //        //Verifica se a classificação deve ir para outra classificacao
        //        var history = RegistraHistoricoDePara(entity);

        //        ContratoParcelaBoleto boleto = _repository.FindById((int)history.CodBoleto);
        //        if (boleto.MensagemImpressa != MensagemImpressa)
        //            boleto.MensagemImpressa = MensagemImpressa;

        //        boleto.CodHistorico = history.Cod;

        //        // Verifica a Etapa do Boleto
        //       Enumeradores.EtapaBoleto etapa = BoletoRotinasUtils.ObterEtapaDaSintese((int)entity.CodSintese);
        //        // Aprova o Boleto
        //        if (etapa == Enumeradores.EtapaBoleto.SupervisorAprovado)
        //        {
        //            // gera o nosso numero
        //            bRotinas.ProcessarNossoNumero(boleto);
        //            // gerar linha digitavel
        //            var _codigoBarra = BoletoUtils.GerarCodigoBarra(boleto);

        //            var parcela = _dbParcela.FindById((int)boleto.CodParcela);

        //            //SE FOR NOVO BOLETO DEVE CRIAR NA TABELA DE CONTRATOPARCELA O NOSSO NUMERO
        //            if (!DbRotinas.ConverterParaBool(boleto.Alterar))
        //            {
        //                parcela.NossoNumero = boleto.NossoNumero;
        //            }
        //            parcela.LinhaDigitavel = _codigoBarra.LinhaDigitavel;
        //            _dbParcela.Update(parcela);//ATUALIZA PARCELA

        //            boleto.Status = (short)Enumeradores.StatusBoleto.Aprovado;
        //            boleto.DataStatus = DateTime.Now;
        //            boleto.DataAprovacao = DateTime.Now;
        //            boleto.CodUsuarioStatus = user.Cod;
        //        }
        //        else if (etapa == Enumeradores.EtapaBoleto.AnalistaReprova || etapa == Enumeradores.EtapaBoleto.SupervisorReprova)
        //        {
        //            boleto.Status = (short)Enumeradores.StatusBoleto.Reprovado;
        //            boleto.DataStatus = DateTime.Now;
        //            boleto.CodUsuarioStatus = WebRotinas.ObterUsuarioLogado().Cod;
        //        }

        //        // Reprova o Boleto
        //        _repository.Update(boleto);

        //        return Json(new { OK = true, Titulo = "Solicitação!", Mensagem = "Operação Registrada!" });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { OK = false, Mensagem = ex.Message });
        //    }
        //}


        [HttpPost]
        public JsonResult SalvarHistorico(RegistrarBoletoHistoricoCommand command)
        {
            var user = WebRotinas.ObterUsuarioLogado();
            command.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
            var result = _boletoHandler.Handle(command);
            return Json(result);
        }


        [HttpPost]
        public JsonResult ValidarSolicitacao(ContratoParcelaBoletoViewModel.BoletoViewModelViewModel models)
        {
            try
            {
                var justificativa = models.Observacao.First();
                var usuario = WebRotinas.ObterUsuarioLogado();

                ContratoParcelaBoletoViewModel Solicitacao = new ContratoParcelaBoletoViewModel();
                Solicitacao.ParcelasBoletos = new List<ContratoParcelaBoletoViewModel.ParcelaBoleto>();
                for (int i = 0; i < models.Cod.Length; i++)
                {
                    Solicitacao.ParcelasBoletos.Add(new ContratoParcelaBoletoViewModel.ParcelaBoleto(models.Cod[i], models.CodParcela[i], models.NumeroDocumento[i], models.DataVencimento[i], models.DataMoratoria[i], models.ValorDocumento[i], models.MultaAtrasoPorcentagem[i], models.JurosAoMesPorcentagem[i], models.ValorDesconto[i]));
                }
                List<ContratoParcelaBoleto> _ParcelaBoleto = new List<ContratoParcelaBoleto>();

                foreach (var solicitado in Solicitacao.ParcelasBoletos)
                {
                    var id = DbRotinas.ConverterParaInt(solicitado.Cod);
                    if (id == 0)
                    {
                        ContratoParcelaBoleto boleto = Activator.CreateInstance<ContratoParcelaBoleto>();
                        boleto.DataCadastro = DateTime.Now;
                    }
                    else
                    {
                        ContratoParcelaBoleto boleto = _dbRepository.FindById(id);
                    }
                    var paramBanco = _repBancoPortador.GetByParcelaId(solicitado.CodParcela);
                    if ((solicitado.JurosAoMesPorcentagem != paramBanco.JurosAoMesPorcentagem || solicitado.MultaAtrasoPercento != paramBanco.MultaAtraso || solicitado.ValorDesconto > 0) && String.IsNullOrEmpty(justificativa))
                    {
                        var parcela = _dbParcela.FindById(solicitado.CodParcela);
                        if (parcela.DataVencimento < DateTime.Now)
                        {
                            return Json(new { OK = false, Titulo = "Solicitação!", Mensagem = "Ao solicitar um desconto é necessario justificar!" });
                        }

                    }
                    if (solicitado.JurosAoMesPorcentagem > paramBanco.JurosAoMesPorcentagem)
                    {
                        return Json(new { OK = false, Titulo = "Solicitação!", Mensagem = "Existe algun(s) boleto(s) com Juros acima do permitido!" });
                    }
                    if (solicitado.MultaAtrasoPercento > paramBanco.MultaAtraso)
                    {
                        return Json(new { OK = false, Titulo = "Solicitação!", Mensagem = "Existe algun(s) boleto(s) com Multa acima do permitido" });
                    }
                    if (_repBoleto.ExisteBoletoPendentePorParcelaId(solicitado.CodParcela))
                    {
                        return Json(new { OK = false, Titulo = "Solicitação!", Mensagem = "Já existe uma solicitação em andamento para este documento." });
                    }

                }
                return Json(new { OK = true, Titulo = "Boleto Solicitado!", Mensagem = "Em breve será análisado." });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ValidarSolicitacaoAvulso(ValidarBoletoAvulsoCommand command)
        {
            var result = _boletoHandler.Handle(command);
            return Json(result);
        }

        [HttpGet]
        public PartialViewResult CarregarAvulso(int cliente, string contratos)
        {
            var dados = _repBoleto.GetBoletoAvulsoView(cliente);
            var listaContratoIds = contratos.Split(',').Select(x => DbRotinas.ConverterParaInt(x)).ToList();

            var listContratos = _dbContrato.All().Where(x => listaContratoIds.Contains(x.Cod)).ToList();
            dados.ListaContrato = (from p in listContratos.OrderBy(x => x.Cod).ToList() select new DictionaryEntry(p.Cod, $"{DbRotinas.Descriptografar(p.NumeroContrato)}")).ToList();

            var contratoGrupos = listContratos.Select(x => x.CodRegionalGrupo).ToList();

            var listaPortadores = _repBancoPortador.GetListBancoPortadorByRegionalGrupoId(string.Join(",", contratoGrupos.Where(x => x != null)));
            dados.ListaBanco = (from p in listaPortadores.OrderBy(x => x.Cod).ToList() select new DictionaryEntry(p.Cod, $"{p.NumeroBanco} - {p.NomeBanco} - {p.CodRegionalCliente}/{p.CompanhiaFiscal}")).ToList();

            //Por padrão deve trazer 2 dias pra frente
            if (dados.DataVencimento == null)
                dados.DataVencimento = ContratoParcelaBoletoViewModel.proximoDiaUtil(DateTime.Now.AddDays(2).Date);


            return PartialView("_Avulso", dados);
        }

        [HttpPost]
        public JsonResult SalvarBoletoAvulso(RegisterBoletoAvulsoCommand command)
        {
            command.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
            var result = _boletoHandler.Handle(command);
            return Json(result);
        }

        [HttpPost]
        public JsonResult SalvarModificar(ContratoParcelaBoletoViewModel.BoletoViewModelViewModel models)
        {
            try
            {
                var justificativa = models.Observacao.First();
                var msgImpressa = models.InfoImpressa.First();
                var usuario = WebRotinas.ObterUsuarioLogado();
                ContratoParcelaBoletoViewModel Solicitacao = ConverterArrayBoletoVMinListBoletoVM(models);

                List<ContratoParcelaBoleto> _ParcelaBoleto = new List<ContratoParcelaBoleto>();

                List<RegisterBoletoCommand> listCommands = new List<RegisterBoletoCommand>();
                foreach (var solicitado in Solicitacao.ParcelasBoletos)
                {
                    #region parametros
                    ContratoParcela parcela = _dbParcela.FindById(solicitado.CodParcela);
                    var cdCliente = _dbContratoCliente.FindAll(x => !x.DataExcluido.HasValue && x.CodContrato == parcela.CodContrato).FirstOrDefault().CodCliente;
                    Cliente cliente = _dbCliente.FindById((int)cdCliente);
                    #endregion

                    listCommands.Add(new RegisterBoletoCommand()
                    {
                        //Info
                        CodParcela = parcela.Cod,
                        CodBoleto = DbRotinas.ConverterParaInt(solicitado.Cod),
                        ClienteId = cliente.Cod,
                        CodigoCliente = DbRotinas.Descriptografar(cliente.CodCliente),
                        CodUsuario = WebRotinas.ObterUsuarioLogado().Cod,
                        FluxoPagamento = parcela.FluxoPagamento,
                        ParcelaDataVencimento = DbRotinas.ConverterParaDatetime(parcela.DataVencimento),
                        ParcelaValorAberto = DbRotinas.ConverterParaDecimal(parcela.ValorAberto),
                        ParcelaValorParcela = DbRotinas.ConverterParaDecimal(parcela.ValorParcela),
                        ParcelaValorImposto = DbRotinas.ConverterParaDecimal(parcela.Imposto),
                        ParcelaDataEmissaoFatura = DbRotinas.ConverterParaDatetime(parcela.DataEmissaoFatura),
                        ParcelaCodigoEmpresaCobranca = parcela.CodigoEmpresaCobranca,
                        ParcelaLinhaDigitavel = parcela.LinhaDigitavel,
                        ParcelaNossoNumero = parcela.NossoNumero,
                        Companhia = parcela.Companhia,
                        //Solicitado
                        TipoSolicitacao = EBoletoSolicitacaoTipo.Alterar, // TIPO DE SOLICITACAO
                        NumeroDocumento = solicitado.NumeroDocumento,
                        ValorDocumento = DbRotinas.ConverterParaDecimal(solicitado.ValorDocumento),
                        DataVencimento = DbRotinas.ConverterParaDatetime(solicitado.DataMoratoria),
                        DataDocumento = DbRotinas.ConverterParaDatetime(solicitado.DataVencimento),
                        JurosAoMesPorcentagem = DbRotinas.ConverterParaDecimal(solicitado.JurosAoMesPorcentagem),
                        MultaAtrasoPorcentagem = DbRotinas.ConverterParaDecimal(solicitado.MultaAtrasoPercento),
                        MensagemImpressa = msgImpressa, //Mensagem a ser Impressa
                        ValorDesconto = DbRotinas.ConverterParaDecimal(solicitado.ValorDesconto),
                        Justificativa = justificativa
                    });
                }
                List<CommandResult> retornos = new List<CommandResult>();
                foreach (var command in listCommands)
                {
                    _boletoHandler.Handle(command);
                }
                //TRATAR O RETORNO
                if (retornos.Any(x => x.Success == false))
                {
                    StringBuilder sb = new StringBuilder();
                    retornos.Where(x => x.Success == false).ToList().ForEach(x => sb.Append($"{Environment.NewLine}{x.Message}"));
                    return Json(new CommandResult(false, $"Algun(s) boleto(s) não foram registrado corretamente!{sb.ToString()}"));
                }
                //CASO TODOS FOREM EXECUTADOS COM SUCESSO
                return Json(new CommandResult(true, $"Operação realizada com sucesso!"));
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        private void atualizarHistoricoDoBoleto(int boletoId, int historicoId)
        {
            Repository<ContratoParcelaBoleto> _db = new Repository<ContratoParcelaBoleto>();
            var Boleto = _db.FindById(boletoId);
            Boleto.CodHistorico = historicoId;
            _db.Update(Boleto);

        }

        #region metodos padrao
        ///// <summary>
        ///// Método para processar o nosso numero
        ///// </summary>
        //private void ProcessarNossoNumero(ContratoParcelaBoleto boleto)
        //{
        //    Repository<ContratoParcelaBoleto> _db = new Repository<ContratoParcelaBoleto>();
        //    Repository<TabelaConstrutora> _repConstrutora = new Repository<TabelaConstrutora>();
        //    BoletoUtils utils = new BoletoUtils();
        //    var b = _db.FindById(boleto.Cod);
        //    ContratoParcela parcela = new ContratoParcela();
        //    if (boleto.ContratoParcela == null)
        //    {
        //        parcela = _dbParcela.FindById(DbRotinas.ConverterParaInt(b.CodParcela));

        //    }
        //    else
        //    {
        //        parcela = boleto.ContratoParcela;
        //    }
        //    var _paramPortador = BoletoUtils.ObterParametroPortador(parcela);

        //    //VERIFICA SE O BOLETO JA POSSUI O NOSSO NUMERO.
        //    if (parcela.ValorParcela == parcela.ValorAberto && parcela.NossoNumero > 0 && parcela.DataEmissaoFatura > _paramPortador.DataInicialRegistroBoleto)
        //    {
        //        b.Alterar = true;
        //        b.NossoNumero = parcela.NossoNumero;
        //    }
        //    else
        //    {
        //        b.NossoNumero = utils.GerarNossoNumero(boleto);
        //    }
        //    _db.CreateOrUpdate(b);
        //    boleto.NossoNumero = b.NossoNumero;
        //}

        /// <summary>
        /// Método para verificar em qual etapa o boleto será processado.
        /// </summary>
        private static void ConfigurarEtapaBoleto(ContratoParcelaBoleto boleto, Enumeradores.EtapaBoleto Etapa)
        {
            if (Etapa == Enumeradores.EtapaBoleto.AtendenteSolicitaProrrogacao)
            {
                boleto.Status = (short)Enumeradores.StatusBoleto.Aprovado;
                boleto.DataStatus = DateTime.Now;
                boleto.DataAprovacao = DateTime.Now;
                boleto.CodUsuarioStatus = WebRotinas.ObterUsuarioLogado().Cod;
            }
            else
            {
                boleto.Status = (short)Enumeradores.StatusBoleto.SolicitacaoAnalista;
                boleto.DataStatus = DateTime.Now;
                boleto.CodUsuarioStatus = WebRotinas.ObterUsuarioLogado().Cod;
            }
        }



        /// <summary>
        /// Método calcula o valor orinal do boleto.
        /// </summary>
        private static void CalcularValorOriginal(ContratoParcelaBoleto boleto, ContratoParcela parcela)
        {
            boleto.ValorDocumentoOriginal = parcela.ValorAberto == parcela.ValorParcela ? ((decimal)parcela.ValorAberto - DbRotinas.ConverterParaDecimal(parcela.Imposto)) : parcela.ValorAberto;
        }

        /// <summary>
        /// Método para validar o boleto e seus dias. Por definição boletos com + de 360 dias de atraso deverá ser somente emitidos como um novo boleto
        /// </summary>
        /// <param > Linha 1</param>
        /// <param name="QtdDias" > Teste para imprimir informação</param>
        /// <returns>new exeption com msg</returns>
        /// <remarks>Deve ser implementado somenta para verificar boletos ja existentes (alterar)</remarks>
        /// <example>(dataVencimento - DateTime.Now).TotalDays</example>    
        private static void ValidarQtdDiasAtraso(int _qtdDias, string _companhia)
        {
            string _companhiaOTIS = "00400";
            string _companhiaSERAL = "00405";
            if (_qtdDias >= 360 && _companhia == _companhiaOTIS)
            {
                throw new Exception("Esta fatura está com mais de 360 dias do seu vencimento e deverá ser emitida somente como um novo boleto.");
            }
            else if (_qtdDias >= 120 && _companhia == _companhiaSERAL)
            {
                throw new Exception("Esta fatura está com mais de 120 dias do seu vencimento e deverá ser emitida somente como um novo boleto.");
            }
        }



        private static ContratoParcelaBoletoViewModel ConverterArrayBoletoVMinListBoletoVM(ContratoParcelaBoletoViewModel.BoletoViewModelViewModel models)
        {
            ContratoParcelaBoletoViewModel Solicitacao = new ContratoParcelaBoletoViewModel
            {
                ParcelasBoletos = new List<ContratoParcelaBoletoViewModel.ParcelaBoleto>()
            };
            for (int i = 0; i < models.Cod.Length; i++)
            {
                Solicitacao.ParcelasBoletos.Add(new ContratoParcelaBoletoViewModel.ParcelaBoleto(models.Cod[i], models.CodParcela[i], models.NumeroDocumento[i], models.DataVencimento[i], models.DataMoratoria[i], models.ValorDocumento[i], models.MultaAtrasoPorcentagem[i], models.JurosAoMesPorcentagem[i], models.ValorDesconto[i]));
            }

            return Solicitacao;
        }
        #endregion
        [HttpGet]
        public ActionResult History(int bId = 0)
        {
            var boleto = _repBoleto.FindById(bId);

            var header = new ClienteHeaderViewModel();
            header = PreecherDadosHeader(boleto);
            ViewBag.ClienteHeader = header;

            var entitie = _dbBoleto.FindById(boleto.Cod);

            return PartialView("_formHistory", entitie);
        }

        private ClienteHeaderViewModel PreecherDadosHeader(ParcelaBoleto boleto)
        {
            var header = new ClienteHeaderViewModel();
            if (boleto.Boleto.TipoSolicitacao == (short)EBoletoSolicitacaoTipo.Avulso)
            {
                var contrato = _repContrato.GetContratoInfoById((int)boleto.CodContrato);
                header = new ClienteHeaderViewModel
                {
                    CodCliente = contrato.CodClienteDecrypt,
                    Cliente = contrato.ClienteDecrypt,
                    NumeroContrato = contrato.NumeroContratoDecrypt,
                    CpfCnpj = DbRotinas.formatarCpfCnpj(contrato.ClienteDocumentoDecrypt),
                    Regional = contrato.RegionalDecrypt,
                    DataVencimento = boleto.Boleto.DataVencimento,
                    StatusContrato = contrato.StatusContrato,
                    Produto = contrato.ProdutoDecrypt,
                    ValorTotalContrato = contrato.ValorTotal,
                    ValorAtrasoContrato = contrato.ValorAtraso,
                    ValorAVencerContrato = contrato.ValorAVencer,
                    DiaAtrasoContrato = contrato.DiasAtraso,
                    CodigoSinteseCliente = "",
                    TipoSolicitacao = boleto.Boleto.TipoSolicitacao
                };
            }
            else
            {
                ContratoParcela dados = _dbParcela.FindById((int)boleto.Parcela.Cod);
                Cliente cliente = dados.Contrato.ContratoClientes.FirstOrDefault().Cliente;
                header = new ClienteHeaderViewModel
                {
                    CodCliente = DbRotinas.Descriptografar(cliente.CodCliente),
                    Cliente = DbRotinas.Descriptografar(cliente.Nome),
                    CpfCnpj = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(cliente.CPF)),
                    DataVencimento = dados.DataVencimento,
                    ValorParcela = dados.ValorParcela,
                    ValorAberto = dados.ValorAberto,
                    NossoNumero = dados.NossoNumero,
                    NumeroDocumento = dados.FluxoPagamento,
                    NumeroContrato = DbRotinas.Descriptografar(dados.Contrato.NumeroContrato),
                    TipoFatura = dados.TipoFatura,
                    CodigoSinteseCliente = dados.CodHistorico > 0 ? dados.Historico.TabelaSintese.CodigoSinteseCliente : "",
                    TipoSolicitacao = boleto.Boleto.TipoSolicitacao
                };
            }

            return header;
        }

        [HttpPost]
        public JsonResult ObterDadosHistory(DTParameters param, int? CodBoleto)
        {
            try
            {
                var dtsource = ListaDadosSolicitacao(CodBoleto);

                var result = WebRotinas.DTRotinas.DTResult(param, dtsource);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public IQueryable<object> ListaDadosSolicitacao(int? CodBoleto)
        {
            var historicos = _dbBoletoHistorico.All().Where(x => x.CodBoleto == CodBoleto);

            IQueryable<object> data = null;
            data = historicos
                    .GroupBy(l => l.TabelaSintese.TabelaFase.CodTipoCobranca)
                    .Select(g => g.OrderByDescending(c => c.Cod).FirstOrDefault())
                    .ToList()
                         .Select(x => new
                         {
                             bId = x.Cod,
                             Usuario = x.CodUsuario > 0 ? x.Usuario.Nome : "Serviço",
                             Data = $"{x.DataCadastro:dd/MM/yyyy HH:mm:ss}",
                             TipoCobranca = x.CodSintese > 0 ? $"{DbRotinas.Descriptografar(x.TabelaSintese.TabelaFase.TabelaTipoCobranca.TipoCobranca)}" : "",
                             Fase = x.CodSintese > 0 ? DbRotinas.Descriptografar(x.TabelaSintese.TabelaFase.Fase) : "",
                             Sintese = x.CodSintese > 0 ? DbRotinas.Descriptografar(x.TabelaSintese.Sintese) : "",
                             Observacao = x.Observacao
                         }).AsQueryable();


            return data;
        }

        [HttpGet]
        public JsonResult ValidarMulta(decimal? multa)
        {
            try
            {
                List<ContratoParcelaBoleto> _ParcelaBoleto = new List<ContratoParcelaBoleto>();
                var _parametroBanco = _dbBancoPortador.FindAll().First();
                if (_parametroBanco == null)
                {
                    _parametroBanco = new TabelaBancoPortador();
                }
                if (_parametroBanco.MultaAtraso >= multa)
                {
                    return Json(new { OK = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { OK = false, Titulo = "Informação!", Mensagem = $"A multa não pode ultrapassar o limite definido de {(_parametroBanco.MultaAtraso / 100):P}" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult ValidarJuros(decimal? juros)
        {
            try
            {
                List<ContratoParcelaBoleto> _ParcelaBoleto = new List<ContratoParcelaBoleto>();
                var _parametroBanco = _dbBancoPortador.FindAll().First();
                if (_parametroBanco == null)
                {
                    _parametroBanco = new TabelaBancoPortador();
                }
                if (_parametroBanco.JurosAoMesPorcentagem >= juros)
                {
                    return Json(new { OK = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { OK = false, Titulo = "Informação!", Mensagem = $"O juros não pode ultrapassar o limite definido de {(_parametroBanco.JurosAoMesPorcentagem / 100):P}" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult ValidarSolicitacaoAlterarExistente(string p = null)
        {
            //VERIFICA SE O BOLETO JA POSSUI  REGISTRO NO BANCO (NOSSO NUMERO)
            try
            {
                Repository<TabelaConstrutora> _repConstrutora = new Repository<TabelaConstrutora>();
                List<ContratoParcela> parcelas = new List<ContratoParcela>();
                string[] idParcelas = p.Split(',');

                foreach (var item in idParcelas)
                {
                    var id = DbRotinas.ConverterParaInt(item);
                    var dados = _dbParcela.FindById(id);
                    if (idParcelas.Count() == 1 && dados == null)
                        return Json(new { Success = false, Title = "Solicitação!", Message = $"Selecione ao menos uma parcela!" }, JsonRequestBehavior.AllowGet);

                    var retorno = _boletoHandler.Handle(new ValidarBoletoExistenteCommand()
                    {
                        CodParcela = dados.Cod,
                        FluxoPagamento = dados.FluxoPagamento,
                        ValorAberto = dados.ValorAberto,
                        ValorParcela = dados.ValorParcela,
                        NossoNumero = dados.NossoNumero,
                        CodigoEmpresaCobranca = dados.CodigoEmpresaCobranca,
                        qtdDias = DbRotinas.ConverterParaInt(((TimeSpan)(DateTime.Now - dados.DataEmissaoFatura)).Days)
                    });

                    if (!retorno.Success)
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                }

                return Json(new { Success = true, Title = "Solicitação!", Message = $"Informações Correta!" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult ValidarSolicitacaoModificar(decimal? juros)
        {
            try
            {
                List<ContratoParcelaBoleto> _ParcelaBoleto = new List<ContratoParcelaBoleto>();
                var _parametroBanco = _dbBancoPortador.FindAll().First();
                if (_parametroBanco == null)
                {
                    _parametroBanco = new TabelaBancoPortador();
                }
                if (_parametroBanco.JurosAoMesPorcentagem >= juros)
                {
                    return Json(new { OK = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { OK = false, Mensagem = $"O juros não pode ultrapassar o limite definido de {(_parametroBanco.JurosAoMesPorcentagem / 100):P}" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult ValidarBoleto(string p = null)
        {
            //VERIFICA SE O BOLETO JA POSSUI  REGISTRO NO BANCO (NOSSO NUMERO)
            try
            {
                List<ContratoParcela> parcelas = new List<ContratoParcela>();
                string[] idParcelas = p.Split(',');
                //return Json(new { OK = true, Titulo = "Solicitação!", Mensagem = $"Informações Correta!" }, JsonRequestBehavior.AllowGet);//TESTE
                foreach (var item in idParcelas)
                {
                    var id = DbRotinas.ConverterParaInt(item);
                    var dados = _dbParcela.FindById(id);
                    ContratoParcelaBoleto boleto = new ContratoParcelaBoleto();
                    DateTime _dtVencimento = dados.DataVencimento;
                    DateTime _dtHoje = DateTime.Now.Date;

                    if (dados.ContratoParcelaBoletoes != null)
                    {
                        var boletos = dados.ContratoParcelaBoletoes.Where(x => x.Status == (short)Enumeradores.StatusBoleto.Aprovado).ToList();
                        if (boletos.Count > 0)
                        {
                            boleto = boletos.OrderByDescending(x => x.Cod).First();
                            _dtVencimento = (DateTime)boleto.DataVencimento;
                        }
                    }
                    var qtdDias = DbRotinas.ConverterParaInt(((TimeSpan)(DateTime.Now - dados.DataEmissaoFatura)).Days);
                    if (_dtVencimento != null)
                    {
                        //RETORNA O PROXIMO DIA UTIL
                        _dtVencimento = ContratoParcelaBoletoViewModel.proximoDiaUtil(_dtVencimento);
                        if (_dtVencimento < _dtHoje && dados.NossoNumero == null)
                        {
                            return Json(new { OK = false, Titulo = "Boleto!", Mensagem = $"A Parcela ({dados.FluxoPagamento}) só serão emitidas faturas a vencer!" }, JsonRequestBehavior.AllowGet);
                        }
                        else if (_dtVencimento < _dtHoje && dados.NossoNumero != null)
                        {
                            return Json(new { OK = false, Titulo = "Boleto!", Mensagem = $"A Parcela ({dados.FluxoPagamento}) já possui registro no banco e deverá ser alterado." }, JsonRequestBehavior.AllowGet);
                        }
                        else if (qtdDias >= 360 && !boleto.Equals(new ContratoParcelaBoleto()))//SE O BOLETO ESTIVER REGISTRADO A MAIS DE 360 DIAS O TERA QUE REGISTRAR NOVAMENTE
                        {
                            return Json(new { OK = false, Titulo = "Boleto!", Mensagem = $"A Parcela ({dados.FluxoPagamento}) possui um registro no banco há mais de 360 dias e só poderá ser emitida como um novo boleto!" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { OK = false, Titulo = "Boleto!", Mensagem = $"Ocorreu um erro inesperado!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { OK = true, Titulo = "Solicitação!", Mensagem = $"Informações Correta!" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult ValidarSolicitacaoNovo(string p = null)
        {
            //VERIFICA SE O BOLETO JA POSSUI  REGISTRO NO BANCO (NOSSO NUMERO)
            try
            {
                List<ContratoParcela> parcelas = new List<ContratoParcela>();
                Repository<TabelaConstrutora> _repConstrutora = new Repository<TabelaConstrutora>();
                string[] idParcelas = p.Split(',');
                foreach (var item in idParcelas)
                {
                    var id = DbRotinas.ConverterParaInt(item);
                    var dados = _dbParcela.FindById(id);
                    var qtdDias = DbRotinas.ConverterParaInt(((TimeSpan)(DateTime.Now - dados.DataEmissaoFatura)).Days);
                    var _paramPortador = BoletoUtils.ObterParametroPortador(dados);
                    dados.ValorAberto = DbRotinas.ConverterParaDecimal($"{dados.ValorAberto:N0}");
                    dados.ValorParcela = DbRotinas.ConverterParaDecimal($"{dados.ValorParcela:N0}");
                    //VERIFICAR PELA COMPANHIA DA PARCELA 0400 ou 0405 A Qtde de dias registrado junto ao banco
                    if (dados.ValorAberto > 0 && ((qtdDias <= _paramPortador.NumeroDiaRegistroBoleto) && dados.DataEmissaoFatura > _paramPortador.DataInicialRegistroBoleto))
                    {
                        //AINDA NAO EXISTE VALIDACAO
                        if (dados.ValorAberto == dados.ValorParcela && dados.NossoNumero.HasValue)
                        {
                            return Json(new { OK = false, Titulo = "Solicitação!", Mensagem = $"A Parcela ({dados.FluxoPagamento}) ja possui um registro no banco!" }, JsonRequestBehavior.AllowGet);
                        }
                        else if (dados.CodigoEmpresaCobranca == "BNS" || dados.CodigoEmpresaCobranca == "FAS")
                        {
                            return Json(new { OK = false, Titulo = "Solicitação!", Mensagem = $"A Parcela ({dados.FluxoPagamento}) esta com a empresa de cobrança {dados.CodigoEmpresaCobranca} !" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new { OK = true, Titulo = "Solicitação!", Mensagem = $"Informações Correta!" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult ValidarJustificarObrigatorio(int s = 0, string obs = null)
        {
            //VERIFICA SE O BOLETO JA POSSUI  REGISTRO NO BANCO (NOSSO NUMERO)
            try
            {
                string Justifica = Request.Form["Observacao"];
                if (Justifica == null)
                {
                    Justifica = obs;
                }
                var id = DbRotinas.ConverterParaInt(s);
                var dados = _dbSintese.FindById(id);
                //AINDA NAO EXISTE VALIDACAO
                if (dados.Justifica == true && String.IsNullOrEmpty(Justifica))
                {
                    return Json(new { OK = false, Titulo = "Atenção!", Mensagem = $"Obrigatório inserir uma justificativa." }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { OK = true, Titulo = "Solicitação!", Mensagem = $"Informações Correta!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult ValidarEmpresaCobranca(string p = null)
        {
            List<ContratoParcela> parcelas = new List<ContratoParcela>();
            string[] idParcelas = p.Split(',');
            foreach (var item in idParcelas)
            {
                var id = DbRotinas.ConverterParaInt(item);
                var dados = _dbParcela.FindById(id);
                //AINDA NAO EXISTE VALIDACAO
                if (dados.CodigoEmpresaCobranca == "BNS" || dados.CodigoEmpresaCobranca == "FAS")
                {
                    return Json(new { OK = false, Titulo = "Verificação ...", Mensagem = $"A Parcela ({dados.FluxoPagamento}) esta com a empresa de cobrança {dados.CodigoEmpresaCobranca} !" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { OK = true, Titulo = "Solicitação!", Mensagem = $"Informações Correta!" }, JsonRequestBehavior.AllowGet);

        }
        public bool BoletoAprovar(ContratoParcelaBoleto boleto)
        {
            BoletoUtils utils = new BoletoUtils();
            try
            {
                Repository<ContratoParcela> _db = new Repository<ContratoParcela>();
                var b = _dbBoleto.FindById(boleto.Cod);
                var _codigoBarra = utils.GerarLinhaDigitavel(b);
                ContratoParcela parcela = _db.FindById(DbRotinas.ConverterParaInt(boleto.CodParcela));
                parcela.LinhaDigitavel = _codigoBarra.LinhaDigitavel;

                //SE FOR NOVO BOLETO DEVE CRIAR NA TABELA DE CONTRATOPARCELA O NOSSO NUMERO
                if (!DbRotinas.ConverterParaBool(boleto.Alterar))
                {
                    parcela.NossoNumero = boleto.NossoNumero;
                }
                _db.CreateOrUpdate(parcela);

                TratarBoletosAntigosDaParcela(boleto);

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar a linha digitavel.", ex);
            }
        }

        private static void TratarBoletosAntigosDaParcela(ContratoParcelaBoleto boleto)
        {
            junix_cobrancaContext cx = new junix_cobrancaContext();
            var boletoes = cx
                            .ContratoParcelaBoletoes
                               .Where(x =>
                                      !x.DataExcluido.HasValue
                                      && boleto.Cod != x.Cod
                                      && boleto.CodParcela == x.CodParcela
                                    );
            List<short> list = new List<short>();
            list.AddRange(new short[] { (short)Enumeradores.StatusBoleto.Cancelado, (short)Enumeradores.StatusBoleto.Reprovado, (short)Enumeradores.StatusBoleto.Obsoleto });
            foreach (var c in boletoes)
            {
                if (!list.Contains((short)c.Status))
                {
                    c.Status = (short)Enumeradores.StatusBoleto.Obsoleto;
                }
            }
            cx.SaveChanges();
        }

        [HttpGet]
        public ActionResult MontarBoleto(int b = 0)
        {
            BoletoUtils gerarBoleto = new BoletoUtils();
            List<Notification> notificatons = new List<Notification>();

            var boleto = _dbBoleto.FindById(b);
            if (b == 0) notificatons.Add(new Notification("Boleto", "Boleto inválido!"));

            var objAcao = _dbTabelaModeloAcao.FindAll(x => !x.DataExcluido.HasValue && x.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.EMAIL_BOLETO_SOLICITADO).FirstOrDefault();
            var assunto = objAcao.TabelaAcaoCobranca?.AcaoCobranca;
            var idContrato = boleto.CodContrato;
            var mensagem = "";
            if (!(boleto.TipoSolicitacao == (short)EBoletoSolicitacaoTipo.Avulso))
            {
                mensagem = criarMensagemBoleto(boleto);
                idContrato = (int)boleto.ContratoParcela?.CodContrato;
            }
            

            var resultado = (MontarBoletoCommandResult)_boletoHandler.Handle(new MontarBoletoCommand() { CodBoleto = (int)b, CodContrato = (int)idContrato, Assunto = assunto, Mensagem = mensagem });

            var model = new BoletoEnvioViewModel();
            model.BoletoHTML.Add(resultado.Boleto.MontaHtmlEmbedded());
            model.Email = resultado.Email;
            model.Assunto = resultado.Assunto;
            model.Mensagem = resultado.Mensagem;


            return PartialView("formBoletoEnvio", model);
        }

        private string criarMensagemBoleto(ContratoParcelaBoleto boleto)
        {
            StringBuilder sb = new StringBuilder();
            var Contrato = boleto.ContratoParcela.Contrato;
            var objRegional = new TabelaRegional();

            var CodRegional = Contrato.TabelaRegionalProduto.CodRegional;
            objRegional = _dbRegional.FindById((int)CodRegional);
            var CodCliente = (int)Contrato.CodClientePrincipal;

            var objCliente = _dbCliente.FindById(CodCliente);
            var objAcao = _dbTabelaModeloAcao.FindAll(x => !x.DataExcluido.HasValue && x.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.EMAIL_BOLETO_SOLICITADO).FirstOrDefault();
            sb.Append(objAcao.Modelo);

            string tbParcelas = MontarTabelaBoletoHtml(boleto);
            //var retorno = _boletoHandler.Handle(new MontarTabelaHtmlCommand()
            //{
            //    ParcelaDataVencimento = boleto.Parcela.DataVencimento,
            //    DataVencimento = (DateTime)boleto.Boleto.DataVencimento,
            //    NumeroContrato = boleto.Parcela.NumeroContrato,
            //    NumeroDocumento = boleto.Boleto.NumeroDocumento,
            //    NumeroParcela = boleto.Parcela.NumeroParcela,
            //    ValorDocumento = (decimal)boleto.Boleto.ValorDocumento,
            //    ValorParcelaAberto = (decimal)boleto.Parcela.ValorAberto
            //});
            //sb = (StringBuilder)retorno.Data;
            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.TabelaParcelas), tbParcelas);
            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NomeComprador), DbRotinas.Descriptografar(objCliente.Nome));
            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.CPF_CPNPJ_Comprador), DbRotinas.Descriptografar(objCliente.CPF));
            if (objRegional != null)
            {
                //DADOS DA REGIONAL PRODUTO;
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.DescricaoFilial), DbRotinas.Descriptografar(objRegional.Regional));
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NumeroFilial), objRegional.Telefone);
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.ResponsavelFilial), objRegional.Responsavel);
            }
            return sb.ToString();
        }

        private string MontarTabelaBoletoHtml(ContratoParcelaBoleto boleto)
        {
            var thead_tr_td = "border:solid #666666 1.0pt;background:#CFCFCF;padding:3.75pt 3.0pt 3.75pt 3.0pt;text-align: center;";
            var thead_tr_td_span = "font-size:10.0pt;font-family:Verdana,sans-serif;color:white";
            StringBuilder sbTabelaParcelas = new StringBuilder();

            sbTabelaParcelas.AppendFormat($"<table border='0' cellspacing='0' cellpadding='0' style='width:100%; border-collapse:collapse'>");
            sbTabelaParcelas.AppendFormat($"<thead>");
            sbTabelaParcelas.AppendFormat($"<tr>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Contrato</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Fatura</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Nº Parcela</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Vecto.Parcela</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Vr.Parcela</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Vecto.Boleto</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Vr.Boleto</span></b></th>");
            sbTabelaParcelas.AppendFormat($"</tr>");
            sbTabelaParcelas.AppendFormat($"</thead>");

            var tbody_tr_td = "border-top:none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;background:white;padding:3.75pt 3.0pt 3.75pt 3.0pt;text-align: center;";
            var tbody_tr_td_span = "font-size:9.0pt; font-family: Verdana ,sans-serif";
            sbTabelaParcelas.AppendFormat($"<tbody>");
            sbTabelaParcelas.AppendFormat($"<tr>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:solid #666666 1.0pt;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{DbRotinas.Descriptografar(boleto.ContratoParcela.Contrato.NumeroContrato)}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{boleto.NumeroDocumento}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{boleto.ContratoParcela.NumeroParcela}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{boleto.ContratoParcela.DataVencimento:dd/MM/yyyy}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{boleto.ValorDocumentoAberto:C2}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{boleto.DataVencimento:dd/MM/yyyy}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{boleto.ValorDocumento:C2}</span></td>");
            sbTabelaParcelas.AppendFormat($"</tr>");
            sbTabelaParcelas.AppendFormat($"</tbody>");

            sbTabelaParcelas.AppendFormat($"</table>");

            return sbTabelaParcelas.ToString();
        }
        [HttpPost]
        public JsonResult RemoverSolicitacao(string b = null)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                var id = DbRotinas.ConverterParaInt(b);
                if (id > 0)
                {
                    var entity = _dbBoleto.FindById(id);
                    entity.Status = (short)Enumeradores.StatusBoleto.Cancelado;
                    entity.DataStatus = DateTime.Now;
                    entity.DataExcluido = DateTime.Now;
                    entity.CodUsuarioStatus = WebRotinas.ObterUsuarioLogado().Cod;
                    _dbBoleto.Update(entity);

                    op = new OperacaoRetorno() { OK = true, Mensagem = $"Operação realizada com sucesso!" };
                }
            }
            catch (Exception ex)
            {

                op = new OperacaoRetorno(ex);
            }

            return Json(op);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Resultado()
        {
            var filtro = WebRotinas.FiltroGeralObter();
            var user = WebRotinas.ObterUsuarioLogado();
            Nullable<int> usuarioId = null;
            string filiais = null;
            short? tipoFluxo = null;
            if (user.PerfilTipo == Enumeradores.PerfilTipo.Analista)
            {
                tipoFluxo = (short)Enumeradores.TipoFluxo.AnalistaBoleto;
            }
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista)
            {
                tipoFluxo = (short)Enumeradores.TipoFluxo.SupervisorBoleto;
            }
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional || user.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                {
                    usuarioId = user.Cod;
                }
                tipoFluxo = (short)Enumeradores.TipoFluxo.FilialBoleto;
                var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == user.Cod && !x.DataExcluido.HasValue);
                filiais = string.Join(",", regionais.Select(x => x.CodRegional).ToArray());
            }
            var resultadoParcelaBoleto = _qry.ObterTotaisBoletoSolicitacao(usuarioId, tipoFluxo, filiais);
            return View("Resultado", resultadoParcelaBoleto);
        }
        [HttpPost]
        public ActionResult Resultado(FiltroGeralDados filtro)
        {
            WebRotinas.FiltroGeralGravar(filtro);
            var user = WebRotinas.ObterUsuarioLogado();
            Nullable<int> usuarioId = null;
            string filiais = null;
            short? tipoFluxo = null;
            if (user.PerfilTipo == Enumeradores.PerfilTipo.Analista)
                tipoFluxo = (short)Enumeradores.TipoFluxo.AnalistaBoleto;
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista)
                tipoFluxo = (short)Enumeradores.TipoFluxo.SupervisorBoleto;
            else if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional || user.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                if (user.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                    usuarioId = user.Cod;
                tipoFluxo = (short)Enumeradores.TipoFluxo.FilialBoleto;
                var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == user.Cod && !x.DataExcluido.HasValue);
                filiais = string.Join(",", regionais.Select(x => x.CodRegional).ToArray());
            }
            var resultadoParcelaBoleto = _qry.ObterTotaisBoletoSolicitacao(usuarioId, tipoFluxo, filiais);
            return View(resultadoParcelaBoleto);
        }


        [HttpPost]
        public JsonResult GetBoletoDataByServer(Cobranca.Helpers.Models.DTParameters param, int? CodSintese, int? CodFase, int? CodSinteseHome)
        {
            try
            {
                var filter = WebRotinas.FiltroGeralObter();
                if (CodSintese == null || CodSintese == 0)
                    CodSintese = filter.CodSinteseBoleto;

                //CASO O USUARIO FOR DA FILIAL DEVE FILTRAR APENAS SEUS BOLETOS SOLICITADOS
                var usuario = WebRotinas.ObterUsuarioLogado();
                Nullable<int> usuarioId = null;
                string filiais = "";
                if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                {
                    usuarioId = usuario.Cod;
                    var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == usuario.Cod && !x.DataExcluido.HasValue);
                    filiais = string.Join(",", regionais.Select(x => x.CodRegional).ToArray());
                }
                else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
                {
                    var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == usuario.Cod && !x.DataExcluido.HasValue);
                    filiais = string.Join(",", regionais.Select(x => x.CodRegional).ToArray());
                }

                #region definirFluxo
                //VERIFICAR O TIPO DE FLUXO E VERIRICAR SE O USUARIO PODE EDITAR
                IQueryable<object> data = null;
                var fluxo = (short)0;
                if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Analista)
                {
                    fluxo = (short)Enumeradores.TipoFluxo.AnalistaBoleto;
                }
                else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista)
                {
                    fluxo = (short)Enumeradores.TipoFluxo.SupervisorBoleto;
                }
                else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional || usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
                {
                    fluxo = (short)Enumeradores.TipoFluxo.FilialBoleto;
                }
                #endregion

                var dtresult = _repBoleto.GetBoletosFromListResult(fluxo, usuarioId, CodFase, CodSintese, filiais, param);

                return Json(dtresult);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
    }
}
