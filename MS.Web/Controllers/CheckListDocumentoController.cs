﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Cobranca.Web.Controllers
{


    public class CheckListDocumentoController : BasicController<TabelaCheckList>
    {
        public const string _nmFiltro = "filtroListaCheckListId";

        public Repository<TabelaCheckList> _dbTabelaCheckList = new Repository<TabelaCheckList>();
        public Repository<TabelaTipoDocumento> _dbTabelaTipoDocumento = new Repository<TabelaTipoDocumento>();
        public Repository<TabelaCheckListNome> _dbTabelaCheckListNome = new Repository<TabelaCheckListNome>();
        public Repository<TabelaBanco> _dbBanco = new Repository<TabelaBanco>();
        ListasRepositorio _dbListas = new ListasRepositorio();

        [HttpGet]
        public override ActionResult Index()
        {
            WebRotinas.CookieRemover(_nmFiltro);
            return RedirectToAction("Lista");

        }

        public override void carregarCustomViewBags()
        {
            var listaTabelaCheckListNome = _dbTabelaCheckListNome.All().ToList();
            listaTabelaCheckListNome.Insert(0, new TabelaCheckListNome());
            ViewBag.checkListNome = from p in listaTabelaCheckListNome select new DictionaryEntry(p.CheckListNome, p.Cod);

            ViewBag.ListaTipoDocumento = _dbListas.TipoDocumento().OrderBy(x => x.Value);
            ViewBag.ListaBanco = _dbListas.Banco().OrderBy(x => x.Value);
            ViewBag.ListaEstadoCivil = EnumeradoresLista.EstadoCivil();
            ViewBag.ListaCategoriaProfissional = EnumeradoresLista.CategoriaProfissional();
            PreencherListas();
            base.carregarCustomViewBags();
        }

        [HttpGet]
        public ActionResult Lista()
        {
            CheckListConsultaViewModel filtro = WebRotinas.CookieObjetoLer<CheckListConsultaViewModel>(_nmFiltro);
            ViewBag.ListaChecklist = FiltrarDados(filtro);
            PreencherListas();

            carregarCustomViewBags();
            return View(filtro);
        }

        [HttpPost]
        public ActionResult Lista(CheckListConsultaViewModel filtro)
        {
            ViewBag.ListaChecklist = FiltrarDados(filtro);
            PreencherListas();
            carregarCustomViewBags();
            WebRotinas.CookieObjetoGravar(_nmFiltro, filtro);

            return View(filtro);
        }


        private List<TabelaCheckList> FiltrarDados(CheckListConsultaViewModel filtro)
        {
            List<TabelaCheckList> listaTabelaCheckList = new List<TabelaCheckList>();

            listaTabelaCheckList = _dbTabelaCheckList.All().Where(x => !x.TabelaCheckListNome.DataExcluido.HasValue).ToList();

            if (filtro.codCheckListNome > 0)
            {
                listaTabelaCheckList = listaTabelaCheckList.Where(c => !c.DataExcluido.HasValue && c.CodCheckListNome == filtro.codCheckListNome).ToList();
            }

            if (filtro.codBanco > 0)
            {
                listaTabelaCheckList = listaTabelaCheckList.Where(c => !c.DataExcluido.HasValue && c.CodBanco == filtro.codBanco).ToList();
            }
            if (filtro.estadoCivil > 0)
            {
                listaTabelaCheckList = listaTabelaCheckList.Where(c => !c.DataExcluido.HasValue && c.EstadoCivil == filtro.estadoCivil).ToList();
            }
            if (filtro.categoriaProfissional > 0)
            {
                listaTabelaCheckList = listaTabelaCheckList.Where(c => !c.DataExcluido.HasValue && c.CategoriaProfissional == filtro.categoriaProfissional).ToList();
            }
            if (filtro.fgts.HasValue)
            {
                listaTabelaCheckList = listaTabelaCheckList.Where(c => !c.DataExcluido.HasValue && c.Fgts == filtro.fgts).ToList();
            }
            if (filtro.cartaoCredito.HasValue)
            {
                listaTabelaCheckList = listaTabelaCheckList.Where(c => !c.DataExcluido.HasValue && c.CartaoCredito == filtro.cartaoCredito).ToList();
            }
            if (filtro.aplicacaoFinanceira.HasValue)
            {
                listaTabelaCheckList = listaTabelaCheckList.Where(c => !c.DataExcluido.HasValue && c.AplicacaoFinanceira == filtro.aplicacaoFinanceira).ToList();
            }
            if (filtro.aluguel.HasValue)
            {
                listaTabelaCheckList = listaTabelaCheckList.Where(c => !c.DataExcluido.HasValue && c.Aluguel == filtro.aluguel).ToList();
            }

            if (filtro.obrigatorio.HasValue)
            {
                listaTabelaCheckList = listaTabelaCheckList.Where(c => !c.DataExcluido.HasValue && c.Obrigatorio == filtro.obrigatorio).ToList();
            }
     
            return listaTabelaCheckList;
        }

        private void PreencherListas()
        {
            var listaTabelaCheckListNome = _dbTabelaCheckListNome.All().ToList();
            listaTabelaCheckListNome.Insert(0, new TabelaCheckListNome());
            ViewBag.ListaTabelaCheckListNome = from p in listaTabelaCheckListNome select new DictionaryEntry(p.CheckListNome, p.Cod);

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "", Value = "" });
            items.Add(new SelectListItem { Text = "Nao", Value = "false" });
            items.Add(new SelectListItem { Text = "Sim", Value = "true" });

            ViewBag.SimNao = from p in items select new DictionaryEntry(p.Text, p.Value);

            List<SelectListItem> iObg = new List<SelectListItem>();
            iObg.Add(new SelectListItem { Text = "Nao", Value = "false" });
            iObg.Add(new SelectListItem { Text = "Sim", Value = "true" });

            ViewBag.Obrigatorio = from p in iObg select new DictionaryEntry(p.Text, p.Value);
        }


        public override ActionResult Cadastro(TabelaCheckList dados)
        {
            return base.Cadastro(dados);
        }

    }
}