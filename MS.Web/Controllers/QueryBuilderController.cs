﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Inputs;
using Cobranca.Domain.CobrancaContext.Handlers;
using Cobranca.Infra.CobrancaContext.Repositories;
using Cobranca.Rotinas.QueryBuilder;
using Cobranca.Shared.Commands;
using Cobranca.Web.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web.Mvc;

namespace Cobranca.Web.Views.QueryBuilder
{
    public class QueryBuilderController : Controller
    {
        private readonly ContratoParcelaRepository _repository;
        private readonly ContratoParcelaHandler _handler;
        private readonly RotaRepository _rotaRepository;
        private readonly ProdutoRepository _produtoRepository;
        private readonly TabelaGrupoRepository _grupoRepository;

        public QueryBuilderController(ContratoParcelaRepository repository, ContratoParcelaHandler handler, RotaRepository rotaRepository, ProdutoRepository produtoRepository, TabelaGrupoRepository grupoRepository)
        {
            _repository = repository;
            _handler = handler;
            _rotaRepository = rotaRepository;
            _produtoRepository = produtoRepository;
            _grupoRepository = grupoRepository;
        }


        [HttpGet]
        public PartialViewResult Index()
        {
            QueryBuilderViewModel model = new QueryBuilderViewModel();

            var teste = _repository.GetByFluxoPagamento("2483802");
            var teste2 = _repository.Get("2483802");
            CreateContratoParcelaCommand command = new CreateContratoParcelaCommand();
            command.FluxoPagamento = "2483802";

            var result = (ICommandResult)_handler.Handle(command);

            List<QueryBuilderRuleModel> rules = QueryBuilderRotinas.ConvertClassToQueryBuilderRules(new ParametroAtendimentoVM());


            ViewBag.Rules = JsonConvert.SerializeObject(rules, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            return PartialView(model);
        }

        [HttpGet]
        public JsonResult GetListByParametroAtendimento()
        {
            var produtos = _produtoRepository.Get();
            List<string> ProdutoItens = new List<string>();
            foreach (var item in produtos)
                item.Produto = Db.Rotinas.DbRotinas.Descriptografar(item.Produto);

            foreach (var prod in produtos.OrderBy(x => x.Produto))
                ProdutoItens.Add(string.Format(@"{{""{0}"":""{1}""}}", prod.Cod, prod.Produto.Replace(Environment.NewLine, "").Replace(@"""", @"\""")));

            var grupos = _grupoRepository.GetAll();
            List<string> grupoEmpresaItens = new List<string>();

            foreach (var p in grupos.OrderBy(x => x.Nome))
                grupoEmpresaItens.Add(string.Format(@"{{""{0}"":""{1}""}}", p.Cod, p.Nome.Replace(Environment.NewLine, "").Replace(@"""", @"\""")));

            return Json(new { Success = true, objProduto = ProdutoItens, objGrupoEmpresa = grupoEmpresaItens }, JsonRequestBehavior.AllowGet);
        }

      

        [HttpGet]
        public JsonResult GetListaRota()
        {

            var rotas = _rotaRepository.Get();
            List<string> itens = new List<string>();
            foreach (var item in rotas)
            {
                item.Nome = Db.Rotinas.DbRotinas.Descriptografar(item.Nome);
            }

            foreach (var rota in rotas.OrderBy(x => x.Nome))
            {
                itens.Add(string.Format(@"{{""{0}"":""{1}""}}", rota.Cod, rota.Nome.Replace(Environment.NewLine, "").Replace(@"""", @"\""")));
            }
            return Json(itens, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObterConfiguracao()
        {
            QueryBuilderViewModel model = new QueryBuilderViewModel();
            //List<QueryBuilderRuleModel> rules = QueryBuilderRotinas.ConvertClassToQueryBuilderRules(new ParametroAtendimentoVM());
            List<QueryBuilderRuleModel> rules = new List<QueryBuilderRuleModel>();

            var rotas = _rotaRepository.Get();
            rules.Add(new QueryBuilderRuleModel(
                "Rota",
                "Rotas",
                "integer",
                rotas.Select(x => new DictionaryEntry(x.Cod, Db.Rotinas.DbRotinas.Descriptografar(x.Nome))).OrderBy(o => o.Value).ToList(),
                "checkbox",
                new string[] { "in", "not_in" }
            ));
            var produtos = _produtoRepository.Get();
            rules.Add(new QueryBuilderRuleModel(
                "Produto",
                "Produtos",
                "integer",
                produtos.Select(x => new DictionaryEntry(x.Cod, Db.Rotinas.DbRotinas.Descriptografar(x.Produto))).OrderBy(o => o.Value).ToList(),
                "checkbox",
                new string[] { "in", "not_in" }
            ));
            rules.Add(new QueryBuilderRuleModel(
                "Produto",
                "Jurídico ?",
                "integer",
                new List<DictionaryEntry>() { new DictionaryEntry(0, "Sim"), new DictionaryEntry(1, "Não") },
                "checkbox",
                new string[] { "equal" }
            ));



            StringBuilder strJson = new StringBuilder();
            rules.ForEach(x => strJson = strJson.Append($"{x.ToJson()},"));
            strJson.Remove(strJson.Length - 1, 1);

            return Json(new
            {
                OK = true,
                Dados = $"[{strJson}]"
            });
        }



        [HttpGet]
        public PartialViewResult AddGroup()
        {
            QueryBuilderViewModel model = new QueryBuilderViewModel();
            //model.TipoCampo = CarregarInputs(model);

            return PartialView("_group", model);
        }

    }
}