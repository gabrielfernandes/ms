﻿using Resources;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Data;
using System.Web.UI.WebControls;
using Cobranca.Web.Models;
using JunixValidator;

namespace Cobranca.Web.Controllers
{
    [Authorize]
    public abstract class BasicController<T> : Controller
    where T : BaseEntity
    {

        public Repository<T> _repository = new Repository<T>();

        public Expression<Func<T, bool>> _where = null;


        [Authorize]
        [HttpGet]
        public virtual ActionResult Index()
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();
            var listaDados = new List<T>();

            carregarCustomViewBags();
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            if (_where != null)
            {
                listaDados = _repository.All().Where(_where).OrderBy(x => x.Cod).ToList();
            }
            else
                listaDados = _repository.All().OrderBy(x => x.Cod).ToList();

            return View(listaDados);
        }
        [Authorize]
        [HttpGet]
        public virtual ActionResult Cadastro(int id = 0)
        {
            carregarCustomViewBags();

            var dados = _repository.FindById(id);
            if (id == 0)
            {
                dados = Activator.CreateInstance<T>();
                dados.DataCadastro = DateTime.Now;
            }
            return View(dados);
        }

        [HttpPost]
        public virtual ActionResult Cadastro(T dados)
        {
            validacaoCustom(dados);
            carregarCustomViewBags();

            if (ModelState.IsValid)
            {
                try
                {
                    dados = _repository.CreateOrUpdate(dados);
                    ViewBag.MensagemOK = "Operacao realizada com sucesso";//Language.MensagemOK;
                    Ok(dados, Request);
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var validationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            ModelState.AddModelError(validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }

                }
            }
            return View(dados);
        }

        [HttpGet]
        public ActionResult Form(int id = 0)
        {
            var dados = _repository.FindById(id);
            if (id == 0)
            {
                dados = Activator.CreateInstance<T>();
                dados.DataCadastro = DateTime.Now;
            }
            carregarCustomViewBags();
            return PartialView("_form", dados);
        }

        [HttpGet]
        public FileResult DownloadExcel()
        {
            string filename = WebRotinas.ObterArquivoTemp();
            FileInfo info = new FileInfo(filename);
            IQueryable<T> dados = _repository.All().OrderBy(x => x.Cod);
            var nmArquivo = carregarListaParaExcel(dados, filename);
            nmArquivo = nmArquivo + DateTime.Now.ToString("ddMMyy") + ".csv";
            return File(info.FullName, "application/force-download", nmArquivo);
        }

        [HttpPost]
        public JsonResult Excluir(int id = 0)
        {
            _repository.Delete(id);

            return Json(new { OK = true });
        }

        public virtual void carregarCustomViewBags()
        {

        }
        public virtual void validacaoCustom(T entity)
        {

        }
        public virtual void Ok(T dados, HttpRequestBase Request)
        {

        }

        public virtual string carregarListaParaExcel(IQueryable<T> Dados, string filename)
        {
            return String.Empty;
        }
        [HttpPost]
        public JsonResult DataHandler(DTParameters param)
        {
            try
            {
                var dtsource = carregarListaDados();

                var result = WebRotinas.DTRotinas.DTResult(param, dtsource);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        public virtual IQueryable<object> carregarListaDados()
        {
            return _repository
                      .All()
                      .ToList().AsQueryable();
        }

    }
}