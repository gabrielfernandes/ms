﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas;
using Cobranca.Domain.CobrancaContext.Commands.ParametroAtendimentoCommands.Inputs;
using Cobranca.Domain.CobrancaContext.Handlers;
using Cobranca.Infra.CobrancaContext.Repositories;
using Cobranca.Shared.Commands;

namespace Cobranca.Web.Controllers
{
    [Authorize]
    public class ParametroAtendimentoController : Controller
    {
        public const string _filtroSessionId = "FiltroPesquisaId";
        public Repository<UsuarioRegional> _dbUsuarioRegionais = new Repository<UsuarioRegional>();
        public Repository<ParametroAtendimento> _dbParametroAtendimento = new Repository<ParametroAtendimento>();
        public Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        public Repository<Rota> _dbRota = new Repository<Rota>();
        public Repository<RotaRegional> _dbRotaRegional = new Repository<RotaRegional>();
        public Repository<TabelaRegionalProduto> _dbRegionalProduto = new Repository<TabelaRegionalProduto>();
        public Repository<TabelaProduto> _dbProduto = new Repository<TabelaProduto>();


        private readonly ParametroAtendimentoRepository _repository;
        private readonly ParametroAtendimentoHandler _handler;

        public ParametroAtendimentoController(ParametroAtendimentoRepository repository, ParametroAtendimentoHandler handler)
        {
            _repository = repository;
            _handler = handler;
        }


        [HttpGet]
        public JsonResult GetParam(int usuario = 0, int regional = 0)
        {
            var parametro = _repository.GetParametroByRegionalAndUsuario(regional, usuario);

            return Json(new {OK = true, Data = parametro.Query }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult FormRota(int usuario = 0, int regional = 0)
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();
            ParametroAtendimento dados = Activator.CreateInstance<ParametroAtendimento>();

            List<int> checkeds = new List<int>();
            if (_repository.CheckExist(regional, usuario))
            { 
                var parametro = _repository.GetParametroByRegionalAndUsuario(regional, usuario);
                if (parametro.QueryRota != null)
                    checkeds = parametro.QueryRota.Split(',').Select(x => DbRotinas.ConverterParaInt(x)).ToList();
            }
            ViewBag.Checkds = checkeds;

            var listaRotas = _dbRota.FindAll(x => !x.DataExcluido.HasValue).ToList();
            foreach (var item in listaRotas)
            {
                item.Nome = DbRotinas.Descriptografar(item.Nome);
                if (string.IsNullOrEmpty(item.Nome))
                {
                    item.Nome = "BRANCA";
                }
            }
            listaRotas = listaRotas.OrderBy(x => DbRotinas.ConverterParaInt(x.Nome)).ToList();

            //var listaRotas = ObterRotas(CodRegional);
            ViewBag.Rotas = listaRotas;
            return PartialView("_formRota", dados);
        }

        private List<DictionaryEntry> GetListRegionais()
        {
            List<TabelaRegional> listaRegionais = _dbRegional.All().ToList();

            foreach (var item in listaRegionais.ToList())
                item.Regional = DbRotinas.Descriptografar(item.Regional);

            if (WebRotinas.ObterUsuarioLogado().Admin != true)
            {
                var regionalIds = WebRotinas.ObterRegionailIdsDoUsuarioLogado();
                listaRegionais = listaRegionais.Where(x => regionalIds.Contains(x.Cod)).ToList();
            }
            return (from p in listaRegionais.OrderBy(x => x.Regional) select new DictionaryEntry(p.Cod, p.Regional)).ToList();
        }

        private List<DictionaryEntry> GetListUsuariosByRegional(int idRegional)
        {
            List<Usuario> usuariosQueryable = _dbUsuarioRegionais.All().Where(x => x.CodRegional == idRegional).Select(u => u.Usuario).ToList();
            return (from p in usuariosQueryable.OrderBy(x => x.Nome) select new DictionaryEntry(p.Cod, p.Nome)).ToList();
        }

        [HttpGet]
        public ActionResult Index(int CodRegional = 0)
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();

            ParametroAtendimentoVM dados = new ParametroAtendimentoVM();
            dados.ListRegionais = GetListRegionais().ToList();


            if (CodRegional > 0)
                dados.ListRegionais = dados.ListRegionais.Where(x => x.Key.ToString() == CodRegional.ToString()).ToList();

            return View(dados);
        }

        [HttpGet]
        public ActionResult FormUsuario(int CodRegional = 0)
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();

            ParametroAtendimentoVM dados = new ParametroAtendimentoVM();
            var regionais = GetListRegionais();
            var regionalSelecionada = regionais.Where(x => x.Key.ToString() == CodRegional.ToString()).FirstOrDefault();
            dados.CodRegional = (int)regionalSelecionada.Key;
            dados.Regional = (string)regionalSelecionada.Value;

            ViewBag.ModelParametro = _dbParametroAtendimento.All().ToList();
            dados.ListUsuarios = GetListUsuariosByRegional(CodRegional);

            //PREENCHE AS ROTAS CADASTRADAS PARA A FILIAL
            var listaRotas = ObterRotas(CodRegional);
            ViewBag.Rotas = from p in listaRotas select new DictionaryEntry(p.Nome, p.Cod);
            return PartialView("_formUsuario", dados);
        }

        [HttpPost]
        public JsonResult SalvarParametrizacao(RegisterParametroAtendimentoCommand command)
        {
            var result = (CommandResult)_handler.Handle(command);
            return Json(result);
        }

        public List<Rota> ObterRotas(int CodRegional = 0)
        {
            var usuario = WebRotinas.ObterUsuarioLogado();
            var regionalIds = WebRotinas.ObterRegionailIdsDoUsuarioLogado();
            var listaRotas = _dbRota.FindAll(x => !x.DataExcluido.HasValue);
            List<int> rotasRegionalIds = new List<int>();
            if (CodRegional > 0) // FILTRA POR UMA ROTA ESPECIFICA
            {
                rotasRegionalIds.Add(CodRegional);
            }
            else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional || usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                // FILTRA POR ROTAS DO USUARIO
                rotasRegionalIds = _dbRotaRegional.FindAll(x => regionalIds.Contains((int)x.CodRegional) && !x.DataExcluido.HasValue).Select(x => (int)x.CodRota).ToList();
            }
            //TRAZ AS ROTAS DA FILIAL
            var rotas = listaRotas.ToList();
            if (rotasRegionalIds.Count > 0)
            {
                rotas = rotas.Where(x => rotasRegionalIds.Contains(x.Cod)).ToList();
            }
            foreach (var item in rotas)
            {
                item.Nome = DbRotinas.Descriptografar(item.Nome);
            }
            rotas = rotas.OrderBy(x => x.Nome).ToList();
            return rotas;
        }
    }
}