﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using System.Collections;
using Cobranca.Db.Rotinas;
using Recebiveis.Web.Models;
using System.Globalization;
using System.IO;
using Cobranca.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class ClienteParcelaCreditoController : BasicController<ClienteParcelaCredito>
    {
        public const string _filtroSessionId = "FiltroCreditoId";
        public Repository<ClienteParcelaCredito> _dbCredito = new Repository<ClienteParcelaCredito>();
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();
        public Repository<TabelaUnidade> _dbUnidade = new Repository<TabelaUnidade>();


        [HttpGet]
        public ActionResult cliente()
        {
            return base.Index();

        }

        [HttpGet]
        public ActionResult credito()
        {
            return base.Index();

        }

        public override void carregarCustomViewBags()
        {
            //junix_cobrancaContext con = new junix_cobrancaContext();
            var listaContrutoras = _dbConstrutora.All();
            var listaEmpreendimentos = _dbEmpreendimento.All();
            ViewBag.ListaConstrutora = from p in listaContrutoras.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);
            ViewBag.listaEmpreendimento = from p in listaEmpreendimentos.Where(x => x.CodConstrutora == x.CodConstrutora).ToList() select new DictionaryEntry(p.NomeEmpreendimento, p.Cod);
            base.carregarCustomViewBags();
        }

        [HttpPost]
        public ActionResult paginarDados(DTParameters param = null)
        {
            var dados = _dbCredito.All().Where(x => x.CodCliente > 0).ToList();

            var lista = (from p in dados
                         select new
                         {
                             CodCliente = DbRotinas.Descriptografar(p.Cliente.CodCliente),
                             Nome = DbRotinas.Descriptografar(p.Cliente.Nome),
                             CPF = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(p.Cliente.CPF)),
                             NumeroDocumento = p.NumeroDocumento,
                             Observacao = p.Observacao,
                             DataPagamento = $"{p.DataPagamento: dd/MM/yyyy}",
                             ValorRecebido = $"{p.ValorRecebido:C2}",
                             ValorSaldo = $"{p.SaldoCredito:C2}"
                         }).AsQueryable();

            var result = WebRotinas.DTRotinas.DTResult(param, lista);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult paginarDadosCredito(DTParameters param = null)
        {
            var dados = _dbCredito.All().Where(x => x.CodCliente == null).ToList();
            var lista = (from p in dados
                     select new
                     {
                         NumeroDocumento = p.NumeroDocumento,
                         Observacao = p.Observacao,
                         DataPagamento = $"{p.DataPagamento: dd/MM/yyyy}",
                         ValorRecebido = $"{p.ValorRecebido:C2}",
                         ValorSaldo = $"{p.SaldoCredito:C2}"
                     }).AsQueryable();

            var result = WebRotinas.DTRotinas.DTResult(param, lista);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public FileResult DownloadExcelCreditoComCliente()
        {
            string filename = WebRotinas.ObterArquivoTemp();
            FileInfo info = new FileInfo(filename);
            var userLogado = WebRotinas.ObterUsuarioLogado();
            var CodUsuarioLogado = userLogado.Cod;
            var dados = _dbCredito.All().Where(x => x.CodCliente > 0).ToList();
            var lista = (from p in dados
                         select new
                         {
                             Nome = DbRotinas.Descriptografar(p.Cliente.Nome),
                             CPF = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(p.Cliente.CPF)),
                             NumeroDocumento = p.NumeroDocumento,
                             Observacao = p.Observacao,
                             DataPagamento = $"{p.DataPagamento: dd/MM/yyyy}",
                             ValorRecebido = $"{p.ValorRecebido:C2}"
                         }).AsQueryable();
            ConverterListaParaExcel.GerarCSVDeGenericList(lista.ToList(), filename);
            var nmarquivo = "creditosvinculadocliente" + DateTime.Now.ToString("ddMMyy") + ".csv";
            return File(info.FullName, "application/force-download", nmarquivo);
        }

        [Authorize]
        public FileResult DownloadExcelCreditoSemCliente(DTParameters param = null)
        {
            string filename = WebRotinas.ObterArquivoTemp();
            FileInfo info = new FileInfo(filename);
            var userLogado = WebRotinas.ObterUsuarioLogado();
            var CodUsuarioLogado = userLogado.Cod;
            var dados = _dbCredito.All().Where(x => x.CodCliente == null).ToList();
            var lista = (from p in dados
                         select new
                         {
                             NumeroDocumento = p.NumeroDocumento,
                             Observacao = p.Observacao,
                             DataPagamento = $"{p.DataPagamento: dd/MM/yyyy}",
                             ValorRecebido = $"{p.ValorRecebido:C2}"
                         }).AsQueryable();
            ConverterListaParaExcel.GerarCSVDeGenericList(lista.ToList(), filename);
            var nmarquivo = "creditosnaovinculados" + DateTime.Now.ToString("ddMMyy") + ".csv";
            return File(info.FullName, "application/force-download", nmarquivo);
        }

        [HttpPost]
        public JsonResult ObterDadosCreditos(short tipo)
        {
            var lista = _dbCredito.All().ToList();
            if (tipo == 1)
            {
                lista = lista.Where(x => x.CodCliente > 0).ToList();
            }
            else
            {
                lista = lista.Where(x => x.CodCliente == null).ToList();

            }

            var g = from p in lista
                    group p by new { year = p.DataPagamento.Value.Year, month = p.DataPagamento.Value.Month } into d
                    orderby d.Key.year
                    select new
                    {
                        date = new DateTime(d.Key.year, d.Key.month, 1),
                        valor = d.Sum(x => x.ValorRecebido)
                    };
            var gp = from p in g
                     group p by new { year = p.date.Year } into d
                     orderby d.Key.year
                     select new
                     {
                         name = $"{d.Key.year}",
                         data = d.Select(x => x.valor).ToArray()
                     };
            return Json(gp);
        }

    }
}