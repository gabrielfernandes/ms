﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.IO;
using System.Web.UI;
using System.Data.OleDb;
using System.Data.SqlClient;
using Recebiveis.Web.Models;
using Cobranca.Rotinas;
using Excel;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Cobranca.Web.Controllers
{
    public class ImportacaoBoletoController : Controller
    {
        private Repository<Contrato>
        _dbContrato = new Repository<Contrato>();

        private Repository<Importacao>
        _dbImportacao = new Repository<Importacao>();

        public Dados _qry = new Dados();
        public const string _filtroSessionId = "FiltroPesquisaId";




        //string[] arquivo;
        List<string> arquivo = new List<string>();

        [HttpGet]
        public ActionResult Import()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ImportLista()
        {
            //var processamentos = _qry.ListarImportacao((short)Enumeradores.TipoImportacao.Parcela);
            var processamentos = _dbImportacao.All().Where(x => x.Tipo == (int)Enumeradores.TipoImportacao.Boleto).ToList();
            return View(processamentos);
        }
        [HttpPost]
        public JsonResult LerArquivo(string[] file)
        {

            try
            {
                CarregarArquivoParaBanco(file);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        [HttpPost]
        public JsonResult ObterTotais(int[] IdImportacao)
        {

            try
            {
                var processamentos = _dbImportacao.All().Where(x => x.Tipo == 2).ToList();
                var objRetorno = from r in processamentos
                                 select new
                                 {
                                     r.Cod,
                                     r.TotalRegistros,
                                     r.TotalRegistrosProcessado,
                                     r.TotalRegistrosErro,
                                     r.TotalRegistrosOK,
                                     r.Mensagem
                                 };

                return Json(new { OK = true, Dados = objRetorno.ToList() });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        private FileInfo GetFile(string file)
        {
            string pathUpload = Server.MapPath("~/Content/files");
            string pathDefinitivo = Server.MapPath(string.Format("~/Content/files/{0}", file));
            return new FileInfo(pathDefinitivo);
        }


        public void CarregarArquivoParaBanco(string[] files)
        {
            foreach (var file in files)
            {
                FileInfo File = GetFile(file);
                var dt = ConvertTXTtoDataTable(File.Name, File.FullName);

                var colunasDePara = new List<ColunaDePara>();
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Companhia", NomeColunaTabela = "Filial" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "NumerodaFatura", NomeColunaTabela = "NumeroFatura" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Numerodocliente", NomeColunaTabela = "NumeroCliente" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "datavencimento", NomeColunaTabela = "DataVencimento" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "valorfatura", NomeColunaTabela = "ValorFatura" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "linhadigitavel", NomeColunaTabela = "LinhaDigitavel" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "datadocumento", NomeColunaTabela = "DataDocumento" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "endereço", NomeColunaTabela = "Endereco" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numero", NomeColunaTabela = "Numero" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "complemento", NomeColunaTabela = "Complemento" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "bairro", NomeColunaTabela = "Bairro" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cep", NomeColunaTabela = "CEP" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "cidade", NomeColunaTabela = "Cidade" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "estado", NomeColunaTabela = "Estado" });

                ImportacaoDb.BulkCopy(dt, "ImportacaoBoleto", colunasDePara);

            }

            int codUsuarioLogado = WebRotinas.ObterUsuarioLogado().Cod;
            var op = ImportacaoDb.ImportacaoBoletoObterStatus(codUsuarioLogado, 0, String.Join("/", files));
            if (op.OK)
            {
                int CodImportacao = DbRotinas.ConverterParaInt(op.Dados);
                new Thread(delegate ()
                {
                    ImportacaoDb.ImportacaoBoletoProcessar(CodImportacao);
                }).Start();


                //Thread thread = new Thread(() => executar(CodImportacao));
                //thread.IsBackground = true;
                //thread.Start();
            }

        }

        public static DataTable ConvertTXTtoDataTable(string strFileName, string strFilePath)
        {
            DataTable dt = new DataTable();
            Int32 c = 0;
            try
            {
                using (StreamReader sr = new StreamReader(strFilePath, System.Text.Encoding.Default, false, 512))
                {
                    string[] headers = new string[] { };
                    string linhaHader = sr.ReadLine();
                    if (linhaHader.Split('\t').Length <= 1)
                    {
                        headers = linhaHader.Split(new Char[] { ';' });
                    }
                    else
                    {
                        headers = linhaHader.Split(new Char[] { '\t' });
                    }
                    foreach (string header in headers)
                    {
                        if (dt.Columns.Count <= 13)
                        {
                            dt.Columns.Add(header.Trim());
                        }
                    }

                    while (true)
                    {
                        DataRow dr = dt.NewRow();
                        string linhatexto = sr.ReadLine();
                        if (linhatexto == null)
                        {
                            break;
                        }
                        string[] quebra = new string[] { };
                        if (linhatexto.Split('\t').Length <= 1)
                        {
                            quebra = linhatexto.Split(new Char[] { ';' });
                        }
                        else
                        {
                            quebra = linhatexto.Split(new Char[] { '\t' });
                        }
                        //remove a ultima coluna
                        if (quebra.Count() == 15)
                        {
                            quebra = quebra.Take(quebra.Count() - 1).ToArray();
                        }
                        for (int i = 0; i < quebra.Count(); i++)
                        {
                            quebra[i] = quebra[i].Replace(";", "").Replace("\"", "");
                            //VERIFICA OS CAMPOS PARA OFUSCAR OS DADOS ANTES DA IMPORTACAO
                            if (i <= 13)
                            {
                                if (i == 2)
                                {
                                    dr[i] = DbRotinas.Criptografar(quebra[i].Trim());
                                }
                                else
                                {
                                    if (i == 3 || i == 6)
                                    {
                                        dr[i] = DbRotinas.ConverterParaDatetimeOuNull(quebra[i].Trim());
                                    }
                                    else
                                    {
                                        dr[i] = quebra[i].Trim();
                                    }
                                }
                            }


                        }
                        c = c + 1;

                        dt.Rows.Add(dr);
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(strFilePath, System.Text.Encoding.Default, false, 512))
            {
                string[] headers = sr.ReadLine().Split(new Char[] { ';' });

                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(';');
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        [HttpPost]
        public JsonResult Excluir(int id)
        {
            try
            {
                _dbImportacao.Delete(id);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        public FileResult ExtrairResultadoImportacaoBoleto(int CodImportacao = 0, short Status = 0)
        {
            var lista = ImportacaoDb.ListarResultadoPorBoleto(CodImportacao, Status);

            string pathFile = Path.Combine(Path.GetTempPath(), Path.GetTempFileName());
            using (TextWriter tw = System.IO.File.CreateText(pathFile))
            {
                tw.WriteLine("Cod\tCompanhia\tNumeroFatura\tNumeroCliente\tdatavencimento\tvalorfatura\tlinhadigitavel\tdatadocumento\tendereco\tnumero\tcomplemento\tbairro\tcep\tcidade\testado\tStatus\tMensagem\tResultado");
                foreach (var item in lista)
                {
                    tw.WriteLine(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9}",
                        item.Cod
                      , item.Filial
                      , item.NumeroFatura
                      , DbRotinas.Descriptografar(item.NumeroCliente)
                      , DbRotinas.ConverterParaDatetime(item.DataVencimento)
                      , item.ValorFatura
                      , item.LinhaDigitavel
                      , item.Status
                      , item.Mensagem
                      , item.Resultado
                    ));
                }
                tw.Close();
            }

            return File(pathFile, "text/csv", "resultado.csv");

        }

        public FileResult LayoutBoleto(int CodImportacao = 0, short Status = 0)
        {


            StringBuilder sb = new StringBuilder();
            sb.AppendLine("companhia;numerofatura;numerocliente;datavencimento;valorfatura;linhadigitavel;");

            return File(new System.Text.UTF8Encoding().GetBytes(sb.ToString()), "text/csv", "layout-boleto.csv");

        }

        [HttpPost]
        public JsonResult Resultado(int CodImportacao, short Status)
        {
            var lista = ImportacaoDb.ListarResultadoPorBoleto(CodImportacao, Status);
            return Json(lista);
        }

        [HttpPost]
        public async Task<JsonResult> Processar(int CodImportacao)
        {
            ImportacaoDb.ImportacaoBoletoProcessar(CodImportacao);
            return Json(new { OK = true });
        }
    }

}
