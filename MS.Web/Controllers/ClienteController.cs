﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Recebiveis.Web.Controllers
{
    public class ClienteController : Controller
    {
        Repository<ClienteContato> _dbClienteContato = new Repository<ClienteContato>();
        Repository<Cliente> _dbCliente = new Repository<Cliente>();
        Repository<PropostaProspect> _dbPropostaProspect = new Repository<PropostaProspect>();

        Repository<ClienteEndereco> _dbClienteEndereco = new Repository<ClienteEndereco>();
        Repository<ContratoAvalista> _dbContratoAvalista = new Repository<ContratoAvalista>();
        Repository<ContratoCliente> _dbContratoCliente = new Repository<ContratoCliente>();
        Repository<TabelaEndereco> _dbEndereco = new Repository<TabelaEndereco>();
        Repository<ClienteContatoEmpresarial> _dbClienteContatoEmpresarial = new Repository<ClienteContatoEmpresarial>();
        public Repository<TabelaTipoCadastro> _dbTabelaTipoCadastro = new Repository<TabelaTipoCadastro>();


        [HttpPost]
        public JsonResult SalvarCliente(PropostaProspect dados)
        {
            try
            {
                Repository<PropostaProspect> _dbProspect = new Repository<PropostaProspect>();
                var prospect = _dbPropostaProspect.FindAll(x => x.CodProposta == dados.CodProposta).OrderByDescending(c => c.Ordem).FirstOrDefault().Ordem;
                dados.Ordem = dados.Ordem > 0 ? prospect + 1 : 1;
                _dbProspect.CreateOrUpdate(dados);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        private void ValidaEstadoCivil(int id = 0, short EstadoCivil = 0)
        {
            if (EstadoCivil == (short)Cobranca.Db.Models.Enumeradores.EstadoCivil.Casado)
            {
                if (_dbPropostaProspect.FindAll(x => x.CodConjuge == id).Count() < 0)
                {
                    ViewBag.Critica = "Favor cadastrar o conjuge!";
                }
            }

        }
        [HttpPost]
        public JsonResult ValidaCriticas(int id = 0, string EstadoCivil = "")
        {
            StringBuilder validacao = new StringBuilder();
            try
            {
                var dados = _dbCliente.FindAll(x => (x.Cod == id));
                if (dados.Count() > 0)
                {
                    if (dados.Where(x => x.EstadoCivil == "2" && !x.CodClienteConjuge.HasValue).Count() > 0)
                    {
                        var conjuge = _dbCliente.FindAll(x => x.CodClienteConjuge == id);
                        if (conjuge.Count() == 0)
                        {
                            validacao.Append(dados.FirstOrDefault().Nome);
                            validacao.Append("<br/>");
                            validacao.Append("Conjuge não cadastrado.");
                        }
                    }
                }
                return Json(new { OK = true, Validacao = validacao.ToString() });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }





        }

        [HttpPost]
        public JsonResult ObterTipoCadastro()
        {
            try
            {
                var lista = new List<DictionaryEntry>();
                var ListaContatoCadastro = _dbTabelaTipoCadastro.All().ToList();
                foreach (var item in ListaContatoCadastro)
                {
                    lista.Add(new DictionaryEntry() { Key = item.Cod, Value = item.Descricao });
                }
                return Json(new { OK = true, Dados = lista });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    OK = false,
                    Mensagem = ex.Message
                });
            }
        }
        [HttpPost]
        public ActionResult SalvarContato(ClienteViewModel.ClienteContatoViewModel models)
        {
            try
            {
                ClienteViewModel ClienteVM = new ClienteViewModel();
                ClienteVM.Contatos = new List<ClienteViewModel.ContatoViewModel>();
                for (int i = 0; i < models.Cod.Length; i++)
                {
                    ClienteVM.Contatos.Add(new ClienteViewModel.ContatoViewModel(models.Cod[i], models.CodCliente[i], models.Contato[i], models.ContatoHOT[i], models.Descricao[i], models.Tipo[i]));
                }

                List<ClienteContato> _ClienteContato = new List<ClienteContato>();
                foreach (var contato in ClienteVM.Contatos)
                {
                    var id = DbRotinas.ConverterParaInt(contato.Cod);
                    var dados = _dbClienteContato.FindById(id);
                    if (id == 0)
                    {
                        dados = Activator.CreateInstance<ClienteContato>();
                        dados.DataCadastro = DateTime.Now;
                    }
                    dados.Cod = id;
                    dados.CodCliente = DbRotinas.ConverterParaInt(contato.CodCliente);
                    //if ((int)DbRotinas.ConverterParaInt(contato.CodTipoCadastro) > 0)
                    //{
                    //    dados.CodTipoCadastro = (int)DbRotinas.ConverterParaInt(contato.CodTipoCadastro);
                    //}
                    dados.Tipo = DbRotinas.ConverterParaShort(contato.Tipo);
                    dados.Contato = DbRotinas.Criptografar(contato.Contato);
                    dados.ContatoHOT = DbRotinas.ConverterParaBool(contato.ContatoHOT);
                    dados.Descricao = contato.Descricao;
                    _dbClienteContato.CreateOrUpdate(dados);
                }

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult SalvarContatoHot(int ContatoId)
        {
            try
            {
                ClienteContato contato = _dbClienteContato.FindById(ContatoId);

                List<ClienteContato> ultimoHot = _dbClienteContato.All().Where(x => x.CodCliente == contato.CodCliente && x.ContatoHOT == true && x.Tipo == contato.Tipo).ToList();
                foreach (var i in ultimoHot)
                {
                    i.ContatoHOT = false;
                    _dbClienteContato.CreateOrUpdate(i);
                }

                contato.Cod = ContatoId;
                contato.ContatoHOT = true;
                contato.ContatoHOTCodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                contato.ContatoHOTData = DateTime.Now;
                _dbClienteContato.CreateOrUpdate(contato);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult SalvarClienteContato(ClienteContatoEmpresarial dados)
        {
            try
            {
                _dbClienteContatoEmpresarial.CreateOrUpdate(dados);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult ExcluirContatoCliente(int id = 0)
        {
            try
            {
                _dbClienteContatoEmpresarial.Delete(id);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult SalvarEnderecoHot(int EnderecoId)
        {
            try
            {
                ClienteEndereco endereco = _dbClienteEndereco.FindById(EnderecoId);
                List<ClienteEndereco> ultimoHot = _dbClienteEndereco.All().Where(x => x.CodCliente == endereco.CodCliente && x.EnderecoHOT == true).ToList();
                foreach (var i in ultimoHot)
                {
                    i.EnderecoHOT = false;
                    _dbClienteEndereco.CreateOrUpdate(i);
                }

                endereco.Cod = EnderecoId;
                endereco.EnderecoHOT = true;
                endereco.EnderecoHOTCodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                endereco.EnderecoHOTData = DateTime.Now;
                _dbClienteEndereco.CreateOrUpdate(endereco);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult SalvarEndereco(int Cod, int CodEndereco, int CodCliente, String Tipo, String Endereco, String Numero, String Complemento, String Bairro, String Cidade, String Estado, String UF, String CEP)
        {
            try
            {

                var cliente = _dbCliente.FindById(CodCliente);


                var clienteEndereco = new ClienteEndereco();// = _dbClienteContato.FindById(CodCliente);
                clienteEndereco.TabelaEndereco = new TabelaEndereco();
                clienteEndereco.TabelaEndereco.Cod = CodEndereco;
                clienteEndereco.DataCadastro = DateTime.Now;
                clienteEndereco.TabelaEndereco.Logradouro = Tipo;
                clienteEndereco.TabelaEndereco.Endereco = Endereco;
                clienteEndereco.TabelaEndereco.Numero = Numero;
                clienteEndereco.TabelaEndereco.Complemento = Complemento;
                clienteEndereco.TabelaEndereco.Bairro = Bairro;
                clienteEndereco.TabelaEndereco.Cidade = Cidade;
                clienteEndereco.TabelaEndereco.Estado = Estado;
                clienteEndereco.TabelaEndereco.UF = UF;
                clienteEndereco.TabelaEndereco.CEP = CEP;
                clienteEndereco.TabelaEndereco.DataCadastro = DateTime.Now;

                var entity = _dbEndereco.CreateOrUpdate(clienteEndereco.TabelaEndereco);

                clienteEndereco.Cod = Cod;
                clienteEndereco.CodEndereco = entity.Cod;
                clienteEndereco.CodCliente = CodCliente;
                _dbClienteEndereco.CreateOrUpdate(clienteEndereco);

                _dbCliente.Update(cliente);


                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult SalvarAvalista(ContratoAvalista dados)
        {
            try
            {
                _dbContratoAvalista.CreateOrUpdate(dados);
                if (dados.EstadoCivil == 2)
                {
                    ContratoAvalista conjuge = new ContratoAvalista();
                    conjuge.NomeAvalista = Request.Form["NomeConjuge"];
                    conjuge.CPF = Request.Form["CPFConjuge"];
                    conjuge.CodConjuge = dados.Cod;
                    conjuge.CodContrato = dados.CodContrato;
                    _dbContratoAvalista.CreateOrUpdate(conjuge);
                    var avalista = _dbContratoAvalista.FindById(dados.Cod);
                    avalista.CodConjuge = conjuge.Cod;
                    _dbContratoAvalista.CreateOrUpdate(avalista);
                }
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        public JsonResult ConsultaConjuge(int CodCliente = 0)
        {
            var nome = "";
            var cpf = "";
            try
            {
                var dados = _dbContratoAvalista.FindAll(x => x.CodConjuge == CodCliente);
                if (dados.Count() > 0)
                {
                    nome = dados.FirstOrDefault().NomeAvalista;
                    cpf = dados.FirstOrDefault().CPF;
                }

                return Json(new { OK = true, Nome = nome, CPF = cpf });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        public JsonResult ValidaEstadoCivil(int CodProspect = 0)
        {
            StringBuilder validacao = new StringBuilder();
            try
            {
                var dados = _dbPropostaProspect.FindAll(x => (x.Cod == CodProspect) && x.EstadoCivil == 2 && !x.CodConjuge.HasValue);
                if (dados.Count() > 0)
                {
                    var conjuge = _dbPropostaProspect.FindAll(x => x.CodConjuge == CodProspect);
                    if (conjuge.Count() < 0)
                    {
                        validacao.Append(dados.FirstOrDefault().Nome);
                        validacao.Append("<br/>");
                        validacao.Append("Conjuge não cadastrado.");
                    }
                }

                return Json(new { OK = true, Validacao = validacao.ToString() });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }


        [HttpPost]
        public JsonResult ListarCliente(int CodCliente = 0, int CodContrato = 0)
        {
            try
            {
                List<String[]> ListaCliente = new List<String[]>();
                var contrato = _dbContratoCliente.FindAll(x => x.CodCliente == CodCliente && x.CodContrato == CodContrato).FirstOrDefault();
                var cliente = _dbCliente.FindAll(x => x.Cod == CodCliente).FirstOrDefault();
                if (cliente != null)
                {

                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Cod) });
                    ListaCliente.Add(new String[] { Convert.ToString(contrato.Tipo) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.PessoaTipo) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.CodCliente) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Nome) });


                    var DataNascimento = DbRotinas.ConverterParaDatetimeOuNull(cliente.DataNascimento);
                    ListaCliente.Add(new String[] { String.Format("{0:dd/MM/yyyy}", Convert.ToString(DataNascimento)).Replace("00:00:00", "") });

                    ListaCliente.Add(new String[] { Convert.ToString(cliente.CPF) });

                    ListaCliente.Add(new String[] { Convert.ToString(cliente.TelefoneResidencial) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.TelefoneComercial) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Celular) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Email) });

                    ListaCliente.Add(new String[] { Convert.ToString(cliente.CEP) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Tipo) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Endereco) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.EnderecoNumero) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.EnderecoComplemento) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Bairro) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Cidade) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.UF) });

                    ListaCliente.Add(new String[] { Convert.ToString(cliente.EstadoCivil) });

                }

                return Json(new { OK = true, InfoCliente = ListaCliente });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ListarProspect(int CodCliente = 0)
        {
            try
            {
                List<String[]> ListaCliente = new List<String[]>();
                var cliente = _dbPropostaProspect.FindAll(x => x.Cod == CodCliente).FirstOrDefault();
                if (cliente != null)
                {
                    ListaCliente.Add(new String[] { cliente.Nome });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.DataNascimento) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Sexo) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.EstadoCivil) });
                    ListaCliente.Add(new String[] { cliente.CPF });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.TipoDocumento) });
                    ListaCliente.Add(new String[] { cliente.RG });
                    ListaCliente.Add(new String[] { cliente.Orgao });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.DataEmissao) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.CategoriaProfissional) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Profissao) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.NumeroDependente) });
                    ListaCliente.Add(new String[] { cliente.Cartorio });
                    ListaCliente.Add(new String[] { cliente.NomePai });
                    ListaCliente.Add(new String[] { cliente.NomeMae });
                    ListaCliente.Add(new String[] { cliente.Nacionalidade });
                    ListaCliente.Add(new String[] { cliente.Naturalidade });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Escolaridade) });
                    ListaCliente.Add(new String[] { cliente.TelefoneResidencial });
                    ListaCliente.Add(new String[] { cliente.TelefoneComercial });
                    ListaCliente.Add(new String[] { cliente.Celular });
                    ListaCliente.Add(new String[] { cliente.Email });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.CEP) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Tipo) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Endereco) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.EnderecoNumero) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.EnderecoComplemento) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Bairro) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Cidade) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.UF) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Cod) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.Ordem) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.CodConjuge) });
                    ListaCliente.Add(new String[] { Convert.ToString(cliente.TipoComprador) });

                }

                return Json(new { OK = true, InfoCliente = ListaCliente });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult SalvarConjuge(PropostaProspect dados)
        {
            try
            {
                var CodProspect = DbRotinas.ConverterParaInt(Request.Form["NomeCliente"]);
                var conjuge = _dbPropostaProspect.FindById(CodProspect);

                var qtd = _dbPropostaProspect.FindAll(x => x.CodConjuge == CodProspect);
                if (qtd.Count() > 0)
                {
                    return Json(new { OK = true, Conjuge = "Já existe conjuge cadastrado!" });
                }
                else
                {
                    dados.CodConjuge = DbRotinas.ConverterParaInt(Request.Form["NomeCliente"]);
                    dados.Ordem = conjuge.Ordem;
                    _dbPropostaProspect.CreateOrUpdate(dados);


                    conjuge.CodConjuge = dados.Cod;
                    conjuge.DataAlteracao = DateTime.Now;
                    _dbPropostaProspect.Update(conjuge);
                    return Json(new { OK = true, Conjuge = "Conjuge cadastrado com sucesso!" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult SalvarConjugeCobranca(Cliente dados)
        {
            try
            {
                var CodCliente = DbRotinas.ConverterParaInt(Request.Form["CodigoCliente"]);
                var conjuge = _dbCliente.FindById(CodCliente);
                var CodContrato = DbRotinas.ConverterParaInt(Request.Form["CodContrato"]);

                var qtd = _dbCliente.FindAll(x => x.CodClienteConjuge == CodCliente);
                if (qtd.Count() > 0)
                {
                    return Json(new { OK = true, Conjuge = "Já existe conjuge cadastrado!" });
                }
                else
                {
                    dados.CodClienteConjuge = CodCliente;
                    var ordem = _dbContratoCliente.FindAll(x => x.CodContrato == CodContrato && x.CodCliente == CodCliente).FirstOrDefault().Ordem;
                    _dbCliente.CreateOrUpdate(dados);
                    VinculoComprador(dados.Cod, (int)ordem, CodContrato, DbRotinas.ConverterParaShort(Request.Form["Tipo"]));
                    conjuge.CodClienteConjuge = dados.Cod;
                    conjuge.DataAlteracao = DateTime.Now;
                    _dbCliente.Update(conjuge);
                    return Json(new { OK = true, Conjuge = "Conjuge cadastrado com sucesso!" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        public JsonResult AlterarProspect(PropostaProspect dados)
        {
            try
            {
                Repository<PropostaProspect> _dbPP = new Repository<PropostaProspect>();
                var CodCliente = DbRotinas.ConverterParaInt(Request.Form["CodCliente"]);
                var prospect = _dbPropostaProspect.FindById(CodCliente);
                if (prospect.TipoComprador == (short)Enumeradores.CompradorTipo.Conjuge && dados.TipoComprador == (short)Enumeradores.CompradorTipo.Comprador)
                {

                    var ultimaOrdem = _dbPropostaProspect.FindAll(x => x.CodProposta == dados.CodProposta).OrderByDescending(c => c.Ordem).FirstOrDefault().Ordem + 1;
                    var VinculoConjuge = _dbPropostaProspect.FindById(DbRotinas.ConverterParaInt(prospect.CodConjuge));

                    VinculoConjuge.CodConjuge = null;
                    _dbPropostaProspect.CreateOrUpdate(VinculoConjuge);

                    dados.Cod = CodCliente;
                    dados.CodConjuge = null;
                    dados.Ordem = ultimaOrdem;
                    _dbPP.CreateOrUpdate(dados);
                }
                else
                {
                    dados.Cod = CodCliente;
                    _dbPP.CreateOrUpdate(dados);
                }
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        public JsonResult AlterarOrdem(OrdemViewModel[] dados)
        {
            try
            {

                foreach (var item in dados)
                {
                    int id = item.id;
                    int ordem = item.ordem;

                    var prospect = _dbPropostaProspect.FindById(id);
                    prospect.Ordem = ordem + 1;
                    if (prospect.CodConjuge.HasValue)
                    {
                        Repository<PropostaProspect> _dbPP = new Repository<PropostaProspect>();
                        var conjuge = _dbPP.FindById(DbRotinas.ConverterParaInt(prospect.CodConjuge));
                        conjuge.Ordem = ordem + 1;
                        _dbPP.CreateOrUpdate(conjuge);
                    }
                    _dbPropostaProspect.CreateOrUpdate(prospect);
                }


                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpPost]
        public JsonResult ExcluirContato(int id = 0)
        {
            Repository<ClienteContato> repoContato = new Repository<ClienteContato>();
            repoContato.Delete(id);

            return Json(new { OK = true });
        }

        [HttpPost]
        public JsonResult ExcluirEndereco(int id = 0)
        {
            Repository<ClienteEndereco> repoClienteEndereco = new Repository<ClienteEndereco>();
            Repository<TabelaEndereco> repoTabelaEndereco = new Repository<TabelaEndereco>();
            int idTabelaEndereco = DbRotinas.ConverterParaInt(_dbClienteEndereco.FindById(id).CodEndereco);

            repoTabelaEndereco.Delete(idTabelaEndereco);
            repoClienteEndereco.Delete(id);


            return Json(new { OK = true });
        }

        //---------------------------------------------------//
        [HttpPost]
        public JsonResult SalvarClienteCobranca(Cliente dados)
        {
            try
            {
                dados.CodCliente = DbRotinas.Criptografar(dados.CodCliente);
                dados.Nome = DbRotinas.Criptografar(dados.Nome);
                dados.CPF = DbRotinas.Criptografar(dados.CPF);
                dados.Email = DbRotinas.Criptografar(dados.Email);
                dados.Celular = DbRotinas.Criptografar(dados.Celular);
                dados.CelularComercial = DbRotinas.Criptografar(dados.CelularComercial);
                dados.CelularPrincipal = DbRotinas.Criptografar(dados.CelularPrincipal);
                dados.TelefoneComercial = DbRotinas.Criptografar(dados.TelefoneComercial);
                dados.TelefoneResidencial = DbRotinas.Criptografar(dados.TelefoneResidencial);

                var dadosSalvo = _dbCliente.CreateOrUpdate(dados);
                return Json(new { OK = true, Cod = dadosSalvo.Cod });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult SalvaVinculoComprador(int CodCliente = 0, int CodContrato = 0, short Tipo = 0)
        {
            try
            {
                ContratoCliente CC = new ContratoCliente();
                var dados = _dbContratoCliente.FindAll(x => x.CodContrato == CodContrato).OrderByDescending(z => z.DataCadastro).FirstOrDefault();
                CC.CodCliente = CodCliente;
                CC.CodContrato = CodContrato;
                CC.Tipo = Tipo;
                CC.Ordem = dados.Ordem > 0 ? dados.Ordem + 1 : 1;
                _dbContratoCliente.CreateOrUpdate(CC);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult AlteraContatoHot(bool Status, int Cod = 0, int usuario = 0)
        {
            try
            {
                ClienteContato CC = new ClienteContato();
                var dados = _dbClienteContato.FindAll(x => x.Cod == Cod).OrderByDescending(z => z.DataCadastro).FirstOrDefault();
                dados.ContatoHOT = Status;
                dados.ContatoHOTCodUsuario = usuario;
                _dbClienteContato.CreateOrUpdate(dados);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        private void VinculoComprador(int CodCliente = 0, int Ordem = 0, int CodContrato = 0, short Tipo = 0)
        {

            ContratoCliente CC = new ContratoCliente();
            var dados = _dbContratoCliente.FindAll(x => x.CodContrato == CodContrato).OrderByDescending(z => z.DataCadastro).FirstOrDefault();
            CC.CodCliente = CodCliente;
            CC.CodContrato = CodContrato;
            CC.Tipo = Tipo;
            CC.Ordem = Ordem > 0 ? Ordem : 1;
            _dbContratoCliente.CreateOrUpdate(CC);

        }

        [HttpGet]
        public JsonResult PesquisarClientes(string q)
        {
            q = DbRotinas.Descriptografar(q.ToUpper());
            var lista = _dbCliente.All().Where(x => x.Nome.Contains(q)).Take(30).ToList();

            return Json(new
            {
                items = from p in lista select new { id = p.Cod, text = DbRotinas.Descriptografar(p.Nome) + " - " + DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(p.CPF)) }
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ObterCodClienteTags(string query)
        {
            var q = DbRotinas.Descriptografar(query.ToUpper());
            var lista = _dbCliente.All().Where(x => x.CodCliente.Contains(q)).Take(30).ToList();
            return Json(new { items = from p in lista select new { value = p.Cod, text = DbRotinas.Descriptografar(p.Nome) + " - " + DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(p.CPF)), codcliente = p.Cod } }, JsonRequestBehavior.AllowGet);
        }

    }
}
