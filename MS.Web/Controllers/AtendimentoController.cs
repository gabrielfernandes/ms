﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Rotinas;
using Cobranca.Rotinas;
using Recebiveis.Web.Models;
using Cobranca.Db.Rotinas;
using System.Threading.Tasks;
using Cobranca.Web.Models;
using System.Collections;
using Cobranca.Rotinas.BoletoGerar;
using Cobranca.Notificacao.EMAIL;
using System.Text;
using Cobranca.Notificacao.Email;
using Cobranca.Rotinas.Modelos;
using BoletoNet;
using System.Data.Entity.Core.Objects;
using System.Text.RegularExpressions;
using System.IO;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Domain.CobrancaContext.Commands.CartaCobrancaCommands;
using Cobranca.Infra.BoletoContext.Repositories;
using Cobranca.Domain.CobrancaContext.Handlers;

namespace Cobranca.Web.Controllers
{
    public class AtendimentoController : BasicController<TabelaBanco>
    {

        private readonly CartaCobrancaHandler _cartaHandler;
        private readonly CartaCobrancaRepository _repCarta;

        public AtendimentoController(CartaCobrancaHandler cartaHandler,CartaCobrancaRepository repCarta)
        {
            _cartaHandler = cartaHandler;
            _repCarta = repCarta;
        }

        public const string _filtroSessionId = "FiltroPesquisaId";
        private EmailRotinas _objEmailRotinas;
        private ParametroPlataforma _objParametro;
        public Repository<ParametroPlataforma> _dbParametro = new Repository<ParametroPlataforma>();
        public const string _filtroAtendimento = "FiltroAtendimento";
        public const string _filtroCodClienteAtendimento = "FiltroCodClienteAtendimento";
        public const string _filtroCodCliente = "FiltroCodCliente";
        Repository<TabelaModeloAcao> _dbTabelaModeloAcao = new Repository<TabelaModeloAcao>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<TabelaParametroTaxa> _dbParametroTaxa = new Repository<TabelaParametroTaxa>();
        public Repository<LogEmail> _dbLogEmail = new Repository<LogEmail>();
        public Repository<ContratoParcela> _dbContratoParcela = new Repository<ContratoParcela>();
        public Repository<TabelaTipoCobrancaAcao> _dbTipoCobrancaAcao = new Repository<TabelaTipoCobrancaAcao>();
        public Repository<ContratoCliente> _dbContratoCliente = new Repository<ContratoCliente>();
        public Repository<TabelaParametroAtendimento> _dbTabelaParametroAtendimento = new Repository<TabelaParametroAtendimento>();
        public Repository<ParametroAtendimento> _dbParametroAtendimento = new Repository<ParametroAtendimento>();
        public Repository<ParametroAtendimentoRota> _dbParametroAtendimentoRota = new Repository<ParametroAtendimentoRota>();
        public Repository<RotaRegional> _dbRotaRegional = new Repository<RotaRegional>();
        public Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        public Repository<TabelaRegionalProduto> _dbRegionalProduto = new Repository<TabelaRegionalProduto>();
        public Repository<TabelaRegionalGrupo> _dbRegionalGrupo = new Repository<TabelaRegionalGrupo>();
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<ClienteContato> _dbClienteContato = new Repository<ClienteContato>();
        public Repository<TabelaFase> _dbFase = new Repository<TabelaFase>();
        public Repository<TabelaSintese> _dbSintese = new Repository<TabelaSintese>();
        public Repository<TabelaMotivoAtraso> _dbMotivoAtraso = new Repository<TabelaMotivoAtraso>();
        public Repository<ClienteAtendimento> atendimento = new Repository<ClienteAtendimento>();
        public Repository<TabelaRegional> _dbFilial = new Repository<TabelaRegional>();
        public Repository<Usuario> _dbUsuario = new Repository<Usuario>();
        public Repository<UsuarioRegional> _dbUsuarioRegional = new Repository<UsuarioRegional>();
        public Repository<Historico> _dbHistorico = new Repository<Historico>();
        public Repository<ContratoAcaoCobrancaHistorico> _dbAcaoCobrancaHistorico = new Repository<ContratoAcaoCobrancaHistorico>();

        public Dados _qry = new Dados();

        ListasRepositorio _dbListas = new ListasRepositorio();

        [HttpGet]
        public ActionResult FilaFinalizada()
        {
            return View();
        }



        [HttpGet]
        public ActionResult Fila()
        {
            //PREENCHER SelectListRegional
            var _user = WebRotinas.ObterUsuarioLogado();

            IQueryable<Usuario> listUsuario = _dbUsuario.All().Where(x => x.PerfilTipo == (short)Enumeradores.PerfilTipo.Regional || x.PerfilTipo == (short)Enumeradores.PerfilTipo.RegionalAdmin);
            if (_user.PerfilTipo == Enumeradores.PerfilTipo.Regional)
            {
                listUsuario.Where(x => x.Cod == _user.Cod);
            }
            else if (_user.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == _user.Cod && !x.DataExcluido.HasValue).Select(x => x.CodRegional).ToList();
                var usuarios = _dbUsuarioRegional.All().Where(x => regionais.Contains(x.CodRegional)).Select(x => x.CodUsuario).ToList();

                listUsuario.Where(x => usuarios.Contains(x.Cod));
            }
            ViewBag.UsuarioAtendente = listUsuario.ToList();

            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Cliente(int CodCliente = 0)
        {

            Repository<Contrato> contrato = new Repository<Contrato>();
            Repository<Cliente> cliente = new Repository<Cliente>();
            DeParaModeloAcao depara = new DeParaModeloAcao();
            Dados _sql = new Dados();

            if (CodCliente == 0) { CodCliente = WebRotinas.CookieObjetoLer<int>(_filtroCodCliente); }
            if (CodCliente == 0) { return RedirectToAction("FilaFinalizada"); }
            var script = _sql.ConsultaScript(CodCliente);

            ViewBag.ScriptAtendimento = script.Count() > 0 ? depara.DeParaCampos(script.FirstOrDefault().Modelo, CodCliente) : Idioma.Traduzir("Não há script para este tipo de cobrança ou faixa de atraso");
            var dados = _dbCliente.FindById(CodCliente);
            if (dados == null) { dados = new Cliente(); }

            var taxas = _dbParametroTaxa.All().ToList().FirstOrDefault();

            try
            {
                List<TabelaFase> listaTabelaFase = new List<TabelaFase>();
                listaTabelaFase = _dbFase.FindAll(x => x.TabelaSintese.Any(c => c.TarefaAtendimento == true && !c.DataExcluido.HasValue)).ToList();
                ViewBag.ListaFase = listaTabelaFase;

                ViewBag.ListaMotivoAtraso = _dbListas.MotivoAtraso();
                ViewBag.ListaResolucao = _dbListas.Resulucao();

                List<TabelaSintese> listaTabelaSintese = new List<TabelaSintese>();
                listaTabelaSintese = _dbSintese.All().ToList();
                ViewBag.ListaTodasSintese = listaTabelaSintese.Where(c => c.TarefaAtendimento == true && !c.DataExcluido.HasValue).OrderBy(x => x.Sintese).ToList();

                List<TabelaMotivoAtraso> listaTabelaMotivoAtraso = new List<TabelaMotivoAtraso>();
                listaTabelaMotivoAtraso = _dbMotivoAtraso.All().ToList();
                ViewBag.ListaTodosMotivos = listaTabelaMotivoAtraso.OrderBy(x => x.MotivoAtraso).ToList();


                var codUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                var codUserRegional = (int)WebRotinas.ObterUsuarioLogado().CodRegional;
                //CRIA UMA LISTA DE ROTAS DEFINIDAS PARA O USUARIO
                List<Rota> listaRotas = new List<Rota>();
                List<ParametroAtendimentoRota> ParametroRotasQueryable = _dbParametroAtendimentoRota.All().Where(x => x.ParametroAtendimento.CodUsuario == codUsuario).ToList();
                if (ParametroRotasQueryable.Count > 0)
                {
                    listaRotas = (List<Rota>)ParametroRotasQueryable.ToList().Select(x => x.Rota).ToList();
                    //VERIFICA SE O USUARIO TEM A TODAS ROTAS.
                    List<int> rotasCod = listaRotas.Select(x => x.Cod).ToList();
                    if (rotasCod.Contains(115))
                    {
                        //SE POSSUIR TRAZ TODAS ROTAS DA SUA REGIONAL
                        listaRotas = _dbRotaRegional
                            .FindAll(x => !x.DataExcluido.HasValue && x.CodRegional == codUserRegional)
                            .ToList()
                            .Select(x => x.Rota)
                            .ToList();
                    }
                }
                List<string> rotas = listaRotas.Select(x => x.Nome).ToList();


                //FILTRA OS CONTRATOS DESTA ROTA
                List<Contrato> listaContratos = new List<Contrato>();
                listaContratos = _dbContrato.FindAll(x => !x.DataExcluido.HasValue && x.CodClientePrincipal == CodCliente).ToList();
                var usuarioLogado = WebRotinas.ObterUsuarioLogado();

                if (usuarioLogado.CodRegional > 0)
                {
                    //MUDANÇA NA REGRA -- USUARIO PODE VER CONTRATOS DE OUTRA REGIONAL
                    //if (usuarioLogado.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                    //{
                    //    listaContratos = listaContratos.Where(x => rotas.Contains(x.RotaArea)).ToList();
                    //}
                    listaContratos = (from p in listaContratos
                                      where p.TabelaRegionalProduto.CodRegional == usuarioLogado.CodRegional
                                      select p).ToList();
                }

                var contratoIds = listaContratos.Select(c => c.Cod).ToList();
                List<ContratoAcaoCobrancaHistorico> listaAcaoCobrancaHistorico = new List<ContratoAcaoCobrancaHistorico>();
                listaAcaoCobrancaHistorico = _dbAcaoCobrancaHistorico.FindAll(x => x.DataExcluido.HasValue && contratoIds.Contains((int)x.CodContrato)).ToList();
                ViewBag.acaoCobranca = listaAcaoCobrancaHistorico.ToList();

                ViewBag.ListaContratos = listaContratos.ToList();


                ViewBag.FilialId = WebRotinas.CookieObjetoLer<int>("FilialId");
                ViewBag.Atendente = WebRotinas.CookieObjetoLer<int>("AtendenteId");

                //var parcelaCliente = _qry.ObterParcelaPorCliente(CodCliente).ToList();
                //List<ContratoParcela> contratoParcela = new List<ContratoParcela>();
                //foreach (var parcelaItem in parcelaCliente)
                //{
                //    contratoParcela.Add(_dbContratoParcela.FindById(parcelaItem.Cod));
                //}

                //ViewBag.parcelasCliente = contratoParcela.ToList();

                //if (dados != null)
                //{
                //    foreach (var item in contratoParcela)
                //    {
                //        _qry.executarAtualizaValorParcela(item.Cod);
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
            return View(dados);
        }

        [HttpGet]
        public ActionResult FilaAtendimento(int Filial = 0, int Atendente = 0)
        {
            //int usuario = DbRotinas.ConverterParaInt(WebRotinas.ObterClienteLogado().Cod);
            Repository<Cliente> dbcliente = new Repository<Cliente>();
            Atendimento objAtendimento = new Atendimento();
            Dados obj = new Dados();

            var clienteAtendimento = objAtendimento.ObterAtendimento(Atendente, Filial);

            if (clienteAtendimento != null)
            {
                WebRotinas.CookieObjetoGravar(_filtroCodClienteAtendimento, clienteAtendimento.CodCliente);
                if (WebRotinas.ObterUsuarioLogado().PerfilTipo != Enumeradores.PerfilTipo.Admin && clienteAtendimento.CodUsuario == WebRotinas.ObterUsuarioLogado().Cod)
                {
                    var cliente = dbcliente.FindById((int)clienteAtendimento.CodCliente);
                    cliente.DataAtendimento = DateTime.Now;
                    cliente.CodUsuarioAtendimento = DbRotinas.ConverterParaInt(WebRotinas.ObterUsuarioLogado().Cod);
                    dbcliente.Update(cliente);
                }


                //Gravar o id da Filial
                WebRotinas.CookieObjetoGravar("FilialId", Filial);
                WebRotinas.CookieObjetoGravar("AtendenteId", Atendente);

                return RedirectToAction("Cliente", new { CodCliente = clienteAtendimento.CodCliente });
            }
            else
            {
                return RedirectToAction("FilaFinalizada");
            }
        }


        [HttpPost]
        public ActionResult ParcelaDados(string contratoId = null, int CodSinteseContrato = 0, int CodSinteseParcela = 0, int tipo = 0, DTParameters param = null)
        {
            var userLogado = WebRotinas.ObterUsuarioLogado();
            var CodUsuarioLogado = userLogado.Cod;
            List<int> CodRegionalUsuario = _dbUsuarioRegional.All().Where(x => x.CodUsuario == CodUsuarioLogado).Select(r => r.CodRegional).ToList();
            var RegionaisUsuario = _dbRegionalGrupo.All().Where(x => CodRegionalUsuario.Contains((int)x.CodRegional)).Select(r => r.CodRegionalCliente).ToList();
            var query_dados = _qry.ListarParcelasPorContrato(contratoId, CodSinteseContrato, CodSinteseParcela).ToList();

            FiltroGeralDados Filtro = WebRotinas.FiltroGeralObter();
            if (Filtro.NumeroFatura != null)
            {
                var nrFaturas = Filtro.NumerosFaturasArray;
                query_dados = query_dados.Where(x => nrFaturas.Any(key => x.FluxoPagamento.Contains(key))).ToList();
            }
            if (Filtro.NotaFiscal != null)
            {
                var nFiscais = Filtro.NotaFiscaisArray;
                query_dados = query_dados.Where(x => nFiscais.Any(key => x.NF.Contains(key))).ToList();
            }

            if (tipo == (short)Enumeradores.FilaAtendimentoTipoParcela.aVencer) { query_dados = query_dados.Where(x => x.DataVencimento >= DateTime.Now.Date).ToList(); }
            if (tipo == (short)Enumeradores.FilaAtendimentoTipoParcela.Atraso) { query_dados = query_dados.Where(x => x.DataVencimento < DateTime.Now.Date && (!x.DataFechamento.HasValue) && x.ValorAberto > 0).ToList(); }
            if (tipo == (short)Enumeradores.FilaAtendimentoTipoParcela.Pago) { query_dados = query_dados.Where(x => x.DataFechamento.HasValue && x.ValorAberto == 0).ToList(); }
            bool IsRegionalUsuario = RegionaisUsuario.Contains("30413") || userLogado.Admin == true ? true : false;//VERIFICA SE A PARCELA ESTA PARA A FILIAL DO USUARIO
            ContratoParcelaDados classe = new ContratoParcelaDados();
            var lista = (from q in query_dados
                         select new
                         {
                             CodCliente = DbRotinas.Descriptografar(q.CodCliente),
                             CodParcela = q.CodParcela,
                             DataEmissao = $"{q.DataEmissaoFatura:dd/MM/yyyy}",
                             DataVencimento = $"{q.DataVencimento:dd/MM/yyyy}",
                             FluxoPagamento = q.FluxoPagamento,
                             NotaFiscal = q.NF,
                             ValorParcela = q.ValorParcela,
                             ValorAberto = q.ValorAberto,
                             ValorParcelaDesc = $"{q.ValorParcela:C}",
                             ValorAbertoDesc = $"{q.ValorAberto:C}",
                             NumeroParcela = q.NumeroParcela,
                             RotaArea = DbRotinas.Descriptografar(q.RotaArea),
                             CodRegionalCliente = q.CodRegionalCliente,
                             Filial = DbRotinas.Descriptografar(q.Filial),
                             DataFechamento = $"{q.DataFechamento:dd/MM/yyyy}",
                             DataPagamento = $"{q.DataPagamento:dd/MM/yyyy}",
                             Status = verificarStatus(q.DataVencimento, q.DataFechamento, q.ValorAberto, q.ValorParcela),
                             Fase = DbRotinas.Descriptografar(q.Fase),
                             NaturezaCliente = q.NaturezaCliente,
                             Sintese = $"{q.CodigoSinteseCliente} - {DbRotinas.Descriptografar(q.Sintese)}",
                             HistoricoObs = DbRotinas.Descriptografar(q.HistoricoObs),
                             IsPago = q.DataFechamento.HasValue && q.ValorAberto == 0 ? true : false,
                             IsRegionalUsuario = RegionaisUsuario.Contains(q.CodRegionalCliente) || userLogado.Admin == true ? true : false,//VERIFICA SE A PARCELA ESTA PARA A FILIAL DO USUARIO
                             PossuiLinhaDigitavel = q.LinhaDigitavel != null ? true : false,
                             TipoFatura = q.TipoFatura,
                             Cia = q.Companhia,
                             Governo = q.Governo,
                             InstrucaoPagamento = q.InstrucaoPagamento,
                             NossoNumero = $"{q.NossoNumero}",
                             Corporativo = q.Corporativo,
                             CodigoEmpresaCobranca = q.CodigoEmpresaCobranca,
                             NumeroContrato = DbRotinas.Descriptografar(q.NumeroContrato),
                             DiasAtraso = q.DiasAtraso,
                             ClienteParcela = DbRotinas.Descriptografar(q.ClienteParcela),
                             ClienteVIP = q.ClienteVIP
                         }).AsQueryable();
            var result = WebRotinas.DTRotinas.DTResult(param, lista);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet, Authorize]
        public JsonResult VerificarAtendimento(int Id)
        {
            try
            {
                var userLogado = WebRotinas.ObterUsuarioLogado();
                var CodUsuarioLogado = userLogado.Cod;
                Dados obj = new Dados();

                var _usuario = WebRotinas.ObterUsuarioLogado();
                var atendeteCod = 0;
                if (_usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                {
                    atendeteCod = _usuario.Cod;
                }
                var listaAcoes = obj.ConsultaCasosAtendimentoPorCliente(atendeteCod, Id);
                var listaAcaoId = listaAcoes.Select(s => s.CodAcaoCobrancaHistorico).ToList();
                List<ContratoAcaoCobrancaHistorico> listaAcaoCobrancaHistorico = new List<ContratoAcaoCobrancaHistorico>();
                DateTime dtNow = DateTime.Now.Date;
                listaAcaoCobrancaHistorico = (from p in _dbAcaoCobrancaHistorico.FindAll()
                                              where !p.DataExcluido.HasValue
                                              && !p.DataEnvio.HasValue
                                              && listaAcaoId.Contains((int)p.Cod)
                                              && (p.DataAtendimentoAgendado < dtNow || !p.DataAtendimentoAgendado.HasValue)
                                              && p.TabelaTipoCobrancaAcao.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.CONTATO
                                              select p).ToList();

                var listaParcelaId = listaAcaoCobrancaHistorico.Select(s => s.CodParcela).ToList();
                var parcelas = _dbContratoParcela.All().Where(x => listaParcelaId.Contains(x.Cod)).Select(x => x.FluxoPagamento).ToList();

                if (parcelas.Count > 0)
                {
                    return Json(new { OK = false, Titulo = "Agendamento", Mensagem = $"As parcelas ({String.Join(",", parcelas)}) possuem ações de contato pendente!" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { OK = true, Mensagem = $"Parcelas livre!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = $"erro: {ex.Message}" }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult FinalizarAtendimento(int id)
        {
            var usuarioId = WebRotinas.ObterUsuarioLogado().Cod;
            //CRIA UMA LISTA DE ROTAS DEFINIDAS PARA O USUARIO
            List<Rota> listaRotas = new List<Rota>();
            List<ParametroAtendimentoRota> ParametroRotasQueryable = _dbParametroAtendimentoRota.All().Where(x => x.ParametroAtendimento.CodUsuario == usuarioId).ToList();
            if (ParametroRotasQueryable.Count > 0)
            {
                listaRotas = (List<Rota>)ParametroRotasQueryable.ToList().Select(x => x.Rota).ToList();
            }
            List<string> rotas = listaRotas.Select(x => x.Nome).ToList();


            //FILTRA OS CONTRATOS DESTA ROTA
            List<Contrato> listaContratos = new List<Contrato>();
            //listaContratos = _dbContrato.FindAll(x => !x.DataExcluido.HasValue && x.CodClientePrincipal == id && rotas.Contains(x.RotaArea)).ToList();
            listaContratos = _dbContrato.FindAll(x => !x.DataExcluido.HasValue && x.CodClientePrincipal == id).ToList();
            List<int> contratoIds = (List<int>)listaContratos.Select(x => x.Cod).ToList();
            junix_cobrancaContext cx = new junix_cobrancaContext();
            var contratos = cx.Contratoes.Where(x => !x.DataExcluido.HasValue && contratoIds.Contains(x.Cod));
            foreach (var c in listaContratos)
            {
                if (c.DataAtendimento < DateTime.Now)
                {
                    c.DataFinalizacao = DateTime.Now;
                    c.CodUsuarioAtendimento = WebRotinas.ObterUsuarioLogado().Cod;
                }
            }
            cx.SaveChanges();
            return RedirectToAction("Fila");
        }
        private string verificarStatus(DateTime? dtVencimento, DateTime? dtFechamento, decimal? vrAberto, decimal? vrParcela)
        {
            var resultado = String.Empty;
            if (dtVencimento >= DateTime.Now && vrAberto > 0)
            {
                resultado = EnumeradoresDescricao.NegociacaoTipoParcela((short)Enumeradores.NegociacaoTipoParcela.aVencer);
            }
            else if (dtVencimento < DateTime.Now && vrAberto > 0)
            {
                resultado = EnumeradoresDescricao.NegociacaoTipoParcela((short)Enumeradores.NegociacaoTipoParcela.Atrasada);
            }
            else if (dtFechamento.HasValue && vrAberto == 0 && vrParcela > 0)
            {
                resultado = EnumeradoresDescricao.NegociacaoTipoParcela((short)Enumeradores.NegociacaoTipoParcela.Paga);
            }
            else if (vrAberto == 0 && vrParcela == 0)
            {
                resultado = EnumeradoresDescricao.NegociacaoTipoParcela((short)Enumeradores.NegociacaoTipoParcela.Cancelada);
            }
            return resultado;

        }

        [HttpPost]
        public JsonResult ConsultarScriptAtendimentoPorParcela(int cod = 0)
        {
            OperacaoRetorno op = new OperacaoRetorno();

            try
            {
                string msgPadrao = "Não há script para este tipo de cobrança ou faixa de atraso";

                var modelo = new ReguaRotinas().ObterScritpAtendimentoPorParcela(cod, WebRotinas.ObterUsuarioLogado().Nome);



                op.OK = modelo.OK;
                op.Dados = string.IsNullOrEmpty(modelo.Mensagem) ? msgPadrao : modelo.Mensagem;
            }
            catch (Exception ex)
            {

                op = new OperacaoRetorno(ex);
            }

            return Json(op);
        }



        [HttpPost]
        public JsonResult ConsultarMsgContrato(string cods = null)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                List<int> codContratos = cods.Split(',').Select(x => DbRotinas.ConverterParaInt(x)).ToList();
                string msgPadrao = "Não há mensagem para este contrato";
                var contratos = _dbContrato.All().Where(x => codContratos.Contains(x.Cod)).ToList();
                op.OK = true;
                StringBuilder row = new StringBuilder();
                foreach (var item in contratos)
                {
                    row.AppendLine($"{item.MensagemContrato}\t");
                }
                op.Dados = string.IsNullOrEmpty(row.ToString()) ? msgPadrao : row.ToString();
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }

        [HttpGet]
        public ActionResult EnvioBoleto(int idCliente = 0, string idParcela = null)
        {
            BoletoUtils gerarBoleto = new BoletoUtils();
            string[] idParcelas = idParcela.Split(',');

            Cliente cliente = _dbCliente.FindById(idCliente);
            var _email = cliente.Email;
            if (cliente.ClienteContatoes != null)
            {
                var contato = cliente.ClienteContatoes.Where(x => x.Tipo == (short)Enumeradores.ContatoTipo.Email && x.ContatoHOT == true && !x.DataExcluido.HasValue).FirstOrDefault();
                _email = contato != null ? contato.Contato : _email;
            }

            var model = new BoletoEnvioViewModel();
            model.BoletoHTML = new List<string>();
            model.ParcelaIds = idParcela;
            model.Email = DbRotinas.Descriptografar(_email);

            int[] ParcelaIds = Array.ConvertAll(idParcelas, delegate (string p) { return int.Parse(p); });

            foreach (var p in ParcelaIds)
            {
                var parcela = _dbContratoParcela.FindById(p);
                if (parcela.LinhaDigitavel != null && parcela.DataVencimento.Date >= DateTime.Now.Date)
                {
                    BoletoUtils.BoletoRetorno objRetornoBoleto = gerarBoleto.GerarBoletoPorLinhaDigitavel(parcela);

                    var html = objRetornoBoleto.Boleto.MontaHtmlEmbedded();
                    model.BoletoHTML.Add(html);
                }
            }

            return PartialView("formEnvio", model);
        }

        [HttpPost]
        public JsonResult ValidarCarta(ValidarGerarCartaCommand command)
        {
            var result = _cartaHandler.Handle(command);
            return Json(result);
        }

        [HttpGet]
        public ActionResult EnvioCarta(int idCliente = 0, string idParcela = null)
        {
            BoletoUtils gerarBoleto = new BoletoUtils();
            string[] idParcelas = idParcela.Split(',');

            Cliente cliente = _dbCliente.FindById(idCliente);
            var _email = cliente.Email;
            if (cliente.ClienteContatoes != null)
            {
                var contato = cliente.ClienteContatoes.Where(x => x.Tipo == (short)Enumeradores.ContatoTipo.Email && x.ContatoHOT == true && !x.DataExcluido.HasValue).FirstOrDefault();
                _email = contato != null ? contato.Contato : _email;
            }

            var model = new BoletoEnvioViewModel();
            model.BoletoHTML = new List<string>();
            model.ParcelaIds = idParcela;
            model.Email = DbRotinas.Descriptografar(_email);

            
            var objAcao = (from p in _dbTabelaModeloAcao.All()
                           where !p.DataExcluido.HasValue &&
                           (p.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.EMAIL
                           || p.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.EMAIL_SEGUNDA_VIA_BOLETO)
                           select p).ToList();

            model.ListaModelosEmail = (from p in objAcao.OrderBy(x => x.Titulo).ToList() select new DictionaryEntry(p.Cod, p.TabelaAcaoCobranca?.AcaoCobranca)).ToList();


            return PartialView("formCarta", model);
        }

        [HttpPost, ValidateInput(false)]
        public JsonResult EnviarBoletoEmail(int CodCliente = 0, string parcelas = null, string email = null, string assunto = null, string mensagem = null)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            List<ContratoParcela> listaParcelas = new List<ContratoParcela>();
            try
            {
                string[] idParcelas = parcelas.Split(',');
                int[] ParcelaIds = Array.ConvertAll(idParcelas, delegate (string p) { return int.Parse(p); });

                List<byte[]> boletos = new List<byte[]>();
                foreach (var p in ParcelaIds)
                {
                    var parcela = _dbContratoParcela.FindById(p);
                    listaParcelas.Add(parcela);

                }
                op = DispararEmailComBoleto(CodCliente, listaParcelas, email, assunto, mensagem);
            }
            catch (Exception ex)
            {

                op = new OperacaoRetorno(ex);
            }

            return Json(op);
        }
        public OperacaoRetorno DispararEmailComBoleto(int CodCliente, List<ContratoParcela> Parcelas, string emailDestinatario, string assunto, string mensagem)
        {
            try
            {
                var objAcao = _dbTabelaModeloAcao.FindAll(x => !x.DataExcluido.HasValue && x.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.EMAIL_SEGUNDA_VIA_BOLETO).FirstOrDefault();
                BoletoUtils gerarBoleto = new BoletoUtils();

                OperacaoRetorno objRetorno = new OperacaoRetorno();

                int codRegional = 0;
                if (Parcelas != null)
                {
                    Contrato contrato = Parcelas.Select(x => x.Contrato).First();
                    if (contrato.CodRegionalProduto > 0)
                    {
                        codRegional = (int)contrato.TabelaRegionalProduto.CodRegional;
                    }
                    if (CodCliente == 0)
                    {
                        CodCliente = (int)contrato.CodClientePrincipal;
                    }
                }
                var objCliente = _dbCliente.FindById(CodCliente);
                codRegional = codRegional > 0 ? codRegional : (int)WebRotinas.ObterUsuarioLogado().CodRegional;
                var objRegional = _dbFilial.FindById(codRegional);
                string emailRegional = WebRotinas.ObterUsuarioLogado().Email ?? "cobflow@junix.com.br";
                _objParametro = _dbParametro.FindAll().FirstOrDefault();
                _objEmailRotinas = new EmailRotinas(_objParametro.SMTPPorta, _objParametro.SMTPServidor, _objParametro.SMTPUsuario, _objParametro.SMTPSenha, _objParametro.fgSSL == true);

                #region Assunto
                var Assunto = "";
                Assunto = objAcao.CodAcaoCobranca > 0 ? objAcao.TabelaAcaoCobranca.AcaoCobranca : "Otis - 2ª via de boleto";
                //INSEIR ASSUNTO
                if (!String.IsNullOrEmpty(assunto))
                    Assunto = assunto;
                #endregion

                #region Destinatario
                List<string> Destinatarios = new List<string>();
                Destinatarios = emailDestinatario.Split(';').Where(x => !string.IsNullOrEmpty(x)).ToList();

                //CASO NO ENVIAR O EMAIL NAO FOR PASSADO DEVERÁ PEGAR O EMAIL NO CADASTRO
                if (Destinatarios.Count == 0)
                    Destinatarios.Add(DbRotinas.Descriptografar(objCliente.Email));
                #endregion

                #region Conteudo
                var Conteudo = "";
                var NomeRemetente = "";
                Conteudo = mensagem;
                if (string.IsNullOrEmpty(mensagem))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(objAcao.Modelo);
                    string tbParcelas = MontarTabelaHtml(Parcelas);
                    sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.TabelaParcelas), tbParcelas);
                    sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NomeComprador), DbRotinas.Descriptografar(objCliente.Nome));
                    sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.CPF_CPNPJ_Comprador), DbRotinas.Descriptografar(objCliente.CPF));
                    if (objRegional != null)
                    {
                        //DADOS DA REGIONAL PRODUTO;
                        sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.DescricaoFilial), DbRotinas.Descriptografar(objRegional.Regional));
                        sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NumeroFilial), objRegional.Telefone);
                        sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.ResponsavelFilial), objRegional.Responsavel);
                    }
                    Conteudo = sb.ToString();
                }

                if (objRegional != null)
                {
                    emailRegional = objRegional.Email;
                    NomeRemetente = objRegional.Responsavel;
                }
                #endregion

                #region Montar Boletos em PDF
                var objEmail = new EmailInfo();
                foreach (var p in Parcelas)
                {
                    EmailInfoArquivo arquivo = new EmailInfoArquivo();
                    if (p.LinhaDigitavel != null)
                    {
                        BoletoUtils.BoletoRetorno objRetornoBoleto = gerarBoleto.GerarBoletoPorLinhaDigitavel(p);

                        var boletoByte = gerarBoleto.MontaBoletoEmBytePDF(objRetornoBoleto.Boleto, "", "", p.FluxoPagamento, false, false);
                        arquivo.Dados = boletoByte;
                        arquivo.NomeArquivo = p.FluxoPagamento + ".pdf";
                        objEmail.Arquivos.Add(arquivo);
                    }
                }
                #endregion


                List<bool> EmailsEnviados = new List<bool>();
                foreach (var destinatario in Destinatarios)
                {
                    LogEmail email = new LogEmail();

                    email.Assunto = Assunto;
                    email.Conteudo = Conteudo;
                    email.EmailRemetente = emailRegional;
                    email.EmailDestinatario = destinatario;
                    email.NomeDestinatario = DbRotinas.Descriptografar(objCliente.Nome);

                    var NotificacaoRetorno = _objEmailRotinas.Enivar(new EmailInfo()
                    {
                        Assunto = email.Assunto,
                        Conteudo = email.Conteudo,
                        EmailRemetente = email.EmailRemetente,
                        EmailDestinatario = email.EmailDestinatario,
                        NomeDestinatario = email.NomeDestinatario != null ? email.NomeDestinatario : "Destinatario",
                        NomeRemetente = email.NomeRemetente != null ? email.NomeRemetente : WebRotinas.ObterUsuarioLogado().Nome,
                        Arquivos = objEmail.Arquivos,
                        HTML = true
                    });

                    //*****STATUS RETORNO******//
                    if (NotificacaoRetorno.OK)
                    {
                        //*****REGISTRA UM HISTORICO NA PARCELAS ******//
                        if (Parcelas != null)
                        {
                            foreach (var parcela in Parcelas)
                            {
                                Historico _hist = new Historico();
                                var clienteId = CodCliente;
                                _hist.CodCliente = clienteId;
                                _hist.CodParcela = parcela.Cod;
                                var msg = $"Destinatário: {destinatario} - Assunto: {email.Assunto}{Environment.NewLine}Conteudo : {email.Conteudo}";
                                _hist.Observacao = StripHTML(msg);
                                _hist.CodSintese = parcela.CodHistorico > 0 ? parcela.Historico.CodSintese : null;
                                _hist.CodMotivoAtraso = parcela.CodHistorico > 0 ? parcela.Historico.CodMotivoAtraso : null;
                                _hist.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                                _dbHistorico.CreateOrUpdate(_hist);
                            }
                        }
                        email.Status = (short)Enumeradores.LogEmailStatus.Enviado;
                        EmailsEnviados.Add(true);
                    }
                    else
                    {
                        email.Status = (short)Enumeradores.LogEmailStatus.FalhaNoEnvio;
                        email.Mensagem = NotificacaoRetorno.Mensagem;
                        EmailsEnviados.Add(false);
                    }

                    //Registra Historico no logEmail
                    RegistraLogEmail(NotificacaoRetorno, email);

                }
                objRetorno.OK = true;
                objRetorno.Mensagem = $"{EmailsEnviados.Where(x => x == true).Count()} e-mail(s) enviado com sucesso!";
                return objRetorno;
            }
            catch (Exception ex)
            {
                return new OperacaoRetorno(ex);
            }

        }

        public void RegistraLogEmail(Notificacao.Suporte.NotificacaoRetorno NotificacaoRetorno, LogEmail email)
        {
            //*******REGISTRA NA TABELA LOGEMAIL******//
            LogEmail _logEmail = new LogEmail();
            _logEmail.EmailRemetente = email.EmailRemetente;
            _logEmail.EmailDestinatario = email.EmailDestinatario;
            _logEmail.NomeDestinatario = email.NomeDestinatario;
            _logEmail.NomeRemetente = email.NomeRemetente;
            _logEmail.Assunto = email.Assunto;
            _logEmail.Conteudo = email.Conteudo;
            _logEmail.HTML = email.HTML;
            _logEmail.Status = (short)Enumeradores.LogEmailStatus.Enviado;
            _logEmail.Mensagem = NotificacaoRetorno.Mensagem;
            _dbLogEmail.CreateOrUpdate(_logEmail);
        }

        private string MontarTabelaHtml(List<ContratoParcela> parcelas)
        {
            var thead_tr_td = "border:solid #666666 1.0pt;background:#CFCFCF;padding:3.75pt 3.0pt 3.75pt 3.0pt;text-align: center;";
            var thead_tr_td_span = "font-size:10.0pt;font-family:Verdana,sans-serif;color:white";
            StringBuilder sbTabelaParcelas = new StringBuilder();

            sbTabelaParcelas.AppendFormat($"<table border='0' cellspacing='0' cellpadding='0' style='width:100%; border-collapse:collapse'>");
            sbTabelaParcelas.AppendFormat($"<thead>");
            sbTabelaParcelas.AppendFormat($"<tr>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Contrato</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Fatura</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Nº Parcela</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Vencimento</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Valor</span></b></th>");
            sbTabelaParcelas.AppendFormat($"</tr>");
            sbTabelaParcelas.AppendFormat($"</thead>");


            var tbody_tr_td = "border-top:none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;background:white;padding:3.75pt 3.0pt 3.75pt 3.0pt";
            var tbody_tr_td_span = "font-size:9.0pt; font-family: Verdana ,sans - serif";
            sbTabelaParcelas.AppendFormat($"<tbody>");
            foreach (var parcela in parcelas)
            {

                //emailRegional = parcela.Contrato.TabelaRegionalProduto.TabelaRegional.Email;
                sbTabelaParcelas.AppendFormat($"<tr>");
                sbTabelaParcelas.AppendFormat($"<td style='border-left:solid #666666 1.0pt;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{DbRotinas.Descriptografar(parcela.Contrato.NumeroContrato)}</span></td>");
                sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{parcela.FluxoPagamento}</span></td>");
                sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{parcela.NumeroParcela}</span></td>");
                sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{parcela.DataVencimento:dd/MM/yyyy}</span></td>");
                sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{parcela.ValorAberto:C2}</span></td>");
                sbTabelaParcelas.AppendFormat($"</tr>");
            }

            sbTabelaParcelas.AppendFormat($"</tbody>");
            sbTabelaParcelas.AppendFormat($"</table>");

            return sbTabelaParcelas.Replace("'", "\"").ToString();
        }

        [HttpGet]
        public ActionResult getMensagem(int idContrato = 0)
        {
            StringBuilder sb = new StringBuilder();
            MensagemFilialViewModel vm = new MensagemFilialViewModel();
            var contrato = _dbContrato.FindById(idContrato);
            var regional = contrato.CodRegionalProduto > 0 ? contrato.TabelaRegionalProduto.CodRegional > 0 ? contrato.TabelaRegionalProduto.TabelaRegional : new TabelaRegional() : new TabelaRegional();
            vm.Responsavel = regional.Responsavel;
            vm.EmailDestinatario = regional.Email;

            List<ContratoParcela> parcelas = contrato.ContratoParcelas.ToList();
            var objAcao = _dbTabelaModeloAcao.FindAll(x => !x.DataExcluido.HasValue && x.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.EMAIL_SEGUNDA_VIA_BOLETO).FirstOrDefault();
            string tbParcelas = MontarTabelaHtml(parcelas);

            Cliente cliente = contrato.ContratoClientes.First().Cliente;

            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.TabelaParcelas), tbParcelas);
            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NomeComprador), DbRotinas.Descriptografar(cliente.Nome));
            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.CPF_CPNPJ_Comprador), DbRotinas.Descriptografar(cliente.CPF));


            int codRegional = (int)WebRotinas.ObterUsuarioLogado().CodRegional;
            var objRegional = _dbFilial.FindById(codRegional);
            if (objRegional != null)
            {
                //DADOS DA REGIONAL PRODUTO;
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.DescricaoFilial), DbRotinas.Descriptografar(objRegional.Regional));
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NumeroFilial), objRegional.Telefone);
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.ResponsavelFilial), objRegional.Responsavel);
            }
            vm.Mensagem = sb.ToString();
            return PartialView("_partialMensagem", vm);
        }

        [HttpPost, ValidateInput(false)]
        public JsonResult EnviarMensagemEmail(int CodContrato, string Assunto = null, string Mensagem = null, string EmailDestinario = null, string NomeDestinario = null)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                op = DispararEmailComMensagem(CodContrato, NomeDestinario, EmailDestinario, Assunto, Mensagem);
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
                op.Mensagem = op.Mensagem + "Destinatário: " + EmailDestinario;
            }

            return Json(op);
        }

        public OperacaoRetorno DispararEmailComMensagem(int CodContrato, string _nomeDestinatario, string _emailDestinatario, string _assunto, string _msg)
        {
            try
            {
                _objParametro = _dbParametro.FindAll().FirstOrDefault();
                _objEmailRotinas = new EmailRotinas(_objParametro.SMTPPorta, _objParametro.SMTPServidor, _objParametro.SMTPUsuario, _objParametro.SMTPSenha, _objParametro.fgSSL == true);

                var objUsuario = _dbUsuario.FindById(WebRotinas.ObterUsuarioLogado().Cod);
                var objRegional = objUsuario.TabelaRegional;
                if (objRegional == null)
                {
                    objRegional = new TabelaRegional();
                }
                LogEmail email = new LogEmail();

                //*****REMETENTE******//
                email.EmailRemetente = objRegional.Email;
                email.NomeRemetente = objRegional.Responsavel;
                //*****DESTINATARIO******//
                email.EmailDestinatario = _emailDestinatario;
                email.NomeDestinatario = _nomeDestinatario;
                //*****EMAIL******//
                email.Assunto = _assunto;
                email.Conteudo = _msg.ToString();
                email.HTML = true;
                //*****DISPARAR EMAIL******//
                var NotificacaoRetorno = _objEmailRotinas.Enivar(new EmailInfo()
                {
                    EmailRemetente = email.EmailRemetente ?? objUsuario.Email,
                    NomeRemetente = email.NomeRemetente ?? objUsuario.Nome,
                    NomeDestinatario = email.NomeDestinatario,
                    EmailDestinatario = email.EmailDestinatario,
                    Assunto = email.Assunto ?? "(sem assunto)",
                    Conteudo = email.Conteudo,
                    HTML = true
                });

                //*****STATUS RETORNO******//
                if (NotificacaoRetorno.OK)
                {
                    email.Status = (short)Enumeradores.LogEmailStatus.Enviado;
                    email.Mensagem = NotificacaoRetorno.Mensagem;

                    //*****REGISTRA UM HISTORICO NO CONTRATO ******//
                    if (CodContrato > 0)
                    {
                        Historico _hist = new Historico();
                        var clienteId = _dbContratoCliente.All().Where(x => x.CodContrato == CodContrato).First().CodCliente;
                        _hist.CodCliente = clienteId;
                        _hist.CodContrato = CodContrato;
                        var msg = email.Assunto + " - " + email.Conteudo;
                        _hist.Observacao = StripHTML(msg);
                        _hist.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                        _dbHistorico.CreateOrUpdate(_hist);
                    }

                }
                else
                {
                    email.Status = (short)Enumeradores.LogEmailStatus.FalhaNoEnvio;
                    email.Mensagem = NotificacaoRetorno.Mensagem;

                }
                //*****REGRISTA UM LOG DO EMAIL******//
                var entityEmail = _dbLogEmail.CreateOrUpdate(email);
                OperacaoRetorno objRetorno = new OperacaoRetorno();
                objRetorno.OK = NotificacaoRetorno.OK;
                objRetorno.Mensagem = NotificacaoRetorno.Mensagem;



                return objRetorno;
            }
            catch (Exception ex)
            {
                var exception = new OperacaoRetorno(ex);
                exception.Mensagem = exception.Mensagem + " detalhe:" + ex.InnerException;
                return exception;
            }

        }

        public static string StripHTML(string inputHTML)
        {
            string noHTML = Regex.Replace(inputHTML, @"<[^>]+>|&nbsp;", "").Trim();
            //noHTML = Regex.Replace(noHTML, @"[\r\n]", " ").Trim();
            noHTML = DbRotinas.Criptografar(noHTML);
            return noHTML;

        }

        [HttpPost]
        public JsonResult VerificaRegionalContrato(string id)
        {
            try
            {
                List<int> codContratos = id.Split(',').Select(x => DbRotinas.ConverterParaInt(x)).ToList();
                OperacaoRetorno objRetorno = new OperacaoRetorno();
                var usuarioLogado = WebRotinas.ObterUsuarioLogado();
                var codRegionalUsuario = WebRotinas.ObterUsuarioLogado().CodRegional;
                var codRegionalContrato = _dbContrato.All().Where(x => codContratos.Contains(x.Cod)).ToList().Select(x => x.TabelaRegionalProduto.CodRegional);

                if (codRegionalContrato.Contains(codRegionalUsuario) || usuarioLogado.PerfilTipo == Enumeradores.PerfilTipo.Admin)
                {
                    objRetorno.OK = true;
                }
                else
                {
                    objRetorno.OK = false;
                }

                return Json(objRetorno);
            }
            catch (Exception ex)
            {
                return Json(new OperacaoRetorno(ex));
            }
        }
        [HttpPost]
        public ActionResult ContratoDados(int clienteId = 0, List<int> contratosIds = null, DTParameters param = null)
        {
            var userLogado = WebRotinas.ObterUsuarioLogado();
            var CodUsuarioLogado = userLogado.Cod;
            List<Contrato> listaContratos = new List<Contrato>();
            listaContratos = _dbContratoCliente.FindAll(x => !x.DataExcluido.HasValue && x.CodCliente == clienteId).Select(x => x.Contrato).ToList();
            FiltroGeralDados Filtro = WebRotinas.FiltroGeralObter();

            //CASO FILTRAR UM CONTRATO DEVERA TRAZER APENAS 1
            if (Filtro.NumeroContrato != null)
            {
                var nrContratos = Filtro.NumerosContratosCriptografadoArray;
                listaContratos = listaContratos.Where(x => nrContratos.Any(key => x.NumeroContrato.Contains(key))).ToList();
            }

            //CASO FILTRAR PELA FILIAL DEVERA TRAZER APENAS CONTRATOS DA FILIAL FILTRADA
            if (Filtro.CodRegional > 0)
            {
                var regionaisProdutos = _dbRegionalProduto.FindAll(x => x.CodRegional == Filtro.CodRegional).Select(x => x.Cod).ToList();
                listaContratos = listaContratos.Where(x => regionaisProdutos.Contains((int)x.CodRegionalProduto)).ToList();
            }

            //SE FILTRAR O NUMERO DA PARCELA DEVE TRAZER SOMENTE OS CONTRATOS DAS PARCELAS
            if (Filtro.NumeroFatura != null)
            {
                var nrFaturas = Filtro.NumerosFaturasArray;
                listaContratos = (from c in listaContratos
                                  let contratoParcelas = from cp in c.ContratoParcelas
                                                         where nrFaturas.Any(key => cp.FluxoPagamento.Contains(key))
                                                         select cp.CodContrato
                                  where contratoParcelas.Contains(c.Cod)
                                  select c).ToList();
            }
            //SE FILTRAR A NOTAFISCAL DA PARCELA DEVE TRAZER SOMENTE OS CONTRATOS DAS PARCELAS
            if (Filtro.NotaFiscal != null)
            {
                var nFiscais = Filtro.NotaFiscaisArray;
                listaContratos = (from c in listaContratos
                                  let contratoParcelas = from cp in c.ContratoParcelas
                                                         where nFiscais.Any(key => cp.NumeroNotaFiscal.Contains(key))
                                                         select cp.CodContrato
                                  where contratoParcelas.Contains(c.Cod)
                                  select c).ToList();
            }
            if (contratosIds != null)
            {
                listaContratos = listaContratos.Where(x => contratosIds.Contains(x.Cod)).ToList();
            }
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var lista = (from q in listaContratos
                         select new
                         {
                             Cod = q.Cod,
                             CodContrato = q.Cod,
                             StatusContrato = q.StatusContrato,
                             NomeConstrutora = q.ContratoParcelas.Count > 0 ? q.ContratoParcelas.OrderByDescending(x => x.DataVencimento).First().Companhia : "",
                             Regional = q.CodRegionalProduto > 0 ? DbRotinas.Descriptografar(q.TabelaRegionalProduto?.TabelaRegional?.Regional) : "",
                             NumeroContrato = DbRotinas.Descriptografar(q.NumeroContrato),
                             Produto = q.CodRegionalProduto.HasValue ? DbRotinas.Descriptografar(q.TabelaRegionalProduto?.TabelaProduto?.Descricao) : "",
                             ValorAtraso = q.ValorAtraso,
                             ValorAVencer = q.ValorAVencer,
                             ValorTotal = q.ValorTotal,
                             ValorAtrasoDesc = $"{q.ValorAtraso:C}",
                             ValorAVencerDesc = $"{q.ValorAVencer:C}",
                             ValorTotalDesc = $"{q.ValorTotal:C}",
                             DiasAtraso = DbRotinas.ConverterParaString(q.DiasAtraso)
                         }).AsQueryable();

            var result = WebRotinas.DTRotinas.DTResult(param, lista);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AcaoCobrancaDados(int clienteId = 0, DTParameters param = null)
        {
            var userLogado = WebRotinas.ObterUsuarioLogado();
            var CodUsuarioLogado = userLogado.Cod;
            Dados obj = new Dados();

            var _usuario = WebRotinas.ObterUsuarioLogado();
            var atendeteCod = 0;
            if (_usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional)
            {
                atendeteCod = _usuario.Cod;
            }
            var listaAcoes = obj.ConsultaCasosAtendimentoPorCliente(atendeteCod, clienteId);
            var listaAcaoId = listaAcoes.Select(s => s.CodAcaoCobrancaHistorico).ToList();
            var listaParcelaId = listaAcoes.Select(s => s.CodParcela).ToList();
            List<ContratoAcaoCobrancaHistorico> listaAcaoCobrancaHistorico = new List<ContratoAcaoCobrancaHistorico>();
            listaAcaoCobrancaHistorico = _dbAcaoCobrancaHistorico.FindAll(x => !x.DataExcluido.HasValue && listaAcaoId.Contains((int)x.Cod)).ToList();
            var listaParcelas = _dbContratoParcela.FindAll(x => !x.DataExcluido.HasValue && listaParcelaId.Contains((int)x.Cod)).ToList();
            var lista = (from h in listaAcaoCobrancaHistorico
                         join p in listaParcelas on h.CodParcela equals p.Cod
                         select new
                         {
                             Cod = p.Cod,
                             Status = h.DataEnvio.HasValue ? "Finalizado" : "Pendente",
                             CodCliente = DbRotinas.Descriptografar(h.Contrato.ContratoClientes.First().Cliente.CodCliente),
                             NumeroContrato = DbRotinas.Descriptografar(h.Contrato.NumeroContrato),
                             FluxoPagamento = p.FluxoPagamento,
                             DataEmissao = p.DataEmissaoFatura,
                             DataVencimento = p.DataVencimento,
                             DataEmissaoDesc = $"{p.DataEmissaoFatura:dd/MM/yyyy}",
                             DataVencimentoDesc = $"{p.DataVencimento:dd/MM/yyyy}",
                             TipoFatura = p.TipoFatura,
                             NotaFiscal = p.NumeroNotaFiscal,
                             ValorParcela = p.ValorParcela,
                             ValorAberto = p.ValorAberto,
                             ValorParcelaDesc = $"{p.ValorParcela:C}",
                             ValorAbertoDesc = $"{p.ValorAberto:C}",
                             DataAgendamento = h.DataAtendimentoAgendado,
                             DataAgendamentoDesc = $"{h.DataAtendimentoAgendado:dd/MM/yyyy}",
                             DataAcao = h.DataCadastro,
                             DataAcaoDesc = $"{h.DataCadastro:dd/MM/yyyy}",
                             TipoAcaoCobranca = DbRotinas.Descriptografar(h.TabelaTipoCobrancaAcao.TabelaTipoCobranca.TipoCobranca),
                             AcaoCobranca = h.TabelaTipoCobrancaAcao.TabelaAcaoCobranca.AcaoCobranca,
                             SituacaoTipo = EnumeradoresDescricao.SituacaoHistorico(h.SituacaoTipo),
                             AcaoStatus = h.Status
                         }).AsQueryable();

            var result = WebRotinas.DTRotinas.DTResult(param, lista);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public FileResult DownloadExcelContrato(int ClienteId)
        {
            string filename = WebRotinas.ObterArquivoTemp();
            FileInfo info = new FileInfo(filename);
            var userLogado = WebRotinas.ObterUsuarioLogado();
            var CodUsuarioLogado = userLogado.Cod;
            List<Contrato> listaContratos = new List<Contrato>();
            listaContratos = _dbContratoCliente.FindAll(x => !x.DataExcluido.HasValue && x.CodCliente == ClienteId).Select(x => x.Contrato).ToList();
            var filtro = WebRotinas.CookieObjetoLer<PesquisaViewModel>(_filtroSessionId);
            var codSintese = filtro.CodSintese;
            if (codSintese > 0)
            {
                //if (usuarioLogado.PerfilTipo == Enumeradores.PerfilfTipo.Regional)
                //{
                //    listaContratos = listaContratos.Where(x => rotas.Contains(x.RotaArea)).ToList();
                //}
                listaContratos = listaContratos.Where(x => x.CodHistorico.HasValue).ToList();
                listaContratos = (from p in listaContratos
                                  join h in listaContratos.Select(x => x.Historico).ToList() on p.CodHistorico equals h.Cod
                                  where (h.CodSintese == codSintese || codSintese == 0)
                                  select p).ToList();
            }
            var lista = (from q in listaContratos
                         select new
                         {
                             Status = q.StatusContrato,
                             Companhia = q.ContratoParcelas.Count > 0 ? q.ContratoParcelas.OrderByDescending(x => x.DataVencimento).First().Companhia : "",
                             Regional = DbRotinas.Descriptografar(q.TabelaRegionalProduto.TabelaRegional.Regional),
                             NrContrato = DbRotinas.Descriptografar(q.NumeroContrato),
                             Produto = q.CodRegionalProduto.HasValue ? DbRotinas.Descriptografar(q.TabelaRegionalProduto.TabelaProduto.Descricao) : "",
                             VrTotal = $"{q.ValorTotal:C}",
                             VrAtraso = $"{q.ValorAtraso:C}",
                             VrAVencer = $"{q.ValorAVencer:C}",
                             DiasAtraso = DbRotinas.ConverterParaString(q.DiasAtraso)
                         }).AsQueryable();
            ConverterListaParaExcel.GerarCSVDeGenericList(lista.ToList(), filename);
            var nmarquivo = "contrato" + DateTime.Now.ToString("ddMMyy") + ".csv";
            return File(info.FullName, "application/force-download", nmarquivo);
        }
        [HttpPost]
        public JsonResult ExportarExcelParcela(DTParameters param, string contratoId = null, int CodSinteseContrato = 0, int CodSinteseParcela = 0, int tipo = 0)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                var userLogado = WebRotinas.ObterUsuarioLogado();
                var CodUsuarioLogado = userLogado.Cod;
                List<int> CodRegionalUsuario = _dbUsuarioRegional.All().Where(x => x.CodUsuario == CodUsuarioLogado).Select(r => r.CodRegional).ToList();
                var RegionaisUsuario = _dbRegionalGrupo.All().Where(x => CodRegionalUsuario.Contains((int)x.CodRegional)).Select(r => r.CodRegionalCliente).ToList();
                var query_dados = _qry.ListarParcelasPorContrato(contratoId, CodSinteseContrato, CodSinteseParcela).ToList();

                FiltroGeralDados Filtro = WebRotinas.FiltroGeralObter();
                if (Filtro.NumeroFatura != null)
                {
                    var nrFaturas = Filtro.NumerosFaturasArray;
                    query_dados = query_dados.Where(x => nrFaturas.Any(key => x.FluxoPagamento.Contains(key))).ToList();
                }
                if (Filtro.NotaFiscal != null)
                {
                    var nFiscais = Filtro.NotaFiscaisArray;
                    query_dados = query_dados.Where(x => nFiscais.Any(key => x.NF.Contains(key))).ToList();
                }

                if (tipo == (short)Enumeradores.FilaAtendimentoTipoParcela.aVencer) query_dados = query_dados.Where(x => x.DataVencimento >= DateTime.Now.Date).ToList();
                if (tipo == (short)Enumeradores.FilaAtendimentoTipoParcela.Atraso) query_dados = query_dados.Where(x => x.DataVencimento < DateTime.Now.Date && (!x.DataFechamento.HasValue) && x.ValorAberto > 0).ToList();
                if (tipo == (short)Enumeradores.FilaAtendimentoTipoParcela.Pago) query_dados = query_dados.Where(x => x.DataFechamento.HasValue && x.ValorAberto == 0).ToList();
                bool IsRegionalUsuario = RegionaisUsuario.Contains("30413") || userLogado.Admin == true ? true : false;//VERIFICA SE A PARCELA ESTA PARA A FILIAL DO USUARIO
                ContratoParcelaDados classe = new ContratoParcelaDados();
                Helper _rotina = new Helper();

                List<ContratoDados> Dados = new List<ContratoDados>();
                List<ConverterListaParaExcel.CollumnExcel> columns = new List<ConverterListaParaExcel.CollumnExcel>();
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CodCliente", Text = Idioma.Traduzir("An8") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NumeroContrato", Text = Idioma.Traduzir("Nr.Contrato") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataEmissao", Text = Idioma.Traduzir("Dt.Emissao") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataVencimento", Text = Idioma.Traduzir("Dt.Vencimento") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "FluxoPagamento", Text = Idioma.Traduzir("Nr.Documento") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "TipoFatura", Text = Idioma.Traduzir("Tp.Doc") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NotaFiscal", Text = Idioma.Traduzir("Nr.Fatura(NF)") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ValorParcelaDesc", Text = Idioma.Traduzir("Valor") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ValorAbertoDesc", Text = Idioma.Traduzir("Vr.Aberto") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NumeroParcela", Text = Idioma.Traduzir("Nr.Parcela") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "RotaArea", Text = Idioma.Traduzir("Cd.Area") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ClienteParcela", Text = Idioma.Traduzir("Cliente Parcela") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CodRegionalCliente", Text = Idioma.Traduzir("BU") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Filial", Text = Idioma.Traduzir("Filial") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataFechamento", Text = Idioma.Traduzir("Dt.Fechamento") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "DataPagamento", Text = Idioma.Traduzir("Dt.Pagamento") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "InstrucaoPagamento", Text = Idioma.Traduzir("Instr.Pagto") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NossoNumero", Text = Idioma.Traduzir("Nosso Número") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Status", Text = Idioma.Traduzir("Status") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Fase", Text = Idioma.Traduzir("Fase") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Sintese", Text = Idioma.Traduzir("Sintese") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CodigoEmpresaCobranca", Text = Idioma.Traduzir("Cobrança") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Cia", Text = Idioma.Traduzir("Cia") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Governo", Text = Idioma.Traduzir("Governo") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Corporativo", Text = Idioma.Traduzir("Corporativo") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "HistoricoObs", Text = Idioma.Traduzir("Observação") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "ClienteVIP", Text = Idioma.Traduzir("ClienteVIP") });

                var lista = (from q in query_dados
                             select new
                             {
                                 CodCliente = DbRotinas.Descriptografar(q.CodCliente),
                                 CodParcela = q.CodParcela,
                                 DataEmissao = $"{q.DataEmissaoFatura:dd/MM/yyyy}",
                                 DataVencimento = $"{q.DataVencimento:dd/MM/yyyy}",
                                 FluxoPagamento = q.FluxoPagamento,
                                 NotaFiscal = q.NF,
                                 ValorParcela = q.ValorParcela,
                                 ValorAberto = q.ValorAberto,
                                 ValorParcelaDesc = $"{q.ValorParcela:C}",
                                 ValorAbertoDesc = $"{q.ValorAberto:C}",
                                 NumeroParcela = q.NumeroParcela,
                                 RotaArea = DbRotinas.Descriptografar(q.RotaArea),
                                 CodRegionalCliente = q.CodRegionalCliente,
                                 Filial = DbRotinas.Descriptografar(q.Filial),
                                 DataFechamento = $"{q.DataFechamento:dd/MM/yyyy}",
                                 DataPagamento = $"{q.DataPagamento:dd/MM/yyyy}",
                                 Status = verificarStatus(q.DataVencimento, q.DataFechamento, q.ValorAberto, q.ValorParcela),
                                 Fase = DbRotinas.Descriptografar(q.Fase),
                                 NaturezaCliente = q.NaturezaCliente,
                                 Sintese = $"{q.CodigoSinteseCliente} - {DbRotinas.Descriptografar(q.Sintese)}",
                                 HistoricoObs = DbRotinas.Descriptografar(q.HistoricoObs),
                                 IsPago = q.DataFechamento.HasValue && q.ValorAberto == 0 ? true : false,
                                 IsRegionalUsuario = RegionaisUsuario.Contains(q.CodRegionalCliente) || userLogado.Admin == true ? true : false,//VERIFICA SE A PARCELA ESTA PARA A FILIAL DO USUARIO
                                 PossuiLinhaDigitavel = q.LinhaDigitavel != null ? true : false,
                                 TipoFatura = q.TipoFatura,
                                 Cia = q.Companhia,
                                 Governo = q.Governo,
                                 InstrucaoPagamento = q.InstrucaoPagamento,
                                 NossoNumero = $"{q.NossoNumero}",
                                 Corporativo = q.Corporativo,
                                 CodigoEmpresaCobranca = q.CodigoEmpresaCobranca,
                                 NumeroContrato = DbRotinas.Descriptografar(q.NumeroContrato),
                                 DiasAtraso = q.DiasAtraso,
                                 ClienteParcela = DbRotinas.Descriptografar(q.ClienteParcela),
                                 ClienteVIP = q.ClienteVIP
                             }).AsQueryable();

                var result = WebRotinas.DTRotinas.DTFilterResult(lista, param);

                var nomeArquivo = Idioma.Traduzir("ListaParcela");
                op = _rotina.GerarArquivo(nomeArquivo, result, columns);
            }
            catch (Exception ex)
            {

                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }

        [HttpGet]
        public ActionResult FormAtendenteRegional(int u = 0)
        {
            var usuario = _dbUsuario.FindById(u);
            var model = usuario.UsuarioRegionals.Where(x => !x.DataExcluido.HasValue).ToList();
            return PartialView("_AtendenteRegional", model);
        }

    }
}