﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Cobranca.Web.Controllers
{


    public class RegionalProdutoController : BasicController<TabelaRegionalProduto>
    {
        public const string _filtroCodFilial = "FiltroCodFilial";
        public Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        public Repository<TabelaRegionalProduto> _dbRegionalProduto = new Repository<TabelaRegionalProduto>();
        public Repository<TabelaProduto> _dbProduto = new Repository<TabelaProduto>();


        public override void carregarCustomViewBags()
        {
            var listaR = _dbRegional.All().ToList();
            foreach (var item in listaR)
            {
                item.Regional = DbRotinas.Descriptografar(item.Regional);
            }
            var listaRegional = listaR.AsQueryable();
            ViewBag.ListaRegional = from p in listaRegional.OrderBy(x => x.Regional).ToList() select new DictionaryEntry(p.Regional, p.Cod);
            var listaP = _dbProduto.All().ToList();
            foreach (var item in listaP)
            {
                item.Produto = DbRotinas.Descriptografar(item.Produto);
                item.Descricao = DbRotinas.Descriptografar(item.Descricao);
            }
            var listaProduto = listaP.AsQueryable();
            ViewBag.ListaProduto = from p in listaProduto.OrderBy(x => x.Produto).ToList() select new DictionaryEntry(p.Produto + " - " + p.Descricao, p.Cod);

            var filial = _dbRegional.FindById(WebRotinas.CookieObjetoLer<int>(_filtroCodFilial));
            ViewBag.Filial = filial;

            base.carregarCustomViewBags();
        }


        public override ActionResult Cadastro(TabelaRegionalProduto dados)
        {
            if (dados.Cod == 0)
            {
                dados.CodRegional = WebRotinas.CookieObjetoLer<int>(_filtroCodFilial);
            }


            return base.Cadastro(dados);
        }

        [HttpGet]
        public ActionResult FilialProdutos(int CodFilial = 0)
        {
            carregarCustomViewBags();

            if (CodFilial == 0)
            {
                CodFilial = WebRotinas.CookieObjetoLer<int>(_filtroCodFilial);

            }
            else
            {
                WebRotinas.CookieObjetoGravar(_filtroCodFilial, CodFilial);
            }
            List<TabelaRegionalProduto> listaDados = new List<TabelaRegionalProduto>();
            listaDados = _repository.All().Where(x => x.CodRegional == CodFilial).ToList();


            var filial = _dbRegional.FindById(CodFilial);
            ViewBag.Filial = filial;

            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();

            return View(listaDados);
        }

        public JsonResult VincularRegionalProduto(int CodRegionalProduto, int CodRegional, int CodProduto)
        {
            var retorno = new
            {
                Ok = true,
                Mensagem = "",
                Cod = 0
            };
            try
            {
                var entity = _dbRegionalProduto.CreateOrUpdate(new TabelaRegionalProduto()
                {
                    Cod = CodRegionalProduto,
                    CodProduto = CodProduto,
                    CodRegional = CodRegional
                });
                retorno = new
                {
                    Ok = true,
                    Mensagem = "",
                    Cod = entity.Cod
                };
            }
            catch (Exception ex)
            {

                retorno = new
                {
                    Ok = false,
                    Mensagem = ex.Message,
                    Cod = 0
                };
            }


            return Json(retorno);

        }
    }

}