﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.IO;
using System.Web.UI;
using System.Data.OleDb;
using System.Data.SqlClient;
using Recebiveis.Web.Models;
using Cobranca.Rotinas;
using Excel;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Cobranca.Web.Controllers
{
    public class ImportacaoParcelaController : Controller
    {
        private Repository<Contrato>
        _dbContrato = new Repository<Contrato>();

        private Repository<Importacao>
        _dbImportacao = new Repository<Importacao>();

        public Dados _qry = new Dados();
        public const string _filtroSessionId = "FiltroPesquisaId";




        //string[] arquivo;
        List<string> arquivo = new List<string>();

        [HttpGet]
        public ActionResult Import()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ImportLista()
        {
            //var processamentos = _qry.ListarImportacao((short)Enumeradores.TipoImportacao.Parcela);
            var processamentos = _dbImportacao.All().Where(x => x.Tipo == 2).ToList();
            return View(processamentos);
        }
        [HttpPost]
        public JsonResult LerArquivo(string[] file)
        {

            try
            {
                CarregarArquivoParaBanco(file);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        [HttpPost]
        public JsonResult ObterTotais(int[] IdImportacao)
        {

            try
            {
                var processamentos = _dbImportacao.All().Where(x => x.Tipo == 2).ToList();
                var objRetorno = from r in processamentos
                                 select new
                                 {
                                     r.Cod,
                                     r.TotalRegistros,
                                     r.TotalRegistrosProcessado,
                                     r.TotalRegistrosErro,
                                     r.TotalRegistrosOK,
                                     r.Mensagem
                                 };

                return Json(new { OK = true, Dados = objRetorno.ToList() });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        private FileInfo GetFile(string file)
        {
            string pathUpload = Server.MapPath("~/Content/files");
            string pathDefinitivo = Server.MapPath(string.Format("~/Content/files/{0}", file));
            return new FileInfo(pathDefinitivo);
        }


        public void CarregarArquivoParaBanco(string[] files)
        {
            foreach (var file in files)
            {
                FileInfo File = GetFile(file);
                var dt = ConvertTXTtoDataTable(File.Name, File.FullName);
                var colunasDePara = new List<ColunaDePara>();

                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "companhia", NomeColunaTabela = "Companhia" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Filial", NomeColunaTabela = "Filial" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Contrato", NomeColunaTabela = "NumeroContrato" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Descricaodafilial", NomeColunaTabela = "FilialDescricao" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "rotaarea", NomeColunaTabela = "RotaArea" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Status", NomeColunaTabela = "StatusContrato" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Tipodecontrato", NomeColunaTabela = "TipoContrato" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Numerodocliente", NomeColunaTabela = "NumeroCliente" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "CPFCNPJ", NomeColunaTabela = "CPFCNPJ" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "Descricao", NomeColunaTabela = "Descricao" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "NumerodaFatura", NomeColunaTabela = "NumeroFatura" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "valorfatura", NomeColunaTabela = "ValorFatura" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "valoremaberto", NomeColunaTabela = "ValorAberto" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "datavencimento", NomeColunaTabela = "DataVencimento" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "datafechamento", NomeColunaTabela = "DataFechamento" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numerodaparcela", NomeColunaTabela = "NumeroParcela" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "governo", NomeColunaTabela = "Governo" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "corporativo", NomeColunaTabela = "Corporativo" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "classificacaofatura", NomeColunaTabela = "CodSinteseCliente" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "notafiscal", NomeColunaTabela = "NotaFiscal" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "tipodocumento", NomeColunaTabela = "TipoDocumento" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "dataemissaofatura", NomeColunaTabela = "DataEmissao" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "instrumentopagto", NomeColunaTabela = "InstrumentoPgto" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "datapagamento", NomeColunaTabela = "DataPagamento" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "nossonumero", NomeColunaTabela = "NossoNumero" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "empresacobranca", NomeColunaTabela = "EmpresaCobranca" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "an8parcela", NomeColunaTabela = "NumeroClienteParcela" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "numerodalinha", NomeColunaTabela = "NumeroLinha" });
                colunasDePara.Add(new ColunaDePara() { NomeColunaDataTable = "impostos", NomeColunaTabela = "Imposto" });
                ImportacaoDb.BulkCopy(dt, "ImportacaoParcela", colunasDePara);
            }

            int codUsuarioLogado = WebRotinas.ObterUsuarioLogado().Cod;
            var op = ImportacaoDb.ImportacaoParcelaObterStatus(codUsuarioLogado, 0, String.Join("/", files));
            if (op.OK)
            {
                int CodImportacao = DbRotinas.ConverterParaInt(op.Dados);
                new Thread(delegate ()
                {
                    ImportacaoDb.ImportacaoParcelaProcessar(CodImportacao);
                }).Start();


                //Thread thread = new Thread(() => executar(CodImportacao));
                //thread.IsBackground = true;
                //thread.Start();
            }

        }

        public static DataTable ConvertTXTtoDataTable(string strFileName, string strFilePath)
        {
            DataTable dt = new DataTable();
            Int32 c = 0;
            try
            {
                using (StreamReader sr = new StreamReader(strFilePath, System.Text.Encoding.Default, false, 512))
                {
                    string[] headers = new string[] { };
                    string linhaHader = sr.ReadLine();
                    if (linhaHader.Split('\t').Length <= 1)
                    {
                        headers = linhaHader.Split(new Char[] { ';' });
                    }
                    else
                    {
                        headers = linhaHader.Split(new Char[] { '\t' });
                    }
                    foreach (string header in headers)
                    {
                        if (dt.Columns.Count <= 29)
                        {
                            dt.Columns.Add(header.Trim());
                        }
                    }
                    //string[] rows = sr.ReadLine().Split(';');
                    var text = sr.ReadToEnd();
                    string[] stringSeparators = new string[] { "\r\n" };
                    var rows = text.Split(stringSeparators, StringSplitOptions.None);

                    foreach (var r in rows)
                    {
                        DataRow dr = dt.NewRow();
                        //string linhatexto = sr.ReadLine();
                        string linhatexto = r.ToString();
                        if (linhatexto == null || String.IsNullOrEmpty(linhatexto))
                        {
                            break;
                        }

                        string[] quebra = new string[] { };
                        if (linhatexto.Split('\t').Length <= 1)
                        {
                            quebra = linhatexto.Split(new Char[] { ';' });
                        }
                        else
                        {
                            quebra = linhatexto.Split(new Char[] { '\t' });
                        }
                        //remove a ultima coluna
                        if (quebra.Count() == 28)
                        {
                            quebra = quebra.Take(quebra.Count() - 1).ToArray();
                        }

                        for (int i = 0; i < quebra.Count(); i++)
                        {
                            quebra[i] = quebra[i].Replace(";", "").Replace("\t", "").Replace("\"", "");
                            //VERIFICA OS CAMPOS PARA OFUSCAR OS DADOS ANTES DA IMPORTACAO
                            if (i == 3 || i == 4 || i == 6 || i == 7 || i == 8 || i == 9 || i == 26)
                            {
                                dr[i] = DbRotinas.Criptografar(quebra[i].Trim());
                            }
                            else if (i == 2)//DESCRICAO FILIAL
                            {
                                dr[i] = DbRotinas.Criptografar(quebra[i].Replace(" SERAL", "").Trim());
                            }
                            else if (i == 13 || i == 14 || i == 21 || i == 23)
                            {
                                dr[i] = DbRotinas.ConverterParaDatetimeOuNull(quebra[i].Trim());
                            }
                            else if (i == 11)
                            {
                                dr[i] = DbRotinas.ConverterParaDecimal(quebra[i].Trim());
                            }
                            else
                            {
                                dr[i] = quebra[i].Trim();
                            }
                        }
                        c = c + 1;

                        dt.Rows.Add(dr);
                    }
                    return dt;

                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(strFilePath, System.Text.Encoding.Default, false, 512))
            {
                string[] headers = sr.ReadLine().Split(new Char[] { ';' });

                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(';');
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        [HttpPost]
        public JsonResult Excluir(int id)
        {
            try
            {
                _dbImportacao.Delete(id);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        public FileResult ExtrairResultadoImportacaoParcela(int CodImportacao = 0, short Status = 0)
        {
            var lista = ImportacaoDb.ListarResultadoPorParcela(CodImportacao, Status);

            string pathFile = Path.Combine(Path.GetTempPath(), Path.GetTempFileName());
            using (TextWriter tw = System.IO.File.CreateText(pathFile))
            {
                tw.WriteLine("Cod;compahia;Filial;Descricaodafilial;Contrato;rotaarea;Status;Tipodecontrato;Numerodocliente;CPFCNPJ;Descricao;NumerodaFatura;valorfatura;valoremaberto;datavencimento;datafechamento;numerodaparcela;governo;corporativo;classificacaofatura;notafiscal;tipodocumento;instrumentopagto;datapagamento;Resultado");
                foreach (var item in lista)
                {
                    tw.WriteLine(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18};{19};{20};{21};{22};{23};{24}",
                        item.Cod
                      , item.Companhia
                      , item.Filial
                      , DbRotinas.Descriptografar(item.FilialDescricao)
                      , DbRotinas.Descriptografar(item.RotaArea)
                      , item.Status
                      , DbRotinas.Descriptografar(item.TipoContrato)
                      , DbRotinas.Descriptografar(item.NumeroCliente)
                      , DbRotinas.Descriptografar(item.CPFCNPJ)
                      , DbRotinas.Descriptografar(item.Descricao)
                      , item.NumeroFatura
                      , item.ValorAberto
                      , item.ValorAberto
                      , item.DataVencimento
                      , item.DataFechamento
                      , item.ValorAberto
                      , item.NumeroParcela
                      , item.Governo
                      , item.Corporativo
                      , item.classificacaofatura
                      , item.notafiscal
                      , item.tipodocumento
                      , item.instrumentopagto
                      , item.datapagamento
                      , item.Resultado
                    ));
                }
                tw.Close();
            }

            return File(pathFile, "text/csv", "resultado.csv");

        }

        public FileResult LayoutCliente(int CodImportacao = 0, short Status = 0)
        {


            StringBuilder sb = new StringBuilder();
            sb.AppendLine("compahia;Filial;Descricaodafilial;Contrato;rotaarea;Status;Tipodecontrato;Numerodocliente;CPFCNPJ;Descricao;NumerodaFatura;valorfatura;valoremaberto;datavencimento;datafechamento;numerodaparcela;governo;corporativo;classificacaofatura;notafiscal;tipodocumento;instrumentopagto;datapagamento;");

            return File(new System.Text.UTF8Encoding().GetBytes(sb.ToString()), "text/txt", "layout-parcela.txt");

        }

        [HttpPost]
        public JsonResult Resultado(int CodImportacao, short Status)
        {
            var lista = ImportacaoDb.ListarResultadoPorCliente(CodImportacao, Status);
            return Json(lista);
        }

        [HttpPost]
        public async Task<JsonResult> Processar(int CodImportacao)
        {
            ImportacaoDb.ImportacaoParcelaProcessar(CodImportacao);
            return Json(new { OK = true });
        }
    }

}
