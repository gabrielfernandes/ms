﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class RegionalGrupoController : BasicController<TabelaRegionalGrupo>
    {
        public Repository<TabelaConstrutora> _repConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaRegional> _repRegional = new Repository<TabelaRegional>();

        public override void carregarCustomViewBags()
        {
            var listaConstrutora = _repConstrutora.All().ToList();
            foreach (var item in listaConstrutora)
            {
                item.NomeConstrutora = DbRotinas.Descriptografar(item.NomeConstrutora);
            }
            ViewBag.ListaConstrutora = from p in listaConstrutora.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            var listaRegional = _repRegional.All().ToList();
            foreach (var item in listaRegional)
            {
                item.Regional = DbRotinas.Descriptografar(item.Regional);
            }
            ViewBag.ListaRegional = from p in listaRegional.OrderBy(x => x.Regional).ToList() select new DictionaryEntry(p.Regional, p.Cod);
        }

        [HttpPost]
        public ActionResult SalvarForm(string[] Cod, string[] CodConstrutora, string[] CodRegional, string[] CodRegionalCliente, string[] Descricao, string[] DataExcluido, string[] DataAlteracao)
        {
            try
            {
                List<TabelaRegionalGrupo> regionalGrupo = new List<TabelaRegionalGrupo>();
                for (int i = 0; i < Cod.Length; i++)
                {
                    regionalGrupo.Add(new TabelaRegionalGrupo() { Cod = DbRotinas.ConverterParaInt(Cod[i]), CodConstrutora = DbRotinas.ConverterParaInt(CodConstrutora[i]), CodRegional = DbRotinas.ConverterParaInt(CodRegional[i]), CodRegionalCliente = CodRegionalCliente[i], Descricao = Descricao[i] });
                }
                List<ClienteContato> _ClienteContato = new List<ClienteContato>();
                foreach (var regProd in regionalGrupo)
                {
                    var id = DbRotinas.ConverterParaInt(regProd.Cod);
                    var dados = _repository.FindById(id);
                    if (id == 0)
                    {
                        dados = Activator.CreateInstance<TabelaRegionalGrupo>();
                        dados.DataCadastro = DateTime.Now;
                    }
                    dados.Cod = id;
                    dados.CodConstrutora = regProd.CodConstrutora;
                    dados.CodRegional = regProd.CodRegional;
                    dados.CodRegionalCliente = regProd.CodRegionalCliente;
                    dados.Descricao = regProd.Descricao;
                    _repository.CreateOrUpdate(dados);
                }

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    OK = false,
                    Mensagem = ex.Message
                });
            }
        }

    }
}