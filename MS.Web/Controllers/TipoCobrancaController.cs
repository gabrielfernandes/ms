﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class TipoCobrancaController : BasicController<TabelaTipoCobranca>
    {
        Repository<TabelaSintese> _repoSintese = new Repository<TabelaSintese>();
        Repository<TabelaFase> _repoFase = new Repository<TabelaFase>();
        Repository<TabelaParametroSintese> _repoParametroSintese = new Repository<TabelaParametroSintese>();

        public override ActionResult Index()
        {

            bool fgSistemaCobranca = WebRotinas.ObterUsuarioLogado().SistemaCobranca == true;

            if (fgSistemaCobranca)
            {
                _where = (x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca);
            }
            else
            {
                _where = (x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Proposta);

            }
            return base.Index();
        }

        public override ActionResult Cadastro(TabelaTipoCobranca dados)
        {
            bool fgSistemaCobranca = WebRotinas.ObterUsuarioLogado().SistemaCobranca == true;
            dados.TipoFluxo = fgSistemaCobranca ? (short)Enumeradores.TipoFluxo.Cobranca : (short)Enumeradores.TipoFluxo.Proposta;
            dados.TipoCobranca = DbRotinas.Criptografar(dados.TipoCobranca);
            return base.Cadastro(dados);
        }

        public override void carregarCustomViewBags()
        {

            ViewBag.ListaTipoEsforcoImpacto = EnumeradoresLista.ImpactoEsforcoTipo();

            base.carregarCustomViewBags();
        }

        [HttpPost]
        public JsonResult SalvarSintese(int CodFase, string Sintese)
        {
            var retorno = new
            {
                Ok = true,
                Mensagem = "",
                CodSintese = 0
            };
            try
            {
                int? ordem = _repoSintese.All().Where(x => x.CodFase == CodFase).Max(x => x.Ordem);

                var entity = _repoSintese.CreateOrUpdate(new TabelaSintese()
                {
                    CodFase = CodFase,
                    Sintese = DbRotinas.Criptografar(Sintese),
                    Ordem = ordem.HasValue ? ordem + 1 : 1
                });
                retorno = new
                {
                    Ok = true,
                    Mensagem = "",
                    CodSintese = entity.Cod
                };
            }
            catch (Exception ex)
            {

                retorno = new
                {
                    Ok = false,
                    Mensagem = ex.Message,
                    CodSintese = 0
                };
            }


            return Json(retorno);
        }

        [HttpPost]
        public JsonResult SalvarFase(int CodTipoCobranca, string Fase)
        {
            var retorno = new
            {
                Ok = true,
                Mensagem = "",
                CodFase = 0
            };
            try
            {
                int? ordem = _repoFase.All().Where(x => x.CodTipoCobranca == CodTipoCobranca).Max(x => x.Ordem);

                var entity = _repoFase.CreateOrUpdate(new TabelaFase()
                {
                    CodTipoCobranca = CodTipoCobranca,
                    Fase = DbRotinas.Criptografar(Fase),
                    Ordem = ordem.HasValue ? ordem + 1 : 1
                });

                retorno = new
                {
                    Ok = true,
                    Mensagem = "Operação realizada com sucesso!",
                    CodFase = entity.Cod
                };

            }
            catch (Exception ex)
            {

                retorno = new
                {
                    Ok = false,
                    Mensagem = ex.Message,
                    CodFase = 0
                };
            }


            return Json(retorno);
        }
        [HttpPost]
        public JsonResult ExcluirFase(int codFase)
        {
            var retorno = new
            {
                Ok = true,
                Mensagem = "",
                CodFase = 0
            };
            try
            {
                _repoFase.Delete(codFase);
            }
            catch (Exception ex)
            {

                retorno = new
                {
                    Ok = false,
                    Mensagem = ex.Message,
                    CodFase = 0
                };
            }


            return Json(retorno);
        }

        [HttpPost]
        public JsonResult ExcluirSintese(int CodSintese)
        {
            var retorno = new
            {
                Ok = true,
                Mensagem = ""
            };
            try
            {
                _repoSintese.Delete(CodSintese);
            }
            catch (Exception ex)
            {

                retorno = new
                {
                    Ok = false,
                    Mensagem = ex.Message
                };
            }


            return Json(retorno);
        }

        [Authorize]
        [HttpGet]
        public ActionResult FluxoSolicitacao()
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();
            var listaDados = new List<TabelaTipoCobranca>();

            carregarCustomViewBags();
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            bool fgSistemaCobranca = WebRotinas.ObterUsuarioLogado().SistemaCobranca == true;
            if (fgSistemaCobranca)
            {
                _where = (x => (x.TipoFluxo == (short)Enumeradores.TipoFluxo.AnalistaBoleto || x.TipoFluxo == (short)Enumeradores.TipoFluxo.SupervisorBoleto || x.TipoFluxo == (short)Enumeradores.TipoFluxo.FilialBoleto));
            }
            if (_where != null)
            {
                listaDados = _repository.All().Where(_where).OrderBy(x => x.Cod).ToList();
            }
            else
                listaDados = _repository.All().OrderBy(x => x.Cod).ToList();


            return View(listaDados);
        }
        [Authorize]
        [HttpPost]
        public JsonResult SalvarFluxoBoleto(TabelaTipoCobranca dados)
        {
            try
            {
                bool fgSistemaCobranca = WebRotinas.ObterUsuarioLogado().SistemaCobranca == true;
                if (dados.TipoFluxo == (short)Enumeradores.TipoFluxo.AnalistaBoleto || dados.TipoFluxo == (short)Enumeradores.TipoFluxo.FilialBoleto || dados.TipoFluxo == (short)Enumeradores.TipoFluxo.SupervisorBoleto)
                {

                    dados.TipoFluxo = dados.TipoFluxo;
                }
                else
                {

                    dados.TipoFluxo = (short)Enumeradores.TipoFluxo.AnalistaBoleto;
                }
                dados.TipoCobranca = DbRotinas.Criptografar(dados.TipoCobranca);
                validacaoCustom(dados);
                carregarCustomViewBags();

                dados = _repository.CreateOrUpdate(dados);
                return Json(new { OK = true, Titulo = "", Mensagem = "Operacao realizada com sucesso" });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult ListarFases(int CodTipoCobranca)
        {
            OperacaoRetorno op = new OperacaoRetorno();

            try
            {
                List<DictionaryEntry> dados = BuscarFases(CodTipoCobranca, (short)Enumeradores.TipoHistorico.Boleto);

                op.Dados = dados;
                op.OK = true;
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }

        private List<DictionaryEntry> BuscarFases(int codTipoCobranca, short historicoTipo)
        {
            List<DictionaryEntry> dados = new List<DictionaryEntry>();

            var fases = _repoFase.All().Where(x => x.CodTipoCobranca == codTipoCobranca && x.TipoHistorico == historicoTipo).ToList();
            dados = (from p in fases
                     select new DictionaryEntry() { Key = p.Cod, Value = DbRotinas.Descriptografar(p.Fase) }).ToList();

            return dados;

        }
        [HttpPost]
        public JsonResult ListarListarSintesePorFase(int CodFase = 0)
        {
            OperacaoRetorno op = new OperacaoRetorno();

            try
            {
                List<DictionaryEntry> dados = new List<DictionaryEntry>();

                var fases = _repoSintese.All().Where(x => x.CodFase == CodFase).ToList();
                dados = (from p in fases
                         select new DictionaryEntry() { Key = p.Cod, Value = DbRotinas.Descriptografar(p.Sintese) }).ToList();

                op.Dados = dados;
                op.OK = true;

            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }


            return Json(op);
        }

        [HttpPost]
        public JsonResult ValidaDe(int cod = 0, int sintese = 0)
        {
            try
            {
                return Json((_repoParametroSintese.All().Where(x => x.Cod == cod && x.CodSinteseDe == (int)sintese).Count() > 0));
            }
            catch (Exception ex) { return Json(false); }
        }

        [HttpGet]
        public ActionResult FormSolicitacao(int id = 0)
        {
            carregarCustomViewBags();

            List<DictionaryEntry> listaFluxo = new List<DictionaryEntry>();
            listaFluxo.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoFluxo((short)Enumeradores.TipoFluxo.FilialBoleto), Value = (short)Enumeradores.TipoFluxo.FilialBoleto });
            listaFluxo.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoFluxo((short)Enumeradores.TipoFluxo.AnalistaBoleto), Value = (short)Enumeradores.TipoFluxo.AnalistaBoleto });
            listaFluxo.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoFluxo((short)Enumeradores.TipoFluxo.SupervisorBoleto), Value = (short)Enumeradores.TipoFluxo.SupervisorBoleto });

            ViewBag.FluxoBoleto = listaFluxo;
            var dados = _repository.FindById(id);
            if (id == 0)
            {
                dados = Activator.CreateInstance<TabelaTipoCobranca>();
                dados.DataCadastro = DateTime.Now;
            }
            return PartialView("_formCustom", dados);
        }
    }
}