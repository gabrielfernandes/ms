﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Cobranca.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class ConstrutoraController : BasicController<TabelaConstrutora>
    {
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaEndereco> _dbEndereco = new Repository<TabelaEndereco>();

        [Authorize]
        [HttpGet]
        public override ActionResult Index()
        {
            if (!WebRotinas.UsuarioAutenticado) WebRotinas.EncerrarSessao();
            if (WebRotinas.ObterUsuarioLogado().PerfilTipo != Enumeradores.PerfilTipo.Admin) return RedirectToAction("Index", "Home", new { msg = "AcessoNegado!" });

            var listaDados = new List<TabelaConstrutora>();

            carregarCustomViewBags();
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();

            return View(listaDados);
        }


        public override ActionResult Cadastro(TabelaConstrutora dados)
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();
            if (WebRotinas.ObterUsuarioLogado().PerfilTipo != Enumeradores.PerfilTipo.Admin)
                return RedirectToAction("Index", "Home", new { msg = "AcessoNegado!" });

            dados.NomeConstrutora = DbRotinas.Descriptografar(dados.NomeConstrutora);
            dados.CNPJ = DbRotinas.Descriptografar(dados.CNPJ);
            if (dados.TabelaEndereco.Cod == 0)
            {
                TabelaEndereco endereco = new TabelaEndereco();
                dados.TabelaEndereco = endereco;
            }
            dados.TabelaEndereco.Logradouro = Request["Logradouro"];
            dados.TabelaEndereco.Endereco = Request["Endereco"];
            dados.TabelaEndereco.Numero = Request["Numero"];
            dados.TabelaEndereco.CEP = Request["CEP"].Replace("-", "");
            dados.TabelaEndereco.Complemento = Request["Complemento"];
            dados.TabelaEndereco.Bairro = Request["Bairro"];
            dados.TabelaEndereco.Cidade = Request["Cidade"];
            dados.TabelaEndereco.Estado = Request["Estado"];
            dados.TabelaEndereco.UF = Request["UF"];
            var entityEndereco = _dbEndereco.CreateOrUpdate(dados.TabelaEndereco);
            dados.CodEndereco = entityEndereco.Cod;
            var _dados = dados;
            return base.Cadastro(dados);
        }

        [HttpPost]
        public JsonResult GetData(DTParameters param)
        {
            try
            {
                var result = WebRotinas.DTRotinas.DTResult(param, obterDados());

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public IQueryable<object> obterDados()
        {
            //QRY PARA TRAZER TODOS BOLETOS
            var data = _repository.All().OrderBy(x => x.Cod).ToList()
                         .Select(x => new
                         {
                             x.Cod,
                             x.DataCadastro,
                             x.CodCliente,
                             NomeConstrutora = DbRotinas.Descriptografar(x.NomeConstrutora),
                             CNPJ = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(x.CNPJ))
                             

                         }).AsQueryable();


            return data;
        }

        [HttpPost]
        public JsonResult ExportarExcel(DTParameters param)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                Helper _rotina = new Helper();
                List<ConverterListaParaExcel.CollumnExcel> columns = new List<ConverterListaParaExcel.CollumnExcel>();
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NomeConstrutora", Text = Idioma.Traduzir("Nome Empresa") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CNPJ", Text = Idioma.Traduzir("CNPJ") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NossoNumero", Text = Idioma.Traduzir("Nosso Numero") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NossoNumeroAtual", Text = Idioma.Traduzir("Nosso Numero Atual") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Lote", Text = Idioma.Traduzir("Nr.Lote") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "LoteAtual", Text = Idioma.Traduzir("Nr.Lote Atual") });

                var dtsource = obterDados();
                var result = WebRotinas.DTRotinas.DTFilterResult(dtsource, param);

                var nomeArquivo = Idioma.Traduzir("ListaConstrutora");
                op = _rotina.GerarArquivo(nomeArquivo, result, columns);
            }
            catch (Exception ex)
            {

                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }



    }
}