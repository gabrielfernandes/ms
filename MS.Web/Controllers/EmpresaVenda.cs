﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class EmpresaVendaController : BasicController<TabelaEmpresaVenda>
    {
        Repository<Usuario> _dbVendedores = new Repository<Usuario>();
        Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();

        [HttpGet]
        public ActionResult Vendedores(int CodEmpresaVenda = 0)
        {
            carregarCustomViewBags();
            var entity = _repository.FindById(CodEmpresaVenda);
            if (CodEmpresaVenda == 0)
            {
                entity = new TabelaEmpresaVenda();
            }
            var listaEmpresasVenda = _repository.All().ToList();
            listaEmpresasVenda.Insert(0, new TabelaEmpresaVenda());
            ViewBag.ListaEmpresaVenda = from p in _repository.All() select new { Cod = p.Cod, Text = p.EmpresaVendas };
            ViewBag.CodEmpresaVenda = CodEmpresaVenda;
            var listaVendedores = _dbVendedores.All().Where(x => x.CodEmpresaVenda.HasValue && x.SistemaProposta == true).ToList();
            if (CodEmpresaVenda > 0)
            {
                listaVendedores = listaVendedores.Where(x => x.CodEmpresaVenda == CodEmpresaVenda).ToList();
            }
            ViewBag.ListaVendedores = listaVendedores.ToList();

            return View(entity);
        }

        [HttpGet]
        public ActionResult Carteira(int CodEmpresaVenda = 0)
        {
            carregarCustomViewBags();
            var entity = _repository.FindById(CodEmpresaVenda);
            if (CodEmpresaVenda == 0)
            {
                entity = new TabelaEmpresaVenda();
            }
            var listaEmpresasVenda = _repository.All().ToList();
            listaEmpresasVenda.Insert(0, new TabelaEmpresaVenda());

            ViewBag.ListaEmpresaVenda = from p in _repository.All() select new { Cod = p.Cod, Text = p.EmpresaVendas };
            ViewBag.CodEmpresaVenda = CodEmpresaVenda;

            ViewBag.ListaEmpreendimentos = _dbEmpreendimento.All().ToList();


            return View(entity);
        }
        [HttpPost]
        public JsonResult CadastroUsuario(Usuario dados)
        {
            Repository<Usuario> repoUsuario = new Repository<Usuario>();
            try
            {
                dados.SistemaProposta = true;
                if (dados.Cod == 0)
                {
                    dados.Login = dados.Email;
                    dados.SenhaTemporaria = WebRotinas.GerarSenhaTemporaria();
                    dados.Senha = dados.SenhaTemporaria;
                }

                var entity = repoUsuario.CreateOrUpdate(dados);
                return Json(new { OK = true, Dados = entity.Cod });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult ObterDadosVendedor(int cod)
        {
            Repository<Usuario> repoUsuario = new Repository<Usuario>();
            try
            {
                var entity = repoUsuario.FindById(cod);
                return Json(new
                {
                    OK = true,
                    Dados = new
                    {
                        Cod = entity.Cod,
                        Nome = entity.Nome,
                        CPF = entity.CPF,
                        Email = entity.Email,
                        TelefoneResidencial = entity.TelefoneResidencial,
                        TelefoneComercial = entity.TelefoneComercial,
                        TelefoneCelular = entity.TelefoneCelular,
                        CEP = entity.CEP,
                        Endereco = entity.Endereco,
                        EnderecoNumero = entity.EnderecoNumero,
                        EnderecoComplemento = entity.EnderecoComplemento,
                        EnderecoBairro = entity.EnderecoBairro,
                        EnderecoEstado = entity.EnderecoEstado,
                        EnderecoCidade = entity.EnderecoCidade,
                        PermissaoProposta = entity.PermissaoProposta,
                        PermissaoProspect = entity.PermissaoProspect
                    }
                });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult ExcluirUsuario(int cod)
        {
            try
            {
                if (cod > 0)
                {
                    _dbVendedores.Delete(cod);
                }
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        [HttpPost]
        public JsonResult UsuairoRedefinirSenha(int cod)
        {
            try
            {
                if (cod > 0)
                {
                    var entity = _dbVendedores.FindById(cod);
                    entity.SenhaTemporaria = WebRotinas.GerarSenhaTemporaria();
                    entity.Senha = entity.Senha;
                    _dbVendedores.CreateOrUpdate(entity);
                }
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
    }
}
