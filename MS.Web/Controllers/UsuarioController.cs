﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Text;

namespace Cobranca.Web.Controllers
{
    public class UsuarioController : BasicController<Usuario>
    {
        public Repository<Usuario> _dbUsuario = new Repository<Usuario>();
        public Repository<LogEmail> _dbLogEmail = new Repository<LogEmail>();
        public Repository<ParametroPlataforma> _dbParametroPlataforma = new Repository<ParametroPlataforma>();
        private Repository<TabelaRegional> _repRegional = new Repository<TabelaRegional>();
        private Repository<TabelaRegionalGrupo> _repRegionalGrupo = new Repository<TabelaRegionalGrupo>();
        private Repository<TabelaConstrutora> _repConstrutora = new Repository<TabelaConstrutora>();
        private Repository<UsuarioRegional> _dbUsuarioRegional = new Repository<UsuarioRegional>();
        private Repository<ParametroAtendimento> _dbParametroAtendimento = new Repository<ParametroAtendimento>();
        private Repository<ParametroAtendimentoProduto> _dbParametroAtendimentoProduto = new Repository<ParametroAtendimentoProduto>();
        private Repository<ParametroAtendimentoRota> _dbParametroAtendimentoRota = new Repository<ParametroAtendimentoRota>();
        public override ActionResult Index()
        {
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            if (usuarioLogado.SistemaCobranca == true)
            {
                _where = x => x.SistemaCobranca == true;
            }

            if (WebRotinas.ObterUsuarioLogado().PerfilTipo != Enumeradores.PerfilTipo.Admin)
            {
                return RedirectToAction("Cadastro", new { id = WebRotinas.ObterUsuarioLogado().Cod });
            }

            return base.Index();
        }


        public override void carregarCustomViewBags()
        {
            long usuarioId = WebRotinas.ObterUsuarioLogado().Cod;
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            var perfil = usuarioLogado.PerfilTipo;
            var listaPerfil = new List<DictionaryEntry>();
            if (perfil == Enumeradores.PerfilTipo.Admin)
            {
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Admin), (short)Enumeradores.PerfilTipo.Admin));
                //listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Escritorio), (short)Enumeradores.PerfilTipo.Escritorio));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Analista), (short)Enumeradores.PerfilTipo.Analista));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.RegionalAdmin), (short)Enumeradores.PerfilTipo.RegionalAdmin));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Regional), (short)Enumeradores.PerfilTipo.Regional));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.SupervisorAnalista), (short)Enumeradores.PerfilTipo.SupervisorAnalista));
            }
            else if (perfil == Enumeradores.PerfilTipo.SupervisorAnalista)
            {
                //listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Admin), (short)Enumeradores.PerfilTipo.Admin));
                //listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Escritorio), (short)Enumeradores.PerfilTipo.Escritorio));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Analista), (short)Enumeradores.PerfilTipo.Analista));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.RegionalAdmin), (short)Enumeradores.PerfilTipo.RegionalAdmin));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Regional), (short)Enumeradores.PerfilTipo.Regional));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.SupervisorAnalista), (short)Enumeradores.PerfilTipo.SupervisorAnalista));

            }
            else if (perfil == Enumeradores.PerfilTipo.Analista)
            {
                //listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Admin), (short)Enumeradores.PerfilTipo.Admin));
                //listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Escritorio), (short)Enumeradores.PerfilTipo.Escritorio));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Analista), (short)Enumeradores.PerfilTipo.Analista));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.RegionalAdmin), (short)Enumeradores.PerfilTipo.RegionalAdmin));
                listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.Regional), (short)Enumeradores.PerfilTipo.Regional));
                //listaPerfil.Add(new DictionaryEntry(EnumeradoresDescricao.PerfilTipo(Enumeradores.PerfilTipo.SupervisorAnalista), (short)Enumeradores.PerfilTipo.SupervisorAnalista));
            }
            else if (perfil == Enumeradores.PerfilTipo.Regional)
            {
                //NAO ENCHERGA NADA

            }
            else if (perfil == Enumeradores.PerfilTipo.Regional)
            {
                //NAO ENCHERGA NADA
            }

            ViewBag.ListaPerfilTipo = listaPerfil;

            var listaRegional = _repRegional.All().ToList();
            foreach (var item in listaRegional)
            {
                item.Regional = DbRotinas.Descriptografar(item.Regional);
            }
            listaRegional = listaRegional.OrderBy(x => x.CodRegionalCliente).ToList();
            ViewBag.ListaRegional = listaRegional.ToList();


        }

        [Authorize]
        [HttpGet]
        public override ActionResult Cadastro(int id = 0)
        {
            var _user = WebRotinas.ObterUsuarioLogado();
            if (id > 0)
            {
                if (DbRotinas.ConverterParaBool(_user.Admin) == false)
                {
                    if (id != _user.Cod)
                    {
                        return RedirectToAction("Index", new { msg = $"Acesso negado!" });
                    }
                }
            }
            return base.Cadastro(id);
        }
        public override ActionResult Cadastro(Usuario dados)
        {
            var baseEntity = _repository.FindById(dados.Cod);
            if (dados.Cod == 0)
            {
                baseEntity = new Usuario();
            }
            else
            {
                dados = validarModificacaoSenha(dados);
            }
            //informação alteradas
            var user = WebRotinas.ObterUsuarioLogado();
            if (user.PerfilTipo == Enumeradores.PerfilTipo.Admin)
            {
                baseEntity.Nome = dados.Nome;
                baseEntity.Email = dados.Email;
                baseEntity.Login = dados.Login;
            }

            if (dados.Cod == WebRotinas.ObterUsuarioLogado().Cod)
            {
                baseEntity.PerfilTipo = (short)WebRotinas.ObterUsuarioLogado().PerfilTipo;
            }
            else
            {
                baseEntity.PerfilTipo = dados.PerfilTipo;
            }

            baseEntity.Admin = dados.Admin;
            baseEntity.CodRegional = dados.CodRegional;
            baseEntity.IdiomaTipo = dados.IdiomaTipo;
            //valida os acesso
            baseEntity.SistemaCobranca = WebRotinas.ObterUsuarioLogado().SistemaCobranca == true;

            if (baseEntity.PerfilTipo == (short)Enumeradores.PerfilTipo.Regional || baseEntity.PerfilTipo == (short)Enumeradores.PerfilTipo.RegionalAdmin || baseEntity.PerfilTipo == (short)Enumeradores.PerfilTipo.Analista || baseEntity.PerfilTipo == (short)Enumeradores.PerfilTipo.SupervisorAnalista)
            {
                baseEntity.Admin = false;
            }
            if (baseEntity.PerfilTipo == (short)Enumeradores.PerfilTipo.Admin)
            {
                baseEntity.Admin = true;
            }
            if (DbRotinas.ConverterParaBool(dados.Bloqueado))
            {
                baseEntity.MotivoBloqueio = dados.MotivoBloqueio;
                baseEntity.DataBloqueado = DateTime.Now;
                baseEntity.Bloqueado = dados.Bloqueado;
            }
            else
            {
                baseEntity.MotivoBloqueio = null;
                baseEntity.DataBloqueado = null;
                baseEntity.Bloqueado = dados.Bloqueado;
            }


            return base.Cadastro(baseEntity);

        }

        private Usuario validarModificacaoSenha(Usuario dados)
        {
            // SO PODERA REDEFINIR A SENHA DO MESMO USUARIO
            if (dados.Cod == WebRotinas.ObterUsuarioLogado().Cod)
            {
                var SenhaAtual = DbRotinas.Descriptografar(Request.Form["SenhaAtual"]);
                var NovaSenha = DbRotinas.Descriptografar(Request.Form["NovaSenha"]);
                var ConfirmarSenha = DbRotinas.Descriptografar(Request.Form["ConfirmarSenha"]);
                var baseEntity = _dbUsuario.FindById(dados.Cod);
                if (baseEntity.Senha == SenhaAtual)
                {
                    dados.Senha = ConfirmarSenha;
                }
            }

            return dados;
        }

        public override void Ok(Usuario dados, HttpRequestBase Request)
        {


            if (!dados.DataPrimeiroAcesso.HasValue && !dados.DataAlteracao.HasValue)
            {
                EnviarSenha(dados.Cod);
            }
            if (dados.Cod == WebRotinas.ObterUsuarioLogado().Cod)
            {
                return;
            }

            var idsFilias = Request.Form["FilialVinculadas"].Split(',').ToList();

            //REMOVE AS PARAMETRIZACOES DO PARA OUTRAS FILIAS
            var _user = _dbUsuario.FindById(dados.Cod);
            var usuariosRegionais = _user.UsuarioRegionals.Where(x => !x.DataExcluido.HasValue).Select(x => x.CodRegional).ToList();
            List<int> regionaisExcluir = new List<int>();
            if (idsFilias.Count() > 0)
            {
                foreach (var item in usuariosRegionais)
                {
                    if (!idsFilias.Contains(DbRotinas.ConverterParaString(item)))
                    {
                        var _parametros = _dbParametroAtendimento.All().Where(x => x.CodUsuario == _user.Cod && x.CodRegional == item).Select(x => x.Cod).ToList();
                        foreach (var produto in _dbParametroAtendimentoProduto.All().Where(x => _parametros.Contains((int)x.CodParametroAtendimento)).ToList())
                        {
                            _dbParametroAtendimentoProduto.Delete(produto.Cod);
                        }
                        foreach (var rota in _dbParametroAtendimentoRota.All().Where(x => _parametros.Contains((int)x.CodParametroAtendimento)).ToList())
                        {
                            _dbParametroAtendimentoRota.Delete(rota.Cod);
                        }
                        foreach (var param in _dbParametroAtendimento.All().Where(x => _parametros.Contains(x.Cod)).ToList())
                        {
                            _dbParametroAtendimento.Delete(param.Cod);
                        }

                        var _vinculoRemover = _dbUsuarioRegional.All().Where(x => x.CodUsuario == _user.Cod && x.CodRegional == item && !x.DataExcluido.HasValue).ToList();
                        foreach (var row in _vinculoRemover)
                        {
                            _dbUsuarioRegional.Delete(row.Cod);
                        }
                    }
                }

            }


            //Repository<UsuarioRegional> _dbUsuarioRegional = new Repository<UsuarioRegional>();
            //if (dados.UsuarioRegionals.Any())
            //{
            //    //DELETA OS VINCULOS 
            //    _dbUsuarioRegional.Delete(x => x.CodUsuario == dados.Cod);
            //}
            //ROTINA DOS VINCULOS DOS CADASTROS
            foreach (var item in idsFilias)
            {
                int cod = DbRotinas.ConverterParaInt(item);
                if (cod == 0)
                    continue;

                var vinculo = new UsuarioRegional()
                {
                    CodUsuario = dados.Cod,
                    CodRegional = cod
                };

                //VERIFICA SE ESTE VINCULO JA EXISTE
                var entityVinculo = _dbUsuarioRegional
                    .All()
                    .Where(x =>
                            x.CodUsuario == vinculo.CodUsuario
                            && x.CodRegional == vinculo.CodRegional
                            )
                    .FirstOrDefault();
                if (entityVinculo == null)
                {
                    _dbUsuarioRegional.CreateOrUpdate(vinculo);
                }
                else
                {
                    entityVinculo.DataAlteracao = DateTime.Now;
                    _dbUsuarioRegional.Update(entityVinculo);

                }

            }
        }

        public JsonResult SalvarAcoes(long id, string projetos)
        {
            return Json(new { Ok = true });
        }
        public JsonResult RedefinirSenha(int Cod = 0)
        {
            ReenviarSenha(Cod);
            return Json(new { Ok = true });
        }
        public void EnviarSenha(int Cod = 0)
        {
            var parametros = _dbParametroPlataforma.All().FirstOrDefault();
            var dados = _dbUsuario.FindById(Cod);
            dados.Senha = Db.Rotinas.DbRotinas.Descriptografar(WebRotinas.GerarSenhaTemporaria());
            _dbUsuario.CreateOrUpdate(dados);

            StringBuilder Conteudo = new StringBuilder();
            Conteudo.Append("<p>Prezado, " + dados.Nome + "</p>");
            Conteudo.Append("<p>Acesso criado abaixo:</p><p><br></p>");
            Conteudo.Append("<p>Login: " + dados.Login + "</p>");
            Conteudo.Append("<p>Senha: " + DbRotinas.Descriptografar(dados.Senha) + "</p>");
            //Conteudo.Append("Sistema: http://intranetbrhomolog.otis.com:202/");
            Conteudo.Append("Sistema: http://intranetbrreport1.otis.com:202/");
            Conteudo.Append("<p><br></p>");
            Conteudo.Append("<p>Atenciosamente,</p>");
            Conteudo.Append("<p>OTIS</p>");
            Conteudo.Append("<p><br></p>");
            Conteudo.Append("<p><br></p>");

            LogEmail email = new LogEmail();
            email.Assunto = "Acesso Sistema - Cobrança";
            email.Conteudo = Convert.ToString(Conteudo);
            email.EmailDestinatario = dados.Email;
            email.EmailRemetente = parametros.EmailRemetente;
            email.EmailCopia = "alessandro.raquella@otis.com";
            email.HTML = true;
            email.NomeDestinatario = dados.Nome;
            email.NomeRemetente = parametros.NomeRemetente;
            email.Status = (short)Enumeradores.LogEmailStatus.AguardandoEnvio;
            email.TentativaEnvio = 0;
            email.Tipo = (short)Enumeradores.TipoAcaoCobranca.EMAIL;
            _dbLogEmail.CreateOrUpdate(email);
        }

        public void ReenviarSenha(int Cod = 0)
        {
            var parametros = _dbParametroPlataforma.All().FirstOrDefault();
            var dados = _dbUsuario.FindById(Cod);
            dados.Senha = Db.Rotinas.DbRotinas.Descriptografar(WebRotinas.GerarSenhaTemporaria());
            _dbUsuario.CreateOrUpdate(dados);

            StringBuilder Conteudo = new StringBuilder();
            Conteudo.Append("<p>Prezado, " + dados.Nome + "</p>");
            Conteudo.Append("<p>Sua senha foi alterada com sucesso!</p><p><br></p>");
            Conteudo.Append("<p>Login: " + dados.Login + "</p>");
            Conteudo.Append("<p>Senha: " + DbRotinas.Descriptografar(dados.Senha) + "</p>");
            //Conteudo.Append("Sistema: http://intranetbrhomolog.otis.com:202/");
            Conteudo.Append("Sistema: http://intranetbrreport1.otis.com:202/");
            Conteudo.Append("<p><br></p>");
            Conteudo.Append("<p>Atenciosamente,</p>");
            Conteudo.Append("<p>OTIS</p>");
            Conteudo.Append("<p><br></p>");
            Conteudo.Append("<p><br></p>");

            LogEmail email = new LogEmail();
            email.Assunto = "Acesso Sistema - Cobrança";
            email.Conteudo = Convert.ToString(Conteudo);
            email.EmailDestinatario = dados.Email;
            email.EmailRemetente = parametros.EmailRemetente;
            email.EmailCopia = "alessandro.raquella@otis.com";
            email.HTML = true;
            email.NomeDestinatario = dados.Nome;
            email.NomeRemetente = parametros.NomeRemetente;
            email.Status = (short)Enumeradores.LogEmailStatus.AguardandoEnvio;
            email.TentativaEnvio = 0;
            email.Tipo = (short)Enumeradores.TipoAcaoCobranca.EMAIL;
            _dbLogEmail.CreateOrUpdate(email);
        }


        [HttpPost]
        public JsonResult GetListData(DTParameters param)
        {
            try
            {
                var dtsource = DataList();
                var result = WebRotinas.DTRotinas.DTResult(param, dtsource);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }

        public IQueryable<object> DataList()
        {
            var dados = _dbUsuario.All().ToList();
            IQueryable<object> data = null;
            data = dados
                        .Select(x => new
                        {
                            Cod = $"{DbRotinas.ConverterParaString(x.Cod)}",
                            Nome = x.Nome,
                            Email = x.Email,
                            Login = x.Login,
                            Perfil = EnumeradoresDescricao.PerfilTipo((Enumeradores.PerfilTipo)x.PerfilTipo),
                            DataUltimoAcesso = $"{x.DataUltimoAcesso:dd/MM/yyyy HH:mm:ss}",
                            DataBloqueo = $"{x.DataBloqueado:dd/MM/yyyy HH:mm:ss}"
                        }).AsQueryable();

            return data;
        }

        [HttpGet]
        public JsonResult VerificarRegional()
        {
            try
            {
                var _usuario = WebRotinas.ObterUsuarioLogado();
                if (_usuario.PerfilTipo != Enumeradores.PerfilTipo.Admin)
                {
                    return Json(new { OK = false, Status = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { OK = false, Status = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        //VERIFICA A TROCA DE REGIONAL,
        [HttpGet]
        public JsonResult VerificaTrocaRegional(int user, string regionais)
        {
            try
            {
                var _user = _dbUsuario.FindById(user);
                var usuariosRegionais = _user.UsuarioRegionals.Where(x => !x.DataExcluido.HasValue).Select(x => x.CodRegional).ToList();
                List<int> regionaisExcluir = new List<int>();
                List<string> regionaisAtuais = regionais.Split(',').ToList();
                foreach (var item in usuariosRegionais)
                {
                    if (!regionaisAtuais.Contains(DbRotinas.ConverterParaString(item)))
                    {
                        regionaisExcluir.Add(DbRotinas.ConverterParaInt(item));
                    }
                }
                if (regionaisExcluir.Count() > 0)
                {
                    return Json(new { OK = false, StatusExcluir = true, Mensagem = $"Ao mover um atendete de filial suas parametrizações de atendimento serão apagadas.\n Deseja continuar mesmo assim?" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { OK = false, StatusExcluir = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public override void validacaoCustom(Usuario entity)
        {
            var originalEntity = _repository.FindById(entity.Cod);
            if (entity.Cod == 0)
            {
                originalEntity = new Usuario();
            }
            if (string.IsNullOrEmpty(originalEntity.Senha))
            {
                entity.Senha = WebRotinas.GerarSenhaTemporaria();
            }
            else
            {
                entity.Senha = originalEntity.Senha;

                string ConfirmarSenha = Request.Form["ConfirmarSenha"];
                string NovaSenha = Request.Form["NovaSenha"];
                string SenhaAtual = Request.Form["SenhaAtual"];

                if (!string.IsNullOrEmpty(SenhaAtual))
                {
                    if (SenhaAtual == DbRotinas.Descriptografar(originalEntity.Senha))
                    {
                        if (NovaSenha == ConfirmarSenha && !string.IsNullOrEmpty(NovaSenha))
                        {
                            if (NovaSenha.Length < 4)
                                ModelState.AddModelError("NovaSenha", "As senhas tem que conter no minimo 4 digitos.");
                            else
                                entity.Senha = DbRotinas.Descriptografar(NovaSenha);
                        }
                        else
                        {
                            ModelState.AddModelError("NovaSenha", "As senhas digitadas não conferem");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("SenhaAtual", "As Senha Atual não confere");
                    }
                }
            }


            if (_repository.FindAll(x => x.Login.ToLower() == entity.Login.ToLower() && x.Cod != entity.Cod && !x.DataExcluido.HasValue).Any())
            {
                ModelState.AddModelError("Login", "Este email ou login já esta sendo utilizado");
            }

        }

        [HttpGet]
        public JsonResult GetUsuarios(string regional, string search)
        {
            var regionais = regional.Split(',').Select(x => DbRotinas.ConverterParaInt(x)).ToList();

            var usuario = WebRotinas.ObterUsuarioLogado();
            var usuariosAtendimentos = _dbUsuarioRegional
                                            .All()
                                            .Where(x => regionais.Contains(x.CodRegional))
                                            .Select(x => x.Usuario)
                                            .ToList();
            usuariosAtendimentos = FiltrarUsuariosAtendimentoPorPerfil(usuariosAtendimentos);

            var lista = usuariosAtendimentos.Where(x => x.Nome.Contains(search)).Take(30).ToList();

            return Json(new
            {
                items = from p in lista select new { id = p.Cod, text = p.Nome }
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAtendente(string r = null)
        {
            var regionais = r.Split(',').Select(x => DbRotinas.ConverterParaInt(x)).ToList();

            var usuario = WebRotinas.ObterUsuarioLogado();
            //SELECIONA OS USUARIOS VINCULADOS AS REGIONAIS
            var usuariosAtendimentos = _dbUsuarioRegional
                                            .All()
                                            .Where(x => (regionais.Contains(x.CodRegional) || r == null) && !x.DataExcluido.HasValue)
                                            .Select(x => x.Usuario)
                                            .Where(x => !x.DataExcluido.HasValue)
                                            .ToList();
            usuariosAtendimentos = FiltrarUsuariosAtendimentoPorPerfil(usuariosAtendimentos);

            var lista = usuariosAtendimentos.ToList();

            return Json(new
            {
                items = from p in lista.GroupBy(x => x.Cod).Select(g => g.First()) select new { id = p.Cod, text = p.Nome }
            }, JsonRequestBehavior.AllowGet);
        }

        private List<Usuario> FiltrarUsuariosAtendimentoPorPerfil(List<Usuario> listaUsuarios)
        {
            var _usuario = WebRotinas.ObterUsuarioLogado();
            switch (_usuario.PerfilTipo)
            {
                case Enumeradores.PerfilTipo.Admin:
                    return listaUsuarios;
                case Enumeradores.PerfilTipo.Analista:
                    return listaUsuarios;
                case Enumeradores.PerfilTipo.Atendimento:
                    return listaUsuarios.Where(x => x.Cod == _usuario.Cod).ToList();
                case Enumeradores.PerfilTipo.Regional:
                    return listaUsuarios.Where(x => x.Cod == _usuario.Cod).ToList();
                case Enumeradores.PerfilTipo.RegionalAdmin:
                    List<int> regionaisIds = WebRotinas.ObterRegionailIdsDoUsuarioLogado();
                    return _dbUsuarioRegional.All().Where(x => regionaisIds.Contains(x.CodRegional) && x.Usuario.PerfilTipo == (short)Enumeradores.PerfilTipo.Regional).Select(x => x.Usuario).ToList();
                case Enumeradores.PerfilTipo.SupervisorAnalista:
                    return listaUsuarios;
                default:
                    return listaUsuarios.Where(x => x.Cod == _usuario.Cod).ToList();
            }

        }

        [HttpGet, Authorize]
        public JsonResult PesquisarAtendenteFila(string q)
        {
            var _user = WebRotinas.ObterUsuarioLogado();
            var qryUsuario = _dbUsuario.All().Where(x => !x.DataExcluido.HasValue);
            List<Usuario> lista = new List<Usuario>();
            if (_user.PerfilTipo == Enumeradores.PerfilTipo.Admin)
            {
                lista = qryUsuario.Where(x =>
                                         x.Nome.ToLower().Contains(q.ToLower())
                                         && (x.PerfilTipo == (short)Enumeradores.PerfilTipo.Regional
                                            || x.PerfilTipo == (short)Enumeradores.PerfilTipo.RegionalAdmin)).Take(30).ToList();
            }
            else
            {
                var regionais = _dbUsuarioRegional.All().Where(x => x.CodUsuario == _user.Cod && !x.DataExcluido.HasValue).Select(x => x.CodRegional).ToList();
                var usuarios = _dbUsuarioRegional.All().Where(x => regionais.Contains(x.CodRegional)).Select(x => x.CodUsuario).ToList();

                lista = qryUsuario.Where(x =>
                                         x.Nome.ToLower().Contains(q.ToLower())
                                         && usuarios.Contains(x.Cod)
                                         && (x.PerfilTipo == (short)Enumeradores.PerfilTipo.Regional
                                            || x.PerfilTipo == (short)Enumeradores.PerfilTipo.RegionalAdmin)).Take(30).ToList();
            }

            return Json(new { items = from p in lista select new { id = p.Cod, text = $"{p.Nome} - {p.Login}" } }, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        [HttpGet]
        public JsonResult PesquisarAtendente(string q)
        {
            var lista = _dbUsuario.All().Where(x => x.PerfilTipo == (short)Enumeradores.PerfilTipo.Regional && x.Nome.ToLower().Contains(q.ToLower())).Take(30).ToList();

            return Json(new { items = from p in lista select new { id = p.Cod, text = $"{p.Nome} - {p.Login}" } }, JsonRequestBehavior.AllowGet);
        }
    }
}