﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Threading.Tasks;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Rotinas;
using Cobranca.Db.Suporte;
using System.IO;

namespace Cobranca.Web.Controllers
{
    public class DocumentoController : Controller
    {
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();

        public Repository<ContratoDocumento> _dbContratoDocumento = new Repository<ContratoDocumento>();
        ListasRepositorio _dbListas = new ListasRepositorio();
        [HttpGet]
        public ActionResult Modal(int idCliente = 0)
        {
            var model = new ContratoDocumento();
            List<Contrato> contratos = new List<Contrato>();
            contratos = _dbContrato.All().Where(x => x.CodClientePrincipal == idCliente).ToList();
            foreach (var item in contratos)
            {
                item.NumeroContrato = DbRotinas.Descriptografar(item.NumeroContrato);
            }

            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            if (usuarioLogado.CodRegional > 0)
            {
                contratos = (from p in contratos
                             where p.TabelaRegionalProduto.CodRegional == usuarioLogado.CodRegional
                             select p).ToList();
            }
            var listaContratos = from p in contratos select new DictionaryEntry(p.NumeroContrato, p.Cod);
            ViewBag.Contratos = listaContratos.ToList();
            ViewBag.ListaTipoDocumento = _dbListas.TipoDocumento();
            model.CodCliente = idCliente;
            return PartialView("_form", model);
        }
        [HttpPost]
        public JsonResult Salvar(ContratoDocumento doc)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                if (string.IsNullOrEmpty(doc.ArquivoNome))
                {
                    ModelState.AddModelError("ArquivoNome", "Dados inválidos! Selecione um documento");
                }
                if (!doc.CodTipoDocumento.HasValue)
                {
                    ModelState.AddModelError("CodTipoDocumento", "Dados inválidos! Selecione um tipo de documento");
                }
                if (ModelState.IsValid)
                {
                    if (doc.CodTipoDocumento == 0) { doc.CodTipoDocumento = null; }
                    doc.ArquivoGuid = Guid.NewGuid().ToString();
                    AnexarDocumento(ref doc, Request);
                    _dbContratoDocumento.CreateOrUpdate(doc);
                    op.OK = true;
                }
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
                op.Mensagem = op.Mensagem;
            }

            return Json(op);
        }

        private void AnexarDocumento(ref ContratoDocumento doc, HttpRequestBase request)
        {
            string pathUpload = Server.MapPath("~/Content/files");
            string pathDefinitivo = Server.MapPath(string.Format("~/Documentos/Contrato/", doc.Cod));
            doc.ArquivoCaminho = String.Format("~/Documentos/Contrato/{0}", doc.ArquivoGuid);
            string file = doc.ArquivoNome;

            if (!Directory.Exists(pathDefinitivo))
                Directory.CreateDirectory(pathDefinitivo);
            if (file != null)
            {
                string pathDoc = Path.Combine(pathUpload, file);
                FileInfo fileInfo = new FileInfo(pathDoc);
                if (fileInfo.Exists)
                {
                    fileInfo.MoveTo(Path.Combine(pathDefinitivo, doc.ArquivoGuid));
                }
            }
        }

        [HttpGet]
        public virtual ActionResult Download(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                // Problem - Log the error, generate a blank file,
                //           redirect to another controller action - whatever fits with your application
                return new EmptyResult();
            }
        }
    }
}