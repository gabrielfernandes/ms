﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class RotaController : BasicController<Rota>
    {
        DateTime? DataNascimento = System.DateTime.Today;

        public const string _filtroSessionId = "FiltroPesquisaId";
        public Dados _qry = new Dados();

        ListasRepositorio _dbListas = new ListasRepositorio();

        public override ActionResult Cadastro(Rota dados)
        {
            dados.Nome = DbRotinas.Criptografar(dados.Nome);
            dados.Descricao = DbRotinas.Criptografar(dados.Descricao);
            return base.Cadastro(dados);
        }

    }
}