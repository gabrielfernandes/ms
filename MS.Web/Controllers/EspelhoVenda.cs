﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Data.Entity.Validation;
using System.Diagnostics;
using Recebiveis.Web.Models;

namespace Cobranca.Web.Controllers
{
    public class EspelhoVendaController : Controller
    {
        public Repository<PropostaProspect> _dbPropostaProspect = new Repository<PropostaProspect>();
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();
        public Repository<TabelaBloco> _dbBloco = new Repository<TabelaBloco>();
        public Repository<TabelaUnidade> _dbUnidade = new Repository<TabelaUnidade>();
        public Repository<TabelaUnidadeStatusHistorico> _dbUnidadeHistorico = new Repository<TabelaUnidadeStatusHistorico>();
        public Repository<TabelaUnidadeReserva> _dbUnidadeReserva = new Repository<TabelaUnidadeReserva>();

        [HttpGet]
        public ActionResult Index(int cdProspect = 0, int cdProposta = 0)
        {
            ViewBag.CodProposta = cdProposta;
          
            var listaContrutoras = _dbConstrutora.All().ToList();
            var listaEmpreendimento = _dbEmpreendimento.All().ToList();
            listaEmpreendimento.Insert(0, new TabelaEmpreendimento());

            ViewBag.ListaConstrutora = from p in listaContrutoras.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);
            ViewBag.ListaEmpreendimento = from p in listaEmpreendimento.OrderBy(x => x.NomeEmpreendimento).ToList() select new DictionaryEntry(p.NomeEmpreendimento, p.Cod);
           
         
            return View();
        }

        [HttpPost]
        public JsonResult PesquisarUnidade(int CodBloco = 0)
        {
            var lista = _dbUnidade.All().Where(x => x.CodBloco == CodBloco).ToList();
            var dados = from p in lista select new { Cod = p.Cod, p.NumeroUnidade, p.TabelaBloco.NomeBloco, p.Andar };

            var d = from p in lista.OrderByDescending(c => c.Andar).GroupBy(x => x.Andar)
                    select new
                    {
                        Andar = p.Key,
                        QdtUnidades = p.Count(),
                        lUnidades = from u in p.ToList() select new
                        {
                            NumeroUnidade = u.NumeroUnidade,
                            QtdDormitorio = u.QtdDormitorio,
                            Andar = u.Andar,
                            Bloco = u.TabelaBloco.NomeBloco,
                            Status = u.Status,
                            ValorVenda = u.Valor,
                            Metragem = u.Metragem,
                            PNE = u.PNE,
                            CodUnidade = u.Cod
                        }
                    };

            return Json(new { OK = true, ListaUnidades = d });

            
        }

        [HttpPost]
        public JsonResult AlteraStatusUnidade(int CodUnidade, short? StatusUnidade, int? CodUsuario, DateTime? DataReserva, String LoginReserva)
        {
            try
            {
                var dados = _dbUnidade.FindById(CodUnidade);
                dados.Status = StatusUnidade;
                dados.DataAlteracao = DateTime.Now;
                dados.DataReserva = DataReserva;
                if (StatusUnidade == 4)
                {
                    dados.LoginReserva = LoginReserva; 
                }
                _dbUnidade.Update(dados);

                TabelaUnidadeStatusHistorico historico = new TabelaUnidadeStatusHistorico();
                historico.Status = (short)StatusUnidade;
                historico.CodUnidade = CodUnidade;
                historico.CodUsuario = CodUsuario;
                _dbUnidadeHistorico.CreateOrUpdate(historico);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult ReservaUnidade(TabelaUnidadeReserva dados)
        {
            try
            {
                UnidadeStatusDb unidade = new UnidadeStatusDb();
                unidade.ReservarUnidade(dados, WebRotinas.ObterUsuarioLogado().Cod, "Unidade reservada");
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ListaHistoricoUnidade(int CodUnidade, int draw, int start, int length)
        {
            DataTableData d = new DataTableData();
            try
            {
                var ListaUnidade = _dbUnidadeHistorico.FindAll(x => x.CodUnidade == CodUnidade).ToList();
                ViewBag.ListaHistoricoUnidade = ListaUnidade.ToList();

                d.draw = draw;
                d.recordsTotal = length;
                int recordsFiltered = ListaUnidade.Count;
                d.data = from p in ListaUnidade.Skip(start).Take(length) select new { Status = Cobranca.Db.Models.EnumeradoresDescricao.StatusUnidade(p.Status), obs = p.Observacao, CodUsuario = p.Usuario.Nome, DataCadastro = p.DataCadastro };
                d.recordsFiltered = recordsFiltered;

                return Json(d, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(d, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ListarBlocoPorEmpreendimento(int CodEmpreendimento)
        {
            var lista = _dbBloco.All().Where(x => x.CodEmpreend == CodEmpreendimento).ToList();
            lista.Insert(0, new TabelaBloco());
            var dados = from p in lista select new { Cod = p.Cod, Text = p.NomeBloco };
            return Json(dados);
        }


    }
}