﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class RegionalController : BasicController<TabelaRegional>
    {
        public Repository<TabelaRegional> _repository = new Repository<TabelaRegional>();
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaRegionalGrupo> _dbRegionalGrupo = new Repository<TabelaRegionalGrupo>();

        public override ActionResult Cadastro(TabelaRegional dados)
        {
            dados.Regional = DbRotinas.Criptografar(dados.Regional);
            return base.Cadastro(dados);
            
        }

        public override void carregarCustomViewBags()
        {
            var lista = _dbConstrutora.All().ToList();
            foreach (var item in lista)
            {
                item.NomeConstrutora = DbRotinas.Descriptografar(item.NomeConstrutora);
                item.CNPJ = DbRotinas.Descriptografar(item.CNPJ);
            }
            var listaContrutoras = lista.AsQueryable();
            ViewBag.ListaConstrutora = from p in listaContrutoras.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);
            base.carregarCustomViewBags();
        }
        public override IQueryable<object> carregarListaDados()
        {
            return _repository
                      .All()
                      .ToList()
                      .Select(x => new
                      {
                          Cod = x.Cod,
                          NomeConstrutora = x.CodConstrutora > 0 ? DbRotinas.Descriptografar(x.TabelaConstrutora.NomeConstrutora) : "",
                          Regional = DbRotinas.Descriptografar(x.Regional),
                          Responsavel = x.Responsavel,
                          Telefone = x.Telefone,
                          Email = x.Email
                      })
                      .AsQueryable();
        }
        public override string carregarListaParaExcel(IQueryable<TabelaRegional> Dados, string filename)
        {
            var obj = Dados.ToList()
                    .Select(x => new
                    {
                        NomeConstrutora = x.CodConstrutora > 0 ? DbRotinas.Descriptografar(x.TabelaConstrutora.NomeConstrutora) : "",
                        Regional = DbRotinas.Descriptografar(x.Regional),
                        Responsavel = x.Responsavel,
                        Telefone = x.Telefone,
                        Email = x.Email
                    }
            );
            ConverterListaParaExcel.GerarCSVDeGenericList(obj.ToList(), filename);
            return @Idioma.Traduzir("Filial");
        }

        [HttpGet]
        public ActionResult AddFormGrupo(int r = 0)
        {
            TabelaRegionalGrupo model = new TabelaRegionalGrupo();
            carregarCustomViewBags();
            model.CodRegional = r;
            return PartialView("_formGrupo", model);
        }

        [HttpPost]
        public ActionResult SalvarGrupo(string[] Cod, string[] CodConstrutora, string[] CodRegional, string[] CodRegionalCliente, string[] Descricao)
        {
            try
            {
                List<TabelaRegionalGrupo> grupos = new List<TabelaRegionalGrupo>();
                for (int i = 0; i < Cod.Length; i++)
                {
                    grupos.Add(new TabelaRegionalGrupo
                    {
                        Cod = DbRotinas.ConverterParaInt(Cod[i]),
                        CodConstrutora = DbRotinas.ConverterParaInt(CodConstrutora[i]),
                        CodRegional = DbRotinas.ConverterParaInt(CodRegional[i]),
                        CodRegionalCliente = CodRegionalCliente[i],
                        Descricao = Descricao[i]
                    });
                }
                List<TabelaRegionalGrupo> _Grupo = new List<TabelaRegionalGrupo>();
                foreach (var _grupo in grupos)
                {
                    var _entity = _dbRegionalGrupo.FindById(_grupo.Cod);
                    if (_entity == null)
                    {
                        _entity = Activator.CreateInstance<TabelaRegionalGrupo>();
                        _entity.DataCadastro = DateTime.Now;
                    }
                    _entity.CodConstrutora = DbRotinas.ConverterParaInt(_grupo.CodConstrutora);
                    _entity.CodRegional = DbRotinas.ConverterParaInt(_grupo.CodRegional);
                    _entity.CodRegionalCliente = _grupo.CodRegionalCliente;
                    _entity.Descricao = _grupo.Descricao;
                    if (!String.IsNullOrEmpty(_entity.CodRegionalCliente))
                    {
                        _dbRegionalGrupo.CreateOrUpdate(_entity);
                    }

                }

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult DeleteGrupo(int rg)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                _dbRegionalGrupo.Delete(rg);

                op.OK = true;
                op.Mensagem = $"Operação Realizada com Sucesso!";
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }

            return Json(op);
        }
    }
}