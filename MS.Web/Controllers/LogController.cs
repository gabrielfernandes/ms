﻿using Aplication.Log;
using Cobranca.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cobranca.Web.Controllers
{
    public class LogController : ApiController
    {
        [Route("log/testeenvio")]
        [HttpGet]
        public LogServicoViewModel TesteLog(string destinatario, string assunto, string conteudo)
        {
            LogServicoViewModel _param = new LogServicoViewModel();
            _param.destinatario = destinatario;
            _param.assunto = assunto;
            _param.counteudo = conteudo;
            var response = Comunicar.EnviarLog(_param.assunto, "", _param.counteudo);
            _param.OK = response.Item1;
            _param.resposta = response.Item2;
            return _param;
        }
    }
}