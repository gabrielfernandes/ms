﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class AcaoCobrancaController : BasicController<TabelaAcaoCobranca>
    {
        Repository<TabelaAcaoCobranca> _dbTabelaAcaoCobranca = new Repository<TabelaAcaoCobranca>();

        public override void carregarCustomViewBags()
        {
            ViewBag.ListaTipoAcaoCobranca = EnumeradoresLista.TipoAcaoCobranca();
            base.carregarCustomViewBags();
        }

    }
}