﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class NaturezaController : BasicController<TabelaNatureza>
    {
        public Repository<TabelaTipoCarteira> _dbTipoCarteira = new Repository<TabelaTipoCarteira>();

        public override void carregarCustomViewBags()
        {
            var listaDeTipoCarteira = _dbTipoCarteira.All();

            ViewBag.ListaTipoCarteira = from p in listaDeTipoCarteira.OrderBy(x => x.TipoCarteira).ToList() select new DictionaryEntry(p.TipoCarteira, p.Cod);
          
            
            
            base.carregarCustomViewBags();
        }
      
    }
}