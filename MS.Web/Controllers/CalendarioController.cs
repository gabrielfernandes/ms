﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;


namespace Cobranca.Web.Controllers
{

    public class CalendarioController : Controller
    {
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<ContratoHistorico> _dbContratoHistorico = new Repository<ContratoHistorico>();
        public Dados _qry = new Dados();

        public void carregarCustomViewBags()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "", Value = "" });
            items.Add(new SelectListItem { Text = "Janeior", Value = "1" });
            items.Add(new SelectListItem { Text = "Fevereiro", Value = "2" });
            items.Add(new SelectListItem { Text = "Março", Value = "3" });
            items.Add(new SelectListItem { Text = "Abril", Value = "4" });
            items.Add(new SelectListItem { Text = "Maio", Value = "5" });
            items.Add(new SelectListItem { Text = "Junho", Value = "6" });
            items.Add(new SelectListItem { Text = "Julho", Value = "7" });
            items.Add(new SelectListItem { Text = "Agosto", Value = "8" });
            items.Add(new SelectListItem { Text = "Setembro", Value = "9" });
            items.Add(new SelectListItem { Text = "Outubro", Value = "10" });
            items.Add(new SelectListItem { Text = "Novembro", Value = "11" });
            items.Add(new SelectListItem { Text = "Dezembro", Value = "12" });

            ViewBag.ListaMeses = from p in items select new DictionaryEntry(p.Text, p.Value);
        }

        [HttpGet]
        public ActionResult Index()
        {
            carregarCustomViewBags();
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult ListarCalendarioEventos(CalendarioDados dados)
        {
            var listaSolitacoes = FiltrarAgenda(dados);
            var resultado = new List<CalendarioEventos>();

            if (listaSolitacoes.Any())
            {
                DateTime dtInicio = listaSolitacoes.Min(x => x.DataAgenda);
                DateTime dtFim = listaSolitacoes.Max(x => x.DataAgenda);

                int totalDias = (int)(dtFim - dtInicio).TotalDays + 1;


                foreach (var item in listaSolitacoes)
                {
                    if (item.DataAgenda <= DateTime.Now)
                    {
                        item.Status = (short)Enumeradores.CalendarioStatus.Atrasado;
                    }
                    else
                    {
                        item.Status = (short)Enumeradores.CalendarioStatus.Agendado;
                    }

                    resultado.Add(MontarEnvento(item));
                }
                //for (int i = 1; i < listaSolitacoes.Count(); i++)
                //{
                //var lAguardando = listaSolitacoes.Where(x => dt >= x.DataAgenda && dt <= x.DataAgenda && x.Status == (short)Enumeradores.CalendarioStatus.Aguardando).ToList();
                //var lAprovado = listaSolitacoes.Where(x => dt >= x.DataAgenda && dt <= x.DataAgenda && x.Status == (short)Enumeradores.CalendarioStatus.Aprovado).ToList();
                //var lCancelado = listaSolitacoes.Where(x => dt >= x.DataAgenda && dt <= x.DataAgenda && x.Status == (short)Enumeradores.CalendarioStatus.Cancelado).ToList();
                //var lReprovado = listaSolitacoes.Where(x => dt >= x.DataAgenda && dt <= x.DataAgenda && x.Status == (short)Enumeradores.CalendarioStatus.Reprovado).ToList();

                //if (lAguardando.Any())
                //    resultado.Add(MontarEnvento(dt, lAguardando));
                //if (lAprovado.Any())
                //    resultado.Add(MontarEnvento(dt, lAprovado));
                //if (lCancelado.Any())
                //    resultado.Add(MontarEnvento(dt, lCancelado));
                //if (lReprovado.Any())
                //    resultado.Add(MontarEnvento(dt, lReprovado));

                //dt = dt.AddDays(1);

                //}


            }
            return Json(resultado);
        }

        private CalendarioEventos MontarEnvento(AgendaOperador item)
        {
            var info = new CalendarioEventos();

            info = new CalendarioEventos
            {
                id = string.Format("{0:ddMMyyyy}_{1}", item.DataAgenda, item.HistoricoId),
                title = string.Format("{0}", item.NomeCliente),
                start = string.Format("{0:yyyy-MM-dd HH:mm}", item.DataAgenda),
                end = string.Format("{0:yyyy-MM-dd HH:mm}", item.MotivoAtraso),
                description = string.Format("{0}</br>{1}", item.Sintese, item.MotivoAtraso),
                url = Url.Action("Historico", "Pesquisa", new { id = item.ContratoId }),
                className = tratarEventoCor(item.Status),
                icon = tratarEventoIcone(item.Status),
            };

            return info;
        }
        private string tratarEventoCor(short status)
        {
            string classe = "bg-info";

            var en = (Enumeradores.CalendarioStatus)status;

            switch (en)
            {
                case Enumeradores.CalendarioStatus.Agendado:
                    {
                        classe = "bg-info";
                        break;
                    }
                //case Enumeradores.CalendarioStatus.Aprovado:
                //    {
                //        classe = "bg-success";
                //        break;
                //    }
                case Enumeradores.CalendarioStatus.Atrasado:
                    {
                        classe = "bg-warning";
                        break;
                    }
                //case Enumeradores.CalendarioStatus.Reprovado:
                //    {
                //        classe = "bg-danger";
                //        break;
                //    }
                default:
                    break;
            }

            return classe;

        }
        private string tratarEventoIcone(short status)
        {
            string classe = "bg-info";

            var en = (Enumeradores.CalendarioStatus)status;

            switch (en)
            {
                case Enumeradores.CalendarioStatus.Agendado:
                    {
                        classe = "fa-clock-o";
                        break;
                    }
                //case Enumeradores.CalendarioStatus.Aprovado:
                //    {
                //        classe = "fa-check";
                //        break;
                //    }
                case Enumeradores.CalendarioStatus.Atrasado:
                    {
                        classe = "fa-exclamation-triangle ";
                        break;
                    }
                //case Enumeradores.CalendarioStatus.Reprovado:
                //    {
                //        classe = "fa-thumbs-o-down";
                //        break;
                //    }
                default:
                    break;
            }

            return classe;

        }

        private List<AgendaOperador> FiltrarAgenda(CalendarioDados model)
        {
            model.UsuarioId = Cobranca.Web.Rotinas.WebRotinas.ObterUsuarioLogado().Cod;

            List<AgendaOperador> resultado = _qry.ObterAgendaPorOperador(model.UsuarioId).ToList();
            return resultado.ToList();
        }
    }
}