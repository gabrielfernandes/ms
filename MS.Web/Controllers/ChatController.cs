﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cobranca.Web.Controllers
{
    public class ChatController : Controller
    {
        private Repository<Usuario>
        _repoUsuario = new Repository<Usuario>();

        private Repository<ChatAtendimento>
        _repoAtendimento = new Repository<ChatAtendimento>();

        private Repository<ChatAtendimentoTipo>
        _repoAtendimentoTipo = new Repository<ChatAtendimentoTipo>();

        private Repository<ChatAtendimentoResultado>
        _repoAtendimentoResultadoTipo = new Repository<ChatAtendimentoResultado>();

        private Repository<ChatAtendimentoMensagem>
        _repoAtendimentoMensagem = new Repository<ChatAtendimentoMensagem>();
        private Repository<Cliente>
        _repCliente = new Repository<Cliente>();

        private Repository<Usuario>
        _repUsuario = new Repository<Usuario>();

        

        public ActionResult Index()
        {
            
            ViewBag.ListaAtendimentos = _repoAtendimento.FindAll(x => !x.DataEncerrado.HasValue).OrderByDescending(x => x.DataCadastro).ToList();

            return View();
        }

        [HttpPost]
        public JsonResult AbrirAtendimento(int id = 0)
        {
            var dados = _repoAtendimento.FindById(id);
            int? usuarioId = WebRotinas.ObterUsuarioLogado().Cod;

            if (!dados.DataAtendimento.HasValue)
            {
                dados.DataAtendimento = DateTime.Now;
                dados.CodUsuario = usuarioId;
                _repoAtendimento.CreateOrUpdate(dados);

                _repoAtendimentoMensagem.CreateOrUpdate(new ChatAtendimentoMensagem()
                {
                    CodUsuario = usuarioId,
                    CodigoAtentimento = id,
                    Tipo = (short)Enumeradores.ChatMensagemTipo.Corretor,
                    Nome = String.Format("Olá meu nome é {0}, em que posso lhe ajudar?", WebRotinas.ObterUsuarioLogado().Nome),
                    Descricao = String.Format("Olá meu nome é {0}, em que posso lhe ajudar?", WebRotinas.ObterUsuarioLogado().Nome)

                });
            }
            return Json(new
            {
                Nome = dados.Nome,
                Email = dados.Email,
                Cliente = dados.Nome,
                Operador = dados.Usuario.Nome,
                Id = dados.Cod,
                Tipo = dados.CodAtendimentoTipo > 0 ? dados.ChatAtendimentoTipo.Nome : ""
            });
        }
        [HttpPost]
        public JsonResult CarregarMensagem(int id = 0, int ultimaMsgId = 0)
        {
            int usuarioId = WebRotinas.ObterUsuarioLogado().Cod;

            var lista = _repoAtendimentoMensagem.FindAll(x => x.CodigoAtentimento == id && x.Cod > ultimaMsgId).OrderBy(x => x.DataCadastro);

            var listaMsgNaoLidas = _repoAtendimentoMensagem.FindAll(x => x.CodigoAtentimento != id && !x.DataLida.HasValue && x.ChatAtendimento.CodUsuario == usuarioId & !x.ChatAtendimento.DataEncerrado.HasValue).GroupBy(x => x.CodigoAtentimento);


            //marca as mensagens como lida;
            foreach (var item in lista.Where(x => !x.DataLida.HasValue).ToList())
            {
                item.DataLida = DateTime.Now;
                _repoAtendimentoMensagem.CreateOrUpdate(item);
                //_repoAtendimentoMensagem.CreateOrUpdate(item);
            }
            _repoAtendimento.SaveChanges();

            var l = from p in lista.ToList()
                    select new
                    {
                        Mensagem = p.Descricao,
                        Data = string.Format("{0:dd/MM/yyyy} as {0:hh:mm}", p.DataCadastro),
                        Usuario = p.CodUsuario > 0 ? p.Usuario.Nome : p.ChatAtendimento.Nome,
                        Id = p.Cod,
                        AtendimentoId = p.CodigoAtentimento,
                        Hora = string.Format("{0:HH:mm:ss}", p.DataCadastro),
                        ArquivoNome = p.ArquivoNome,
                        pathArquivo = string.IsNullOrEmpty(p.ArquivoNome) ? null : Url.Content(Path.Combine("~/documentos/chats/", p.CodigoAtentimento.ToString(), p.Cod.ToString(), p.Arquivo))
                    };


            var naoLidas = from P in listaMsgNaoLidas
                           select new
                           {
                               AtendimentoId = P.Key,
                               Total = P.Count()
                           };

            return Json(new { mensagens = l, mensagensNaoLidas = naoLidas });
        }
        [HttpPost]
        public JsonResult EnivarMensagem(int id, string texto, short tp = 0)
        {
            ChatAtendimentoMensagem msg = new ChatAtendimentoMensagem();
            int? usuarioId = WebRotinas.ObterUsuarioLogado().Cod;
            string nm = WebRotinas.ObterUsuarioLogado().Nome;

            msg.CodigoAtentimento = id;
            msg.Descricao = texto;
            msg.Nome = texto;
            msg.CodUsuario = usuarioId;
            msg.Tipo = (short)Enumeradores.ChatMensagemTipo.Corretor;
            if (tp == 2)
            {
                msg.Arquivo = texto;
                msg.ArquivoNome = Path.GetFileName(texto);
            }
            msg = _repoAtendimentoMensagem.CreateOrUpdate(msg);

            if (tp == 2)
            {
                vincularDocumentoMensagem(msg);
            }

            return Json(new
            {
                Mensagem = msg.Descricao,
                Id = msg.Cod,
                Usuario = nm,
                Hora = string.Format("{0:HH:mm:ss}", DateTime.Now)
            });
        }

        private void vincularDocumentoMensagem(ChatAtendimentoMensagem msg)
        {
            string pathUpload = Server.MapPath("~/Content/files");

            if (!Directory.Exists(pathUpload))
            {
                Directory.CreateDirectory(pathUpload);
            }

            string pathConteudoMsg = Path.Combine("~/Documentos/chats", msg.CodigoAtentimento.ToString(), msg.Cod.ToString());

            string serverPath = Server.MapPath(pathConteudoMsg);

            if (!Directory.Exists(serverPath))
            {
                Directory.CreateDirectory(serverPath);
            }
            try
            {
                FileInfo f = new FileInfo(Path.Combine(pathUpload, msg.ArquivoNome));
                if (f.Exists)
                {
                    System.IO.File.Move(f.FullName, Path.Combine(serverPath, msg.ArquivoNome));
                }

            }
            catch (Exception ex)
            {

            }

        }

        [HttpGet]
        public JsonResult VerificarChamados()
        {
            
            int? usuarioId = WebRotinas.ObterUsuarioLogado().Cod;
            bool chatOn = true;

            var lista = _repoAtendimento.FindAll(x => !x.DataAtendimento.HasValue && !x.DataExcluido.HasValue && !x.CodUsuario.HasValue).ToList();


            var l = from p in lista
                    select new
                    {
                        Nome = p.Nome,
                        Data = string.Format("{0:dd/MM/yyyy} as {0:hh:mm}", p.DataCadastro),
                        Hora = string.Format("{0:HH:mm:ss}", p.DataCadastro),
                        TempoEmEspera = string.Format("{0:hh}:{0:mm}h", DateTime.Now - p.DataCadastro),
                        Empresa = p.Nome,
                        Tipo = p.CodAtendimentoTipo> 0 ? p.ChatAtendimentoTipo.Nome : "",
                        Id = p.Cod
                    };



            return Json(new { atendimentos = l, chatStatus = chatOn }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AlterarStatusChat(bool online = false)
        {
            int  usuarioId = WebRotinas.ObterUsuarioLogado().Cod;

            var usr = _repoUsuario.FindById(usuarioId);
            //usr.ChatStatus = online ? (short)Enumeradores.ChatStatus.Online : (short)Enumeradores.ChatStatus.Offline;
            //usr.ChatStatusData = DateTime.Now;
            _repoUsuario.CreateOrUpdate(usr);
            WebRotinas.CookieGravar("chatStatusSession", online ? "1" : "0");
            return Json(new { OK = true });
        }


        [HttpGet]
        public ActionResult HistoricoPesquisa()
        {
            ChatPesquisaViewModel model = new ChatPesquisaViewModel();            
            model.DataAte = DateTime.Now.AddMonths(1).AddDays(-DateTime.Now.Day);
            model.DataDe = DateTime.Now;
            CarregarListasViewBag(ref model);
            return View(model);
        }

        private void CarregarListasViewBag(ref ChatPesquisaViewModel model)
        {
            model.SelectListTipoAtendimento = new SelectList(_repoAtendimentoTipo.SelectList(x =>  !x.DataExcluido.HasValue), "value", "key", model.AtendimentoTipoId);
            model.SelectListTipoResultado = new SelectList(_repoAtendimentoResultadoTipo.SelectList(x => !x.DataExcluido.HasValue), "value", "key", model.ResultadoId);
        }

        [HttpPost]
        public ActionResult HistoricoResultado(ChatPesquisaViewModel filtro)
        {
            var resultado = _repoAtendimento.FindAll(x => !x.DataExcluido.HasValue && x.DataEncerrado.HasValue).ToList();


            filtro.Resultado = resultado;

           

            if (filtro.ResultadoId > 0)
                filtro.Resultado = filtro.Resultado.Where(x => x.CodResultado == filtro.ResultadoId).ToList();
            
            if (!string.IsNullOrEmpty(filtro.Nome))
                filtro.Resultado = filtro.Resultado.Where(x => x.Nome.Contains(filtro.Nome)).ToList();

            if (!string.IsNullOrEmpty(filtro.Email))
                filtro.Resultado = filtro.Resultado.Where(x => x.Nome.Contains(filtro.Email)).ToList();

            if (!string.IsNullOrEmpty(filtro.Email))
                filtro.Resultado = filtro.Resultado.Where(x => x.Nome.Contains(filtro.Email)).ToList();

            if (filtro.AtendimentoTipoId > 0)
                filtro.Resultado = filtro.Resultado.Where(x => x.CodAtendimentoTipo == filtro.AtendimentoTipoId).ToList();

            if (filtro.DataDe.HasValue)
                filtro.Resultado = filtro.Resultado.Where(x => x.DataCadastro >= filtro.DataDe.Value.Date).ToList();

            if (filtro.DataAte.HasValue)
                filtro.Resultado = filtro.Resultado.Where(x => x.DataCadastro <= filtro.DataAte.Value.Date).ToList();

            if (filtro.UsuarioId > 0)
                filtro.Resultado = filtro.Resultado.Where(x => x.CodUsuario == filtro.UsuarioId).ToList();


            return View(filtro.Resultado);
        }
        [HttpGet]
        public ActionResult HistoricoResultado()
        {
            var resultado = _repoAtendimento.FindAll(x => !x.DataExcluido.HasValue && x.DataEncerrado.HasValue).ToList();
            return View(resultado);
        }
        [HttpGet]
        public ActionResult HistoricoDetalhe(int id = 0)
        {
            var atendimento = _repoAtendimento.FindById(id);

            return View(atendimento);
        }
        [HttpPost]
        public JsonResult Finalizar(int id = 0)
        {

            var atentimento = _repoAtendimento.FindById(id);

            atentimento.DataEncerrado = DateTime.Now;

            ChatAtendimentoMensagem msg = new ChatAtendimentoMensagem();
            int? usuarioId = WebRotinas.ObterUsuarioLogado().Cod;

            msg.CodigoAtentimento = id;
            msg.Descricao = string.Format("Atendimento encerrado pelo operador {0} as {1}.", WebRotinas.ObterUsuarioLogado().Nome, DateTime.Now);
            msg.Nome = msg.Descricao;
            msg.CodUsuario = usuarioId;
            msg.Tipo = (short)Enumeradores.ChatMensagemTipo.Automatica;
            msg.DataCadastro = DateTime.Now;
            _repoAtendimentoMensagem.CreateOrUpdate(msg);

            _repoAtendimento.CreateOrUpdate(atentimento);

            return Json(new { OK = true });
        }
        [HttpPost]
        public JsonResult ObterListaOperadoresOnLine()
        {
            
            int usuarioId = WebRotinas.ObterUsuarioLogado().Cod;
            var operadores = _repoUsuario.FindAll(x => !x.DataExcluido.HasValue && x.PerfilTipo != (short)Enumeradores.PerfilTipo.Corretor && x.Cod != usuarioId).ToList();

            var lista = from p in operadores
                        select new
                        {
                            Id = p.Cod,
                            Nome = p.Nome,
                            TotalAtendimento = p.ChatAtendimentoes.Count(x => !x.DataEncerrado.HasValue)
                        };

            return Json(lista);
        }
        [HttpPost]
        public JsonResult TrasnferirAtendimento(int AtendimentoId, int OperadorId)
        {
            int usuarioId = WebRotinas.ObterUsuarioLogado().Cod;

            var atendimento = _repoAtendimento.FindById(AtendimentoId);
            atendimento.CodUsuario = OperadorId;

            ChatAtendimentoMensagem msg = new ChatAtendimentoMensagem();
            msg.CodigoAtentimento = AtendimentoId;
            msg.Descricao = string.Format("Atendimento transferido pelo operador {0} as {1}.", WebRotinas.ObterUsuarioLogado().Nome, DateTime.Now);
            msg.Nome = msg.Descricao;
            msg.CodUsuario = usuarioId;
            msg.Tipo = (short)Enumeradores.ChatMensagemTipo.Automatica;
            msg.DataCadastro = DateTime.Now;
            _repoAtendimentoMensagem.CreateOrUpdate(msg);

            _repoAtendimento.CreateOrUpdate(atendimento);

            return Json(new { OK = true });
        }

    }
}