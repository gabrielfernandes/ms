﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class TabelaParametroAtendimentoController : Controller
    {
        public Repository<TabelaParametroAtendimento> _dbTabelaParametroAtendimento = new Repository<TabelaParametroAtendimento>();
        public Repository<TabelaTipoCobranca> _dbTabelaTipoCobranca = new Repository<TabelaTipoCobranca>();

        public Repository<TabelaEquipe> _dbTabelaEquipe = new Repository<TabelaEquipe>();
        public Repository<TabelaAcaoCobranca> _dbTabelaAcaoCobranca = new Repository<TabelaAcaoCobranca>();
        public Repository<Usuario> _dbUsuario = new Repository<Usuario>();
        public Repository<TabelaParametroAtendimentoUsuario> _dbTabelaParametroAtendimentoUsuario = new Repository<TabelaParametroAtendimentoUsuario>();
        ListasRepositorio _dbListas = new ListasRepositorio();


        public ActionResult CadastroEstrategia()
        {
            var listaTiposDeCobranca = _dbTabelaTipoCobranca.All().ToList();
            listaTiposDeCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaTipoCobranca = from p in listaTiposDeCobranca.Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);


            var listaParametroAtendimento = _dbTabelaParametroAtendimento.All().ToList();
            listaParametroAtendimento.Insert(0, new TabelaParametroAtendimento());
            ViewBag.ListaEstrategia = from p in listaParametroAtendimento.OrderBy(x => x.NomeEstrategia).ToList() select new DictionaryEntry(p.NomeEstrategia, p.Cod);

            var dados = _dbTabelaParametroAtendimento.All().ToList();

            return View(dados.ToList());
        }
        public ActionResult CondicaoEstrategia()
        {
            var listaTiposDeCobranca = _dbTabelaTipoCobranca.All();
            listaTiposDeCobranca.ToList().Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaTipoCobranca = from p in listaTiposDeCobranca.Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);

            var listaParametroAtendimento = _dbTabelaParametroAtendimento.All().ToList();
            listaParametroAtendimento.Insert(0, new TabelaParametroAtendimento());
            ViewBag.ListaEstrategia = from p in listaParametroAtendimento.OrderBy(x => x.NomeEstrategia).ToList() select new DictionaryEntry(p.NomeEstrategia, p.Cod);


            return View();
        }

        [HttpGet]
        public ActionResult UsuarioEstrategia()
        {

            var listaTiposDeCobranca = _dbTabelaTipoCobranca.All().ToList();
            listaTiposDeCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaTipoCobranca = from p in listaTiposDeCobranca.Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);

            var listaParametroAtendimento = _dbTabelaParametroAtendimento.All().ToList();
            listaParametroAtendimento.Insert(0, new TabelaParametroAtendimento());
            ViewBag.ListaEstrategia = from p in listaParametroAtendimento.OrderBy(x => x.NomeEstrategia).ToList() select new DictionaryEntry(p.NomeEstrategia, p.Cod);

            var listaEquipe = _dbTabelaEquipe.All().ToList();
            listaEquipe.Insert(0, new TabelaEquipe());
            ViewBag.ListaEquipe = from p in listaEquipe.OrderBy(x => x.Equipe).ToList() select new DictionaryEntry(p.Equipe, p.Cod);

            var listaUsuario = _dbUsuario.All().ToList();
            listaUsuario.Insert(0, new Usuario());
            ViewBag.ListaUsuario = from p in listaUsuario.OrderBy(x => x.Nome).ToList() select new DictionaryEntry(p.Nome, p.Cod);

            var dados = _dbTabelaParametroAtendimentoUsuario.All().ToList();
            return View(dados.ToList());
        }

        public JsonResult SalvarAbrangencia(TabelaParametroAtendimentoUsuario dados)
        {

            try
            {
                if (dados.CodEquipe == 0)
                {
                    dados.CodEquipe = null;
                }
                if (dados.CodUsuario == 0)
                {
                    dados.CodUsuario = null;
                }
                _dbTabelaParametroAtendimentoUsuario.CreateOrUpdate(dados);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        public JsonResult SalvarEstrategia(int cod = 0, string estrategia = "")
        {
            try
            {
                TabelaParametroAtendimento TAP = new TabelaParametroAtendimento();
                if (cod > 0)
                {
                    TAP.Cod = cod;
                }
                TAP.NomeEstrategia = estrategia;
                _dbTabelaParametroAtendimento.CreateOrUpdate(TAP);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpGet]
        public JsonResult CarregarTiposCobrancaAcao()
        {
            List<DictionaryEntry> dados = new List<DictionaryEntry>();
            if (WebRotinas.UsuarioAutenticado)
            {
                dados = (from p in _dbTabelaAcaoCobranca.All().ToList()
                         select new DictionaryEntry() { Key = p.Cod, Value = p.AcaoCobranca }).ToList();
                dados.Insert(0, new DictionaryEntry());
            }
            return Json(dados, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult CarregarTiposCobranca()
        {
            List<DictionaryEntry> dados = new List<DictionaryEntry>();
            if (WebRotinas.UsuarioAutenticado)
            {
                dados = (from p in _dbTabelaTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList()
                         select new DictionaryEntry() { Key = p.Cod, Value = DbRotinas.Descriptografar(p.TipoCobranca) }).ToList();
                dados.Insert(0, new DictionaryEntry());
            }
            return Json(dados, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SalvarQuery(int CodEstrategia = 0, string Query = "")
        {
            try
            {
                var dados = _dbTabelaParametroAtendimento.FindAll(x => x.Cod == CodEstrategia).First();

                dados.Cod = CodEstrategia;
                dados.CondicaoQuery = Query;
                _dbTabelaParametroAtendimento.CreateOrUpdate(dados);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

        [HttpPost]
        public JsonResult ConsultaQueryBuilder(int CodParametroAtendimento = 0)
        {
            try
            {

                var dados = _dbTabelaParametroAtendimento.FindAll(x => x.Cod == CodParametroAtendimento).First();
                string Query = dados.CondicaoQuery;
                return Json(new { OK = true, Query = Query });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }


    }
}