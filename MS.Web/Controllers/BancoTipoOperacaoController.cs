﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class BancoTipoOperacaoController : BasicController<TabelaBancoTipoOperacao>
    {
        public Repository<TabelaBanco> _dbBanco = new Repository<TabelaBanco>();
        public Repository<TabelaTipoImovel> _dbTipoImovel = new Repository<TabelaTipoImovel>();
        public override void carregarCustomViewBags()
        {
            //var teste = _dbConstrutora.All().ToList().OrderBy(x=>x.NomeConstrutora);
            //ViewBag.Teste = new SelectList(teste, "Cod", "NomeConstrutora");

            var listaBanco = _dbBanco.All();
            ViewBag.ListaBanco = from p in listaBanco.OrderBy(x => x.NomeBanco).ToList() select new DictionaryEntry(p.NomeBanco, p.Cod);
            var listaTipoImovel = _dbTipoImovel.All();
            ViewBag.ListaTipoImovel = from p in listaTipoImovel.OrderBy(x => x.TipoImovel).ToList() select new DictionaryEntry(p.TipoImovel, p.Cod);

            base.carregarCustomViewBags();
        }


    }
}