﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class InclusaoContratoController : Controller
    {
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<Contrato> _dbPropostaProspect = new Repository<Contrato>();
        public Repository<TabelaProduto> _dbProduto = new Repository<TabelaProduto>();
        public Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        public Repository<TabelaRegionalProduto> _dbRegionalProduto = new Repository<TabelaRegionalProduto>();
        public Dados _qry = new Dados();
        [HttpGet]
        public ActionResult Index()
        {
            List<TabelaConstrutora> listaConstrutora = new List<TabelaConstrutora>();
            listaConstrutora = _dbConstrutora.All().OrderBy(x => x.NomeConstrutora).ToList();
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            List<TabelaRegional> listaRegional = new List<TabelaRegional>();
            listaRegional = _dbRegional.All().OrderBy(x => x.Regional).ToList();
            listaRegional.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaRegional select new DictionaryEntry(p.Regional, p.Cod);

            List<TabelaProduto> listaProduto = new List<TabelaProduto>();
            listaProduto = _dbProduto.All().OrderBy(x => x.Produto).ToList();
            listaProduto.Insert(0, new TabelaProduto());
            ViewBag.ListaProduto = from p in listaProduto select new DictionaryEntry(p.Produto, p.Cod);

            return View();
        }

        [HttpPost]
        public JsonResult CalcularValorParcela(decimal Valor = 0, int QtdParcela = 0)
        {
            try
            {
             var dados =  _qry.ListarValorParcela(Valor, QtdParcela).FirstOrDefault();
                return Json(new {
                    OK = true,
                    ValorParcela = dados.ValorParcela,
                    ValorTotal = dados.ValorTotal,
                    Resto = dados.Resto
                });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }

    }
}