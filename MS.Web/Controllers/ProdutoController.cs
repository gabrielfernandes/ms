﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class ProdutoController : BasicController<TabelaProduto>
    {
        DateTime? DataNascimento = System.DateTime.Today;

        public const string _filtroSessionId = "FiltroPesquisaId";
        public Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        public Dados _qry = new Dados();

        ListasRepositorio _dbListas = new ListasRepositorio();

        public override ActionResult Cadastro(TabelaProduto dados)
        {
            dados.Produto = DbRotinas.Criptografar(dados.Produto);
            dados.Descricao = DbRotinas.Criptografar(dados.Descricao);
            return base.Cadastro(dados);
        }

        public override void carregarCustomViewBags()
        {
            ViewBag.Lista = new List<DictionaryEntry>();
            ViewBag.ListaRegional = _dbListas.Regional();
        }

    }
}