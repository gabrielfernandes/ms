﻿using Cobranca.Db.Models;
using Cobranca.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Web.Rotinas;
using Cobranca.Db.Repositorio;
using System.Web.Security;

namespace Cobranca.Web.Controllers
{
    public class ModuloClienteController : Controller
    {
        public const string _clienteSessionId = "_clienteSessionId";
        private Repository<Cliente> _repository = new Repository<Cliente>();
        private Repository<ContratoCliente> _ContratoCliente = new Repository<ContratoCliente>();
        private Repository<ContratoParcela> _ContratoParcela = new Repository<ContratoParcela>();

        [HttpGet]
        public ActionResult Index(string returnUrl)
        {
            WebRotinas.CookieRemover(_clienteSessionId);
            var viewModel = new LoginClienteViewModel { ReturnUrl = returnUrl };

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult Index(LoginClienteViewModel dados)
        {


            if (ModelState.IsValid)
            {
                var usr = _repository.FindAll(x => x.CPF == dados.CPF && x.CPF == dados.Senha && !x.DataExcluido.HasValue).FirstOrDefault();
                if (usr != null && usr.Cod > 0)
                {
                    WebRotinas.AutenticarCliente(usr);
                    WebRotinas.CookieObjetoGravar(_clienteSessionId, usr);
                    return RedirectToAction("Home", "ModuloCliente");
                }
                else
                {
                    ModelState.AddModelError("", "Dados inválido");
                }
            }

            return View(dados);
        }
        public ActionResult Home()
        {
            ModuloClienteViewModel model = new ModuloClienteViewModel();
            Dados _qry = new Dados();
            Cliente cliente = WebRotinas.CookieObjetoLer<Cliente>(_clienteSessionId);

            if (cliente.Cod == 0)
            {
                return RedirectToAction("Index", "ModuloCliente");
            }

            //Cria o Perfil para o atendimento
            ChatAtendimento perfilchat = new ChatAtendimento();
            perfilchat.Nome = cliente.Nome;
            perfilchat.Email = cliente.Email;
            perfilchat.CodAtendimentoTipo = 1;

            ViewBag.Perfil = perfilchat;

            var contratos = _ContratoCliente.All().Where(x => x.CodCliente == cliente.Cod).ToList();
            List<ContratoParcelaViewModel> parcelas = new List<ContratoParcelaViewModel>();

            foreach (var c in contratos)
            {
                var cParcelas = _ContratoParcela.All().Where(x => x.CodContrato == c.CodContrato).ToList();
                //Atualiza o Valor da Parcela
                foreach (var i in cParcelas) { _qry.executarAtualizaValorParcela(i.Cod); }
                cParcelas = _ContratoParcela.All().Where(x => x.CodContrato == c.CodContrato).ToList();
                cParcelas = cParcelas.OrderByDescending(x => x.DataVencimento).ToList();
                foreach (var p in cParcelas)
                {

                    parcelas.Add(new ContratoParcelaViewModel
                    {
                        CodParcela = p.Cod,
                        NumeroContrato = p.Contrato.NumeroContrato,
                        NumeroParcela = p.NumeroParcela,
                        DataVencimento = p.DataVencimento,
                        strDataVencimento = string.Format("{0:dd/MM/yyyy}", p.DataVencimento),
                        ValorParcela = string.Format("{0:0,0.00}", p.ValorParcela),
                        ValorAtualizado = string.Format("{0:0,0.00}", p.ValorAtualizado), // atualizar quando for criado o campo valorAtualizado
                        ValorTotal = p.ValorParcela, // atualizar quando for criado o campo valorAtualizado
                        StatusParcela = (p.DataVencimento < DateTime.Now ? (short)Enumeradores.StatusFatura.Vencida : (short)Enumeradores.StatusFatura.EmDia),
                        Status = EnumeradoresDescricao.StatusFatura(p.DataVencimento < DateTime.Now ? (short)Enumeradores.StatusFatura.Vencida : (short)Enumeradores.StatusFatura.EmDia)
                    });
                }

            }

            ViewBag.ListaParcelas = parcelas;



            var parcelaAtrasada = parcelas.OrderBy(m => m.DataVencimento).First();
            var totalDias = DateTime.Now - parcelaAtrasada.DataVencimento;

            model.NumeroFatura = "00551787";
            model.Data = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            model.StatusFatura = EnumeradoresDescricao.StatusFatura(2);
            model.ValorParcela = string.Format("{0:0,0.00}", parcelas.Sum(x => x.ValorTotal).Value);
            model.ValorTotal = string.Format("{0:0,0.00}", parcelas.Sum(x => x.ValorTotal).Value);
            model.DataFatura = string.Format("{0:dd/MM/yyyy}", parcelaAtrasada.DataVencimento);
            model.NumeroDiasAtraso = Convert.ToInt32(totalDias.TotalDays);
            model.NumeroTotalParcela = 12;
            model.CodCliente = cliente.Cod;
            model.NomeCliente = cliente.Nome;
            model.EmailCliente = cliente.Email;
            return View(model);
        }

        [HttpGet]
        public ActionResult Sair(string returnUrl)
        {
            var viewModel = new LoginViewModel { ReturnUrl = returnUrl };

            FormsAuthentication.SignOut();

            return RedirectToAction("Index");
        }
    }
}