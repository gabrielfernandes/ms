﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using Cobranca.Db.Rotinas;
using System.Web.Hosting;
using System.IO;
using System.Web.Caching;

namespace Cobranca.Web.Controllers
{


    public class LoginController : Controller
    {
        private Repository<Usuario>
             _repository = new Repository<Usuario>();
        private Repository<UsuarioAcessoLog>
            _dbUsuarioAcessoLog = new Repository<UsuarioAcessoLog>();
        private Repository<ParametroPlataforma>
          _dbParametroPlataforma = new Repository<ParametroPlataforma>();
        private LoginDb
            _dbLogin = new LoginDb();
        [HttpGet]
        public ActionResult Index(string returnUrl)
        {

            //testeImport();

            //abiente de DEV (manter o usuario logado)
            //var usuario = _repository.FindById(18);
            //WebRotinas.AutenticarUsuario(usuario);


            string vl = DbRotinas.Criptografar("é complicado ç ão ê");
            string vl2 = DbRotinas.Descriptografar(vl);

            if (WebRotinas.UsuarioAutenticado)
            {
                return RedirectToAction("Index", "home", null);
            }


            var parametro = _dbParametroPlataforma.All().FirstOrDefault();
            WebRotinas.ParametroEmpresaGravar(parametro);

            var viewModel = new LoginViewModel { ReturnUrl = returnUrl };

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Cadastre(string returnUrl)
        {
            var viewModel = new CadastreViewModel();

            return View(viewModel);
        }
        //[HttpPost]
        //public ActionResult Cadastre(CadastreViewModel dados)
        //{

        //    if (_repository.FindAll(x => x.Login == dados.Email && !string.IsNullOrEmpty(dados.Email)).Any())
        //    {
        //        ModelState.AddModelError("Email", Language.EmailEmUso);
        //    }


        //    if (ModelState.IsValid)
        //    {
        //        var entity = new Usuario()
        //        {
        //            Nome = dados.Nome,
        //            Login = dados.Email,
        //            Senha = dados.Senha,
        //            DataCadastro = DateTime.Now,
        //            PerfilTipo = (short)Enumeradores.PerfilTipo.Admin,
        //        };

        //        entity = _repository.CreateOrUpdate(entity);

        //        ViewBag.MensagemOK = Language.MensagemOK;
        //    }


        //    return View(dados);
        //}


        [HttpPost]
        public ActionResult Index(LoginViewModel dados)
        {

            if (ModelState.IsValid)
            {
                //var usr = _repository.FindAll(x => x.Login == dados.Login && x.Senha == dados.Senha && !x.DataExcluido.HasValue && !x.DataBloqueado.HasValue).FirstOrDefault();
                var usr = _dbLogin.SingIn(dados.Login, Db.Rotinas.DbRotinas.Descriptografar(dados.Senha));
                if (usr != null && usr.Cod > 0)
                {

                    RegistrarLogAcesso(usr);

                    WebRotinas.AutenticarUsuario(usr);


                    if (usr.SistemaCobranca == true)
                    {
                        return RedirectToAction("Index", "Home", null);
                    }
                   
                    return RedirectToAction("AcessoNegado");

                }
                else
                {
                    ModelState.AddModelError("", "Login inválido");
                }
            }

            return View(dados);
        }
        

        private void RegistrarLogAcesso(Usuario usr)
        {
            usr.DataPrimeiroAcesso = usr.DataPrimeiroAcesso.HasValue ? usr.DataPrimeiroAcesso : DateTime.Now;
            usr.DataUltimoAcesso = DateTime.Now;
            _repository.CreateOrUpdate(usr);

            _dbUsuarioAcessoLog.CreateOrUpdate(new UsuarioAcessoLog()
            {
                CodUsuario = usr.Cod,
                DataAcesso = DateTime.Now,
                IP = WebRotinas.ObterIP()
            });
        }

        [HttpGet]
        public ActionResult Sair(string returnUrl)
        {
            var viewModel = new LoginViewModel { ReturnUrl = returnUrl };

            int usuarioId = WebRotinas.ObterUsuarioLogado().Cod;

            var usr = _repository.FindById(usuarioId);

            //  _repository.CreateOrUpdate(usr);

            FormsAuthentication.SignOut();

            return RedirectToAction("Index");
        }


    }
}