﻿using Cobranca.Db.Repositorio;
using Cobranca.Db.Models;
using Cobranca.Db.Rotinas;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Rotinas;
using System.IO;

namespace Cobranca.Web.Controllers
{

    public class HomeController : Controller
    {
        public Repository<TabelaBloco> _dbBloco = new Repository<TabelaBloco>();
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<Usuario> _dbUsuario = new Repository<Usuario>();
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<Historico> _dbHistorico = new Repository<Historico>();
        private Repository<Usuario> _repUsuario = new Repository<Usuario>();
        private Repository<TabelaFase> _repFase = new Repository<TabelaFase>();
        private Repository<TabelaConstrutora> _repConstrutora = new Repository<TabelaConstrutora>();
        private Repository<TabelaEmpreendimento> _repEmpreendimento = new Repository<TabelaEmpreendimento>();
        private Repository<TabelaRegional> _repRegional = new Repository<TabelaRegional>();
        private Repository<TabelaProduto> _repProduto = new Repository<TabelaProduto>();
        private Repository<Contrato> _repContrato = new Repository<Contrato>();
        private Repository<TabelaCampanha> _repCampanha = new Repository<TabelaCampanha>();
        private Repository<TabelaTipoCobranca> _repTipoCobranca = new Repository<TabelaTipoCobranca>();
        private Repository<ContratoParcela> _repContratoParcela = new Repository<ContratoParcela>();
        private Repository<TabelaMotivoAtraso> _repTabelaMotivoAtraso = new Repository<TabelaMotivoAtraso>();
        private Repository<TabelaResolucao> _repTabelaResolucao = new Repository<TabelaResolucao>();
        private Repository<TabelaAging> _dbTabelaAging = new Repository<TabelaAging>();
        private Repository<TabelaEscritorio> _repTabelaEscritorio = new Repository<TabelaEscritorio>();
        private Repository<TabelaDia> _dbTabelaDia = new Repository<TabelaDia>();
        private Repository<ParametroAtendimento> _dbParametroAtendimento = new Repository<ParametroAtendimento>();
        private Repository<ParametroAtendimentoRota> _dbParametroAtendimentoRota = new Repository<ParametroAtendimentoRota>();
        private Repository<ParametroAtendimentoProduto> _dbParametroAtendimentoProduto = new Repository<ParametroAtendimentoProduto>();
        private Repository<RotaRegional> _dbRotaRegional = new Repository<RotaRegional>();
        private Repository<UsuarioRegional> _dbUsuarioRegional = new Repository<UsuarioRegional>();

        public Dados _qry = new Dados();


        public const string _filtroSessionId = "FiltroHome";
        [HttpGet]
        public ActionResult Index(int atendimento = 0)
        {



            var listAging = _dbTabelaAging.All().ToList();
            ViewBag.ListandoAging = listAging;
            ViewBag.Atendimento = atendimento;


            HomePesquisaViewModel model = new HomePesquisaViewModel();
            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);
            var usuario = WebRotinas.ObterUsuarioLogado();
            if (usuario.CodRegional > 0)
            {
                model.CodRegional = (int)usuario.CodRegional;
                IQueryable<RotaRegional> rotasFilial = _dbRotaRegional.FindAll(x => x.CodRegional == usuario.CodRegional && !x.DataExcluido.HasValue);

                if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional)
                {
                    var listaRotasUsuario = _dbParametroAtendimentoRota.FindAll(x => x.ParametroAtendimento.CodUsuario == usuario.Cod && !x.DataExcluido.HasValue).ToList();
                    List<int> rotasUsuario = listaRotasUsuario.Select(x => (int)x.CodRota).ToList();
                    rotasFilial = rotasFilial.Where(x => rotasUsuario.Contains((int)x.CodRota));
                }
                List<int> rotasIds = rotasFilial.Select(r => (int)r.CodRota).ToList();
                model.CodRotas = String.Join(", ", rotasIds.ToArray());
            }
            bool fgSistemaCobranca = WebRotinas.ObterUsuarioLogado().SistemaCobranca == true;
            //var listaCampanha = _repCampanha.All().ToList();
            //listaCampanha.Insert(0, new TabelaCampanha());
            //ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);

            var listaCobranca = _repTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca && !x.DataExcluido.HasValue).ToList();

            listaCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaCobranca = from p in listaCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);


            //PREENCHE OS DROPDOWNS MULTIPLES
            PreencherListas(model);

            var listaEscritorio = _repTabelaEscritorio.All().ToList();
            listaEscritorio.Insert(0, new TabelaEscritorio());
            ViewBag.ListaEscritorio = from p in listaEscritorio.OrderBy(x => x.Escritorio).ToList() select new DictionaryEntry(p.Escritorio, p.Cod);

            var listaPrazo = _dbTabelaDia.All().ToList();

            ViewBag.ListandoDia = listaPrazo;


            var ListaFase = _repFase.All().OrderBy(x => x.TabelaTipoCobranca.Ordem).ToList();

            model.HistoricoesContrato = new List<HistoricoDados>();//FiltrarHistoricoesHomeContrato(model);
            model.HistoricoesCliente = new List<HistoricoDados>();// FiltrarHistoricoesHomeCliente(model);
            model.HistoricoesParcela = new List<HistoricoDados>(); //FiltrarHistoricoesHomeParcela(model);
            model.HistoricoesBoleto = new List<HistoricoDados>(); //FiltrarHistoricoesHomeParcela(model);
            GraficosAging(model);

            if (fgSistemaCobranca)
            {
                listaCobranca = listaCobranca.Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
                model.ListaFase = ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
            }
            else
            {
                listaCobranca = listaCobranca.Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Proposta).ToList();
                model.ListaFase = ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.Proposta).ToList();
            }

            if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Analista || usuario.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista || usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin || usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional || usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                var TipoCobrancaBoleto = _repTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.AnalistaBoleto || x.TipoFluxo == (short)Enumeradores.TipoFluxo.SupervisorBoleto).ToList();
                if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Analista)
                {
                    model.ListaFaseBoleto = ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.AnalistaBoleto).ToList();
                }
                else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista)
                {
                    model.ListaFaseBoleto = ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.SupervisorBoleto).ToList();
                }
                else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional || usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
                {
                    model.ListaFaseBoleto = ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.FilialBoleto).ToList();
                }
                else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin)
                {
                    model.ListaFaseBoleto = ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.AnalistaBoleto || x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.SupervisorBoleto || x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.FilialBoleto).ToList();
                }

            }

            model.tabSelecionada = (short)Enumeradores.tabsHomeCobranca.Status;

            WebRotinas.FiltroGeralRemover();
            return View(model);
        }

        [HttpGet]
        public ActionResult Solicitacao(int atendimento = 0)
        {
            HomeViewModel model = new HomeViewModel();
            model.SetUsuario(WebRotinas.ObterUsuarioLogado());
            model.tabSelecionada = Enumeradores.tabsHomeCobranca.Solicitacao;
            var where = String.Empty;
            //model.ListaRegionais = new SelectList(UtilsRotinas.GetSelectListItemRegional(where, null));


            return View(model);
        }

        [HttpGet]
        public ActionResult Desempenho(int atendimento = 0)
        {
            HomeViewModel model = new HomeViewModel();
            model.SetUsuario(WebRotinas.ObterUsuarioLogado());
            model.tabSelecionada = Enumeradores.tabsHomeCobranca.Desempenho;

            return View(model);
        }

        [HttpGet]
        public ActionResult Indicadores()
        {
            return View();
        }
        [HttpGet]
        public ActionResult PagamentoAtraso()
        {
            HomePesquisaViewModel model = new HomePesquisaViewModel();
            var listaConstrutora = _repConstrutora.All().ToList();
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            var listaRegional = _repRegional.All().ToList();
            listaRegional.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaRegional.OrderBy(x => x.Regional).ToList() select new DictionaryEntry(p.Regional, p.Cod);

            var listaProduto = _repProduto.All().ToList();
            listaProduto.Insert(0, new TabelaProduto());
            ViewBag.ListaProduto = from p in listaProduto.OrderBy(x => x.Produto).ToList() select new DictionaryEntry(p.Produto, p.Cod);

            var listaCampanha = _repCampanha.All().ToList();
            listaCampanha.Insert(0, new TabelaCampanha());
            ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);

            var listaCobranca = _repTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca && !x.DataExcluido.HasValue).ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaCobranca = from p in listaCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);

            var listaParcelas = _repContratoParcela.All().ToList();
            if (model.CodConstrutora > 0)
            {
                listaParcelas = listaParcelas.Where(x => x.Contrato.TabelaRegionalProduto.TabelaRegional.CodConstrutora == model.CodConstrutora).ToList();
            }
            if (model.CodRegional > 0)
            {
                listaParcelas = listaParcelas.Where(x => x.Contrato.TabelaRegionalProduto.CodRegional == model.CodRegional).ToList();
            }
            if (model.CodProduto > 0)
            {
                listaParcelas = listaParcelas.Where(x => x.Contrato.TabelaRegionalProduto.CodProduto == model.CodProduto).ToList();
            }


            ViewBag.ListaParcelas = listaParcelas.ToList();

            var listaEscritorio = _repTabelaEscritorio.All().ToList();

            listaEscritorio.Insert(0, new TabelaEscritorio());
            ViewBag.ListaEscritorio = from p in listaEscritorio.OrderBy(x => x.Escritorio).ToList() select new DictionaryEntry(p.Escritorio, p.Cod);


            var listaAging = _dbTabelaAging.All().ToList();

            listaAging.Insert(0, new TabelaAging());
            ViewBag.ListaAging = from p in listaAging.OrderBy(x => x.Aging).ToList() select new DictionaryEntry(p.Aging, p.Cod);


            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Login", null);
            }


            //model.ListaFase = _repFase.All().ToList();
            model.ListaContratoes = FiltrarContrato(model, _repContrato.All());
            return View(model);
        }
        [HttpPost]
        public ActionResult PagamentoAtraso(HomePesquisaViewModel model)
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Login", null);
            }

            var listaConstrutora = _repConstrutora.All().ToList();
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            var listaRegional = _repRegional.All().ToList();
            listaRegional.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaRegional.OrderBy(x => x.Regional).ToList() select new DictionaryEntry(p.Regional, p.Cod);

            var listaProduto = _repProduto.All().ToList();
            listaProduto.Insert(0, new TabelaProduto());
            ViewBag.ListaProduto = from p in listaProduto.OrderBy(x => x.Produto).ToList() select new DictionaryEntry(p.Produto, p.Cod);

            var listaCampanha = _repCampanha.All().ToList();
            listaCampanha.Insert(0, new TabelaCampanha());
            ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);


            var listaParcelas = _repContratoParcela.All().ToList();
            if (model.CodConstrutora > 0)
            {
                listaParcelas = listaParcelas.Where(x => x.Contrato.TabelaRegionalProduto.TabelaRegional.CodConstrutora == model.CodConstrutora).ToList();
            }
            if (model.CodRegional > 0)
            {
                listaParcelas = listaParcelas.Where(x => x.Contrato.TabelaRegionalProduto.CodRegional == model.CodRegional).ToList();
            }
            if (model.CodProduto > 0)
            {
                listaParcelas = listaParcelas.Where(x => x.Contrato.TabelaRegionalProduto.CodProduto == model.CodProduto).ToList();
            }


            ViewBag.ListaParcelas = listaParcelas.ToList();


            var listaCobranca = _repTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca && !x.DataExcluido.HasValue).ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaCobranca = from p in listaCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);
            model.ListaFase = _repFase.All().ToList();
            model.ListaContratoes = FiltrarContrato(model, _repContrato.All());

            var listaEscritorio = _repTabelaEscritorio.All().ToList();

            listaEscritorio.Insert(0, new TabelaEscritorio());
            ViewBag.ListaEscritorio = from p in listaEscritorio.OrderBy(x => x.Escritorio).ToList() select new DictionaryEntry(p.Escritorio, p.Cod);


            var listaAging = _dbTabelaAging.All().ToList();

            listaAging.Insert(0, new TabelaAging());
            ViewBag.ListaAging = from p in listaAging.OrderBy(x => x.Aging).ToList() select new DictionaryEntry(p.Aging, p.Cod);


            return View(model);
        }
        [HttpGet]
        public ActionResult MotivoAtraso()
        {
            HomePesquisaViewModel model = new HomePesquisaViewModel();
            var listaConstrutora = _repConstrutora.All().ToList();
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            var listaRegional = _repRegional.All().ToList();
            listaRegional.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaRegional.OrderBy(x => x.Regional).ToList() select new DictionaryEntry(p.Regional, p.Cod);

            var listaProduto = _repProduto.All().ToList();
            listaProduto.Insert(0, new TabelaProduto());
            ViewBag.ListaProduto = from p in listaProduto.OrderBy(x => x.Produto).ToList() select new DictionaryEntry(p.Produto, p.Cod);

            var listaCampanha = _repCampanha.All().ToList();
            listaCampanha.Insert(0, new TabelaCampanha());
            ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);

            var listaCobranca = _repTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca && !x.DataExcluido.HasValue).ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaCobranca = from p in listaCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);
            ViewBag.ListaMotivoAtraso = _repTabelaMotivoAtraso.All().ToList();


            var listaEscritorio = _repTabelaEscritorio.All().ToList();

            listaEscritorio.Insert(0, new TabelaEscritorio());
            ViewBag.ListaEscritorio = from p in listaEscritorio.OrderBy(x => x.Escritorio).ToList() select new DictionaryEntry(p.Escritorio, p.Cod);


            var listaAging = _dbTabelaAging.All().ToList();

            listaAging.Insert(0, new TabelaAging());
            ViewBag.ListaAging = from p in listaAging.OrderBy(x => x.Aging).ToList() select new DictionaryEntry(p.Aging, p.Cod);

            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Login", null);
            }
            model.ListaFase = _repFase.All().ToList();
            model.ListaContratoes = FiltrarContrato(model, _repContrato.All().Take(20));
            return View(model);
        }
        [HttpPost]
        public ActionResult MotivoAtraso(HomePesquisaViewModel model)
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Login", null);
            }

            var listaConstrutora = _repConstrutora.All().ToList();
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            var listaRegional = _repRegional.All().ToList();
            listaRegional.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaRegional.OrderBy(x => x.Regional).ToList() select new DictionaryEntry(p.Regional, p.Cod);

            var listaProduto = _repProduto.All().ToList();
            listaProduto.Insert(0, new TabelaProduto());
            ViewBag.ListaProduto = from p in listaProduto.OrderBy(x => x.Produto).ToList() select new DictionaryEntry(p.Produto, p.Cod);

            var listaCampanha = _repCampanha.All().ToList();
            listaCampanha.Insert(0, new TabelaCampanha());
            ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);
            ViewBag.ListaMotivoAtraso = _repTabelaMotivoAtraso.All().ToList();
            var listaCobranca = _repTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca && !x.DataExcluido.HasValue).ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaCobranca = from p in listaCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);
            var listaEscritorio = _repTabelaEscritorio.All().ToList();

            listaEscritorio.Insert(0, new TabelaEscritorio());
            ViewBag.ListaEscritorio = from p in listaEscritorio.OrderBy(x => x.Escritorio).ToList() select new DictionaryEntry(p.Escritorio, p.Cod);


            var listaAging = _dbTabelaAging.All().ToList();

            listaAging.Insert(0, new TabelaAging());
            ViewBag.ListaAging = from p in listaAging.OrderBy(x => x.Aging).ToList() select new DictionaryEntry(p.Aging, p.Cod);

            model.ListaFase = _repFase.All().ToList();
            model.ListaContratoes = FiltrarContrato(model, _repContrato.All());

            return View(model);
        }
        [HttpGet]
        public ActionResult Resolucao()
        {
            HomePesquisaViewModel model = new HomePesquisaViewModel();
            var listaConstrutora = _repConstrutora.All().ToList();
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);

            var listaRegional = _repRegional.All().ToList();
            listaRegional.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaRegional.OrderBy(x => x.Regional).ToList() select new DictionaryEntry(p.Regional, p.Cod);

            var listaProduto = _repProduto.All().ToList();
            listaProduto.Insert(0, new TabelaProduto());
            ViewBag.ListaProduto = from p in listaProduto.OrderBy(x => x.Produto).ToList() select new DictionaryEntry(p.Produto, p.Cod);

            var listaCampanha = _repCampanha.All().ToList();
            listaCampanha.Insert(0, new TabelaCampanha());
            ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);

            var listaCobranca = _repTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca && !x.DataExcluido.HasValue).ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaCobranca = from p in listaCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);
            ViewBag.ListaResolucao = _repTabelaResolucao.All().ToList();

            var listaEscritorio = _repTabelaEscritorio.All().ToList();

            listaEscritorio.Insert(0, new TabelaEscritorio());
            ViewBag.ListaEscritorio = from p in listaEscritorio.OrderBy(x => x.Escritorio).ToList() select new DictionaryEntry(p.Escritorio, p.Cod);


            var listaAging = _dbTabelaAging.All().ToList();

            listaAging.Insert(0, new TabelaAging());
            ViewBag.ListaAging = from p in listaAging.OrderBy(x => x.Aging).ToList() select new DictionaryEntry(p.Aging, p.Cod);


            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Login", null);
            }
            model.ListaFase = _repFase.All().ToList();
            model.ListaContratoes = FiltrarContrato(model, _repContrato.All().Take(20));
            return View(model);
        }
        [HttpPost]
        public ActionResult Resolucao(HomePesquisaViewModel model)
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Login", null);
            }


            //PREENCHE OS DROPDOWNS MULTIPLES
            PreencherListas(model);

            var listaCampanha = _repCampanha.All().ToList();
            listaCampanha.Insert(0, new TabelaCampanha());
            ViewBag.ListaCampanha = from p in listaCampanha.OrderBy(x => x.Campanha).ToList() select new DictionaryEntry(p.Campanha, p.Cod);
            ViewBag.ListaResolucao = _repTabelaResolucao.All().ToList();
            var listaCobranca = _repTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca && !x.DataExcluido.HasValue).ToList();
            listaCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaCobranca = from p in listaCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);
            model.ListaFase = _repFase.All().ToList();
            model.ListaContratoes = FiltrarContrato(model, _repContrato.All());

            var listaEscritorio = _repTabelaEscritorio.All().ToList();

            listaEscritorio.Insert(0, new TabelaEscritorio());
            ViewBag.ListaEscritorio = from p in listaEscritorio.OrderBy(x => x.Escritorio).ToList() select new DictionaryEntry(p.Escritorio, p.Cod);


            var listaAging = _dbTabelaAging.All().ToList();

            listaAging.Insert(0, new TabelaAging());
            ViewBag.ListaAging = from p in listaAging.OrderBy(x => x.Aging).ToList() select new DictionaryEntry(p.Aging, p.Cod);


            return View(model);
        }
        [HttpPost]
        public ActionResult Index(HomePesquisaViewModel model)
        {
            //string encriptLocalWeb = new Conexao().CriptografarString("Data Source=186.202.39.76;Initial Catalog=even_imobflow;User ID=even_imobflow;Password=jnx@2312");
            //string encriptOtis = new Conexao().CriptografarString("Data Source=CBRSBD05.BR.NA.OTIS.UTC.COM;Initial Catalog=DB_COBFLOW;User ID=cobflow;Password=2rEYaxetre3AsTe;");
            //string encriptLocal = new Conexao().CriptografarString(@"Data Source=.\SQLEXPRESS;Initial Catalog=DB_COBFLOW;User ID=sa;Password=jnx@2312;");
            //string encriptLocal2 = new Conexao().CriptografarString(@"Data Source=.\SQLEXPRESS;Initial Catalog=DB_COBFLOW;User ID=sa;Password=Pa$$w0rd;");

            var usuario = WebRotinas.ObterUsuarioLogado();
            //VERIFICA SE USUARIO ESTA AUTENTICADO
            if (!HttpContext.User.Identity.IsAuthenticated) { return RedirectToAction("Index", "Login", null); }

            //ENCONTRA O USUARIO A SER FILTRADO
            var UsuarioFiltrar = UsuarioAFiltrar(model);

            model.CodUsuarioAtendimento = UsuarioFiltrar.Cod;
            model.CodPerfilTipo = (short)UsuarioFiltrar.PerfilTipo;

            model.TipoFluxo = obterTipoFluxoUsuario(usuario);


            if (model.regionalIds != null) { model.CodRegionais = String.Join(",", model.regionalIds); }
            if (model.construtorasIds != null) { model.CodConstrutoras = String.Join(",", model.construtorasIds); }
            if (model.produtosIds != null) { model.CodProdutos = String.Join(",", model.produtosIds); }
            if (model.agingIds != null) { model.CodAgings = String.Join(",", model.agingIds); }
            if (model.usuarioIds != null) { model.CodUsuarios = String.Join(",", model.usuarioIds); }

            model.HistoricoesContrato = FiltrarHistoricoesHomeContrato(model);
            model.HistoricoesCliente = new List<HistoricoDados>();// FiltrarHistoricoesHomeCliente(model);
            model.HistoricoesParcela = FiltrarHistoricoesHomeParcela(model);
            model.HistoricoesBoleto = FiltrarHistoricoesHomeBoleto(model);

            //GRAFICO DE AGING
            GraficosAging(model);

            model.ListaFase = _repFase.All().ToList();
            if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Analista || usuario.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista || usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin || usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional || usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin)
                {
                    model.ListaFaseBoleto = model.ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.AnalistaBoleto || x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.SupervisorBoleto || x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.FilialBoleto).ToList();
                }
                else
                {
                    model.ListaFaseBoleto = model.ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)model.TipoFluxo).ToList();
                }
            }


            //PREENCHE OS DROPDOWNS MULTIPLES
            PreencherListas(model);
            var listAging = _dbTabelaAging.All().ToList();
            ViewBag.ListandoAging = listAging;

            //GRAVA O FILTRO FEITO NAS ABAS.
            FiltroGeralDados filter = new FiltroGeralDados();
            filter.CodUsuarioAtendimento = model.CodUsuarioAtendimento;
            filter.CodPerfilTipo = model.CodPerfilTipo;
            filter.RegionalIds = model.regionalIds;
            filter.ConstrutorasIds = model.construtorasIds;
            filter.ProdutosIds = model.produtosIds;
            filter.AgingIds = model.agingIds;
            WebRotinas.FiltroGeralGravar(filter);



            WebRotinas.CookieObjetoGravar(_filtroSessionId, model);
            return View(model);
        }

        [HttpGet]
        public ActionResult ObterSolicitacao(HomePesquisaViewModel model)
        {
            //VERIFICA SE USUARIO ESTA AUTENTICADO
            if (!HttpContext.User.Identity.IsAuthenticated) { return RedirectToAction("Index", "Login", null); }
            var usuario = WebRotinas.ObterUsuarioLogado();
            //ENCONTRA O USUARIO A SER FILTRADO
            var UsuarioFiltrar = UsuarioAFiltrar(model);
            model.CodUsuarioAtendimento = UsuarioFiltrar.Cod;
            model.CodPerfilTipo = (short)UsuarioFiltrar.PerfilTipo;
            model.TipoFluxo = obterTipoFluxoUsuario(usuario);
            if (model.CodPerfilTipo == (short)Enumeradores.PerfilTipo.Regional)
            {
                model.CodRegionais = String.Join(",", WebRotinas.ObterRegionailIdsDoUsuarioLogado());
            }

            model.HistoricoesBoleto = FiltrarHistoricoesHomeBoleto(model);
            model.ListaFase = _repFase.All().ToList();
            if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Analista || usuario.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista || usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin || usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional || usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin)
                {
                    model.ListaFaseBoleto = model.ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.AnalistaBoleto || x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.SupervisorBoleto || x.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.FilialBoleto).ToList();
                }
                else
                {
                    model.ListaFaseBoleto = model.ListaFase.Where(x => x.TabelaTipoCobranca.TipoFluxo == (short)model.TipoFluxo).ToList();
                }
            }
            return PartialView("_SolicitacaoPartial", model);
        }

        private Nullable<short> obterTipoFluxoUsuario(UsuarioLogadoInfo usuario)
        {
            Nullable<short> TipoFluxo = null;
            if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Analista || usuario.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista || usuario.PerfilTipo == Enumeradores.PerfilTipo.Admin || usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional || usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
            {
                if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Analista)
                {
                    TipoFluxo = (short)Enumeradores.TipoFluxo.AnalistaBoleto;
                }
                else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista)
                {

                    TipoFluxo = (short)Enumeradores.TipoFluxo.SupervisorBoleto;
                }
                else if (usuario.PerfilTipo == Enumeradores.PerfilTipo.Regional || usuario.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin)
                {
                    TipoFluxo = (short)Enumeradores.TipoFluxo.FilialBoleto;
                }
            }
            return TipoFluxo;
        }

        private Usuario UsuarioAFiltrar(HomePesquisaViewModel model)
        {
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            Usuario UsuarioFiltrar = new Usuario();
            if (usuarioLogado.PerfilTipo == Enumeradores.PerfilTipo.Atendimento)
            {
                //SE O USUARIO FOR ATENDIMENTO DEVERA PEGAR O USUARIO LOGADO
                UsuarioFiltrar = _dbUsuario.FindById(usuarioLogado.Cod);
            }
            else
            {
                //SE O USUARIO FILTRAR ALGUM ATENDENTE 
                if (model.usuarioIds != null)
                {
                    var idFiltrado = DbRotinas.ConverterParaInt(model.usuarioIds.First());
                    if (idFiltrado > 0)
                    {
                        model.CodUsuarioAtendimento = idFiltrado;
                        UsuarioFiltrar = _dbUsuario.FindById(model.CodUsuarioAtendimento);
                    }
                    else
                    {
                        UsuarioFiltrar = _dbUsuario.FindById(usuarioLogado.Cod);
                    }
                }
                //CASO USUARIO NAO FILTRAR NINGUEM
                else
                {
                    UsuarioFiltrar = _dbUsuario.FindById(usuarioLogado.Cod);
                }
            }
            return UsuarioFiltrar;
        }
        private void PreencherListas(HomePesquisaViewModel model)
        {
            var usuario = WebRotinas.ObterUsuarioLogado();
            var where = String.Empty;
            //PREENCHER SelectListRegional
            ViewBag.ListaRegional = new SelectList(UtilsRotinas.GetSelectListItemRegional(where, model.construtorasIds));
            bool admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;
            var cdReginal = WebRotinas.ObterUsuarioLogado().CodRegional;
            //var usuariosAtendimentos = _dbUsuarioRegional.All().Select(x => x.Usuario).ToList();
            //usuariosAtendimentos = FiltrarUsuariosAtendimentoPorPerfil(usuariosAtendimentos);
            //ViewBag.ListaUsuarioAtendimento = usuariosAtendimentos.ToList();
            //ViewBag.ListaUsuarioAtendimento.Insert(0, new Usuario() { Cod = 0, Nome = "--" });
            var _where = $"";

            ViewBag.ListaAtendentes = new SelectList(UtilsRotinas.GetSelectListItemAtendente("", model.usuarioIds));

            where = String.Empty;
            ViewBag.ListaProduto = new SelectList(UtilsRotinas.GetSelectListItemProduto(where, model.construtorasIds));


            //PREENCHE CONSTRUTORAS
            List<SelectListItem> construtoraItens = new List<SelectListItem>();
            var listaConstrutora = _repConstrutora.All().ToList();
            foreach (var item in listaConstrutora)
            {
                item.NomeConstrutora = DbRotinas.Descriptografar(item.NomeConstrutora);
            }
            foreach (var construtora in listaConstrutora)
            {
                construtoraItens.Add(new SelectListItem()
                {
                    Value = construtora.Cod.ToString(),
                    Text = construtora.NomeConstrutora,
                    Selected = false
                });
            }
            if (model.CodConstrutoras != null)
            {
                foreach (var i in model.CodConstrutoras.Split(','))
                {
                    if (construtoraItens.Where(x => x.Value == i).FirstOrDefault() != null)
                    {
                        construtoraItens.Where(x => x.Value == i).FirstOrDefault().Selected = true;
                    }
                }
            }
            ViewBag.ListaConstrutora = new SelectList(construtoraItens);

            //PREENCHE Agins
            List<SelectListItem> agingItens = new List<SelectListItem>();
            var listaAging = _dbTabelaAging.All().ToList();
            foreach (var aging in listaAging)
            {
                agingItens.Add(new SelectListItem()
                {
                    Value = aging.Cod.ToString(),
                    Text = aging.Aging,
                    Selected = false
                });
            }
            if (model.CodAgings != null)
            {
                foreach (var i in model.CodAgings.Split(','))
                {
                    if (agingItens.Where(x => x.Value == i).FirstOrDefault() != null)
                    {
                        agingItens.Where(x => x.Value == i).FirstOrDefault().Selected = true;
                    }
                }
            }
            ViewBag.ListaAging = new SelectList(agingItens);
        }

        private List<Usuario> FiltrarUsuariosAtendimentoPorPerfil(List<Usuario> listaUsuarios)
        {
            var _usuario = WebRotinas.ObterUsuarioLogado();
            switch (_usuario.PerfilTipo)
            {
                case Enumeradores.PerfilTipo.Admin:
                    return listaUsuarios;
                case Enumeradores.PerfilTipo.Analista:
                    return listaUsuarios;
                case Enumeradores.PerfilTipo.Atendimento:
                    return listaUsuarios.Where(x => x.Cod == _usuario.Cod).ToList();
                case Enumeradores.PerfilTipo.Regional:
                    return listaUsuarios.Where(x => x.Cod == _usuario.Cod).ToList();
                case Enumeradores.PerfilTipo.RegionalAdmin:
                    List<int> regionaisIds = WebRotinas.ObterRegionailIdsDoUsuarioLogado();
                    return _dbUsuarioRegional.All().Where(x => regionaisIds.Contains(x.CodRegional) && x.Usuario.PerfilTipo == (short)Enumeradores.PerfilTipo.Regional).Select(x => x.Usuario).ToList();
                case Enumeradores.PerfilTipo.SupervisorAnalista:
                    return listaUsuarios;
                default:
                    return listaUsuarios.Where(x => x.Cod == _usuario.Cod).ToList();
            }

        }

        private List<ContratoDados> FiltrarContratoHome(HomePesquisaViewModel model)
        {
            var Admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;


            List<ContratoDados> resultado = _qry.ListarContratosHome(model.CodTipoCobranca, model.CodConstrutoras, model.CodRegionais, model.CodProdutos, model.CodAging).ToList();
            return resultado.ToList();
        }
        private List<HistoricoDados> FiltrarHistoricoesHomeContrato(HomePesquisaViewModel model)
        {

            List<HistoricoDados> resultado = _qry.ListarHistoricoesContratoHome(model.CodTipoCobranca, model.CodConstrutoras, model.CodRegionais, model.CodProdutos, model.CodRotas, model.CodAging, model.PeriodoDe, model.PeriodoAte, model.CodUsuarioAtendimento, model.CodPerfilTipo).ToList();
            return resultado.ToList();
        }
        private List<HistoricoDados> FiltrarHistoricoesHomeCliente(HomePesquisaViewModel model)
        {
            var Admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;
            List<HistoricoDados> resultado = _qry.ListarHistoricoesClienteHome(model.CodTipoCobranca, model.CodConstrutoras, model.CodRegionais, model.CodProdutos, model.CodAging, model.PeriodoDe, model.PeriodoAte).ToList();
            return resultado.ToList();
        }
        private List<HistoricoDados> FiltrarHistoricoesHomeParcela(HomePesquisaViewModel model)
        {
            List<HistoricoDados> resultado = _qry.ListarHistoricoesParcelaHome(model.CodTipoCobranca, model.CodConstrutoras, model.CodRegionais, model.CodProdutos, model.CodRotas, model.CodAgings, model.PeriodoDe, model.PeriodoAte, model.CodUsuarioAtendimento, model.CodPerfilTipo).ToList();
            return resultado.ToList();
        }
        private List<HistoricoDados> FiltrarHistoricoesHomeBoleto(HomePesquisaViewModel model)
        {
            List<HistoricoDados> resultado = _qry.ListarHistoricoesBoletoHome(model.CodTipoCobranca, model.CodConstrutoras, model.CodRegionais, model.CodProdutos, model.CodRotas, model.CodAgings, model.PeriodoDe, model.PeriodoAte, model.CodUsuarioAtendimento, model.CodPerfilTipo, model.TipoFluxo).ToList();
            return resultado.ToList();
        }

        private List<Contrato> FiltrarContrato(HomePesquisaViewModel model, IQueryable<Contrato> queryable)
        {
            var Admin = WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin;


            List<Contrato> resultado = queryable.ToList();
            if (!Admin)
            {
                resultado = resultado.Where(x => x.CodEscritorio == Web.Rotinas.WebRotinas.ObterUsuarioLogado().CodEscritorio).ToList();
            }
            if (model.CodTipoCobranca > 0)
            {
                resultado = resultado.Where(x => x.CodTipoCobranca == model.CodTipoCobranca).ToList();
            }
            if (model.CodConstrutora > 0)
            {
                resultado = resultado.Where(x => x.TabelaRegionalProduto.TabelaRegional.CodConstrutora == model.CodConstrutora).ToList();
            }
            if (model.CodRegional > 0)
            {
                resultado = resultado.Where(x => x.TabelaRegionalProduto.CodRegional == model.CodRegional).ToList();
            }
            if (model.CodProduto > 0)
            {
                resultado = resultado.Where(x => x.TabelaRegionalProduto.CodProduto == model.CodProduto).ToList();
            }
            if (model.CodSintese > 0)
            {
                resultado = resultado.Where(x => x.ContratoHistoricoes.Any() && x.ContratoHistoricoes.LastOrDefault().CodSintese == model.CodSintese).ToList();
            }
            if (model.CodEscritorio > 0)
            {
                resultado = resultado.Where(x => x.CodEscritorio == model.CodEscritorio).ToList();
            }
            if (model.CodAging > 0)
            {
                resultado = resultado.Where(x => x.CodAging == model.CodAging).ToList();
            }
            return resultado.ToList();
        }
        private void PreencherListas(ref HomePesquisaViewModel model)
        {
        }

        [HttpPost]
        public JsonResult ListarBlocoPorEmpreendimento(int CodEmpreendimento)
        {
            var lista = _dbBloco.All().Where(x => x.CodEmpreend == CodEmpreendimento).ToList();
            lista.Insert(0, new TabelaBloco());
            var dados = from p in lista select new { Cod = p.Cod, Text = p.NomeBloco };
            return Json(dados);
        }


        public void GraficosAging(HomePesquisaViewModel model)
        {
            ViewBag.GraficoAging = (from p in model.HistoricoesParcela
                                    group p by new { aging = p.Aging, codAging = p.CodAging, Cor = p.CorAging } into g
                                    orderby g.Key.codAging
                                    select new
                                    {
                                        agingId = g.Key.codAging,
                                        name = g.Key.aging,
                                        Valor = g.Sum(x => x.ValorAberto),
                                        y = g.Select(x => x.CodParcela).Distinct().Count(),
                                        color = "#" + g.Key.Cor
                                    }).ToList();

            ViewBag.GraficoAgingValor = (from p in model.HistoricoesParcela
                                         group p by new { aging = p.Aging, codAging = p.CodAging, Cor = p.CorAging } into g
                                         orderby g.Key.codAging
                                         select new
                                         {
                                             agingId = g.Key.codAging,
                                             name = g.Key.aging,
                                             Valor = g.Sum(x => x.ValorAberto),
                                             y = g.Sum(x => x.ValorAberto),
                                             color = "#" + g.Key.Cor
                                         }).ToList();
        }
        //[HttpPost]
        //public JsonResult ObterDadosAging(HomePesquisaViewModel model)
        //{

        //    var filtro = WebRotinas.CookieObjetoLer<HomePesquisaViewModel>(_filtroSessionId);

        //    if (filtro.regionalIds != null)
        //    {
        //        model.CodRegionais = String.Join(",", filtro.regionalIds);
        //    }
        //    if (filtro.construtorasIds != null)
        //    {
        //        model.CodConstrutoras = String.Join(",", filtro.construtorasIds);
        //    }
        //    if (filtro.produtosIds != null)
        //    {
        //        model.CodProdutos = String.Join(",", filtro.produtosIds);
        //    }
        //    if (filtro.agingIds != null)
        //    {
        //        model.CodAgings = String.Join(",", filtro.agingIds);
        //    }
        //    var usuarioFiltro = UsuarioAFiltrar(model);
        //    var listaTabelaAging = _qry.FiltrarRelatorioAging(model.CodTipoCobranca, model.CodConstrutoras, model.CodEmpreendimento, model.CodRegionais, model.CodProdutos, model.CodBloco, model.CodEscritorio, model.CodAging);

        //    var maximo = listaTabelaAging.Sum(x => x.QtdContrato);
        //    //var categorias = from a in listaTabelaAging select new { aging = a.Aging };

        //    //var dados = _dbContrato.All().GroupBy(x => x.TabelaAging);
        //    //.Where(x => x.TabelaUnidade.TabelaBloco.CodEmpreend == model.CodEmpreendimento)
        //    var json = from p in listaTabelaAging
        //               select new
        //               {
        //                   name = p.Aging,
        //                   Valor = p.ValorAtraso,
        //                   y = p.QtdParcela,
        //                   color = "#" + p.Cor,
        //                   maximo = maximo,
        //                   agingId = p.CodAging
        //               };

        //    return Json(json);
        //}

        //[HttpPost]
        //public JsonResult ObterDadosAgingValor(HomePesquisaViewModel model)
        //{
        //    var filtro = WebRotinas.CookieObjetoLer<HomePesquisaViewModel>(_filtroSessionId);

        //    if (filtro.regionalIds != null)
        //    {
        //        model.CodRegionais = String.Join(",", filtro.regionalIds);
        //    }
        //    if (filtro.construtorasIds != null)
        //    {
        //        model.CodConstrutoras = String.Join(",", filtro.construtorasIds);
        //    }
        //    if (filtro.produtosIds != null)
        //    {
        //        model.CodProdutos = String.Join(",", filtro.produtosIds);
        //    }


        //    var listaTabelaAging = _qry.FiltrarRelatorioAging(model.CodTipoCobranca, model.CodConstrutoras, model.CodEmpreendimento, model.CodRegionais, model.CodProdutos, model.CodBloco, model.CodEscritorio, model.CodAging);

        //    var maximo = listaTabelaAging.Sum(x => x.QtdContrato);
        //    //var categorias = from a in listaTabelaAging select new { aging = a.Aging };

        //    //var dados = _dbContrato.All().GroupBy(x => x.TabelaAging);
        //    //.Where(x => x.TabelaUnidade.TabelaBloco.CodEmpreend == model.CodEmpreendimento)
        //    var json = from p in listaTabelaAging
        //               select new
        //               {
        //                   name = p.Aging,
        //                   Valor = p.ValorAtraso,
        //                   y = p.ValorAtraso,
        //                   color = "#" + p.Cor,
        //                   maximo = maximo,
        //                   agingId = p.CodAging
        //               };

        //    return Json(json);
        //}


        [HttpGet]
        public ActionResult MapaDeCliente()
        {
            HomePesquisaViewModel filtro = new HomePesquisaViewModel();
            CarregarViewBags(filtro);
            return View(filtro);
        }

        [HttpGet]
        public ActionResult ContatoAtraso()
        {
            HomePesquisaViewModel filtro = new HomePesquisaViewModel();

            CarregarViewBags(filtro);
            filtro.ListaFase = _repFase.All().OrderBy(x => x.TabelaTipoCobranca.Ordem).ToList();
            filtro.ListaContratoesDados = FiltrarContratoHome(filtro);
            return View(filtro);
        }

        [HttpPost]
        public ActionResult ContatoAtraso(HomePesquisaViewModel model)
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Login", null);
            }
            CarregarViewBags(model);

            model.ListaFase = _repFase.All().ToList();
            model.ListaContratoesDados = FiltrarContratoHome(model);
            return View(model);
        }

        private void CarregarViewBags(HomePesquisaViewModel filtro)
        {
            var listaCobranca = _repTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca && !x.DataExcluido.HasValue).ToList();

            listaCobranca.Insert(0, new TabelaTipoCobranca());
            ViewBag.ListaCobranca = from p in listaCobranca.OrderBy(x => x.TipoCobranca).ToList() select new DictionaryEntry(DbRotinas.Descriptografar(p.TipoCobranca), p.Cod);


            var listaConstrutora = _repConstrutora.All().ToList();
            listaConstrutora.Insert(0, new TabelaConstrutora());
            ViewBag.ListaConstrutora = from p in listaConstrutora.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);


            var listaPrazo = _dbTabelaDia.All().ToList();
            ViewBag.ListandoDia = listaPrazo;

            var listaRegional = _repRegional.All().ToList();
            listaRegional.Insert(0, new TabelaRegional());
            ViewBag.ListaRegional = from p in listaRegional.OrderBy(x => x.Regional).ToList() select new DictionaryEntry(p.Regional, p.Cod);

            var listaProduto = _repProduto.All().ToList();
            listaProduto.Insert(0, new TabelaProduto());
            ViewBag.ListaProduto = from p in listaProduto.OrderBy(x => x.Descricao).ToList() select new DictionaryEntry(p.Descricao, p.Cod);

            var ListaFilial = _repRegional.All().ToList();
            ListaFilial.Insert(0, new TabelaRegional());
            ViewBag.ListaFilial = from p in ListaFilial.OrderBy(x => x.Regional).ToList() select new DictionaryEntry(p.Regional, p.Cod);

            var listaEscritorio = _repTabelaEscritorio.All().ToList();

            listaEscritorio.Insert(0, new TabelaEscritorio());
            ViewBag.ListaEscritorio = from p in listaEscritorio.OrderBy(x => x.Escritorio).ToList() select new DictionaryEntry(p.Escritorio, p.Cod);


            var listaUsuario = _dbUsuario.All().ToList();

            listaUsuario.Insert(0, new Usuario());
            ViewBag.ListaUsuario = from p in listaUsuario.OrderBy(x => x.Nome).ToList() select new DictionaryEntry(p.Nome, p.Cod);

            var listaAging = _dbTabelaAging.All().ToList();

            listaAging.Insert(0, new TabelaAging());
            ViewBag.ListaAging = from p in listaAging.OrderBy(x => x.Aging).ToList() select new DictionaryEntry(p.Aging, p.Cod);
            var listAging = _dbTabelaAging.All().ToList();
            ViewBag.ListandoAging = listAging;
        }

        [HttpPost]
        public ActionResult MapaDeCliente(HomePesquisaViewModel filtro)
        {
            CarregarViewBags(filtro);
            return View(filtro);
        }



        [HttpPost]
        public JsonResult ListarCEPs(int? CodTipoCobranca, int? CodEmpresa, int? CodFilial, int? CodProduto, int? CodAging)
        {
            var all = _dbCliente.All();

            if (CodTipoCobranca > 0)
            {
                all = all.Where(x => x.ContratoClientes.Any(c => c.Contrato.CodTipoCobranca == CodTipoCobranca));
            }
            //if (CodEmpresa > 0)
            //{
            //    all = all.Where(x => x.CodEmpresa == CodEmpresa);
            //}
            if (CodProduto > 0)
            {
                all = all.Where(x => x.ContratoClientes.Any(c => c.Contrato.CodRegionalProduto == CodProduto));
            }
            //if (CodAging > 0)
            //{
            //    all = all.Where(x => x.CodAging == CodAging);
            //}
            if (CodFilial > 0)
            {
                all = all.Where(x => x.ContratoClientes.Any(c => c.Contrato.TabelaRegionalProduto.CodRegional == CodFilial));
            }

            var teste = (from p in all.ToList()
                         group p by p.Cidade into g
                         select new { Cidade = g.ToList() }).First();


            var listaClieteEndereco = from p in all.ToList().Take(30)

                                      select new
                                      {
                                          nome = p.Nome,
                                          cep = p.CEP,
                                          endereco = string.Format("{0} , {1} , {2}", p.Endereco, p.Bairro, p.Cidade),
                                          numero = p.Cod,
                                          status = "vendido"//EnumeradoresDescricao.StatusProposta(p.Status),
                                      };

            return Json(new
            {
                OK = true,
                Dados = listaClieteEndereco
            });
        }

        [HttpGet]
        public JsonResult GetPoint(int? de, int? ate)
        {
            var all = _dbCliente.All();

            var take = (int)ate - (int)de;
            var listaClieteEndereco = from p in all.ToList().Skip((int)de).Take(take)
                                      select new
                                      {
                                          nome = p.Nome,
                                          cep = p.CEP,
                                          endereco = string.Format("{0} , {1} , {2}", p.Endereco, p.Bairro, p.Cidade),
                                          numero = p.Cod,
                                          status = "vendido"//EnumeradoresDescricao.StatusProposta(p.Status),
                                      };

            return Json(new
            {
                OK = true,
                Data = listaClieteEndereco
            });
        }



        [HttpGet]
        public JsonResult filtroSessao()
        {
            var filtro = WebRotinas.CookieObjetoLer<HomePesquisaViewModel>(_filtroSessionId);

            return Json(new { OK = true, construtoras = filtro.construtorasIds, regionais = filtro.regionalIds, produtos = filtro.produtosIds, agings = filtro.agingIds, usuarios = filtro.usuarioIds }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult alterarIdiomaGet(short tipo)
        {
            WebRotinas.AtualizarIdiomaCache(tipo);

            Idioma.LimparCache();

            return RedirectToAction("Index");
        }

        public JsonResult AlterarIdiomaSistema(short tp)
        {
            OperacaoRetorno op = new OperacaoRetorno();

            try
            {
                WebRotinas.AtualizarIdiomaCache(tp);
                op.OK = true;
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }
        [HttpGet]
        public FileStreamResult DownloadCSV(string guid, string nm)
        {
            string filename = WebRotinas.ObterArquivoTemp(guid);
            FileInfo info = new FileInfo(filename);
            return File(info.OpenRead(), "application/force-download", nm);
        }
    }
}