﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using Cobranca.Web.Rotinas;
using Recebiveis.Web.Models;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Recebiveis.Web.Controllers
{
    public class ReguaCampanhaController : Controller
    {

        Repository<TabelaCampanha> _dbCampanha = new Repository<TabelaCampanha>();
        Repository<TabelaAcaoCobranca> _dbAcao = new Repository<TabelaAcaoCobranca>();
        Repository<TabelaCampanhaAcao> _dbCampanhaAcao = new Repository<TabelaCampanhaAcao>();
        Repository<TabelaCampanhaCondicao> _dbCampanhaCondicao = new Repository<TabelaCampanhaCondicao>();
        Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();
        Repository<TabelaTipoBloqueio> _dbTipoBloqueio = new Repository<TabelaTipoBloqueio>();
        Repository<TabelaAging> _dbAging = new Repository<TabelaAging>();
        Repository<TabelaSintese> _dbSintese = new Repository<TabelaSintese>();

        ListasRepositorio _dbListas = new ListasRepositorio();
        // GET: Regua
        public ActionResult Index()
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();

            List<TabelaCampanha> tipos = _dbCampanha.All().ToList();
            ViewBag.ListaAcoes = _dbAcao.All().ToList();
            return View(tipos);
        }

        public ActionResult Filtro()
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();

            var lista = _dbCampanhaCondicao.All().Where(x => x.Excecao == false).ToList();
            TratarListaCondicoes(ref lista);
            ViewBag.ListaCampanha = _dbListas.Campanha();
            ViewBag.ListaTipo = EnumeradoresLista.ListaCondicaoTipos();
            ViewBag.ListaCondicao = EnumeradoresLista.ListaCondicao();
            return View(lista);
        }
        public ActionResult Excecao()
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();

            var lista = _dbCampanhaCondicao.All().Where(x => x.Excecao == true).ToList();
            TratarListaCondicoes(ref lista);
            ViewBag.ListaCampanha = _dbListas.Campanha();
            ViewBag.ListaTipo = EnumeradoresLista.ListaCondicaoTipos();
            ViewBag.ListaCondicao = EnumeradoresLista.ListaCondicao();
            return View(lista);
        }

        private void TratarListaCondicoes(ref List<TabelaCampanhaCondicao> lista)
        {
            foreach (var item in lista)
            {
                switch ((Enumeradores.CondicaoTipo)item.Tipo)
                {
                    case Enumeradores.CondicaoTipo.ValorSaldo:
                    case Enumeradores.CondicaoTipo.ValorParcela:
                    case Enumeradores.CondicaoTipo.ValorContrato:
                        {
                            item.Valor = string.Format("{0:C}", DbRotinas.ConverterParaFloat(item.Valor));
                            break;
                        }
                    case Enumeradores.CondicaoTipo.DiasAtraso:
                        {
                            item.Valor = item.Valor + " dia(s)";
                            break;
                        }
                    case Enumeradores.CondicaoTipo.TipoBloqueio:
                        {
                            item.Valor = _dbTipoBloqueio.FindById(DbRotinas.ConverterParaInt(item.Valor)).TipoBloqueio;
                            break;
                        }
                    case Enumeradores.CondicaoTipo.Aging:
                        {
                            item.Valor = _dbAging.FindById(DbRotinas.ConverterParaInt(item.Valor)).Aging;
                            break;
                        }
                    case Enumeradores.CondicaoTipo.StatusFaseSintese:
                        {
                            var sintese = _dbSintese.FindById(DbRotinas.ConverterParaInt(item.Valor));
                            if (sintese != null)
                            {
                                item.Valor = string.Format("{0} - {1}", sintese.TabelaFase.Fase, sintese.Sintese);
                            }
                            else
                            {
                                item.Valor = item.Valor;
                            }
                            
                            break;
                        }
                    case Enumeradores.CondicaoTipo.Empreendimento:
                        {
                            item.Valor = _dbEmpreendimento.FindById(DbRotinas.ConverterParaInt(item.Valor)).NomeEmpreendimento;
                            break;
                        }
                    default:
                        {
                            item.Valor = item.Valor;
                            break;
                        }

                }

            }
        }

        [HttpPost]
        public ActionResult Filtro(TabelaCampanhaCondicao dados)
        {

            if (ModelState.IsValid)
            {
                dados.DataCadastro = DateTime.Now;
                _dbCampanhaCondicao.CreateOrUpdate(dados);

                ViewBag.MensagemOK = Language.MensagemOK;
                ViewBag.RedirecionarUrl = Url.Action("Filtro", "Regua");
            }

            var lista = _dbCampanhaCondicao.All().Where(x => x.Excecao == false).ToList();
            TratarListaCondicoes(ref lista);
            ViewBag.ListaTipoCobranca = _dbListas.TipoCobranca();
            ViewBag.ListaTipo = EnumeradoresLista.ListaCondicaoTipos();
            ViewBag.ListaCondicao = EnumeradoresLista.ListaCondicao();
            return View(lista);
        }

        [HttpPost]
        public ActionResult Excecao(TabelaCampanhaCondicao dados)
        {


            if (ModelState.IsValid)
            {
                dados.DataCadastro = DateTime.Now;
                _dbCampanhaCondicao.CreateOrUpdate(dados);

                ViewBag.MensagemOK = Language.MensagemOK;
                ViewBag.RedirecionarUrl = Url.Action("Excecao", "Regua");
            }
            var lista = _dbCampanhaCondicao.All().Where(x => x.Excecao == true).ToList();
            TratarListaCondicoes(ref lista);
            ViewBag.ListaCampanha = _dbListas.Campanha();
            ViewBag.ListaTipo = EnumeradoresLista.ListaCondicaoTipos();
            ViewBag.ListaCondicao = EnumeradoresLista.ListaCondicao();
            return View(lista);
        }


        [HttpPost]
        public JsonResult ReguaSalvar(List<ReguaCadastroCampanhaViewModel> dados)
        {
            try
            {

                _dbCampanhaAcao.Delete(x => !x.DataExcluido.HasValue);

                foreach (var tipo in dados)
                {
                    foreach (var acao in tipo.CodAcoes)
                    {
                        var i = new TabelaCampanhaAcao()
                        {

                            CodAcao = acao,
                            CodCampanha = tipo.CodCampanha,
                            DataCadastro = DateTime.Now
                        };
                        _dbCampanhaAcao.CreateOrUpdate(i);
                    }
                }



                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }


        }

        [HttpPost]
        public JsonResult ListaTipoBloqueio()
        {
            var lista = _dbListas.TipoBloqueio();

            var selectList = from p in lista select new { Cod = p.Value, Text = p.Key };

            return Json(selectList);
        }
        [HttpPost]
        public JsonResult ListaAging()
        {
            var lista = _dbListas.Aging();

            var selectList = from p in lista select new { Cod = p.Value, Text = p.Key };

            return Json(selectList);
        }
        [HttpPost]
        public JsonResult ListaEmpreendimento()
        {
            var lista = _dbListas.Empreendimentos();

            var selectList = from p in lista select new { Cod = p.Value, Text = p.Key };

            return Json(selectList);
        }
        [HttpPost]
        public JsonResult ListaFaseSintese()
        {
            var lista = _dbListas.FaseSintese();

            var selectList = from p in lista select new { Cod = p.Value, Text = p.Key };

            return Json(selectList);
        }
        [HttpPost]
        public JsonResult ExcluirCondicao(int cod = 0)
        {
            try
            {
                _dbCampanhaCondicao.Delete(cod);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {

                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
    }
}