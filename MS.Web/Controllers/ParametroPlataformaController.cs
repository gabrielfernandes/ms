﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.IO;
using System.Data.Entity.Validation;

namespace Cobranca.Web.Controllers
{
    [Authorize]
    public class ParametroPlataformaController : Controller
    {
        public Repository<ParametroPlataforma> _dbParametroPlataforma = new Repository<ParametroPlataforma>();

        [HttpGet]
        public ActionResult Cadastro(int id = 0)
        {
            var dados = _dbParametroPlataforma.All().FirstOrDefault();
            ViewBag.LayoutTipo = EnumeradoresLista.LayoutSistema();
            return View(dados);
        }
        [HttpPost]
        public ActionResult Cadastro(ParametroPlataforma parametro)
        {
            try
            {
                var parametroOriginal = _dbParametroPlataforma.FindById(parametro.Cod);

                if (string.IsNullOrEmpty(parametro.SMTPSenha))
                {
                    parametro.SMTPSenha = parametroOriginal.SMTPSenha;
                }

                parametroOriginal.Logo = parametro.Logo;
                parametroOriginal.TituloPagina = parametro.TituloPagina;
                parametroOriginal.LayoutTipo = parametro.LayoutTipo;
                parametroOriginal.EmailRemetente = parametro.EmailRemetente;
                parametroOriginal.NomeRemetente = parametro.NomeRemetente;
                parametroOriginal.SMTPPorta = parametro.SMTPPorta;
                parametroOriginal.SMTPServidor = parametro.SMTPServidor;
                parametroOriginal.SMTPUsuario = parametro.SMTPUsuario;
                parametroOriginal.SMTPSenha = parametro.SMTPSenha;
                parametroOriginal.fgSSL = parametro.fgSSL;
                parametroOriginal.CaminhoLogo = parametro.CaminhoLogo;
                parametroOriginal.Layout = parametro.Layout;
                parametroOriginal.Icone = parametro.Icone;
                parametroOriginal.NomeEmpresa = parametro.NomeEmpresa;
                parametroOriginal.UrlSistema = parametro.UrlSistema;

                parametro = _dbParametroPlataforma.CreateOrUpdate(parametroOriginal);

                ViewBag.LayoutTipo = EnumeradoresLista.LayoutSistema();
                //AnexarDocumento(ref parametro, Request);
                WebRotinas.ParametroEmpresaGravar(parametro);
                ViewBag.MensagemOK = "Operação realizada com sucesso!";
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ModelState.AddModelError(validationError.PropertyName, validationError.ErrorMessage);
                    }
                }

            }
            return View(parametro);
        }

        private void AnexarDocumento(ref ParametroPlataforma par, HttpRequestBase request)
        {
            string pathUpload = Server.MapPath("~/Content/files");
            string pathDefinitivo = Server.MapPath(string.Format("~/Empresa/Logo/", par.Cod));
            string file = String.Format("~/Empresa/Logo/{0}", par.Cod);

            if (!Directory.Exists(pathDefinitivo))
                Directory.CreateDirectory(pathDefinitivo);
            if (file != null)
            {
                string pathDoc = Path.Combine(pathUpload, file);
                FileInfo fileInfo = new FileInfo(pathDoc);
                if (fileInfo.Exists)
                {
                    fileInfo.MoveTo(Path.Combine(pathDefinitivo, Convert.ToString(par.Cod)));
                }
            }
        }

        public ActionResult SaveUploadedFile()
        {
            bool isSavedSuccessfully = true;
            string fName = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {

                        var originalDirectory = new DirectoryInfo(string.Format("{0}Images\\WallImages", Server.MapPath(@"\")));

                        string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "imagepath");

                        var fileName1 = Path.GetFileName(file.FileName);

                        bool isExists = System.IO.Directory.Exists(pathString);

                        if (!isExists)
                            System.IO.Directory.CreateDirectory(pathString);

                        var path = string.Format("{0}\\{1}", pathString, file.FileName);
                        file.SaveAs(path);

                    }

                }

            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }


            if (isSavedSuccessfully)
            {
                return Json(new { Message = fName });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        [HttpPost]
        public JsonResult AlterarLogoEmpresa(string arquivo)
        {

            var dados = _dbParametroPlataforma.All().FirstOrDefault();

            string pathUpload = Server.MapPath("~/Content/files");
            string arquivoUpload = Server.MapPath(string.Format("~/Content/files/{0}", arquivo));
            string pathDefinitivo = Server.MapPath(string.Format("~/Content/"));
            string file = Server.MapPath("~/Content/logo.png");



            if (!Directory.Exists(pathDefinitivo))
                Directory.CreateDirectory(pathDefinitivo);
            if (System.IO.File.Exists(arquivoUpload))
            {

                try
                {
                    if (System.IO.File.Exists(file))
                        System.IO.File.Delete(file);
                }
                catch (Exception ex)
                {

                }

                System.IO.File.Move(arquivoUpload, file);
                dados.CaminhoLogo = "~/Content/logo.png";
                dados = _dbParametroPlataforma.CreateOrUpdate(dados);
                WebRotinas.ParametroEmpresaGravar(dados);
            }

            return Json(new { OK = true });
        }


        [HttpPost]
        public JsonResult AlterarIconeEmpresa(string arquivo)
        {

            var dados = _dbParametroPlataforma.All().FirstOrDefault();

            string pathUpload = Server.MapPath("~/Content/files");
            string arquivoUpload = Server.MapPath(string.Format("~/Content/files/{0}", arquivo));
            string pathDefinitivo = Server.MapPath(string.Format("~/Empresa/Logo/"));
            string file = Server.MapPath(String.Format("~/Empresa/Logo/{0}", arquivo));



            if (!Directory.Exists(pathDefinitivo))
                Directory.CreateDirectory(pathDefinitivo);
            if (System.IO.File.Exists(arquivoUpload))
            {

                try
                {
                    if (System.IO.File.Exists(file))
                        System.IO.File.Delete(file);
                }
                catch (Exception ex)
                {

                }

                System.IO.File.Move(arquivoUpload, file);
                dados.Icone = String.Format("~/Empresa/Logo/{0}", arquivo);
                dados = _dbParametroPlataforma.CreateOrUpdate(dados);
                WebRotinas.ParametroEmpresaGravar(dados);
            }

            return Json(new { OK = true });
        }
        [HttpGet, AllowAnonymous]
        public FileResult ObterLogoEmpresa()
        {

            string str = WebRotinas.ObterParametro().LogoEmpresa;

            if (string.IsNullOrEmpty(str) || !System.IO.File.Exists(Server.MapPath(str)))
            {
                str = "~/Content/logo.png";
            }

            return File(Url.Content(str), " image");
        }
        [HttpGet]
        public FileResult ObterIconeEmpresa()
        {

            string str = WebRotinas.ObterParametro().Icone;

            if (string.IsNullOrEmpty(str) || !System.IO.File.Exists(Server.MapPath(str)))
            {
                str = "~/Content/img/logo/icon_junix.ico";
            }

            return File(Url.Content(str), " image");
        }

        [HttpGet]
        public ActionResult Idioma(int id = 0)
        {


            var lista = new IdiomaDb().Listagem();
            return View(lista);
        }
        [HttpPost]
        public JsonResult SalvarPalavra(string texto)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                if (string.IsNullOrEmpty(texto))
                {
                    op.addCampoErro("novaPalavra", Cobranca.Rotinas.Idioma.Traduzir("Campo obrigatório"));
                    return Json(op);
                }

                new IdiomaDb().SalvarTraducao(new Db.Models.Pesquisa.IdiomaInfo()
                {
                    Cod = texto,
                    enUS = texto,
                    esES = texto,
                    ptBR = texto
                });

                Cobranca.Rotinas.Idioma.LimparCache();
                op.OK = true;
                op.Dados = Url.Action("Idioma", "ParametroPlataforma", new { cod = Url.Encode(texto) });
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }
        [HttpPost]
        public JsonResult SalvarTraducao(string name, string pk, string value)
        {
            var op = new IdiomaDb().AlterarIdioma(name, pk, value);
            Cobranca.Rotinas.Idioma.LimparCache();
            return Json(op);
        }
    }
}