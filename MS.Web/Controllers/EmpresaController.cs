﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class EmpresaController : BasicController<TabelaEmpresa>
    {
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaGestao> _dbGestao = new Repository<TabelaGestao>();

        public override void carregarCustomViewBags()
        {
            var listaConstrutoras = _dbConstrutora.All();
            var listaDeGestao = _dbGestao.All();

            ViewBag.ListaConstrutora = from p in listaConstrutoras.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);
            ViewBag.ListaGestao = from p in listaDeGestao.OrderBy(x => x.NomeGestao).ToList() select new DictionaryEntry(p.NomeGestao, p.Cod);
            
            base.carregarCustomViewBags();
        }

      
    }
}