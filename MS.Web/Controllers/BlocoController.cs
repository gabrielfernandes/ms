﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class BlocoController : BasicController<TabelaBloco>
    {

        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();
        public Repository<TabelaUnidade> _dbUnidade = new Repository<TabelaUnidade>();
        public override void carregarCustomViewBags()
        {
            //junix_cobrancaContext con = new junix_cobrancaContext();
            var listaContrutoras = _dbConstrutora.All();
            var listaEmpreendimentos = _dbEmpreendimento.All();       
            ViewBag.ListaConstrutora = from p in listaContrutoras.OrderBy(x => x.NomeConstrutora).ToList() select new DictionaryEntry(p.NomeConstrutora, p.Cod);
            ViewBag.listaEmpreendimento = from p in listaEmpreendimentos.Where(x => x.CodConstrutora == x.CodConstrutora).ToList() select new DictionaryEntry(p.NomeEmpreendimento, p.Cod);
            base.carregarCustomViewBags();
        }

        public override void Ok(TabelaBloco dados, HttpRequestBase Request)
        {
            base.Ok(dados, Request);
        }

        [HttpPost]
        public JsonResult Pesquisar(int id = 0)
        {
            var lista = _repository.All().Where(x => x.CodEmpreend == id).ToList();
            //lista.Insert(0, new TabelaBloco() { NomeBloco = "" });
            var dados = from p in lista select new { Cod = p.Cod, Text = p.NomeBloco };
            return Json(dados);
        }
        [HttpPost]
        public JsonResult ListarUnidadePorBloco(int CodBloco = 0)
        {
            var lista = _dbUnidade.All().Where(x => x.CodBloco == CodBloco).ToList();
            lista.Insert(0, new TabelaUnidade());
            var dados = from p in lista select new { Cod = p.Cod, Text = p.NumeroUnidade };
            return Json(dados);
        }

    }
}