﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class LogServicoController : BasicController<LogServico>
    {
        public Repository<LogServico> _repository = new Repository<LogServico>();
        [HttpGet]
        public override ActionResult Index()
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();
            var listaDados = new List<LogServico>();

            carregarCustomViewBags();
            var usuarioLogado = WebRotinas.ObterUsuarioLogado();
            listaDados = _repository.All().OrderByDescending(x => x.Cod).Take(500).ToList();

            return View(listaDados);
        }

        [HttpGet]
        public ActionResult FiltrarTimeLine(LogServicoViewModel model)
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();
            var listaDados = new List<LogServico>();
            var qry = _repository.FindAll(x => !x.DataExcluido.HasValue);
            if (!String.IsNullOrEmpty(model.Filtro))
            {
                qry = qry.Where(x => x.Mensagem.Contains(model.Filtro));
            }
            if (!String.IsNullOrEmpty(model.PeriodoDe))
            {
                var date = DbRotinas.ConverterParaDatetime(model.PeriodoDe);
                qry = qry.Where(x => x.DataInicio >= date);
            }
            if (!String.IsNullOrEmpty(model.PeriodoAte))
            {
                var date = DbRotinas.ConverterParaDatetime(model.PeriodoAte);
                qry = qry.Where(x => x.DataInicio <= date);
            }
            listaDados = qry.OrderByDescending(x => x.Cod).Take(500).ToList();
            return PartialView("_timeline", listaDados);
        }


    }
}