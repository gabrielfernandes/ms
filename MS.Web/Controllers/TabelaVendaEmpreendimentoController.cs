﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class TabelaVendaEmpreendimentoController : BasicController<TabelaVendaEmpreendimento>
    {
        private Repository<TabelaVenda> _dbTabelaVenda = new Repository<TabelaVenda>();
        private Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();


        public override ActionResult Cadastro(int id = 0)
        {
            int tabelaId = Cobranca.Db.Rotinas.DbRotinas.ConverterParaInt(Request.QueryString["tabela"]);
            ViewBag.ListaEmpreendimentoTabela = _repository.All().Where(x => x.CodTabelaVenda == tabelaId).ToList();

            return base.Cadastro(id);
        }
        public override ActionResult Cadastro(TabelaVendaEmpreendimento dados)
        {
            ViewBag.ListaEmpreendimentoTabela = _repository.All().Where(x => x.CodTabelaVenda == dados.CodTabelaVenda).ToList();
            ViewBag.RedirecionarUrl = Url.Action("Cadastro", "TabelaVendaEmpreendimento", new { tabela = dados.CodTabelaVenda });
            return base.Cadastro(dados);
        }

        public override void carregarCustomViewBags()
        {
            var listaTabelaVenda = _dbTabelaVenda.All().ToList();
            listaTabelaVenda.Insert(0, new TabelaVenda());
            ViewBag.ListaTabelaVenda = from p in listaTabelaVenda.OrderBy(x => x.Nome).ToList() select new DictionaryEntry(p.Nome, p.Cod);

            var listaEmpreendimento = _dbEmpreendimento.All().ToList();
            listaEmpreendimento.Insert(0, new TabelaEmpreendimento());
            ViewBag.ListaEmpreendimento = from p in listaEmpreendimento.OrderBy(x => x.NomeEmpreendimento).ToList() select new DictionaryEntry(p.NomeEmpreendimento, p.Cod);


            base.carregarCustomViewBags();
        }
    }
}