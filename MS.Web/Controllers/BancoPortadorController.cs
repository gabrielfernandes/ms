﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using Cobranca.Rotinas.BoletoGerar;
using BoletoNet;
using System.Configuration;
using System.IO;
using Cobranca.Rotinas.BoletoGerar;
using Cobranca.Rotinas.BNet;
using Cobranca.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class BancoPortadorController : BasicController<TabelaBancoPortador>
    {
        public Repository<TabelaBanco> _dbBanco = new Repository<TabelaBanco>();
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<TabelaBancoPortador> _dbParametroBoleto = new Repository<TabelaBancoPortador>();
        public Repository<TabelaBancoPortadorGrupo> _dbParametroBoletoGrupo = new Repository<TabelaBancoPortadorGrupo>();
        public Repository<TabelaBancoPortadorInstrucao> _dbBoletoInstrucao = new Repository<TabelaBancoPortadorInstrucao>();
        public Repository<ContratoParcela> _dbContratoParcela = new Repository<ContratoParcela>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        public Repository<TabelaRegionalGrupo> _dbRegionalGrupo = new Repository<TabelaRegionalGrupo>();

        //[HttpGet]
        //public override ActionResult Cadastro(int id = 0)
        //{
        //    carregarCustomViewBags();

        //    var dados = _dbParametroBoleto.All().First();
        //    if (id == 0)
        //    {
        //        dados.DataCadastro = DateTime.Now;
        //    }
        //    return View(dados);
        //}

        public override void carregarCustomViewBags()
        {
            var bancos = _dbBanco.All().ToList();
            ViewBag.ListaBancos = from p in bancos.OrderBy(x => x.NomeBanco) select new DictionaryEntry(p.Cod, $"{p.NomeBanco} - Instrumento ({p.CodCliente})");



            var listaInstrucao = new List<DictionaryEntry>();
            listaInstrucao.Add(new DictionaryEntry() { Key = null, Value = null });
            listaInstrucao.Add(new DictionaryEntry(EnumeradoresDescricao.InstrucaoBancoPortador(Enumeradores.InstrucaoBancoPortador.BancoAutorizado), (short)Enumeradores.InstrucaoBancoPortador.BancoAutorizado));
            listaInstrucao.Add(new DictionaryEntry(EnumeradoresDescricao.InstrucaoBancoPortador(Enumeradores.InstrucaoBancoPortador.CobrarAtrasoDia), (short)Enumeradores.InstrucaoBancoPortador.CobrarAtrasoDia));
            listaInstrucao.Add(new DictionaryEntry(EnumeradoresDescricao.InstrucaoBancoPortador(Enumeradores.InstrucaoBancoPortador.CobrarMultaValor), (short)Enumeradores.InstrucaoBancoPortador.CobrarMultaValor));
            listaInstrucao.Add(new DictionaryEntry(EnumeradoresDescricao.InstrucaoBancoPortador(Enumeradores.InstrucaoBancoPortador.ConcederDesconto), (short)Enumeradores.InstrucaoBancoPortador.ConcederDesconto));
            listaInstrucao.Add(new DictionaryEntry(EnumeradoresDescricao.InstrucaoBancoPortador(Enumeradores.InstrucaoBancoPortador.ConcederDescontoValordia), (short)Enumeradores.InstrucaoBancoPortador.ConcederDescontoValordia));
            listaInstrucao.Add(new DictionaryEntry(EnumeradoresDescricao.InstrucaoBancoPortador(Enumeradores.InstrucaoBancoPortador.DispensarJuros), (short)Enumeradores.InstrucaoBancoPortador.DispensarJuros));

            ViewBag.ListaInstrucoes = listaInstrucao.OrderBy(x => x.Value);

            var listaGrupo = _dbParametroBoletoGrupo.All()
              .OrderBy(x => x.NomeArquivo)
              .ToList();
            listaGrupo.Insert(0, new TabelaBancoPortadorGrupo { NomeArquivo = "Selecione", Cod = 0 });
            ViewBag.GrupoBancoPortador = from p in listaGrupo.ToList() select new DictionaryEntry(p.NomeArquivo, p.Cod);
            base.carregarCustomViewBags();
        }
        #region GRUPO BANCO PORTADOR
        [HttpGet]
        public ActionResult DadosGrupo(int cod = 0)
        {
            var dados = new Cobranca.Db.Models.TabelaBancoPortadorGrupo();
            if (cod > 0) dados = _dbParametroBoletoGrupo.FindById(cod);

            return PartialView("_formGrupo", dados);
        }
        [HttpPost, Authorize]
        public JsonResult SalvarGrupo(TabelaBancoPortadorGrupo dados)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                _dbParametroBoletoGrupo.CreateOrUpdate(dados);
                var listaGrupo = _dbParametroBoletoGrupo.All().ToList();
                var dataDrop = from p in listaGrupo.OrderBy(x => x.NomeArquivo).ToList() select new DictionaryEntry(p.Cod, p.NomeArquivo);
                op.OK = true;
                return Json(new { OK = op.OK, Titulo = "Grupo", Mensagem = "Operação Realizada com Sucesso!", DropDown = dataDrop, id = dados.Cod });
            }
            catch (Exception ex)
            {
                return Json(new { OK = op.OK, Titulo = "Ops...", Mensagem = "Tente novamente!", Erro = $"{ex.Message}" });
            }

        }
        [HttpPost]
        public ActionResult RemoverGrupo(int cod = 0)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                var dados = new Cobranca.Db.Models.TabelaBancoPortadorGrupo();
                var parametroBoletos = _dbParametroBoleto.All().Where(x => !x.DataExcluido.HasValue && x.CodBancoPortadorGrupo == cod).ToList();
                foreach (var item in parametroBoletos)
                {
                    _dbParametroBoleto.Delete(item.Cod);
                }
                _dbParametroBoleto.Delete(cod);
                var listaGrupo = _dbParametroBoletoGrupo.All().ToList();
                var dataDrop = from p in listaGrupo.OrderBy(x => x.NomeArquivo).ToList() select new DictionaryEntry(p.Cod, p.NomeArquivo);
                op.OK = true;
                return Json(new { OK = op.OK, Titulo = "Grupo", Mensagem = "Operação Realizada com Sucesso!", DropDown = dataDrop, id = dados.Cod });
            }
            catch (Exception ex)
            {
                return Json(new { OK = op.OK, Titulo = "Ops...", Mensagem = "Tente novamente!", Erro = $"{ex.Message}" });
            }
        }
        [HttpPost]
        #endregion

        public JsonResult GerarBoletoTeste(int Cod = 0)
        {
            try
            {

                BoletoUtils boleto = new BoletoUtils();
                var html = boleto.GerandoBoletoTeste().MontaHtmlEmbedded();

                return Json(new { OK = true, Html = html });
            }
            catch (Exception ex)
            {

                return Json(new { OK = false, Html = ex.Message + " - " + ex.InnerException.Message, Mensagem = ex.Message });
            }


        }
        [HttpPost]
        public JsonResult ObterParametroBancoPortador(short codigoBanco = 0)
        {
            try
            {

                var dados = _repository.All().Where(x => x.BancoNumero == codigoBanco).FirstOrDefault();
                if (dados == null)
                {
                    dados = Activator.CreateInstance<TabelaBancoPortador>();
                    dados.DataCadastro = DateTime.Now;
                }
                return Json(new { OK = true, Id = dados.Cod });
            }
            catch (Exception ex)
            {

                return Json(new { OK = false, Mensagem = ex.Message + " - " + ex.InnerException.Message });
            }


        }
        [HttpGet]
        public ActionResult ImportarRetorno()
        {

            return View();
        }

        [HttpPost]
        public JsonResult ListDataPerServer(DTParameters param)
        {
            try
            {
                var dtsource = ListaDados();
                var result = WebRotinas.DTRotinas.DTResult(param, dtsource);

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
        public IQueryable<object> ListaDados()
        {
            var Dados = _repository.All();
            IQueryable<object> data = null;
            var usuario = WebRotinas.ObterUsuarioLogado();

            data = Dados
                         .ToList()
                         .Select(x => new
                         {
                             Cod = x.Cod,
                             Companhia = x.Companhia,
                             Banco = x.TabelaBanco?.NomeBanco,
                             Regional = x.CodRegionalGrupo > 0 ? (x.TabelaRegionalGrupo.CodRegionalCliente) : "",
                             x.CompanhiaFiscal,
                             Portador = x.Portador,
                             CNPJ = DbRotinas.formatarCpfCnpj(x.CNPJ),
                             Carteira = x.Carteira,
                             Agencia = x.Agencia,
                             Conta = x.Conta,
                             NossoNumeroAtual = x.NossoNumeroAtual,
                             LoteAtual = x.LoteAtual,
                             x.DataInicialRegistroBoleto,
                             DataInicialRegistroBoletoDesc = $"{x.DataInicialRegistroBoleto:dd/MM/yyyy}",
                             x.NumeroDiaRegistroBoleto

                         }).AsQueryable();

            return data;
        }

        [HttpPost]
        public ActionResult ImportarRetorno(FormCollection formCollection)
        {
            try
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                Stream uploadFileStream = file.InputStream;
                string ArquivoNome = "";
                ArquivoNome = file.FileName;


                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string ArquivoTipo = file.ContentType;
                    if (!ArquivoTipo.Equals("application/octet-stream"))
                    {
                        ArquivoNome = "Favor anexar arquivo em formato (*.ret;*.crt)";
                    }
                    else
                    {
                        var banco = montarBanco();
                        ArquivoRetornoCNAB400 cnab400 = new ArquivoRetornoCNAB400();
                        cnab400.LerArquivoRetorno(banco, uploadFileStream);

                        //ArquivoRetorno arquivo = new ArquivoRetorno(TipoArquivo.CNAB400);
                        //arquivo.LerArquivoRetorno(banco, uploadFileStream);

                        if (cnab400 == null)
                        {
                            ArquivoNome = "Arquivo não processado!";
                        }

                        foreach (var detalhe in cnab400.ListaDetalhe)
                        {

                        }

                    }
                }

                TempData["resultado"] = "Retorno Realizado.";
            }
            catch (Exception ex)
            {
                TempData["resultado"] = ex.Message;
            }

            return View();
        }

        [HttpGet]
        public ActionResult FormBanco(int id = 0, short nrBanco = 0)
        {
            carregarCustomViewBags();

            var dados = _repository.FindById(id);
            if (id == 0)
            {
                dados = Activator.CreateInstance<TabelaBancoPortador>();
                dados.DataCadastro = DateTime.Now;
                dados.BancoNumero = nrBanco;
            }

            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            ViewBag.ListaCarteira = lista.ToList();

            return PartialView("_form", dados);
        }

        [HttpGet]
        public JsonResult ObterCarteira(int id = 0)
        {
            try
            {
                var dados = _repository.FindById(id);

                var cdCarteira = (short)0;
                if (dados != null) { cdCarteira = (short)dados.Carteira; }
                return Json(new { OK = true, carteira = cdCarteira }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    OK = false,
                    Mensagem = ex.Message + " - " + ex.InnerException.Message
                });
            }
        }

        public Banco montarBanco()
        {
            var cedente = _dbParametroBoleto.All().FirstOrDefault();

            var boletoInfo = new Banco(cedente.CodBanco.HasValue ? (short)cedente.TabelaBanco.Numero.Value : (short)0);

            return boletoInfo;
        }

        [Authorize]
        [HttpGet]
        public override ActionResult Cadastro(int id = 0)
        {
            var model = _repository.FindById(id);
            if (model == null)
            {
                model = new TabelaBancoPortador();
            }
            carregarCustomViewBags();
            var listaRegionais = _dbRegionalGrupo.All().ToList();
            List<DictionaryEntry> listDrop = new List<DictionaryEntry>();
            foreach (var item in listaRegionais)
            {
                listDrop.Add(new DictionaryEntry(item.Cod, $"{DbRotinas.Descriptografar(item.TabelaRegional.Regional)} - {item.CodRegionalCliente} | {DbRotinas.Descriptografar(item.TabelaConstrutora.NomeConstrutora)} - {item.TabelaConstrutora.CodCliente}"));
            }
            listDrop = listDrop.OrderBy(x => x.Value).ToList();
            listDrop.Insert(0, new DictionaryEntry { Key = 0, Value = "Selecione" });

            ViewBag.dropRegionais = listDrop;

            List<DictionaryEntry> Carteiras = new List<DictionaryEntry>();
            List<DictionaryEntry> InstrucoesPgto = new List<DictionaryEntry>();

            if (model.TabelaBanco?.Numero == (int)EnumBoleto.BancoCodigo.Itau)
            {
                Carteiras = EnumBoletoLista.Carteira_Itau();
                InstrucoesPgto = EnumBoletoLista.Instrucoes_Itau();
            }
            else if (model.TabelaBanco?.Numero == (int)EnumBoleto.BancoCodigo.Santander)
            {
                Carteiras = EnumBoletoLista.Carteira_Santander();
                InstrucoesPgto = EnumBoletoLista.Instrucoes_Santander();
            }

            ViewBag.ListaCarteira = Carteiras;
            ViewBag.dropInstrucao = InstrucoesPgto;

            return View("Cadastro", model);
        }

        [HttpGet]
        public ActionResult AddFormInstrucao(int IdPortador = 0)
        {
            TabelaBancoPortadorInstrucao model = new TabelaBancoPortadorInstrucao();
            var portador = _dbParametroBoleto.FindById(IdPortador);
            model.CodBancoPortador = IdPortador;

            List<DictionaryEntry> InstrucoesPgto = new List<DictionaryEntry>();
            if (portador.TabelaBanco?.Numero == (int)EnumBoleto.BancoCodigo.Itau)
            {
                InstrucoesPgto = EnumBoletoLista.Instrucoes_Itau();
            }
            else if (portador.TabelaBanco?.Numero == (int)EnumBoleto.BancoCodigo.Santander)
            {
                InstrucoesPgto = EnumBoletoLista.Instrucoes_Santander();
            }

            ViewBag.dropInstrucao = InstrucoesPgto;
            return PartialView("_instrucao", model);
        }

        [HttpPost]
        public JsonResult getObservacaoInstrucao(string cdInstrucao = null)
        {
            try
            {
                var obs = UtilsCustom.ObterObservacaoInstrucao(DbRotinas.ConverterParaInt(cdInstrucao));
                return Json(new { OK = true, Html = obs });
            }
            catch (Exception ex)
            {

                return Json(new { OK = false, Mensagem = ex.Message + " - " + ex.InnerException.Message });
            }


        }

        [HttpPost]
        public JsonResult SalvarBancoPortador(TabelaBancoPortador dados)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                if (dados.CodRegionalGrupo == 0) dados.CodRegionalGrupo = null;
                dados = _repository.CreateOrUpdate(dados);
                op.OK = true;
                op.Mensagem = "Operacao realizada com sucesso";//Language.MensagemOK;
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }

            return Json(op);

        }

        [HttpPost]
        public JsonResult ListarCarteira(int CodBanco = 0)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                var banco = _dbBanco.FindById(CodBanco);
                if (banco.Numero == (int)EnumBoleto.BancoCodigo.Itau)
                {
                    op.Dados = EnumBoletoLista.Carteira_Itau();
                }
                else if (banco.Numero == (int)EnumBoleto.BancoCodigo.Santander)
                {
                    op.Dados = EnumBoletoLista.Carteira_Santander();
                }

                op.OK = true;

            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }


            return Json(op);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult SalvarInstrucao(string[] Cod, string[] CodBancoPortador, string[] CodigoInstrucao, string[] Numero, string[] Descricao)
        {
            try
            {
                List<TabelaBancoPortadorInstrucao> instrucoes = new List<TabelaBancoPortadorInstrucao>();
                for (int i = 0; i < Cod.Length; i++)
                {
                    instrucoes.Add(new TabelaBancoPortadorInstrucao
                    {
                        Cod = DbRotinas.ConverterParaInt(Cod[i]),
                        CodBancoPortador = DbRotinas.ConverterParaInt(CodBancoPortador[i]),
                        CodigoInstrucao = CodigoInstrucao[i] ?? null,
                        Numero = Numero[i] ?? null,
                        Descricao = Descricao[i]
                    });
                }
                foreach (var _instrucao in instrucoes)
                {
                    var _entity = _dbBoletoInstrucao.FindById(_instrucao.Cod);
                    if (_entity == null)
                    {
                        _entity = Activator.CreateInstance<TabelaBancoPortadorInstrucao>();
                        _entity.DataCadastro = DateTime.Now;
                    }
                    _entity.CodBancoPortador = DbRotinas.ConverterParaInt(_instrucao.CodBancoPortador);
                    _entity.CodigoInstrucao = _instrucao.CodigoInstrucao;
                    _entity.Numero = _instrucao.Numero;
                    _entity.Descricao = _instrucao.Descricao;
                    _dbBoletoInstrucao.CreateOrUpdate(_entity);
                }

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ExportarExcel(DTParameters param, string contratoId = null, int CodSinteseContrato = 0, int CodSinteseParcela = 0, int tipo = 0)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                var userLogado = WebRotinas.ObterUsuarioLogado();
                var CodUsuarioLogado = userLogado.Cod;
                Helper _rotina = new Helper();
                List<ConverterListaParaExcel.CollumnExcel> columns = new List<ConverterListaParaExcel.CollumnExcel>();
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Companhia", Text = Idioma.Traduzir("Companhia") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CompanhiaFiscal", Text = Idioma.Traduzir("Comp.Fiscal") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Portador", Text = Idioma.Traduzir("Cedente") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "CNPJ", Text = Idioma.Traduzir("CNPJ") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Carteira", Text = Idioma.Traduzir("Carteira") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Agencia", Text = Idioma.Traduzir("Agencia") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "Conta", Text = Idioma.Traduzir("Conta") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "NossoNumeroAtual", Text = Idioma.Traduzir("Nosso Numero") });
                columns.Add(new ConverterListaParaExcel.CollumnExcel() { Key = "LoteAtual", Text = Idioma.Traduzir("Lote Atual") });

                var lista = ListaDados();
                var result = WebRotinas.DTRotinas.DTFilterResult(lista, param);

                var nomeArquivo = Idioma.Traduzir("ConfiguracaoBoleto");
                op = _rotina.GerarArquivo(nomeArquivo, result, columns);
            }
            catch (Exception ex)
            {

                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }

        [HttpGet]
        public JsonResult PesquisarPortadores(string q)
        {
            q = q.ToLower();

            var lista = _dbParametroBoleto.All().Where(x => x.Companhia.Contains(q) || x.CompanhiaFiscal.Contains(q)).Take(30).ToList();

            return Json(new
            {
                items = from p in lista
                        select new
                        {
                            id = p.Cod,
                            text = $"{(EnumBoleto.BancoCodigo)p.BancoNumero} - {p.Companhia} - {p.CompanhiaFiscal} - {DbRotinas.Descriptografar(p.TabelaRegionalGrupo.CodRegionalCliente)}"
                        }
            }, JsonRequestBehavior.AllowGet);
        }


    }
}
