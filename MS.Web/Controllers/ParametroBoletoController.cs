﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class ParametroBoletoController : BasicController<TabelaParametroBoleto>
    {

        public Repository<TabelaBanco> _dbBanco = new Repository<TabelaBanco>();
        public Repository<TabelaBancoPortadorGrupo> _dbGrupo = new Repository<TabelaBancoPortadorGrupo>();

        public override void carregarCustomViewBags()
        {
            var listaBancos = _dbBanco.All();
            ViewBag.Bancos = from p in listaBancos.OrderBy(x => x.NomeBanco).ToList() select new DictionaryEntry(p.NomeBanco, p.Cod);

        }

        [HttpGet]
        public ActionResult DadosGrupo(int cod = 0)
        {
            var dados = new Cobranca.Db.Models.TabelaBancoPortadorGrupo();
            if (cod > 0) dados = _dbGrupo.FindById(cod);

            return PartialView("_formGrupo", dados);
        }

    }
}