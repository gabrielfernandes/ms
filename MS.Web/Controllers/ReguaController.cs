﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using Cobranca.Db.Suporte;
using Cobranca.Rotinas;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using Recebiveis.Web.Models;
using Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Recebiveis.Web.Controllers
{
    [Authorize]
    public class ReguaController : Controller
    {

        Repository<TabelaTipoCobranca> _dbTipoCobranca = new Repository<TabelaTipoCobranca>();
        Repository<TabelaTipoCobrancaPosicao> _dbTabelaTipoCobrancaPosicao = new Repository<TabelaTipoCobrancaPosicao>();
        Repository<TabelaAcaoCobranca> _dbAcao = new Repository<TabelaAcaoCobranca>();
        Repository<TabelaFase> _dbFase = new Repository<TabelaFase>();
        Repository<TabelaSintese> _dbSintese = new Repository<TabelaSintese>();
        Repository<TabelaTipoCobrancaAcao> _dbTipoCobrancaAcao = new Repository<TabelaTipoCobrancaAcao>();
        Repository<TabelaTipoCobrancaCondicao> _dbTabelaTipoCobrancaCondicao = new Repository<TabelaTipoCobrancaCondicao>();
        Repository<TabelaProduto> _dbProduto = new Repository<TabelaProduto>();
        Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();
        Repository<TabelaTipoBloqueio> _dbTipoBloqueio = new Repository<TabelaTipoBloqueio>();
        Repository<TabelaAging> _dbAging = new Repository<TabelaAging>();
        Repository<TabelaTipoCobrancaAcao> _dbModeloAcaoCobranca = new Repository<TabelaTipoCobrancaAcao>();
        Repository<LogEmail> _dbLogEmail = new Repository<LogEmail>();
        Repository<ParametroPlataforma> _dbParametroPlataforma = new Repository<ParametroPlataforma>();
        Repository<TabelaTipoCobrancaLogProcessamento> _dbTabelaTipoCobrancaLogProcessamento = new Repository<TabelaTipoCobrancaLogProcessamento>();
        Repository<TabelaGrupo> _dbTabelaGrupo = new Repository<TabelaGrupo>();
        Repository<TabelaFase> _dbTabelaFase = new Repository<TabelaFase>();
        Repository<TabelaSintese> _dbTabelaSintese = new Repository<TabelaSintese>();
        Repository<Cliente> _dbCliente = new Repository<Cliente>();
        Repository<Contrato> _dbContrato = new Repository<Contrato>();

        FilialRepositorio _repoFilial = new FilialRepositorio();

        ListasRepositorio _dbListas = new ListasRepositorio();

        private const string _reguaSoberana = "_reguaSoberana";

        public ActionResult ReguaSoberana()
        {
            WebRotinas.CookieObjetoGravar(_reguaSoberana, true);

            return RedirectToAction("Index");
        }

        public ActionResult ReguaFilial()
        {
            WebRotinas.CookieObjetoGravar(_reguaSoberana, false);

            return RedirectToAction("Index");
        }

        public ActionResult Index(int codFilialSelecionada = 0)
        {
            ViewBag.fgReguaSoberana = WebRotinas.CookieObjetoLer<bool>(_reguaSoberana);

            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();


            if (!(WebRotinas.ObterUsuarioLogado().PerfilTipo == Cobranca.Db.Models.Enumeradores.PerfilTipo.SupervisorAnalista || WebRotinas.ObterUsuarioLogado().PerfilTipo == Cobranca.Db.Models.Enumeradores.PerfilTipo.Analista || WebRotinas.ObterUsuarioLogado().PerfilTipo == Cobranca.Db.Models.Enumeradores.PerfilTipo.RegionalAdmin || WebRotinas.ObterUsuarioLogado().PerfilTipo == Cobranca.Db.Models.Enumeradores.PerfilTipo.Admin))
            {
                return RedirectToAction("index", "home", new { msg = "access_denied" });
            }

            List<TabelaTipoCobranca> tipos =
                _dbTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();

            ViewBag.ListaAcoes = _dbAcao.All().ToList();

            var lista = _repoFilial.ListagemPorUsuario(null, WebRotinas.ObterUsuarioLogado().Cod);
            lista.Insert(0, new TabelaRegional() { Regional = "", Cod = 0 });


            List<DictionaryEntry> filialRegua = new List<DictionaryEntry>();

            filialRegua.Add(new DictionaryEntry() { Key = Enumeradores.ReguaFilialTipo.SP, Value = (int)Enumeradores.ReguaFilialTipo.SP });
            filialRegua.Add(new DictionaryEntry() { Key = Enumeradores.ReguaFilialTipo.CTR, Value = (int)Enumeradores.ReguaFilialTipo.CTR });
            filialRegua.Add(new DictionaryEntry() { Key = Enumeradores.ReguaFilialTipo.SUL, Value = (int)Enumeradores.ReguaFilialTipo.SUL });
            filialRegua.Add(new DictionaryEntry() { Key = Enumeradores.ReguaFilialTipo.NRT, Value = (int)Enumeradores.ReguaFilialTipo.NRT });

            ViewBag.ListaFilial = filialRegua;

            ViewBag.CodFilialSelecionada = codFilialSelecionada;

            ViewBag.NomeFilial = string.Empty;

            if (codFilialSelecionada > 0)
            {
                ViewBag.NomeFilial = EnumeradoresDescricao.ReguaFilialTipo(codFilialSelecionada);
            }

            return View(tipos);
        }
        [HttpGet]
        public ActionResult FormAcao(int id = 0, bool fgReguaSoberana = false, int codFilialSelecionada = 0)
        {
            ViewBag.fgReguaSoberana = fgReguaSoberana;
            ViewBag.CodFilialSelecionada = codFilialSelecionada;

            var dados = _dbTipoCobrancaAcao.FindById(id);

            if (string.IsNullOrEmpty(dados.Titulo))
            {
                dados.Titulo = dados.TabelaAcaoCobranca.AcaoCobranca;
            }
            if (string.IsNullOrEmpty(dados.Modelo))
            {
                if (dados.TabelaAcaoCobranca.TabelaModeloAcaos.Where(x => !x.DataExcluido.HasValue).LastOrDefault() != null)
                {
                    dados.Modelo = dados.TabelaAcaoCobranca.TabelaModeloAcaos.Where(x => !x.DataExcluido.HasValue).LastOrDefault().Modelo;
                }
            }
            ViewBag.ListaTipo = EnumeradoresLista.ListaCondicaoTiposAcao();
            ViewBag.ListaTipoOperadorLogico = EnumeradoresLista.ListaCondicaoOperadorLogico();
            ViewBag.ListaCondicao = EnumeradoresLista.ListaCondicao();
            ViewBag.ListaAcaoTipo = EnumeradoresLista.ListaAcaoTipo();
            ViewBag.ListaHistoricoTipo = EnumeradoresLista.HistoricoTipo();

            var fases = _dbFase.All().Where(x => x.CodTipoCobranca == dados.CodTipoCobranca && x.TipoHistorico == (dados.AcaoHistoricoTipo ?? (short)Enumeradores.HistoricoTipo.Parcela)).ToList();
            var sinteses = _dbSintese.All().Where(x => x.CodFase == dados.CodFase).ToList();

            ViewBag.ListaFase =
                (from p in fases
                 select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.Fase), Value = p.Cod }).ToList();

            ((List<DictionaryEntry>)ViewBag.ListaFase).Insert(0, new DictionaryEntry() { Key = "--", Value = "0" });

            ViewBag.ListaSintese =
                (from p in sinteses
                 select new DictionaryEntry() { Key = DbRotinas.Descriptografar(p.Sintese), Value = p.Cod }).ToList();


            return View(dados);
        }
        [HttpGet]
        public ActionResult TipoCobrancaAcoes(int id = 0)
        {
            var dados = _dbTipoCobranca.FindById(id);
            return View(dados);
        }
        public ActionResult Processar()
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();

            List<TabelaTipoCobranca> tipos = _dbTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
            return View(tipos);
        }
        public ActionResult Filtro()
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();

            var lista = _dbTabelaTipoCobrancaCondicao.All().Where(x => x.Excecao == false).ToList();
            TratarListaCondicoes(ref lista);
            ViewBag.ListaTipoCobranca = _dbListas.TipoCobranca();
            ViewBag.ListaTipo = EnumeradoresLista.ListaCondicaoTipos();
            ViewBag.ListaCondicao = EnumeradoresLista.ListaCondicao();
            return View(lista);
        }
        public ActionResult Excecao()
        {
            if (!WebRotinas.UsuarioAutenticado)
                WebRotinas.EncerrarSessao();

            var lista = _dbTabelaTipoCobrancaCondicao.All().Where(x => x.Excecao == true).ToList();
            TratarListaCondicoes(ref lista);
            ViewBag.ListaTipoCobranca = _dbListas.TipoCobranca();
            ViewBag.ListaTipo = EnumeradoresLista.ListaCondicaoTipos();
            ViewBag.ListaCondicao = EnumeradoresLista.ListaCondicao();
            return View(lista);
        }

        private void TratarListaCondicoes(ref List<TabelaTipoCobrancaCondicao> lista)
        {
            foreach (var item in lista)
            {
                switch ((Enumeradores.CondicaoTipo)item.Tipo)
                {
                    case Enumeradores.CondicaoTipo.ValorSaldo:
                    case Enumeradores.CondicaoTipo.ValorParcela:
                    case Enumeradores.CondicaoTipo.ValorContrato:
                        {
                            item.ValorDescricao = string.Format("{0:C}", DbRotinas.ConverterParaFloat(item.Valor));
                            break;
                        }
                    case Enumeradores.CondicaoTipo.DiasAtraso:
                        {
                            item.ValorDescricao = item.Valor + " dia(s)";
                            break;
                        }
                    case Enumeradores.CondicaoTipo.TipoBloqueio:
                        {
                            item.ValorDescricao = _dbTipoBloqueio.FindById(DbRotinas.ConverterParaInt(item.Valor)).TipoBloqueio;
                            break;
                        }
                    case Enumeradores.CondicaoTipo.Aging:
                        {
                            item.ValorDescricao = _dbAging.FindById(DbRotinas.ConverterParaInt(item.Valor)).Aging;
                            break;
                        }
                    case Enumeradores.CondicaoTipo.StatusFaseSintese:
                        {
                            var sintese = _dbSintese.FindById(DbRotinas.ConverterParaInt(item.Valor));
                            if (sintese != null)
                            {
                                item.ValorDescricao = string.Format("{0} - {1}", sintese.TabelaFase.Fase, sintese.Sintese);
                            }
                            else
                            {
                                item.ValorDescricao = item.Valor;
                            }
                            break;
                        }
                    case Enumeradores.CondicaoTipo.Empreendimento:
                        {
                            var empre = _dbEmpreendimento.FindById(DbRotinas.ConverterParaInt(item.Valor));
                            if (empre != null)
                                item.ValorDescricao = empre.NomeEmpreendimento;
                            break;
                        }
                    default:
                        {
                            item.ValorDescricao = item.Valor;
                            break;
                        }
                }

            }
        }

        [HttpPost]
        public ActionResult Filtro(TabelaTipoCobrancaCondicao dados)
        {

            if (ModelState.IsValid)
            {
                dados.DataCadastro = DateTime.Now;
                _dbTabelaTipoCobrancaCondicao.CreateOrUpdate(dados);

                ViewBag.MensagemOK = Language.MensagemOK;
                ViewBag.RedirecionarUrl = Url.Action("Filtro", "Regua");
            }

            var lista = _dbTabelaTipoCobrancaCondicao.All().Where(x => x.Excecao == false).ToList();
            TratarListaCondicoes(ref lista);
            ViewBag.ListaTipoCobranca = _dbListas.TipoCobranca();
            ViewBag.ListaTipo = EnumeradoresLista.ListaCondicaoTipos();
            ViewBag.ListaCondicao = EnumeradoresLista.ListaCondicao();
            return View(lista);
        }

        [HttpPost]
        public ActionResult Excecao(TabelaTipoCobrancaCondicao dados)
        {

            if (ModelState.IsValid)
            {
                dados.DataCadastro = DateTime.Now;
                _dbTabelaTipoCobrancaCondicao.CreateOrUpdate(dados);

                ViewBag.MensagemOK = Language.MensagemOK;
                ViewBag.RedirecionarUrl = Url.Action("Excecao", "Regua");
            }
            var lista = _dbTabelaTipoCobrancaCondicao.All().Where(x => x.Excecao == true).ToList();
            TratarListaCondicoes(ref lista);
            ViewBag.ListaTipoCobranca = _dbListas.TipoCobranca();
            ViewBag.ListaTipo = EnumeradoresLista.ListaCondicaoTipos();
            ViewBag.ListaCondicao = EnumeradoresLista.ListaCondicao();
            return View(lista);
        }

        [HttpPost]
        public JsonResult ReguaSalvar(List<ReguaCadastroViewModel> dados)
        {
            try
            {
                return Json(new { OK = true });

                //_dbTipoCobrancaAcao.Delete(x => !x.DataExcluido.HasValue);

                //foreach (var tipo in dados)
                //{
                //    foreach (var acao in tipo.CodAcoes)
                //    {
                //        var i = new TabelaTipoCobrancaAcao()
                //        {
                //            CodAcaoCobranca = acao,
                //            CodTipoCobranca = tipo.CodTipoCobranca,
                //            DataCadastro = DateTime.Now
                //        };
                //        _dbTipoCobrancaAcao.CreateOrUpdate(i);
                //        //if (entity.TabelaAcaoCobranca.TabelaModeloAcaos.Where(x => !x.DataExcluido.HasValue).LastOrDefault() != null)
                //        //{
                //        //    entity.Modelo = entity.TabelaAcaoCobranca.TabelaModeloAcaos.Where(x => !x.DataExcluido.HasValue).LastOrDefault().Modelo;
                //        //}
                //    }
                //}



                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }


        }


        [HttpPost]
        public JsonResult ReguaClacular(int cod = 0, int processamentoId = 0)
        {
            TabelaTipoCobrancaLogProcessamento processo = new TabelaTipoCobrancaLogProcessamento();
            int totalCasos = 0;
            try
            {
                Dados d = new Dados();

                if (processamentoId == 0)
                {
                    processo.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                    processo.Guid = Guid.NewGuid().ToString();
                    processo = _dbTabelaTipoCobrancaLogProcessamento.CreateOrUpdate(processo);
                }
                else
                {
                    processo = _dbTabelaTipoCobrancaLogProcessamento.FindById(processamentoId);
                }

                totalCasos = d.executarCondicosTipoCobranca(cod, processo.Cod);
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

            return Json(new { OK = true, CodProcesso = processo.Cod, total = totalCasos });
        }

        [HttpPost]
        public JsonResult ListaTipoBloqueio()
        {
            var lista = _dbListas.TipoBloqueio();

            var selectList = from p in lista select new { Cod = p.Value, Text = p.Key };

            return Json(selectList);
        }
        [HttpPost]
        public JsonResult ListaAging()
        {
            var lista = _dbListas.Aging();

            var selectList = from p in lista select new { Cod = p.Value, Text = p.Key };

            return Json(selectList);
        }
        [HttpPost]
        public JsonResult ListaEmpreendimento()
        {
            var lista = _dbListas.Empreendimentos();

            var selectList = from p in lista select new { Cod = p.Value, Text = p.Key };

            return Json(selectList);
        }
        [HttpPost]
        public JsonResult ListaFaseSintese()
        {
            var lista = _dbListas.FaseSintese();

            var selectList = from p in lista select new { Cod = p.Value, Text = p.Key };

            return Json(selectList);
        }
        [HttpPost]
        public JsonResult ExcluirCondicao(int cod = 0)
        {
            try
            {
                _dbTabelaTipoCobrancaCondicao.Delete(cod);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {

                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
        [HttpPost, ValidateInput(false)]
        public JsonResult SalvarTipoCobrancaAcao(TabelaTipoCobrancaAcao dados)
        {
            try
            {
                var baseEntity = _dbTipoCobrancaAcao.FindById(dados.Cod);
                baseEntity.Titulo = dados.Titulo;
                baseEntity.PeriodoEmDias = dados.PeriodoEmDias;
                baseEntity.Modelo = dados.Modelo;
                baseEntity.CondicaoTipo = dados.CondicaoTipo;
                baseEntity.CampoTipo = dados.CampoTipo;
                baseEntity.CondicaoQuery = dados.CondicaoQuery;
                baseEntity.AcaoTipo = dados.AcaoTipo.HasValue ? dados.AcaoTipo : (short)Enumeradores.AcaoTipo.Cliente;

                baseEntity.AcaoHistoricoTipo = dados.AcaoHistoricoTipo;
                baseEntity.CodFase = dados.CodFase;
                baseEntity.CodSintese = dados.CodSintese;
                baseEntity.AcaoHistoricoObservacao = DbRotinas.Criptografar(dados.AcaoHistoricoObservacao);

                baseEntity.AgruparProcessosNegativacao = dados.AgruparProcessosNegativacao;
                baseEntity.DiasAtrasoDeNegativacao = dados.DiasAtrasoDeNegativacao;
                baseEntity.DiasAtrasoAteNegativacao = dados.DiasAtrasoAteNegativacao;

                _dbTipoCobrancaAcao.CreateOrUpdate(baseEntity);

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }


        }

        [HttpPost]
        public JsonResult VincularAcaoTipoCobranca(int tipoCobrancaId, int acaoId, int? ordem)
        {
            try
            {
                if (!ordem.HasValue)
                    ordem = 1;


                var tipo = _dbAcao.FindById(acaoId);

                var tpModelo = tipo.TabelaModeloAcaos.FirstOrDefault();

                string modelo = "",titulo = "";

                if (tpModelo != null)
                {
                    titulo = tipo.AcaoCobranca;
                    modelo = tpModelo.Modelo;
                }

                _dbTipoCobrancaAcao.CreateOrUpdate(new TabelaTipoCobrancaAcao()
                {
                    CodTipoCobranca = tipoCobrancaId,
                    CodAcaoCobranca = acaoId,
                    PeriodoEmDias = ordem,
                    Modelo = modelo,
                    Titulo = titulo
                });
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        //
        [HttpPost]
        public JsonResult ExcluirAcao(int id)
        {
            try
            {
                _dbTipoCobrancaAcao.Delete(id);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult SalvarQueryTipoCobranca(int Cod, string CondicaoQuery)
        {
            try
            {
                var entity = _dbTipoCobranca.FindById(Cod);
                entity.CondicaoQuery = CondicaoQuery;
                _dbTipoCobranca.CreateOrUpdate(entity);

                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult IncluirNovaPosicao(int? id, int? posicao, int codFilialSelecionada = 0)
        {
            var existe = _dbTabelaTipoCobrancaPosicao.All().Any(x => x.CodTipoCobranca == id && x.NumeroDias == posicao && (x.CodFilial == codFilialSelecionada || !x.CodFilial.HasValue));
            if (!existe)
            {
                TabelaTipoCobrancaPosicao p = new TabelaTipoCobrancaPosicao();
                p.NumeroDias = posicao;
                p.CodTipoCobranca = id;
                if (codFilialSelecionada > 0)
                    p.CodFilial = codFilialSelecionada;

                _dbTabelaTipoCobrancaPosicao.CreateOrUpdate(p);
            }
            return Json(new { OK = true });
        }
        [HttpPost]
        public JsonResult ExcluirPosicao(int id)
        {
            _dbTabelaTipoCobrancaPosicao.Delete(id);
            return Json(new { OK = true });
        }
        [HttpPost]
        public JsonResult CadastrarEnvioEmailTeste(int Cod, string EmailDestinatario)
        {
            try
            {
                var usuairoLogado = WebRotinas.ObterUsuarioLogado();
                var dadosAcoes = _dbModeloAcaoCobranca.FindById(Cod);
                var parametro = _dbParametroPlataforma.All().FirstOrDefault();

                if (string.IsNullOrEmpty(parametro.EmailRemetente))
                {
                    parametro.EmailRemetente = usuairoLogado.Email;
                }

                if (string.IsNullOrEmpty(parametro.NomeRemetente))
                {
                    parametro.NomeRemetente = parametro.EmailRemetente;
                }

                if (string.IsNullOrEmpty(dadosAcoes.Titulo))
                {
                    dadosAcoes.Titulo = "Teste Açao de Cobrança";
                }

                LogEmail log = new LogEmail();
                log.EmailRemetente = parametro.EmailRemetente;
                log.EmailDestinatario = EmailDestinatario;
                log.NomeRemetente = parametro.NomeRemetente;

                dadosAcoes.Modelo = TratarConteudo(dadosAcoes.Modelo);

                log.Conteudo = dadosAcoes.Modelo;
                log.TentativaEnvio = 0;
                log.Status = (short)Enumeradores.LogEmailStatus.AguardandoEnvio;
                log.Tipo = (short)Enumeradores.TipoAcaoCobranca.EMAIL;
                log.HTML = true;
                log.NomeDestinatario = usuairoLogado.Nome;
                log.EmailCopia = usuairoLogado.Email;
                log.Assunto = dadosAcoes.Titulo;
                if (string.IsNullOrEmpty(dadosAcoes.Modelo) && dadosAcoes.CodAcaoCobranca.HasValue)
                    log.Conteudo = dadosAcoes.TabelaAcaoCobranca.TabelaModeloAcaos.FirstOrDefault().Modelo;
                else
                    log.Conteudo = dadosAcoes.Modelo;
                _dbLogEmail.CreateOrUpdate(log);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }

        private string TratarConteudo(string modelo)
        {



            Contrato objContrato = _dbContrato.FindAll(x => x.ValorAtraso > 0).FirstOrDefault();
            Cliente objCliente = _dbCliente.FindById(objContrato.CodClientePrincipal ?? 0);
            if (objCliente == null)
            {
                objCliente = new Cliente();
            }
            StringBuilder sb = new StringBuilder();
            sb.Append(modelo);
            string tbParcelas = MontarTabelaHtml(objContrato.ContratoParcelas.Where(x => x.DataVencimento < DateTime.Now));

            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.TabelaParcelas), tbParcelas);
            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NomeComprador), DbRotinas.Descriptografar(objCliente.Nome));
            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.CPF_CPNPJ_Comprador), DbRotinas.Descriptografar(objCliente.CPF));
            //sb = sb.Replace("@CPF_CNPJ_COMPRADOR", DbRotinas.Descriptografar(objCliente.CNPJ));

            //DADOS DA REGIONAL PRODUTO;
            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.DescricaoFilial), "REGIONAL");
            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NumeroFilial), "TELEFONE");
            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.ResponsavelFilial), "RESP. FILIAL");
            return sb.ToString();
        }
        private string MontarTabelaHtml(IEnumerable<ContratoParcela> acoes)
        {
            StringBuilder sbTabelaParcelas = new StringBuilder();

            sbTabelaParcelas.AppendFormat($"<table border='1'>");
            sbTabelaParcelas.AppendFormat($"<tr stype='border-bottom:solid 1px #808080;'>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Contrato</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Fatura</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Nº Parcela</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Vencimento</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Valor</th>");
            sbTabelaParcelas.AppendFormat($"</tr>");



            foreach (var parcela in acoes)
            {


                //emailRegional = parcela.Contrato.TabelaRegionalProduto.TabelaRegional.Email;

                sbTabelaParcelas.AppendFormat($"<tr style='border-bottom:solid 1px #808080'>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{DbRotinas.Descriptografar(parcela.Contrato.NumeroContrato)}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.FluxoPagamento}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.NumeroParcela}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.DataVencimento:dd/MM/yyyy}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.ValorAberto:C2}</td>");
                sbTabelaParcelas.AppendFormat($"</tr>");

            }
            sbTabelaParcelas.AppendFormat($"</table>");

            return sbTabelaParcelas.ToString();
        }
        [HttpGet]
        public JsonResult CarregarFase()
        {
            List<DictionaryEntry> dados = new List<DictionaryEntry>();
            if (WebRotinas.UsuarioAutenticado)
            {
                dados.Insert(0, new DictionaryEntry());
                foreach (var item in _dbSintese.All().Where(
                            x => !x.TabelaFase.DataExcluido.HasValue
                               && !x.TabelaFase.TabelaTipoCobranca.DataExcluido.HasValue
                               && !x.DataExcluido.HasValue
                               && x.TabelaFase.TabelaTipoCobranca.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca
                            ).OrderBy(x => x.TabelaFase.TabelaTipoCobranca.TipoCobranca).ThenBy(x => x.TabelaFase.Fase).ThenBy(x => x.Sintese).ToList())
                {
                    dados.Add(new DictionaryEntry()
                    {
                        Key = item.Cod,
                        Value = item.TabelaFase.TabelaTipoCobranca.TipoCobranca + " - " + item.TabelaFase.Fase + " - " + item.Sintese
                    });
                }
            }
            return Json(dados.ToList(), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult CarregarProduto()
        {
            List<DictionaryEntry> dados = new List<DictionaryEntry>();
            if (WebRotinas.UsuarioAutenticado)
            {
                dados = (from p in _dbProduto.All().ToList()
                         select new DictionaryEntry() { Key = p.Cod, Value = p.Descricao }).ToList();
                dados.Insert(0, new DictionaryEntry());
            }
            return Json(dados, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ListarFasesPorTipoHistorico(int CodTipoCobranca, short HistoricoTipo)
        {
            OperacaoRetorno op = new OperacaoRetorno();

            try
            {
                //List<DictionaryEntry> dados = new List<DictionaryEntry>();

                List<DictionaryEntry> dados = ListarFases(CodTipoCobranca, HistoricoTipo);

                op.Dados = dados;
                op.OK = true;
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }

        private List<DictionaryEntry> ListarFases(int codTipoCobranca, short historicoTipo)
        {
            List<DictionaryEntry> dados = new List<DictionaryEntry>();

            var fases = _dbFase.All().Where(x => x.CodTipoCobranca == codTipoCobranca && x.TipoHistorico == historicoTipo).ToList();
            dados = (from p in fases
                     select new DictionaryEntry() { Key = p.Cod, Value = DbRotinas.Descriptografar(p.Fase) }).ToList();

            return dados;

        }

        [HttpPost]
        public JsonResult ListarListarSinteseProFase(int CodFase = 0)
        {
            OperacaoRetorno op = new OperacaoRetorno();

            try
            {
                List<DictionaryEntry> dados = new List<DictionaryEntry>();

                var fases = _dbSintese.All().Where(x => x.CodFase == CodFase).ToList();
                dados = (from p in fases
                         select new DictionaryEntry() { Key = p.Cod, Value = DbRotinas.Descriptografar(p.Sintese) }).ToList();

                op.Dados = dados;
                op.OK = true;

            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }


            return Json(op);
        }

        [HttpGet, ValidateInput(false)]
        public JsonResult ObterFiltrosQueryBuilder()
        {
            OperacaoRetorno op = new OperacaoRetorno();


            try
            {
                var all = _dbTabelaGrupo.All();


                FaseSinteseDb _objFaseDb = new FaseSinteseDb();
                var classificacoes = _objFaseDb.ListarHistorico(Enumeradores.TipoHistorico.Parcela).OrderBy(x=> x.CodigoSinteseCliente);

                var itensClassificacao = new List<string>();

                foreach (var p in classificacoes)
                {
                    itensClassificacao.Add(string.Format(@"{{""{0}"":""{3} - {2} - {1}""}}", p.Cod, p.Sintese.ToUpper(), p.TabelaFase.Fase.ToUpper(), p.CodigoSinteseCliente != null ? p.CodigoSinteseCliente.ToUpper() : ""));
                }

                var itens = new List<string>();

                foreach (var p in all.ToList())
                {
                    itens.Add(string.Format(@"{{""{0}"":""{1}""}}", p.Cod, p.Nome));
                }


                var corporativoAll = _dbCliente.All().Where(x => !String.IsNullOrEmpty(x.Corporativo)).Select(a => a.Corporativo).Distinct().ToList();
                var itensCorporativo = new List<string>();
                foreach (var p in corporativoAll.ToList())
                {
                    itensCorporativo.Add(string.Format(@"{{""{0}"":""{1}""}}", p, p));
                }


                op.Dados = new
                {
                    listaClassificacaoParcela = itensClassificacao,
                    listaGruposEmpresa = itens,
                    listaCorporativo = itensCorporativo
                };

                op.OK = true;

            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }

            return Json(op, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AlterarDiaAcao(int CodAcaoCobranca, int Cod, int Dias, int? CodFilialSelecionada)
        {
            OperacaoRetorno op = new OperacaoRetorno();
            try
            {
                var objTipoCobrancaAcao = _dbTipoCobrancaAcao.FindById(Cod);


                int maxPeriodoEmDias = _dbTipoCobrancaAcao.FindAll(x => x.CodAcaoCobranca == CodAcaoCobranca && !x.CodFilial.HasValue).Max(x => x.PeriodoEmDias ?? 0);

                //if (_dbTipoCobrancaAcao.FindAll(x => x.PeriodoEmDias == Dias && x.CodAcaoCobranca == CodAcaoCobranca).Any())
                //{
                //    op.OK = false;
                //    op.Mensagem = Idioma.Traduzir("Já existe uma ação.");
                //    return Json(op);
                //}

                if (CodFilialSelecionada > 0)
                {
                    if (Dias > maxPeriodoEmDias)
                    {
                        op.OK = false;
                        op.Mensagem = Idioma.Traduzir("É possivel apenas antecipar as ações");
                        return Json(op);
                    }

                    if (!objTipoCobrancaAcao.CodFilial.HasValue)
                    {
                        TabelaTipoCobrancaAcao acao = new TabelaTipoCobrancaAcao()
                        {
                            CodFilial = CodFilialSelecionada,
                            AcaoHistoricoObservacao = objTipoCobrancaAcao.AcaoHistoricoObservacao,
                            AcaoHistoricoTipo = objTipoCobrancaAcao.AcaoHistoricoTipo,
                            AcaoTipo = objTipoCobrancaAcao.AcaoTipo,
                            AgruparProcessosNegativacao = objTipoCobrancaAcao.AgruparProcessosNegativacao,
                            CampoTipo = objTipoCobrancaAcao.CampoTipo,
                            CodAcaoCobranca = objTipoCobrancaAcao.CodAcaoCobranca,
                            CodFase = objTipoCobrancaAcao.CodFase,
                            CodModelo = objTipoCobrancaAcao.CodModelo,
                            CodSintese = objTipoCobrancaAcao.CodSintese,
                            CodTipoCobranca = objTipoCobrancaAcao.CodTipoCobranca,
                            CondicaoQuery = objTipoCobrancaAcao.CondicaoQuery,
                            CondicaoTipo = objTipoCobrancaAcao.CondicaoTipo,
                            DiasAtrasoAteNegativacao = objTipoCobrancaAcao.DiasAtrasoAteNegativacao,
                            DiasAtrasoDeNegativacao = objTipoCobrancaAcao.DiasAtrasoDeNegativacao,
                            Modelo = objTipoCobrancaAcao.Modelo,
                            PeriodoEmDias = Dias,
                            Titulo = objTipoCobrancaAcao.Titulo,
                            CodTipoCobrancaAcaoSoberana = objTipoCobrancaAcao.Cod
                        };
                        _dbTipoCobrancaAcao.CreateOrUpdate(acao);

                    }
                    else
                    {
                        objTipoCobrancaAcao.PeriodoEmDias = Dias;
                        _dbTipoCobrancaAcao.CreateOrUpdate(objTipoCobrancaAcao);
                    }
                }
                else
                {
                    if (WebRotinas.ObterUsuarioLogado().PerfilTipo == Enumeradores.PerfilTipo.Admin)
                    {
                        objTipoCobrancaAcao.PeriodoEmDias = Dias;
                        _dbTipoCobrancaAcao.CreateOrUpdate(objTipoCobrancaAcao);
                    }
                }


                op.OK = true;
                op.Mensagem = Idioma.Traduzir("Operação realizada com Sucesso");
            }
            catch (Exception ex)
            {
                op = new OperacaoRetorno(ex);
            }
            return Json(op);
        }
    }
}
