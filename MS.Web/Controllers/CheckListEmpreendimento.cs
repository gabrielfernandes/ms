﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Cobranca.Web.Controllers
{

  
    public class CheckListEmpreendimentoController : BasicController<TabelaEmpreendimento>
    {
        public Repository<TabelaEmpreendimento> _dbTabelaEmpreendimento = new Repository<TabelaEmpreendimento>();
        public Repository<TabelaCheckListNome> _dbCheckList = new Repository<TabelaCheckListNome>();
        public Repository<TabelaEndereco> _dbEndereco = new Repository<TabelaEndereco>();
        public override ActionResult Cadastro(TabelaEmpreendimento dados)
        {

            var baseEntity = _repository.FindById(dados.Cod);
            baseEntity.CodCheckListNome = dados.CodCheckListNome;

            return base.Cadastro(baseEntity);
        }

        public override void carregarCustomViewBags()
        {
            var listaEmpreendimento = _dbTabelaEmpreendimento.All();
            ViewBag.ListaEmpreendimento = from p in listaEmpreendimento.OrderBy(x => x.NomeEmpreendimento).ToList() select new DictionaryEntry(p.NomeEmpreendimento, p.Cod);
            var listaCheckList = _dbCheckList.All();
            ViewBag.ListaCheckList = from p in listaCheckList.OrderBy(x => x.CheckListNome).ToList() select new DictionaryEntry(p.CheckListNome, p.Cod);
            ViewBag.ListaCheckListEmpreendimento = from p in listaCheckList.Where(x=>x.TipoCheckList != (short)Enumeradores.CheckListTipo.Proposta).OrderBy(x => x.CheckListNome).ToList() select new DictionaryEntry(p.CheckListNome, p.Cod);

            base.carregarCustomViewBags();
        }
        public override void validacaoCustom(TabelaEmpreendimento entity)
        {
            if (entity.CodEndereco > 0)
            {
                _dbEndereco.CreateOrUpdate(entity.TabelaEndereco);
            }

            base.validacaoCustom(entity);
        }

        public JsonResult VincularCheckList(int CodEmpreendimento, int CodCheckList)
        {
            try
            {
                var dados = _dbTabelaEmpreendimento.FindById(CodEmpreendimento);
                dados.CodCheckListNome = CodCheckList;
                dados.DataAlteracao = DateTime.Now;
                _dbTabelaEmpreendimento.Update(dados);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }

        }
    }
  
}