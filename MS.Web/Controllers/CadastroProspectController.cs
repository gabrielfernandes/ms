﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Models;
using Cobranca.Web.Rotinas;
using System.Linq.Expressions;
using System.Collections;
using Cobranca.Db.Rotinas;

namespace Cobranca.Web.Controllers
{
    public class CadastroProspectController : Controller
    {
        public Repository<PropostaProspect> _dbPropostaProspect = new Repository<PropostaProspect>();
        public Repository<Proposta> _dbProposta = new Repository<Proposta>();
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult IncluirProspect(PropostaProspect dados)
        {
            try
            {
                Proposta proposta = new Proposta();
                proposta.DataCadastro = DateTime.Now;
                proposta.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                proposta.Status = (short)Enumeradores.StatusProposta.SemProposta;
             
                if (WebRotinas.ObterUsuarioLogado().CodEmpresaVenda > 0)
                {
                    proposta.CodEmpresaVenda = WebRotinas.ObterUsuarioLogado().CodEmpresaVenda;
                }
                _dbProposta.CreateOrUpdate(proposta);
                dados.CodProposta = proposta.Cod;
                dados.Ordem = 1;
                dados.TipoComprador = 1;
                _dbPropostaProspect.CreateOrUpdate(dados);
                return Json(new { OK = true });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult ListarClientePorCPF(string CPF)
        {
            try
            {
              var dados = _dbPropostaProspect.FindAll(x => x.CPF.Contains(CPF) && !x.DataExcluido.HasValue);
                var info = new
                {
                    NomeCliente = dados.ToList().LastOrDefault().Nome,
                    DataNascimento = string.Format("{0:dd/MM/yyyy}", dados.ToList().LastOrDefault().DataNascimento),
                    Sexo = dados.ToList().LastOrDefault().Sexo,
                    EstadoCivil = dados.ToList().LastOrDefault().EstadoCivil,
                    TipoDocumento = dados.ToList().LastOrDefault().TipoDocumento,
                    NumeroDocumento = dados.ToList().LastOrDefault().RG,
                    OrgaoExpedidor = dados.ToList().LastOrDefault().Orgao,
                    DataEmissao = string.Format("{0:dd/MM/yyyy}", dados.ToList().LastOrDefault().DataEmissao),
                    CategoriaProfissional = dados.ToList().LastOrDefault().CategoriaProfissional,
                    Profissao = dados.ToList().LastOrDefault().Profissao,
                    NumeroDependente = dados.ToList().LastOrDefault().NumeroDependente,
                    Nacionalidade = dados.ToList().LastOrDefault().Nacionalidade,
                    Naturalidade = dados.ToList().LastOrDefault().Naturalidade,
                    Escolaridade = dados.ToList().LastOrDefault().Escolaridade,
                    Cartorio = dados.ToList().LastOrDefault().Cartorio,
                    NomePai = dados.ToList().LastOrDefault().NomePai,
                    NomeMae = dados.ToList().LastOrDefault().NomeMae,
                    Email = dados.ToList().LastOrDefault().Email,
                    TelefoneResidencial = dados.ToList().LastOrDefault().TelefoneResidencial,
                    TelefoneComercial = dados.ToList().LastOrDefault().TelefoneComercial,
                    Celular = dados.ToList().LastOrDefault().Celular,
                    CelularComercial = dados.ToList().LastOrDefault().CelularComercial,
                    CEP = dados.ToList().LastOrDefault().CEP,
                    EnderecoTipo = dados.ToList().LastOrDefault().Tipo,
                    Endereco = dados.ToList().LastOrDefault().Endereco,
                    EnderecoNumero = dados.ToList().LastOrDefault().EnderecoNumero,
                    EnderecoComplemento = dados.ToList().LastOrDefault().EnderecoComplemento,
                    Bairro = dados.ToList().LastOrDefault().Bairro,
                    Cidade = dados.ToList().LastOrDefault().Cidade,
                    UF = dados.ToList().LastOrDefault().UF
                    //NumeroDocumento = dados.ToList().LastOrDefault().NumeroDocumento,
                };
              return Json(new { OK = true, DadosCliente = info });
            }
            catch (Exception ex)
            {
                return Json(new { OK = false, Mensagem = ex.Message });
            }
        }
    }
}