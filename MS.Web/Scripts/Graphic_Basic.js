﻿$(function () {
    $('#Recebimento').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Pagamento vs Atraso Mensal'
        },
        //subtitle: {
        //    text: 'Grafico Mensal'
        //},
        xAxis: {
            categories: [
                'Janeiro',
                'Fevereiro',
                'Março',
                'Abril',
                'Maio',
                'Junho',
                'Julho',
                'Setembro',
                'Outubo',
                'Novembro',
                'Dezembro'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Quantidade'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.0,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Em Atraso',
            data: [parseInt(20), parseInt(19), parseInt(15), parseInt(12), parseInt(10), parseInt(8), parseInt(7), parseInt(6), parseInt(4), parseInt(9), parseInt(12), parseInt(8) ]
        },{
            name: 'Pagamentos',
            data: [parseInt(18), parseInt(26), parseInt(20), parseInt(8), parseInt(7), parseInt(17), parseInt(9), parseInt(3), parseInt(5), parseInt(17), parseInt(3), parseInt(2)]
        }]
    });
});