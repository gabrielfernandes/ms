﻿$(function () {
    $('#GraficoTestePizza').highcharts({
        title: {
            text: 'Consolidado - Junix Informática'
        },
        xAxis: {
            categories: ['Sms', 'Email', 'Contrato Inconforme', 'Negociação', 'Consolidação']
        },
        labels: {
            items: [{
                html: 'Total Consolidado',
                style: {
                    left: '50px',
                    top: '18px',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            }]
        },
        series: [{
            type: 'column',
            name: 'Fazer Contato',
            data: [3, 2, 1, 3, 4]
        }, {
            type: 'column',
            name: 'Enviar',
            data: [2, 3, 5, 7, 6]
        }, {
            type: 'column',
            name: 'Pronto Para Envio',
            data: [4, 3, 3, 9, 0]
        }, {
            type: 'spline',
            name: 'Média',
            data: [3, 2.67, 3, 6.33, 3.33],
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[3],
                fillColor: 'white'
            }
        }, {
            type: 'pie',
            name: 'Total',
            data: [{
                name: 'Fazer Contato',
                y: 13,
                color: Highcharts.getOptions().colors[0] // Jane's color
            }, {
                name: 'Enviar',
                y: 23,
                color: Highcharts.getOptions().colors[1] // John's color
            }, {
                name: 'Pronto Para Envio',
                y: 19,
                color: Highcharts.getOptions().colors[2] // Joe's color
            }],
            center: [100, 80],
            size: 100,
            showInLegend: false,
            dataLabels: {
                enabled: false
            }
        }]
    });
});
