﻿(function ($) {
    $('body').on('click', '.widget-panel-sortable > .widget-panel .handle > div.jnxwidget-ctrls > a[data-rel="reload"]', function (ev) {
        ev.preventDefault();

        var $this = $(this).closest('.panel');
        $this.block({
            message: '',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#fff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff',
                width: '50%'
            },
            overlayCSS: { backgroundColor: '#FFF' }
        });
        $this.addClass('reloading');

        setTimeout(function () {
            $this.unblock();
            $this.removeClass('reloading');
        }, 900);

    }).on('click', '.widget-panel-sortable > .widget-panel .handle > div.jnxwidget-ctrls > a[data-rel="close"]', function (ev) {
        ev.preventDefault();

        var $this = $(this);
        var $panel = $this.closest('.widget-panel-sortable');

        $panel.fadeOut(500, function () {
            $panel.remove();
        });

    }).on('click', '.widget-panel-sortable > .widget-panel .handle > div.jnxwidget-ctrls > a[data-rel="fullscreen"]', function (ev) {
        ev.preventDefault();

        var $this = $(this).closest('.widget-panel-sortable')
        $this.toggleClass('fullscreen');

    }).on('click', '.widget-panel-sortable > .widget-panel .handle > div.jnxwidget-ctrls > a[data-rel="collapse"]', function (ev) {
        ev.preventDefault();
        var $this = $(this),
                $panel = $this.closest('.widget-panel-sortable'),
                $body = $panel.find('.panel, .table'),
                do_collapse = !$panel.hasClass('panel-collapse');

        if ($panel.is('[data-collapsed="1"]')) {
            $panel.attr('data-collapsed', 0);
            $body.hide();
            do_collapse = false;
        }

        if (do_collapse) {
            $body.slideUp('normal');
            $panel.addClass('panel-collapse');
        }
        else {
            $body.slideDown('normal');
            $panel.removeClass('panel-collapse');
        }
    });

    // removeable-list -- remove parent elements
    var $removalList = $(".removeable-list");
    $(".removeable-list .remove").each(function () {
        var $this = $(this);
        $this.click(function (event) {
            event.preventDefault();

            var $parent = $this.parent('li');
            $parent.slideUp(500, function () {
                $parent.delay(3000).remove();

                if ($removalList.find("li").length == 0) {
                    $removalList.html('<li class="text-danger"><p>All items has been deleted.</p></li>');
                }
            });
        });
    });

    var $filterBtn = $(".toggle-filter");
    var $filterBoxId = $filterBtn.attr('data-block-id');
    var $filterBox = $('#' + $filterBoxId);

    if ($filterBox.hasClass('visible-box')) {
        $filterBtn.parent('li').addClass('active');
    }

    $filterBtn.click(function (event) {
        event.preventDefault();

        if ($filterBox.hasClass('visible-box')) {
            $filterBtn.parent('li').removeClass('active');
            $filterBox.removeClass('visible-box').addClass('hidden-box').slideUp();
        } else {
            $filterBtn.parent('li').addClass('active');
            $filterBox.removeClass('hidden-box').addClass('visible-box').slideDown();
        }
    });
})(jQuery);

function removeElement($ele, $parentEle) {
    var $this = $($ele);
    $this.parent($parentEle).css({
        opacity: '0'
    });
}