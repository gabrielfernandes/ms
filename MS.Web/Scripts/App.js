﻿window.paceOptions = {
    ajax: false, // disabled
    document: true, // disabled
    eventLag: false, // disabled
    elements: {
        selectors: ['.navbar']
    }
};
window.carregando = function (b) {
    if (b) {
        $("#div-carregando").show();
    } else {
        $("#div-carregando").fadeOut("slow");
    }
}


//$(document).ajaxStart(function () {
//    carregando(true);
//});
//$(document).ajaxComplete(function () {
//    carregando(false);
//});

$.fn.dataTable.ext.type.order['datahora-pre'] = function (d) {
    return moment(d, "DD/MM/YYYY HH:mm:ss").unix();
};

$.fn.dataTable.ext.type.order['moeda-pre'] = function (d) {
    return jx.formRotinas.converterParaFloat(d);
};
//************************* INICIALIZAR TABELA *********************************
$.fn.dataTable.Api.register('inicializar()', function ($dataTable) {
    $dataTable.find("#tb-dados > tr").removeClass("loading");
    $dataTable.find("#tb-loading").hide();
    $dataTable.find("#tb-dados").show();

    $dt_head = $dataTable.parent().parent().find('thead#table-tsearch').removeClass("hide");

    $dt_head = $dataTable.find('thead#table-tsearch').removeClass("hide");

});
//************************* IMPLEMENTAR O LINHA CLICK *********************************
$.fn.dataTable.Api.register('selecionado()', function ($dataTable) {
    var $dt_body = $dataTable.find('tbody');
    $dt_body.on('click', 'tr', function () {
        var tb = $(this).parent().parent();
        if ($(this).hasClass('selecionado')) {
            $(this).removeClass('selecionado');
        }
        else {
            tb.find('tr.selecionado').removeClass('selecionado');
            $(this).addClass('selecionado');
        }
        if (linhaClick) {
            linhaClick($(this));
        }
    });
});
//************************* ADICIONAR OS CAMPOS SEARCH *********************************
$.fn.dataTable.Api.register('addSearch()', function ($dataTable) {
    var $dt_head = $dataTable.find('thead#table-tsearch > tr.table-header').find('td');
    if ($dt_head.length == 0) {
        $dt_head = $dataTable.parent().parent().find('thead#table-tsearch > tr.table-header').find('td');
    }
    // ADICIONA OS IMPUTS PARA SEARCH
    $dt_head.each(function () {
        var title = $dt_head.eq($(this).index()).text();
        if (title != '#') {
            var $col_td = $($dt_head.eq($(this).index()));
            if ($col_td.attr("data-collumn-search") == "false") {
                $(this).text('');
            } else if ($col_td.attr("data-filter") == "int") {
                $(this).html($('<input/>', { 'type': 'text', 'data-mask': 'inteiro' }).addClass('width-100 search-input-text'));
            } else if ($col_td.attr("data-filter") == "check_all") {
                $(this).empty();
                $(this).attr("for", "dtable-check-all");
                $(this).append('<div class="checkbox checkbox-inline checkbox-default checkbox-all" style="margin-bottom:-1px;font-size:14px; padding-left: 2px; padding-right: 0px; margin-top:3px; margin-right: 5px;"><input data-tipo="check_all" style="width:17px; height:16px; margin:0px;" title="Marcar/Desmarcar Todos" type="checkbox"><label></label></div>')
                $("input[data-tipo='check_all']").click(function (event) {
                    var checkboxAll = $(this);
                    var rowcollection = $dataTable.DataTable().$("input[type='checkbox']", { "page": "all" });
                    if ($(this).prop("checked")) {
                        checkboxAll.removeClass("checked_partial").addClass("checked_all");
                        rowcollection.each(function (index, elem) {
                            $(elem).prop("checked", true);
                        });
                    } else {
                        checkboxAll.removeClass("checked_all").removeClass("checked_all");
                        rowcollection.each(function (index, elem) {
                            $(elem).prop("checked", false);
                        });
                    }
                });
                $dataTable.DataTable().$("input[type='checkbox']", { "page": "all" }).click(function (event) {
                    var $table = $($dataTable.DataTable().table().header()).parent();
                    var checkboxAll = $table.find("input[data-tipo='check_all']");
                    var rowscollectionCheckeds = $dataTable.DataTable().$("input[type='checkbox']:checked", { "page": "all" });
                    var rowcollection = $dataTable.DataTable().$("input[type='checkbox']", { "page": "all" });
                    if (rowscollectionCheckeds.length == rowcollection.length) {
                        checkboxAll.prop("checked", true);
                        checkboxAll.removeClass("checked_partial").addClass("checked_all");
                    } else if (rowscollectionCheckeds.length > 0) {
                        checkboxAll.addClass("checked_partial");
                    } else if (rowscollectionCheckeds.length == 0) {
                        checkboxAll.removeClass("checked_partial").removeClass("checked_all");
                        checkboxAll.prop("checked", false);
                    }
                });
            } else if ($col_td.attr("data-filter") == "float") {
                //$(this).html($('<input/>', { 'type': 'text', 'data-mask': 'filter-money', 'class': 'text-right' }).addClass('width-100 search-input-text'));
                $(this).html($('<input/>', { 'type': 'text', 'class': 'text-right' }).addClass('width-100 search-input-text'));
            } else if ($col_td.attr("data-filter") == "money") {
                $(this).html($('<input/>', { 'type': 'text' }).addClass('width-100 search-input-text'));
            } else if ($col_td.attr("data-filter") == "date") {
                $(this).html($('<input/>', { 'type': 'text', 'data-mask': 'datetime' }).addClass('width-100 search-input-text'));
            } else if ($col_td.attr("data-filter") == "cpf") {
                $(this).html($('<input/>', { 'type': 'text', 'data-mask': 'cnpj2' }).addClass('width-100 search-input-text'));
            } else {
                $(this).html($('<input/>', { 'type': 'text' }).addClass('width-100 search-input-text'));
            }

        }
    });
    $('input[type=text].search-input-text').on('keyup', function (event) {
        if (event.keyCode == 13 || event.keyCode == 9) {
            var value = $(this).val();

            $dataTable.block({
                message: '',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#fff',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff',
                    width: '50%'
                },
                overlayCSS: { backgroundColor: '#FFF' }
            });
            $dataTable.addClass('carregando');
            $dataTable.DataTable()
                .columns($(this).parent().index())
                .search(this.value)
                .draw();
        }
    });
});
//************************* ADICIONAR O TOTAL DA TABELA *********************************
$.fn.dataTable.Api.register('sum()', function () {
    this.columns('.sum-int', { page: 'current' }).every(function () {
        var column = this;
        var sum = 0;
        if (column.data().length) {
            var sum = column
                .data()
                .reduce(function (a, b) {
                    a = parseInt(jx.formRotinas.intVal(a), 10);
                    if (isNaN(a)) { a = 0; }

                    b = parseInt(jx.formRotinas.intVal(b), 10);
                    if (isNaN(b)) { b = 0; }
                    return a + b;
                });
        }

        $(column.footer()).html('Total: ' + sum);
    });
    this.columns('.sum-float', { page: 'current' }).every(function () {
        var column = this;
        var sum = 0;
        if (column.data().length) {
            sum = column.data().reduce(function (a, b) {
                a = parseFloat(jx.formRotinas.floatVal(a), 10);
                if (isNaN(a)) { a = 0; }

                b = parseFloat(jx.formRotinas.floatVal(b), 10);
                if (isNaN(b)) { b = 0; }
                return a + b;
            });
        }
        sum = jx.formRotinas.floatVal(sum);
        $(column.footer()).css('padding', '4px');
        $(column.footer()).html('Total: ' + 'R$ ' + sum.toFixed(2).replace('.', ',').replace(/([0-9]{2})$/g, "$1").toString().replace(/(\d)(?=(\d{3})+\b)/g, "$1."));

    });
    this.columns('.sum-float-percent', { page: 'current' }).every(function () {
        var column = this;
        var sum = 0;
        if (column.data().length) {
            sum = column.data().reduce(function (a, b) {
                a = parseFloat(jx.formRotinas.floatVal(a), 10);
                if (isNaN(a)) { a = 0; }

                b = parseFloat(jx.formRotinas.floatVal(b), 10);
                if (isNaN(b)) { b = 0; }
                return a + b;
            });
        }

        sum = jx.formRotinas.floatVal(sum);
        $(column.footer()).html('Total: ' + sum.toFixed(2).replace('.', ',').replace(/([0-9]{2})$/g, "$1").toString().replace(/(\d)(?=(\d{3})+\b)/g, "$1.") + '%');

    });
    this.columns('.sum-float').every(function () {
        var column = this;
        var sum = 0;
        if (column.data().length) {
            sum = column.data().reduce(function (a, b) {
                a = parseFloat(jx.formRotinas.floatVal(a), 10);
                if (isNaN(a)) { a = 0; }

                b = parseFloat(jx.formRotinas.floatVal(b), 10);
                if (isNaN(b)) { b = 0; }
                return a + b;
            });
        }
        sum = jx.formRotinas.floatVal(sum);
        //console.log('Total: ' + 'R$' + sum.toFixed(2).replace('.', ',').replace(/([0-9]{2})$/g, "$1").toString().replace(/(\d)(?=(\d{3})+\b)/g, "$1."));

    });
});

$.fn.scrollToMe = function () {
    var x = this.offset().top, y = this.offset().left;
    $('html, body').stop().animate({ scrollTop: x }, 1000);
    return this;

};
$(document).ready(function () {

    $("[title]").tooltip();
    $(".modal-dados-invalidos").modal("show");
    $(".field-validation-error").removeClass("field-validation-error").addClass("label label-danger animated bounceIn");

    $(".validation-summary-errors").removeClass("validation-summary-errors");
    $(".input-validation-error").removeClass("input-validation-error").parent().addClass("has-error");
    $("input[data-mask='time']").mask('00:00h');
    //$("input[data-mask='datetime']").datepicker({ language: 'pt-BR', format: 'dd/mm/yyyy', orientation: 'bottom' });
    //$("#filtro-data-master").datepicker({ language: 'pt-BR', format: 'dd/mm/yyyy' });

    // $("input[data-mask='months']").datepicker({ language: 'pt-BR', format: 'mm/yyyy', orientation: 'bottom', minViewMode: "months", autoclose: true });
    $("input[data-mask='months']").mask("00/0000");
    $("input[data-mask='filter-money']").mask("###.###.##0,00", { reverse: true });
    $("input[data-mask='money']").mask("#.##0,00", { reverse: true });
    $("input[data-mask='money2']").mask("###0,00", { reverse: true });
    $("input[data-mask='inteiro']").mask('#');
    $("input[data-mask='nossonumero']").mask('00000000');
    $("input[data-mask='lote']").mask('00000');
    $("input[data-mask='percentual']").mask("###0,00", { reverse: true });
    $("input[data-mask='cpf']").mask('000.000.000-00', { reverse: true });
    $("input[data-mask='cep']").mask('00000-000', { reverse: true });
    $("input[data-mask='telresidencial']").mask('(00) 0000-0000', { reverse: true });
    $("input[data-mask='telcelular']").mask('(00) 00000-0000', { reverse: true });
    $("input[data-mask='cnpj']").mask('00.000.000/0000-00', { reverse: true });
    $("input[data-mask='cnpj2']").mask('00.000.000/0000-00');
    $("input[data-mask='datetimehours2']").mask('00/00/0000 00:00');
    $("input[data-mask='datetimehours']").mask('00/00/0000 00:00');
    $("input[data-mask='datetimehours']").datetimepicker();
    $("input[data-mask='datetime']").mask('00/00/0000');
    $("input[data-mask='datetime2']").mask('00/00/0000');
    $("input[data-mask='datetime']").datetimepicker({
        //showClear: true,
        format: "DD/MM/YYYY"
    });
    $("input[data-mask='datetimeRange']").each(function () {
        var options = {
            showWeekNumbers: true,
            "ranges": {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                'Último Mês': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Para",
                "toLabel": "De",
                "todayRangeLabel": "Hoje",
                "customRangeLabel": "Custom",
                "weekLabel": "S",
                "daysOfWeek": ["Do", "Se", "Te", "Qu", "Qu", "Se", "Sa"],
                "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                "firstDay": 1
            },
            "alwaysShowCalendars": true,
            "startDate": $("input[datatime-ranger='De']").val() == "" ? moment() : $("input[datatime-ranger='De']").val(),
            "endDate": $("input[datatime-ranger='Ate']").val() == "" ? moment() : $("input[datatime-ranger='Ate']").val(),
        };

        var additionalOptions = { opens: "left" };
        if ($(this).prop("open", "center")) {
            additionalOptions.opens = "center";
        }
        if ($(this).prop("open", "center")) {
            additionalOptions.opens = "right";
        }
        $.extend(options, additionalOptions);
        $(this).daterangepicker(options);
    });

    $("input[data-mask='datetimeRange']").on('apply.daterangepicker', function (ev, picker) {
        $("input[datatime-ranger='De']").val(picker.startDate.format('DD/MM/YYYY'));
        $("input[datatime-ranger='Ate']").val(picker.endDate.format('DD/MM/YYYY'));
    });
    $("input[data-mask='datetimeRange']").on('cancel.daterangepicker', function (ev, picker) {
        $("input[data-mask='datetimeRange']").val('');
        $("input[datatime-ranger='De']").val('');
        $("input[datatime-ranger='Ate']").val('');
    });
    $("input[data-mask='datetimeRangeHours']").daterangepicker({
        "timePicker": true,
        "timePicker24Hour": true,
        "timePickerIncrement": 5,
        "autoApply": true,
        "ranges": {
            'Hoje': [moment().startOf('day'), moment()],
            'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este Mês': [moment().startOf('month'), moment().endOf('month')],
            'Último Mês': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "locale": {
            "format": "DD/MM/YYYY H:mm",
            "separator": " - ",
            "applyLabel": "Aplicar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Para",
            "toLabel": "De",
            "todayRangeLabel": "Hoje",
            "customRangeLabel": "Custom",
            "weekLabel": "S",
            "daysOfWeek": ["Do", "Se", "Te", "Qu", "Qu", "Se", "Sa"],
            "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            "firstDay": 1
        },
    });
    $("input[data-mask='datetimeRangeHours']").on('apply.daterangepicker', function (ev, picker) {
        $("input[datatime-ranger='De']").val(picker.startDate.format('DD/MM/YYYY HH:mm:ss'));
        $("input[datatime-ranger='Ate']").val(picker.endDate.format('DD/MM/YYYY HH:mm:ss'));
    });
    $("input[data-mask='datetimeRangeHours']").on('cancel.daterangepicker', function (ev, picker) {
        $("input[data-mask='datetimeRangeHours']").val('');
        $("input[datatime-ranger='De']").val('');
        $("input[datatime-ranger='Ate']").val('');
    });
    //{ locale: 'pt-BR',format: 'mm/yyyy' }
    $("li.dropdown").hover(function () {
        $(this).addClass("open is-open");
    }, function () {
        $(this).removeClass("open is-open");
    });
    //$("form").each(function (i, e) {
    //    console.log(e);
    //    if ($(e).find("input[data-mask='email']").length > 0) {
    //        $(e).validate({
    //            rules: {
    //                field: {
    //                    required: true,
    //                    email: true
    //                }
    //            }
    //        });
    //    };
    //});


    $(".select2-multiple").select2();
    $(".select2").select2();

    $('.editor-html').summernote({ height: '250px' });
    $("input.switch[type='checkbox']").bootstrapSwitch({ onText: 'Sim', offText: 'Não', onColor: 'primary' });
    //menu
    //var sideslider = $('[data-toggle=collapse-side]');
    //var sel = sideslider.attr('data-target');
    //var sel2 = sideslider.attr('data-target-2');
    //sideslider.click(function (event) {
    //    $(sel).toggleClass('in');
    //    $(sel2).toggleClass('out');
    //});
    try {
        $.fn.editable.defaults.mode = 'popup';
        $('.editable').editable();
        //data-ordem
        $('.data-table').each(function (i, e) {
            var el = $(e);
            var deffOrdem = el.attr("data-ordem");
            if (!deffOrdem) {
                deffOrdem = 0;
            }
            el.DataTable({
                "dom": '<<t>p>',
                "order": [[deffOrdem, "desc"]],
                "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            });
        });


    } catch (e) {
        console.log(e);
    }
    (function ($) {
        $.fn.serializeObject = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };

    })(jQuery);
    $(".data-table tbody").on('click', 'tr', function () {
        var table = $(this).parent().parent();

        if ($(this).hasClass('selecionado')) {
            $(this).removeClass('selecionado');
        }
        else {
            table.find('tr.selecionado').removeClass('selecionado');
            $(this).addClass('selecionado');
        }

        if (linhaClick) {
            linhaClick($(this));
        }
    });
    $(".data-table").find("input[type='checkbox']").change(function (i) {
        var el = $(this);
        $(".data-table input[type='checkbox']").not("#" + el.attr("id")).each(function (indx, e) {
            $(e).parent().parent().parent().removeClass("selecionado");
            $(e).attr("checked", false);
        });
        el.parent().parent().parent().toggleClass("selecionado");

        if (el.parent().parent().parent().hasClass("selecionado")) {
            el.attr("checked", true);
        } else {
            el.attr("checked", false);
        }
        if (linhaClick) {
            linhaClick(el);
        }
    });



});

jx = {

    notice: undefined,
    mensagem: function (titulo, texto, type, icon) {
        if (jx.notice) {
            jx.notice.remove();
        }
        if (titulo === "") {
            titulo = texto;
            texto = "";
        }
        jx.notice = new PNotify({
            title: titulo,
            text: texto,
            hide: true,
            type: type,
            icon: icon,
            animate: {
                animate: true,
                in_class: "flipInX",
                out_class: "bounceOut"
            },
            opacity: 1,
            shadow: true,
            width: "240px",
            height: "500px",
            buttons: {
                closer: true,
                sticker: true,
                classes: { closer: null, pin_up: null, pin_down: null }
            }
        });
    },
    tratarOperacaoRetorno: function (obj, callback) {
        if (obj.OK) {
            if (obj.Mensagem) {
                jx.mensagem('', obj.Mensagem, 'success', 'fa fa-check ');
            }
        } else {
            jx.mensagem('', 'Dados inválidos! </br>' + obj.Mensagem, 'warning', 'fa fa-warning ');
        }
        if (callback) {
            callback(obj);
        }
    },
    modalConfirm: function (titulo, mensagem, evento) {
        $(".modal-title").css({ 'color': '#c76161' });
        $(".modal-color-line").css({ 'background': '#c76161' });
        $(".modal-title").html("<i class='fa fa-info'></i>" + " " + titulo);
        $(".corpo-msg").html("<div class='col-md-12'>" + mensagem + "</div>");
        $("#jxModalDefault").modal("show");
        $("button[data-name='btn-confirmar']").off("click").click(evento)

    },
    modalOK: function (mensagem, operacao) {
        if (operacao) {
            $(".modal-title").html("<i class='fa fa-info'></i> Sucesso!");
        }
        else {
            $(".modal-title").html("<i class='fa fa-info'></i> Erro!");
        }
        $(".corpo-msg").html("<div class='col-md-12'> " + mensagem + "</div>");
        $("#jxModalDefault").modal("show");
        $(".modal-footer").empty();
        $("button[data-name='btn-cancelar']").hide();
        $("button[data-name='btn-confirmar']").html("<i class='fa fa-check'></i>" + " OK");
        $("button[data-name='btn-confirmar']").off("click").click(function () {
            $('#jxModalDefault').modal('toggle')
            document.location.reload(false);
        });

    },
    formRotinas: {
        formatarValorMoeda: function (c) {
            var valor = parseFloat(c).toFixed(2).toString().replace(".", ",");
            var el = $(document.createElement("input"));
            el.val(valor).mask('000.000.000.000.000,00', { reverse: true });
            return el.val();
        },
        formatarValorMilhar: function (c) {
            var valor = parseFloat(c).toFixed(0).toString().replace(".", ",");
            var el = $(document.createElement("input"));
            el.val(valor).mask('000.000.000.000.000', { reverse: true });
            return el.val();
        },
        converterParaFloat: function (valor) {
            if (valor) {
                return parseFloat(valor.replace("R$", "").replace(".", "").replace(",", "."));
            } else {
                return 0;
            }
        },
        intVal: function (i) {
            return typeof i === 'string' ?
                i.replace(/[\R$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        },
        floatVal: function (i) {
            return typeof i === 'string' ?
                i.replace(/[\R$.]/g, '').replace(/[,]/g, '.') * 1 :
                typeof i === 'number' ?
                    i : 0;
        },
        destacarCampoErro: function (campo, msg) {
            var $campo = $("#" + campo);
            $campo.removeClass("error");
            $campo.addClass("error")
            if (msg) {
                $campo.attr("title", msg);
            }
            $campo.blur(function () {
                if ($campo.val()) {
                    $campo.removeClass("error");
                    $campo.removeAttr("title");
                }

            });
        },
        obterDadosForm: function (formId) {
            var dados = {};
            $(formId).find("input,select,textarea,input:hidden").each(function (i, e) {
                var $ctrl = $(e);
                var $type = $ctrl.attr("type");
                var $mask = $ctrl.attr("data-mask");
                var name = $ctrl.attr("name");
                var value = $ctrl.val();
                switch ($type) {
                    case "checkbox":
                        {
                            value = $ctrl.prop("checked");
                            break;
                        }
                    default:
                }
                if ($mask !== undefined) {
                    switch ($mask) {
                        case "money":
                        case "money2":
                        case "money3":
                        case "money6":
                            {
                                value = value.replace(/\./g, "");
                                break;
                            }
                        default:
                    }
                }

                if (dados[name] !== undefined) {
                    if (!dados[$(e).attr("name")].push) {
                        dados[name] = [dados[name]];
                    }
                    dados[name].push(value || '');
                } else {
                    dados[name] = value || '';
                }
            })
            return dados;
        }
    },
    calendarioOpen: function (eventos, visualizacaoPadrao, retorno) {
        $('#dropdown-menu-mt').click(function () {
            $('#calendarDefault').fullCalendar('changeView', 'month');
        });

        $('#dropdown-menu-ag').click(function () {
            $('#calendarDefault').fullCalendar('changeView', 'agendaWeek');
        });

        $('#dropdown-menu-td').click(function () {
            $('#calendarDefault').fullCalendar('changeView', 'agendaDay');
        });

        function setarMesSelecionado() {
            var d = $('#calendarDefault').fullCalendar('getDate')._d;
        }

        function carregarMesSelecionado() {
            var date = new Date($('#calendarDefault').fullCalendar('getDate'));
            var d = $('#calendarDefault').fullCalendar('getDate')._d;
            var m = d.getMonth()
            var obj = {
                dia: 1, ano: d.getFullYear(), mes: parseInt(m)
            };
            $('#calendarDefault').fullCalendar('gotoDate', obj.ano + "-" + obj.mes);
        }

        var dados = eval(eventos); // Array com os eventos
        $('#calendarDefault').fullCalendar('destroy'); // Destroy calendario para criar um novo

        //******* CONSTRUÇÃO DO CALENDARIO *******
        $('#calendarDefault').fullCalendar({
            //Define as posicoes e botoes no cabeçalho
            header: {
                left: 'prev,next today',
                center: 'title',
                right: ''
            },
            //opção de colocar string no botao
            buttonText: {
                prev: '',
                next: ''
            },
            //Define a linguagem do calendario
            lang: 'pt-br',
            //Define qual tipo de visualização -- MES | AGENDA | DIA
            defaultView: visualizacaoPadrao, //'month' | 'agendaWeek' | 'agendaDay'
            selectable: true,
            selectHelper: true,
            //DEFINIR O CLICK NA LINHA PARA GERAR OU NAO UM NOVO EVENTO
            select: function (start, end, allDay) {

                if (confirm("Deseja agendar para esta data?") === true) {
                    retorno(start, end, allDay);
                }

                $('#calendarDefault').fullCalendar('unselect');
            },
            editable: false,
            //PREENCHER O ARRAY DE EVENTOS
            events: dados,
            //VERIFICA NOS EVENTOS SE TEM ALGUMA DESCRIÇAO OU ICONE E OS COLOCAM
            eventRender: function (event, element, icon) {
                element.find('.fc-time').append("<br />");
                if (!event.description === "") {
                    element.find('.fc-title').append("<br /><span class='ultra-light'>" + event.description +
                        "</span>");
                }
                if (!event.icon === "") {
                    { element.find('.fc-title').append("<i class='air air-top-right fa " + event.icon + " '></i>"); }
                }
            },


            //slotMinutes: 15,
            //snapMinutes: 15,
            //defaultEventMinutes: 30,
            //firstHour: 8,

            eventClick: function (calEvent, jsEvent, view) {
                $(".evento-destacado").removeClass("evento-destacado");
                $(this).addClass("evento-destacado");
            },
            windowResize: function (event, ui) {
                $('#calendarDefault').fullCalendar('render');
            }
        });

        $("#jnxCalendarDefault").modal("show");

        setTimeout(function () {
            $('#calendarDefault').fullCalendar('today');
        }, 200);
    },
    submit: function (url, data, callbackOK) {
        jx.carregando(true);
        $.post(url, data, function (retorno) {
            jx.tratarOperacaoRetorno(retorno, function () {
                callbackOK(retorno);
            });
            jx.carregando(false);
        }).fail(function (xhr, status, error) {
            jx.carregando(false);
            jx.mensagem('Erro', error, 'danger', 'fa fa-close ');
        });
    },
    carregando: function (show, texto) {
        if (false) {
            if (show) {
                if (texto) {
                    parent.$("#carregando-msg").empty().append(texto);
                } else {
                    parent.$("#carregando-msg").empty().append('Aguarde Carregando..');
                }
                parent.$("#div-carregando").show();
            } else {
                parent.$("#div-carregando").fadeOut("slow");
            }
        } else {
            if (show) {
                if (texto) {
                    $("#carregando-msg").empty().append(texto);
                } else {
                    $("#carregando-msg").empty().append('Aguarde Carregando..');
                }
                $("#div-carregando").show();
            } else {
                $("#div-carregando").fadeOut("slow");
            }
        }

    },
    popularDropdown: function (el, url, data, callbackOK) {
        jx.submit(url, data, function (d) {
            var dropDonw = $(el);

            if (dropDonw.length) {
                dropDonw.empty();
                $.each(d.Dados, function (index, item) {
                    dropDonw.append($("<option />").val(item.Key).text(item.Value));
                });
            }
            if (callbackOK) {
                callbackOK();
            }
        });
    },
    dtableSearch: function (dtable) {
        dtable.append("<thead class='hide' id='table-tsearch'></thead>");
        dtable.find('thead#table-tsearch').append("<tr class='table-header'></tr>");
        dtable.find('thead#dtheader > tr').find('th').each(function () {
            var title = dtable.find('thead#dtheader > tr#trheader').find('th').eq($(this).index()).text();
            var td = '';
            if ($(this).attr("data-collumn-search") == "false") {
                td = '<td class="text-right " data-filter="int" data-collumn-search="true"></td>';
            } else if ($(this).hasClass("check_all")) {
                td = '<td class="text-right " data-filter="check_all" data-collumn-search="true"></td>';
            } else if ($(this).hasClass("sum-int")) {
                td = '<td class="text-right " data-filter="int" data-collumn-search="true"></td>';
            } else if ($(this).hasClass("sum-float")) {
                td = '<td class="text-right " data-filter="float" data-collumn-search="true"></td>';
            } else if ($(this).hasClass("col-id")) {
                td = '<td class="text-center" data-collumn-search="false">#</td>';
            } else if ($(this).hasClass("filter-int")) {
                td = '<td class="text-center" data-filter="int" data-collumn-search="true"></td>';
            } else if ($(this).hasClass("filter-cpf")) {
                td = '<td class="text-center" data-filter="cpf" data-collumn-search="true"></td>';
            } else if ($(this).hasClass("filter-date")) {
                td = '<td class="text-center" data-filter="date" data-collumn-search="true"></td>';
            } else {
                td = '<td class="text-center" data-filter="string"></td>';
            }
            if ($(this).attr("data-collumn-search") == "false") {
                td = td.replace('data-collumn-search="true"', 'data-collumn-search="false"')
            }
            dtable.find('thead#table-tsearch > tr ').append(td);
        });
    },
    dtableFooter: function (dtable) {
        dtable.append("<tfoot></tfoot>");
        dtable.find('tfoot').append("<tr class='table-foot'></tr>");
        dtable.find('thead > tr').find('th').each(function () {
            var title = dtable.find('thead#dtheader > tr#trheader').find('th').eq($(this).index()).text();
            if ($(this).hasClass("sum-int") || $(this).hasClass("sum-float")) {
                dtable.find('tfoot > tr ').append('<th class="text-right"></th>');
            } else {
                dtable.find('tfoot > tr ').append('<th class="text-center"></th>');
            }
        });
    },
    dtableButtons: function ($dtables, dt, buttons) {
        var retorno = [];
        if ($.inArray("length", buttons) >= 0) {
            var lengthMenu = dt.settings()[0].aLengthMenu;
            var vals = $.isArray(lengthMenu[0]) ? lengthMenu[0] : lengthMenu;
            var lang = $.isArray(lengthMenu[0]) ? lengthMenu[1] : lengthMenu;
            var text = function (dt) {
                return dt.i18n('buttons.pageLength', {
                    "-1": 'Mostrar todas linhas',
                    _: 'Mostrar %d linhas'
                }, dt.page.len());
            };
            retorno.push({
                extend: 'colvis',
                text: text,
                className: 'buttons-page-length',
                autoClose: true,
                buttons: $.map(vals, function (val, i) {
                    return {
                        text: lang[i],
                        className: 'button-page-length',
                        action: function (e, dt) {
                            dt.page.len(val).draw();
                        },
                        init: function (dt, node, conf) {
                            var that = this;
                            var fn = function () {
                                that.active(dt.page.len() === val);
                            };

                            dt.on('length.dt' + conf.namespace, fn);
                            fn();
                        },
                        destroy: function (dt, node, conf) {
                            dt.off('length.dt' + conf.namespace);
                        }
                    };
                }),
                init: function (dt, node, conf) {
                    var that = this;
                    dt.on('length.dt' + conf.namespace, function () {
                        that.text(text(dt));
                    });
                },
                destroy: function (dt, node, conf) {
                    dt.off('length.dt' + conf.namespace);
                }
            });
        }
        if ($.inArray("excel", buttons) >= 0) {
            retorno.push({
                text: '<i class="fa fa-cloud-download"></i>',
                extend: 'csvHtml5',
                className: 'dt-button-excel',
                title: 'teste',
                exportOptions: {
                    columns: ':visible'
                },
                action: function (e, dt, node, config) {
                    DownloadExcel($dtables, e, dt, node, config);
                }
            });
        }
        if ($.inArray("filter", buttons) >= 0) {
            retorno.push({
                text: '<i class="fa fa-filter"></i>',
                className: 'dt-button-filter',
                active: 'active',
                disabled: 'disabled',
                action: function (e, dt, node, config) {
                    FiltrarContrato();
                }
            });
        }
        return retorno;
    },
    daterangerpickerClear: function () {
        $("input[data-mask='datetimeRange']").val('');
        $("input[datatime-ranger='De']").val('');
        $("input[datatime-ranger='Ate']").val('');
    },
    getElementAnchor: function () {
        // Javascript Pegar o elemento para ancora
        var anchorUrl = document.location.toString();
        if (anchorUrl.match('#')) {
            var id = '#' + anchorUrl.split('#')[1];
            var element = $(id);
            if (element) {
                return element;
            }
            return undefined;

        }
    },
    scrollToElement: function (elemnt, acrescimo) {
        var $header = $('.navbar.navbar-clean');
        var $footer = $('#footer');
        var $window = $(window).on('resize', function () {
            var height = $(this).height() - ($header.height() + $footer.height() + acrescimo);
            elemnt.height(height);
        }).trigger('resize'); //on page load
    },
    limiteCaracter: function (field, maxChar) {
        if ($(field).val().length > maxChar) {
            $(field).val($(field).val().substr(0, maxChar));
        }
        $(field).attr('maxlength', maxChar);
    },
    inicializarDataTable: function (el) {
        el.DataTable({
            "language": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                paging: false,
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",

                "buttons": {
                    "selectAll": "Marcar Todos",
                    "selectNone": "Desmarcar Todos"
                },
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                oAria: {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "dom": '<"row"<"col-xs-12 col-md-12"t>>',
        });
    },
    dTablesBlock: function (dTables) {
        dTables.block({
            message: '',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#fff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff',
                width: '50%'
            },
            overlayCSS: { backgroundColor: '#FFF' }
        });
        dTables.addClass('carregando');
    },
    dTablesUnblock: function (dTables) {
        dTables.unblock();
        dTables.removeClass('carregando');
    },
    FilterSwitcherHabilitar: function () {
        var $Switcher = $("#btnFilterSwitcher");
        $Switcher.removeClass("hide");
        $Switcher.find("a[disabled]").removeAttr("disabled");
        var today = new Date();
        var ticked = "&t=" + today.getSeconds();
        $("#filter-body-switcher").load("/Pesquisa/FiltroGeralShwitcher/?t=" + ticked, function (r) {


            $("[data-role='apply_filter']").click(function () {
                jx.FilterSwitcherApply();
            });
            $("[data-role='clear_filter']").click(function () {
                jx.FilterSwitcherClear();
            });
            jx.PreecherSelectpicker();

        });
    },
    FilterSwitcherApply: function () {
        var form = $("#form-filter-switcher");
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: jx.formRotinas.obterDadosForm(form),
            success: function (response) {
                jx.mensagem('Aplicando Filtros!', 'Aguarde...', 'success', 'fa fa-check ');
                setTimeout(function () {
                    location.reload();
                }, 1500);

            }
        });
    },
    FilterSwitcherClear: function () {
        var today = new Date();
        var ticked = "&t=" + today.getSeconds();
        $.get('/Pesquisa/ClearFiltroGeralShwitcher/?t=' + ticked, function (r) {
            setTimeout(function () {
                location.reload();
            }, 1500);
        });
    },
    PreecherSelectpicker: function () {
        $(".selectpicker[multiple]").each(function () {
            var checkeds = [];
            $this = $(this);
            $this.find("option.selected").each(function (i, el) {
                checkeds.push(el.value);
            });
            $this.val(checkeds);
            $this.selectpicker('refresh');
        })

    },
    pergunta: function (titulo, texto, callbackOK, callbackCancel) {
        jx.notice = (new PNotify({
            title: titulo,
            text: texto,
            icon: null,
            hide: false,
            confirm: {
                confirm: true,
                buttons: [{
                    text: 'Sim',
                    addClass: 'btn btn-sm btn-success'
                }, {
                    text: 'Não',
                    addClass: 'btn btn-sm btn-danger'
                }]
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            }
        })).get().on('pnotify.confirm', function () {
            if (callbackOK) {
                callbackOK();
            }
        }).on('pnotify.cancel', function () {
            if (callbackCancel) {
                callbackCancel();
            }
        });
    },
}


//$("form").validate().find("input[data-mask='email']").rules("add", {
//    email: true
//}); 