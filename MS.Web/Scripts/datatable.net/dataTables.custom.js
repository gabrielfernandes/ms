/*! DataTables Custom
 * ©2017 Junix Informática - Gabriel Correia
 */

(/** @lends <global> */function (window, document, undefined) {
    $.fn.dataTable.ext.type.order['datahora-pre'] = function (d) {
        return moment(d, "DD/MM/YYYY HH:mm:ss").unix();
    };
    $.fn.dataTable.ext.type.order['moeda-pre'] = function (d) {
        return jx.formRotinas.converterParaFloat(d);
    };
    //************************* INICIALIZAR TABELA *********************************
    $.fn.dataTable.Api.register('inicializar()', function ($dataTable) {
        $dataTable.find("#tb-dados > tr").removeClass("loading");
        $dataTable.find("#tb-loading").hide();
        $dataTable.find("#tb-dados").show();
        $dataTable.find("#table-tsearch").removeClass("hide");
    });
    //************************* IMPLEMENTAR O LINHA CLICK *********************************
    $.fn.dataTable.Api.register('selecionado()', function ($dataTable) {
        var $dt_body = $dataTable.find('tbody');
        $dt_body.on('click', 'tr', function () {
            var tb = $(this).parent().parent();
            if ($(this).hasClass('selecionado')) {
                $(this).removeClass('selecionado');
            }
            else {
                tb.find('tr.selecionado').removeClass('selecionado');
                $(this).addClass('selecionado');
            }
            if (linhaClick) {
                linhaClick($(this));
            }
        });
    });

    //************************* ADICIONAR OS CAMPOS SEARCH *********************************
    $.fn.dataTable.Api.register('addSearch()', function ($dataTable) {
        
        var $dt_head = $dataTable.find('thead#table-tsearch > tr.table-header').find('td');
        // ADICIONA OS IMPUTS PARA SEARCH
        $dt_head.each(function () {
            var title = $dt_head.eq($(this).index()).text();
            if (title != '#') {
                var $col_td = $($dt_head.eq($(this).index()));
                if ($col_td.attr("data-collumn-search") == "false") {
                    $(this).text('');
                } else if ($col_td.attr("data-filter") == "int") {
                    $(this).html($('<input/>', { 'type': 'text', 'data-mask': 'inteiro' }).addClass('width-100 search-input-text'));
                } else if ($col_td.attr("data-filter") == "check_all") {
                    $(this).empty();
                    $(this).append('<div class="checkbox checkbox-inline checkbox-default checkbox-all" style="margin-bottom:4px;font-size:14px;  padding-left: 7px; padding-right: 0px; margin:0; margin-top:2px;"><input data-tipo="check_all" type="checkbox"><label></label></div>')
                    $("input[data-tipo='check_all']").click(function (event) {
                        var checkboxAll = $(this);
                        var rowcollection = $dataTable.DataTable().$("input[type='checkbox']", { "page": "all" });
                        if ($(this).prop("checked")) {
                            checkboxAll.removeClass("checked_partial").addClass("checked_all");
                            rowcollection.each(function (index, elem) {
                                $(elem).prop("checked", true);
                            });
                        } else {
                            checkboxAll.removeClass("checked_all").removeClass("checked_all");
                            rowcollection.each(function (index, elem) {
                                $(elem).prop("checked", false);
                            });
                        }
                    });
                    $dataTable.DataTable().$("input[type='checkbox']", { "page": "all" }).click(function (event) {
                        var checkboxAll = $("input[data-tipo='check_all']");
                        var rowscollectionCheckeds = $dataTable.DataTable().$("input[type='checkbox']:checked", { "page": "all" });
                        var rowcollection = $dataTable.DataTable().$("input[type='checkbox']", { "page": "all" });
                        if (rowscollectionCheckeds.length == rowcollection.length) {
                            checkboxAll.prop("checked", true);
                            checkboxAll.removeClass("checked_partial").addClass("checked_all");
                        } else if (rowscollectionCheckeds.length > 0) {
                            checkboxAll.addClass("checked_partial");
                        } else if (rowscollectionCheckeds.length == 0) {
                            checkboxAll.removeClass("checked_partial").removeClass("checked_all");
                            checkboxAll.prop("checked", false);
                        }
                    });
                } else if ($col_td.attr("data-filter") == "float") {
                    $(this).html($('<input/>', { 'type': 'text', 'data-mask': 'filter-money', 'class': 'text-right' }).addClass('width-100 search-input-text'));
                } else if ($col_td.attr("data-filter") == "money") {
                    $(this).html($('<input/>', { 'type': 'text' }).addClass('width-100 search-input-text'));
                } else if ($col_td.attr("data-filter") == "date") {
                    $(this).html($('<input/>', { 'type': 'text', 'data-mask': 'datetime' }).addClass('width-100 search-input-text'));
                } else if ($col_td.attr("data-filter") == "cpf") {
                    $(this).html($('<input/>', { 'type': 'text', 'data-mask': 'cnpj2' }).addClass('width-100 search-input-text'));
                } else {
                    $(this).html($('<input/>', { 'type': 'text' }).addClass('width-100 search-input-text'));
                }

            }
        });
        $('input[type=text].search-input-text').on('keyup', function (event) {
            if (event.keyCode == 13 || event.keyCode == 9) {
                var value = $(this).val();
                $dataTable.block({
                    message: '',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#fff',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff',
                        width: '50%'
                    },
                    overlayCSS: { backgroundColor: '#FFF' }
                });
                $dataTable.addClass('carregando');
                $dataTable.DataTable()
                    .columns($(this).parent().index())
                    .search(this.value)
                    .draw();
            }
        });
    });

    //************************* ADICIONAR O TOTAL DA TABELA *********************************
    $.fn.dataTable.Api.register('sum()', function () {
        this.columns('.sum-int', { page: 'current' }).every(function () {
            var column = this;
            var sum = 0;
            if (column.data().length) {
                var sum = column
                    .data()
                    .reduce(function (a, b) {
                        a = parseInt(dataTableCustom.intVal(a), 10);
                        if (isNaN(a)) { a = 0; }

                        b = parseInt(dataTableCustom.intVal(b), 10);
                        if (isNaN(b)) { b = 0; }
                        return a + b;
                    });
            }

            $(column.footer()).html('Total: ' + sum);
        });
        this.columns('.sum-float', { page: 'current' }).every(function () {
            debugger;
            var column = this;
            var sum = 0;
            if (column.data().length) {
                sum = column.data().reduce(function (a, b) {
                    a = (jx.formRotinas.floatVal(a), 10);
                    if (isNaN(a)) { a = 0; }

                    b = parseFloat(jx.formRotinas.floatVal(b), 10);
                    if (isNaN(b)) { b = 0; }
                    return a + b;
                });
            }
            sum = jx.formRotinas.floatVal(sum);
            $(column.footer()).html('Total: ' + 'R$' + sum.toFixed(2).replace('.', ',').replace(/([0-9]{2})$/g, "$1").toString().replace(/(\d)(?=(\d{3})+\b)/g, "$1."));

        });
    });
    dataTableCustom = {
        dtableSearch: function (dtable) {
            dtable.append("<thead class='hide' id='table-tsearch'></thead>");
            dtable.find('thead#table-tsearch').append("<tr class='table-header'></tr>");
            dtable.find('thead#dtheader > tr').find('th').each(function () {
                var title = dtable.find('thead#dtheader > tr#trheader').find('th').eq($(this).index()).text();
                var td = '';
                if ($(this).attr("data-collumn-search") == "false") {
                    td = '<td class="text-right " data-filter="int" data-collumn-search="true"></td>';
                } else if ($(this).hasClass("check_all")) {
                    td = '<td class="text-right " data-filter="check_all" data-collumn-search="true"></td>';
                } else if ($(this).hasClass("sum-int")) {
                    td = '<td class="text-right " data-filter="int" data-collumn-search="true"></td>';
                } else if ($(this).hasClass("sum-float")) {
                    td = '<td class="text-right " data-filter="float" data-collumn-search="true"></td>';
                } else if ($(this).hasClass("col-id")) {
                    td = '<td class="text-center" data-collumn-search="false">#</td>';
                } else if ($(this).hasClass("filter-int")) {
                    td = '<td class="text-center" data-filter="int" data-collumn-search="true"></td>';
                } else if ($(this).hasClass("filter-cpf")) {
                    td = '<td class="text-center" data-filter="cpf" data-collumn-search="true"></td>';
                } else if ($(this).hasClass("filter-date")) {
                    td = '<td class="text-center" data-filter="date" data-collumn-search="true"></td>';
                } else {
                    td = '<td class="text-center" data-filter="string"></td>';
                }
                if ($(this).attr("data-collumn-search") == "false") {
                    td = td.replace('data-collumn-search="true"', 'data-collumn-search="false"')
                }
                dtable.find('thead#table-tsearch > tr ').append(td);
            });
        },
        dtableFooter: function (dtable) {
            dtable.append("<tfoot></tfoot>");
            dtable.find('tfoot').append("<tr class='table-foot'></tr>");
            dtable.find('thead > tr').find('th').each(function () {
                var title = dtable.find('thead#dtheader > tr#trheader').find('th').eq($(this).index()).text();
                if ($(this).hasClass("sum-int") || $(this).hasClass("sum-float")) {
                    dtable.find('tfoot > tr ').append('<th class="text-right"></th>');
                } else {
                    dtable.find('tfoot > tr ').append('<th class="text-center"></th>');
                }
            });
        },
        intVal: function (i) {
            return typeof i === 'string' ?
                i.replace(/[\R$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        },
        floatVal: function (i) {
            return typeof i === 'string' ?
                i.replace(/[\R$.]/g, '').replace(/[,]/g, '.') * 1 :
                typeof i === 'number' ?
                    i : 0;
        }
    }
}(window, document));

