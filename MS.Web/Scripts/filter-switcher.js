﻿/*!
 * jQuery Cookie Plugin v1.4.1
 *
 * Released under the MIT license
 */

/* Filter Switcher
 * Author: Gabriel
 * File Description: Filter Switcher
 */

jQuery(document).ready(function ($) {
    //SIDE PANEL 
    //--------------------------------------------------------
    filter_switcher = $('.filter-switcher'),
        panelWidth = filter_switcher.outerWidth(true);

    $('.filter-switcher .trigger').on("click", function () {
        var $this = $(this);
        if (!$this.is('[disabled=disabled]')) {
            if ($(".filter-switcher.closed").length > 0) {
                filter_switcher.animate({ "left": "0px" });
                $(".filter-switcher.closed").removeClass("closed");
                $(".filter-switcher").addClass("opened");
                $(".filter-switcher .trigger i").removeClass("fa fa-filter").addClass("fa fa-times");
            } else {
                $(".filter-switcher.opened").removeClass("opened");
                $(".filter-switcher").addClass("closed");
                $(".filter-switcher .trigger i").removeClass("fa fa-times").addClass("fa fa-filter");
                filter_switcher.animate({ "left": '-' + panelWidth });
            }
        }
        return false;
    });

});    	