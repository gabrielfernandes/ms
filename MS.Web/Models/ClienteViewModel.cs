﻿using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Cobranca.Web.Models
{
    public class ClienteViewModel
    {
        public List<ContatoViewModel> Contatos { get; set; }
       
        public class ContatoViewModel
        {
            public int Cod { get; private set; }
            public int CodCliente { get; private set; }
            public int CodTipoCadastro { get; private set; }
            public string Contato { get; private set; }
            public bool ContatoHOT { get; private set; }
            public string Descricao { get; private set; }
            public short Tipo { get; private set; }

            public ContatoViewModel(string Cod, string CodCliente, /*string CodTipoCadastro,*/ string Contato, string ContatoHOT, string Descricao, string Tipo)
            {
                this.Cod = DbRotinas.ConverterParaInt(Cod);
                this.CodCliente = DbRotinas.ConverterParaInt(CodCliente);
                //this.CodTipoCadastro = DbRotinas.ConverterParaInt(CodTipoCadastro);
                this.ContatoHOT = DbRotinas.ConverterParaBool(ContatoHOT);
                this.Contato = Contato;
                this.Descricao = Descricao;
                this.Tipo = DbRotinas.ConverterParaShort(Tipo);
            }

            
        }
        public class ClienteContatoViewModel
        {
            public string[] Cod { get; set; }
            public string[] CodCliente { get; set; }
            public string[] CodTipoCadastro { get; set; }
            public string[] Contato { get; set; }
            public string[] ContatoHOT { get; set; }
            public string[] Descricao { get; set; }
            public string[] Tipo { get; set; }


        }
    }
}