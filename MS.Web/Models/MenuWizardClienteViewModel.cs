﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    [Serializable]
    public class MenuWizardClienteViewModel
    {
        public MenuWizardClienteViewModel(int codigoProposta, int codigoCliente, string menuSelecionado)
        {
            this.CodProposta = codigoProposta;
            this.CodCliente = codigoCliente;
            this.MenuSelecionado = menuSelecionado;
        }
        public int CodProposta { get; set; }
        public int CodCliente { get; set; }
        public string MenuSelecionado { get; set; }
    }
}