﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Cobranca.Web.Models
{
    public class ContratoParcelaBoletoViewModel
    {
        
        public List<ParcelaBoleto> ParcelasBoletos { get; set; }

        public class ParcelaBoleto
        {
            public int Cod { get; set; }
            public int CodParcela { get; set; }
            public string NumeroDocumento { get; set; }
            public Nullable<DateTime> DataVencimento { get; set; }
            public Nullable<DateTime> DataMoratoria { get; set; }
            public Nullable<decimal> ValorDocumento { get; set; }

            public Nullable<decimal> MultaAtrasoPercento { get; set; }
            public Nullable<decimal> MultaAtrasoValor { get; set; }
            public decimal JurosAoMesPorcentagem { get; set; }
            public decimal JurosAoMeValor { get; set; }
            public int PeriodoMes { get; set; }
            public Nullable<decimal> ValorAcrescimo { get; set; }
            public Nullable<decimal> ValorAtualizado { get; set; }
            public Nullable<decimal> ValorDesconto { get; set; }
            public int QtdDias { get; set; }

            public ParcelaBoleto(string Cod = null, string CodParcela = null, string NumeroDocumento = null, string DataVencimento = null, string DataMoratoria = null, string ValorDocumento = null, string MultaAtrasoPorcentagem = null, string JurosAoMesPorcentagem = null, string ValorDesconto = null)
            {
                this.Cod = DbRotinas.ConverterParaInt(Cod);
                this.CodParcela = DbRotinas.ConverterParaInt(CodParcela);
                this.NumeroDocumento = NumeroDocumento;
                this.DataVencimento = DbRotinas.ConverterParaDatetime(DataVencimento);
                this.DataMoratoria = DbRotinas.ConverterParaDatetime(DataMoratoria);
                this.ValorDocumento = DbRotinas.ConverterParaDecimal(ValorDocumento);
                this.MultaAtrasoPercento = DbRotinas.ConverterParaDecimal(MultaAtrasoPorcentagem);
                this.QtdDias = ((DateTime)this.DataMoratoria - (DateTime)this.DataVencimento).Days;
                this.PeriodoMes = DbRotinas.DiferencaMes((DateTime)this.DataVencimento, (DateTime)this.DataMoratoria) == 0 ? (DateTime)this.DataVencimento < DateTime.Now ? 1 : 0 : DbRotinas.DiferencaMes((DateTime)this.DataVencimento, (DateTime)this.DataMoratoria);
                this.MultaAtrasoValor = DbRotinas.ConverterParaDatetime(this.DataVencimento).Date < DbRotinas.ConverterParaDatetime(this.DataMoratoria).Date ? this.ValorDocumento * (this.MultaAtrasoPercento / 100) : 0;
                this.JurosAoMesPorcentagem = DbRotinas.ConverterParaDecimal(JurosAoMesPorcentagem);
                this.JurosAoMeValor = QtdDias >= 0 ? (decimal)((((this.JurosAoMesPorcentagem / 30) * this.QtdDias) / 100) * this.ValorDocumento) : (decimal)0;
                this.ValorDesconto = DbRotinas.ConverterParaDecimal(ValorDesconto);
                this.ValorAtualizado = (this.ValorDocumento + this.JurosAoMeValor + this.MultaAtrasoValor) - this.ValorDesconto;

            }


        }
        public class BoletoViewModelViewModel
        {
            public string[] Cod { get; set; }
            public string[] CodParcela { get; set; }
            public string[] NumeroDocumento { get; set; }
            public string[] DataVencimento { get; set; }
            public string[] DataMoratoria { get; set; }
            public string[] ValorDocumento { get; set; }
            public string[] ValorDesconto { get; set; }
            public string[] MultaAtrasoPorcentagem { get; set; }
            public string[] JurosAoMesPorcentagem { get; set; }
            public string[] Observacao { get; set; }
            public string[] InfoImpressa { get; set; }


        }

        public static DateTime proximoDiaUtil(DateTime dt)
        {
            while (true)
            {
                if (dt.DayOfWeek == DayOfWeek.Saturday)
                {
                    dt = dt.AddDays(2);
                    return proximoDiaUtil(dt);
                }
                else if (dt.DayOfWeek == DayOfWeek.Sunday)
                {
                    dt = dt.AddDays(1);
                    return proximoDiaUtil(dt);
                }
                else if (Feriado(dt) == true)
                {
                    dt = dt.AddDays(1);
                    return proximoDiaUtil(dt);
                }
                else return dt;
            }
        }
        public static bool Feriado(DateTime dt)
        {
            Repository<ParametroFeriado> _dbFeriado = new Repository<ParametroFeriado>();
            var _listFeriados = _dbFeriado.FindAll(x=> x.Data == dt && x.Ativo == true && !x.DataExcluido.HasValue).ToList();
                           
            if (_listFeriados.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}