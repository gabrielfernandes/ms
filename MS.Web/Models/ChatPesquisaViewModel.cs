﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Cobranca.Web.Models
{
    [Serializable]
    public class ChatPesquisaViewModel
    {
        public long ClienteId { get; set; }
        public long UsuarioId { get; set; }
        public long ChamadoId { get; set; }
        public DateTime? DataDe { get; set; }
        public DateTime? DataAte { get; set; }

        [XmlIgnoreAttribute]
        public System.Web.Mvc.SelectList SelectListClientes { get; set; }

        [XmlIgnoreAttribute]
        public System.Web.Mvc.SelectList SelectListUsuarios { get; set; }

        [XmlIgnoreAttribute]
        public System.Web.Mvc.SelectList SelectListTrabalhos { get; set; }

        [XmlIgnoreAttribute]
        public System.Web.Mvc.SelectList SelectListOrigens { get; set; }

        public long OritemTipoId { get; set; }

        public long TrabalhoTipoId { get; set; }

        [XmlIgnoreAttribute]
        public System.Web.Mvc.SelectList SelectListSistema { get; set; }

        public long SistemaId { get; set; }

        public string Email { get; set; }
        public string Nome { get; set; }

        [XmlIgnoreAttribute]
        public List<Db.Models.ChatAtendimento> Resultado { get; set; }

        public long? EmpresaId { get; set; }

        public System.Web.Mvc.SelectList SelectListTipoAtendimento { get; set; }

        public long? AtendimentoTipoId { get; set; }

        public long? ResultadoId { get; set; }

        public System.Web.Mvc.SelectList SelectListTipoResultado { get; set; }
    }
}