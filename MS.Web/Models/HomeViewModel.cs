﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cobranca.Db.Models;

namespace Cobranca.Web.Models
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {
            ListaRegionais = new DictionaryEntry();
            ListaProdutos = new DictionaryEntry();
            ListaAgings = new DictionaryEntry();
            ListaAtendentes = new DictionaryEntry();
            HomeIndex = new HomeIndexViewModel();
            HomeSolicitacao = new HomeSolicitacaoViewModel();
            HomeDesempenho = new HomeDesempenhoViewModel();
        }

        public void SetUsuario(UsuarioLogadoInfo usuario)
        {
            UsuarioCod = usuario.Cod;
            UsuarioPerfil = (Enumeradores.PerfilTipo)usuario.PerfilTipo;
            UsuarioIsAdmin = usuario.Admin == true ? true : false;
        }



        public int UsuarioCod { get; set; }
        public Enumeradores.PerfilTipo UsuarioPerfil { get; set; }
        public bool UsuarioIsAdmin { get; set; }
        public Enumeradores.tabsHomeCobranca tabSelecionada { get; set; }


        public DictionaryEntry ListaRegionais { get; set; }
        public DictionaryEntry ListaProdutos { get; set; }
        public DictionaryEntry ListaAgings { get; set; }
        public DictionaryEntry ListaAtendentes { get; set; }

        public HomeIndexViewModel HomeIndex { get; set; }
        public HomeSolicitacaoViewModel HomeSolicitacao { get; set; }
        public HomeDesempenhoViewModel HomeDesempenho { get; set; }
    }
}