﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Cobranca.Web.Models
{
    [Serializable]
    public class ModuloClienteViewModel
    {
        public int? CodCliente { get; set; }
        public string NomeCliente { get; set; }
        public string EmailCliente { get; set; }
        public string NumeroFatura { get; set; }
        public int NumeroParcela { get; set; }
        public int NumeroTotalParcela { get; set; }
        public string Data { get; set; }
        public string StatusFatura { get; set; }
        public string ValorTotal { get; set; }
        public string ValorParcela { get; set; }
        public string DataFatura { get; set; }
        public int NumeroDiasAtraso { get; set; }


        [XmlIgnoreAttribute]
        public System.Web.Mvc.SelectList SelectListClientes { get; set; }

       
    }
}