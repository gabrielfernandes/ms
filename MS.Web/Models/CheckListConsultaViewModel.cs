﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class CheckListConsultaViewModel
    {
        public int codCheckListNome { get; set; }
        public Nullable<int> codTipoDocumento { get; set; }
        public Nullable<int> codBanco { get; set; }
        public string Documento { get; set; }
        public Nullable<short> estadoCivil { get; set; }
        public Nullable<short> categoriaProfissional { get; set; }
        public Nullable<bool> fgts { get; set; }
        public Nullable<bool> cartaoCredito { get; set; }
        public Nullable<bool> aplicacaoFinanceira { get; set; }
        public Nullable<bool> aluguel { get; set; }
        public Nullable<bool> obrigatorio { get; set; }
    }
}