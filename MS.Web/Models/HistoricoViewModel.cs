﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class HistoricoViewModel
    {
        public int Cod { get; set; }

        public string ParcelaIds { get; set; }
        public Nullable<int> CodCliente { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public Nullable<int> CodParcela { get; set; }
        public Nullable<int> CodSintese { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public System.DateTime DataCadastro { get; set; }
        public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<System.DateTime> DataExcluido { get; set; }
        public string Observacao { get; set; }
        public Nullable<int> CodMotivoAtraso { get; set; }
        public Nullable<int> CodResolucao { get; set; }
        public Nullable<System.DateTime> DataAgenda { get; set; }
        

    }
}