﻿using Cobranca.Db.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    [Serializable]
    public class UsuarioLogadoInfo
    {
        public int Cod { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public Enumeradores.PerfilTipo PerfilTipo { get; set; }
        public Enumeradores.TipoFluxo TipoFluxo { get; set; }
        public Enumeradores.SistemaModulo Modulo { get; set; }
        public DateTime DataInicioSessao { get; set; }
        public string Token
        {
            get
            {
                return string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}",
                    this.Cod,
                    this.Nome,
                    this.Email,
                    (short)this.PerfilTipo,
                    (short)this.TipoFluxo,
                    (short)this.Modulo,
                    this.DataInicioSessao,
                    this.Login,
                    this.CodEmpresaVenda,
                    this.CodEscritorio,
                    this.Admin,
                    this.CodRegional,
                    (short)this.Idioma
                    );
            }
        }
        public string Login { get; set; }
        public int? CodEmpresaVenda { get; set; }

        public bool SistemaCobranca
        {
            get
            {
                return Modulo == Enumeradores.SistemaModulo.Cobranca;
            }
        }


        public int? CodEscritorio { get; internal set; }
        public bool? Admin { get; internal set; }

        public int? CodRegional { get; set; }


        public Enumeradores.IdiomaTipo Idioma { get; set; }
    }
}