﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class PerguntaAnaliseDocumentalViewModel
    {
        public int CodAnaliseDocumental { get; set; }
        public int CodDocumento { get; set; }
        public bool Confere { get; set; }
        public decimal Valor { get; set; }
        public string Observacao { get; set; }
    }
}