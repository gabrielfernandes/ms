﻿using Cobranca.Db.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{

    public class QueryBuilderViewModel
    {
        public string Query { get; set; }
    }

}