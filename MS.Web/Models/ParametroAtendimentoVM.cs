﻿using Cobranca.Rotinas.QueryBuilder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Cobranca.Web.Models
{
    public class ParametroAtendimentoVM : AttributesValues
    {
        public ParametroAtendimentoVM()
        {
            this.ListRegionais = new List<DictionaryEntry>();
            this.ListUsuarios = new List<DictionaryEntry>();

        }

        [IsQueryBuilder(true)]
        [DisplayName("Nome do Produto")]
        [RulesInput("checkbox")]
        [RulesValue("N", "M", "B", "P", "T", "S")]
        [RulesOperator("IN", "NOT IN")]
        public string Produto { get; private set; }

        [DisplayName("Rota")]
        [IsQueryBuilder(false)]
        [RulesOperator("IN", "NOT IN")]
        public string Rota { get; private set; }

        [DisplayName("Jurídico?")]
        [IsQueryBuilder(true)]
        [RulesValue("Sim", "Não")]
        [RulesInput("radio")]
        [RulesOperator("==")]
        public bool Juridico { get; private set; }


        [DisplayName("Dias de Atraso")]
        [IsQueryBuilder(true)]
        [RulesValue("Sim", "Não")]
        [RulesInput("radio")]
        [RulesOperator("==")]
        public int DiasAtraso { get; private set; }

        public Nullable<int> CodRegional { get; set; }
        public string Regional { get; set; }
        public List<DictionaryEntry> ListRegionais { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public string Usuario { get; set; }
        public List<DictionaryEntry> ListUsuarios { get; set; }
        public string Query { get; set; }
        public string Rotas { get; set; }

    }
}