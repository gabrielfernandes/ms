﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Cobranca.Web.Models
{
    [Serializable]
    public class ContratoParcelaViewModel
    {
        public Nullable<int> CodBoleto { get; set; }
        public Nullable<int> CodParcela { get; set; }

        public Nullable<int> CodContrato { get; set; }
        public Nullable<int> CodNatureza { get; set; }
        public string FluxoPagamento { get; set; }
        public string strDataVencimento { get; set; }
        public string DataPagamento { get; set; }
        public string ValorParcela { get; set; }
        public string ValorRecebido { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<int> NaturezaCliente { get; set; }
        public Nullable<short> StatusParcela { get; set; }
        public Nullable<decimal> ValorAberto { get; set; }
        public string NumeroParcela { get; set; }


        public string ValorAtualizado { get; set; }

        public string Status { get; set; }

        public Nullable<decimal> ValorTotal { get; set; }

        public DateTime DataVencimento { get; set; }
        public string NumeroContrato { get; set; }

        public string Natureza { get; set; }
        public Nullable<decimal> Juros { get; set; }
        public Nullable<decimal> Multa { get; set; }
        public string TipoParcela { get; set; }
        public string QtdDiasAtraso { get; set; }

        public bool PossuiLinhaDigitavel { get; set; }
        public string NotaFiscal { get; set; }
        public string TipoFatura { get; set; }
    }
}