﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class LogServicoViewModel
    {
        public string Filtro { get; set; }
        public string PeriodoDe { get; set; }
        public string PeriodoAte { get; set; }

        public string destinatario { get; set; }
        public string assunto { get; set; }
        public string counteudo { get; set; }
        public bool OK { get; set; }
        public IEnumerable<dynamic> resposta { get; set; }

    }
}