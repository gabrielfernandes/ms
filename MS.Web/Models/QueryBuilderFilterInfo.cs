﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    [Serializable]
    public class QueryBuilderFilterInfo
    {
        public string Id { get; set; }
        public string label { get; set; }
        public string type { get; set; }
        public string input { get; set; }
        public object values { get; set; }


        public void add(string id, string label, QueryBuilderType type, QueryBuilderInput input, object values)
        {
            this.Id = id;
            this.label = label;
            this.type = type.ToString();
            this.input = input.ToString();
            this.values = values;
        }
        public enum QueryBuilderType
        {
            integer = 1
        }
        public enum QueryBuilderInput
        {
            input = 1,
            select = 2
        }
    }

}