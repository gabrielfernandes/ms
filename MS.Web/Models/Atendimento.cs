﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Web.Rotinas;

namespace Cobranca.Web.Models
{
    public class Atendimento
    {

        public Repository<ClienteAtendimento> atendimento = new Repository<ClienteAtendimento>();
        public Repository<Usuario> _dbUsuario = new Repository<Usuario>();

        public ClienteAtendimento ObterAtendimento(int AtendenteId = 0, int RegionalId = 0)
        {
            var _user = WebRotinas.ObterUsuarioLogado();
            Dados obj = new Dados();

            if (_user.PerfilTipo == Enumeradores.PerfilTipo.Regional)
            {
                AtendenteId = _user.Cod;
            }
            var _atendente = _dbUsuario.FindById(AtendenteId);

            //OBTEM A LISTA DE CLIENTES DE SUA FILA 
            //  (obs: a fila de atendimento não é atruida a um usuário, e sim determinado
            //  pelos parametros de cada atendente que determina quais ações deve pegar)
            var lista = obj.ConsultaCasosAtendimento(AtendenteId, RegionalId, 0);

            foreach (var item in lista)
            {
                //LISTA DE ATENDIMENTOS PENDETES DESTE CLIENTE (CodCliente)
                var clienteAtendimento = atendimento.All().Where(x => x.CodCliente == item.CodCliente && !x.DataExcluido.HasValue && !x.DataFinalizacaoAtendimento.HasValue).FirstOrDefault();


                //VERIFICA SE O CLIENTE JA POSSUI REGISTRO NO ClienteAtendimento
                if (clienteAtendimento == null)
                {
                    //DEVE CRIAR UM REGISTRO NO ClienteAtendimento PARA QUE O CLIENTE NÃO SEJA ATENDINDO PELOS DEMAIS ATENDETES
                    ClienteAtendimento atendimentoCliente = new ClienteAtendimento();
                    atendimentoCliente.CodCliente = item.CodCliente;
                    atendimentoCliente.CodUsuario = WebRotinas.ObterUsuarioLogado().Cod;
                    atendimentoCliente.DataAtendimento = DateTime.Now;

                    if (_user.Cod == _atendente.Cod)
                    {
                        return atendimento.CreateOrUpdate(atendimentoCliente);
                    }
                    else
                    {
                        return atendimentoCliente;
                    }
                    

                }
                else if (clienteAtendimento != null && clienteAtendimento.CodUsuario == _atendente.Cod && _user.Cod != _atendente.Cod && (_user.PerfilTipo == Enumeradores.PerfilTipo.Admin || _user.PerfilTipo == Enumeradores.PerfilTipo.SupervisorAnalista || _user.PerfilTipo == Enumeradores.PerfilTipo.Analista || _user.PerfilTipo == Enumeradores.PerfilTipo.RegionalAdmin))
                {
                    return clienteAtendimento;
                }
                else if (clienteAtendimento != null)
                {
                    //CASO O CLIENTE JA POSSUI UM ATENDIMENTO DEVE APENAS ATUALIZAR O REGISTRO
                    if (clienteAtendimento.CodUsuario == _atendente.Cod && _user.Cod == _atendente.Cod)
                    {
                        clienteAtendimento.DataAlteracao = DateTime.Now;
                        return atendimento.Update(clienteAtendimento);
                    }
                }

            }
            return null;


        }







    }
}