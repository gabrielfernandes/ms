﻿using Cobranca.Db.Rotinas;
using Cobranca.Domain.BoletoContext.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Cobranca.Web.Models
{
    public class ClienteHeaderViewModel
    {
        public string CodCliente { get; set; }
        public string  Cliente { get; set; }
        public string CpfCnpj { get; set; }
        public string NumeroContrato { get; set; }
        public string Banco { get; set; }
        public string NumeroDocumento { get; set; }
        public Nullable<System.DateTime> DataVencimento { get; set; }
        public Nullable<System.Decimal> ValorParcela { get; set; }
        public Nullable<System.Decimal> ValorAberto { get; set; }
        public string TipoFatura { get; set; }
        public Nullable<int> NossoNumero { get; set; }
        public string CodigoSinteseCliente { get; set; }
        public string StatusContrato { get; set; }
        public string Produto { get; set; }
        public decimal? ValorTotalContrato { get; set; }
        public decimal? ValorAtrasoContrato { get; set; }
        public decimal? ValorAVencerContrato { get; set; }
        public decimal? DiaAtrasoContrato { get; set; }
        public string Regional { get; set; }

        public short? TipoSolicitacao { get; set; }
    }
}