﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Cobranca.Web.Models
{
    public class BoletoAvulsoViewModel
    {
        public BoletoAvulsoViewModel()
        {
            ListaBanco = new List<DictionaryEntry>();
        }
        public int ClienteId { get; set; }
        public string NumeroDocumento { get; set; }
        public string NumeroBanco { get; set; }
        public Nullable<DateTime> DataVencimento { get; set; }
        public Nullable<decimal> ValorDocumento { get; set; }
        public string Justificativa { get; set; }
        public string MensagemImpressa { get; set; }



        public string CodCliente { get; set; }
        public string NomeCliente { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public Nullable<decimal> ValorAtraso { get; set; }
        public Nullable<int> QtdeContrato { get; set; }
        public Nullable<int> DiasAtraso { get; set; }

        public List<DictionaryEntry> ListaBanco { get; set; }
    }
}