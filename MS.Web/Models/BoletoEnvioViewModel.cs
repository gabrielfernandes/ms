﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class BoletoEnvioViewModel
    {
        public BoletoEnvioViewModel()
        {
            BoletoHTML = new List<string>();
            ListaModelosEmail = new List<DictionaryEntry>();
        }
        public string ParcelaIds { get; set; }
        public Nullable<int> CodCliente { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public System.DateTime DataCadastro { get; set; }
        public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<System.DateTime> DataExcluido { get; set; }
        public List<DictionaryEntry> ListaModelosEmail { get; set; }
        public int? TipoModeloEmail { get; set; }

        public List<string> BoletoHTML { get; set; }
        public string Email { get; set; }
        public string Assunto { get; set; }

        public string Mensagem { get; set; }
    }
}
