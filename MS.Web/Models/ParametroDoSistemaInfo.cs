﻿using Cobranca.Db.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cobranca.Web.Models
{
    public class ParametroDoSistemaInfo
    {

        public ParametroDoSistemaInfo()
        {
            LayoutSistema = Enumeradores.LayoutSistema.Padrao;
            LogoEmpresa = string.Empty;
            Icone = string.Empty;
        }

        public Enumeradores.LayoutSistema LayoutSistema { get; set; }
        public string LogoEmpresa { get; set; }

        public string Icone { get; set; }
    }
}