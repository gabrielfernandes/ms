﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Rotinas.BoletoGerar;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.Mvc;

namespace Cobranca.Web.Suporte
{
    /// <summary>
    /// Summary description for DownloadSeguro
    /// </summary>
    public class GerarBoleto : IHttpHandler
    {
        public Repository<ContratoParcela> _dbContratoParcela = new Repository<ContratoParcela>();
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<TabelaBancoPortador> _dbParametroBoleto = new Repository<TabelaBancoPortador>();

        public void ProcessRequest(HttpContext context)
        {

            string cd = context.Request.QueryString["CodParcela"];
            int codigo = 0;
            int.TryParse(cd, out codigo);
            //GerarBoleto(codigo,  context);
            //context.Response.ContentType = "text/plain";
            //context.Response.Write(cd);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        //public void GerarBoleto(int CodParcela,  HttpContext context)
        //{
        //    try
        //    {
        //        var parcela = _dbContratoParcela.FindById(CodParcela);
        //        var contrato = _dbContrato.FindById(parcela.CodContrato.Value);
        //        var sacador = _dbCliente.FindById(contrato.CodClientePrincipal.Value);
        //        var cedente = _dbParametroBoleto.All().FirstOrDefault();

        //        GerarBoleto boleto = new GerarBoleto();
        //        BoletoInfo boletoInfo = new BoletoInfo();

        //        boletoInfo.SacadoNome = sacador.Nome;
        //        boletoInfo.SacadoCPF = sacador.CPF;
        //        boletoInfo.SacadoEndereco = sacador.Endereco;
        //        boletoInfo.SacadoCEP = sacador.CEP;
        //        boletoInfo.SacadoBairro = sacador.Bairro;
        //        boletoInfo.SacadoCidade = sacador.Cidade;
        //        boletoInfo.SacadoUF = sacador.UF;

        //        boletoInfo.CedenteAgencia = cedente.Agencia;
        //        boletoInfo.CedenteDigitoAgencia = cedente.AgenciaDigito;
        //        boletoInfo.CedenteConta = cedente.Conta;
        //        boletoInfo.CedenteDigitoConta = cedente.ContaDigito;
        //        boletoInfo.CedenteNome = cedente.Portador;
        //        boletoInfo.CedenteCPF = cedente.CNPJ;
        //        boletoInfo.CedenteCodigo = cedente.CodigoCedente;
        //        boletoInfo.CedenteNumeroBoleto = parcela.FluxoPagamento;//VERIFICAR

        //        boletoInfo.ValorBoleto = Convert.ToString(contrato.ContratoParcelas.FirstOrDefault().ValorParcela.Value);
        //        boletoInfo.DataVencimento = Convert.ToString(contrato.ContratoParcelas.FirstOrDefault().DataVencimento);

        //        boletoInfo.DescricaoBoleto = "Não aceitar valor após a data de vencimento."; //VERIFICAR
        //        boletoInfo.NumeroDocumento = parcela.FluxoPagamento; //VERIFICAR
        //        boletoInfo.NumeroBanco = cedente.CodBanco.HasValue ? (short)cedente.TabelaBanco.Numero.Value : (short)0;
        //        boletoInfo.Carteira = "109"; //VERIFICAR

        //        //var html = boleto.GerandoBoleto(boletoInfo).MontaHtmlEmbedded();

        //        ConverterHTMLParaPDF(html ,  context);
        //    }
        //    catch (Exception ex)
        //    {


        //    }
        //}

        //public void ConverterHTMLParaPDF(string HTML, HttpContext context)
        //{
        //    Document document = new Document();
        //    string FilePath = "C:\\teste.pdf";
        //    PdfWriter.GetInstance(document, new FileStream(FilePath, FileMode.Create));
        //    document.Open();

        //    iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();

        //    iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document);

        //    hw.Parse(new StringReader(HTML));
        //    document.Close();

        //    context.Response.ClearContent();
        //    context.Response.ClearHeaders();
        //    context.Response.AddHeader("Content-Disposition", "inline;filename=" + s);
        //    context.Response.ContentType = "application/pdf";
        //    context.Response.WriteFile(FilePath);
        //    context.Response.Flush();
        //    context.Response.Clear();

        //}
   

    }
}