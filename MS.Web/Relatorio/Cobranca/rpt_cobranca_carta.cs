﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraRichEdit;

namespace Cobranca.Web.Views.Relatorio
{
    public partial class rpt_cobranca_carta : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_cobranca_carta()
        {
            InitializeComponent();
        }


        private void xrRichText1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.xrRichText1.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        }

        
    }
}
