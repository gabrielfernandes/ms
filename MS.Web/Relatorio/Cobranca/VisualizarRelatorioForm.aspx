﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VisualizarRelatorioForm.aspx.cs" Inherits="Cobranca.Web.Relatorio.VisualizarRelatorioForm" %>

<%@ Register Assembly="DevExpress.XtraReports.v13.2.Web, Version=13.2.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>

<body>
    <form id="form1" runat="server" style="height: 100%">
        <div>
            <dx:ASPxDocumentViewer ID="reportViewer" runat="server" ClientInstanceName="reportViewer" Height="450px">
                <StylesViewer>
                    <BookmarkSelectionBorder BorderColor="Gray" BorderStyle="Dashed" BorderWidth="3px"></BookmarkSelectionBorder>
                </StylesViewer>

                <StylesSplitter>
                    <Pane>
                        <Paddings Padding="16px"></Paddings>
                    </Pane>
                </StylesSplitter>
            </dx:ASPxDocumentViewer>
        </div>
    </form>
</body>
</html>
