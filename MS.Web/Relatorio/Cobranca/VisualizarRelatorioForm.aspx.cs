﻿using Cobranca.Db.Rotinas;
using Cobranca.Web.Views.Relatorio;
using System;
using Cobranca.Web.Rotinas;
using System.ComponentModel;
using System.Reflection;
using System.Data.SqlClient;
using Cobranca.Web.DataSet.Cobranca;
using Cobranca.Web.DataSet.Cobranca.DataSet_CobrancaTableAdapters;
using Cobranca.Web.Models;
using System.Data;
using Cobranca.Db.Models;
using Cobranca.Domain.BoletoContext.Enums;

namespace Cobranca.Web.Relatorio
{
    public partial class VisualizarRelatorioForm : System.Web.UI.Page
    {

        public const string _filtroSessionId = "FiltroRelatorio";

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            if (WebRotinas.UsuarioAutenticado)
            {
                RelatorioPesquisaViewModel filtro = obterFiltros();

                switch (filtro.ReportCod)
                {
                    case (short)Enumeradores.TipoRelatorio.ReguaParcelaSemAcoes:
                        {
                            ExecutarRelatorioReguaSemAcoes(filtro);
                            break;
                        }
                    case (short)Enumeradores.TipoRelatorio.AtendimentoAgendados:
                        {
                            ExecutarRelatorioAtendimentoAgendado(filtro);
                            break;
                        }
                    case (short)Enumeradores.TipoRelatorio.Atendimento:
                        {
                            ExecutarRelatorioAtendimento(filtro);
                            break;
                        }
                    case (short)Enumeradores.TipoRelatorio.ParcelaSemAtendimento:
                        {
                            ExecutarRelatorioParcelaSemAtendimento(filtro);
                            break;
                        }
                    case (short)Enumeradores.TipoRelatorio.AgingFilial:
                        {
                            ExecutarRelatorioAgingFilial(filtro);
                            break;
                        }
                    case (short)Enumeradores.TipoRelatorio.GestaoBoleto:
                        {
                            ExecutarRelatorioGestaoBoleto(filtro);
                            break;
                        }
                    case (short)Enumeradores.TipoRelatorio.GestaoCarta:
                        {
                            ExecutarRelatorioGestaoCarta(filtro);
                            break;
                        }
                    case (short)Enumeradores.TipoRelatorio.GestaoFilial:
                        {
                            ExecutarRelatorioGestaoFilial(filtro);
                            break;
                        }
                    default:
                        {
                            Context.Response.Write($"<span>!</span>");
                            break;
                        }
                }
            }
            else
            {
                Response.Redirect("~/Login/Index");
            }

        }
        // }

        private RelatorioPesquisaViewModel obterFiltros()
        {
            WebRotinas.CookieObjetoLer<RelatorioPesquisaViewModel>(_filtroSessionId);
            RelatorioPesquisaViewModel filtro = new RelatorioPesquisaViewModel();
            filtro.ReportCod = DbRotinas.ConverterParaShort(Request.QueryString["ReportCod"]);
            filtro.codigoRelatorio = Request.QueryString["codigoRelatorio"];
            filtro.CodUsuarioAtendente = DbRotinas.ConverterParaInt(Request.QueryString["CodUsuarioAtendente"]);
            filtro.CodRegional = DbRotinas.ConverterParaInt(Request.QueryString["CodRegional"]);
            filtro.AN8 = string.IsNullOrEmpty(Request.QueryString["AN8"]) ? null : DbRotinas.RemoverCaracterEspeciais(DbRotinas.Descriptografar(Request.QueryString["AN8"]));
            filtro.CNPJ = string.IsNullOrEmpty(Request.QueryString["CNPJ"]) ? null : DbRotinas.Descriptografar(DbRotinas.RemoverCaracterEspeciais(Request.QueryString["CNPJ"].Replace(".", "").Replace("/", "").Replace("-", "")));
            filtro.Contrato = string.IsNullOrEmpty(Request.QueryString["Contrato"]) ? null : DbRotinas.Descriptografar(Request.QueryString["Contrato"]);
            filtro.ValorAbertoDe = DbRotinas.ConverterParaDecimal(Request.QueryString["ValorAbertoDe"]);
            filtro.ValorAbertoAte = DbRotinas.ConverterParaDecimal(Request.QueryString["ValorAbertoAte"]);
            filtro.DataAgendamentoDe = DbRotinas.ConverterParaDatetime(Request.QueryString["PeriodoDe"]);
            filtro.DataAgendamentoAte = DbRotinas.ConverterParaDatetime(Request.QueryString["PeriodoAte"]);
            filtro.DataPeriodoDe = DbRotinas.ConverterParaDatetime(Request.QueryString["PeriodoDe"]);
            filtro.DataPeriodoAte = DbRotinas.ConverterParaDatetime(Request.QueryString["PeriodoAte"]);
            filtro.ContratoId = DbRotinas.ConverterParaInt(Request.QueryString["ContratoId"]);
            filtro.ClienteId = DbRotinas.ConverterParaInt(Request.QueryString["ClienteId"]);
            filtro.Aberto = DbRotinas.ConverterParaBool(Request.QueryString["Aberto"] == "on" ? true : false);
            filtro.Vencido = DbRotinas.ConverterParaBool(Request.QueryString["Vencido"] == "on" ? true : false);
            filtro.Pago = DbRotinas.ConverterParaBool(Request.QueryString["Pago"] == "on" ? true : false);
            filtro.AtendenteIds = string.IsNullOrEmpty(Request.QueryString["usuarioIds"]) ? null : Request.QueryString["usuarioIds"];
            filtro.Regionais = string.IsNullOrEmpty(Request.QueryString["regionalIds"]) ? null : Request.QueryString["regionalIds"];
            filtro.Solicitadores = string.IsNullOrEmpty(Request.QueryString["usuarioSolicitaIds"]) ? null : Request.QueryString["usuarioSolicitaIds"];
            filtro.Aprovadores = string.IsNullOrEmpty(Request.QueryString["usuarioAprovaIds"]) ? null : Request.QueryString["usuarioAprovaIds"];
            filtro.SolicitacaoTipo = string.IsNullOrEmpty(Request.QueryString["solicitacaoTipoIds"]) ? null : Request.QueryString["solicitacaoTipoIds"];
            filtro.StatusBoleto = string.IsNullOrEmpty(Request.QueryString["statusBoletoIds"]) ? null : Request.QueryString["statusBoletoIds"];
            filtro.StatusBoleto = string.IsNullOrEmpty(Request.QueryString["statusBoletoIds"]) ? null : Request.QueryString["statusBoletoIds"];
            filtro.TipoPeriodo = DbRotinas.ConverterParaInt(Request.QueryString["TipoPeriodo"]);

            if (Request.QueryString["CondicaoQuery"] != null)
            {
                filtro.CondicaoQuery = Request.QueryString["CondicaoQuery"].Replace("IN('", "IN(").Replace("')", ")");
            }

            return filtro;
        }

        #region METODOS CHAMADA DO RELATÓRIO

        private void ExecutarRelatorioReguaSemAcoes(RelatorioPesquisaViewModel filtro)
        {
            rpt_cobranca_regua_sem_acoes Report = new rpt_cobranca_regua_sem_acoes();
            DataSet_Cobranca ds = new DataSet_Cobranca();
            SP_RELATORIO_REGUA_PARCELAS_SEM_ACOESTableAdapter tableAdapter = new SP_RELATORIO_REGUA_PARCELAS_SEM_ACOESTableAdapter();

            tableAdapter.Fill(ds.SP_RELATORIO_REGUA_PARCELAS_SEM_ACOES
                , null
                , filtro.CodContrutoras
                , filtro.CodRegionais
                , filtro.CodProdutos
                , filtro.CodRotas
                , filtro.CodAgins
                , filtro.DataPeriodoDe
                , filtro.DataPeriodoAte
                , filtro.CodUsuario
                , filtro.PerfilTipo
                );

            Report.DataSource = ds;
            Report.CreateDocument();
            reportViewer.Report = Report;
        }

        private void ExecutarRelatorioAtendimentoAgendado(RelatorioPesquisaViewModel filtro)
        {
            rpt_cobranca_atendimento_agendados Report = new rpt_cobranca_atendimento_agendados();
            DataSet_Cobranca ds = new DataSet_Cobranca();
            SP_RELATORIO_ATENDIMENTO_AGENDADOSTableAdapter tableAdapter = new SP_RELATORIO_ATENDIMENTO_AGENDADOSTableAdapter();

            tableAdapter.Fill(ds.SP_RELATORIO_ATENDIMENTO_AGENDADOS
                , filtro.AtendenteIds
                , filtro.AN8
                , filtro.CNPJ
                , filtro.Contrato
                , filtro.DataPeriodoDe == DateTime.MinValue ? null : filtro.DataAgendamentoDe
                , filtro.DataPeriodoAte == DateTime.MinValue ? null : filtro.DataAgendamentoAte
                );

            foreach (DataTable tables in ds.Tables)
            {
                if (tables.TableName == "SP_RELATORIO_ATENDIMENTO_AGENDADOS")
                {
                    foreach (DataRow r in tables.Rows)
                    {
                        r["CodCliente"] = DbRotinas.Descriptografar(r["CodCliente"].ToString());
                        r["Cliente"] = DbRotinas.Descriptografar(r["Cliente"].ToString());
                        r["CpfCnpj"] = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(r["CpfCnpj"].ToString()));
                        r["NumeroContrato"] = DbRotinas.Descriptografar(r["NumeroContrato"].ToString());
                        r["Sintese"] = DbRotinas.Descriptografar(r["Sintese"].ToString());
                    }
                }
            }
            Report.DataSource = ds;

            Report.CreateDocument();
            reportViewer.Report = Report;
        }

        private void ExecutarRelatorioGestaoBoleto(RelatorioPesquisaViewModel filtro)
        {
            rpt_cobranca_boleto_detalhado Report = new rpt_cobranca_boleto_detalhado();
            DataSet_Cobranca ds = new DataSet_Cobranca();
            spReportBoletoDetalhadoTableAdapter tableAdapter = new spReportBoletoDetalhadoTableAdapter();
            ChangeTimeout(Report, 7200);

            tableAdapter.Fill(ds.spReportBoletoDetalhado
                , filtro.Regionais
                , filtro.Solicitadores
                , filtro.Aprovadores
                , filtro.Contrato
                , filtro.ClienteId
                , filtro.NumeroDocumento
                , filtro.SolicitacaoTipo
                , filtro.StatusBoleto
                , filtro.TipoPeriodo
                , filtro.DataPeriodoDe == DateTime.MinValue ? null : filtro.DataPeriodoDe
                , filtro.DataPeriodoAte == DateTime.MinValue ? null : filtro.DataPeriodoAte               
                );


            foreach (DataTable tables in ds.Tables)
            {
                if (tables.TableName == "spReportBoletoDetalhado")
                {
                    foreach (DataRow r in tables.Rows)
                    {
                        r["Regional"] = DbRotinas.Descriptografar(r["Regional"].ToString());
                        r["CodigoCliente"] = DbRotinas.Descriptografar(r["CodigoCliente"].ToString());
                        r["Cliente"] = DbRotinas.Descriptografar(r["Cliente"].ToString());
                        r["NumeroContrato"] = DbRotinas.Descriptografar(r["NumeroContrato"].ToString());
                    }
                }
            }
            Report.DataSource = ds;

            Report.CreateDocument();
            reportViewer.Report = Report;
        }

        private void ExecutarRelatorioGestaoCarta(RelatorioPesquisaViewModel filtro)
        {
            rpt_cobranca_carta Report = new rpt_cobranca_carta();
            DataSet_Cobranca ds = new DataSet_Cobranca();
            spReportCobrancaCartaTableAdapter tableAdapter = new spReportCobrancaCartaTableAdapter();
            ChangeTimeout(Report, 7200);

            tableAdapter.Fill(ds.spReportCobrancaCarta
              , filtro.Regionais
              , filtro.AtendenteIds
              , filtro.Contrato
              , filtro.ClienteId
              , filtro.NumeroDocumento
              , filtro.DataPeriodoDe == DateTime.MinValue ? null : filtro.DataPeriodoDe
              , filtro.DataPeriodoAte == DateTime.MinValue ? null : filtro.DataPeriodoAte
              );

            foreach (DataTable tables in ds.Tables)
            {
                if (tables.TableName == "spReportCobrancaCarta")
                {
                    foreach (DataRow r in tables.Rows)
                    {
                        r["Regional"] = DbRotinas.Descriptografar(r["Regional"].ToString());
                        r["ClienteNome"] = DbRotinas.Descriptografar(r["ClienteNome"].ToString());
                        r["NumeroContrato"] = DbRotinas.Descriptografar(r["NumeroContrato"].ToString());
                        r["ClienteDocumento"] = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(r["ClienteDocumento"].ToString()));
                        r["Email"] = DbRotinas.Descriptografar(r["Email"].ToString());
                    }
                }
            }
            Report.DataSource = ds;

            Report.CreateDocument();
            reportViewer.Report = Report;
        }


        private void ExecutarRelatorioGestaoFilial(RelatorioPesquisaViewModel filtro)
        {
            rpt_cobranca_filial_detalhado Report = new rpt_cobranca_filial_detalhado();
            DataSet_Cobranca ds = new DataSet_Cobranca();
            spReportCobrancaFilialDetalhadoTableAdapter tableAdapter = new spReportCobrancaFilialDetalhadoTableAdapter();
            ChangeTimeout(Report, 7200);

            tableAdapter.Fill(ds.spReportCobrancaFilialDetalhado
               , filtro.Regionais
               , filtro.Contrato
               , filtro.ClienteId
               , filtro.NumeroDocumento
               , filtro.DataPeriodoDe == DateTime.MinValue ? null : filtro.DataPeriodoDe
               , filtro.DataPeriodoAte == DateTime.MinValue ? null : filtro.DataPeriodoAte
               , filtro.Aberto
               , filtro.Vencido
               , filtro.Pago
               );


            foreach (DataTable tables in ds.Tables)
            {
                if (tables.TableName == "spReportCobrancaFilialDetalhado")
                {
                    foreach (DataRow r in tables.Rows)
                    {
                        r["Regional"] = DbRotinas.Descriptografar(r["Regional"].ToString());
                        r["NomeCliente"] = DbRotinas.Descriptografar(r["NomeCliente"].ToString());
                        r["CPF"] = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(r["CPF"].ToString()));
                        r["NumeroContrato"] = DbRotinas.Descriptografar(r["NumeroContrato"].ToString());
                        r["Produto"] = DbRotinas.Descriptografar(r["Produto"].ToString());
                        r["Rota"] = DbRotinas.Descriptografar(r["Rota"].ToString());
                        r["Sintese"] = DbRotinas.Descriptografar(r["Sintese"].ToString());
                    }
                }
            }
            Report.DataSource = ds;

            Report.CreateDocument();
            reportViewer.Report = Report;
        }

        private void ExecutarRelatorioAtendimento(RelatorioPesquisaViewModel filtro)
        {
            rpt_cobranca_atendimento Report = new rpt_cobranca_atendimento();
            DataSet_Cobranca ds = new DataSet_Cobranca();
            SP_RELATORIO_ATENDIMENTOTableAdapter tableAdapter = new SP_RELATORIO_ATENDIMENTOTableAdapter();
            ChangeTimeout(Report, 7200);

            tableAdapter.Fill(ds.SP_RELATORIO_ATENDIMENTO
                , filtro.AtendenteIds
                , filtro.AN8
                , filtro.CNPJ
                , filtro.Contrato
                , filtro.DataPeriodoDe == DateTime.MinValue ? null : filtro.DataAgendamentoDe
                , filtro.DataPeriodoAte == DateTime.MinValue ? null : filtro.DataAgendamentoAte
                );

            foreach (DataTable tables in ds.Tables)
            {
                if (tables.TableName == "SP_RELATORIO_ATENDIMENTO")
                {
                    foreach (DataRow r in tables.Rows)
                    {
                        //r["Regional"] = DbRotinas.Descriptografar(r["Regional"].ToString());
                        r["CodCliente"] = DbRotinas.Descriptografar(r["CodCliente"].ToString());
                        r["Cliente"] = DbRotinas.Descriptografar(r["Cliente"].ToString());
                        r["CpfCnpj"] = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(r["CpfCnpj"].ToString()));
                        r["NumeroContrato"] = DbRotinas.Descriptografar(r["NumeroContrato"].ToString());
                        r["Sintese"] = DbRotinas.Descriptografar(r["Sintese"].ToString());
                    }
                }
            }
            Report.DataSource = ds;

            Report.Name = $"Controle de atendimento";
            Report.CreateDocument();
            reportViewer.Report = Report;
        }

        private void ExecutarRelatorioParcelaSemAtendimento(RelatorioPesquisaViewModel filtro)
        {
            rpt_cobranca_sem_atendimento Report = new rpt_cobranca_sem_atendimento();
            DataSet_Cobranca ds = new DataSet_Cobranca();
            SP_RELATORIO_PARCELAS_SEM_ATENDIMENTOTableAdapter tableAdapter = new SP_RELATORIO_PARCELAS_SEM_ATENDIMENTOTableAdapter();
            ChangeTimeout(Report, 7200);

            tableAdapter.Fill(ds.SP_RELATORIO_PARCELAS_SEM_ATENDIMENTO
                , filtro.CodRegional
                , filtro.AN8
                , filtro.CNPJ
                , filtro.Contrato
                , filtro.DataPeriodoDe == DateTime.MinValue ? null : DbRotinas.ConverterParaString(filtro.DataAgendamentoDe)
                , filtro.DataPeriodoAte == DateTime.MinValue ? null : DbRotinas.ConverterParaString(filtro.DataAgendamentoAte)
                , filtro.CondicaoQuery
                );

            foreach (DataTable tables in ds.Tables)
            {
                if (tables.TableName == "SP_RELATORIO_PARCELAS_SEM_ATENDIMENTO")
                {
                    foreach (DataRow r in tables.Rows)
                    {
                        r["CodCliente"] = DbRotinas.Descriptografar(r["CodCliente"].ToString());
                        r["Cliente"] = DbRotinas.Descriptografar(r["Cliente"].ToString());
                        r["CPF"] = DbRotinas.formatarCpfCnpj(DbRotinas.Descriptografar(r["CPF"].ToString()));
                        r["NumeroContrato"] = DbRotinas.Descriptografar(r["NumeroContrato"].ToString());
                        r["Sintese"] = DbRotinas.Descriptografar(r["Sintese"].ToString());
                        r["Produto"] = DbRotinas.Descriptografar(r["Produto"].ToString());
                        r["Rota"] = DbRotinas.Descriptografar(r["Rota"].ToString());
                        r["Regional"] = DbRotinas.Descriptografar(r["Regional"].ToString());
                    }
                }
            }
            Report.DataSource = ds;

            Report.Name = $"Controle de parcelas sem atendimento";
            Report.CreateDocument();
            reportViewer.Report = Report;
        }

        private void ExecutarRelatorioAgingFilial(RelatorioPesquisaViewModel filtro)
        {
            rpt_partial_aging_filial Report = new rpt_partial_aging_filial();
            DataSet_Cobranca ds = new DataSet_Cobranca();
            SP_RELATORIO_AGING_POR_FILIALTableAdapter tableAdapter = new SP_RELATORIO_AGING_POR_FILIALTableAdapter();
            ChangeTimeout(Report, 7200);

            tableAdapter.Fill(ds.SP_RELATORIO_AGING_POR_FILIAL
                , filtro.CodRegional
                , filtro.AN8
                , filtro.CNPJ
                , filtro.Contrato
                , filtro.DataPeriodoDe == DateTime.MinValue ? null : DbRotinas.ConverterParaString(filtro.DataAgendamentoDe)
                , filtro.DataPeriodoAte == DateTime.MinValue ? null : DbRotinas.ConverterParaString(filtro.DataAgendamentoAte)
                , filtro.CondicaoQuery
                );

            foreach (DataTable tables in ds.Tables)
            {
                if (tables.TableName == "SP_RELATORIO_AGING_POR_FILIAL")
                {
                    foreach (DataRow r in tables.Rows)
                    {
                        r["Regional"] = DbRotinas.Descriptografar(r["Regional"].ToString());
                    }
                }
            }
            Report.DataSource = ds;

            Report.Name = $"Aging de Atraso";
            Report.CreateDocument();
            reportViewer.Report = Report;
        }
        #endregion


        private void ChangeTimeout(Component component, int timeout)
        {
            if (!component.GetType().Name.Contains("TableAdapter"))
            {
                return;
            }

            PropertyInfo adapterProp = component.GetType().GetProperty("CommandCollection", BindingFlags.NonPublic | BindingFlags.GetProperty | BindingFlags.Instance);
            if (adapterProp == null)
            {
                return;
            }

            SqlCommand[] command = adapterProp.GetValue(component, null) as SqlCommand[];

            if (command == null)
            {
                return;
            }

            command[0].CommandTimeout = timeout;
        }


    }
}