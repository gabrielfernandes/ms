﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace Cobranca.Web.Upload
{
    public class FilesStatus
    {
        public const string HandlerPath = "/Upload/";

        public string group { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public string progress { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }

        public FilesStatus() { }

        public FilesStatus(FileInfo fileInfo, bool getDimensions, string mimeType)
        {
            SetValues(fileInfo.Name, (int)fileInfo.Length, fileInfo.FullName, getDimensions, mimeType);
        }

        public FilesStatus(string fileName, int fileLength, string fullPath, bool getDimensions, string mimeType)
        {
            SetValues(fileName, fileLength, fullPath, getDimensions, mimeType);
        }

        private void SetValues(string fileName, int fileLength, string fullPath, bool getDimensions, string mimeType)
        {
            var isImage = false;
            var ext = Path.GetExtension(fullPath).ToLower();

            switch (ext)
            {
                case ".jpg":
                case ".jpeg":
                case ".png":
                case ".gif":
                    isImage = true;
                    break;
            }

            name = fileName;
            type = mimeType;
            size = fileLength;
            progress = "1.0";
            url = HandlerPath + "UploadHandler.ashx?f=" + fileName;
            delete_url = HandlerPath + "UploadHandler.ashx?f=" + fileName;
            delete_type = "DELETE";

            if (isImage)
            {
                using (FileStream stream = new FileStream(fullPath, FileMode.Open, FileAccess.Read))
                {
                    var fileSize = ConvertBytesToMegabytes(stream.Length);
                    if (fileSize > 3 || !IsImage(ext)) thumbnail_url = "/Content/img/generalFile.png";
                    else thumbnail_url = @"data:image/png;base64," + EncodeFile(stream);
                }
            }
        }

        private bool IsImage(string ext)
        {
            return ext == ".gif" || ext == ".jpg" || ext == ".png";
        }

        private string EncodeFile(Stream stream)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public static void VerifyDirectory(string dir)
        {
            if (!String.IsNullOrEmpty(Path.GetExtension(dir)))
                dir = Path.GetDirectoryName(dir);

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }
    }    
}