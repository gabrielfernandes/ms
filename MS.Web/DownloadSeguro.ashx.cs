﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recebiveis.Web
{
    /// <summary>
    /// Summary description for DownloadSeguro
    /// </summary>
    public class DownloadSeguro : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            string cd = context.Request.QueryString["cd"];
            context.Response.ContentType = "text/plain";
            context.Response.Write(cd);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}