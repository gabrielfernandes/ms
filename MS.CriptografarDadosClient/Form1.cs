﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Otis.Cobflow.CriptografarDadosClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Ofuscar();
        }

        private void Ofuscar()
        {

            escreverLOG("Processo iniciado ...");

            using (junix_cobrancaContext cx = new junix_cobrancaContext())
            {
                //var tiposCobranca  = cx.TabelaTipoCobrancas.Where(x => !x.DataExcluido.HasValue);
                //var fases = cx.TabelaFases.Where(x => !x.DataExcluido.HasValue);
                //var sinteses = cx.TabelaSintese.Where(x => !x.DataExcluido.HasValue);
                //var clientes = cx.Clientes.Where(x => !x.DataExcluido.HasValue);
                //var contatos = cx.ClienteContatoes.Where(x => !x.DataExcluido.HasValue);
                //var contratos = cx.Contratoes.Where(x => !x.DataExcluido.HasValue);
                //var motivoAtraso = cx.TabelaMotivoAtrasoes.Where(x => !x.DataExcluido.HasValue);
                var regionais = cx.TabelaRegionals.Where(x => !x.DataExcluido.HasValue && x.Cod > 8);
                //var produtos = cx.TabelaProdutoes.Where(x => !x.DataExcluido.HasValue);
                //var construtoras = cx.TabelaConstrutoras.Where(x => !x.DataExcluido.HasValue);
                //var usuarios = cx.Usuarios.Where(x => !x.DataExcluido.HasValue);

                //foreach (var item in construtoras)
                //{
                //    item.NomeConstrutora = DbRotinas.Criptografar(item.NomeConstrutora);
                //    item.CNPJ = DbRotinas.Criptografar(item.CNPJ);
                //}
                //escreverLOG("Preparando os Empresas ...");
                foreach (var item in regionais)
                {
                    item.Complemento = DbRotinas.Criptografar(item.Regional);
                }
                //escreverLOG("Preparando as Filiais ...");
                //foreach (var item in produtos)
                //{
                //    item.Produto = DbRotinas.Criptografar(item.Produto);
                //    item.Descricao = DbRotinas.Criptografar(item.Descricao);
                //
                //}
                //escreverLOG("Preparando os Produtos ...");
                //foreach (var item in tiposCobranca)
                //{
                //    item.TipoCobranca = DbRotinas.Criptografar(item.TipoCobranca);
                //}
                //escreverLOG("Preparando os Tipo de Cobrança ...");
                //foreach (var item in fases)
                //{
                //    item.Fase = DbRotinas.Criptografar(item.Fase);
                //}
                //escreverLOG("Preparando as Fase ...");
                //foreach (var item in sinteses)
                //{
                //    item.Sintese = DbRotinas.Criptografar(item.Sintese);
                //}
                //escreverLOG("Preparando as Sintese ...");
                //foreach (var item in clientes)
                //{
                //    item.CodCliente = DbRotinas.Criptografar(item.CodCliente);
                //    item.Nome = DbRotinas.Criptografar(item.Nome);
                //    item.CPF = DbRotinas.Criptografar(item.CPF);
                //    item.Email = DbRotinas.Criptografar(item.Email);
                //    item.TelefoneComercial = DbRotinas.Criptografar(item.TelefoneComercial);
                //    item.TelefoneResidencial = DbRotinas.Criptografar(item.TelefoneResidencial);
                //    item.Celular = DbRotinas.Criptografar(item.Celular);
                //    item.CelularComercial = DbRotinas.Criptografar(item.CelularComercial);
                //    item.CelularPrincipal = DbRotinas.Criptografar(item.CelularPrincipal);
                //}
                //escreverLOG("Preparando os Cliente ...");
                //foreach (var item in contatos)
                //{
                //    item.Contato = DbRotinas.Criptografar(item.Contato);
                //}
                //
                //foreach (var item in contratos)
                //{
                //    item.NumeroContrato = DbRotinas.Criptografar(item.NumeroContrato);
                //}
                //escreverLOG("Preparando os Contratos ...");
                //foreach (var item in motivoAtraso)
                //{
                //    item.MotivoAtraso = DbRotinas.Criptografar(item.MotivoAtraso);
                //}
                //escreverLOG("Preparando os Usuarios ...");
                //foreach (var item in usuarios)
                //{
                //    item.Senha = DbRotinas.Criptografar(item.Senha);
                //}
                cx.SaveChanges();

            }
            escreverLOG("Processo Concluído ...");

        }

        private void escreverLOG(string v)
        {            
            richTextBox_LOG.AppendText(v);
            richTextBox_LOG.AppendText(System.Environment.NewLine);
        }
    }
}
