﻿namespace Otis.Cobflow.CriptografarDadosClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox_tbTipoCobranca = new System.Windows.Forms.CheckBox();
            this.checkBox_tbFase = new System.Windows.Forms.CheckBox();
            this.checkBox_tbSintese = new System.Windows.Forms.CheckBox();
            this.richTextBox_LOG = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 45);
            this.button1.TabIndex = 0;
            this.button1.Text = "REGIONAL";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox_tbTipoCobranca
            // 
            this.checkBox_tbTipoCobranca.AutoSize = true;
            this.checkBox_tbTipoCobranca.Location = new System.Drawing.Point(182, 13);
            this.checkBox_tbTipoCobranca.Name = "checkBox_tbTipoCobranca";
            this.checkBox_tbTipoCobranca.Size = new System.Drawing.Size(131, 17);
            this.checkBox_tbTipoCobranca.TabIndex = 1;
            this.checkBox_tbTipoCobranca.Text = "TIPO DE COBRANCA";
            this.checkBox_tbTipoCobranca.UseVisualStyleBackColor = true;
            // 
            // checkBox_tbFase
            // 
            this.checkBox_tbFase.AutoSize = true;
            this.checkBox_tbFase.Location = new System.Drawing.Point(182, 36);
            this.checkBox_tbFase.Name = "checkBox_tbFase";
            this.checkBox_tbFase.Size = new System.Drawing.Size(53, 17);
            this.checkBox_tbFase.TabIndex = 1;
            this.checkBox_tbFase.Text = "FASE";
            this.checkBox_tbFase.UseVisualStyleBackColor = true;
            // 
            // checkBox_tbSintese
            // 
            this.checkBox_tbSintese.AutoSize = true;
            this.checkBox_tbSintese.Location = new System.Drawing.Point(182, 59);
            this.checkBox_tbSintese.Name = "checkBox_tbSintese";
            this.checkBox_tbSintese.Size = new System.Drawing.Size(72, 17);
            this.checkBox_tbSintese.TabIndex = 1;
            this.checkBox_tbSintese.Text = "SINTESE";
            this.checkBox_tbSintese.UseVisualStyleBackColor = true;
            // 
            // richTextBox_LOG
            // 
            this.richTextBox_LOG.Location = new System.Drawing.Point(13, 137);
            this.richTextBox_LOG.Name = "richTextBox_LOG";
            this.richTextBox_LOG.Size = new System.Drawing.Size(349, 323);
            this.richTextBox_LOG.TabIndex = 2;
            this.richTextBox_LOG.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 472);
            this.Controls.Add(this.richTextBox_LOG);
            this.Controls.Add(this.checkBox_tbSintese);
            this.Controls.Add(this.checkBox_tbFase);
            this.Controls.Add(this.checkBox_tbTipoCobranca);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox_tbTipoCobranca;
        private System.Windows.Forms.CheckBox checkBox_tbFase;
        private System.Windows.Forms.CheckBox checkBox_tbSintese;
        private System.Windows.Forms.RichTextBox richTextBox_LOG;
    }
}

