﻿using Cobranca.Helpers;
using System;

namespace Cobranca.Domain.CobrancaContext.Queries
{
    public class ContratoInfoQueryResult
    {
        public Nullable<int> CodUsuarioImportacao { get; set; }
        public string NumeroContrato { get; set; }
        public string NumeroContratoDecrypt
        {
            get { return BaseRotinas.Descriptografar(NumeroContrato); }
            set { NumeroContrato = BaseRotinas.Descriptografar(value); }
        }
        public DateTime DataCadastro { get; set; }
        public Nullable<DateTime> DataExcluido { get; set; }
        public Nullable<DateTime> DataAlteracao { get; set; }
        public Nullable<int> CodClientePrincipal { get; set; }
        public Nullable<decimal> ValorAtraso { get; set; }
        public Nullable<int> CodAging { get; set; }
        public Nullable<int> DiasAtraso { get; set; }
        public Nullable<int> CodRegionalProduto { get; set; }
        public Nullable<decimal> ValorAVencer { get; set; }
        public Nullable<decimal> ValorPago { get; set; }
        public Nullable<decimal> ValorTotal { get; set; }
        public Nullable<int> CodImportacao { get; set; }
        public Nullable<short> Governo { get; set; }
        public string StatusContrato { get; set; }
        public string MensagemContrato { get; set; }
        public Nullable<int> CodRegionalGrupo { get; set; }


        public string Aging { get; set; }
        public string Cliente { get; set; }
        public string ClienteDecrypt
        {
            get { return BaseRotinas.Descriptografar(Cliente); }
            set { Cliente = BaseRotinas.Descriptografar(value); }
        }
        public string CodCliente { get; set; }
        public string CodClienteDecrypt
        {
            get { return BaseRotinas.Descriptografar(CodCliente); }
            set { CodCliente = BaseRotinas.Descriptografar(value); }
        }
        public string ClienteDocumento { get; set; }
        public string ClienteDocumentoDecrypt
        {
            get { return BaseRotinas.Descriptografar(ClienteDocumento); }
            set { ClienteDocumento = BaseRotinas.Descriptografar(value); }
        }
        public string Regional { get; set; }
        public string RegionalDecrypt
        {
            get { return BaseRotinas.Descriptografar(Regional); }
            set { Regional = BaseRotinas.Descriptografar(value); }
        }
        public string Produto { get; set; }
        public string ProdutoDecrypt
        {
            get { return BaseRotinas.Descriptografar(Produto); }
            set { Produto = BaseRotinas.Descriptografar(value); }
        }
       
    }
}
