﻿using System;

namespace Cobranca.Domain.CobrancaContext.Queries
{
    public class HistoricoQueryResult
    {
        public int? CodCliente { get; set; }
        public int? CodContrato { get; set; }
        public int? CodParcela { get; set; }
        public int? CodSintese { get; set; }
        public int? CodUsuario { get; set; }
        public string Observacao { get; set; }
        public int? CodMotivoAtraso { get; set; }
        public int? CodResolucao { get; set; }
        public DateTime? DataAgenda { get; set; }
        public int? CodImportacao { get; set; }
        public int? CodAcaoCobranca { get; set; }
    }
}
