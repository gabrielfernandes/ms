﻿
using System;

namespace Cobranca.Domain.CobrancaContext.Queries
{
    public class RelatorioFilialDetalhadoQueryResult
    {
        public int? Ano { get; set; }
        public string NomeCliente { get; set; }
        public string CPF { get; set; }
        public string NumeroContrato { get; set; }
        public string FluxoPagamento { get; set; }
        public string TipoFatura { get; set; }
        public DateTime? DataEmissaoFatura { get; set; }
        public DateTime? DataVencimento { get; set; }
        public decimal? ValorParcela { get; set; }
        public decimal? ValorAberto { get; set; }
        public string Regional { get; set; }
        public string Produto { get; set; }
        public string Rota { get; set; }
        public string Sintese { get; set; }
        public string CodigoSinteseCliente { get; set; }
        public DateTime? DataHistorico { get; set; }
        public string CodigoEmpresaCobranca { get; set; }

    }
}
