﻿using System;

namespace Cobranca.Domain.CobrancaContext.Queries
{
    public class SinteseQueryResult
    {
        public int? CodSintese { get; set; }
        public int? CodFase { get; set; }
        public string NomeSintese { get; set; }
        public int? Ordem { get; set; }
        public bool? AcaoBloqueada { get; set; }
        public string CodigoSinteseCliente { get; set; }
        public bool? Justifica { get; set; }
    }
}
