﻿
namespace Cobranca.Domain.CobrancaContext.Queries
{
    public class ParametroAtendimentoRotaQueryResult
    {
        public int CodParametroAtendimento { get; set; }
        public string QueryRota { get; set; }
    }
}
