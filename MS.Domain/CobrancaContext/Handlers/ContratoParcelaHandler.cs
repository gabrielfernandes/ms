﻿using Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Inputs;
using Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared.Commands;
using JunixValidator;

namespace Cobranca.Domain.CobrancaContext.Handlers
{
    public class ContratoParcelaHandler : Notifiable, ICommandHandler<CreateContratoParcelaCommand>
    {
        private readonly IContratoParcelaRepository _repository;

        public ContratoParcelaHandler(IContratoParcelaRepository repository)
        {
            _repository = repository;
        }

        public ICommandResult Handle(CreateContratoParcelaCommand command)
        {
            // Criar a entidade
            var parcela = new ContratoParcela(command.FluxoPagamento);

            // Validar entidades e VOs
            //AddNotifications(parcela.Notifications);

            if (Invalid)
                return new CommandResult(
                    false,
                    "Por favor, corrija os campos abaixo",
                    Notifications);

            // Persistir o cliente
            _repository.Save(parcela);


            // Retornar o resultado para tela
            return new CommandResult(true, "Operação realizada com Sucesso!", new
            {
                Id = parcela.Cod,
                FluxoPagamento = parcela.FluxoPagamento
            });
        }
    }
}
