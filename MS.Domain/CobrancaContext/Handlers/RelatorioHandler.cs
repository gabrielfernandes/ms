﻿using Cobranca.Domain.CobrancaContext.Commands.ReportCommands;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared.Commands;
using JunixValidator;
using System;
using System.Linq;

namespace Cobranca.Domain.CobrancaContext.Handlers
{
    public class RelatorioHandler :
        Notifiable,
         ICommandHandler<CreateReportFilialDetailsCommand>
    {
        private readonly IRelatorioRepository _repository;

        public RelatorioHandler(IRelatorioRepository repository)
        {
            _repository = repository;
        }

        public ICommandResult Handle(CreateReportFilialDetailsCommand command)
        {

            if (Invalid)
                return new CommandResult(
                    false,
                    "Por favor, corrija os campos abaixo",
                    Notifications);

            // GET Rows
            var result = _repository.GetReportFilialDetails(
                command.Contrato,
                command.ClienteId,
                command.NumeroDocumento,
                command.DataPeriodoDe,
                command.DataPeriodoAte,
                command.Aberto,
                command.Vencido,
                command.Pago
                );


            // Retornar o resultado para tela
            return new CommandResult(true, "Operação realizada com Sucesso!", new
            {
               result
            });
        }
    }

}
