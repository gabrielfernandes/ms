﻿using Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Results;
using Cobranca.Domain.CobrancaContext.Commands.ParametroAtendimentoCommands.Inputs;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Helpers;
using Cobranca.Shared.Commands;
using JunixValidator;
using System;
using System.Linq;

namespace Cobranca.Domain.CobrancaContext.Handlers
{
    public class ClienteHandler :
        Notifiable
    {
        private readonly IClienteRepository _repository;

        public ClienteHandler(IClienteRepository repository)
        {
            _repository = repository;
        }

    }

}
