﻿using Cobranca.Domain.CobrancaContext.Commands.CartaCobrancaCommands;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared.Commands;
using JunixValidator;
using System;
using System.Linq;

namespace Cobranca.Domain.CobrancaContext.Handlers
{
    public class CartaCobrancaHandler :
        Notifiable,
         ICommandHandler<ValidarGerarCartaCommand>
    {
        private readonly ICartaCobrancaRepository _repository;

        public CartaCobrancaHandler(ICartaCobrancaRepository repository)
        {
            _repository = repository;
        }

        public ICommandResult Handle(ValidarGerarCartaCommand command)
        {
            // Agrupar as Validações
            AddNotifications(command);

            if (command.ListaParcelas == null)
                AddNotification("Parcela", "Selecione ao menos uma parcela!");

            if (Invalid)
                return new CommandResult(
                    false,
                    "Por favor, corrija os campos abaixo",
                    Notifications);

            // Retornar o resultado para tela
            return new CommandResult(true, "Validação OK!");
        }
    }

}
