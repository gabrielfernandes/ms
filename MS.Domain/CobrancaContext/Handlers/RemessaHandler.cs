﻿using Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Results;
using Cobranca.Domain.CobrancaContext.Commands.RemessaCommands.Inputs;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared.Commands;
using JunixValidator;

namespace Cobranca.Domain.CobrancaContext.Handlers
{
    public class RemessaHandler :
        Notifiable,
        ICommandHandler<CreateRemessaCommand>
    {
        private readonly IRemessaRepository _repository;

        public ICommandResult Handle(CreateRemessaCommand command)
        {
            // Obtem os boletos
            _repository.GetBoletosFromStaus(command.FluxoPagamento);



            //Percorrer a lista e inplementar as linhas

            var parcela = new ContratoParcela(command.FluxoPagamento);



            // Retornar o resultado para tela
            return new CommandResult(true, "Operação realizada com Sucesso!", new { parcela.FluxoPagamento });
        }
    }
}
