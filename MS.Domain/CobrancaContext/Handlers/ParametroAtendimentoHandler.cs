﻿using Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Results;
using Cobranca.Domain.CobrancaContext.Commands.ParametroAtendimentoCommands.Inputs;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Helpers;
using Cobranca.Shared.Commands;
using JunixValidator;
using System;
using System.Linq;

namespace Cobranca.Domain.CobrancaContext.Handlers
{
    public class ParametroAtendimentoHandler :
        Notifiable,
        ICommandHandler<RegisterParametroAtendimentoCommand>
    {
        private readonly IParametroAtendimentoRepository _repository;

        public ParametroAtendimentoHandler(IParametroAtendimentoRepository repository)
        {
            _repository = repository;
        }

        public ICommandResult Handle(RegisterParametroAtendimentoCommand command)
        {

            foreach (var usuario in command.Usuarios)
            {
                //Verifica se já existe algum parametro cadastrado
                if (_repository.CheckExist(command.Regional, usuario))
                    command.Cod = _repository.GetParametroByRegionalAndUsuario(command.Regional, usuario).Cod;


                //Criar Entidade
                var parametro = new ParametroAtendimento(command.Cod, usuario, command.Regional, command.Query, String.Join(",", command.QueryRota));

                // Agrupar as Validações
                AddNotifications(parametro);

                // Checar as notificações
                if (Invalid)
                    return new CommandResult(false,
                    "Por favor, corrija os campos abaixo",
                    Notifications);

                //DESVINCULAR ATENDIMENTOS DO USUÁRIO ONDE NÃO SATISFAZER MAIS AS CODIÇÕES
                _repository.DesvincularClienteAtendimentoNaoPertencente(usuario);

                //Persistir o parametro
                _repository.CreateOrUpdate(parametro);
            }
            return new CommandResult(true, "Operação realizada com sucesso!");
        }

    }

}
