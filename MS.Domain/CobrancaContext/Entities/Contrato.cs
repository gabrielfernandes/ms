﻿using Cobranca.Shared.Entities;
using System;

namespace Cobranca.Domain.CobrancaContext.Entities
{
    public class Contrato : Entity<int>
    {
        //ENTITY FRAMEWORK
        protected Contrato() { }

        public string NumeroContrato { get; private set; }
        public string StatusContrato { get; private set; }
        public string MensagemContrato { get; private set; }
        public int? CodRegionalGrupo { get; private set; }
        public int? CodAging { get; private set; }
        public int? DiasAtraso { get; private set; }
        public decimal? ValorAtraso { get; private set; }
        public int? CodClientePrincipal { get; private set; }
        public int? CodImportacao { get; private set; }


    }
}
