﻿using Cobranca.Shared.Entities;
using JunixValidator.Validation;

namespace Cobranca.Domain.CobrancaContext.Entities
{
    public class ParametroAtendimento : Entity<int>
    {
        protected ParametroAtendimento() { }

        public ParametroAtendimento(int cod, int codUsuario, int codReional, string query, string queryRota)
        {
            Cod = cod;
            CodUsuario = codUsuario;
            CodRegional = codReional;
            Query = query;
            QueryRota = queryRota;

            AddNotifications(new ValidationContract()
               .Requires()
               .IsGreaterOrEqualsThan(codUsuario, 0, "Usuario", "Selecione ao menos um usuário")
               .IsGreaterOrEqualsThan((int)codReional, 0, "Filial", "Selecione ao menos uma Filial")
               .IsNotNullOrEmpty(query, "Query", "A query é obrigatória")
           );
        }

        public int CodUsuario { get; private set; }
        public int CodRegional { get; private set; }
        public string Query { get; private set; }
        public string QueryRota { get; private set; }

    }
}
