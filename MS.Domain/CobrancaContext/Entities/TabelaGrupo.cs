﻿

using Cobranca.Shared.Entities;

namespace Cobranca.Domain.CobrancaContext.Entities
{
    public class TabelaGrupo : Entity<int>
    {
        protected TabelaGrupo() { }

        public string Nome { get; private set; }
        public bool? AcaoBloqueada { get; private set; }
    }
}
