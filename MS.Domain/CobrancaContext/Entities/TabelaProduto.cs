﻿using Cobranca.Shared.Entities;

namespace Cobranca.Domain.CobrancaContext.Entities
{
    public class TabelaProduto : Entity<int>
    {
        public TabelaProduto(string produto, string descricao)
        {
            Produto = produto;
            Descricao = descricao;
        }

        protected TabelaProduto() { }

        public string Produto { get; private set; }
        public string Descricao { get; private set; }
    }
}
