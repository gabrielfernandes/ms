﻿

using Cobranca.Shared.Entities;

namespace Cobranca.Domain.CobrancaContext.Entities
{
    public class Rota : Entity<int>
    {
        public Rota(string nome, string descricao)
        {
            Nome = nome;
            Descricao = descricao;
        }

        protected Rota() { }

        public string Nome { get; private set; }
        public string Descricao { get; private set; }
    }
}
