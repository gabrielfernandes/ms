﻿using Cobranca.Shared.Entities;
using System;


namespace Cobranca.Domain.CobrancaContext.Entities
{
    public class Historico : Entity<int>
    {
        protected Historico() { }

        public Nullable<int> CodCliente { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public Nullable<int> CodParcela { get; set; }
        public Nullable<int> CodSintese { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public string Observacao { get; set; }
        public Nullable<int> CodMotivoAtraso { get; set; }
        public Nullable<int> CodResolucao { get; set; }
        public Nullable<System.DateTime> DataAgenda { get; set; }
        public Nullable<int> CodImportacao { get; set; }
        public Nullable<int> CodAcaoCobranca { get; set; }
    }
}
