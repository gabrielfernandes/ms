﻿using Cobranca.Shared.Entities;
using Cobranca.Shared.ValueObjects;

namespace Cobranca.Domain.CobrancaContext.Entities
{
    public class Sintese : Entity<int>
    {
        protected Sintese() { }

        public int? CodFase { get; private set; }
        public string NomeSintese { get; private set; }
        public int? Ordem { get; private set; }
        public bool? AcaoBloqueada { get; private set; }
        public string CodigoSinteseCliente { get; private set; }
        public bool? Justifica { get; private set; }
    }
}
