﻿using Cobranca.Shared.Entities;
using Cobranca.Shared.ValueObjects;

namespace Cobranca.Domain.CobrancaContext.Entities
{
    public class Cliente : Entity<int>
    {
        protected Cliente() { }
        public string CodCliente { get; private set; }
        public string Nome { get; private set; }
        public Documento Documento { get; set; }
        public Email Email { get; set; }
        public Endereco Endereco { get; private set; }
    }
}
