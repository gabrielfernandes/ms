﻿using Cobranca.Shared.Entities;

namespace Cobranca.Domain.CobrancaContext.Commands.RotaCommands.Results
{
    public class RotaCommandResult : Entity<int>
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
    }
}
