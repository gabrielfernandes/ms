﻿using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;
using System.Collections.Generic;
using System.Linq;

namespace Cobranca.Domain.CobrancaContext.Commands.CartaCobrancaCommands
{
    public class ValidarGerarCartaCommand : Notifiable, ICommand
    {
        public int ClienteId { get; set; }
        public string Parcelas { get; set; }
        public string Justificativa { get; set; }

        public List<string> ListaParcelas
        {
            get { return Parcelas != null ? Parcelas.Split(',').Select(x => x).ToList() : null; }
        }

        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .IsNotNullOrEmpty(Parcelas, "Parcela", "Selecione ao menos uma parcela")
                .IsNotNull(ListaParcelas, "Parcela", "Selecione ao menos uma parcela")
            );
            return Valid;
        }
    }
}
