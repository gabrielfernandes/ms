﻿using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;

namespace Cobranca.Domain.CobrancaContext.Commands.ParametroAtendimentoCommands.Inputs
{
    public class RegisterParametroAtendimentoCommand : Notifiable, ICommand
    {
        public int Cod { get; set; }
        public int Regional { get; set; }
        public int[] Usuarios { get; set; }
        public string Query { get; set; }
        public int[] QueryRota { get; set; }

        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .IsGreaterThan(Regional, 0, "Filial", "Nenhuma filial selecionada")
                .IsNotNull(Usuarios, "Usuário", "Nenhum usuário selecionado")
                .IsNotNull(Query, "Condição", "Configure ao menos uma opção")
            );
            return Valid;
        }
    }
}
