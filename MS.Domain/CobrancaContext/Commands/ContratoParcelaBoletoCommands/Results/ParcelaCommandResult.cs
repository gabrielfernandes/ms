﻿
using Cobranca.Shared.Entities;
using System;

namespace Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Results
{
    public class ParcelaCommandResult : Entity<int>
    {
        public string FluxoPagamento { get; set; }
        public DateTime DataVencimento { get; set; }
    }
}
