﻿using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;

namespace Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Inputs
{
    public class CreateContratoParcelaCommand : Notifiable, ICommand
    {
        public string FluxoPagamento { get; set; }

        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .HasMinLen(FluxoPagamento, 3, "FluxoPagamento", "O nome deve conter pelo menos 3 caracteres")
                .HasMaxLen(FluxoPagamento, 40, "FirstName", "O nome deve conter no máximo 40 caracteres")
            //.HasMinLen(LastName, 3, "LastName", "O sobrenome deve conter pelo menos 3 caracteres")
            //.HasMaxLen(LastName, 40, "LastName", "O sobrenome deve conter no máximo 40 caracteres")
            //.IsEmail(Email, "Email", "O E-mail é inválido")
            //.HasLen(Document, 11, "Document", "CPF inválido")
            );
            return Valid;
        }
    }
}
