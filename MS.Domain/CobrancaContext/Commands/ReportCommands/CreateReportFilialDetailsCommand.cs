﻿using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;
using System;

namespace Cobranca.Domain.CobrancaContext.Commands.ReportCommands
{
    public class CreateReportFilialDetailsCommand : Notifiable, ICommand
    {
        public string Contrato { get; set; }
        public int? ClienteId { get; set; }
        public string NumeroDocumento { get; set; }
        public DateTime? DataPeriodoDe { get; set; }
        public DateTime? DataPeriodoAte { get; set; }
        public bool? Aberto { get; set; }
        public bool? Vencido { get; set; }
        public bool? Pago { get; set; }

        bool ICommand.Valid()
        {
            return Valid;
        }
    }
}
