﻿using Cobranca.Shared.Entities;

namespace Cobranca.Domain.CobrancaContext.Commands.ProdutoCommands.Results
{
    public class ProdutoCommandResult : Entity<int>
    {
        public string Produto { get; set; }
        public string Descricao { get; set; }
    }
}
