﻿using Cobranca.Domain.CobrancaContext.Commands.RotaCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Queries;
using Cobranca.Shared.Repositories;
using System.Collections.Generic;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface ISinteseRepository : IBaseRepository<Sintese>
    {
        SinteseQueryResult GetSinteseByCodigoSinteseCliente(string codigoCliente);
    }
}
