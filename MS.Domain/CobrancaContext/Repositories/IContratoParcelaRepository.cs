﻿
using Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;
using System.Collections.Generic;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface IContratoParcelaRepository
    {
        //bool CheckDocument(string document);
        void Save(ContratoParcela parcela);
        ContratoParcela Get(int cod);
        ContratoParcela GetByFluxoPagamento(string fluxoPagamento);
        ParcelaCommandResult Get(string fluxopagamento);
    }
}
