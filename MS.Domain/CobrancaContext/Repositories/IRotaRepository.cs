﻿
using Cobranca.Domain.CobrancaContext.Commands.RotaCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;
using System.Collections.Generic;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface IRotaRepository
    {
        Rota GetById(int cod);
        List<RotaCommandResult> Get();
    }
}
