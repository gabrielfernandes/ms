﻿using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Shared.Repositories;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface IClienteRepository : IBaseRepository<Cliente>
    {
    }
}
