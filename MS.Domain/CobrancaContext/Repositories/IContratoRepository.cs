﻿using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Queries;
using Cobranca.Shared.Repositories;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface IContratoRepository : IBaseRepository<Contrato>
    {
        ContratoInfoQueryResult GetContratoInfoById(int codContrato);
    }
}
