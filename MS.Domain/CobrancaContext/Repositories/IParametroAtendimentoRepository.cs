﻿using Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface IParametroAtendimentoRepository /*: IBaseRepository<ParametroAtendimento>*/
    {
        bool CheckExist(int codRegional, int codUsuario);
        ParametroAtendimento GetParametroByRegionalAndUsuario(int codRegional, int codUsuairo);

        ParametroAtendimento CreateOrUpdate(ParametroAtendimento entity);

        /// <summary>
        /// Liberar clientes (tabela: ClienteAtendimento) onde as condições parametrizadas do usuário não satisfazer mais as condições.
        /// <para>Para que outro atendente possa dar atendimentos a estes.</para>
        /// </summary>  
        /// <param name="usuarioId">Id do Usuario</param>
        void DesvincularClienteAtendimentoNaoPertencente(int usuarioId);
    }
}
