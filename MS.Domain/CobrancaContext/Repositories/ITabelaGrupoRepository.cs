﻿using Cobranca.Domain.CobrancaContext.Commands.RotaCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Shared.Repositories;
using System.Collections.Generic;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface ITabelaGrupoRepository : IBaseRepository<TabelaGrupo>
    {
        TabelaGrupo GetById(int cod);
        List<TabelaGrupo> GetAll();
    }
}
