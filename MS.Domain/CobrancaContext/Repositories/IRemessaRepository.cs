﻿using Cobranca.Domain.CobrancaContext.Commands.RemessaCommands.Results;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface IRemessaRepository
    {
        RemessaCommandResult GetBoletosFromStaus(string fluxopagamento);
    }
}
