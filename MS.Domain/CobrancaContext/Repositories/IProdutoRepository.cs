﻿
using Cobranca.Domain.CobrancaContext.Commands.ProdutoCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;
using System.Collections.Generic;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface IProdutoRepository
    {
        TabelaProduto GetById(int cod);
        List<ProdutoCommandResult> Get();
    }
}
