﻿using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Queries;
using Cobranca.Shared.Repositories;
using System.Collections.Generic;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface IHistoricoRepository : IBaseRepository<Historico>
    {
        List<HistoricoQueryResult> GetHistoricoByParcelaId(int codParcela);
    }
}
