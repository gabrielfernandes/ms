﻿using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Queries;
using Cobranca.Shared.Repositories;
using System;
using System.Collections.Generic;

namespace Cobranca.Domain.CobrancaContext.Repositories
{
    public interface IRelatorioRepository
    {
        List<RelatorioFilialDetalhadoQueryResult> GetReportFilialDetails(string contrato, int? clienteId, string numeroDocumento, DateTime? DataPeriodoDe, DateTime? DataPeriodoAte, bool? Aberto, bool? Vencido, bool? Pago);
    }
}
