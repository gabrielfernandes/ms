﻿namespace Cobranca.Domain.CobrancaContext.Enums
{
    public enum ERelatorioBoletoDetalhadoPeriodoTipo
    {
        Solicitacao = 1,
        Status = 2,
        Vencimento = 3
    }
    public class ERelatorioBoletoDetalhadoPeriodoTipoDescricao
    {
        public static string ERelatorioBoletoDetalhadoPeriodoTipoDesc(ERelatorioBoletoDetalhadoPeriodoTipo? tipo)
        {
            ERelatorioBoletoDetalhadoPeriodoTipo tp = (ERelatorioBoletoDetalhadoPeriodoTipo)tipo;
            switch (tp)
            {
                case ERelatorioBoletoDetalhadoPeriodoTipo.Solicitacao:
                    return "Data de Solicitação";
                case ERelatorioBoletoDetalhadoPeriodoTipo.Status:
                    return "Data do Status";
                case ERelatorioBoletoDetalhadoPeriodoTipo.Vencimento:
                    return "Data de Vencimento";
                default:
                    return "--";
            }
        }
    }
}
