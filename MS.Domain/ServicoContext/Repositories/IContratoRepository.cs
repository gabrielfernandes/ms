﻿using Cobranca.Domain.ServicoContext.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Domain.ServicoContext.Repositories
{
    public interface IContratoRepository
    {
        IEnumerable<ListContratoQueryResult> GetContratoToJoin();
    }
}
