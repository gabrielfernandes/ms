﻿using Cobranca.Domain.ServicoContext.Entities;
using Cobranca.Domain.ServicoContext.Queries;
using System.Collections.Generic;

namespace Cobranca.Domain.ServicoContext.Repositories
{
    public interface IClienteEnderecoRepository
    {
        IEnumerable<ListClienteEnderecoQueryResult> GetClienteEnderecoToJoin();
        IEnumerable<ListContratoQueryResult> GetContratoAndClienteToJoin();
        void CreateEnderecosByList(ref List<ClienteEndereco> lista);
        void UpdateEnderecosByList(ref List<ClienteEndereco> lista);
        void CreateClienteEnderecosByList(ref List<ClienteEndereco> lista);
        void UpdateClienteEnderecosByList(ref List<ClienteEndereco> lista);
    }
}
