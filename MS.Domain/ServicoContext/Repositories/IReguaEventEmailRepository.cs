﻿using Cobranca.Domain.ServicoContext.Entities;
using Cobranca.Domain.ServicoContext.Queries;
using System.Collections.Generic;

namespace Cobranca.Domain.ServicoContext.Repositories
{
    public interface IReguaEventEmailRepository
    {
        List<EnventoEmailInfoQueryResult> GetEventsEmail();
    }
}
