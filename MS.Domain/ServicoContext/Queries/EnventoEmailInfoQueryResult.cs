﻿namespace Cobranca.Domain.ServicoContext.Queries
{
    public class EnventoEmailInfoQueryResult
    {
        public int CodContrato { get; set; }
        public int CodParcela { get; set; }
        public int Cod { get; set; }
        public int CodClientePrincipal { get; set; }
        public int CodRegional { get; set; }
        public int CodTipoCobrancaAcao { get; set; }
    }
}
