﻿using Cobranca.Domain.ServicoContext.Enums;
using System;

namespace Cobranca.Domain.ServicoContext.Queries
{
    public class ListClienteEnderecoQueryResult
    {
        public ListClienteEnderecoQueryResult(int codClienteEndereco, int? codEndereco, int? codContrato, DateTime dataCadastro, DateTime dataAlteracao, short tipoEndereco)
        {
            CodClienteEndereco = codClienteEndereco;
            CodEndereco = codEndereco;
            CodContrato = codContrato;
            DataCadastro = dataCadastro;
            DataAlteracao = dataAlteracao;
            TipoEndereco = (EClienteEnderecoType)tipoEndereco;
        }

        public Nullable<int> CodClienteEndereco { get; set; }
        public Nullable<int> CodEndereco { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataAlteracao { get; set; }
        public EClienteEnderecoType TipoEndereco { get; set; }
    }
}
