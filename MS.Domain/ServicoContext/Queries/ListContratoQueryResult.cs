﻿namespace Cobranca.Domain.ServicoContext.Queries
{
    public class ListContratoQueryResult
    {
        public int Cod { get; set; }
        public string NumeroContrato { get; set; }
        public int CodCliente { get; set; }
    }
}
