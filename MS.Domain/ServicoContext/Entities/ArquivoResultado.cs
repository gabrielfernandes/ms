﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Domain.ImportacaoContext.Entities
{
    public class ArquivoResultado
    {
        public ArquivoResultado(string linha)
        {
            Linha = $"OK;{linha}";
        }
        public ArquivoResultado(Exception ex, string linha)
        {
            Linha = $"ERRO;{linha};{ex.InnerException};{ex.Message}";
        }
        public string Linha { get; private set; }
    }
}
