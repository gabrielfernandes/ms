﻿using Cobranca.Shared.ValueObjects;
using System;

namespace Cobranca.Domain.ServicoContext.Entities
{
    public class ClienteEndereco
    {
        public ClienteEndereco(int? codClienteEnderecoCob, int? codEnderecoCob, Endereco enderecoCob, int? codClienteEnderecoObra, int? codEnderecoObra, Endereco enderecoObra, string mensagemContrato, string contrato, int? codContrato, int? codCliente, string status, string importMensagem, string conteudoOriginal)
        {
            CodClienteEnderecoCob = codClienteEnderecoCob;
            CodEnderecoCob = codEnderecoCob;
            EnderecoCob = enderecoCob;
            CodClienteEnderecoObra = codClienteEnderecoObra;
            CodEnderecoObra = codEnderecoObra;
            EnderecoObra = enderecoObra;
            MensagemContrato = mensagemContrato;
            Contrato = contrato;
            CodContrato = codContrato;
            CodCliente = codCliente;
            Status = Status;
            ImportMensagem = importMensagem;
            ConteudoOriginal = conteudoOriginal;

        }

        //ENTITY FRAMEWORK
        protected ClienteEndereco() { }

        public Nullable<int> CodImportacao { get; set; }
        public string Contrato { get; private set; }
        public Nullable<int> CodClienteEnderecoCob { get; set; }
        public int? CodEnderecoCob { get; private set; }
        public Endereco EnderecoCob { get; private set; }
        public Nullable<int> CodClienteEnderecoObra { get; set; }
        public int? CodEnderecoObra { get; private set; }
        public Endereco EnderecoObra { get; private set; }
        public Nullable<DateTime> DataProcessamento { get; private set; }
        public Nullable<int> CodCliente { get; private set; }
        public Nullable<int> CodContrato { get; private set; }
        public string Status { get; private set; }
        public string MensagemContrato { get; private set; }
        public string ConteudoOriginal { get; private set; }
        public string ImportMensagem { get; set; }
        public string ErroEndCobranca { get; set; }
        public string ErroEndObra { get; set; }

        public void SetCodEnderecoCob(int codEnderecoCob)
        {
            CodEnderecoCob = codEnderecoCob;
        }

        public void SetCodEnderecoObra(int codEnderecoObra)
        {
            codEnderecoObra = codEnderecoObra;
        }
    }
}
