﻿namespace Cobranca.Domain.ServicoContext.Enums
{
    public enum  EClienteEnderecoType
    {
        Cliente = 1,
        Cobranca = 2,
        Obra = 3
    }
}
