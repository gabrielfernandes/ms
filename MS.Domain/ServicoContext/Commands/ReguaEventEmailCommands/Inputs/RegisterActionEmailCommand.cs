﻿using Cobranca.Shared.Commands;
using JunixValidator;
using System;

namespace Cobranca.Domain.ServicoContext.Commands.ReguaEventEmailCommands.Inputs
{
    public class RegisterActionEmailCommand : Notifiable, ICommand
    {
        public RegisterActionEmailCommand() { }

        public int? CodAcao { get; set; }
        public int? CodCliente { get; set; }
        public int? CodRegional { get; set; }

        public string Erro { get; set; }

        public DateTime? DataProcessamento { get; set; }

        bool ICommand.Valid()
        {
            return Valid;
        }
    }
}
