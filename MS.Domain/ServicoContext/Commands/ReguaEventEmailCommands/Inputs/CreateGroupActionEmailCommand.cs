﻿using Cobranca.Shared.Commands;
using JunixValidator;
using System;

namespace Cobranca.Domain.ServicoContext.Commands.ReguaEventEmailCommands.Inputs
{
    public class CreateGroupActionEmailCommand : Notifiable, ICommand
    {
        public CreateGroupActionEmailCommand() { }

        public DateTime? DataProcessamento { get; set; }

        bool ICommand.Valid()
        {
            return Valid;
        }
    }
}
