﻿using Cobranca.Domain.ServicoContext.Commands.ReguaEventEmailCommands.Inputs;
using Cobranca.Shared.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Domain.ServicoContext.Commands.ReguaEventEmailCommands.Outputs
{
    public class CreateGroupActionEmailResult : ICommandResult
    {
        public CreateGroupActionEmailResult(bool success, string message, List<RegisterActionEmailCommand> list)
        {
            Success = success;
            Message = message;
            ListResult = list;
        }

        public CreateGroupActionEmailResult(bool success, string message, object data)
        {
            Success = success;
            Message = message;
            Data = data;
        }

        public bool Success { get; set; }
        public string Message { get; set; }
        public List<RegisterActionEmailCommand> ListResult { get; set; }
        public object Data { get; set; }
    }
}
