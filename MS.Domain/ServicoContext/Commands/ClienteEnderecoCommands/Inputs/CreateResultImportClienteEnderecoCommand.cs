﻿using Cobranca.Shared.Commands;
using JunixValidator;

namespace Cobranca.Domain.ServicoContext.Commands.ClienteEnderecoCommands.Inputs
{
    public class CreateResultImportClienteEnderecoCommand : Notifiable, ICommand
    {
        public CreateResultImportClienteEnderecoCommand(string status, int inside, string message)
        {
            Status = status;
            Inside = inside;
            Message = message;
        }

        public string Status { get; private set; }
        public int Inside { get; private set; }
        public string Message { get; private set; }

        bool ICommand.Valid()
        {
            return Valid;
        }
    }
}
