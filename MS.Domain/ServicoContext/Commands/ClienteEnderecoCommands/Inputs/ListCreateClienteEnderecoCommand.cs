﻿using Cobranca.Shared.Commands;
using JunixValidator;
using System.Collections.Generic;

namespace Cobranca.Domain.ServicoContext.Commands.ClienteEnderecoCommands.Inputs
{
    public class ListCreateClienteEnderecoCommand : Notifiable, ICommand
    {
        public ListCreateClienteEnderecoCommand() { }
        public ListCreateClienteEnderecoCommand(List<CreateClienteEnderecoCommand> listCommands)
        {
            ListCommands = listCommands;
        }

        public List<CreateClienteEnderecoCommand> ListCommands { get; set; }

        bool ICommand.Valid()
        {
            return Valid;
        }
    }
}
