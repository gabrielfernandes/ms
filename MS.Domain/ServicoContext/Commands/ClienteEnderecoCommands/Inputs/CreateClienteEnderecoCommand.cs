﻿using Cobranca.Shared.Commands;
using JunixValidator;
using System;

namespace Cobranca.Domain.ServicoContext.Commands.ClienteEnderecoCommands.Inputs
{
    public class CreateClienteEnderecoCommand : Notifiable, ICommand
    {
        public const string _caminhoPendente = @"C:\Sites\Cobflow\SERVICO\Importacao\ClienteEnderecoPendente\";
        public const string _caminhoProcessado = @"C:\Sites\Cobflow\SERVICO\Importacao\ClienteEnderecoProcessado\";
        public const string _caminhoSwap = @"C:\Sites\Cobflow\SERVICO\Importacao\ImportacaoSwap\Endereco";

        public CreateClienteEnderecoCommand() { }

        public CreateClienteEnderecoCommand(CreateClienteEnderecoCommand item, int codContrato, int codCliente)
        {
            this.NumeroContrato = item.NumeroContrato;
            this.An8Cob = item.An8Cob;
            this.EnderecoCob = item.EnderecoCob;
            this.NumeroCob = item.NumeroCob;
            this.ComplementoCob = item.ComplementoCob;
            this.BairroCob = item.BairroCob;
            this.CepCob = item.CepCob;
            this.CidadeCob = item.CidadeCob;
            this.EstadoCob = item.EstadoCob;
            this.An8Obra = item.An8Obra;
            this.EnderecoObra = item.EnderecoObra;
            this.NumeroObra = item.NumeroObra;
            this.ComplementoObra = item.ComplementoObra;
            this.BairroObra = item.BairroObra;
            this.CepObra = item.CepObra;
            this.CidadeObra = item.CidadeObra;
            this.EstadoObra = item.EstadoObra;
            this.MensagemContrato = item.MensagemContrato;
            this.ConteudoOriginal = item.ConteudoOriginal;
            this.ImportStatus = item.ImportStatus;
            this.ImportMensagem = item.ImportMensagem;
            this.CodContrato = codContrato;
            this.CodCliente = codCliente;


        }

        public string NumeroContrato { get; set; }
        public string An8Cob { get; set; }
        public string EnderecoCob { get; set; }
        public string NumeroCob { get; set; }
        public string ComplementoCob { get; set; }
        public string BairroCob { get; set; }
        public string CepCob { get; set; }
        public string CidadeCob { get; set; }
        public string EstadoCob { get; set; }
        public string An8Obra { get; set; }
        public string EnderecoObra { get; set; }
        public string NumeroObra { get; set; }
        public string ComplementoObra { get; set; }
        public string BairroObra { get; set; }
        public string CepObra { get; set; }
        public string CidadeObra { get; set; }
        public string EstadoObra { get; set; }
        public string MensagemContrato { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public Nullable<int> CodCliente { get; set; }
        public string ConteudoOriginal { get; set; }
        public string ImportStatus { get; set; }
        public string ImportMensagem { get; set; }

        bool ICommand.Valid()
        {
            return Valid;
        }
    }
}
