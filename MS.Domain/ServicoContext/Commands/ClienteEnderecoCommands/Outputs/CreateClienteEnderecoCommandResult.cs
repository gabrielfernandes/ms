﻿using Cobranca.Shared.Commands;
using System.Collections.Generic;

namespace Cobranca.Domain.ServicoContext.Commands.ClienteEnderecoCommands.Outputs
{
    public class CreateClienteEnderecoCommandResult : ICommandResult
    {
        public CreateClienteEnderecoCommandResult(bool success, string message, List<string> list)
        {
            Success = success;
            Message = message;
            ListResult = list;
        }

        public CreateClienteEnderecoCommandResult(bool success, string message, object data)
        {
            Success = success;
            Message = message;
            Data = data;
        }

        public bool Success { get; set; }
        public string Message { get; set; }
        public List<string> ListResult { get; set; }
        public object Data { get; set; }
    }
}
