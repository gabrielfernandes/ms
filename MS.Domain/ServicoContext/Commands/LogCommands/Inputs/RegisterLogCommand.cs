﻿using Cobranca.Shared.Commands;
using JunixValidator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Domain.ServicoContext.Commands.LogCommands.Inputs
{
    public class RegisterLogCommand : Notifiable, ICommand
    {
        public const string _caminhoPasta = @"C:\Sites\Cobflow\SERVICO\log\";
        public const string _nome = @"LOG-OTIS";

        public RegisterLogCommand(string mensagem)
        {
            this.Mensagem = $"INFO {DateTime.Now:dd-MM-yyyy HH:mm:ss} - {mensagem}";
            this.CaminhoArquivoLog = $"{_caminhoPasta}({DateTime.Now.ToString("dd-MM-yyyy")}){_nome}.txt";
        }
        public RegisterLogCommand(Exception ex)
        {
            this.Mensagem = $"ERROR{DateTime.Now:dd-MM-yyyy HH:mm:ss} - {ex.Message} - {ex.InnerException}";
            this.CaminhoArquivoLog = $"{_caminhoPasta}({DateTime.Now.ToString("dd-MM-yyyy")}){_nome}.txt";
        }

        public string Mensagem { get; private set; }
        public string CaminhoArquivoLog { get; private set; }

        bool ICommand.Valid()
        {
            return Valid;
        }
    }
}
