﻿using Cobranca.Shared.Commands;
using JunixValidator;

namespace Cobranca.Domain.ServicoContext.Commands.ImportacaoResultadoCommands
{
    public class MoveResultCommand : Notifiable, ICommand
    {
        public MoveResultCommand(string nomeArquivo, string caminhoOriginal, string caminhoDestino, string id)
        {
            NomeArquivo = nomeArquivo;
            CaminhoOriginal = caminhoOriginal;
            CaminhoDestino = caminhoDestino;
            Id = id;
        }

        public string NomeArquivo { get; private set; }
        public string CaminhoOriginal { get; private set; }
        public string CaminhoDestino { get; private set; }
        public string Id { get; private set; }

        bool ICommand.Valid()
        {
            return Valid;
        }
    }
}
