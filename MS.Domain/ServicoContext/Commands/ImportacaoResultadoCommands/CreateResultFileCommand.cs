﻿using Cobranca.Shared.Commands;
using JunixValidator;
using System.Collections.Generic;

namespace Cobranca.Domain.ServicoContext.Commands.ImportacaoResultadoCommands
{
    public class CreateResultFileCommand : Notifiable, ICommand
    {
        public CreateResultFileCommand(string caminhoArquivo, string nomeArquivo, List<string> dados)
        {
            CaminhoArquivo = caminhoArquivo;
            NomeArquivo = nomeArquivo;
            Dados = dados;

        }

        public string CaminhoArquivo { get; private set; }
        public string NomeArquivo { get; private set; }
        public string CaminhoCompleto { get; set; }
        public List<string> Dados { get; private set; }

        bool ICommand.Valid()
        {
            return Valid;
        }
    }
}
