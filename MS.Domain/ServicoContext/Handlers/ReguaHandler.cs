﻿using Cobranca.Domain.ServicoContext.Commands.ReguaEventEmailCommands.Inputs;
using Cobranca.Domain.ServicoContext.Commands.ReguaEventEmailCommands.Outputs;
using Cobranca.Domain.ServicoContext.Repositories;
using Cobranca.Shared.Commands;
using JunixValidator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Domain.ServicoContext.Handlers
{
    public class ReguaHandler :
        Notifiable,
        ICommandHandler<CreateGroupActionEmailCommand>,
        ICommandHandler<RegisterActionEmailCommand>
    {
        private readonly IReguaEventEmailRepository _repEventEmailRepository;

        public ReguaHandler(IReguaEventEmailRepository repEventEmailRepository)
        {
            _repEventEmailRepository = repEventEmailRepository;
        }

        public ICommandResult Handle(CreateGroupActionEmailCommand command)
        {
            List<RegisterActionEmailCommand> listRegisterCommands = new List<RegisterActionEmailCommand>();
            var events = _repEventEmailRepository.GetEventsEmail();
            var gRegional = events.GroupBy(x => x.CodRegional);

            foreach (var itemRegional in gRegional)
            {
                int codRegional = itemRegional.Key;
                var gCliente = itemRegional.GroupBy(x => x.CodClientePrincipal);
                foreach (var itemCliente in gCliente)
                {
                    try
                    {
                        int codCliente = itemCliente.Key;
                        var gAcao = itemCliente.GroupBy(x => x.CodTipoCobrancaAcao);
                        foreach (var itemAcao in gAcao)
                        {
                            try
                            {
                                int codAcao = itemAcao.Key;
                                listRegisterCommands.Add(new RegisterActionEmailCommand()
                                {
                                    CodAcao = codAcao,
                                    CodCliente = codCliente,
                                    CodRegional = codRegional
                                });
                            }
                            catch (Exception ex) { listRegisterCommands.Add(new RegisterActionEmailCommand() { CodRegional = codRegional, CodCliente = codCliente, CodAcao = itemAcao.Key, Erro = $"Erro ao processar evento de email: {ex.Message}" }); }
                        }
                    }
                    catch (Exception ex) { listRegisterCommands.Add(new RegisterActionEmailCommand() { CodRegional = codRegional, Erro = $"Erro ao processar evento de email: {ex.Message}" }); }
                }
            }

            return new CreateGroupActionEmailResult(true, "Arquivos Agrupados!", listRegisterCommands);
        }

        public ICommandResult Handle(RegisterActionEmailCommand command)
        {
            return new CommandResult(true, "Arquivos Agrupados!");
        }
    }
}
