﻿
using Cobranca.Domain.ServicoContext.Commands.ImportacaoResultadoCommands;
using Cobranca.Domain.ServicoContext.Entities;
using Cobranca.Domain.ServicoContext.Enums;
using Cobranca.Domain.ServicoContext.Repositories;
using Cobranca.Shared.Commands;
using Cobranca.Shared.ValueObjects;
using JunixValidator;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Cobranca.Domain.ServicoContext.Handlers
{
    public class ImportResultHandlers :
        Notifiable,
        ICommandHandler<MoveResultCommand>,
        ICommandHandler<CreateResultFileCommand>
    {
        private readonly IClienteEnderecoRepository _repClienteEndereco;
        private readonly IContratoRepository _repContrato;

        public ImportResultHandlers(IClienteEnderecoRepository repClienteEndereco, IContratoRepository repContrato)
        {
            _repClienteEndereco = repClienteEndereco;
            _repContrato = repContrato;
        }

        public ICommandResult Handle(MoveResultCommand command)
        {
            string PastaFinal = "";

            if (!string.IsNullOrEmpty(command.Id))
                PastaFinal = Path.Combine(command.CaminhoDestino, command.Id);
            else
                PastaFinal = Path.Combine(command.CaminhoDestino);

            if (!Directory.Exists(PastaFinal))
                Directory.CreateDirectory(PastaFinal);

            string DestinoFinal = Path.Combine(PastaFinal, command.NomeArquivo);

            if (!File.Exists(DestinoFinal))
                File.Move(command.CaminhoOriginal, DestinoFinal);

            return new CommandResult(true, "Movido");
        }
        public ICommandResult Handle(CreateResultFileCommand command)
        {
            if (!Directory.Exists(command.CaminhoArquivo))
                Directory.CreateDirectory(command.CaminhoArquivo);

            command.CaminhoCompleto = Path.Combine(command.CaminhoArquivo, command.NomeArquivo);

            FileStream fs = null;
            if (!File.Exists(command.CaminhoCompleto))
                fs = new FileStream(command.CaminhoCompleto, FileMode.OpenOrCreate, FileAccess.Write);
            else if (File.Exists(command.CaminhoCompleto) && File.GetLastWriteTime(command.CaminhoCompleto) < DateTime.Today)
                fs = new FileStream(command.CaminhoCompleto, FileMode.OpenOrCreate, FileAccess.Write);
            else
                fs = new FileStream(command.CaminhoCompleto, FileMode.Append);

            StreamWriter sw = new StreamWriter(fs);
            foreach (var item in command.Dados)
            {
                sw.WriteLine(item);
            }
            sw.Close();
            fs.Close();

            return new CommandResult(true, "Arquivo Gerado", new { CaminhoCompleto = command.CaminhoCompleto });
        }
    }
}
