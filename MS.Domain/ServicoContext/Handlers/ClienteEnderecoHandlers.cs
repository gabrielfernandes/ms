﻿
using Cobranca.Domain.ServicoContext.Commands.ClienteEnderecoCommands.Inputs;
using Cobranca.Domain.ServicoContext.Commands.ClienteEnderecoCommands.Outputs;
using Cobranca.Domain.ServicoContext.Entities;
using Cobranca.Domain.ServicoContext.Enums;
using Cobranca.Domain.ServicoContext.Repositories;
using Cobranca.Shared.Commands;
using Cobranca.Shared.ValueObjects;
using JunixValidator;
using System.Collections.Generic;
using System.Linq;

namespace Cobranca.Domain.ServicoContext.Handlers
{
    public class ClienteEnderecoHandlers :
        Notifiable,
        ICommandHandler<ListCreateClienteEnderecoCommand>,
        ICommandHandler<CreateClienteEnderecoCommand>
    {
        private readonly IClienteEnderecoRepository _repClienteEndereco;
        private readonly IContratoRepository _repContrato;

        public ClienteEnderecoHandlers(IClienteEnderecoRepository repClienteEndereco, IContratoRepository repContrato)
        {
            _repClienteEndereco = repClienteEndereco;
            _repContrato = repContrato;
        }

        public ICommandResult Handle(ListCreateClienteEnderecoCommand command)
        {
            //Procurar os Ids dos Contratos
            var ListaContrato = _repClienteEndereco.GetContratoAndClienteToJoin();
            command.ListCommands = (from e in command.ListCommands
                                    join contrato in ListaContrato on e.NumeroContrato equals contrato.NumeroContrato into contratoJoin
                                    from c in contratoJoin.DefaultIfEmpty()
                                    select new CreateClienteEnderecoCommand(e, c != null ? c.Cod : 0, c != null ? c.CodCliente : 0)
                                    ).ToList();

            //Preencher Lista de Enderecos Cadastrados
            var listEnderecoCob = _repClienteEndereco.GetClienteEnderecoToJoin();

            //Criar Lista de entidades
            var listaEnderecos = (from e in command.ListCommands
                                  join eCob in listEnderecoCob.Where(x => x.TipoEndereco == EClienteEnderecoType.Cobranca) on e.CodContrato equals eCob.CodContrato into gEndCob
                                  join eObr in listEnderecoCob.Where(x => x.TipoEndereco == EClienteEnderecoType.Obra) on e.CodContrato equals eObr.CodContrato into gEndObr
                                  from eCob in gEndCob.DefaultIfEmpty()
                                  from eObr in gEndObr.DefaultIfEmpty()
                                  select new ClienteEndereco(
                                      eCob != null ? eCob.CodClienteEndereco : 0,
                                      eCob != null ? (int)eCob.CodEndereco : 0,
                                      new Endereco(e.CepCob, e.EnderecoCob, e.NumeroCob, e.BairroCob, e.CidadeCob, e.EstadoCob),
                                      eObr != null ? eObr.CodClienteEndereco : 0,
                                      eCob != null ? (int)eCob.CodEndereco : 0,
                                      new Endereco(e.CepObra, e.EnderecoObra, e.NumeroObra, e.BairroObra, e.CidadeObra, e.EstadoObra),
                                      e.MensagemContrato,
                                      e.NumeroContrato,
                                      e.CodContrato,
                                      e.CodCliente,
                                      e.ImportStatus,
                                      e.ImportMensagem,
                                      e.ConteudoOriginal
                                      )
                            ).ToList();

            //Update Enderecos
            _repClienteEndereco.UpdateEnderecosByList(ref listaEnderecos);

            //Insert Enderecos
            _repClienteEndereco.CreateEnderecosByList(ref listaEnderecos);

            //Update Cliente Enderecos
            _repClienteEndereco.UpdateClienteEnderecosByList(ref listaEnderecos);

            //Insert Cliente Enderecos
            _repClienteEndereco.CreateClienteEnderecosByList(ref listaEnderecos);

            //Consolidando o resultado
            List<string> resultado = new List<string>();
            listaEnderecos.ForEach(x => resultado.Add($"{x.Status};{x.ConteudoOriginal};{x.ImportMensagem}"));

            return new CreateClienteEnderecoCommandResult(true, "Arquivo processado!", resultado);
        }

        public ICommandResult Handle(CreateClienteEnderecoCommand command)
        {

            var Contratos = _repContrato.GetContratoToJoin();


            return new CreateClienteEnderecoCommandResult(true, "Bem vindo ao balta Store", new
            {
                Name = command.ToString(),
                Email = command.An8Cob
            });
        }
    }
}
