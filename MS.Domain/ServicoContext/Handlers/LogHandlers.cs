﻿
using Cobranca.Domain.ServicoContext.Commands.LogCommands.Inputs;
using Cobranca.Shared.Commands;
using JunixValidator;
using System;
using System.IO;

namespace Cobranca.Domain.ServicoContext.Handlers
{
    public class LogHandlers : Notifiable, ICommandHandler<RegisterLogCommand>
    {
        public ICommandResult Handle(RegisterLogCommand command)
        {
            if (!Directory.Exists(RegisterLogCommand._caminhoPasta))
                Directory.CreateDirectory(RegisterLogCommand._caminhoPasta);

            FileStream fs = null;
            if (!File.Exists(command.CaminhoArquivoLog))
                fs = new FileStream(command.CaminhoArquivoLog, FileMode.OpenOrCreate, FileAccess.Write);
            else if (File.Exists(command.CaminhoArquivoLog) && File.GetLastWriteTime(command.CaminhoArquivoLog) < DateTime.Today)
                fs = new FileStream(command.CaminhoArquivoLog, FileMode.OpenOrCreate, FileAccess.Write);
            else
                fs = new FileStream(command.CaminhoArquivoLog, FileMode.Append);

            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine(command.Mensagem);
            sw.Close();
            fs.Close();

            return new CommandResult(true, "Log Registrado!");
        }
    }
}
