﻿using Cobranca.Helpers;
using System;

namespace Cobranca.Domain.BoletoContext.Queries
{
    public class BancoPortadorInstrucaoQueryResult
    {
        public int Cod { get; set; }
        public Nullable<int> CodBancoPortador { get; set; }
        public string CodigoInstrucao { get; set; }
        public string Numero { get; set; }
        public string Descricao { get; set; }
    }
}
