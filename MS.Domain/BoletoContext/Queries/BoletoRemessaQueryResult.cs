﻿using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Helpers;
using System;

namespace Cobranca.Domain.BoletoContext.Queries
{
    public class BoletoRemessaQueryResult 
    {
        public Nullable<int> CodParcela { get; set; }
        public Nullable<int> CodBoleto { get; set; }
        public string ClienteNome { get; set; }
        public string ClienteDecrypt_
        {
            get { return BaseRotinas.Descriptografar(ClienteNome); }
            set { ClienteNome = BaseRotinas.Descriptografar(value); }
        }
        public string ClienteCPF { get; set; }
        public string ClienteCPFDecrypt_
        {
            get { return BaseRotinas.Descriptografar(ClienteCPF); }
            set { ClienteCPF = BaseRotinas.Descriptografar(value); }
        }
        public string CodigoClienteDecrypt_
        {
            get { return BaseRotinas.Descriptografar(CodigoCliente); }
            set { CodigoCliente = BaseRotinas.Descriptografar(value); }
        }
        public string ClienteEndereco { get; set; }
        public string ClienteEnderecoNumero { get; set; }
        public string ClienteEnderecoCEP { get; set; }
        public string ClienteEnderecoBairro { get; set; }
        public string ClienteEnderecoCidade { get; set; }
        public string ClienteEnderecoUF { get; set; }
        public string ClienteEnderecoComplemento { get; set; }

        public string ParcelaTipoFatura { get; set; }
        public string Companhia { get; set; }
        public string CompanhiaFiscal { get; set; }
        public int? NumeroParcela { get; set; }
        public short BancoNumero { get; set; }
        public string Regional { get; set; }
        public string RegionalDecrypt_
        {
            get { return BaseRotinas.Descriptografar(Regional); }
            set { Regional = BaseRotinas.Descriptografar(value); }
        }
        public Nullable<int> CodContrato { get; set; }
        public Nullable<int> CodHistorico { get; set; }
        public Nullable<int> CodClientePrincipal { get; set; }
        public string CodigoCliente { get; set; }
        
       
        public string NumeroDocumento { get; set; }

        public Nullable<DateTime> DataVencimento { get; set; }
        public string DataVencimentoDesc { get { return $"{DataVencimento:dd/MM/yyyy}"; } }

        public Nullable<DateTime> DataDocumento { get; set; }
        public string DataDocumentoDesc { get { return $"{DataDocumento:dd/MM/yyyy}"; } }

        public Nullable<Decimal> JurosAoMesPorcentagem { get; set; }
        public string JurosAoMesPorcentagemDesc { get { return $"{JurosAoMesPorcentagem:N2}"; } }

        public Nullable<Decimal> JurosAoMesPorcentagemPrevisto { get; set; }
        public string JurosAoMesPorcentagemPrevistoDesc { get { return $"{JurosAoMesPorcentagemPrevisto:N2}"; } }

        public Nullable<Decimal> ValorJuros { get; set; }
        public string ValorJurosDesc { get { return $"{ValorJuros:C}"; } }

        public Nullable<Decimal> ValorJurosPrevisto { get; set; }
        public string ValorJurosPrevistosDesc { get { return $"{ValorJurosPrevisto:C}"; } }

        public Nullable<Decimal> MultaAtrasoPorcentagem { get; set; }
        public string MultaAtrasoPorcentagemDesc { get { return $"{MultaAtrasoPorcentagem:N2}"; } }

        public Nullable<Decimal> MultaAtrasoPorcentagemPrevisto { get; set; }
        public string MultaAtrasoPorcentagemPrevistoDesc { get { return $"{MultaAtrasoPorcentagemPrevisto:N2}"; } }

        public Nullable<Decimal> ValorMulta { get; set; }
        public string ValorMultaDesc { get { return $"{ValorMulta:C}"; } }

        public Nullable<Decimal> ValorMultaPrevisto { get; set; }
        public string ValorMultaPrevistoDesc { get { return $"{ValorMultaPrevisto:C}"; } }

        public Nullable<Decimal> ValorDocumentoOriginal { get; set; }
        public string ValorDocumentoOriginalDesc { get { return $"{ValorDocumentoOriginal:C}"; } }

        public Nullable<Decimal> ValorDocumentoPrevisto { get; set; }
        public string ValorDocumentoPrevistoDesc { get { return $"{ValorDocumentoPrevisto:C}"; } }


        public Nullable<Decimal> ValorDocumento { get; set; }
        public string ValorDocumentoDesc { get { return $"{ValorDocumento:C}"; } }

        public string Observacao { get; set; }

        public Nullable<short> TipoFluxo { get; set; }

        public short TipoSolicitacao { get; set; }
        public string TipoSolicitacaoDesc { get { return EBoletoSolicitacaoTipoDescricao.EBoletoSolicitacaoTipoDesc((EBoletoSolicitacaoTipo)TipoSolicitacao); } }

        public Nullable<short> Status { get; set; }

        public string Usuario { get; set; }

        public Nullable<DateTime> DataCadastro { get; set; }
        public string DataCadastroDesc { get { return $"{DataCadastro:dd/MM/yyyy HH:mm}"; } }

        public Nullable<DateTime> DataAprovacao { get; set; }
        public string DataAprovacaoDesc { get { return $"{DataAprovacao:dd/MM/yyyy HH:mm}"; } }

        public Nullable<int> NossoNumero { get; set; }

        public Nullable<DateTime> DataRemessa { get; set; }
        public string DataRemessaDesc { get { return $"{DataRemessa:dd/MM/yyyy}"; } }

        public Nullable<bool> Alterar { get; set; }

        public Nullable<int> CodLogArquivo { get; set; }

        public string LinhaDigitavel { get; set; }
        public int? CodBancoPortador { get; set; }

        public string Erro { get; set; }
    }
}
