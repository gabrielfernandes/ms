﻿using Cobranca.Helpers;
using System;

namespace Cobranca.Domain.BoletoContext.Queries
{
    public class BancoPortadorQueryResult
    {
        public int Cod { get; set; }
        public Nullable<int> CodBanco { get; set; }
        public string NumeroBanco { get; set; }
        public string NomeBanco { get; set; }
        public string Portador { get; set; }
        public string CNPJ { get; set; }
        public string Agencia { get; set; }
        public string AgenciaDigito { get; set; }
        public string Conta { get; set; }
        public string ContaDigito { get; set; }
        public string CompanhiaFiscal { get; set; }
        public Nullable<int> CodRegionalGrupo { get; set; }
        public string CodRegionalCliente { get; set; }
        public string NomeRegional { get; set; }
        public Nullable<decimal> JurosAoMesPorcentagem { get; set; }
        public Nullable<decimal> TaxaPorParcela { get; set; }
        public Nullable<decimal> MultaAtraso { get; set; }
        public Nullable<int> NumeroDiaRegistroBoleto { get; set; }
        public Nullable<decimal> MoraAtrasoPorcetagemDia { get; set; }
        public Nullable<DateTime> DataInicialRegistroBoleto { get; set; }

        public Nullable<short> Carteira { get; set; }
        public string CEP { get; set; }
        public string Endereco { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Bairro { get; set; }
        public string UF { get; set; }
        public string EnderecoNumero { get; set; }
        public string EnderecoComplemento { get; set; }
        public string NomeRegionalDecrypt
        {
            get { return BaseRotinas.Descriptografar(NomeRegional); }
            set { NomeRegional = BaseRotinas.Descriptografar(value); }
        }
    }
}
