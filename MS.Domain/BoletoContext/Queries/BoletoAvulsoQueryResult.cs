﻿using Cobranca.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Cobranca.Domain.BoletoContext.Queries
{
    public class BoletoAvulsoQueryResult
    {
        public BoletoAvulsoQueryResult()
        {
            ListaContrato = new List<DictionaryEntry>();
            ListaBanco = new List<DictionaryEntry>();
        }
        public int? ClienteId { get; set; }
        public string CodCliente { get; set; }
        public string NomeCliente { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public int? QtdContrato { get; set; }
        public int? DiasAtraso { get; set; }
        public decimal? ValorAtraso { get; set; }
        public short NumeroBanco { get; set; }
        public string NumeroDocumento { get; set; }
        public decimal? ValorDocumento { get; set; }
        public string MensagemImpressa { get; set; }
        public string Justificativa { get; set; }

        public string CodClienteDecrypt
        {
            get { return BaseRotinas.Descriptografar(CodCliente); }
            set { CodCliente = BaseRotinas.Descriptografar(value); }
        }
        public string NomeClienteDecrypt
        {
            get { return BaseRotinas.Descriptografar(NomeCliente); }
            set { NomeCliente = BaseRotinas.Descriptografar(value); }
        }

        public string CPFDecrypt
        {
            get { return BaseRotinas.Descriptografar(CPF); }
            set { CPF = BaseRotinas.Descriptografar(value); }
        }

        public string EmailDecrypt
        {
            get { return BaseRotinas.Descriptografar(Email); }
            set { Email = BaseRotinas.Descriptografar(value); }
        }

        public List<DictionaryEntry> ListaContrato { get; set; }
        public List<DictionaryEntry> ListaBanco { get; set; }
        public DateTime? DataVencimento { get; set; }
    }
}
