﻿using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Helpers.Models;
using Cobranca.Shared.Repositories;
using System;
using System.Collections.Generic;

namespace Cobranca.Domain.BoletoContext.Repositories
{
    public interface IBoletoRepository : IBaseRepository<ParcelaBoleto>
    {
        bool UpdateLinhaDigitavelParcela(int parcelaId, string linhaDigitavel);

        int CreateOrUpdateNossoNumero(ParcelaBoleto boleto);

        BoletoAvulsoQueryResult GetBoletoAvulsoView(int cliente);

        bool ExisteBoletoPendentePorParcelaId(int codParcela);

        bool ExisteBoletoPendenteByContratoId(int codBoleto);

        void ReprovarBoletoAntigosDestaParcela(int boletoId, int parcelaId, int sinteseId, string obs);

        void ReprovarBoletoAntigosDesteContrato(int boletoId, int contratoId, int sinteseId, string obs);

        DTResult GetBoletosFromListResult(short fluxo, int? usuarioId, int? CodFase, int? CodSintese, string filiais, DTParameters parameters);

        bool ParcelaJaPossuiuClassificacaoC6(int codParcela);

        List<BoletoRemessaQueryResult> GetBoletosFromListRemessa(int bancoId, DateTime? periodoDe, DateTime? periodoAte, List<int> boletoIds);


        void AtualizarBoletosSetRemessaEmitida(List<int> boletoIds, int codLogArquivo, int usuarioId);
    }
}
