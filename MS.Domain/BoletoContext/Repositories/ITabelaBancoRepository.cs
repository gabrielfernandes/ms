﻿using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Shared.Repositories;
using System.Collections.Generic;

namespace Cobranca.Domain.BoletoContext.Repositories
{
    public interface ITabelaBancoRepository : IBaseRepository<TabelaBanco>
    {
        List<TabelaBanco> GetAllBancos();
    }
}
