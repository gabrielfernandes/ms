﻿using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Shared.Repositories;

namespace Cobranca.Domain.BoletoContext.Repositories
{
    public interface IParametroSinteseRepository : IBaseRepository<ParametroSintese>
    {
        bool ExisteSinteseDePara(int codSinteseDe);
        int ObterSintesePara(int codSinteseDe);
        int? ObterSinteseDeEtapa(EBoletoEtapa etapa);
        int? ObterSinteseParaEtapa(EBoletoEtapa etapa);
        EBoletoEtapa? ObterEtapaDaSintese(int codSintese);
    }
}
