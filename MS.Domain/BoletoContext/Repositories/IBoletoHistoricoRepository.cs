﻿using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Shared.Repositories;

namespace Cobranca.Domain.BoletoContext.Repositories
{
    public interface IBoletoHistoricoRepository : IBaseRepository<BoletoHistorico>
    {
        bool UpdateBoletoSetLastHistory(int codBoleto);

        BoletoHistorico GetLastHistoryByCodBoleto(int codBoleto);

        string GetLastObservacaoByCodBoleto(int codBoleto);
    }
}
