﻿using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Shared.Repositories;
using System.Collections.Generic;

namespace Cobranca.Domain.BoletoContext.Repositories
{
    public interface IBancoPortadorRepository : IBaseRepository<BancoPortador>
    {
        BancoPortadorQueryResult GetByParcelaId(int codParcela);
        BancoPortadorQueryResult GetByBancoPortadorId(int bancoPortadorId);
        BancoPortadorQueryResult GetBancoPortadorByCodContratoAndNumeroBanco(int codContrato, short numeroBanco);
        List<BancoPortadorInstrucaoQueryResult> GetInstrucoesByBancoPortadorId(int bancoPortadorId);
        int GerarNossoNumero(int bancoPortadorId);
        int GerarNumeroLote(int bancoPortadorId);
        List<BancoPortadorQueryResult> GetListBancoPortadorByRegionalGrupoId(string regionalGrupoIds);
    }
}
