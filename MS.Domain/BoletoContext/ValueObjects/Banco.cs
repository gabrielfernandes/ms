﻿using Cobranca.Shared.ValueObjects;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cobranca.Domain.BoletoContext.ValueObjects
{
    public class Banco : ValueObject
    {
        protected Banco() { }

        public Banco(int? codBanco, short numeroBanco, string nomeBanco)
        {
            Cod = codBanco;
            NumeroBanco = numeroBanco;
            NomeBanco = nomeBanco;
        }

        public int? Cod { get; private set; }
        [NotMapped]
        public short NumeroBanco { get; private set; }
        [NotMapped]
        public string NomeBanco { get; private set; }


        public void SetCodBanco(int cod)
        {
            Cod = cod;
        }
        
        private bool Validate()
        {
            return true;
        }
    }
}
