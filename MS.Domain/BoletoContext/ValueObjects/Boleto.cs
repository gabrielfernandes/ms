﻿using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Shared.ValueObjects;
using System;

namespace Cobranca.Domain.BoletoContext.ValueObjects
{
    public class Boleto : ValueObject
    {
        protected Boleto() { }

        public Boleto(int? codUsuario, DateTime dataVencimento, string numeroDocumento, decimal valorDocumento, EBoletoStatusType status, EBoletoSolicitacaoTipo tipoSolicitacao, string mensagemImpressa)
        {
            CodUsuario = codUsuario;
            DataVencimento = dataVencimento;
            NumeroDocumento = numeroDocumento;
            ValorDocumento = valorDocumento;
            Status = (short)status;
            TipoSolicitacao = (short)tipoSolicitacao;
            MensagemImpressa = mensagemImpressa;
        }
        public Boleto(int? codUsuario, DateTime dataVencimento, string numeroDocumento, decimal valorDocumento, decimal? vrDesconto, int? nossoNumero, string linhaDigitavel, EBoletoStatusType status, EBoletoSolicitacaoTipo tipoSolicitacao, string mensagemImpressa)
        {
            CodUsuario = codUsuario;
            DataVencimento = dataVencimento;
            NumeroDocumento = numeroDocumento;
            ValorDocumento = valorDocumento;
            ValorDesconto = vrDesconto;
            NossoNumero = nossoNumero;
            LinhaDigitavel = linhaDigitavel;
            Status = (short)status;
            TipoSolicitacao = (short)tipoSolicitacao;
            MensagemImpressa = mensagemImpressa;
        }

        public int? CodUsuario { get; private set; }
        public DateTime DataVencimento { get; private set; }
        public string NumeroDocumento { get; private set; }
        public Decimal ValorDocumento { get; private set; }
        public Nullable<decimal> ValorDesconto { get; private set; }
        public int? NossoNumero { get; private set; }
        public string LinhaDigitavel { get; private set; }
        public short? Status { get; private set; }
        public DateTime? DataStatus { get; private set; }
        public int? CodUsuarioStatus { get; private set; }
        public DateTime? DataAprovacao { get; private set; }
        public string MensagemImpressa { get; private set; }
        public short? TipoSolicitacao { get; private set; }


        public void SetNossoNumero(int nossoNumero)
        {
            NossoNumero = nossoNumero;
        }

        public void SetValorDocumento(decimal valorDocumento)
        {
            ValorDocumento = valorDocumento;
        }
        public void SetLinhaDigitavel(string linhaDigitavel)
        {
            LinhaDigitavel = linhaDigitavel;
        }

        public void ConfigurarEtapa(EBoletoEtapa etapa, int usuarioId)
        {
            if (etapa == EBoletoEtapa.AtendenteSolicitaProrrogacao)
            {
                this.Status = (short)EBoletoStatusType.Aprovado;
                this.DataStatus = DateTime.Now;
                this.DataAprovacao = DateTime.Now;
                this.CodUsuarioStatus = usuarioId;
            }
            else if (etapa == EBoletoEtapa.AtendenteSolicitaBoletoAvulso)
            {
                this.Status = (short)EBoletoStatusType.SolicitacaoAnalista;
                this.DataStatus = DateTime.Now;
                this.CodUsuarioStatus = usuarioId;
            }
            else if (etapa == EBoletoEtapa.SupervisorAprovado)
            {
                this.Status = (short)EBoletoStatusType.Aprovado;
                this.DataStatus = DateTime.Now;
                this.DataAprovacao = DateTime.Now;
                this.CodUsuarioStatus = usuarioId;
            }
            else if (etapa == EBoletoEtapa.AnalistaReprova || etapa == EBoletoEtapa.SupervisorReprova)
            {
                this.Status = (short)EBoletoStatusType.Reprovado;
                this.DataStatus = DateTime.Now;
                this.CodUsuarioStatus = usuarioId;
            }
            else
            {
                this.Status = (short)EBoletoStatusType.SolicitacaoAnalista;
                this.DataStatus = DateTime.Now;
                this.CodUsuarioStatus = usuarioId;
            }
        }

        private bool Validate()
        {
            return true;
        }
    }
}
