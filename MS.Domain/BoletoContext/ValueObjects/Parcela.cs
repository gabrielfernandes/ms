﻿using Cobranca.Shared.ValueObjects;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cobranca.Domain.BoletoContext.ValueObjects
{
    public class Parcela : ValueObject
    {
        public Parcela()
        {

        }
        public Parcela(int cod, DateTime dataVencimento, decimal valorParcela, decimal? valorImposto, decimal valorAberto, int? nossoNumero, DateTime? dataEmissaoFatura,string instrPgto, string companhia, string companhiaFiscal)
        {
            Cod = cod;
            DataVencimento = dataVencimento;
            ValorParcela = valorParcela;
            ValorImposto = valorImposto;
            ValorAberto = valorAberto;
            NossoNumero = nossoNumero;
            DataEmissaoFatura = dataEmissaoFatura;
            InstrucaoPagamento = instrPgto;
            Companhia = companhia;
            CompanhiaFiscal = companhiaFiscal;
        }

        public int? Cod { get; private set; }
        [NotMapped]
        public DateTime DataVencimento { get; private set; }
        [NotMapped]
        public decimal ValorParcela { get; private set; }
        [NotMapped]
        public decimal? ValorImposto { get; private set; }
        [NotMapped]
        public decimal ValorAberto { get; private set; }
        [NotMapped]
        public int? NossoNumero { get; private set; }
        [NotMapped]
        public DateTime? DataEmissaoFatura { get; private set; }
        [NotMapped]
        public string InstrucaoPagamento { get; private set; }
        [NotMapped]
        public string Companhia { get; private set; }
        [NotMapped]
        public string CompanhiaFiscal { get; private set; }


        public void SetNossoNumero(int nossoNumero)
        {
            NossoNumero = nossoNumero;
        }


        private bool Validate()
        {
            return true;
        }
    }
}
