﻿using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;
using System;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs
{
    public class RegisterBoletoCommand : Notifiable, ICommand
    {
        public int CodUsuario { get; set; }
        public int CodParcela { get; set; }
        public int ClienteId { get; set; }
        public string FluxoPagamento { get; set; }
        public int? ParcelaNossoNumero { get; set; }
        public DateTime ParcelaDataVencimento { get; set; }
        public decimal ParcelaValorAberto { get; set; }
        public decimal ParcelaValorParcela { get; set; }
        public decimal? ParcelaValorImposto { get; set; }
        public DateTime? ParcelaDataEmissaoFatura { get; set; }
        public string ParcelaCodigoEmpresaCobranca { get; set; }
        public string ParcelaLinhaDigitavel { get; set; }
        public int PeriodoEmMes { get; set; }
        public string InstrucaoPagamento { get; set; }
        public string Companhia { get; set; }
        public string CompanhiaFiscal { get; set; }
        public int? qtdDias { get; set; }

        public int? CodBoleto { get; set; }
        public string NumeroDocumento { get; set; }
        public DateTime DataVencimento { get; set; }
        public DateTime DataDocumento { get; set; }
        public decimal JurosAoMesPorcentagem { get; set; }
        public decimal MultaAtrasoPorcentagem { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal ValorDocumento { get; set; }
        public string MensagemImpressa { get; set; }
        public string CodigoCliente { get; set; }
        public string Justificativa { get; set; }
        public EBoletoSolicitacaoTipo TipoSolicitacao { get; set; }

        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .IsGreaterThan(CodParcela, 0, "CodParcela", "Parcela não enviado!")
            );
            return Valid;
        }
    }
}
