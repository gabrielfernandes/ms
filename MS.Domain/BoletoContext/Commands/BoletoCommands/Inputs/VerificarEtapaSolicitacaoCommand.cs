﻿using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs
{
    public class VerificarEtapaSolicitacaoCommand : Notifiable, ICommand
    {
        public VerificarEtapaSolicitacaoCommand(decimal jurosAoMesPorcentagemPrevisto, decimal jurosAoMesPorcentagem, decimal multaAtrasoPorcentagemPrevisto, decimal multaAtrasoPorcentagem, decimal valorDesconto)
        {
            JurosAoMesPorcentagemPrevisto = jurosAoMesPorcentagemPrevisto;
            JurosAoMesPorcentagem = jurosAoMesPorcentagem;
            MultaAtrasoPorcentagemPrevisto = multaAtrasoPorcentagemPrevisto;
            MultaAtrasoPorcentagem = multaAtrasoPorcentagem;
            ValorDesconto = valorDesconto;
        }

        public decimal JurosAoMesPorcentagemPrevisto { get; private set; }
        public decimal JurosAoMesPorcentagem { get; private set; }
        public decimal MultaAtrasoPorcentagemPrevisto { get; private set; }
        public decimal MultaAtrasoPorcentagem { get; private set; }
        public decimal ValorDesconto { get; private set; }

        bool ICommand.Valid()
        {
            return Valid;
        }
    }
}
