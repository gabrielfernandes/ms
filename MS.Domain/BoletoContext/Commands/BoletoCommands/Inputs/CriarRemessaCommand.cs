﻿using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;
using System.Collections.Generic;
using System;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs
{
    public class CriarRemessaCommand : Notifiable, ICommand
    {
        public List<int> BoletoIds { get; set; }
        public int CodUsuario { get;  set; }
        public DateTime? PeriodoDe { get; set; }
        public DateTime? PeriodoAte { get; set; }

        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .IsNotNull(BoletoIds, "Boletos", "Selecione ao menos um boleto!")
            );
            return Valid;
        }
    }
}
