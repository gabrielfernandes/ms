﻿using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs
{
    public class RegistrarBoletoHistoricoCommand : Notifiable, ICommand
    {
        public int Cod { get; set; }
        public Nullable<int> CodSintese { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public string Observacao { get; set; }
        public Nullable<int> CodMotivoAtraso { get; set; }
        public Nullable<int> CodResolucao { get; set; }
        public Nullable<System.DateTime> DataAgenda { get; set; }
        public Nullable<int> CodImportacao { get; set; }
        public Nullable<int> CodBoleto { get; set; }

        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .IsGreaterThan((int)CodBoleto, 0, "CodBoleto", "Parcela não enviado!")
            );
            return Valid;
        }
    }
}
