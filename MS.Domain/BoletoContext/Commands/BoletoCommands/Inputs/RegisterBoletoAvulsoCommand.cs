﻿using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;
using System;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs
{
    public class RegisterBoletoAvulsoCommand : Notifiable, ICommand
    {
        public int CodBoleto { get; set; }
        public int ClienteId { get; set; }
        public int? CodBancoPortador { get; set; }
        public short BancoNumero { get; set; }
        public DateTime DataVencimento { get; set; }
        public int? CodContrato { get; set; }
        public string NumeroDocumento { get; set; }
        public int CodUsuario { get; set; }
        public string Justificativa { get; set; }
        public string MensagemImpresa { get; set; }
        public Decimal ValorDocumento { get; set; }
        public string ParcelaLinhaDigitavel { get; set; }
        public EBoletoSolicitacaoTipo TipoSolcitacao { get; set; }


        public void SetNumeroDocumento(string numeroContrato)
        {
            NumeroDocumento = numeroContrato;
        }
        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .IsGreaterThan(ClienteId, 0, "Cliente", "Cliente não enviado!")
            );
            return Valid;
        }

        
    }
}
