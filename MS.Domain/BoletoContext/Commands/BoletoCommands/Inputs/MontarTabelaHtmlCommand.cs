﻿using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;
using System;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs
{
    public class MontarTabelaHtmlCommand : Notifiable, ICommand
    {
        public string NumeroContrato { get; set; }
        public decimal ValorDocumento { get; set; }
        public DateTime DataVencimento { get; set; }
        public string NumeroDocumento { get; set; }
        public decimal ValorParcelaAberto { get; set; }
        public string NumeroParcela { get; set; }
        public DateTime ParcelaDataVencimento { get; set; }        

        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .IsGreaterThan(ValorParcelaAberto, 0, "Valor Aberto", $"A parcela possui o valor em aberto de {ValorParcelaAberto}!")
            );
            return Valid;
        }
    }
}
