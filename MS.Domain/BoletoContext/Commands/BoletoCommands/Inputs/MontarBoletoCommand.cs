﻿using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;
using System;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs
{
    public class MontarBoletoCommand : Notifiable, ICommand
    {
        public int ClienteId { get; set; }
        public int CodBoleto { get; set; }
        public int CodContrato { get; set; }
        public string Assunto { get; set; }
        public string Mensagem { get; set; }

        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .IsGreaterThan(CodBoleto, 0, "CodBoleto", "Boleto inválido!")
            );
            return Valid;
        }
    }
}
