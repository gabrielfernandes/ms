﻿using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs
{
    public class ValidarBoletoExistenteCommand : Notifiable, ICommand
    {
        public int CodParcela { get; set; }
        public decimal? ValorAberto { get; set; }
        public decimal? ValorParcela { get; set; }
        public string CodigoEmpresaCobranca { get; set; }
        public int? NossoNumero { get; set; }
        public int ClienteId { get; set; }
        public string FluxoPagamento { get; set; }


        public int? qtdDias { get; set; }
        public int CodBoleto { get; set; }

        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .IsGreaterThan(CodParcela, 0, "CodParcela", "Parcela não enviado!")
            );
            return Valid;
        }
    }
}
