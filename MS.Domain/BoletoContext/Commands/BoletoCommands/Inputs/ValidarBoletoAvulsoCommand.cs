﻿using Cobranca.Shared.Commands;
using JunixValidator;
using JunixValidator.Validation;
using System.Collections.Generic;
using System.Linq;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs
{
    public class ValidarBoletoAvulsoCommand : Notifiable, ICommand
    {
        public int ClienteId { get; set; }
        public string Contratos { get; set; }
        public string Justificativa { get; set; }

        public List<string> ListaContratos
        {
            get { return Contratos != null ? Contratos.Split(',').Select(x => x).ToList() : null; }
        }

        bool ICommand.Valid()
        {
            AddNotifications(new ValidationContract()
                .IsGreaterThan(ClienteId, 0, "Cliente", "Cliente não enviado!")
                .IsNotNullOrEmpty(Contratos, "Contrato", "Selecione ao menos um contrato")
            );
            return Valid;
        }
    }
}
