﻿using Cobranca.Shared.Commands;
using System.Collections.Generic;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Outputs
{
    public class MontarBoletoCommandResult : ICommandResult
    {
        public MontarBoletoCommandResult(bool success, string message, string assunto, BoletoNet.BoletoBancario boleto, string email, string mensagem)
        {
            Success = success;
            Message = message;
            Assunto = assunto;
            Boleto = boleto;
            Email = email;
            Mensagem = mensagem;
        }

        public MontarBoletoCommandResult(bool success, string message, object data)
        {
            Success = success;
            Message = message;
            Data = data;
        }

        public string Assunto { get; set; }
        public BoletoNet.BoletoBancario Boleto { get; set; }
        public string Email { get; set; }
        public string Mensagem { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
