﻿using Cobranca.Shared.Commands;
using System.Collections.Generic;

namespace Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Outputs
{
    public class CriarRemessaCommandResult : ICommandResult
    {
        public CriarRemessaCommandResult(bool success, string message, List<int> listaBoletoOK, List<int> arquivoLogIds, List<byte[]> arquivos)
        {
            Success = success;
            Message = message;
            Arquivos = arquivos;
            ListaBoletoOK = listaBoletoOK;
            ArquivoLogIds = arquivoLogIds;
        }

        public CriarRemessaCommandResult(bool success, string message, object data)
        {
            Success = success;
            Message = message;
            Data = data;
        }

        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public List<int> ArquivoLogIds { get; set; }
        public List<int> ListaBoletoOK { get; set; }
        public List<byte[]> Arquivos { get; set; }
    }
}
