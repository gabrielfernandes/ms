﻿using Cobranca.Domain.BoletoContext.ValueObjects;
using Cobranca.Shared.Entities;
using System;

namespace Cobranca.Domain.BoletoContext.Entities
{
    public class BancoPortadorLogArquivo : Entity<int>
    {
        //ENTITY FRAMEWORK
        protected BancoPortadorLogArquivo() { }

        public BancoPortadorLogArquivo(string nomeDocumento, byte[] arquivo, int? boletoTotalArquivo, int? codUsuario)
        {
            NomeDocumento = nomeDocumento;
            Arquivo = arquivo;
            BoletoTotalArquivo = boletoTotalArquivo;
            CodUsuario = codUsuario;
        }

        public Nullable<int> CodBancoPortador { get; private set; }
        public string NomeDocumento { get; private set; }
        public string NumeroLote { get; private set; }
        public Nullable<short> TipoArquivo { get; private set; }
        public string Banco { get; private set; }
        public string Companhia { get; private set; }
        public byte[] Arquivo { get; private set; }
        public Nullable<int> BoletoTotalArquivo { get; private set; }
        public Nullable<DateTime> DataProcessamento { get; private set; }
        public Nullable<int> CodUsuario { get; private set; }
        public string Log { get; private set; }


    }
}
