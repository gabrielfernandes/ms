﻿using Cobranca.Domain.BoletoContext.ValueObjects;
using Cobranca.Shared.Entities;
using System;

namespace Cobranca.Domain.BoletoContext.Entities
{
    public class TabelaBanco : Entity<int>
    {

        //ENTITY FRAMEWORK
        protected TabelaBanco() { }

        public string NomeBanco { get; private set; }
        public int Numero { get; private set; }
        public string CodCliente { get; private set; }
        public Nullable<int> NossoNumero { get; private set; }
        public Nullable<int> NossoNumeroAtual { get; private set; }
        public Nullable<int> Lote { get; private set; }
        public Nullable<int> LoteAtual { get; private set; }
        public Nullable<bool> BancoPadrao { get; private set; }

    }
}
