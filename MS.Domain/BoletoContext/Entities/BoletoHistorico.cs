﻿using Cobranca.Shared.Entities;

namespace Cobranca.Domain.BoletoContext.Entities
{
    public class BoletoHistorico : Entity<int>
    {
        private int? codUsuario;
        private int v;
        private string justificativa;

        protected BoletoHistorico() { }

        public BoletoHistorico(int codBoleto, int codUsuario, int codSintese, string observacao)
        {
            CodBoleto = codBoleto;
            CodUsuario = codUsuario;
            CodSintese = codSintese;
            Observacao = observacao;
        }

      
        public int CodBoleto { get; private set; }
        public int CodUsuario { get; private set; }
        public int CodSintese { get; private set; }
        public string Observacao { get; private set; }

    }
}
