﻿using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Domain.BoletoContext.ValueObjects;
using Cobranca.Helpers;
using Cobranca.Shared.Entities;
using System;

namespace Cobranca.Domain.BoletoContext.Entities
{
    public class ParcelaBoleto : Entity<int>
    {
        public ParcelaBoleto(
            ValueObjects.Boleto boleto,
            decimal jurosAoMesPorcentagem,
            decimal multaAtrasoPorcentagem,
            int codCliente,
            string codigoCliente,
            Parcela parcela,
            decimal jurosPrevistoPorcentagem,
            decimal multaPrevistaPorcentagem,
            short? bancoNumero,
            int? codBancoPortador)
        {
            Boleto = boleto;
            CodCliente = codCliente;
            CodigoCliente = codigoCliente;
            Parcela = parcela;
            PeriodoEmMes = BaseRotinas.DiferencaMes(parcela.DataVencimento, boleto.DataVencimento);
            JurosAoMesPorcentagem = jurosAoMesPorcentagem;
            MultaAtrasoPorcentagem = multaAtrasoPorcentagem;
            JurosAoMesPorcentagemPrevisto = jurosPrevistoPorcentagem;
            MultaAtrasoPorcentagemPrevisto = multaPrevistaPorcentagem;
            DataDocumento = parcela.DataVencimento;
            BancoNumero = bancoNumero;
            CodBancoPortador = codBancoPortador;

        }

        public ParcelaBoleto(
            ValueObjects.Boleto boleto,
            int clienteId,
            string codigoCliente,
            short bancoNumero,
            int bancoPortador,
            DateTime dataDocumento,
            decimal jurosAoMesPorcentagem,
            decimal multaAtrasoPorcentagem
            )
        {
            Boleto = boleto;
            Parcela = new Parcela();
            ValorDocumentoOriginal = boleto.ValorDocumento;
            CodCliente = clienteId;
            CodigoCliente = codigoCliente;
            BancoNumero = bancoNumero;
            CodBancoPortador = bancoPortador;
            DataDocumento = dataDocumento;
            JurosAoMesPorcentagem = jurosAoMesPorcentagem;
            JurosAoMesPorcentagemPrevisto = jurosAoMesPorcentagem;
            MultaAtrasoPorcentagem = multaAtrasoPorcentagem;
            MultaAtrasoPorcentagemPrevisto = multaAtrasoPorcentagem;
        }

        //CONGELAR INFORMACOES DA PARCELA NO MOMENTO DA SOLICITACAO
        public void CongelarInformacoesParcela()
        {
            this.Companhia = this.Parcela.Companhia;
            this.Imposto = this.Parcela.ValorImposto;
            this.ValorDocumentoAberto = this.Parcela.ValorAberto;
            this.InstrucaoPagamento = this.Parcela.InstrucaoPagamento;
        }

        public  void CongelarInformacoesBoletoAvulso(int codContrato)
        {
            CodContrato = codContrato;
        }

        public void CalcularValorOriginal()
        {
            this.ValorDocumentoOriginal = Parcela.ValorAberto == Parcela.ValorParcela ? ((decimal)Parcela.ValorAberto - Parcela.ValorImposto) : Parcela.ValorAberto;
        }

        public void CalcularValoresPrevisto()
        {
            if (ValorDocumentoOriginal == null)
                this.CalcularValorOriginal();

            this.ValorJurosPrevisto = BoletoUtils.CalcularValorJurosSimples((decimal)this.ValorDocumentoOriginal, JurosAoMesPorcentagemPrevisto, Parcela.DataVencimento, (DateTime)this.Boleto.DataVencimento);
            this.ValorMultaPrevisto = BoletoUtils.CalcularValorMulta(this.ValorDocumentoOriginal, MultaAtrasoPorcentagemPrevisto, Parcela.DataVencimento, (DateTime)this.Boleto.DataVencimento);
            this.ValorDocumentoPrevisto = (decimal)this.ValorJurosPrevisto + (decimal)this.ValorMultaPrevisto + (Parcela.ValorAberto);
        }

        public decimal CalcularValorDocumento()
        {
            this.ValorJuros = BoletoUtils.CalcularValorJurosSimples((decimal)ValorDocumentoOriginal, (decimal)JurosAoMesPorcentagem, Parcela.DataVencimento, this.Boleto.DataVencimento);
            this.ValorMulta = BoletoUtils.CalcularValorMulta((decimal)ValorDocumentoOriginal, (decimal)MultaAtrasoPorcentagem, Parcela.DataVencimento, this.Boleto.DataVencimento);
            decimal vrDocumento = BaseRotinas.ConverterParaDecimal(this.ValorJuros + (decimal)this.ValorMulta + (decimal)this.ValorDocumentoOriginal);
            vrDocumento = vrDocumento - (decimal)this.Boleto.ValorDesconto;
            return vrDocumento;
        }

        internal void DefinirBoletoAlterarcao()
        {
            this.Alterar = true;
            this.Boleto.SetNossoNumero((int)this.Parcela.NossoNumero);
        }


        //ENTITY FRAMEWORK
        protected ParcelaBoleto() { }

        public Nullable<decimal> JurosAoMesPorcentagem { get; private set; }
        public Nullable<decimal> MultaAtrasoPorcentagem { get; private set; }
        public Nullable<decimal> ValorJuros { get; private set; }
        public Nullable<decimal> ValorMulta { get; private set; }
        public ValueObjects.Boleto Boleto { get; private set; }


        public DateTime DataDocumento { get; set; }
        public decimal JurosAoMesPorcentagemPrevisto { get; private set; }
        public decimal MultaAtrasoPorcentagemPrevisto { get; private set; }
        public Nullable<decimal> ValorJurosPrevisto { get; private set; }
        public Nullable<decimal> ValorMultaPrevisto { get; private set; }
        public Nullable<decimal> ValorDocumentoPrevisto { get; private set; }
        public Nullable<decimal> ValorDocumentoOriginal { get; private set; }
        public Nullable<int> PeriodoEmMes { get; private set; }
        public int? CodHistorico { get; private set; }
        public Nullable<int> CodLote { get; private set; }
        public Nullable<decimal> Imposto { get; private set; }
        public Nullable<decimal> ValorDocumentoAberto { get; private set; }
        public string Companhia { get; private set; }
        public string InstrucaoPagamento { get; private set; }
        public string CodigoCliente { get; private set; }
        public int CodCliente { get; private set; }
        public int? CodContrato { get; private set; }
        public bool? Alterar { get; private set; }
        public short? BancoNumero { get; private set; }
        public int? CodBancoPortador { get; private set; }

        public Parcela Parcela { get; private set; }


    }
}
