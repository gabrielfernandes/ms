﻿using Cobranca.Shared.Entities;
using System;

namespace Cobranca.Domain.BoletoContext.Entities
{
    public class ParametroSintese : Entity<int>
    {
        public ParametroSintese(int? codSinteseDe, int? codSintesePara, int? codUsuario, short? etapa, string observacao)
        {
            CodSinteseDe = codSinteseDe;
            CodSintesePara = codSintesePara;
            CodUsuario = codUsuario;
            Etapa = etapa;
            Observacao = observacao;
        }

        // ENTITY FRAMEWORK
        protected ParametroSintese() { }


        public int? CodSinteseDe { get; private set; }
        public int? CodSintesePara { get; private set; }
        public int? CodUsuario { get; private set; }
        public short? Etapa { get; private set; }
        public short? Tipo { get; private set; }
        public string Observacao { get; private set; }




    }
}


