﻿using Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Inputs;
using Cobranca.Domain.BoletoContext.Commands.BoletoCommands.Outputs;
using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Domain.BoletoContext.Repositories;
using Cobranca.Domain.BoletoContext.ValueObjects;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Helpers;
using Cobranca.Shared.Commands;
using JunixValidator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cobranca.Domain.BoletoContext.Handlers
{
    public class BoletoHandler :
        Notifiable,
        ICommandHandler<ValidarBoletoExistenteCommand>,
        ICommandHandler<RegisterBoletoCommand>,
        ICommandHandler<MontarBoletoCommand>,
        ICommandHandler<MontarTabelaHtmlCommand>,
        ICommandHandler<ValidarBoletoAvulsoCommand>,
        ICommandHandler<RegisterBoletoAvulsoCommand>,
        ICommandHandler<RegistrarBoletoHistoricoCommand>
    {
        private readonly IBoletoRepository _repository;
        private readonly IBancoPortadorRepository _repBancoPortador;
        private readonly IBoletoHistoricoRepository _repBoletoHistorico;
        private readonly IParametroSinteseRepository _repParametroSintese;
        private readonly ISinteseRepository _repSintese;
        private readonly IClienteRepository _repCliente;
        private readonly IContratoRepository _repContrato;
        private readonly IHistoricoRepository _repHistorico;
        private readonly IBancoPortadorLogArquivoRepository _repBancoPortadorLogArquivo;
        private readonly ITabelaBancoRepository _repBanco;

        public BoletoHandler(IBoletoRepository repository, IBancoPortadorRepository repBancoPortador, IBoletoHistoricoRepository repBoletoHistorico, IParametroSinteseRepository repParamSintese, IClienteRepository repCliente, IContratoRepository repContrato, ISinteseRepository repSintese, IHistoricoRepository repHistorico, IBancoPortadorLogArquivoRepository repBancoPortadorLogArquivo, ITabelaBancoRepository repBanco)
        {
            _repository = repository;
            _repBancoPortador = repBancoPortador;
            _repBoletoHistorico = repBoletoHistorico;
            _repParametroSintese = repParamSintese;
            _repCliente = repCliente;
            _repContrato = repContrato;
            _repSintese = repSintese;
            _repHistorico = repHistorico;
            _repBancoPortadorLogArquivo = repBancoPortadorLogArquivo;
            _repBanco = repBanco;

        }

        public ICommandResult Handle(ValidarBoletoExistenteCommand command)
        {
            var paramBanco = _repBancoPortador.GetByParcelaId(command.CodParcela);

            if (command.CodParcela > 0)
            {
                var sinteseC6 = _repSintese.GetSinteseByCodigoSinteseCliente("C6");
                var historicos = _repHistorico.GetHistoricoByParcelaId(command.CodParcela);
                if (historicos.Any(x => x.CodSintese == (int)sinteseC6.CodSintese))
                {
                    // CASO ALGUM DIA A PARCELA JÁ POSSUIU A CLASSIFICACAO C6
                    return new CommandResult(false, $"A Parcela ({command.FluxoPagamento}) só poderá ser emitida como um novo boleto, pois já foi classificada como {sinteseC6.CodigoSinteseCliente}!");
                }
            }


            if (command.ValorAberto != command.ValorParcela || (!command.NossoNumero.HasValue || command.NossoNumero == 0))
            {
                //CASO A PARCELA FOR PARCIALMENTE PAGA OU AINDA NAO POSSUIR REGISTRO NO BANCO (NOSSO NUMERO) DEVERÁ SER EMITIDA COMO NOVO BOLETO
                return new CommandResult(false, $"A Parcela ({command.FluxoPagamento}) só poderá ser emitida como um novo boleto!");
            }
            else if (command.CodigoEmpresaCobranca == "BNS" || command.CodigoEmpresaCobranca == "FAS")
            {
                //SE A PARCELA PERTENCER A ESTAS DETERMINADAS EMPRESA DE COBRANÇA
                return new CommandResult(false, $"A Parcela ({command.FluxoPagamento}) esta com a empresa de cobrança {command.CodigoEmpresaCobranca} !");
            }
            //else if (command.qtdDias >= paramBanco.NumeroDiaRegistroBoleto && paramBanco.TabelaRegionalGrupo?.TabelaConstrutora.CodCliente == dados.Companhia)
            else if (command.qtdDias >= paramBanco.NumeroDiaRegistroBoleto)
            {
                //VERIFICAR PELA COMPANHIA DA PARCELA 0400 ou 0405 A Qtde de dias registrado junto ao banco
                return new CommandResult(false, $"Esta Parcela ({command.FluxoPagamento}) possui um registro no banco há mais de {paramBanco.NumeroDiaRegistroBoleto} dias e só poderá ser emitida como um novo boleto!");
            }

            // Checar as notificações
            if (Invalid)
                return new CommandResult(false, "Não foi possível realizar sua solicitação");

            return new CommandResult(true, "Validação realizada com sucesso");
        }

        public ICommandResult Handle(RegisterBoletoCommand command)
        {
            try
            {
                if (!(command.DataVencimento.Date > DateTime.Now.AddDays(1).Date))
                    AddNotification("DataVencimento", "O Vencimento é inválido!");

                // Checar as notificações
                if (Invalid)
                    return new CommandResult(false, "Não foi possível realizar sua solicitação", Notifications);

                var paramBanco = _repBancoPortador.GetByParcelaId(command.CodParcela);

                var parcela = new Parcela(command.CodParcela, command.ParcelaDataVencimento, command.ParcelaValorParcela, command.ParcelaValorImposto, command.ParcelaValorAberto, command.ParcelaNossoNumero, command.ParcelaDataEmissaoFatura, command.InstrucaoPagamento, command.Companhia, command.CompanhiaFiscal);
                var boletoPadrao = new ValueObjects.Boleto(command.CodUsuario, command.DataVencimento, command.NumeroDocumento, command.ValorDocumento, command.ValorDesconto, command.ParcelaNossoNumero, command.ParcelaLinhaDigitavel, EBoletoStatusType.SolicitacaoAtendente, command.TipoSolicitacao, command.MensagemImpressa);
                var entitie = new ParcelaBoleto(
                    boletoPadrao,
                    command.JurosAoMesPorcentagem,
                    command.MultaAtrasoPorcentagem,
                    command.ClienteId,
                    command.CodigoCliente,
                    parcela,
                    BaseRotinas.ConverterParaDecimal(paramBanco.JurosAoMesPorcentagem),
                    BaseRotinas.ConverterParaDecimal(paramBanco.MultaAtraso),
                    BaseRotinas.ConverterParaShort(paramBanco.NumeroBanco),
                    paramBanco.Cod
                    );

                entitie.CongelarInformacoesParcela(); //CONGELAR INFORMAÇÕES DA PARCELA NO DIA DA SOLICITAÇÃO

                entitie.CalcularValorOriginal(); //VALOR DOCUMENTO COM DESCONTO JA CALCULADO

                entitie.CalcularValoresPrevisto();//VALOR PREVISTO SE NÂO HOUVESSE ALGUM DESCONTO

                entitie.Boleto.SetValorDocumento(entitie.CalcularValorDocumento());//VALOR DO DOCUMENTO COM OS DESCONTOS CALCULADOS

                EBoletoEtapa Etapa = this.VerificarTipoEtapaSolicitacao(entitie);
                entitie.Boleto.ConfigurarEtapa(Etapa, command.CodUsuario);//CONFIGURAR A ETAPA DO BOLETO

                var entity = _repository.CreateOrUpdate(entitie);

                if (entitie.Boleto.Status == (short)EBoletoStatusType.Aprovado)
                {
                    ProcessarNossoNumero(entitie, paramBanco);// PROCESSAR O NOSSO NUMERO

                    var listaDigitavel = BoletoUtils.MontarLinhaDigitavel(
                    entitie.Boleto.ValorDocumento,
                    entitie.Boleto.DataVencimento,
                    entitie.Boleto.NumeroDocumento,
                    paramBanco.NumeroBanco.ToString(),
                    (int)entitie.Boleto.NossoNumero,
                    paramBanco.CNPJ,
                    paramBanco.Portador,
                    paramBanco.Agencia,
                    paramBanco.AgenciaDigito,
                    paramBanco.Conta,
                    paramBanco.ContaDigito,
                    paramBanco.Carteira.ToString(),
                    paramBanco.Endereco,
                    paramBanco.EnderecoNumero,
                    paramBanco.Bairro,
                    paramBanco.Cidade,
                    paramBanco.UF,
                    paramBanco.CEP);

                    entitie.Boleto.SetLinhaDigitavel(listaDigitavel);
                    _repository.UpdateLinhaDigitavelParcela((int)entitie.Parcela.Cod, entitie.Boleto.LinhaDigitavel);

                    ReprovarBoletosAbertoNaoAprovado(entitie);
                }
                //if (command.Contratos == null) AddNotification("Contrato", "Selecione ao menos um contrato!");

                //preencher historico
                var historico = new BoletoHistorico(entitie.Cod, command.CodUsuario, (int)_repParametroSintese.ObterSinteseParaEtapa(Etapa), command.Justificativa);
                historico = _repBoletoHistorico.CreateOrUpdate(historico);

                if (_repParametroSintese.ExisteSinteseDePara(historico.CodSintese))
                {
                    var sintesePara = _repParametroSintese.ObterSintesePara(historico.CodSintese);
                    _repBoletoHistorico.CreateOrUpdate(new BoletoHistorico(entitie.Cod, command.CodUsuario, (int)sintesePara, command.Justificativa));
                }

                _repBoletoHistorico.UpdateBoletoSetLastHistory(entitie.Cod);

                // Agrupar as Validações
                AddNotifications(command);

                // Checar as notificações
                if (Invalid)
                    return new CommandResult(false, "Não foi possível realizar sua solicitação", Notifications);

                return new CommandResult(true, "Validação realizada com sucesso");
            }
            catch (Exception ex)
            {
                AddNotification("Erro", $"Descricao:{ex.Message}");
                return new CommandResult(false, "Não foi possível realizar sua solicitação", Notifications);
            }
        }

        public ICommandResult Handle(MontarBoletoCommand command)
        {
            try
            {
                var boleto = _repository.FindById(command.CodBoleto);
                var cliente = _repCliente.FindById(boleto.CodCliente);
                var contrato = _repContrato.FindById(command.CodContrato);


                var email = BaseRotinas.Descriptografar(cliente.Email.Endereco);

                // Agrupar as Validações
                AddNotifications(command);

                /*************** TRECHO MODIFICADO **********************/
                var idBancoPortador = boleto.CodBancoPortador;
                if (idBancoPortador == null)
                {
                    /* CASO NAO ESTEJA PREENCHIDO O BANCO PORTADOR */
                    idBancoPortador = _repBancoPortador.GetByParcelaId((int)boleto.Parcela.Cod).Cod;
                }
                var paramBanco = _repBancoPortador.GetByBancoPortadorId((int)idBancoPortador);
                /*************** FIM TRECHO MODIFICADO **********************/

                var instrucoes = _repBancoPortador.GetInstrucoesByBancoPortadorId(paramBanco.Cod);

                #region Cedente
                var cedente = BoletoUtils.MontarCedente(
                                paramBanco.CNPJ,
                                paramBanco.Portador,
                                0,
                                paramBanco.Agencia,
                                paramBanco.Conta,
                                paramBanco.ContaDigito,
                                paramBanco.Carteira.ToString(),
                                paramBanco.Endereco,
                                paramBanco.EnderecoNumero,
                                paramBanco.Bairro,
                                paramBanco.Cidade,
                                paramBanco.UF,
                                paramBanco.CEP);
                #endregion

                #region Sacado
                var sacado = BoletoUtils.MontarSacado(
                    BaseRotinas.Descriptografar(cliente.Nome),
                    BaseRotinas.Descriptografar(cliente.Documento.Numero),
                    cliente.Endereco.Logradouro,
                    cliente.Endereco.Numero,
                    cliente.Endereco.CEP,
                    cliente.Endereco.Bairro,
                    cliente.Endereco.Cidade,
                    cliente.Endereco.UF,
                    cliente.Endereco.Complemento
                    );
                #endregion

                List<string> boletosHTML = new List<string>();

                var retorno = BoletoUtils.GerarBoletoPorLinhaDigitavel(
                    boleto.Boleto.LinhaDigitavel,
                    boleto.Boleto.DataVencimento,
                    boleto.Boleto.ValorDocumento,
                    cedente.Codigo,
                    cedente,
                    boleto.Boleto.NumeroDocumento,
                    (int)boleto.Boleto.NossoNumero,
                    boleto.Companhia,
                    sacado,
                    BaseRotinas.ConverterParaShort(paramBanco.NumeroBanco),
                    BaseRotinas.ConverterParaDecimal(boleto.ValorDocumentoAberto),
                    BaseRotinas.ConverterParaDecimal(boleto.ValorJuros),
                    BaseRotinas.ConverterParaDecimal(boleto.ValorMulta),
                    BaseRotinas.ConverterParaDecimal(boleto.Imposto),
                    BaseRotinas.ConverterParaDecimal(boleto.Boleto.ValorDesconto),
                    BaseRotinas.ConverterParaDecimal(boleto.Boleto.ValorDocumento),
                    (short)boleto.Boleto.TipoSolicitacao,
                    boleto.Boleto.MensagemImpressa,
                    BaseRotinas.ConverterParaDecimal(paramBanco.JurosAoMesPorcentagem),
                    BaseRotinas.ConverterParaDecimal(paramBanco.MultaAtraso),
                    instrucoes.Select(x => x.Descricao).ToList()
                    );
                // Checar as notificações
                if (Invalid)
                    return new MontarBoletoCommandResult(false, "Não foi possível realizar sua solicitação", Notifications);

                return new MontarBoletoCommandResult(true, "Validação realizada com sucesso", command.Assunto, retorno, email, command.Mensagem);
            }
            catch (Exception ex)
            {
                AddNotification("Erro", $"Descricao:{ex.InnerException}");
                return new MontarBoletoCommandResult(false, "Não foi possível realizar sua solicitação", Notifications);
            }
        }

        public ICommandResult Handle(MontarTabelaHtmlCommand command)
        {

            //var cliente = _repCliente.FindById(command.CodCliente);


            var thead_tr_td = "border:solid #666666 1.0pt;background:#CFCFCF;padding:3.75pt 3.0pt 3.75pt 3.0pt;text-align: center;";
            var thead_tr_td_span = "font-size:10.0pt;font-family:Verdana,sans-serif;color:white";
            StringBuilder sbTabelaParcelas = new StringBuilder();

            sbTabelaParcelas.AppendFormat($"<table border='0' cellspacing='0' cellpadding='0' style='width:100%; border-collapse:collapse'>");
            sbTabelaParcelas.AppendFormat($"<thead>");
            sbTabelaParcelas.AppendFormat($"<tr>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Contrato</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Fatura</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Nº Parcela</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Vecto.Parcela</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Vr.Parcela</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Vecto.Boleto</span></b></th>");
            sbTabelaParcelas.AppendFormat($"<th style='{thead_tr_td}'><b><span style='{thead_tr_td_span}'>Vr.Boleto</span></b></th>");
            sbTabelaParcelas.AppendFormat($"</tr>");
            sbTabelaParcelas.AppendFormat($"</thead>");

            var tbody_tr_td = "border-top:none;border-bottom:solid #666666 1.0pt;border-right:solid #666666 1.0pt;background:white;padding:3.75pt 3.0pt 3.75pt 3.0pt;text-align: center;";
            var tbody_tr_td_span = "font-size:9.0pt; font-family: Verdana ,sans-serif";
            sbTabelaParcelas.AppendFormat($"<tbody>");
            sbTabelaParcelas.AppendFormat($"<tr>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:solid #666666 1.0pt;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{BaseRotinas.Descriptografar(command.NumeroContrato)}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{command.NumeroDocumento}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{command.NumeroParcela}</span></td>");
            sbTabelaParcelas.AppendFormat(format: $"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{command.DataVencimento:dd/MM/yyyy}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{command.ValorParcelaAberto:C2}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{command.ParcelaDataVencimento:dd/MM/yyyy}</span></td>");
            sbTabelaParcelas.AppendFormat($"<td style='border-left:none;{tbody_tr_td}'><span style='{tbody_tr_td_span}'>{command.ValorDocumento:C2}</span></td>");
            sbTabelaParcelas.AppendFormat($"</tr>");
            sbTabelaParcelas.AppendFormat($"</tbody>");

            sbTabelaParcelas.AppendFormat($"</table>");

            // Agrupar as Validações
            AddNotifications(command);

            // Checar as notificações
            if (Invalid)
                return new CommandResult(false, "Não foi possível realizar sua solicitação", Notifications);

            return new CommandResult(true, "Validação realizada com sucesso", sbTabelaParcelas.ToString());
        }

        public ICommandResult Handle(ValidarBoletoAvulsoCommand command)
        {

            // Agrupar as Validações
            AddNotifications(command);

            if (command.Contratos == null)
                AddNotification("Contrato", "Selecione ao menos um contrato!");


            // Checar as notificações
            if (Invalid)
                return new CommandResult(false, "Não foi possível realizar sua solicitação", Notifications);

            return new CommandResult(true, "Validação realizada com sucesso");
        }

        public ICommandResult Handle(RegisterBoletoAvulsoCommand command)
        {
            #region Parametro
            var paramBanco = _repBancoPortador.GetByBancoPortadorId((int)command.CodBancoPortador);
            command.SetNumeroDocumento(_repContrato.FindById((int)command.CodContrato).NumeroContrato);
            var cliente = _repCliente.FindById(command.ClienteId);
            #endregion

            if (command.Justificativa == null)
                AddNotification("Justificativa", "Ao solicitar um boleto avulso será obrigatório uma justifica!");

            if (!(command.ValorDocumento > 0))
                AddNotification("ValorDocumento", "O Valor do Documento é inválido!");

            if (!(command.DataVencimento.Date > DateTime.Now.AddDays(1).Date))
                AddNotification("DataVencimento", "O Vencimento é inválido!");

            command.TipoSolcitacao = EBoletoSolicitacaoTipo.Avulso;

            var boletoPadrao = new ValueObjects.Boleto(command.CodUsuario, command.DataVencimento, BaseRotinas.Descriptografar(command.NumeroDocumento), command.ValorDocumento, EBoletoStatusType.SolicitacaoAnalista, command.TipoSolcitacao, command.MensagemImpresa);

            // Gerar as Entidades
            var entitie = new ParcelaBoleto(boletoPadrao, command.ClienteId, BaseRotinas.Descriptografar(cliente.CodCliente), BaseRotinas.ConverterParaShort(paramBanco.NumeroBanco), paramBanco.Cod, DateTime.Now, (decimal)paramBanco.JurosAoMesPorcentagem, (decimal)paramBanco.MultaAtraso);

            entitie.CongelarInformacoesBoletoAvulso((int)command.CodContrato);

            EBoletoEtapa Etapa = this.VerificarTipoEtapaSolicitacao(entitie);
            entitie.Boleto.ConfigurarEtapa(Etapa, command.CodUsuario);

            // Agrupar as Validações
            AddNotifications(entitie);

            // Checar as notificações
            if (Invalid)
                return new CommandResult(false, "Não foi possível realizar sua solicitação", Notifications);

            //Salvar
            _repository.CreateOrUpdate(entitie);

            if (entitie.Boleto.Status == (short)EBoletoStatusType.Aprovado)
            {
                ProcessarNossoNumero(entitie, paramBanco);// PROCESSAR O NOSSO NUMERO
           
                var listaDigitavel = BoletoUtils.MontarLinhaDigitavel(
                entitie.Boleto.ValorDocumento,
                entitie.Boleto.DataVencimento,
                entitie.Boleto.NumeroDocumento,
                paramBanco.NumeroBanco,
                (int)entitie.Boleto.NossoNumero,
                paramBanco.CNPJ,
                paramBanco.Portador,
                paramBanco.Agencia,
                paramBanco.AgenciaDigito,
                paramBanco.Conta,
                paramBanco.ContaDigito,
                paramBanco.Carteira.ToString(),
                paramBanco.Endereco,
                paramBanco.EnderecoNumero,
                paramBanco.Bairro,
                paramBanco.Cidade,
                paramBanco.UF,
                paramBanco.CEP);

                entitie.Boleto.SetLinhaDigitavel(listaDigitavel);

                // Reprovar todas solicitações em aberto onde o id do contrato for o mesmo
                ReprovarBoletosAbertoNaoAprovado(entitie);

            }

            //preencher historico
            var historico = new BoletoHistorico(entitie.Cod, command.CodUsuario, (int)_repParametroSintese.ObterSinteseParaEtapa(Etapa), command.Justificativa);
            historico = _repBoletoHistorico.CreateOrUpdate(historico);

            if (_repParametroSintese.ExisteSinteseDePara(historico.CodSintese))
            {
                var sintesePara = _repParametroSintese.ObterSintesePara(historico.CodSintese);
                _repBoletoHistorico.CreateOrUpdate(new BoletoHistorico(entitie.Cod, command.CodUsuario, (int)sintesePara, command.Justificativa));
            }


            _repBoletoHistorico.UpdateBoletoSetLastHistory(entitie.Cod);

            return new CommandResult(true, "Boleto avulso criado!");
        }

        public ICommandResult Handle(CriarRemessaCommand command)
        {
            try
            {
                List<int> ListaBoletosOK = new List<int>();
                List<TabelaBanco> bancos = _repBanco.GetAllBancos();
                List<byte[]> listArquivos = new List<byte[]>();
                List<int> listLogId = new List<int>();

                foreach (var banco in bancos)
                {

                    StringBuilder Conteudo = new StringBuilder();
                    var listBoletos = _repository.GetBoletosFromListRemessa(banco.Cod, command.PeriodoDe, command.PeriodoAte, command.BoletoIds);

                    if (listBoletos.Count() == 0) continue; // SE NAO ENCONTRAR NENHUM BOLETO PARA ESTE BANCO
                    var nomeArquivo = "";
                    var group = listBoletos.GroupBy(x => x.CodBancoPortador).ToList();
                    foreach (var gBoletos in group)
                    {
                        var parametroCedente = _repBancoPortador.GetByBancoPortadorId((int)gBoletos.FirstOrDefault().CodBancoPortador);

                        #region INSTRUCOES DO BANCO (BANCO PORTADOR INSTRUCAO)
                        List<string> listaInstrucoes = new List<string>();
                        var instrucoes = _repBancoPortador.GetInstrucoesByBancoPortadorId((int)parametroCedente.Cod).ToList();
                        if (instrucoes.Count() > 0) listaInstrucoes = instrucoes.Select(x => x.Descricao).ToList();
                        #endregion

                        BoletoNet.Banco _banco = new BoletoNet.Banco(BaseRotinas.ConverterParaInt(parametroCedente.NumeroBanco));
                        #region CEDENTE (BANCO PORTADOR)
                        BoletoNet.Cedente _cedente = BoletoUtils.MontarCedente(
                                    parametroCedente.CNPJ,
                                    parametroCedente.Portador,
                                    9087893,//FIXO TROCAR
                                    parametroCedente.Agencia,
                                    parametroCedente.Conta,
                                    parametroCedente.ContaDigito,
                                    parametroCedente.Carteira.ToString(),
                                    parametroCedente.Endereco,
                                    parametroCedente.EnderecoNumero,
                                    parametroCedente.Bairro,
                                    parametroCedente.Cidade,
                                    parametroCedente.UF,
                                    parametroCedente.CEP);

                        int LoteAtual = _repBancoPortador.GerarNumeroLote(parametroCedente.Cod);
                        int qtdTitulosTotal = BaseRotinas.ConverterParaInt(gBoletos.Count());
                        string strArchive = "";

                        #endregion

                        List<BoletoNet.Boleto> boletos = new List<BoletoNet.Boleto>();
                        #region CONVERTER LISTA EM BoletoNet.Boleto
                        boletos = gBoletos.Select(x => BoletoUtils.ConvertListBoletoToListBoletoNet(
                            BaseRotinas.ConverterParaInt(x.CodBoleto),
                            x.CodigoClienteDecrypt_,
                            x.NumeroDocumento,
                            BaseRotinas.ConverterParaInt(x.NumeroParcela),
                            x.NossoNumero.ToString(),
                            BaseRotinas.ConverterParaDatetime(x.DataVencimento),
                            BaseRotinas.ConverterParaDatetime(x.DataAprovacao),
                            BaseRotinas.ConverterParaDecimal(x.ValorDocumento),
                            x.ParcelaTipoFatura,
                            x.CompanhiaFiscal,
                            x.Alterar,
                            x.TipoSolicitacao,
                            BaseRotinas.ConverterParaDecimal(parametroCedente.MultaAtraso),
                            BaseRotinas.ConverterParaDecimal(parametroCedente.JurosAoMesPorcentagem),
                            x.Companhia,
                            _banco,
                            _cedente,
                            x.ClienteDecrypt_,
                            x.ClienteCPFDecrypt_,
                            x.ClienteEndereco,
                            x.ClienteEnderecoNumero,
                            x.ClienteEnderecoCEP,
                            x.ClienteEnderecoBairro,
                            x.ClienteEnderecoCidade,
                            x.ClienteEnderecoUF,
                            x.ClienteEnderecoComplemento,
                            listaInstrucoes
                            )).ToList();
                        //PREENCHER OS DETALHES
                        #endregion

                        if (_banco.Codigo == 341)
                        {
          
                            strArchive = BoletoUtils.MontarRemessaItau(_banco, _cedente, LoteAtual, qtdTitulosTotal, boletos, ref ListaBoletosOK);
                        }
                        else if (_banco.Codigo == 33)
                        {
                            strArchive = BoletoUtils.MontarRemessaSantander(_banco, _cedente, LoteAtual, qtdTitulosTotal, boletos, ref ListaBoletosOK);
                        }

                        nomeArquivo = BoletoUtils.GerarNomeArquivoRemessa(_banco.Codigo, parametroCedente.Conta, parametroCedente.ContaDigito,  LoteAtual);
                        Conteudo.Append($"{strArchive}");
                    }

                    var arquivo = Encoding.UTF8.GetBytes(Conteudo.ToString());
                    listArquivos.Add(arquivo);

                    
                    var entitieLog = new BancoPortadorLogArquivo(nomeArquivo, arquivo, ListaBoletosOK.Count(), command.CodUsuario);
                    _repBancoPortadorLogArquivo.CreateOrUpdate(entitieLog);
                    listLogId.Add(entitieLog.Cod);

                    _repository.AtualizarBoletosSetRemessaEmitida(ListaBoletosOK, entitieLog.Cod, command.CodUsuario);

                }
                // Checar as notificações
                if (Invalid)
                    return new CriarRemessaCommandResult(false, "Não foi possível realizar sua solicitação", Notifications);

                return new CriarRemessaCommandResult(true, "Operação realizado com sucesso!", ListaBoletosOK, listLogId, listArquivos);
            }
            catch (Exception ex)
            {
                AddNotification("Erro ao gerar remessa", $"Descrição:{ex.Message}");
                return new CriarRemessaCommandResult(false, "Não foi possível realizar sua solicitação", Notifications);
            }
        }

        public ICommandResult Handle(RegistrarBoletoHistoricoCommand command)
        {
            if (command.Observacao == null)
                command.Observacao = _repBoletoHistorico.GetLastObservacaoByCodBoleto((int)command.CodBoleto);

            //preencher entidade
            var entityHistorico = new BoletoHistorico((int)command.CodBoleto, (int)command.CodUsuario, (int)command.CodSintese, command.Observacao);
            _repBoletoHistorico.CreateOrUpdate(entityHistorico);
            var ultimaSintese = entityHistorico.CodSintese;

            if (_repParametroSintese.ExisteSinteseDePara(entityHistorico.CodSintese))
            {
                var sintesePara = _repParametroSintese.ObterSintesePara(entityHistorico.CodSintese);
                _repBoletoHistorico.CreateOrUpdate(new BoletoHistorico((int)command.CodBoleto, (int)command.CodUsuario, (int)sintesePara, command.Observacao));
                ultimaSintese = sintesePara;
            }

            var entitie = _repository.FindById((int)command.CodBoleto);
            var paramBanco = _repBancoPortador.GetByBancoPortadorId((int)entitie.CodBancoPortador);
            var etapa = _repParametroSintese.ObterEtapaDaSintese(entityHistorico.CodSintese);

            if (etapa == EBoletoEtapa.SupervisorAprovado)
            {

                ProcessarNossoNumero(entitie, paramBanco);// PROCESSAR O NOSSO NUMERO

                var listaDigitavel = BoletoUtils.MontarLinhaDigitavel(
                entitie.Boleto.ValorDocumento,
                entitie.Boleto.DataVencimento,
                entitie.Boleto.NumeroDocumento,
                paramBanco.NumeroBanco,
                (int)entitie.Boleto.NossoNumero,
                paramBanco.CNPJ,
                paramBanco.Portador,
                paramBanco.Agencia,
                paramBanco.AgenciaDigito,
                paramBanco.Conta,
                paramBanco.ContaDigito,
                paramBanco.Carteira.ToString(),
                paramBanco.Endereco,
                paramBanco.EnderecoNumero,
                paramBanco.Bairro,
                paramBanco.Cidade,
                paramBanco.UF,
                paramBanco.CEP);

                entitie.Boleto.SetLinhaDigitavel(listaDigitavel);

                // Reprovar todas solicitações em aberto onde o id do contrato for o mesmo
                ReprovarBoletosAbertoNaoAprovado(entitie);
            }

            if (etapa > 0)
                entitie.Boleto.ConfigurarEtapa((EBoletoEtapa)etapa, (int)command.CodUsuario);

            _repository.Update(entitie);

            _repBoletoHistorico.UpdateBoletoSetLastHistory((int)command.CodBoleto);

            // Agrupar as Validações
            AddNotifications(entitie);

            // Checar as notificações
            if (Invalid)
                return new CommandResult(false, "Não foi possível realizar sua solicitação", Notifications);


            return new CommandResult(true, "Boleto avulso registrado!");
        }

        private void ReprovarBoletosAbertoNaoAprovado(ParcelaBoleto entitie)
        {
            var idSintese = (int)_repParametroSintese.ObterSinteseParaEtapa(EBoletoEtapa.ReprovarObsoletos);
            var obs = $"Foi solicitado um novo boleto para este documento.";
            if (entitie.Boleto.TipoSolicitacao == (short)EBoletoSolicitacaoTipo.Avulso)
            {
                if (_repository.ExisteBoletoPendenteByContratoId((int)entitie.CodContrato))
                    _repository.ReprovarBoletoAntigosDesteContrato(entitie.Cod, (int)entitie.CodContrato, idSintese, obs);
            }
            else
            {
                if (_repository.ExisteBoletoPendentePorParcelaId((int)entitie.Parcela.Cod))
                    _repository.ReprovarBoletoAntigosDestaParcela(entitie.Cod, (int)entitie.Parcela.Cod, idSintese, obs);
            }

        }

        public void ProcessarNossoNumero(ParcelaBoleto entitie, BancoPortadorQueryResult paramBanco)
        {

            if (entitie.Boleto.TipoSolicitacao == (short)EBoletoSolicitacaoTipo.Avulso)
            {
                entitie.Boleto.SetNossoNumero(_repBancoPortador.GerarNossoNumero(paramBanco.Cod));
            }
            if (entitie.Parcela.ValorParcela == entitie.Parcela.ValorAberto && entitie.Parcela.NossoNumero > 0 && entitie.Parcela.DataEmissaoFatura > paramBanco.DataInicialRegistroBoleto && !(_repository.ParcelaJaPossuiuClassificacaoC6((int)entitie.Parcela.Cod)))
                entitie.DefinirBoletoAlterarcao();
            else
                entitie.Boleto.SetNossoNumero(_repBancoPortador.GerarNossoNumero(paramBanco.Cod));

            //_repository.CreateOrUpdateNossoNumero(boleto);//ATUALIZA O NOSSO NUMERO NO BOLETO
        }


        public EBoletoEtapa VerificarTipoEtapaSolicitacao(ParcelaBoleto entitie)
        {
            EBoletoEtapa tipoSolicitacao = EBoletoEtapa.AtendenteSolicitaComDesconto;
            if (entitie.Boleto.TipoSolicitacao == (short)EBoletoSolicitacaoTipo.Avulso)
            {
                tipoSolicitacao = EBoletoEtapa.AtendenteSolicitaBoletoAvulso;
            }
            //SE O BOLETO FOR APENAS PRORROGAÇÂO DO VENCIMENTO O MESMO SERA APROVADO DIRETO
            else if (entitie.JurosAoMesPorcentagemPrevisto == entitie.JurosAoMesPorcentagem && entitie.MultaAtrasoPorcentagemPrevisto == entitie.MultaAtrasoPorcentagem && entitie.Boleto.ValorDesconto <= 0)
            {
                tipoSolicitacao = EBoletoEtapa.AtendenteSolicitaProrrogacao;
            }
            else
            {
                tipoSolicitacao = EBoletoEtapa.AtendenteSolicitaComDesconto;
            }
            return tipoSolicitacao;
        }
    }
}
