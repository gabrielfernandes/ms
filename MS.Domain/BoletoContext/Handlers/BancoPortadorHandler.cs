﻿using Cobranca.Domain.BoletoContext.Repositories;
using Cobranca.Shared.Commands;
using JunixValidator;

namespace Cobranca.Domain.BoletoContext.Handlers
{
    public class BancoPortadorHandler :
         Notifiable
    {
        private readonly IBancoPortadorRepository _repository;

        public BancoPortadorHandler(IBancoPortadorRepository repository)
        {
            _repository = repository;
        }

    }
}
