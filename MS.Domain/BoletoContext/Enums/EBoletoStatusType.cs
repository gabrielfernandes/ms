﻿
namespace Cobranca.Domain.BoletoContext.Enums
{
    public enum EBoletoStatusType
    {
        SolicitacaoAtendente = 1,
        SolicitacaoAnalista = 2,
        SolicitacaoSupervisor = 3,
        Aprovado = 4,
        Reprovado = 5,
        Cancelado = 6,
        Obsoleto = 7
    }

    public class EBoletoStatusTypeDescricao
    {
        public static string EBoletoStatusTypeDescricaoDesc(EBoletoStatusType? tipo)
        {
            EBoletoStatusType tp = (EBoletoStatusType)tipo;
            switch (tp)
            {
                case EBoletoStatusType.Aprovado:
                    return "Aprovado";
                case EBoletoStatusType.Cancelado:
                    return "Cancelado";
                case EBoletoStatusType.Reprovado:
                    return "Reprovado";
                default:
                    return "--";
            }
        }
    }

}
