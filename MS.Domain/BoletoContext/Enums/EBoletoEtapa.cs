﻿namespace Cobranca.Domain.BoletoContext.Enums
{
    public enum EBoletoEtapa
    {
        AtendenteSolicitaComDesconto = 1,
        AtendenteSolicitaProrrogacao = 2,
        SupervisorAprovado = 3,
        AnalistaReprova = 4,
        SupervisorReprova = 5,
        PrazoExpira = 6,
        AtendenteSolicitaBoletoAvulso = 7,
        ReprovarObsoletos = 8
    }
}
