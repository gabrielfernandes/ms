﻿
namespace Cobranca.Domain.BoletoContext.Enums
{
    public enum EBoletoSolicitacaoTipo
    {
        Alterar = 1,
        Novo = 2,
        Avulso = 3
    }
    public class EBoletoSolicitacaoTipoDescricao
    {
        public static string EBoletoSolicitacaoTipoDesc(EBoletoSolicitacaoTipo? tipo)
        {
            EBoletoSolicitacaoTipo tp = (EBoletoSolicitacaoTipo)tipo;
            switch (tp)
            {
                case EBoletoSolicitacaoTipo.Alterar:
                    return "Alterar";
                case EBoletoSolicitacaoTipo.Avulso:
                    return "Avulso";
                case EBoletoSolicitacaoTipo.Novo:
                    return "Novo";
                default:
                    return "--";
            }
        }
    }
}
