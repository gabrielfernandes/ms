﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Boleto.Net.BoletoJunix
{
    public static class EnumBoleto
    {
        public enum BancoCodigo
        {
            BancodoBrasil = 1,
            Bancoob = 756,
            Banestes = 21,
            Bradesco = 237,
            BankBoston = 744,
            Banrisul = 41,
            BRB = 70,
            Caixa = 104,
            CECRED = 85,
            Daycoval = 707,
            Itau = 341,
            HSBC = 399,
            //Unibanco = 409,
            Nordeste = 4,
            Real = 356,
            Safra = 422,
            Santander = 33,
            Sicredi = 748,
            Sofisa = 637,
            Sudameris = 215
        }
        #region ITAU
        public enum Letra
        {
            A = 1,
            B = 2,
            C = 3,
            D = 4,
            E = 5,
            F = 6,
            G = 7,
            H = 8,
            I = 9
        }
        public enum Instrucoes_Itau
        {
            Informacao_Responsabilidade_Beneficiario = 0,
            Devolver_Apos_05_Dias_Do_Vencimento = 2,
            Devolver_Apos_30_Dias_Do_Vencimento = 3,
            Receber_Conforme_Instrucoes_No_Proprio_Titulo = 5,
            Devolver_Apos_10_Dias_Do_Vencimento = 6,
            Devolver_Apos_15_Dias_Do_Vencimento = 7,
            Devolver_Apos_20_Dias_Do_Vencimento = 8,
            Protestar = 9,
            Nao_Protestar = 10,
            Devolver_Apos_25_Dias_Do_Vencimento = 11,
            Devolver_Apos_35_Dias_Do_Vencimento = 12,
            Devolver_Apos_40_Dias_Do_Vencimento = 13,
            Devolver_Apos_45_Dias_Do_Vencimento = 14,
            Devolver_Apos_50_Dias_Do_Vencimento = 15,
            Devolver_Apos_55_Dias_Do_Vencimento = 16,
            Devolver_Apos_60_Dias_Do_Vencimento = 17,
            Devolver_Apos_90_Dias_Do_Vencimento = 18,
            Nao_Receber_Apos_05_Dias_Do_Vencimento = 19,
            Nao_Receber_Apos_10_Dias_Do_Vencimento = 20,
            Nao_Receber_Apos_15_Dias_Do_Vencimento = 21,
            Nao_Receber_Apos_20_Dias_Do_Vencimento = 22,
            Nao_Receber_Apos_25_Dias_Do_Vencimento = 23,
            Nao_Receber_Apos_30_Dias_Do_Vencimento = 24,
            Nao_Receber_Apos_35_Dias_Do_Vencimento = 25,
            Nao_Receber_Apos_40_Dias_Do_Vencimento = 26,
            Nao_Receber_Apos_45_Dias_Do_Vencimento = 27,
            Nao_Receber_Apos_50_Dias_Do_Vencimento = 28,
            Nao_Receber_Apos_55_Dias_Do_Vencimento = 29,
            Importancia_De_Desconto_Por_Dia = 30,
            Nao_Receber_Apos_60_Dias_Do_Vencimento = 31,
            Nao_Receber_Apos_90_Dias_Do_Vencimento = 32,
            Conceder_Abatimento_Mesmo_Apos_Vencimento = 33,
            Protestar_Apos_Xx_Dias_Corridos_Do_Vencimento = 34,
            Protestar_Apos_Xx_Dias_Uteis_Do_Vencimento = 35,
            Receber_Ate_O_Ultimo_Dia_Do_Mes_De_Vencimento = 37,
            Conceder_Desconto_Mesmo_Apos_Vencimento = 38,
            Nao_Receber_Apos_O_Vencimento = 39,
            Conceder_Desconto_Conforme_Nota_De_Credito = 40,
            Protesto_Para_Fins_Falimentares = 42,
            Sujeito_A_Protesto_Se_Nao_For_Pago_No_Vencimento = 43,
            Importancia_Por_Dia_De_Atraso_A_Partir_De_Ddmmaa = 44,
            Tem_Dia_Da_Graca = 45,
            //Uso_Do_Banco = 47,
            Dispensar_Juros_Comissao_De_Permanencia = 47,
            Receber_Somente_Com_A_Parcela_Anterior_Quitada = 51,
            Efetuar_O_Pagamento_Somente_Atraves_Deste_Boleto_E_Na_Rede_Bancaria = 52,
            //Uso_Do_Banco = 53,
            Apos_Vencimento_Pagavel_Somente_Na_Empresa = 54,
            //Uso_Do_Banco = 56,
            Somar_Valor_Do_Titulo_Ao_Valor_Do_Campo_Mora_Multa_Caso_Exista = 58,
            Devolver_Apos_365_Dias_De_Vencido = 59,
            Cobranca_Negociada_Pagavel_Somente_Por_Este_Boleto_Na_Rede_Bancaria = 61,
            Titulo_Entregue_Em_Penhor_Em_Favor_Do_Beneficiario_Acima = 62,
            Titulo_Transferido_A_Favor_Do_Beneficiario = 66,
            Entrada_Em_Negativacao_Expressa = 67,
            Nao_Negativar = 70,
            //Uso_Do_Banco = 78,
            Valor_Da_Ida_Engloba_Multa_De_10_Porcento_Pro_Rata = 79,
            Cobrar_Juros_Apos_15_Dias_Da_Emissao = 80,
            Operacao_Ref_A_Vendor = 83,
            Apos_Vencimento_Consultar_A_Agencia_Beneficiario = 84,
            Antes_Do_Vencimento_Ou_Apos_15_Dias, _Pagavel_Somente_Em_Nossa_Sede = 86,
            //Uso_Do_Banco = 87,
            Nao_Receber_Antes_Do_Vencimento = 88,
            //Uso_Do_Banco = 89,
            No_Vencimento_Pagavel_Em_Qualquer_Agencia_Bancaria = 90,
            Nao_Receber_Apos_Xx_Dias_Do_Vencimento = 91,
            Devolver_Apos_Xx_Dias_Do_Vencimento = 92
            //Mensagens_Nos_Boletos_Com_30_Posicoes = 93,
            //Mensagens_Nos_Boletos_Com_40_Posicoes = 94
            //Uso_Do_Banco = 95,
            //Duplicata_/_Fatura_Nº = 98,

        }
        public enum Instrucoes_Santander
        {

            Baixar_Apos_15_Dias = 02,
            Baixar_Apos_30_Dias = 03,
            Nao_Baixar = 04,
            Protestar = 06,
            Nao_Protestar = 07,
            Nao_Cobrar_Juros_DeMora = 08,
            Juros_Ao_Dia = 98,
            Multa_Vencimento = 99
        }
        public enum Carteira_Itau
        {
            Direta_Eletronica_Emissao_Intregal_Carne = 108,
            Direta_Eletronica_Emissao_Intregal_Simples = 180,
            Direta_Eletronica_Emissao_Parcial_Simples = 121,
            Direta_Eletronica_Sem_Emissao_Dolar = 150,
            Direta_Eletronica_Sem_Emissao_Simples = 109,
            Duplicatas_Transferencia_Desconto = 191,
            Escritural_Eletronica_Carne = 104,
            Escritural_Eletronica_Cobranca_Inteligente = 188,
            Escritural_Eletronica_Dolar = 147,
            Escritural_Eletronica_Simples = 112,
            Escritural_Eletronica_Simples_Faixa_Nosso_Numero_Livre = 115
        }

        public enum Carteira_Santander
        {
            Cobranca_Simples_Com_Registro = 101,
            Cobranca_Simples_Sem_Registro = 102
        }
        #endregion

    }
    public static class EnumBoletoDescricao
    {
        public static string BancoCodigo(EnumBoleto.BancoCodigo tp)
        {
            switch (tp)
            {
                case EnumBoleto.BancoCodigo.BancodoBrasil:
                    return "Banco do Brasil S.A.";
                case EnumBoleto.BancoCodigo.Bancoob:
                    return "Banco Cooperativo do Brasil S.A. - BANCOOB";
                case EnumBoleto.BancoCodigo.Banestes:
                    return "BANESTES S.A. Banco do Estado do Espírito Santo";
                case EnumBoleto.BancoCodigo.BankBoston:
                    return "BankBoston N.A.";
                case EnumBoleto.BancoCodigo.Banrisul:
                    return "Banco do Estado do Rio Grande do Sul S.A.";
                case EnumBoleto.BancoCodigo.Bradesco:
                    return "Banco Bradesco S.A.";
                case EnumBoleto.BancoCodigo.BRB:
                    return "BRB - Banco de Brasília S.A.";
                case EnumBoleto.BancoCodigo.Caixa:
                    return "Caixa Econômica Federal";
                case EnumBoleto.BancoCodigo.CECRED:
                    return "Cooperativa Central de Crédito Urbano-CECRED";
                case EnumBoleto.BancoCodigo.Daycoval:
                    return "Banco Daycoval S.A.";
                case EnumBoleto.BancoCodigo.HSBC:
                    return "HSBC Bank Brasil S.A. - Banco Múltiplo";
                case EnumBoleto.BancoCodigo.Itau:
                    return "Itaú Unibanco S.A.";
                case EnumBoleto.BancoCodigo.Nordeste:
                    return "Banco do Nordeste do Brasil S.A";
                case EnumBoleto.BancoCodigo.Real:
                    return "Banco Real S.A.";
                case EnumBoleto.BancoCodigo.Safra:
                    return "Banco Safra S.A.";
                case EnumBoleto.BancoCodigo.Santander:
                    return "Banco Santander (Brasil) S.A.";
                case EnumBoleto.BancoCodigo.Sicredi:
                    return "Banco Cooperativo Sicredi S.A.";
                case EnumBoleto.BancoCodigo.Sofisa:
                    return "Banco Sofisa S.A.";
                case EnumBoleto.BancoCodigo.Sudameris:
                    return "Banco Comercial e de Investimento Sudameris S.A.";
                default:
                    return "--";
            }
        }
        public static string Carteira_Itau(EnumBoleto.Carteira_Itau tp)
        {
            switch (tp)
            {
                case EnumBoleto.Carteira_Itau.Direta_Eletronica_Emissao_Intregal_Carne:
                    return "108 - Direta Eletrônica Emissão Integral Carne";
                case EnumBoleto.Carteira_Itau.Direta_Eletronica_Emissao_Intregal_Simples:
                    return "180 - Direta Eletrônica Emissão Integral Simples";
                case EnumBoleto.Carteira_Itau.Direta_Eletronica_Emissao_Parcial_Simples:
                    return "121 - Direta Eletrônica Emissão Parcial Simples";
                case EnumBoleto.Carteira_Itau.Direta_Eletronica_Sem_Emissao_Dolar:
                    return "150 - Direta Eletrônica Emissão Sem Emissão Dólar";
                case EnumBoleto.Carteira_Itau.Direta_Eletronica_Sem_Emissao_Simples:
                    return "109 - Direta Eletrônica Sem Emissão Simples";
                case EnumBoleto.Carteira_Itau.Duplicatas_Transferencia_Desconto:
                    return "191 - Duplicatas - Transferência de Desconto";
                case EnumBoleto.Carteira_Itau.Escritural_Eletronica_Carne:
                    return "104 - Escritural - Eletrônica - Carnê";
                case EnumBoleto.Carteira_Itau.Escritural_Eletronica_Cobranca_Inteligente:
                    return "188 - Escritural - Eletrônica - Cobrança Inteligente (B2B)";
                case EnumBoleto.Carteira_Itau.Escritural_Eletronica_Dolar:
                    return "147 - Escritural - Eletrônica - Dólar";
                case EnumBoleto.Carteira_Itau.Escritural_Eletronica_Simples:
                    return "122 - Escritural - Eletrônica - Simples";
                case EnumBoleto.Carteira_Itau.Escritural_Eletronica_Simples_Faixa_Nosso_Numero_Livre:
                    return "115 - Escritural - Eletrônica - Faixa Nosso Número Livre";
                default:
                    return "--";
            }
        }

        public static string Carteira_Santander(EnumBoleto.Carteira_Santander tp)
        {
            switch (tp)
            {
                case EnumBoleto.Carteira_Santander.Cobranca_Simples_Com_Registro:
                    return "101 - Simples Rápida Com Registro";
                case EnumBoleto.Carteira_Santander.Cobranca_Simples_Sem_Registro:
                    return "102 - Simples Rápida Sem Registro";
                default:
                    return "--";
            }
        }


        #region ITAU JANEIRO 2017
        public static string Instrucoes_Itau(EnumBoleto.Instrucoes_Itau tp)
        {
            switch (tp)
            {
                case EnumBoleto.Instrucoes_Itau.Informacao_Responsabilidade_Beneficiario:
                    return "00 - INFORMAÇÃO DE RESPONSABILIDADE  DO BENIFICIÁRIO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_05_Dias_Do_Vencimento:
                    return "02 - DEVOLVER APÓS 05 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_30_Dias_Do_Vencimento:
                    return "03 - DEVOLVER APÓS 30 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Receber_Conforme_Instrucoes_No_Proprio_Titulo:
                    return "05 - RECEBER CONFORME INSTRUÇÕES NO PRÓPRIO TÍTULO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_10_Dias_Do_Vencimento:
                    return "06 - DEVOLVER APÓS 10 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_15_Dias_Do_Vencimento:
                    return "07 - DEVOLVER APÓS 15 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_20_Dias_Do_Vencimento:
                    return "08 - DEVOLVER APÓS 20 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Protestar:
                    return "09 - PROTESTAR";
                case EnumBoleto.Instrucoes_Itau.Nao_Protestar:
                    return "10 - NÃO PROTESTAR (inibe protesto, quando houver instrução permanente na conta corrente)";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_25_Dias_Do_Vencimento:
                    return "11 - DEVOLVER APÓS 25 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_35_Dias_Do_Vencimento:
                    return "12 - DEVOLVER APÓS 35 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_40_Dias_Do_Vencimento:
                    return "13 - DEVOLVER APÓS 40 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_45_Dias_Do_Vencimento:
                    return "14 - DEVOLVER APÓS 45 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_50_Dias_Do_Vencimento:
                    return "15 - DEVOLVER APÓS 50 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_55_Dias_Do_Vencimento:
                    return "16 - DEVOLVER APÓS 55 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_60_Dias_Do_Vencimento:
                    return "17 - DEVOLVER APÓS 60 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_90_Dias_Do_Vencimento:
                    return "18 - DEVOLVER APÓS 90 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_05_Dias_Do_Vencimento:
                    return "19 - NÃO RECEBER APÓS 05 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_10_Dias_Do_Vencimento:
                    return "20 - NÃO RECEBER APÓS 10 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_15_Dias_Do_Vencimento:
                    return "21 - NÃO RECEBER APÓS 15 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_20_Dias_Do_Vencimento:
                    return "22 - NÃO RECEBER APÓS 20 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_25_Dias_Do_Vencimento:
                    return "23 - NÃO RECEBER APÓS 25 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_30_Dias_Do_Vencimento:
                    return "24 - NÃO RECEBER APÓS 30 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_35_Dias_Do_Vencimento:
                    return "25 - NÃO RECEBER APÓS 35 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_40_Dias_Do_Vencimento:
                    return "26 - NÃO RECEBER APÓS 40 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_45_Dias_Do_Vencimento:
                    return "27 - NÃO RECEBER APÓS 45 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_50_Dias_Do_Vencimento:
                    return "28 - NÃO RECEBER APÓS 50 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_55_Dias_Do_Vencimento:
                    return "29 - NÃO RECEBER APÓS 55 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Importancia_De_Desconto_Por_Dia:
                    return "30 - IMPORTÂNCIA DE DESCONTO POR DIA";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_60_Dias_Do_Vencimento:
                    return "31 - NÃO RECEBER APÓS 60 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_90_Dias_Do_Vencimento:
                    return "32 - NÃO RECEBER APÓS 90 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Conceder_Abatimento_Mesmo_Apos_Vencimento:
                    return "33 - CONCEDER ABATIMENTO REF. À PIS-PASEP/COFIN/CSSL, MESMO APÓS VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Protestar_Apos_Xx_Dias_Corridos_Do_Vencimento:
                    return "34 - PROTESTAR APÓS XX DIAS CORRIDOS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Protestar_Apos_Xx_Dias_Uteis_Do_Vencimento:
                    return "35 - PROTESTAR APÓS XX DIAS ÚTEIS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Receber_Ate_O_Ultimo_Dia_Do_Mes_De_Vencimento:
                    return "37 - RECEBER ATÉ O ÚLTIMO DIA DO MÊS DE VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Conceder_Desconto_Mesmo_Apos_Vencimento:
                    return "38 - CONCEDER DESCONTO MESMO APÓS VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_O_Vencimento:
                    return "39 - NÃO RECEBER APÓS O VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Conceder_Desconto_Conforme_Nota_De_Credito:
                    return "40 - CONCEDER DESCONTO CONFORME NOTA DE CRÉDITO";
                case EnumBoleto.Instrucoes_Itau.Protesto_Para_Fins_Falimentares:
                    return "42 - PROTESTO PARA FINS FALIMENTARES";
                case EnumBoleto.Instrucoes_Itau.Sujeito_A_Protesto_Se_Nao_For_Pago_No_Vencimento:
                    return "43 - SUJEITO A PROTESTO SE NÃO FOR PAGO NO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Importancia_Por_Dia_De_Atraso_A_Partir_De_Ddmmaa:
                    return "44 - IMPORTÂNCIA POR DIA DE ATRASO A PARTIR DE DDMMAA";
                case EnumBoleto.Instrucoes_Itau.Tem_Dia_Da_Graca:
                    return "45 - TEM DIA DA GRAÇA";
                case EnumBoleto.Instrucoes_Itau.Dispensar_Juros_Comissao_De_Permanencia:
                    return "47 - DISPENSAR JUROS/COMISSÃO DE PERMANÊNCIA";
                case EnumBoleto.Instrucoes_Itau.Receber_Somente_Com_A_Parcela_Anterior_Quitada:
                    return "51 - RECEBER SOMENTE COM A PARCELA ANTERIOR QUITADA";
                case EnumBoleto.Instrucoes_Itau.Efetuar_O_Pagamento_Somente_Atraves_Deste_Boleto_E_Na_Rede_Bancaria:
                    return "52 - EFETUAR O PAGAMENTO SOMENTE ATRAVÉS DESTE BOLETO E NA REDE BANCÁRIA";
                case EnumBoleto.Instrucoes_Itau.Apos_Vencimento_Pagavel_Somente_Na_Empresa:
                    return "54 - APÓS VENCIMENTO PAGÁVEL SOMENTE NA EMPRESA";
                case EnumBoleto.Instrucoes_Itau.Somar_Valor_Do_Titulo_Ao_Valor_Do_Campo_Mora_Multa_Caso_Exista:
                    return "58 - SOMAR VALOR DO TÍTULO AO VALOR DO CAMPO MORA/MULTA CASO EXISTA";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_365_Dias_De_Vencido:
                    return "59 - DEVOLVER APÓS 365 DIAS DE VENCIDO";
                case EnumBoleto.Instrucoes_Itau.Cobranca_Negociada_Pagavel_Somente_Por_Este_Boleto_Na_Rede_Bancaria:
                    return "61 - COBRANÇA NEGOCIADA. PAGÁVEL SOMENTE POR ESTE BOLETO NA REDE BANCÁRIA";
                case EnumBoleto.Instrucoes_Itau.Titulo_Entregue_Em_Penhor_Em_Favor_Do_Beneficiario_Acima:
                    return "62 - TÍTULO ENTREGUE EM PENHOR EM FAVOR DO BENEFICIÁRIO ACIMA";
                case EnumBoleto.Instrucoes_Itau.Titulo_Transferido_A_Favor_Do_Beneficiario:
                    return "66 - TÍTULO TRANSFERIDO A FAVOR DO BENEFICIÁRIO";
                case EnumBoleto.Instrucoes_Itau.Entrada_Em_Negativacao_Expressa:
                    return "67 - ENTRADA EM NEGATIVAÇÃO EXPRESSA (IMPRIME: SUJEITO A NEGATIVAÇÃO APÓS O VENCIMENTO)";
                case EnumBoleto.Instrucoes_Itau.Nao_Negativar:
                    return "70 - NÃO NEGATIVAR (INIBE A ENTRADA EM NEGATIVAÇÃO EXPRESSA)";
                case EnumBoleto.Instrucoes_Itau.Valor_Da_Ida_Engloba_Multa_De_10_Porcento_Pro_Rata:
                    return "79 - VALOR DA IDA ENGLOBA MULTA DE 10% PRO RATA";
                case EnumBoleto.Instrucoes_Itau.Cobrar_Juros_Apos_15_Dias_Da_Emissao:
                    return "80 - COBRAR JUROS APÓS 15 DIAS DA EMISSÃO (para títulos com vencimento à vista)";
                case EnumBoleto.Instrucoes_Itau.Operacao_Ref_A_Vendor:
                    return "83 - OPERAÇÃO REF A VENDOR";
                case EnumBoleto.Instrucoes_Itau.Apos_Vencimento_Consultar_A_Agencia_Beneficiario:
                    return "84 - APÓS VENCIMENTO CONSULTAR A AGÊNCIA BENEFICIÁRIO";
                case EnumBoleto.Instrucoes_Itau.Antes_Do_Vencimento_Ou_Apos_15_Dias:
                    return "85 - ANTES DO VENCIMENTO OU APÓS 15 DIAS, PAGÁVEL SOMENTE EM NOSSA SEDE";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Antes_Do_Vencimento:
                    return "88 - NÃO RECEBER ANTES DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.No_Vencimento_Pagavel_Em_Qualquer_Agencia_Bancaria:
                    return "90 - NO VENCIMENTO PAGÁVEL EM QUALQUER AGÊNCIA BANCÁRIA";
                case EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_Xx_Dias_Do_Vencimento:
                    return "91 - NÃO RECEBER APÓS XX DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Itau.Devolver_Apos_Xx_Dias_Do_Vencimento:
                    return "92 - DEVOLVER APÓS XX DIAS DO VENCIMENTO";
                default:
                    return "--";
            }
        }


        public static string Instrucoes_Santander(EnumBoleto.Instrucoes_Santander tp)
        {
            switch (tp)
            {
                case EnumBoleto.Instrucoes_Santander.Baixar_Apos_15_Dias:
                    return "02 - BAIXAR APÓS QUINZE DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Santander.Baixar_Apos_30_Dias:
                    return "03 - BAIXAR APÓS 30 DIAS DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Santander.Nao_Baixar:
                    return "04 - NÃO BAIXAR";
                case EnumBoleto.Instrucoes_Santander.Protestar:
                    return "06 - PROTESTAR APÓS XX DO VENCIMENTO";
                case EnumBoleto.Instrucoes_Santander.Nao_Protestar:
                    return "07 - NÃO PROTESTAR";
                case EnumBoleto.Instrucoes_Santander.Nao_Cobrar_Juros_DeMora:
                    return "08 - NÃO COBRAR JUROS DE MORA";
                case EnumBoleto.Instrucoes_Santander.Juros_Ao_Dia:
                    return "98 - APÓS VENCIMENTO COBRAR JUROS DE {0} {1} POR DIA DE ATRASO";
                case EnumBoleto.Instrucoes_Santander.Multa_Vencimento:
                    return "99 - APÓS VENCIMENTO COBRAR MULTA DE XX";
                default:
                    return "--";
            }
        }
        public static object Retnor()
        {
            return new { Letra = "A", Msg = "teste" };
        }
        public static string InstrucaoItauObs(EnumBoleto.Letra tp)
        {
            switch (tp)
            {
                case EnumBoleto.Letra.A:
                    return "No caso da instrução “42”, o beneficiário passa a ter prioridade no recebimento quando o pagador estiver com falência decretada. A quantidade de dias será utilizada para as instruções de negativação expressa e protesto, não sendo possível informar no arquivo remessa de entrada as instruções de negativação e protesto, simultaneamente para o mesmo título. Se forem enviadas as duas instruções, a única a ser considerada será a instrução da negativação. Na negativação, caso o campo quantidade de dias seja informado com zeros, será considerado o prazo de <strong>2 (dois)</strong> dias corridos após o vencimento. No caso da instrução “39”, se informar “00” será impresso no boleto a literal< strong > “NÃO RECEBER APÓS O VENCIMENTO”</ strong >.";
                case EnumBoleto.Letra.B:
                    return "O conteúdo da mensagem será informado na primeira linha disponível no campo de <strong>“instruções”</strong> do boleto. Caso todas as linhas deste campo estejam ocupadas, a mensagem será impressa acima do campo “Sacador / Avalista”. Utilizando-se deste campo para instrução “93”, para indicação do nome e dados do sacador / avalista, deve-se utilizar-se do registro tipo “1” ou do registro tipo “5”.";
                case EnumBoleto.Letra.C:
                    return "O conteúdo da mensagem será informado na primeira linha disponível no campo de <strong>“instruções”</strong> do boleto. Caso todas as linhas deste campo estejam ocupadas, a mensagem será impressa nos campos “Sacador / Avalista” e “data da mora”. Utilizando-se deste campo para instrução “94”, para indicação do nome e dados do sacador / avalista, deve-se utilizar-se do registro tipo “1” ou do registro tipo “5”";
                case EnumBoleto.Letra.D:
                    return "Informar o número da Duplicata/Fatura nas posições 087 a 106. Se este campo estiver com brancos ou zeros, a mensagem não será impressa.";
                case EnumBoleto.Letra.E:
                    return "Informar o valor do desconto por dia nas posições 180 a 192.";
                case EnumBoleto.Letra.F:
                    return "Informar o valor por dia de atraso nas posições 161 a 173 e a data nas posições 386 a 391.";
                case EnumBoleto.Letra.G:
                    return "Pode ser cancelada pela agência, Itaú Empresas na Internet ou através de arquivo, Código de Ocorrência 35, Nota 6, (utilizando a instrução 2196). Depois de cancelada, comandar a instrução de protesto novamente.";
                case EnumBoleto.Letra.H:
                    return "É impressa mensagem no boleto informando prazo de protesto.";
                case EnumBoleto.Letra.I:
                    return "Informar o VALOR do abatimento (nunca em percentual) referente a PIS-PASEP/COFIN/CSSL nas posições 206 a 218. A instrução será impressa no boleto com a literal: “ABATIMENTO DE XXXX.XXX,XX REF. PIS - PASEP / COFIN / CSSL, MESMO APOS VCTO”.";
                default:
                    return "--";
            }
        }

        #endregion

    }
    public static class EnumBoletoLista
    {
        #region ITAU
        public static List<DictionaryEntry> Instrucoes_Itau()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Informacao_Responsabilidade_Beneficiario), Value = (short)EnumBoleto.Instrucoes_Itau.Informacao_Responsabilidade_Beneficiario });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_05_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_05_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_30_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_30_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Receber_Conforme_Instrucoes_No_Proprio_Titulo), Value = (short)EnumBoleto.Instrucoes_Itau.Receber_Conforme_Instrucoes_No_Proprio_Titulo });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_10_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_10_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_15_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_15_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_20_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_20_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Protestar), Value = (short)EnumBoleto.Instrucoes_Itau.Protestar });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Protestar), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Protestar });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_25_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_25_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_35_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_35_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_40_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_40_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_45_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_45_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_50_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_50_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_55_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_55_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_60_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_60_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_90_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_90_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_05_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_05_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_10_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_10_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_15_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_15_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_20_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_20_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_25_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_25_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_30_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_30_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_35_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_35_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_40_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_40_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_45_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_45_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_50_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_50_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_55_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_55_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Importancia_De_Desconto_Por_Dia), Value = (short)EnumBoleto.Instrucoes_Itau.Importancia_De_Desconto_Por_Dia });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_60_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_60_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_90_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_90_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Conceder_Abatimento_Mesmo_Apos_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Conceder_Abatimento_Mesmo_Apos_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Protestar_Apos_Xx_Dias_Corridos_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Protestar_Apos_Xx_Dias_Corridos_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Protestar_Apos_Xx_Dias_Uteis_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Protestar_Apos_Xx_Dias_Uteis_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Receber_Ate_O_Ultimo_Dia_Do_Mes_De_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Receber_Ate_O_Ultimo_Dia_Do_Mes_De_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Conceder_Desconto_Mesmo_Apos_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Conceder_Desconto_Mesmo_Apos_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_O_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_O_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Conceder_Desconto_Conforme_Nota_De_Credito), Value = (short)EnumBoleto.Instrucoes_Itau.Conceder_Desconto_Conforme_Nota_De_Credito });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Protesto_Para_Fins_Falimentares), Value = (short)EnumBoleto.Instrucoes_Itau.Protesto_Para_Fins_Falimentares });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Sujeito_A_Protesto_Se_Nao_For_Pago_No_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Sujeito_A_Protesto_Se_Nao_For_Pago_No_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Importancia_Por_Dia_De_Atraso_A_Partir_De_Ddmmaa), Value = (short)EnumBoleto.Instrucoes_Itau.Importancia_Por_Dia_De_Atraso_A_Partir_De_Ddmmaa });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Tem_Dia_Da_Graca), Value = (short)EnumBoleto.Instrucoes_Itau.Tem_Dia_Da_Graca });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Dispensar_Juros_Comissao_De_Permanencia), Value = (short)EnumBoleto.Instrucoes_Itau.Dispensar_Juros_Comissao_De_Permanencia });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Receber_Somente_Com_A_Parcela_Anterior_Quitada), Value = (short)EnumBoleto.Instrucoes_Itau.Receber_Somente_Com_A_Parcela_Anterior_Quitada });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Efetuar_O_Pagamento_Somente_Atraves_Deste_Boleto_E_Na_Rede_Bancaria), Value = (short)EnumBoleto.Instrucoes_Itau.Efetuar_O_Pagamento_Somente_Atraves_Deste_Boleto_E_Na_Rede_Bancaria });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Apos_Vencimento_Pagavel_Somente_Na_Empresa), Value = (short)EnumBoleto.Instrucoes_Itau.Apos_Vencimento_Pagavel_Somente_Na_Empresa });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Somar_Valor_Do_Titulo_Ao_Valor_Do_Campo_Mora_Multa_Caso_Exista), Value = (short)EnumBoleto.Instrucoes_Itau.Somar_Valor_Do_Titulo_Ao_Valor_Do_Campo_Mora_Multa_Caso_Exista });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_365_Dias_De_Vencido), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_365_Dias_De_Vencido });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Cobranca_Negociada_Pagavel_Somente_Por_Este_Boleto_Na_Rede_Bancaria), Value = (short)EnumBoleto.Instrucoes_Itau.Cobranca_Negociada_Pagavel_Somente_Por_Este_Boleto_Na_Rede_Bancaria });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Titulo_Entregue_Em_Penhor_Em_Favor_Do_Beneficiario_Acima), Value = (short)EnumBoleto.Instrucoes_Itau.Titulo_Entregue_Em_Penhor_Em_Favor_Do_Beneficiario_Acima });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Titulo_Transferido_A_Favor_Do_Beneficiario), Value = (short)EnumBoleto.Instrucoes_Itau.Titulo_Transferido_A_Favor_Do_Beneficiario });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Entrada_Em_Negativacao_Expressa), Value = (short)EnumBoleto.Instrucoes_Itau.Entrada_Em_Negativacao_Expressa });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Negativar), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Negativar });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Valor_Da_Ida_Engloba_Multa_De_10_Porcento_Pro_Rata), Value = (short)EnumBoleto.Instrucoes_Itau.Valor_Da_Ida_Engloba_Multa_De_10_Porcento_Pro_Rata });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Cobrar_Juros_Apos_15_Dias_Da_Emissao), Value = (short)EnumBoleto.Instrucoes_Itau.Cobrar_Juros_Apos_15_Dias_Da_Emissao });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Operacao_Ref_A_Vendor), Value = (short)EnumBoleto.Instrucoes_Itau.Operacao_Ref_A_Vendor });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Apos_Vencimento_Consultar_A_Agencia_Beneficiario), Value = (short)EnumBoleto.Instrucoes_Itau.Apos_Vencimento_Consultar_A_Agencia_Beneficiario });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Antes_Do_Vencimento_Ou_Apos_15_Dias), Value = (short)EnumBoleto.Instrucoes_Itau.Antes_Do_Vencimento_Ou_Apos_15_Dias });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Antes_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Antes_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.No_Vencimento_Pagavel_Em_Qualquer_Agencia_Bancaria), Value = (short)EnumBoleto.Instrucoes_Itau.No_Vencimento_Pagavel_Em_Qualquer_Agencia_Bancaria });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_Xx_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Nao_Receber_Apos_Xx_Dias_Do_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Itau(EnumBoleto.Instrucoes_Itau.Devolver_Apos_Xx_Dias_Do_Vencimento), Value = (short)EnumBoleto.Instrucoes_Itau.Devolver_Apos_Xx_Dias_Do_Vencimento });
            return lista;
        }

        public static List<DictionaryEntry> Instrucoes_Santander()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Santander(EnumBoleto.Instrucoes_Santander.Baixar_Apos_15_Dias), Value = (short)EnumBoleto.Instrucoes_Santander.Baixar_Apos_15_Dias });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Santander(EnumBoleto.Instrucoes_Santander.Baixar_Apos_30_Dias), Value = (short)EnumBoleto.Instrucoes_Santander.Baixar_Apos_30_Dias });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Santander(EnumBoleto.Instrucoes_Santander.Juros_Ao_Dia), Value = (short)EnumBoleto.Instrucoes_Santander.Juros_Ao_Dia });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Santander(EnumBoleto.Instrucoes_Santander.Multa_Vencimento), Value = (short)EnumBoleto.Instrucoes_Santander.Multa_Vencimento });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Santander(EnumBoleto.Instrucoes_Santander.Nao_Baixar), Value = (short)EnumBoleto.Instrucoes_Santander.Nao_Baixar });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Santander(EnumBoleto.Instrucoes_Santander.Nao_Cobrar_Juros_DeMora), Value = (short)EnumBoleto.Instrucoes_Santander.Nao_Cobrar_Juros_DeMora });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Santander(EnumBoleto.Instrucoes_Santander.Nao_Protestar), Value = (short)EnumBoleto.Instrucoes_Santander.Nao_Protestar });
            lista.Add(new DictionaryEntry() { Key = EnumBoletoDescricao.Instrucoes_Santander(EnumBoleto.Instrucoes_Santander.Protestar), Value = (short)EnumBoleto.Instrucoes_Santander.Protestar });

            return lista;
        }
        public static List<DictionaryEntry> Carteira_Itau()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Direta_Eletronica_Emissao_Intregal_Carne), Key = (short)EnumBoleto.Carteira_Itau.Direta_Eletronica_Emissao_Intregal_Carne });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Direta_Eletronica_Emissao_Intregal_Simples), Key = (short)EnumBoleto.Carteira_Itau.Direta_Eletronica_Emissao_Intregal_Simples });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Direta_Eletronica_Emissao_Parcial_Simples), Key = (short)EnumBoleto.Carteira_Itau.Direta_Eletronica_Emissao_Parcial_Simples });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Direta_Eletronica_Sem_Emissao_Dolar), Key = (short)EnumBoleto.Carteira_Itau.Direta_Eletronica_Sem_Emissao_Dolar });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Direta_Eletronica_Sem_Emissao_Simples), Key = (short)EnumBoleto.Carteira_Itau.Direta_Eletronica_Sem_Emissao_Simples });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Duplicatas_Transferencia_Desconto), Key = (short)EnumBoleto.Carteira_Itau.Duplicatas_Transferencia_Desconto });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Escritural_Eletronica_Carne), Key = (short)EnumBoleto.Carteira_Itau.Escritural_Eletronica_Carne });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Escritural_Eletronica_Cobranca_Inteligente), Key = (short)EnumBoleto.Carteira_Itau.Escritural_Eletronica_Cobranca_Inteligente });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Escritural_Eletronica_Dolar), Key = (short)EnumBoleto.Carteira_Itau.Escritural_Eletronica_Dolar });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Escritural_Eletronica_Simples), Key = (short)EnumBoleto.Carteira_Itau.Escritural_Eletronica_Simples });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Itau(EnumBoleto.Carteira_Itau.Escritural_Eletronica_Simples_Faixa_Nosso_Numero_Livre), Key = (short)EnumBoleto.Carteira_Itau.Escritural_Eletronica_Simples_Faixa_Nosso_Numero_Livre });

            return lista;
        }

        public static List<DictionaryEntry> Carteira_Santander()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Santander(EnumBoleto.Carteira_Santander.Cobranca_Simples_Com_Registro), Key = (short)EnumBoleto.Carteira_Santander.Cobranca_Simples_Com_Registro });
            lista.Add(new DictionaryEntry() { Value = EnumBoletoDescricao.Carteira_Santander(EnumBoleto.Carteira_Santander.Cobranca_Simples_Sem_Registro), Key = (short)EnumBoleto.Carteira_Santander.Cobranca_Simples_Sem_Registro });

            return lista;
        }

        #endregion
    }
}
