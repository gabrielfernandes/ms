using System;
using System.Collections.Generic;
using System.Text;

namespace Cobranca.Helpers.BNet.Enums
{
    internal enum DateInterval
    {
        Second,
        Minute,
        Hour,
        Day,
        Week,
        Month,
        Quarter,
        Year
    }
}
