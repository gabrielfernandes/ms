﻿namespace Cobranca.Helpers.BNet.Enums
{
    public static class TipoOcorrenciaRemessaExtension
    {
        public static string Format(this BoletoNet.TipoOcorrenciaRemessa ocorrencia)
        {
            return Utils.FormatCode(((int)ocorrencia).ToString(), 2);
        }
    }
}
