﻿using Cobranca.Helpers.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Dynamic;

namespace Cobranca.Helpers
{
    public class DTable
    {
        public class Rotinas
        {

            public static List<object> GetResult(DTParameters param, IQueryable<object> dtResult)
            {
                if (param.Length > 0)
                {
                    return FilterResult(dtResult, param).Skip(param.Start).Take(param.Length).ToList();
                }
                else
                {
                    return FilterResult(dtResult, param).Skip(param.Start).ToList();
                }
            }
            public static List<object> DTGetResultExport(DTParameters param, IQueryable<object> dtResult)
            {
                return FilterResult(dtResult, param).Skip(param.Start).ToList();
            }
            public static int Count(DTParameters param, IQueryable<object> dtResult)
            {
                return FilterResult(dtResult, param).ToList().Count();
            }
            public static IEnumerable<object> FilterResult(IQueryable<object> dtResult, DTParameters param)
            {
                var _where = "1 = 1";
                if (param.Columns != null)
                {
                    foreach (var col in param.Columns)
                    {
                        if (col.Search.Value != null)
                        {
                            var search = col.Search.Value;
                            string[] expression = { ">", "<", "<>", "=", ">=", "<=", "=>", "=<" };
                            string[] expressionLinq = { "!=", "==" };
                            string[] expressionDate = { "/" };
                            string[] expressionNumber = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
                            if (expression.Any(x => search.Contains(x)))
                            {
                                search = search.Replace(",", ".");
                                if (search.Contains("<>"))
                                {
                                    _where += $@" && {col.Name} != {search.Replace("<>", "")}";
                                }
                                else if (search.Contains("="))
                                {

                                    if (search.Contains(">="))
                                    {
                                        _where += $@" && {col.Name} {search}";
                                    }
                                    else if (search.Contains("<="))
                                    {
                                        _where += $@" && {col.Name} {search}";
                                    }
                                    else if (search.Contains("=>"))
                                    {
                                        _where += $@" && {col.Name} >= {search.Replace("=>", "")}";
                                    }
                                    else if (search.Contains("=<"))
                                    {
                                        _where += $@" && {col.Name} <= {search.Replace("=<", "")}";
                                    }
                                    else
                                    {
                                        _where += $@" && {col.Name} == {search.Replace("=", "")}";
                                    }
                                }
                                else
                                {
                                    _where += $@" && {col.Name} {search}";
                                }
                            }
                            else if (expressionDate.Any(x => search.Contains(x)))
                            {
                                _where += $@" && {col.Name} == Convert.ToDateTime(""{ BaseRotinas.ConverterParaDatetime(search) }"")";
                            }
                            else if (expressionNumber.Any(x => search.Contains(x)))
                            {
                                _where += $@" && {col.Name}.ToString().Contains(""{search}"")";
                            }
                            else
                            {
                                _where += $@" && {col.Data}.ToLower().Contains(""{col.Search.Value.Replace("*", "").ToLower()}"")";
                            }

                        }
                    }
                }
                var _order = "";
                if (param.Order != null)
                {
                    foreach (var o in param.Order)
                    {
                        if (o != null)
                        {
                            _order += $@"{param.Columns[o.Column].Name} {o.Dir}";
                        }
                        if (_order == " ASC" || _order == " DESC")
                        {
                            _order = "Cod ASC";
                        }
                    }
                }
                var teste = (dtResult.AsEnumerable().Where(_where));
                if (param.Order != null)
                {
                    teste = teste.OrderBy(_order);
                }
                return teste;
            }

            public static DTQuery FilterConvertToQuery(DTParameters param)
            {
                var _where = "1 = 1";
                if (param.Columns != null)
                {
                    foreach (var col in param.Columns)
                    {
                        if (col.Search.Value != null)
                        {
                            var search = col.Search.Value;
                            var encrypt = false;

                            if (col.Data.Substring((col.Data.Length - 1), 1) == "_")
                                encrypt = true;

                            string[] expression = { ">", "<", "<>", "=", ">=", "<=", "=>", "=<" };
                            string[] expressionLinq = { "!=", "==" };
                            string[] expressionDate = { "/" };
                            string[] expressionNumber = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
                            if (expression.Any(x => search.Contains(x)))
                            {


                                search = search.Replace(",", ".");
                                if (search.Contains("<>"))
                                {
                                    _where += $@" AND {col.Name} != {search.Replace("<>", "")}";
                                }
                                else if (search.Contains("="))
                                {

                                    if (search.Contains(">="))
                                    {
                                        _where += $@" AND {col.Name} {search}";
                                    }
                                    else if (search.Contains("<="))
                                    {
                                        _where += $@" AND {col.Name} {search}";
                                    }
                                    else if (search.Contains("=>"))
                                    {
                                        _where += $@" AND {col.Name} >= {search.Replace("=>", "")}";
                                    }
                                    else if (search.Contains("=<"))
                                    {
                                        _where += $@" AND {col.Name} <= {search.Replace("=<", "")}";
                                    }
                                    else
                                    {
                                        _where += $@" AND {col.Name} = {search.Replace("=", "")}";
                                    }
                                }
                                else
                                {
                                    _where += $@" AND {col.Name} {search}";
                                }
                            }
                            else if (expressionDate.Any(x => search.Contains(x)))
                            {
                                _where += $@" AND CAST({col.Name} as Date) = CAST(CONVERT(DATETIME,""{ BaseRotinas.ConverterParaDatetime(search) }"") as Date)";
                            }
                            else if (expressionNumber.Any(x => search.Contains(x)))
                            {
                                _where += $@" AND {col.Name} LIKE('{search}')";
                            }
                            else
                            {
                                if (search.Contains("*"))
                                {
                                    search.Replace("*", "");
                                }
                                search = BaseRotinas.Descriptografar(search);
                                _where += $@" AND {col.Name} LIKE('{search.ToLower()}')";
                            }

                        }
                    }
                }
                var _order = "";
                if (param.Order != null)
                {
                    foreach (var o in param.Order)
                    {
                        if (o != null)
                        {
                            _order += $@"{param.Columns[o.Column].Name} {o.Dir}";
                        }
                        if (_order == " ASC" || _order == " DESC")
                        {
                            _order = "Cod ASC";
                        }
                    }
                }
                var DE = param.Start;
                var ATE = param.Length;
                DTQuery objRetorno = new DTQuery()
                {
                    Registro_De = DE,
                    Registro_Ate = DE + ATE,
                    Where = $" ({_where})",
                    Order_by = _order
                };

                return objRetorno;
            }



            public static DTResult Result(DTParameters param, IQueryable<object> dtResult)
            {
                var data = GetResult(param, dtResult);
                int count = Count(param, dtResult);
                DTResult result = new DTResult
                {
                    draw = param.Draw,
                    data = data,
                    recordsFiltered = count,
                    recordsTotal = count
                };
                return result;
            }

            public static DTResult ResultDT<T>(int total, int draw, List<T> dtResult)
            {
                var list = dtResult.Cast<object>().ToList();
                DTResult result = new DTResult
                {
                    draw = draw,
                    data = list,
                    recordsFiltered = total,
                    recordsTotal = total
                };
                return result;
            }
        }
    }
}
