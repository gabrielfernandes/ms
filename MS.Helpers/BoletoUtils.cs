﻿using Boleto.Net.BoletoJunix;
using BoletoNet;
using BoletoNet.Enums;
using BoletoNet.Util;
using Cobranca.Helpers.BNet;
using System;
using System.Collections.Generic;

namespace Cobranca.Helpers
{
    public class BoletoUtils
    {
        public static string MontarLinhaDigitavel(
            decimal valorDocumento,
            DateTime dataVencimento,
            string numeroDocumento,
            string bancoNumero,
            int nossoNumero,
            string cnpj,
            string nome,
            string agencia,
            string agenciaDigito,
            string conta,
            string contaDigito,
            string carteira,
            string endereco,
            string enderecoNumero,
            string bairro,
            string cidade,
            string uf,
            string cep)
        {
            try
            {
                Cedente cedente = MontarCedente(
                    cnpj,
                    nome,
                    0,
                    agencia,
                    conta,
                    contaDigito,
                    carteira,
                    endereco,
                    enderecoNumero,
                    bairro,
                    cidade,
                    uf,
                    cep
                    );
                var _nossoNumero = FitStringLength(nossoNumero.ToString(), 8, 8, '0', 0, true, true, true);
                BoletoNet.Boleto boleto = new BoletoNet.Boleto(Convert.ToDateTime(dataVencimento), Convert.ToDecimal(valorDocumento), cedente.Carteira, _nossoNumero, cedente);
                boleto.Banco = new Banco(BaseRotinas.ConverterParaInt(bancoNumero));
                boleto.ContaBancaria = new ContaBancaria(agencia, agenciaDigito, conta, contaDigito);
                BoletoBancario boleto_bancario = new BoletoBancario();
                boleto_bancario.CodigoBanco = BaseRotinas.ConverterParaShort(bancoNumero);
                boleto.NumeroDocumento = numeroDocumento;
                boleto_bancario.Boleto = boleto;
                boleto.Banco.FormataCodigoBarra(boleto);
                boleto.Banco.FormataLinhaDigitavel(boleto);
                return boleto.CodigoBarra.LinhaDigitavel;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar a linha digitavel.", ex);
            }
        }

        public static Cedente MontarCedente(
            string cnpj,
            string nome,
            int? convenio,
            string agencia,
            string conta,
            string contaDigito,
            string carteira,
            string endereco,
            string enderecoNumero,
            string bairro,
            string cidade,
            string uf,
            string cep
            )
        {
            try
            {
                #region Cedente
                //Cedente cedente = new Cedente("48079274000144", "ELEVADORES OTIS LTDA", "2938", "00589", "5");
                Cedente cedente = new Cedente(cnpj, nome, agencia, conta, contaDigito ?? "");
                if (convenio > 0)
                    cedente.Convenio = BaseRotinas.ConverterParaInt(convenio);
                cedente.ContaBancaria.DigitoAgencia = "";
                //cedente.ContaBancaria = new ContaBancaria(agencia, "0", conta, conta, contaDigito);
                //cedente.Codigo = conta;
                cedente.Carteira = carteira;
                cedente.Endereco = new Endereco();
                cedente.Endereco.End = endereco != null ? endereco : "";
                cedente.Endereco.Numero = enderecoNumero != null ? enderecoNumero : "";
                cedente.Endereco.Complemento = null;
                cedente.Endereco.Bairro = bairro;
                cedente.Endereco.Cidade = cidade;
                cedente.Endereco.UF = uf;
                cedente.Endereco.CEP = cep;

                cedente.CodigoTransmissao = "0";

                #endregion
                return cedente;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao montar cedente", ex);
            }
        }


        public static Sacado MontarSacado(
                string cliente,
                string documento,
                string endereco,
                string enderecoNumero,
                string cep,
                string bairro,
                string cidade,
                string uf,
                string complemento
            )
        {
            try
            {
                Endereco sacadoEndereco = new Endereco();
                sacadoEndereco.End = endereco ?? "--";
                sacadoEndereco.Numero = enderecoNumero ?? "--";
                sacadoEndereco.CEP = cep ?? "--";
                sacadoEndereco.Bairro = bairro ?? "--";
                sacadoEndereco.Cidade = cidade ?? "--";
                sacadoEndereco.UF = uf ?? "--";
                sacadoEndereco.Complemento = complemento ?? "--";

                if (documento.Length < 11)
                {
                    int qtd = 11 - documento.Length;
                    for (int i = 0; i < qtd; i++)
                    {
                        documento = "0" + documento;
                    }
                }
                else if (documento.Length > 11 && documento.Length < 14)
                {
                    documento = "0" + documento;
                }
                Sacado sacado = new Sacado(
                    documento,
                    cliente,
                    sacadoEndereco);
                return sacado;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro no Sacado .", ex);
            }
        }

        /// <summary>
        /// Formata o campo de acordo com o tipo e o tamanho 
        /// </summary>        
        public static string FitStringLength(string SringToBeFit, int maxLength, int minLength, char FitChar, int maxStartPosition, bool maxTest, bool minTest, bool isNumber)
        {
            try
            {
                string result = "";

                if (maxTest == true)
                {
                    // max
                    if (SringToBeFit.Length > maxLength)
                    {
                        result += SringToBeFit.Substring(maxStartPosition, maxLength);
                    }
                }

                if (minTest == true)
                {
                    // min
                    if (SringToBeFit.Length <= minLength)
                    {
                        if (isNumber == true)
                        {
                            result += (string)(new string(FitChar, (minLength - SringToBeFit.Length)) + SringToBeFit);
                        }
                        else
                        {
                            result += (string)(SringToBeFit + new string(FitChar, (minLength - SringToBeFit.Length)));
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Exception tmpEx = new Exception("Problemas ao Formatar a string. String = " + SringToBeFit, ex);
                throw tmpEx;
            }
        }

        //

        public static decimal? CalcularValorJurosSimples(decimal ValorPresente, decimal Taxa, DateTime DataVencimento, DateTime DataBoleto)
        {
            var DtNow = DateTime.Now;
            var QtdDay = (DataBoleto - DataVencimento).Days;
            if (QtdDay <= 0)
            {
                return 0;
            }
            else
            {
                return decimal.Round(((((Taxa / 30) * QtdDay) / 100) * ValorPresente), 2, MidpointRounding.AwayFromZero);
            }
        }


        public static decimal? CalcularValorJurosComposto(decimal ValorPresente, decimal Taxa, int Periodo)
        {
            return ((ValorPresente * (decimal)Math.Pow(1.0 + (BaseRotinas.ConverterParaDouble(Taxa) / 100), Periodo)) - ValorPresente);
        }


        internal static decimal? CalcularValorMulta(decimal? Valor, decimal? MultaAtraso, DateTime DataVencimento)
        {
            var DtNow = DateTime.Now;
            var DtFirstDayMonth = new DateTime(DtNow.Year, DtNow.Month, 1);
            if (DataVencimento.Date >= DtNow.Date)
            {
                return 0;
            }
            else if (DataVencimento.Date >= DtFirstDayMonth.Date)
            {
                return (Valor * (MultaAtraso / 100));
            }
            else
            {
                return (Valor * (MultaAtraso / 100));
            }
        }

        public static decimal? CalcularValorMulta(decimal? Valor, decimal? MultaAtraso, DateTime DataVencimento, DateTime DataBoleto)
        {
            if (DataBoleto.Date <= DataVencimento.Date)
            {
                return 0;
            }
            else if (DataBoleto.Date > DataVencimento.Date)
            {
                return decimal.Round(BaseRotinas.ConverterParaDecimal(Valor * (MultaAtraso / 100)), 2, MidpointRounding.AwayFromZero);
            }
            else
            {
                return decimal.Round(BaseRotinas.ConverterParaDecimal(Valor * (MultaAtraso / 100)), 2, MidpointRounding.AwayFromZero);
            }
        }

        public static BoletoNet.Boleto ConvertListBoletoToListBoletoNet(
            int codBoleto,
            string codCliente,
            string numeroDocumento,
            int numeroParcela,
            string nossoNumero,
            DateTime dataVencimento,
            DateTime dataAprovacao,
            decimal valorDocumento,
            string parcelaTipoFatura,
            string companhiaFiscal,
            bool? alterarExistente,
            short tipoSolicitacao,
            decimal multaAtrasoPorcentagem,
            decimal jurosAoMesPorcentagem,
            string companhia,
            Banco _banco,
            Cedente _cedente,
            string clienteNome,
            string clienteDocumento,
            string clienteEndereco,
            string clienteNumero,
            string clienteCEP,
            string clienteBairro,
            string clienteCidade,
            string clienteUF,
            string clienteComplemento,
            List<string> listaInstrucaoDescricao)
        {
            BoletoNet.Boleto boleto = new BoletoNet.Boleto(Convert.ToDateTime(dataVencimento), (decimal)valorDocumento, BaseRotinas.ConverterParaString(_cedente.Carteira), _cedente.ContaBancaria.Conta, _cedente);
            boleto.NumeroDocumento = numeroDocumento;
            boleto.CodBoleto = codBoleto;
            boleto.CompanhiaFiscal = BaseRotinas.FormatCode(companhiaFiscal, "0", 4, true);
            boleto.Companhia = BaseRotinas.FormatCode(companhia, "0", 4, true);
            boleto.NossoNumero = BaseRotinas.ConverterParaString(nossoNumero);

            boleto.EspecieDocumento = DefinirEspecie((EnumBoleto.BancoCodigo)_banco.Codigo);
            boleto.Banco = _banco;
            boleto.DataDocumento = (DateTime)dataAprovacao;

            boleto.Sacado = BoletoUtils.MontarSacado(
                        clienteNome,
                        clienteDocumento,
                        clienteEndereco,
                        clienteNumero,
                        clienteCEP,
                        clienteBairro,
                        clienteCidade,
                        clienteUF,
                        clienteComplemento
                        );
            boleto.DataDesconto = boleto.DataVencimento;


            //Validar a multa com otis.
            boleto.JurosMora = (multaAtrasoPorcentagem / 10);

            //***********************INFORMACOES QUE DEVERAO FORMAR UMA CHAVE NO ARQUIVO REMESSA**********************
            boleto.Companhia = BaseRotinas.FormatCode(companhia, "0", 4, true);
            boleto.CodCliente = BaseRotinas.FormatCode(codCliente, "0", 8, true);
            boleto.NumeroParcela = BaseRotinas.ConverterParaInt(numeroParcela);
            boleto.TipoDocumento = parcelaTipoFatura;
            boleto.Alterar = alterarExistente ?? false;
            boleto.TipoSolicitacao = tipoSolicitacao;

            boleto = MontarInstruicao(boleto, valorDocumento, dataVencimento, jurosAoMesPorcentagem, multaAtrasoPorcentagem, listaInstrucaoDescricao, _banco.Codigo);

            return boleto;

        }


        #region MONTAR REMESSA ITAU
        public static string MontarRemessaItau(
            Banco banco,
            Cedente cedente,
            int LoteAtual,
            int qtdTitulosTotal,
            List<BoletoNet.Boleto> boletos,
            ref List<int> ListaBoletoOK
            )
        {
            #region UTILs
            int numeroRegistro = 2;
            int numeroRegistroLote = 1;
            decimal vlTitulosTotal = 0;
            string strArchive = "";
            #endregion

            #region HEADER CABEÇALHO
            //GERAR O HEADER (CABEÇALHO)
            string _header = " ";
            _header = BoletoUtils.GerarHeaderRemessaCNAB240(cedente, banco.Codigo, LoteAtual);
            strArchive += $"{_header}{Environment.NewLine}";
            #endregion

            #region HEADER CABEÇALHO ADICIONAL
            //GERAR O HEADER (CABEÇALHO ADICIONAL)
            string _headerAdicional = " ";
            _headerAdicional = BoletoUtils.GerarHeaderLoteRemessaCNAB240(cedente, banco.Codigo, (int)LoteAtual);
            strArchive += $"{_headerAdicional}{Environment.NewLine}";
            #endregion

            #region DETALHE CONTEUDO
            //PREENCHER OS DETALHES
            foreach (BoletoNet.Boleto _boleto in boletos)
            {
                try
                {

                    _boleto.Banco = banco;

                    //VERIFICA SE O BOLETO É ALTERACAO
                    if (_boleto.Alterar)
                    {
                        //GERAR O DETALHE P
                        string DetalheAlteracaoP = "";
                        DetalheAlteracaoP = BoletoUtils.GerarDetalheAlterarcaoDadosSegmentoPRemessa240(_boleto, numeroRegistroLote, "");
                        strArchive += $"{DetalheAlteracaoP}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE Q
                        string DetalheAlteracaoQ = "";
                        DetalheAlteracaoQ = BoletoUtils.GerarDetalheAlterarcaoDadosSegmentoQRemessa240(_boleto, numeroRegistroLote, "");
                        strArchive += $"{DetalheAlteracaoQ}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE P
                        string DetalheVencimentoP = "";
                        DetalheVencimentoP = BoletoUtils.GerarDetalheAlterarcaoVencimentoSegmentoPRemessa240(_boleto, numeroRegistroLote, "");
                        strArchive += $"{DetalheVencimentoP}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE Q
                        string DetalheVencimentoQ = "";
                        DetalheVencimentoQ = BoletoUtils.GerarDetalheAlterarcaoVencimentoSegmentoQRemessa240(_boleto, numeroRegistroLote, "");
                        strArchive += $"{DetalheVencimentoQ}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;

                    }
                    else
                    {
                        //GERAR O DETALHE P
                        string DetalheP = "";
                        DetalheP = BoletoUtils.GerarDetalheSegmentoPRemessa240(_boleto, numeroRegistroLote, "");
                        strArchive += $"{DetalheP}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE Q
                        string DetalheQ = "";
                        DetalheQ = BoletoUtils.GerarDetalheSegmentoQRemessa240(_boleto, numeroRegistroLote);
                        strArchive += $"{DetalheQ}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;
                        qtdTitulosTotal++;
                    }

                    ListaBoletoOK.Add(_boleto.CodBoleto);
                }
                catch (Exception)
                {
                }
            }
            #endregion

            #region GERAR O TRAILER LOTE (RODAPE LOTE)
            //GERAR O TRAILER LOTE (RODAPE LOTE)
            string _radapeLote = " ";
            _radapeLote = BoletoUtils.GerarTrailerLoteRemessa240(banco.Codigo, numeroRegistro, vlTitulosTotal, qtdTitulosTotal);
            strArchive += $"{_radapeLote}{Environment.NewLine}";
            #endregion

            #region GERAR O TRAILER (RODAPE)
            //GERAR O TRAILER (RODAPE)
            string _radape = " ";
            _radape = BoletoUtils.GerarTrailerRemessa240(banco.Codigo, numeroRegistro);
            strArchive += $"{_radape}{Environment.NewLine}";
            #endregion

            return strArchive;
        }
        #endregion


        #region MONTAR REMESSA SANTANDER
        public static string MontarRemessaSantander(
            Banco banco,
            Cedente cedente,
            int LoteAtual,
            int qtdTitulosTotal,
            List<BoletoNet.Boleto> boletos,
            ref List<int> ListaBoletoOK
            )
        {
                        #region UTILs
            int numeroRegistro = 2;
            int numeroRegistroLote = 1;
            decimal vlTitulosTotal = 0;
            string strArchive = "";
            #endregion

            #region HEADER CABEÇALHO
            //GERAR O HEADER (CABEÇALHO)
            string _header = " ";
            _header = banco.GerarHeaderRemessa(cedente, TipoArquivo.CNAB240, LoteAtual);
            strArchive += $"{_header}{Environment.NewLine}";
            #endregion

            #region HEADER CABEÇALHO ADICIONAL
            //GERAR O HEADER (CABEÇALHO ADICIONAL)
            string _headerAdicional = " ";
            _headerAdicional = banco.GerarHeaderLoteRemessa("", cedente, LoteAtual, TipoArquivo.CNAB240);
            strArchive += $"{_headerAdicional}{Environment.NewLine}";
            #endregion

            #region DETALHE CONTEUDO
            //PREENCHER OS DETALHES
            foreach (BoletoNet.Boleto _boleto in boletos)
            {
                try
                {

                    _boleto.Banco = banco;

                    //VERIFICA SE O BOLETO É ALTERACAO
                    if (_boleto.Alterar)
                    {
                        //GERAR O DETALHE P
                        string DetalheAlteracaoP = "";
                        DetalheAlteracaoP = banco.GerarDetalheSegmentoPRemessa(_boleto, numeroRegistroLote, "");
                        //DetalheAlteracaoP = BoletoUtils.GerarDetalheAlterarcaoDadosSegmentoPRemessa240(_boleto, numeroRegistroLote, "");
                        strArchive += $"{DetalheAlteracaoP}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE Q
                        string DetalheAlteracaoQ = "";
                        DetalheAlteracaoQ = banco.GerarDetalheSegmentoQRemessa(_boleto, numeroRegistroLote, _boleto.Sacado);
                        //DetalheAlteracaoQ = BoletoUtils.GerarDetalheAlterarcaoDadosSegmentoQRemessa240(_boleto, numeroRegistroLote, "");
                        strArchive += $"{DetalheAlteracaoQ}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE P
                        string DetalheVencimentoP = "";
                        DetalheVencimentoP = banco.GerarDetalheSegmentoPRemessa(_boleto, numeroRegistroLote, "");
                        //DetalheVencimentoP = BoletoUtils.GerarDetalheAlterarcaoVencimentoSegmentoPRemessa240(_boleto, numeroRegistroLote, "");
                        strArchive += $"{DetalheVencimentoP}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE Q
                        string DetalheVencimentoQ = "";
                        DetalheVencimentoQ = banco.GerarDetalheSegmentoQRemessa(_boleto, numeroRegistroLote, _boleto.Sacado);
                        //DetalheVencimentoQ = BoletoUtils.GerarDetalheAlterarcaoVencimentoSegmentoQRemessa240(_boleto, numeroRegistroLote, "");
                        strArchive += $"{DetalheVencimentoQ}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;

                    }
                    else
                    {
                        //GERAR O DETALHE P
                        string DetalheP = "";
                        DetalheP = banco.GerarDetalheSegmentoPRemessa(_boleto, numeroRegistroLote, "");
                        //DetalheP = BoletoUtils.GerarDetalheSegmentoPRemessa240(_boleto, numeroRegistroLote, "");
                        strArchive += $"{DetalheP}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE Q
                        string DetalheQ = "";
                        DetalheQ = banco.GerarDetalheSegmentoQRemessa(_boleto, numeroRegistroLote, TipoArquivo.CNAB240);
                        //DetalheQ = BoletoUtils.GerarDetalheSegmentoQRemessa240(_boleto, numeroRegistroLote);
                        strArchive += $"{DetalheQ}{Environment.NewLine}";
                        numeroRegistroLote++;
                        numeroRegistro++;
                        qtdTitulosTotal++;
                    }

                    ListaBoletoOK.Add(_boleto.CodBoleto);
                }
                catch (Exception)
                {
                }
            }
            #endregion

            #region GERAR O TRAILER LOTE (RODAPE LOTE)
            //GERAR O TRAILER LOTE (RODAPE LOTE)
            string _radapeLote = " ";
            _radapeLote = banco.GerarTrailerLoteRemessa(numeroRegistro);
            //_radapeLote = BoletoUtils.GerarTrailerLoteRemessa240(banco.Codigo, numeroRegistro, vlTitulosTotal, qtdTitulosTotal);
            strArchive += $"{_radapeLote}{Environment.NewLine}";
            #endregion

            #region GERAR O TRAILER (RODAPE)
            //GERAR O TRAILER (RODAPE)
            string _radape = " ";

            _radapeLote = banco.GerarTrailerRemessa(numeroRegistro, TipoArquivo.CNAB240, cedente, vlTitulosTotal);
            _radape = BoletoUtils.GerarTrailerRemessa240(banco.Codigo, numeroRegistro);
            strArchive += $"{_radape}{Environment.NewLine}";
            #endregion

            return strArchive;
        }
        #endregion

        /// <summary>
        /// Gerar linha de Header
        /// </summary>
        /// <param name="cedente">classe preenchida de cedente.</param>
        /// <param name="nrBanco">Número do Banco</param>
        /// <param name="numeroArquivoRemessa">numero do lote do arquivo remessa</param>
        /// <returns>retorna o header</returns>
        public static string GerarHeaderRemessaCNAB240(Cedente cedente, int nrBanco, int numeroArquivoRemessa)
        {
            try
            {
                #region Comentário
                //POS INI/FINAL    TAMANHO Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
                //------------------------------------------------------------------------------------------------------------------------------------------------
                //001 - 003	    3	    Z0012	        Nº do Banco da Companhia. 	        CODIGO DO BCO NA COMPENSACAO                
                //004 - 007	    4	    S0001	        UDV - Valor Definido pelo Us.       LOTE DE SERVICO                             0000
                //008 - 008        01	    Z0065           Tipo de Linha do Formatador         REGISTRO HEADER DO ARQUIVO                   
                //009 - 017        09	    S0002           Em branco                           USO EXCLUSIVO NEXXERA                        
                //018 - 018        01	    Z0001           Cód.Pessoa Fís./Jurídica            TIPO DE INSCRICAO DA EMPRESA                29739737000102
                //019 - 032        14	    S0001           UDV - Valor Definido pelo Us.       CNPJ EMPRESA DEBITADA                        
                //033 - 052        20	    S0002           Em branco                           CODIGO DO CONVENIO DO BANCO                  
                //053 - 057        05	    Z0003           Agência Bancária da Companhia       NUMERO DA AGENCIA DEBITADA                   
                //058 - 058        01	    Z0005           Dígito da Agência Banc.da Cia       DIGITO VERIFICADOR AGENCIA                   
                //059 - 070        12	    Z0004           Nº da Cta Bancária da Cia           NUMERO DA C/C DEBITADA                       
                //071 - 071        01	    S0002           Em branco                           DIGITO VERIFICADOR CONTA CORRENTE            
                //072 - 072        01	    Z0006           Dígito da Conta Banc.da Cia         DAC DA AGENCIA/CTA DEBITADA                  
                //073 - 102        30	    Z0009           Nome da Companhia                   NOME DA EMPRESA                              
                //103 - 110        08	    Z0013           Nome do Banco da Companhia          NOME DO BANCO                                
                //111 - 132        22	    S0002           Em branco                           NOME DO BANCO                               NEXXERA
                //133 - 142        10	    S0001           UDV - Valor Definido pelo Us.       COMPLEMENTO DE REGISTROS                    1
                //143 - 143        01	    S0001           UDV - Valor Definido pelo Us.       CODIGO REMESSA/RETORNO                       
                //144 - 151        08	    Z0010           Data Criação do Arq.Formatado       DATA DA GERACAO DO ARQUIVO                   
                //152 - 157        06	    Z0011           Hora Criação do Arq.Formatado       HORA DE GERACAO DO ARQUIVO                   
                //158 - 164        07	    Z0058           Nº do Arquivo Bancário              NUMERO SEQUENCIAL DO ARQUIVO                
                //165 - 167        03	    S0001           UDV - Valor Definido pelo Us.       NUMERO DA VERSAO DO LAYOUT                  020
                //168 - 172        05	    S0001           UDV - Valor Definido pelo Us.       DENSIDADE DA GRAVACAO DO ARQUIVO            01600 
                //173 - 191        19	    S0002           Em branco                           PARA USO RESERVADO DO BANCO                  
                //192 - 211        20	    S0002           Em branco                           PARA USO RESERVADO DA EMPRESA                
                //212 - 221        10	    S0002           Em branco                           OBSERVACAO DO LAYOUT DO BANCO                
                //222 - 240        19      S0002	        Em branco                           USO EXCLUSIVO NEXXERA
                #endregion

                string header = Utils.FormatCode(nrBanco.ToString(), "0", 3, true); ;  // Nº do Banco da Companhia. / CODIGO DO BCO NA COMPENSACAO 
                header += "0000";    // UDV - Valor Definido pelo Us./ LOTE DE SERVICO / 0000
                header += "0";   // Tipo de Linha do Formatador         REGISTRO HEADER DO ARQUIVO                   
                header += Utils.FormatCode("", " ", 9);   // Em branco / USO EXCLUSIVO NEXXERA
                header += (cedente.CPFCNPJ.Length == 11 ? "1" : "2");   // Cód.Pessoa Fís./Jurídica / TIPO DE INSCRICAO DA EMPRESA / 29739737000102                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                header += Utils.FormatCode(cedente.CPFCNPJ, "0", 14, true);  // UDV - Valor Definido pelo Us. / CNPJ EMPRESA DEBITADA                        
                header += Utils.FormatCode("", " ", 20);
                header += "0";    // Em branco / CODIGO DO CONVENIO DO BANCO                                                                                                       
                header += Utils.FormatCode(cedente.ContaBancaria.Agencia, " ", 4, true); // Agência Bancária da Companhia / NUMERO DA AGENCIA DEBITADA                                          
                header += " ";   // Dígito da Agência Banc.da Cia / DIGITO VERIFICADOR AGENCIA                   
                header += "0000000";
                header += Utils.FormatCode(cedente.ContaBancaria.Conta, "0", 5, true);// Nº da Cta Bancária da Cia / NUMERO DA C/C DEBITADA      
                header += " ";  // Em branco DIGITO VERIFICADOR CONTA CORRENTE                                                                                                         
                header += Utils.FormatCode(String.IsNullOrEmpty(cedente.ContaBancaria.DigitoConta) ? " " : cedente.ContaBancaria.DigitoConta, " ", 1, true);// Dígito da Conta Banc.da Cia         DAC DA AGENCIA/CTA DEBITADA                                   
                header += Utils.FitStringLength(cedente.Nome, 30, 30, ' ', 0, true, true, false);// Nome da Companhia / NOME DA EMPRESA                                 
                header += Utils.FormatCode("BCO ITAU", " ", 8); // Nome do Banco da Companhia / NOME DO BANCO                                                                                         
                header += Utils.FormatCode("", " ", 22);//Nome do Banco da Companhia / NOME DO BANCO
                header += Utils.FormatCode("NEXXERA", " ", 10);// Em branco / NOME DO BANCO / NEXXERA                                                               
                header += "1";// UDV - Valor Definido pelo Us. / COMPLEMENTO DE REGISTROS / 1                                                                                                                                              
                header += DateTime.Now.ToString("ddMMyyyyHHmmss");// Data Criação do Arq.Formatado / DATA DA GERACAO DO ARQUIVO // Hora Criação do Arq.Formatado / HORA DE GERACAO DO ARQUIVO                 
                header += Utils.FormatCode(numeroArquivoRemessa.ToString(), "0", 7, true); //  Nº do Arquivo Bancário / NUMERO SEQUENCIAL DO ARQUIVO / 
                header += "020"; // Nº do Arquivo Bancário / NUMERO DA VERSAO DO LAYOUT / 020
                header += "01600"; // UDV - Valor Definido pelo Us. / NUMERO DA VERSAO DO LAYOUT / 01600
                header += Utils.FormatCode("", " ", 19);//Em branco / PARA USO RESERVADO DO BANCO /         
                header += Utils.FormatCode("", " ", 20);//Em branco / PARA USO RESERVADO DA EMPRESA /
                header += Utils.FormatCode("", " ", 10);//Em branco / OBSERVACAO DO LAYOUT DO BANCO /
                header += Utils.FormatCode("", " ", 19);//Em branco / USO EXCLUSIVO NEXXERA /
                header = Utils.SubstituiCaracteresEspeciais(header);


                return header;                                                                                                                                             // Em branco                           USO EXCLUSIVO NEXXERA

            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar HEADER do arquivo de remessa do CNAB240.", ex);
            }
        }

        public static string GerarNomeArquivoRemessa(int banco, string conta, string digito, int loteAtual)
        {
            var nome = "remessa.rem";
            if (banco == 341)
            {
                nome = $"COB_341_{conta}{digito}_{ Utils.FormatCode(loteAtual.ToString(), "0", 8, true)}.rem";
            }
            else if (banco == 33)
            {
                nome = $"COB_353_{conta}{digito}_{ Utils.FormatCode(loteAtual.ToString(), "0", 8, true)}.rem";
            }
            return nome;
        }
        #region REMESSA MONTAGEM ITAU



        /// <summary>
        ///POS INI/FINAL    TAM     Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
        ///------------------------------------------------------------------------------------------------------------------------------------------------
        ///001 - 003        03	    Z0012           Nº do Banco da Companhia            CODIGO DO BANCO NA COMPENSACAO	 
        ///004 - 007        04	    S0001           UDV - Valor Definido pelo           Us.LOTE IDENTIFICACAO DE PAGTOS             0001
        ///008 - 008        01	    S0001           UDV - Valor Definido pelo Us. 	    REGISTRO HEADER DE LOTE	                    1
        ///009 - 009        01	    S0001           UDV - Valor Definido pelo Us. 	    TIPO DE OPERACAO                            R
        ///010 - 011        02	    S0001           UDV - Valor Definido pelo Us. 	    TIPO DE PAGTO                               01
        ///012 - 013        02	    S0002           Em branco                           USO EXCLUSIVO NEXXERA	 
        ///014 - 016        03	    S0001           UDV - Valor Definido pelo Us. 	    N.VERSAO DO LAYOUT DO LOTE                  010
        ///017 - 017        01	    S0002           Em branco USO                       EXCLUSIVO NEXXERA	 
        ///018 - 018        01	    Z0001           Cód. Pessoa Fís./Jurídica           TIPO INSCRICAO EMPRESA DEBITADA
        ///019 - 033        15	    S0001           UDV - Valor Definido pelo Us. 	    CNPJ EMPRESA DEBITADA   29739737000102
        ///034 - 053        20	    S0002           Em branco                           CODIGO CONVERNIO BANCO	 
        ///054 - 058        05	    Z0003           Agência Bancária da Companhia       NUMERO AGENCIA DEBITADA	 
        ///059 - 059        01	    Z0005           Dígito da Agência Banc.da Cia       DIGITO VERIFICADOR DA AGENCIA	 
        ///060 - 071        12	    Z0004           Nº da Cta Bancária da Cia           NUMERO DE C/C DEBITADA	 
        ///072 - 072        01	    S0002           Em branco                           DIGITO VERIFICADOR DA CONTA
        ///073 - 073        01	    Z0006           Dígito da Conta Banc.da Cia         DAC DA AGENCIA/CTA DEBITADA	 
        ///074 - 103        30	    Z0009           Nome da Companhia                   NOME DA EMPRESA
        ///104 - 143        40	    S0002           Em branco                           MENSAGEM 1	 
        ///144 - 183        40	    S0002           Em branco                           MENSAGEM 2	 
        ///184 - 191        08	    Z0058           Nº do Arquivo Bancário              NUMERO DO RETORNO
        ///192 - 199        08	    Z0010           Data Criação do Arq.Formatado       DATA GERACAO DO ARQUIVO
        ///200 - 207        08	    S0003           Zero                                DATA CREDITO 
        ///208 - 214        07	    S0002           Em branco                           CODIGO MODELO PERSONALIZADO	 
        ///215 - 240        26	    S0002           Em branco                           USO EXCLUSIVO NEXXERA
        /// </summary>
        /// <param name="cedente">classe preenchida de cedente.</param>
        /// <param name="nrBanco">Número do Banco</param>
        /// <param name="numeroArquivoRemessa">numero do lote do arquivo remessa</param>
        /// <returns>retorna o header</returns>
        public static string GerarHeaderLoteRemessaCNAB240(Cedente cedente, int nrBanco, int numeroArquivoRemessa)
        {
            try
            {
                string header = Utils.FormatCode(nrBanco.ToString(), "0", 3, true);               //Nº do Banco da Companhia          | CODIGO DO BANCO NA COMPENSACAO	       |  
                header += Utils.FormatCode("0001", "0", 4, true);                                //UDV - Valor Definido pelo         | Us.LOTE IDENTIFICACAO DE PAGTOS         |    0001
                header += Utils.FormatCode("1", " ", 1, true);                                   //UDV - Valor Definido pelo Us. 	 | REGISTRO HEADER DE LOTE	               |    1
                header += Utils.FormatCode("R", " ", 1, true);                                   //UDV - Valor Definido pelo Us. 	 | TIPO DE OPERACAO                        |    R
                header += Utils.FormatCode("01", "0", 2, true);                                  //UDV - Valor Definido pelo Us. 	 | TIPO DE PAGTO                           |    01
                header += Utils.FormatCode("", " ", 2);                                          //Em branco                         | USO EXCLUSIVO NEXXERA	               | 
                header += Utils.FormatCode("010", " ", 3);                                       //UDV - Valor Definido pelo Us. 	 | N.VERSAO DO LAYOUT DO LOTE              |    010
                header += Utils.FormatCode("", " ", 1);                                          //Em branco USO                     | EXCLUSIVO NEXXERA	                   | 
                header += (cedente.CPFCNPJ.Length == 11 ? "1" : "2");                            //Cód. Pessoa Fís./Jurídica         | TIPO INSCRICAO EMPRESA DEBITADA         | 
                header += Utils.FormatCode(cedente.CPFCNPJ, "0", 15, true);                      //UDV - Valor Definido pelo Us. 	 | CNPJ EMPRESA DEBITADA   29739737000102  | 
                header += Utils.FormatCode("", " ", 20);                                         //Em branco                         | CODIGO CONVERNIO BANCO	               | 
                header += Utils.FormatCode(cedente.ContaBancaria.Agencia, "0", 5, true);         //Agência Bancária da Companhia     | NUMERO AGENCIA DEBITADA	               | 
                header += Utils.FormatCode("", " ", 1);                                          //Dígito da Agência Banc.da Cia     | DIGITO VERIFICADOR DA AGENCIA	       | 
                header += Utils.FormatCode(cedente.ContaBancaria.Conta, "0", 12, true);          //Nº da Cta Bancária da Cia         | NUMERO DE C/C DEBITADA	               | 
                header += Utils.FormatCode("", " ", 1);                                          //Em branco                         | DIGITO VERIFICADOR DA CONTA             | 
                header += Utils.FormatCode(String.IsNullOrEmpty(cedente.ContaBancaria.DigitoConta) ? " " : cedente.ContaBancaria.DigitoConta, " ", 1); //Dígito da Conta Banc.da Cia       | DAC DA AGENCIA/CTA DEBITADA	           | 
                header += Utils.FitStringLength(cedente.Nome, 30, 30, ' ', 0, true, true, false);//Nome da Companhia                 | NOME DA EMPRESA                         | 
                header += Utils.FormatCode("", " ", 40);                                         //Em branco   | MENSAGEM 1 | 
                header += Utils.FormatCode("", " ", 40);                                         //Em branco                         | MENSAGEM 2	                           | 
                header += Utils.FormatCode(numeroArquivoRemessa.ToString(), "0", 8, true);       //Nº do Arquivo Bancário            | NUMERO DO RETORNO                       | 
                header += DateTime.Now.ToString("ddMMyyyy");                                     //Data Criação do Arq.Formatado     | DATA GERACAO DO ARQUIVO                 | 
                header += Utils.FormatCode("", "0", 8);                                          //Zero                              | DATA CREDITO                            | 
                header += Utils.FormatCode("", " ", 7);                                          //Em branco                         | CODIGO MODELO PERSONALIZADO	           | 
                header += Utils.FormatCode("", " ", 26);                                         //Em branco                         | USO EXCLUSIVO NEXXERA                   | 
                header = Utils.SubstituiCaracteresEspeciais(header);

                return header;                                                                                                                                             // Em branco                           USO EXCLUSIVO NEXXERA

            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar HEADER do arquivo de remessa do CNAB240.", ex);
            }
        }


        #region DETALHE 
        #region NOVO BOLETO
        public static string GerarDetalheSegmentoPRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoP;
                _segmentoP = "341";                                                                                                 //03    Z0012   Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO
                _segmentoP += "0001";                                                                                               //04    S0001   UDV -Valor Definido pelo Us.        LOTE DE SERVICO                             0001
                _segmentoP += "3";                                                                                                  //01    Z0065   Tipo de Linha do Formatador         REGISTRO DETALHE DE LOTE
                _segmentoP += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true);                     //05    Z0062   Detalhe/ Seqüência no Segmento      N.SEQUENCIAL REGISTRO LOTE
                _segmentoP += "P";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO SEGMENTO REG DETALHE                 P
                _segmentoP += " ";                                                                                                  //01    S0002   Em branco                           USO EXCLUSIVO NEXXERA
                _segmentoP += ObterCodigoDaOcorrencia(boleto);                                                                      //02    Z0071   Código de Envio do Banco            TIPO DE MOVIMENTO
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Agencia, 6, 6, '0', 0, true, true, true);          //06    Z0003   Agência Bancária da Companhia       Agência Bancária da Companhia
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Conta, 12, 12, '0', 0, true, true, true);          //12    Z0004   Nº da Cta Bancária da Cia NUMERO    DA CONTA CORRENTE
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);//01    Z0006   Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA CONTA
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);//01    Z0006   Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA AG/ CONTA
                _segmentoP += Utils.FitStringLength(boleto.NossoNumero, 20, 20, ' ', 1, true, true, false);                     //20    Z0067   Nº da Duplicata Bancária            IDENTIFICACAO DO TITULO NO BANCO
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO CARTEIRA                             1
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        FORMA DE CADASTRO TITULO BANCO              1
                _segmentoP += "2";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        TIPO DE DOCUMENTO                           2
                _segmentoP += Utils.FitStringLength(boleto.NumeroDocumento, 1, 1, ' ', 1, true, true, false);                     //01    Z0086   Geração do Boleto                   IDENT. EMISSAO BOLETO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                                            //01    Z0086   Geração do Boleto                   IDENT. DA DISTRIBUICAO / ENTREGA
                _segmentoP += Utils.FitStringLength(boleto.NumeroDocumento, 15, 15, '0', 0, true, true, true);                      //15    Z0068   Nº do Documento                     N. DO DOCUMENTO DE COBRANCA
                _segmentoP += Utils.FitStringLength(boleto.DataVencimento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);   //08    Z0049   Data de Vencimento                  DATA DE VENCIMENTO DO TITULO
                _segmentoP += Utils.FitStringLength(boleto.ValorBoleto.ApenasNumeros(), 15, 15, '0', 0, true, true, true);          //15    Z5835   Payment Amount - Withholding        VALOR DO PAGAMENTO
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.Agencia) ? " " : boleto.Cedente.ContaBancaria.Agencia, "0", 5, true); //05    Z0003   Agência Bancária da Companhia       AGENCIA ENCARREGADA DE COBRANCA
                _segmentoP += " ";                                                                                                  //01    Z0005   Dígito da Agência Banc.da Cia       DIGITO VERIFICADOR DA AGENCIA
                _segmentoP += "02";                                                                                                 //02    S0001   UDV -Valor Definido pelo Us.        ESPECIE DE TITULO                           02
                _segmentoP += "N";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        IDENT TITULOS ACEITO / NAO ACEITO           N
                _segmentoP += Utils.FitStringLength(boleto.DataDocumento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);    //08    Z0069   Data da Fatura                      DATA DA EMISSAO DO TITULO
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO DO JUROS DE MORA                     1
                _segmentoP += Utils.FitStringLength(boleto.DataVencimento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);   //08    Z0049   Data de Vencimento                  DATA DE JUROS DE MORA
                _segmentoP += Utils.FitStringLength(boleto.JurosMora.ApenasNumeros(), 15, 15, '0', 0, true, true, true);            //15    Z0075   Juros Diários                       JUROS DE MORA POR DIA/ TAXA
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO DE DESCONTO 1                        1
                _segmentoP += Utils.FitStringLength(boleto.DataDesconto.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);     //08    Z0076   Data de Vcto c / Desconto -         C / R   DATA DO DESCONTO                    1
                _segmentoP += Utils.FitStringLength(boleto.ValorDesconto.ApenasNumeros(), 15, 15, '0', 0, true, true, true);        //15    Z0051   Desconto Obtido                     VALOR/ PERCENTUAL A SER CONCEDIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                                           //15    S0003   Zero                                VALOR DO IOF A SER RECOLHIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                                           //15    S0003   Zero                                VALOR DO ABATIMENTO
                //***********************INFORMACOES QUE DEVERAO FORMAR UMA CHAVE NO ARQUIVO REMESSA********************** //25    Z0070   Código de Usuário do Cliente        IDENTIFICACAO DO TITULO NA EMPRESSA
                //Companhia -- Ficou de verificar
                _segmentoP += Utils.FitStringLength(boleto.CompanhiaFiscal, 5, 5, '0', 0, true, true, true) +
                //Tipo DOcumento
                Utils.FitStringLength(boleto.TipoDocumento.ToString(), 2, 2, '0', 0, true, true, true) +
                //NumeroLinha
                //Utils.FitStringLength(boleto.NumeroParcela.ToString(), 2, 2, '0', 0, true, true, true) +
                //FluxoPagamento
                Utils.FitStringLength(boleto.NumeroDocumento, 10, 10, '0', 0, true, true, true) +
                //CodCliente
                Utils.FitStringLength(boleto.CodCliente, 8, 8, '0', 0, true, true, true);
                //********************************************************************************************************
                _segmentoP += Utils.FitStringLength("3", 1, 1, '0', 0, true, true, true);                                           //01    S0001   UDV -Valor Definido pelo            Us.CODIGO DE PROTESTO                       3
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                                            //02    Z0074   Dias para Protesto                  NUMERO DE DIAS PARA PROTESTO
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO PARA BAIXA / DEVOLUCAO               1
                _segmentoP += "001";                                                                                                //03    S0001   UDV -Valor Definido pelo Us.        NUMERO DE DIAS PARA BAIXA / DEVOLUCAO       001
                _segmentoP += "09";                                                                                                 //02    S0001   UDV -Valor Definido pelo Us.        CODIGO DA MOEDA                             09
                _segmentoP += Utils.FitStringLength("", 10, 10, '0', 0, true, true, true);                                          //10    S0003   Zero                                NUMERO DO CONTRATO DA OPER.DE CRED.	 
                _segmentoP += " ";
                _segmentoP = Utils.SubstituiCaracteresEspeciais(_segmentoP);

                return _segmentoP;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }
        public static string GerarDetalheSegmentoQRemessa240(BoletoNet.Boleto boleto, int numeroRegistro)
        {
            try
            {
                string _zeros16 = new string('0', 16);
                string _brancos10 = new string(' ', 10);
                string _brancos28 = new string(' ', 28);
                string _brancos40 = new string(' ', 40);

                string _segmentoQ;

                _segmentoQ = "341";
                _segmentoQ += "0001";
                _segmentoQ += "3";
                _segmentoQ += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true);
                _segmentoQ += "Q";
                _segmentoQ += " ";

                _segmentoQ += ObterCodigoDaOcorrencia(boleto);
                if (boleto.Sacado.CPFCNPJ.Length <= 11)
                    _segmentoQ += "1";
                else
                    _segmentoQ += "2";

                _segmentoQ += Utils.FitStringLength(boleto.Sacado.CPFCNPJ, 15, 15, '0', 0, true, true, true);
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Nome.TrimStart(' '), 30, 30, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += "          ";
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.End.TrimStart(' '), 40, 40, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.Bairro.TrimStart(' '), 15, 15, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.CEP, 8, 8, ' ', 0, true, true, false).ToUpper(); ;
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.Cidade.TrimStart(' '), 15, 15, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.UF, 2, 2, ' ', 0, true, true, false).ToUpper();
                if (boleto.Sacado.CPFCNPJ.Length <= 11)
                    _segmentoQ += "1";
                else
                    _segmentoQ += "2";

                _segmentoQ += Utils.FitStringLength(boleto.Sacado.CPFCNPJ, 15, 15, '0', 0, true, true, true);
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Nome.TrimStart(' '), 30, 30, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += _brancos10;
                _segmentoQ += "000";
                //***********************INFORMACOES QUE DEVERAO FORMAR UMA CHAVE NO ARQUIVO REMESSA********************** //25    Z0070   Código de Usuário do Cliente        IDENTIFICACAO DO TITULO NA EMPRESSA
                //Companhia
                _segmentoQ += Utils.FitStringLength(boleto.CompanhiaFiscal, 5, 5, '0', 0, true, true, true) +
                //Tipo DOcumento
                Utils.FitStringLength(boleto.TipoDocumento.ToString(), 2, 2, '0', 0, true, true, true) +
                //NumeroLinha
                //Utils.FitStringLength(boleto.NumeroParcela.ToString(), 2, 2, '0', 0, true, true, true) +
                //FluxoPagamento
                Utils.FitStringLength(boleto.NumeroDocumento, 10, 10, '0', 0, true, true, true) +
                //CodCliente
                Utils.FitStringLength(boleto.CodCliente, 3, 3, '0', 0, true, true, true);
                //********************************************************************************************************
                _segmentoQ += Utils.FitStringLength("", 5, 5, ' ', 0, true, true, false);
                _segmentoQ += "109";

                _segmentoQ = Utils.SubstituiCaracteresEspeciais(_segmentoQ);

                return _segmentoQ;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO Q DO DETALHE do arquivo de REMESSA.", ex);
            }
        }
        #endregion

        #region ALTERACAO
        public static string GerarDetalheAlterarcaoDadosSegmentoPRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoP;
                _segmentoP = "341";                                                                                   //3   Z0012 Nº do Banco da Companhia        CODIGO BANCO NA COMPENSACAO                             
                _segmentoP += "0001";                                                                                 //4   S0001 UDV -Valor Definido pelo Us.    LOTE DE SERVICO
                _segmentoP += "3";                                                                                    //1   S0001 UDV -Valor Definido pelo Us.    REGISTRO DETALHE DE LOTE
                _segmentoP += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true);       //5   Z0062 Detalhe/ Seqüência no Segmento  N.SEQUENCIAL REGISTRO LOTE
                _segmentoP += "P";                                                                                    //1   S0001 UDV -Valor Definido pelo Us.    CODIGO SEGMENTO REG DETALHE
                _segmentoP += " ";                                                                                    //1   S0002 Em branco                       USO EXCLUSIVO NEXXERA
                _segmentoP += "31"; //ALTERAÇÃO DE OUTROS DADOS                                                       //2   S0001 UDV -Valor Definido pelo Us.    TIPO DE MOVIMENTO
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Agencia, 5, 5, '0', 0, true, true, true); //5   Z0003 Agência Bancária da Companhia   AGÊNCIA BANCÁRIA DA COMPANHIA
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true); //1   Z0006 Dígito da Conta Banc.da Cia     DIGITO VERIFICADOR DA AGÊNCIA
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Conta, 12, 12, '0', 0, true, true, true);//12  Z0004 Nº da Cta Bancária da Cia       NUMERO DA CONTA CORRENTE
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);//1   Z0006 Dígito da Conta Banc.da Cia     DIGITO VERIFICADOR DA CONTA
                _segmentoP += "1";                                                                                    //1   Z0006 Dígito da Conta Banc.da Cia     DIGITO VERIFICADOR DA AG/ CONTA
                _segmentoP += Utils.FitStringLength(boleto.NossoNumero, 20, 20, ' ', 1, true, true, false);           //20  Z0067 Nº da Duplicata Bancária        IDENTIFICACAO DO TITULO NO BANCO
                _segmentoP += "1";                                                                                    //1   S0001 UDV -Valor Definido pelo Us.    CODIGO CARTEIRA
                _segmentoP += "0";                                                                                    //1   S0003 Zero                            FORMA DE CADASTRO TITULO BANCO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       TIPO DE DOCUMENTO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       IDENT. EMISSAO BOLETO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       IDENT. DA DISTRIBUICAO/ ENTREGA
                _segmentoP += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                           //15  S0002 Em branco                       N. DO DOCUMENTO DE COBRANCA
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DE VENCIMENTO DO TITULO
                _segmentoP += Utils.FitStringLength(boleto.ValorBoleto.ApenasNumeros(), 15, 15, '0', 0, true, true, true); //15  Z60C2 Payment Amount - Withholding    VALOR DO PAGAMENTO
                _segmentoP += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                              //5   S0003 Zero                            AGENCIA ENCARREGADA DE COBRANCA
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco DIGITO VERIFICADOR DA AGENCIA
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                              //2   S0003 Zero                            ESPECIE DE TITULO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco IDENT TITULOS ACEITO/ NAO ACEITO
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DA EMISSAO DO TITULO
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO DO JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DE JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            JUROS DE MORA POR DIA / TAXA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO DE DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DO DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            VALOR / PERCENTUAL A SER CONCEDIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            VALOR DO IOF A SER RECOLHIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            VALOR DO ABATIMENTO
                _segmentoP += Utils.FitStringLength("", 25, 25, ' ', 0, true, true, false);                           //25  S0002 Em branco                       IDENTIFICACAO DO TITULO NA EMPRESSA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO DE PROTESTO
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                              //2   S0003 Zero                            NUMERO DE DIAS PARA PROTESTO
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO PARA BAIXA / DEVOLUCAO
                _segmentoP += Utils.FitStringLength("", 3, 3, ' ', 0, true, true, false);                             //3   S0002 Em branco                       NUMERO DE DIAS PARA BAIXA/ DEVOLUCAO
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                              //2   S0003 Zero                            CODIGO DA MOEDA
                _segmentoP += Utils.FitStringLength("", 10, 10, '0', 0, true, true, true);                            //10  S0003 Zero                            NUMERO DO CONTRATO DA OPER.DE CRED.
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       USO EXCLUSIVO NEXXERA                                                    

                _segmentoP += " ";
                _segmentoP = Utils.SubstituiCaracteresEspeciais(_segmentoP);

                return _segmentoP;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        public static string GerarDetalheAlterarcaoDadosSegmentoQRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoQ;
                _segmentoQ = "341";                                                                             //3	    Z0012 Nº do Banco da Companhia          CODIGO BANCO NA COMPENSACAO
                _segmentoQ += Utils.FitStringLength("1", 4, 4, '0', 0, true, true, true);                       //4	    S0001 UDV - Valor Definido pelo Us.     LOTE DE SERVICO
                _segmentoQ += Utils.FitStringLength("3", 1, 1, '0', 0, true, true, true);                       //1	    S0001 UDV - Valor Definido pelo Us. 	REGISTRO DETALHE DE LOTE
                _segmentoQ += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true); //5	    Z0062 Detalhe/Seqüência no Segmento     N.SEQUENCIAL REGISTRO LOTE
                _segmentoQ += "Q";                                                                              //1	    S0001 UDV - Valor Definido pelo Us. 	CODIGO SEGMENTO REG DETALHE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1	    S0002 Em branco                         USO EXCLUSIVO NEXXERA
                _segmentoQ += "31"; //ALTERAÇÃO DE OUTROS DADOS                                                 //2	    S0001 UDV - Valor Definido pelo Us. 	TIPO DE MOVIMENTO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1	    S0003 Zero                              TIPO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15	S0003 Zero                              NUMERO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40	S0002 Em branco                         NOME DO SACADOR
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40	S0002 Em branco                         ENDERECO - SACADO
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15	S0002 Em branco                         BAIRRO
                _segmentoQ += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                        //5	    S0003 Zero                              CEP Benefic./Pagador - 5 díg.
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3	    S0003 Zero                              CEP Benefic./Pagador - 3 díg.
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15	S0002 Em branco                         CIDADE DO BENEFIC./PAGADOR
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2	    S0002 Em branco                         UNIDADE DE FEDERACAO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1	    S0003 Zero                              CÓD.PESSOA FÍS./JUR.BEN./PAG
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15	S0003 Zero                              CPF/CNPJ DO BENEFIC./PAGADOR
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40	S0002 Em branco                         NOME DO SACADOR / AVALISTA
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3	    S0003 Zero                              Nº BANCO DO BENEFIC./PAGADOR
                _segmentoQ += Utils.FitStringLength("", 20, 20, ' ', 0, true, true, false);                     //20	S0002 Em branco                         NOSSO N BANCO CORRESPONDENTE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1	    S0002 Em branco                         FORMA DE ACESSO AO BOLETO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2	    S0002 Em branco                         PRIMEIRA INSTRUCAO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2	    S0002 Em branco                         SEGUNDA INSTRUCAO
                _segmentoQ += Utils.FitStringLength(boleto.Carteira, 3, 3, '0', 0, true, true, true);           //3	    S0001 UDV - Valor Definido pelo Us. 	VARIACAO DA CARTEIRA / CARTEIRA                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                

                _segmentoQ += " ";
                _segmentoQ = Utils.SubstituiCaracteresEspeciais(_segmentoQ);

                return _segmentoQ;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        public static string GerarDetalheAlterarcaoVencimentoSegmentoPRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoP;
                _segmentoP = "341";                                                                             //3   Z0012 Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO                                                                                        
                _segmentoP += "0001";                                                                           //4   S0001 UDV -Valor Definido pelo Us.        LOTE DE SERVICO
                _segmentoP += "3";                                                                              //1   S0001 UDV -Valor Definido pelo Us.        REGISTRO DETALHE DE LOTE
                _segmentoP += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true); //5   Z0062 Detalhe/ Seqüência no Segmento      N.SEQUENCIAL REGISTRO LOTE
                _segmentoP += "P";                                                                              //1   S0001 UDV -Valor Definido pelo Us.        CODIGO SEGMENTO REG DETALHE
                _segmentoP += " ";                                                                              //1   S0002 Em branco                           USO EXCLUSIVO NEXXERA
                _segmentoP += "06"; //ALTERACAO DE VENCIMENTO                                                   //2   S0001 UDV -Valor Definido pelo Us.        TIPO DE MOVIMENTO                   
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Agencia, 5, 5, '0', 0, true, true, true); //5   Z0003 Agência Bancária da Companhia       AGÊNCIA BANCÁRIA DA COMPANHIA        
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);  //1   Z0006 Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA AGÊNCIA
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Conta, 12, 12, '0', 0, true, true, true);  //12  Z0004 Nº da Cta Bancária da Cia           NUMERO DA CONTA CORRENTE
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);  //1   Z0006 Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA CONTA
                _segmentoP += "5";                                                                              //1   Z0006 Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA AG/ CONTA                        
                _segmentoP += Utils.FitStringLength(boleto.NossoNumero, 20, 20, ' ', 1, true, true, false);     //20  Z0067 Nº da Duplicata Bancária            IDENTIFICACAO DO TITULO NO BANCO                        
                _segmentoP += "1";                                                                              //1   S0001 UDV -Valor Definido pelo Us.        CODIGO CARTEIRA                        
                _segmentoP += "0";                                                                              //1   S0003 Zero                                FORMA DE CADASTRO TITULO BANCO                        
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                        //1   S0002 Em branco                           TIPO DE DOCUMENTO              
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                        //1   S0002 Em branco                           IDENT. EMISSAO BOLETO                    
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                        //1   S0002 Em branco                           IDENT. DA DISTRIBUICAO/ ENTREGA                    
                _segmentoP += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15  S0002 Em branco                           N. DO DOCUMENTO DE COBRANCA
                _segmentoP += Utils.FitStringLength(boleto.DataVencimento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);//8   Z60C1 Data de Vencimento DATA DE VENCIMENTO DO TITULO                
                _segmentoP += Utils.FitStringLength(boleto.ValorBoleto.ApenasNumeros(), 15, 15, '0', 0, true, true, true);//15  Z60C2 Payment Amount - Withholding  VALOR DO PAGAMENTO
                _segmentoP += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                        //5   S0003 Zero                                AGENCIA ENCARREGADA DE COBRANCA
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1   S0002 Em branco                           DIGITO VERIFICADOR DA AGENCIA
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                        //2   S0003 Zero                                ESPECIE DE TITULO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1   S0002 Em branco                           IDENT TITULOS ACEITO/ NAO ACEITO
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                       //8   S0003 Zero                                DATA DA EMISSAO DO TITULO   
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                       //1   S0003 Zero                                CODIGO DO JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                        //8   S0003 Zero                                DATA DE JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                JUROS DE MORA POR DIA / TAXA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1   S0003 Zero                                CODIGO DE DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                        //8   S0003 Zero                                DATA DO DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                VALOR / PERCENTUAL A SER CONCEDIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                VALOR DO IOF A SER RECOLHIDO  
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                VALOR DO ABATIMENTO
                _segmentoP += Utils.FitStringLength("", 25, 25, ' ', 0, true, true, false);                     //25  Z0070   Código de Usuário do Cliente      IDENTIFICACAO DO TITULO NA EMPRESSA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1   S0003 Zero            CODIGO DE PROTESTO                               
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                        //2   S0003 Zero                                NUMERO DE DIAS PARA PROTESTO                                   
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1   S0003 Zero                                CODIGO PARA BAIXA / DEVOLUCAO                                
                _segmentoP += Utils.FitStringLength("", 3, 3, ' ', 0, true, true, false);                       //3   S0002 Em branco                           NUMERO DE DIAS PARA BAIXA/ DEVOLUCAO                                
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                        //2   S0003 Zero                                CODIGO DA MOEDA                                
                _segmentoP += Utils.FitStringLength("", 10, 10, '0', 0, true, true, true);                      //10  S0003 Zero                                NUMERO DO CONTRATO DA OPER.DE CRED.
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                        //1   S0002 Em branco                           USO EXCLUSIVO NEXXERA

                _segmentoP += " ";
                _segmentoP = Utils.SubstituiCaracteresEspeciais(_segmentoP);

                return _segmentoP;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        public static string GerarDetalheAlterarcaoVencimentoSegmentoQRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoQ;
                _segmentoQ = "341";                                                                             //3     Z0012 Nº do Banco da Companhia          CODIGO BANCO NA COMPENSACAO
                _segmentoQ += Utils.FitStringLength("1", 4, 4, '0', 0, true, true, true);                       //4     S0001 UDV -Valor Definido pelo Us.      LOTE DE SERVICO
                _segmentoQ += Utils.FitStringLength("3", 1, 1, '0', 0, true, true, true);                       //1     S0001 UDV -Valor Definido pelo Us.      REGISTRO DETALHE DE LOTE
                _segmentoQ += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true); //5     Z0062 Detalhe/ Seqüência no Segmento    N.SEQUENCIAL REGISTRO LOTE
                _segmentoQ += "Q";                                                                              //1     S0001 UDV -Valor Definido pelo Us.      CODIGO SEGMENTO REG DETALHE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1     S0002 Em branco                         USO EXCLUSIVO NEXXERA
                _segmentoQ += "06"; //ALTERAÇÃO DO VENCIMENTO                                                   //2     S0001 UDV -Valor Definido pelo Us.      TIPO DE MOVIMENTO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1     S0003 Zero                              TIPO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15    S0003 Zero                              NUMERO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40    S0002 Em branco                         NOME DO SACADOR
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40    S0002 Em branco                         ENDERECO -SACADO
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15    S0002 Em branco                         BAIRRO
                _segmentoQ += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                        //5     S0003 Zero                              CEP Benefic./ Pagador - 5 díg.
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3     S0003 Zero                              CEP Benefic./ Pagador - 3 díg.
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15    S0002 Em branco                         CIDADE DO BENEFIC./ PAGADOR
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2     S0002 Em branco                         UNIDADE DE FEDERACAO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1     S0003 Zero                              Cód.Pessoa Fís./ Jur.Ben./ Pag
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15    S0003 Zero                              CPF / CNPJ do Benefic./ Pagador
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40    S0002 Em branco                         NOME DO SACADOR / AVALISTA
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3    S0003 Zero                              Nº Banco do Benefic./ Pagador
                _segmentoQ += Utils.FitStringLength("", 20, 20, ' ', 0, true, true, false);                     //20    S0002 Em branco                         NOSSO N BANCO CORRESPONDENTE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1     S0002 Em branco                         FORMA DE ACESSO AO BOLETO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2     S0002 Em branco                         PRIMEIRA INSTRUCAO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2     S0002 Em branco                         SEGUNDA INSTRUCAO
                _segmentoQ += Utils.FitStringLength(boleto.Carteira, 3, 3, '0', 0, true, true, true);           //3     S0001 UDV -Valor Definido pelo Us.      VARIACAO DA CARTEIRA / CARTEIRA

                _segmentoQ += " ";
                _segmentoQ = Utils.SubstituiCaracteresEspeciais(_segmentoQ);

                return _segmentoQ;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }
        #endregion
        #endregion

        #region TRAILER
        /// <summary>
        /// Gerar trailer do lote
        /// </summary>
        /// <param name="numeroRegistro">Quantidade dos Títulos.</param>
        /// <param name="vlrTituloTotal">Valor Total dos Títulos</param>
        /// <param name="qtdTituloTotal">Quantidade Total dos Títulos</param>
        /// <returns>Retorna o linha de rodapé</returns>
        public static string GerarTrailerLoteRemessa240(int nrBanco, int numeroRegistro, decimal vlrTituloTotal, int qtdTituloTotal)
        {
            try
            {
                #region Comentário
                //POS INI/FINAL    TAM     Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
                //------------------------------------------------------------------------------------------------------------------------------------------------
                //001 - 003	    003      Z0012            Nº do Banco da Companhia           CODIGO DO BANCO NA COMPENSACAO	 
                //004 - 004	    004      S0001            UDV - Valor Definido pelo Us.      LOTE DE SERVICO	                            0001
                //008 - 001	    001      S0001            UDV - Valor Definido pelo Us.      REGISTRO TRAILER DE LOTE	                5
                //009 - 009	    009      S0002            Em branco                          USO EXCLUSIVO NEXXERA	 
                //018 - 006	    006      Z0063            Seq. de Linha em Segmento          EP QTDE REGISTROS DO LOTE
                //024 - 006	    006      Z0022            Cont. de Linha Arq.Formatado       QTDE TITULOS EM COBRANCA
                //030 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //047 - 006	    006      S0003            Zero                               QTDE TITULOS EM COBRANCA	 
                //053 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //070 - 006	    006      S0003            Zero                               QTDE TITULOS EM COBRANCA	 
                //076 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //093 - 006	    006      S0003            Zero                               QTDE TITULOS EM COBRANCA	 
                //099 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //116 - 008	    008      S0002            Em branco                          NUMERO DO AVISO DE LANCAMENTO	 
                //124 - 117	    117      S0002            Em branco                          USO EXCLUSIVO NEXXERA
                #endregion
                string _trailer = Utils.FormatCode(nrBanco.ToString(), "0", 3, true);         //003   Nº do Banco da Companhia           CODIGO DO BANCO NA COMPENSACAO	      
                _trailer += Utils.FormatCode("0001", "0", 4, true);                           //004   UDV - Valor Definido pelo Us.      LOTE DE SERVICO	                            0001     
                _trailer += Utils.FormatCode("5", "0", 1); ;                                  //001   UDV - Valor Definido pelo Us.      REGISTRO TRAILER DE LOTE	                5     
                _trailer += Utils.FormatCode("", " ", 9);                                     //009   Em branco                          USO EXCLUSIVO NEXXERA	      
                _trailer += Utils.FormatCode(numeroRegistro.ToString(), "0", 6, true);        //006   Seq. de Linha em Segmento          EP QTDE REGISTROS DO LOTE     
                // Totalização da Cobrança Simples                                                        
                _trailer += Utils.FormatCode(qtdTituloTotal.ToString(), "0", 6, true);        //006   Cont. de Linha Arq.Formatado       QTDE TITULOS EM COBRANCA     
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //Totalização cobrança vinculada                                                  
                _trailer += Utils.FormatCode("", "0", 6);                                     //006   Zero                               QTDE TITULOS EM COBRANCA	              
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 

                _trailer += Utils.FormatCode("", "0", 6);                                     //006   Zero                               QTDE TITULOS EM COBRANCA	      
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	                                                                                                                                                                                                                                                                                                                                                                    
                _trailer += Utils.FormatCode("", "0", 6);                                     //006   Zero                               QTDE TITULOS EM COBRANCA	                         
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	  
                _trailer += Utils.FormatCode("", " ", 8);                                     //008   Em branco                          NUMERO DO AVISO DE LANCAMENTO	         
                _trailer += Utils.FormatCode("", " ", 117);                                   //117   Em branco                          USO EXCLUSIVO NEXXERA    
                return _trailer;
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao gerar Trailer de Lote do arquivo de remessa.", e);
            }

        }

        /// <summary>
        /// Gerar trailer do arquivo remessa
        /// </summary>
        /// <param name="nrBanco">Quantidade dos Títulos.</param>
        /// <param name="numeroRegistro">Valor Total dos Títulos</param>
        /// <returns>Retorna o linha de trailer</returns>
        public static string GerarTrailerRemessa240(int nrBanco, int numeroRegistro)
        {
            try
            {
                #region Comentário
                //POS INI/FINAL    TAM     Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
                //------------------------------------------------------------------------------------------------------------------------------------------------
                //001 - 003	    003	    Z0012           Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO	 
                //004 - 007	    004	    S0001           UDV - Valor Definido pelo Us.       LOTE DE SERVICO	                            9999
                //008 - 008	    001	    S0001           UDV - Valor Definido pelo Us. 	    REGISTRO TRAILER DE ARQUIVO	                9
                //009 - 017	    009	    S0002           Em branco                           USO EXCLUSIVO NEXXERA	 
                //018 - 023	    006	    S0001           UDV - Valor Definido pelo Us. 	    QTDE LOTES DO ARQUIVO	                    000001
                //024 - 029	    006	    Z0022           Cont. de Linha Arq.Formatado        QTDE REGISTROS DO ARQUIVO
                //030 - 035	    006	    S0001           UDV - Valor Definido pelo Us. 	    QTDE DE CONTAS P/CONC.(LOTES)               000000
                //036 - 240	    205	    S0002           Em branco                           USO EXCLUSIVO NEXXERA
                #endregion
                string _trailer = Utils.FormatCode(nrBanco.ToString(), "0", 3, true);                           //003   Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO
                _trailer += Utils.FormatCode("", "9", 4, true);                                                 //004	UDV - Valor Definido pelo Us.       LOTE DE SERVICO	                            9999
                _trailer += Utils.FormatCode("", "9", 1);                                                       //001	UDV - Valor Definido pelo Us. 	    REGISTRO TRAILER DE ARQUIVO	                9
                _trailer += Utils.FormatCode("", " ", 9);                                                       //009	Em branco                           USO EXCLUSIVO NEXXERA	 
                _trailer += Utils.FormatCode("000001", "0", 6, true);                                           //006	UDV - Valor Definido pelo Us. 	    QTDE LOTES DO ARQUIVO	                    000001
                _trailer += Utils.FitStringLength(numeroRegistro.ToString(), 6, 6, '0', 0, true, true, true);   //006	Cont. de Linha Arq.Formatado        QTDE REGISTROS DO ARQUIVO
                _trailer += Utils.FormatCode("", "0", 6);                                                       //006	UDV - Valor Definido pelo Us. 	    QTDE DE CONTAS P/CONC.(LOTES)               000000                        
                _trailer += Utils.FormatCode("", " ", 205);                                                     //205	Em branco                           USO EXCLUSIVO NEXXERA

                _trailer = Utils.SubstituiCaracteresEspeciais(_trailer);

                return _trailer;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do registro TRAILER do arquivo de REMESSA.", ex);
            }
        }
        #endregion

        #endregion


        public static BoletoBancario GerarBoletoPorLinhaDigitavel(
            string linhaDigitavel,
            DateTime datavencimento,
            decimal valorAberto,
            string cedenteCodigo,
            Cedente cedente,
            string numeroDocumento,
            int nossoNumero,
            string companhia,
            Sacado sacado,
            short numeroBanco,
            decimal valorDocumentoAberto,
            decimal valorJuros,
            decimal valorMulta,
            decimal imposto,
            decimal valorDesconto,
            decimal valorDocumento,
            short tipoSolicitacao,
            string mensagemImpressa,
            decimal parametroJurosAoMesPorcentagem,
            decimal parametroMultaAtraso,
            List<string> instrucoes

            )
        {
            //ROTINA PARA OBTER OS DADOS VIA LINHA DIGITAVEL
            BoletoInfo boletoInfo = LerLinhaDigitavelCNAB400(linhaDigitavel, numeroDocumento);
            return MontarBoleto(
                BaseRotinas.ConverterParaDatetime(boletoInfo.DataVencimento),
                boletoInfo.ValorBoleto,
                boletoInfo.Carteira,
                cedenteCodigo,
                cedente,
                boletoInfo.NumeroDocumento,
                boletoInfo.NossoNumero,
                companhia,
                sacado,
                boletoInfo.NumeroBanco,
                valorDocumentoAberto,
                valorJuros,
                valorMulta,
                imposto,
                valorDesconto,
                valorDocumento,
                tipoSolicitacao,
                mensagemImpressa,
                parametroJurosAoMesPorcentagem,
                parametroMultaAtraso,
                instrucoes);

        }

        public static BoletoBancario MontarBoleto(
            DateTime datavencimento,
            decimal valorAberto,
            string carteira,
            string cedenteCodigo,
            Cedente cedente,
            string numeroDocumento,
            string nossoNumero,
            string companhia,
            Sacado sacado,
            short numeroBanco,
            decimal valorDocumentoAberto,
            decimal valorJuros,
            decimal valorMulta,
            decimal imposto,
            decimal valorDesconto,
            decimal valorDocumento,
            short tipoSolicitacao,
            string mensagemImpressa,
            decimal parametroJurosAoMesPorcentagem,
            decimal parametroMultaAtraso,
            List<string> instrucoes
            )
        {
            try
            {
                BoletoNet.Boleto boleto = new BoletoNet.Boleto(datavencimento, valorAberto, carteira, cedenteCodigo, cedente);
                boleto.NumeroDocumento = numeroDocumento;
                boleto.NossoNumero = nossoNumero;
                boleto.Companhia = companhia;
                boleto.Sacado = sacado;

                boleto.EspecieDocumento = DefinirEspecie((EnumBoleto.BancoCodigo)numeroBanco);
                boleto.Banco = new Banco(numeroBanco);
                BoletoBancario boleto_bancario = new BoletoBancario();
                boleto_bancario.CodigoBanco = numeroBanco;
                boleto_bancario.Boleto = boleto;
                boleto_bancario.Boleto.Valida();
                //boleto_bancario.Boleto.DataDocumento = (DateTime)boleto.DataDocumento;
                //boleto_bancario.Boleto.DataProcessamento = (DateTime)parcela.DataDocumento;
                boleto_bancario.Boleto.NumeroDocumento = numeroDocumento;
                boleto_bancario.Boleto.LocalPagamento = "Pagar em qualquer banco até o vencimento";

                if (tipoSolicitacao == 2) // ALTERAR
                {
                    boleto_bancario.Boleto.ValorBoleto = BaseRotinas.ConverterParaDecimal(valorDocumentoAberto) + BaseRotinas.ConverterParaDecimal(valorJuros) + BaseRotinas.ConverterParaDecimal(valorMulta);
                    boleto_bancario.Boleto.ValorDesconto = BaseRotinas.ConverterParaDecimal(imposto) + BaseRotinas.ConverterParaDecimal(valorDesconto);
                    boleto_bancario.Boleto.ValorCobrado = BaseRotinas.ConverterParaDecimal(valorDocumento);
                }
                else
                {
                    boleto_bancario.Boleto.ValorBoleto = BaseRotinas.ConverterParaDecimal(valorDocumento);
                    boleto_bancario.Boleto.ValorDesconto = BaseRotinas.ConverterParaDecimal(valorDesconto);
                    boleto_bancario.Boleto.ValorCobrado = BaseRotinas.ConverterParaDecimal(valorDocumento);
                }


                boleto = MontarInstruicao(
                     boleto,
                     boleto_bancario.Boleto.ValorCobrado,
                     BaseRotinas.ConverterParaDatetime(boleto_bancario.Boleto.DataVencimento),
                     parametroJurosAoMesPorcentagem,
                     parametroMultaAtraso,
                     instrucoes,
                     numeroBanco

                );

                //MENSAGEM DIGITADA PELO ATENDENTE.
                if (!String.IsNullOrEmpty(mensagemImpressa))
                {
                    boleto.Instrucoes.Add(new Instrucao(numeroBanco) { Descricao = "" });
                    boleto.Instrucoes.Add(new Instrucao(numeroBanco) { Descricao = mensagemImpressa });
                }
                cedente.MostrarCNPJnoBoleto = true;
                boleto_bancario.MostrarEnderecoCedente = true;
                boleto_bancario.MostrarCodigoCarteira = true;
                boleto_bancario.OcultarReciboSacado = true;
                boleto_bancario.OcultarReciboSacadoOTIS = false;

                //boleto_bancario.MostrarComprovanteEntrega = false;

                return boleto_bancario;
            }
            catch (Exception ex)
            {
                throw new Exception($"erro ao montar o boleto", ex);
            }
        }
        private static IEspecieDocumento DefinirEspecie(EnumBoleto.BancoCodigo banco)
        {
            switch (banco)
            {
                case EnumBoleto.BancoCodigo.Itau:
                    return new EspecieDocumento_Itau("99");
                case EnumBoleto.BancoCodigo.Santander:
                    return new EspecieDocumento_Santander("2");
                default:
                    {
                        return new EspecieDocumento_Itau("99");
                    }
            }

        }
        public static BoletoNet.Boleto MontarInstruicao(BoletoNet.Boleto boleto, decimal ValorDocumento, DateTime DataVencimento, decimal jurosAoMesPorcentagem, decimal multaAtraso, List<string> Instrucoes, decimal nrBanco)
        {
            try
            {
                // SE ESTIVER NA INSTRUCAO DO BANCO PORTADOR DEVE CALCULAR O JUROS E A MULTA APENAS PARA INFORMAR NO CAMPO Informacao de responsabilidade do beneficiador
                var _ValorJuros = ((((jurosAoMesPorcentagem / 30) * 1) / 100) * ValorDocumento);
                var _ValorMulta = (ValorDocumento * (multaAtraso / 100));
                if (Instrucoes != null)
                {
                    foreach (var instrucao in Instrucoes)
                    {

                        boleto.Instrucoes.Add(
                            new Instrucao((int)nrBanco)
                            {
                                Descricao = instrucao
                                                    .Replace("@DATA_VENCIMENTO", $"{DataVencimento:dd/MM/yyyy}")
                                                    .Replace("@VR_JUROS_POR_DIA", $"{BaseRotinas.ConverterParaDecimal(_ValorJuros):C2}")
                                                    .Replace("@VR_MULTA_ATRASO", $"{BaseRotinas.ConverterParaDecimal(_ValorMulta):C2}")
                                                    .Replace("@%_JUROS_POR_DIA", $"{BaseRotinas.ConverterParaDecimal(jurosAoMesPorcentagem):N2}%")
                                                    .Replace("@%_MULTA_ATRASO", $"{BaseRotinas.ConverterParaDecimal(multaAtraso):N2}%")
                            }
                            );
                    }
                }
                return boleto;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao ler detalhe do arquivo de .", ex);
            }
        }

        public static BoletoInfo LerLinhaDigitavelCNAB400(string linha, string numeroDocumento)
        {
            #region Disposição Boleto
            //            \*
            //            *------------------------------------------------------
            //              *Campo 1 - AAABC.CCDDX
            //              * AAA - Código do Banco
            //              * B   -Moeda
            //              * CCC - Carteira
            //              * DD - 2 primeiros números Nosso Número
            //              *X - DAC Campo 1(AAABC.CCDD) Mod10
            //              *
            //              * Campo 2 - DDDDD.DEEEEY
            //              * DDDDD.D - Restante Nosso Número
            //              *EEEE - 4 primeiros numeros do número do documento
            //              * Y       -DAC Campo 2(DDDDD.DEEEEY) Mod10
            //              *
            //              * Campo 3 - EEEFF.FFFGHZ
            //              * EEE - Restante do número do documento
            //              * FFFFF   -Código do Cliente
            //              * G       -DAC(Carteira / Nosso Numero(sem DAC) / Numero Documento / Codigo Cliente)
            //              * H - zero
            //              * Z - DAC Campo 3
            //              *
            //              *Campo 4 - K
            //              * K - DAC Código de Barras
            //              *
            //              * Campo 5 - UUUUVVVVVVVVVV
            //              * UUUU - Fator Vencimento
            //              * VVVVVVVVVV -Valor do Título
            //              */
            #endregion
            try
            {
                string linhaDigitavel = linha.Replace(" ", "").Replace(".", "");

                BoletoInfo detalhe = new BoletoInfo();

                var CodBanco = linhaDigitavel.Substring(0, 3);
                detalhe.NumeroBanco = BaseRotinas.ConverterParaShort(CodBanco);
                if ((EnumBoleto.BancoCodigo)BaseRotinas.ConverterParaInt(CodBanco) == EnumBoleto.BancoCodigo.Itau)
                {
                    var Moeda = linhaDigitavel.Substring(3, 1);
                    detalhe.Carteira = linhaDigitavel.Substring(4, 3);
                    detalhe.NossoNumero = linhaDigitavel.Substring(7, 2) + linhaDigitavel.Substring(10, 6);
                    detalhe.NumeroDocumento = linhaDigitavel.Substring(16, 4) + linhaDigitavel.Substring(21, 3);
                    detalhe.CedenteNumeroBoleto = linhaDigitavel.Substring(16, 4) + linhaDigitavel.Substring(21, 3);
                    var CodigoCliente = linhaDigitavel.Substring(24, 5);
                    var fatorVencimento = BaseRotinas.ConverterParaInt(linhaDigitavel.Substring(33, 4));
                    DateTime dtVencimento = UtilsCustom.ConvertFatorVencimentoEmData(fatorVencimento);
                    detalhe.DataVencimento = BaseRotinas.ConverterParaString(dtVencimento.Date);
                    detalhe.ValorBoleto = UtilsCustom.ConverterValorStringEmDecimal(linhaDigitavel);
                }
                if ((EnumBoleto.BancoCodigo)BaseRotinas.ConverterParaInt(CodBanco) == EnumBoleto.BancoCodigo.Santander)
                {

                    var Moeda = linhaDigitavel.Substring(3, 1);
                    detalhe.Carteira = linhaDigitavel.Substring(28, 3);
                    detalhe.NossoNumero = linhaDigitavel.Substring(13, 7) + linhaDigitavel.Substring(21, 6);
                    detalhe.NossoNumero = detalhe.NossoNumero.Substring(0, 12);
                    detalhe.NumeroDocumento = numeroDocumento;
                    detalhe.CedenteNumeroBoleto = linhaDigitavel.Substring(16, 4) + linhaDigitavel.Substring(21, 3);
                    var CodigoCliente = linhaDigitavel.Substring(24, 5);
                    var fatorVencimento = BaseRotinas.ConverterParaInt(linhaDigitavel.Substring(33, 4));
                    DateTime dtVencimento = UtilsCustom.ConvertFatorVencimentoEmData(fatorVencimento);
                    detalhe.DataVencimento = BaseRotinas.ConverterParaString(dtVencimento.Date);
                    detalhe.ValorBoleto = UtilsCustom.ConverterValorStringEmDecimal(linhaDigitavel);
                }


                return detalhe;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao ler detalhe do arquivo de .", ex);
            }
        }

        /// <summary>
        /// Obtem o código de ocorrência formatado, utiliza '01' - 'Entrada de titulos como padrão'
        /// </summary>
        /// <param name="boleto">Boleto</param>
        /// <returns>Código da ocorrência</returns>
        public static string ObterCodigoDaOcorrencia(BoletoNet.Boleto boleto)
        {
            return boleto.Remessa != null && !string.IsNullOrEmpty(boleto.Remessa.CodigoOcorrencia) ? Utils.FormatCode(boleto.Remessa.CodigoOcorrencia, 2) : TipoOcorrenciaRemessa.EntradaDeTitulos.Format();
        }
    }
}
