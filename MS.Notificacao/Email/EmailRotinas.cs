﻿using Cobranca.Notificacao.Email;
using Cobranca.Notificacao.Suporte;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Cobranca.Notificacao.EMAIL
{
    public class EmailRotinas
    {
        private int _SMTPPorta;
        private string _SMTPServidor;
        private string _SMTPUsuario;
        private string _SMTPSenha;
        private bool _fgSSL;

        public EmailRotinas(
           int? SMTPPorta,
           string SMTPServidor,
           string SMTPUsuario,
           string SMTPSenha,
           bool fgSSL
            )
        {
            _SMTPPorta = SMTPPorta.HasValue ? SMTPPorta.Value : 587;
            _SMTPServidor = SMTPServidor;
            _SMTPUsuario = SMTPUsuario;
            _SMTPSenha = SMTPSenha;
            _fgSSL = fgSSL;
        }

        public NotificacaoRetorno Enivar(EmailInfo objEmail)
        {
            try
            {
                MailMessage email = new MailMessage();

                SmtpClient smtp = new SmtpClient(_SMTPServidor, _SMTPPorta);

                if (_SMTPUsuario != string.Empty && _SMTPSenha != string.Empty)
                {
                    NetworkCredential smtpinfo = new NetworkCredential(_SMTPUsuario, _SMTPSenha);

                    smtp.UseDefaultCredentials = false;
                    smtp.EnableSsl = _fgSSL;
                    smtp.Credentials = smtpinfo;
                }

                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                email.From = new MailAddress(objEmail.EmailRemetente, objEmail.NomeRemetente);
                email.SubjectEncoding = Encoding.GetEncoding("ISO-8859-1");
                email.BodyEncoding = Encoding.GetEncoding("ISO-8859-1");

                email.IsBodyHtml = true;

                email.Body = objEmail.Conteudo;

                email.Subject = objEmail.Assunto;

                var emailDestinatarios = objEmail.EmailDestinatario.ToString().Split(';');
                foreach (var emailDestinatario in emailDestinatarios)
                {
                    email.To.Add(emailDestinatario);
                }              
                MailAddress copy = new MailAddress("plataforma@junix.com.br");
                email.Bcc.Add(copy);
                if (objEmail.Arquivos != null)
                {
                    foreach (var obj in objEmail.Arquivos)
                    {
                        Attachment att = new Attachment(new MemoryStream(obj.Dados), obj.NomeArquivo);
                        email.Attachments.Add(att);
                    }
                }

                smtp.Port = _SMTPPorta;//nova porta
                smtp.Send(email);
            }
            catch (Exception ex)
            {
                var exception = new NotificacaoRetorno(ex);
                exception.Mensagem = exception.Mensagem + " detalhe:" + ex.InnerException;
                exception.Mensagem = exception.Mensagem
                    + " Informações > "
                    + " EmailRemetente: "
                    + objEmail.EmailRemetente
                    + " NomeRemetente: "
                    + objEmail.NomeRemetente
                    + " EmailDestinatario: "
                    + objEmail.EmailDestinatario;
                exception.OK = false;
                return exception;
                //return new NotificacaoRetorno(ex);
            }

            return new NotificacaoRetorno();
        }

    }
}
