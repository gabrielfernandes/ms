﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cobranca.Notificacao.Email
{
    public class EmailInfo
    {

        public EmailInfo()
        {
            Arquivos = new List<EmailInfoArquivo>();
        }

        public string GUID { get; set; }
        public string EmailRemetente { get; set; }
        public string EmailDestinatario { get; set; }
        public string NomeRemetente { get; set; }
        public string NomeDestinatario { get; set; }
        public string Assunto { get; set; }
        public string Conteudo { get; set; }

        public List<EmailInfoArquivo> Arquivos { get; set; }
        public bool? HTML { get; set; }
    }

    public class EmailInfoArquivo
    {
        public string NomeArquivo { get; set; }
        public byte[] Dados { get; set; }
        public string CaminhoFisico { get; set; }
    }
}