﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cobranca.Notificacao.Suporte
{
    public class NotificacaoRetorno
    {
        public NotificacaoRetorno()
        {
            this.OK = true;

        }

        public NotificacaoRetorno(Exception ex)
        {
            this.OK = false;
            this.Mensagem = ex.Message;
            this.Dados = ex;
        }
        public bool OK { get; set; }
        public string Mensagem { get; set; }
        public object Dados { get; set; }
    }
}
