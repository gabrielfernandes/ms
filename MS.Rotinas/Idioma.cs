﻿using Cobranca.Db.Models;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using Cobranca.Db.Suporte;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cobranca.Rotinas
{
    public static class Idioma
    {
        private static bool _editarIdioma = false;
        private static bool _insertIfNotExists = true;

        public static HtmlString TraduzirJs(string codigo)
        {
            return new HtmlString(DbRotinas.TratarTexto(',', Traduzir(codigo)));
        }

        public static string Traduzir(string codigo)
        {
            Enumeradores.IdiomaTipo tipoIdioma = ObterIdiomaUsuarioAtual();
            var listaPalavras = ObterListaIdioma(tipoIdioma);

            if (!string.IsNullOrEmpty(codigo))
            {
                if (string.IsNullOrEmpty(DbRotinas.ConverterParaString(listaPalavras[codigo.ToLower()])))
                {
                    if (_insertIfNotExists)
                    {
                        SalvarPalavraNoBanco(codigo.ToLower());
                    }
                    return codigo;
                }
                else
                {
                    if (listaPalavras[codigo.ToLower()].ToString().Trim().Length > 0)
                    {
                        string traducao = listaPalavras[codigo.ToLower()].ToString().Trim();
                        if (!String.IsNullOrEmpty(traducao)) { traducao = traducao.Replace("'", "\""); }

                        if (!_editarIdioma)
                        {
                            return traducao;
                        }
                        else
                        {
                            return string.Format("traduzir={0}&{1}&", codigo, traducao);
                        }


                    }
                    else
                    {

                        return codigo;
                    }
                }
            }
            else return codigo;
        }




        private static void SalvarPalavraNoBanco(string codigo)
        {
            new IdiomaDb().SalvarTraducao(new IdiomaInfo()
            {
                Cod = codigo,
                esES = codigo,
                ptBR = codigo,
                enUS = codigo
            });
        }

        public static string Traduzir(string codigo, bool tratarScript)
        {
            string texto = Traduzir(codigo);

            if (!string.IsNullOrEmpty(texto))
                return TratarScript(texto);
            else
                return texto;
        }

        private static string TratarScript(string texto)
        {
            return texto.Trim().Replace("'", "\\'").Replace("\n", "").Replace("\r", "");
        }

        public static string Traduzir(string codigo, int tipoIdioma)
        {
            return Traduzir(codigo, tipoIdioma, false);
        }

        public static string Traduzir(string codigo, int tipoIdioma, bool tratarScript)
        {
            var listaPalavras = ObterListaIdioma((Enumeradores.IdiomaTipo)tipoIdioma);

            if (!string.IsNullOrEmpty(codigo))
            {
                if (listaPalavras[codigo] == null)
                {
                    return codigo;
                }
                else
                {
                    if (tratarScript)
                        return TratarScript(listaPalavras[codigo].ToString());
                    else
                        return listaPalavras[codigo].ToString().Trim();
                }
            }
            else return codigo;
        }

        public static Enumeradores.IdiomaTipo ObterIdiomaUsuarioAtual()
        {
            Enumeradores.IdiomaTipo tipoIdioma = Enumeradores.IdiomaTipo.pt_BR;

            if (HttpContext.Current != null && HttpContext.Current.Session["idioma"] != null)
            {
                short t = DbRotinas.ConverterParaShort(HttpContext.Current.Session["idioma"]);
                if (t != 0)
                {
                    tipoIdioma = (Enumeradores.IdiomaTipo)t;
                }
                else
                {
                    tipoIdioma = Enumeradores.IdiomaTipo.pt_BR;
                }

                return tipoIdioma;
            }

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                tipoIdioma = IdiomaLerCookie();
            }
            return tipoIdioma;
        }

        private static Hashtable ObterListaIdioma(Enumeradores.IdiomaTipo tipoIdioma)
        {
            var nomeCacheIdioma = "cacheIdioma_" + tipoIdioma.ToString();

            if (System.Web.HttpRuntime.Cache[nomeCacheIdioma] == null)
            {
                IdiomaDb obj = new IdiomaDb();
                var lista = obj.Listagem();

                Hashtable tbIdioma = new Hashtable();

                foreach (var palavra in lista)
                {
                    if (tipoIdioma == Enumeradores.IdiomaTipo.pt_BR)
                        tbIdioma.Add(palavra.Cod.ToLower(), palavra.ptBR);
                    else if (tipoIdioma == Enumeradores.IdiomaTipo.en_US)
                        tbIdioma.Add(palavra.Cod.ToLower(), palavra.enUS);
                    else if (tipoIdioma == Enumeradores.IdiomaTipo.es_ES)
                        tbIdioma.Add(palavra.Cod.ToLower(), palavra.esES);
                }

                System.Web.HttpRuntime.Cache.Add(nomeCacheIdioma, tbIdioma, null, DateTime.MaxValue, new TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
                return tbIdioma;
            }
            else
            {
                return (Hashtable)System.Web.HttpRuntime.Cache[nomeCacheIdioma];
            }
        }

        public static void IdiomaGravarCookie(string id)
        {
            HttpCookie cookie = new HttpCookie("idioma", id);
            cookie.Expires = DateTime.Now.AddYears(1);

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static Enumeradores.IdiomaTipo IdiomaLerCookie()
        {
            if (HttpContext.Current.Request.Cookies["idioma"] != null)
            {
                Enumeradores.IdiomaTipo tipoIdioma;
                switch (HttpContext.Current.Request.Cookies["idioma"].Value)
                {
                    case "es_ES":
                        tipoIdioma = Enumeradores.IdiomaTipo.es_ES;
                        break;

                    case "en_US":
                        tipoIdioma = Enumeradores.IdiomaTipo.en_US;
                        break;

                    default:
                        tipoIdioma = Enumeradores.IdiomaTipo.pt_BR;
                        break;
                }
                return tipoIdioma;
            }
            else
                return Enumeradores.IdiomaTipo.pt_BR;
        }

        public static void LimparCache()
        {
            var nomeCacheIdiomaPt = "cacheIdioma_" + Enumeradores.IdiomaTipo.pt_BR.ToString();
            var nomeCacheIdiomaUs = "cacheIdioma_" + Enumeradores.IdiomaTipo.en_US.ToString();
            var nomeCacheIdiomaEs = "cacheIdioma_" + Enumeradores.IdiomaTipo.es_ES.ToString();

            if (System.Web.HttpRuntime.Cache[nomeCacheIdiomaPt] != null)
                System.Web.HttpRuntime.Cache.Remove(nomeCacheIdiomaPt);

            if (System.Web.HttpRuntime.Cache[nomeCacheIdiomaEs] != null)
                System.Web.HttpRuntime.Cache.Remove(nomeCacheIdiomaEs);

            if (System.Web.HttpRuntime.Cache[nomeCacheIdiomaUs] != null)
                System.Web.HttpRuntime.Cache.Remove(nomeCacheIdiomaUs);
        }
    }
}
