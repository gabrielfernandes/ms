﻿using Cobranca.Db.Rotinas;
using Cobranca.Rotinas.Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Rotinas.Integracoes
{
    public class IntegracaoRepasse
    {
        public void AtualizarContratos()
        {
            List<ContratoRepasse> contratosRepasse = new List<ContratoRepasse>();
            contratosRepasse = this.ListarContratosRepasse();
            ProcessarCasos(contratosRepasse);
        }

        private void ProcessarCasos(List<ContratoRepasse> contratosRepasse)
        {
            SqlConnection conexao = new SqlConnection("Data Source=186.202.39.68;Initial Catalog=tendaqa_cobflow;Persist Security Info=True;User ID=tendaqa_cobflow;Password=jnx@2312;");
            SqlCommand cmd = null;

            try
            {
                conexao.Open();
                foreach (var item in contratosRepasse)
                {
                    try
                    {
                        cmd = new SqlCommand("SP_ATUALIZAR_CONTRATO_REPASSE", conexao);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("CodSAP", item.CodSAP);
                        cmd.Parameters.AddWithValue("PrevisaoSGI", item.PrevisaoSGI);
                        cmd.Parameters.AddWithValue("StatusRepasse", item.StatusRepasse);
                        cmd.Parameters.AddWithValue("TipoVenda", item.TipoVenda);
                        cmd.ExecuteNonQuery();
                        item.OK = true;
                    }
                    catch (Exception ex)
                    {
                        item.OK = false;
                        item.Mensagem = ex.Message;
                    }
                 
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                conexao.Close();
            }
        }

        private List<ContratoRepasse> ListarContratosRepasse()
        {
            List<ContratoRepasse> lista = new List<ContratoRepasse>();
            SqlConnection conexao = new SqlConnection("Data Source=186.202.39.68;Initial Catalog=tenda_imobflow;Persist Security Info=True;User ID=tenda_imobflow;Password=J7f7p2s2023;");
            SqlCommand cmd = null;
            SqlDataReader dr;
            cmd = new SqlCommand("SP_INTEGRACAO_REPASSE_COBRANCA", conexao);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conexao.Open();
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {

                    while (dr.Read())
                    {
                        ContratoRepasse item = new ContratoRepasse();

                        item.CodSAP = DbRotinas.ConverterParaString(dr["CodSAP"]);
                        item.PrevisaoSGI = DbRotinas.ConverterParaDatetimeOuNull(dr["PrevisaoSGI"]);
                        item.StatusRepasse = DbRotinas.ConverterParaString(dr["StatusRepasse"]);
                        item.TipoVenda = DbRotinas.ConverterParaString(dr["TipoVenda"]);

                        lista.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                conexao.Close();
            }


            return lista;
        }
    }

}
