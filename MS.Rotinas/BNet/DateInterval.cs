using System;
using System.Collections.Generic;
using System.Text;

namespace Cobranca.Rotinas.BNet
{
    internal enum DateInterval
    {
        Second,
        Minute,
        Hour,
        Day,
        Week,
        Month,
        Quarter,
        Year
    }
}
