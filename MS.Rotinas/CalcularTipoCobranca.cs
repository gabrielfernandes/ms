﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cobranca.Rotinas
{
    public class CalcularTipoCobranca
    {
        Repository<TabelaTipoCobranca> _repoTipoCobranca = new Repository<TabelaTipoCobranca>();
        Repository<Contrato> _repoContrato = new Repository<Contrato>();
        Repository<ContratoParcela> _repoContratoParcela = new Repository<ContratoParcela>();
        Repository<TabelaParametroTaxa> _repoTaxa = new Repository<TabelaParametroTaxa>();
        Repository<ContratoTipoCobrancaHistorico> _repoContratoTipoCobrancaHistorico = new Repository<ContratoTipoCobrancaHistorico>();

        public void Calcular()
        {
            var listaTipoCobranca = _repoTipoCobranca.All().ToList();

            foreach (var item in listaTipoCobranca)
            {
                ProcessarCasosPorTipo(item);
            }
        }

        public void ProcessarCasosPorTipo(TabelaTipoCobranca item)
        {
            //Dados objDados = new Dados();

            //var contratos = objDados.executarCondicosTipoCobranca(item).Take(100);

            //foreach (var contrato in contratos)
            //{
            //    ProcessarPorContrato(contrato.CodContrato, item);
            //}
        }
        public void CalcularJurosMulta(int CodContrato = 0)
        {
            Repository<ContratoParcela> _dbContratoParcela = new Repository<ContratoParcela>();
            var listaParcela = _repoContratoParcela.All().Where(x => x.CodContrato == CodContrato).ToList();
            var taxas = _repoTaxa.All().FirstOrDefault();

            foreach (var item in listaParcela)
            {
                var DataVencimento = item.DataVencimento;

                var ValorParcela = item.ValorParcela;

                var Meses = SubtrairData(DateTime.Today, DataVencimento);

                var multa = DividirSemErro((decimal)(taxas.MultaDe * item.ValorParcela), 100);

                var juros = Math.Pow((double)(1 + DividirSemErro((decimal)(taxas.JurosDe), 100)), Meses)-1;

                var atualizado =  ((((decimal)juros * ValorParcela)+1) + multa + ValorParcela);

                var parcela = _repoContratoParcela.FindById(item.Cod);

                parcela.ValorAtualizado = (decimal)Math.Round((double)atualizado, 2);
                parcela.Juros = (decimal)Math.Round(juros, 2);
                parcela.Multa = (decimal)Math.Round(multa, 2);
               
                _dbContratoParcela.CreateOrUpdate(parcela);

            }

        }
        private int SubtrairData(DateTime data1, DateTime data2)
        {
            int meses;
            if (data1 != null && data2 != null)
            {
                meses = ((data1- data2).Days/30);
            }
            else
            {
                meses = 0;
            }

            return meses;
        }
        private decimal DividirSemErro(decimal Valor1, decimal Valor2)
        {
            decimal resultado;
            if (Valor1 > 0 && Valor2 > 0)
            {
                resultado = Valor1 / Valor2;
            }
            else
            {
                resultado = 0;
            }
            return resultado;
        }
        private void ProcessarPorContrato(int codContrato, TabelaTipoCobranca tipoCobranca )
        {
            var objcontrato = _repoContrato.FindById(codContrato);
                        
            var historico = new ContratoTipoCobrancaHistorico()
            {
                CodTipoCobranca = tipoCobranca.Cod,
                DataCadastro = DateTime.Now,
                CodContrato = codContrato
            };
            _repoContratoTipoCobrancaHistorico.CreateOrUpdate(historico);
            objcontrato.CodTipoCobranca = tipoCobranca.Cod;

            _repoContrato.CreateOrUpdate(objcontrato);
        }
    }
}
