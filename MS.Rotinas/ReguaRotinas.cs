﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using Cobranca.Db.Suporte;
using Cobranca.Notificacao.Email;
using Cobranca.Notificacao.EMAIL;
using Cobranca.Rotinas.BoletoGerar;
using Cobranca.Rotinas.Modelos;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Rotinas
{
    public class ReguaRotinas
    {

        Repository<TabelaTipoCobrancaAcao> _dbTabelaTipoCobrancaAcao = new Repository<TabelaTipoCobrancaAcao>();
        Repository<Cliente> _dbCliente = new Repository<Cliente>();
        Repository<Contrato> _dbContrato = new Repository<Contrato>();
        Repository<ContratoAcaoCobrancaHistorico> _dbContratoAcao = new Repository<ContratoAcaoCobrancaHistorico>();
        Repository<LogEmail> _dbLogEmail = new Repository<LogEmail>();
        public Repository<ParametroPlataforma> _dbParametro = new Repository<ParametroPlataforma>();

        private EmailRotinas _objEmailRotinas;

        junix_cobrancaContext _db = new junix_cobrancaContext();

        public OperacaoDbRetorno ProcesssarEventosEmail()
        {
            try
            {
                OperacaoDbRetorno op = new OperacaoDbRetorno();

                var items = ListarEventosEmails();
                var grupoRegional = items.GroupBy(x => x.CodRegional);

                foreach (var itemRegional in grupoRegional)
                {
                    int codRegional = itemRegional.Key;
                    var grupoCliente = itemRegional.GroupBy(x => x.CodClientePrincipal);
                    foreach (var itemCliente in grupoCliente)
                    {
                        int codCliente = itemCliente.Key;
                        var grupoAcao = itemCliente.GroupBy(x => x.CodTipoCobrancaAcao);
                        foreach (var itemAcao in grupoAcao)
                        {
                            int codAcao = itemAcao.Key;

                            processarAcao(codAcao, codCliente, itemAcao, codRegional);
                            MarcarAcoes(true, "ENVIADO COM SUCESSO", itemAcao);
                        }
                    }

                }
                op.Mensagem = "Preparação dos e-mails concluida";
                return op;
            }
            catch (Exception ex)
            {
                var exception = new OperacaoDbRetorno(ex);
                exception.Mensagem = exception.Mensagem + " detalhe:" + ex.InnerException;
                return exception;
            }
        }

        private void MarcarAcoes(bool OK, string Mensagem, IGrouping<int, EventoEmailInfo> acoes)
        {

            foreach (var item in acoes)
            {
                var evento = _db.ContratoAcaoCobrancaHistoricoes.Where(x => x.Cod == item.Cod).FirstOrDefault();
                if (evento != null)
                {
                    evento.DataEnvio = DateTime.Now;
                    evento.Status = Mensagem;

                }
            }
            _db.SaveChanges();

        }

        private void processarAcao(int CodTipoCobrancaAcao, int CodCliente, IEnumerable<EventoEmailInfo> acoes, int CodRegional)
        {
            LogEmail email = new LogEmail();
            try
            {
                var objAcao = _dbTabelaTipoCobrancaAcao.FindById(CodTipoCobrancaAcao);
                var objCliente = _dbCliente.FindById(CodCliente);
                var objRegional = _db.TabelaRegionals.Find(CodRegional);
                StringBuilder sb = new StringBuilder();

                bool _fgAnexarBoleto = objAcao.TabelaAcaoCobranca.Tipo == (short)Enumeradores.TipoAcaoCobranca.EMAIL_SEGUNDA_VIA_BOLETO;

                sb.Append(objAcao.Modelo);

                string tbParcelas = MontarTabelaHtml(acoes);

                string emailRegional = "";
                //var objEvento = _dbContratoAcao.FindById(CodTipoCobrancaAcao)


                email.Assunto = objAcao.Titulo;

                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.TabelaParcelas), tbParcelas);
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NomeComprador), DbRotinas.Descriptografar(objCliente.Nome));
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.CPF_CPNPJ_Comprador), DbRotinas.Descriptografar(objCliente.CPF));
                //sb = sb.Replace("@CPF_CNPJ_COMPRADOR", DbRotinas.Descriptografar(objCliente.CNPJ));

                if (objRegional != null)
                {
                    //DADOS DA REGIONAL PRODUTO;
                    sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.DescricaoFilial), DbRotinas.Descriptografar(objRegional.Regional));
                    sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NumeroFilial), objRegional.Telefone);
                    sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.ResponsavelFilial), objRegional.Responsavel);
                    emailRegional = objRegional.Email;
                    email.NomeRemetente = objRegional.Responsavel;
                }




                email.Conteudo = sb.ToString();
                email.EmailDestinatario = DbRotinas.Descriptografar(objCliente.Email);
                //email.EmailDestinatario = "alessandro.raquella@otis.com";
                email.HTML = true;
                email.EmailRemetente = emailRegional;

                bool emailOK = !string.IsNullOrEmpty(email.Conteudo) && !string.IsNullOrEmpty(email.Assunto);

                email.Status = emailOK ? (short)Enumeradores.LogEmailStatus.AguardandoEnvio : (short)Enumeradores.LogEmailStatus.FalhaNoEnvio;

                email.NomeDestinatario = DbRotinas.Descriptografar(objCliente.Nome);

                if (_fgAnexarBoleto)
                    email.Status = (short)Enumeradores.LogEmailStatus.NaoEnviar;


                var objLogEmail = _dbLogEmail.CreateOrUpdate(email);

                if (_fgAnexarBoleto)
                {
                    AnexarBoletosEnviarEmail(acoes, objLogEmail);
                }
                //new EmailRotinas().Enivar(email);
            }
            catch (Exception ex)
            {
                var exception = new Notificacao.Suporte.NotificacaoRetorno(ex);
                exception.Mensagem = exception.Mensagem + " detalhe:" + ex.InnerException;
                exception.Mensagem = exception.Mensagem
                    + " Informações > "
                    + " EmailRemetente: "
                    + email.EmailRemetente
                    + " NomeRemetente: "
                    + email.NomeRemetente
                    + " EmailDestinatario: "
                    + email.EmailDestinatario;
            }

        }

        private void AnexarBoletosEnviarEmail(IEnumerable<EventoEmailInfo> acoes, LogEmail email)
        {

            var _objParametro = _dbParametro.FindAll().FirstOrDefault();
            _objEmailRotinas = new EmailRotinas(_objParametro.SMTPPorta, _objParametro.SMTPServidor, _objParametro.SMTPUsuario, _objParametro.SMTPSenha, _objParametro.fgSSL == true);
            var arquivo = new EmailInfo();
            BoletoUtils gerarBoleto = new BoletoUtils();
            var boleto = new EmailInfoArquivo();
            foreach (var p in acoes)
            {
                var objParcela = _db.ContratoParcelas.Find(p.CodParcela);

                if (objParcela.LinhaDigitavel != null)
                {
                    BoletoUtils.BoletoRetorno objRetornoBoleto = gerarBoleto.GerarBoletoPorLinhaDigitavel(objParcela);

                    var boletoByte = gerarBoleto.MontaBoletoEmBytePDF(objRetornoBoleto.Boleto, "", "", objParcela.FluxoPagamento, false, false);
                    boleto.Dados = boletoByte;
                    boleto.NomeArquivo = objParcela.FluxoPagamento + ".pdf";
                    arquivo.Arquivos.Add(boleto);
                }
            }
            if (arquivo.Arquivos.Any())//
            {

                //OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false, Mensagem = $"EVENTO DESATIVADO" };
                OperacaoDbRetorno op = new OperacaoDbRetorno();
                if (op.OK)
                {
                    var NotificacaoRetorno = _objEmailRotinas.Enivar(new EmailInfo()
                    {
                        Assunto = email.Assunto,
                        Conteudo = email.Conteudo,
                        EmailRemetente = email.EmailRemetente,
                        EmailDestinatario = email.EmailDestinatario,
                        NomeDestinatario = email.NomeDestinatario,
                        NomeRemetente = email.NomeRemetente,
                        Arquivos = arquivo.Arquivos,
                        HTML = true
                    });
                    if (NotificacaoRetorno.OK)
                    {
                        email.Status = (short)Enumeradores.LogEmailStatus.Enviado;
                        email.Mensagem = NotificacaoRetorno.Mensagem;
                    }
                    else
                    {
                        email.Status = (short)Enumeradores.LogEmailStatus.FalhaNoEnvio;
                        email.Mensagem = NotificacaoRetorno.Mensagem;
                    }
                }
                else
                {
                    email.Status = (short)Enumeradores.LogEmailStatus.FalhaNoEnvio;
                    email.Mensagem = op.Mensagem;
                }
            }
            else
            {
                email.Status = (short)Enumeradores.LogEmailStatus.NaoEnviar;
                email.Mensagem = "Ação não possui nenhum boleto para anexo";
            }

            var entityEmail = _dbLogEmail.CreateOrUpdate(email);
        }

        private string MontarTabelaHtml(IEnumerable<EventoEmailInfo> acoes)
        {
            StringBuilder sbTabelaParcelas = new StringBuilder();

            sbTabelaParcelas.AppendFormat($"<table border='1'>");
            sbTabelaParcelas.AppendFormat($"<tr stype='border-bottom:solid 1px #808080;'>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Contrato</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Fatura</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Nº Parcela</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Vencimento</th>");
            sbTabelaParcelas.AppendFormat($"<th stype='width:300px;text-align:center;'>Valor</th>");
            sbTabelaParcelas.AppendFormat($"</tr>");



            foreach (var item in acoes)
            {
                var parcela = _db.ContratoParcelas.Where(x => x.Cod == item.CodParcela).FirstOrDefault();

                //emailRegional = parcela.Contrato.TabelaRegionalProduto.TabelaRegional.Email;

                sbTabelaParcelas.AppendFormat($"<tr style='border-bottom:solid 1px #808080'>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{DbRotinas.Descriptografar(parcela.Contrato.NumeroContrato)}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.FluxoPagamento}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.NumeroParcela}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.DataVencimento:dd/MM/yyyy}</td>");
                sbTabelaParcelas.AppendFormat($"<td stype='width:300px;text-align:center;'>{parcela.ValorAberto:C2}</td>");
                sbTabelaParcelas.AppendFormat($"</tr>");

            }
            sbTabelaParcelas.AppendFormat($"</table>");

            return sbTabelaParcelas.ToString();
        }

        /// <summary>
        /// Método que cria os eventos da regua conforme parametros do sistema;
        /// </summary>
        /// <returns>OperacaoDbRetorno OK/Mensagem</returns>
        public OperacaoDbRetorno ExecutarRegua()
        {
            OperacaoDbRetorno retorno = new OperacaoDbRetorno();
            StringBuilder log = new StringBuilder();
            try
            {

                log.Append($"{DateTime.Now:dd/MM/yyyy HH:mm:ss} - Processando Açoes de E-Mail");
                log.Append(Environment.NewLine);
                //ACOES DE EMAIL
                var opEmail = GerarAcoes(Enumeradores.TipoAcaoCobranca.EMAIL);//GERANDO AS ACOES
                var _log = new LogServico
                {
                    TipoServico = (short)Enumeradores.TipoServico.Regua_Email,
                    Mensagem = $"Processamento Diário - Ações de E-Mail - OK = {opEmail.OK} - Mensagem: {opEmail.Mensagem}"
                };
                GravarLogServico(_log);


                //ACOES DE CONTATO

                log.Append($"{DateTime.Now:dd/MM/yyyy HH:mm:ss} - Processando Açoes de Contato");
                log.Append(Environment.NewLine);
                var opContato = GerarAcoes(Enumeradores.TipoAcaoCobranca.CONTATO);//GERANDO AS ACOES
                _log = new LogServico
                {
                    TipoServico = (short)Enumeradores.TipoServico.Regua_Contato,
                    Mensagem = $"Processamento Diário - Ações de CONTATO - OK = {opContato.OK} - Mensagem: {opContato.Mensagem}"
                };
                GravarLogServico(_log);

                //ACOES DE 2ª via de BOLETO
                log.Append($"{DateTime.Now:dd/MM/yyyy HH:mm:ss} - Processando Açoes de Envio de Boleto");
                log.Append(Environment.NewLine);
                var opEmailBoleto = GerarAcoes(Enumeradores.TipoAcaoCobranca.EMAIL_SEGUNDA_VIA_BOLETO);//GERANDO AS ACOES
                _log = new LogServico
                {
                    TipoServico = (short)Enumeradores.TipoServico.Regua_Email_boleto,
                    Mensagem = $"Processamento Diário - 2ª via de Boleto - OK = {opContato.OK} - Mensagem: {opContato.Mensagem}",
                };
                GravarLogServico(_log);

                //ACOES DE NEGATIVACAO

                log.Append($"{DateTime.Now:dd/MM/yyyy HH:mm:ss} - Processando Açoes de Serasa");
                log.Append(Environment.NewLine);
                var opNegativacao = GerarAcoes(Enumeradores.TipoAcaoCobranca.NEGATIVACAO_SERASA);
                _log = new LogServico
                {
                    TipoServico = (short)Enumeradores.TipoServico.Regua_Negativacao,
                    Mensagem = $"Processamento Diário - Negativação - OK = {opContato.OK} - Mensagem: {opContato.Mensagem}",
                };
                GravarLogServico(_log);

                retorno.OK = opEmail.OK && opContato.OK;
                retorno.Mensagem = log.ToString();
                return retorno;

            }
            catch (Exception ex)
            {
                var opRetorno = new OperacaoDbRetorno(ex);
                opRetorno.Mensagem = $"{log} {Environment.NewLine} detalhe: {ex.Message}";
                return opRetorno;
            }

        }
        public List<EventoEmailInfo> ListarEventosEmails()
        {
            List<EventoEmailInfo> eventos = new List<EventoEmailInfo>();

            try
            {

                using (SqlConnection conn = new SqlConnection(DbRotinas.ObterConnectionString()))
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[SP_COBRANCA_REGUA_ACAO_EMAIL_LISTAR]", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;

                    //cmd.Parameters.AddWithValue("@Tipo", (short)tipo);
                    conn.Open();
                    var dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        eventos.Add(new EventoEmailInfo()
                        {
                            Cod = DbRotinas.ConverterParaInt(dr["Cod"]),
                            CodClientePrincipal = DbRotinas.ConverterParaInt(dr["CodClientePrincipal"]),
                            CodContrato = DbRotinas.ConverterParaInt(dr["CodContrato"]),
                            CodParcela = DbRotinas.ConverterParaInt(dr["CodParcela"]),
                            CodTipoCobrancaAcao = DbRotinas.ConverterParaInt(dr["CodTipoCobrancaAcao"]),
                            CodRegional = DbRotinas.ConverterParaInt(dr["CodRegional"])
                        });
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return eventos;

        }
        /// <summary>
        /// Rotina cria as ações por tipo
        /// </summary>
        /// <param name="tipo">Tipo da ação</param>
        /// <returns>OperacaoDbRetorno</returns>
        private OperacaoDbRetorno GerarAcoes(Enumeradores.TipoAcaoCobranca tipo)
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();
            try
            {

                using (SqlConnection conn = new SqlConnection(DbRotinas.ObterConnectionString()))
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[SP_COBRANCA_REGUA_EXECUTAR_POR_TIPO_ACAO]", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    conn.Open();
                    cmd.Parameters.AddWithValue("@Tipo", (short)tipo);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                op = new OperacaoDbRetorno(ex);
            }

            return op;
        }


        public OperacaoDbRetorno ObterScritpAtendimentoPorParcela(int CodParcela, string NomeUsuarioAtendente)
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();
            try
            {
                int codCliente = 0;
                int codContrato = 0;
                string modelo = string.Empty;
                using (SqlConnection conn = new SqlConnection(DbRotinas.ObterConnectionString()))
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[SP_COBRANCA_REGUA_PESQUISAR_ACAO_POR_PARCELA]", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    conn.Open();
                    cmd.Parameters.AddWithValue("@CodParcela", CodParcela);

                    var dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        codCliente = DbRotinas.ConverterParaInt(dr["CodCliente"]);
                        codContrato = DbRotinas.ConverterParaInt(dr["CodContrato"]);
                        modelo = DbRotinas.ConverterParaString(dr["Modelo"]);
                    }
                }

                op = MontarScritpAtendimento(CodParcela, codCliente, codContrato, modelo, NomeUsuarioAtendente);
            }
            catch (Exception ex)
            {
                op = new OperacaoDbRetorno(ex);
            }

            return op;
        }

        public OperacaoDbRetorno FinalizarAtendimentoPorParcela(int? codParcela, DateTime? dataAgenda, string obsStatus)
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();
            try
            {
                using (SqlConnection conn = new SqlConnection(DbRotinas.ObterConnectionString()))
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[SP_COBRANCA_ATENDIMENTO_FINALIZAR_POR_PARCELA]", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600;
                    conn.Open();
                    cmd.Parameters.AddWithValue("@CodParcela", codParcela);
                    cmd.Parameters.AddWithValue("@DataAgendamento", dataAgenda);
                    cmd.Parameters.AddWithValue("@Status", obsStatus);
                    cmd.ExecuteNonQuery();

                }

                op.OK = true;

            }
            catch (Exception ex)
            {
                op = new OperacaoDbRetorno(ex);
            }
            return op;
        }

        private OperacaoDbRetorno MontarScritpAtendimento(int codParcela, int codCliente, int codContrato, string Modelo, string UsuarioAtendente)
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();

            var objParcela = _db.ContratoParcelas.Where(x => x.Cod == codParcela).FirstOrDefault();
            var objContrato = _db.Contratoes.Where(x => x.Cod == codContrato).FirstOrDefault();
            var objCliente = _db.Clientes.Where(x => x.Cod == codCliente).FirstOrDefault();

            StringBuilder sb = new StringBuilder();

            sb.Append(Modelo);

            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.TabelaParcelas), string.Empty);

            if (objCliente != null)
            {
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.NomeComprador), DbRotinas.Descriptografar(objCliente.Nome));
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.CPF_CPNPJ_Comprador), DbRotinas.Descriptografar(objCliente.CPF));
            }

            if (objParcela != null)
            {
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.ValorParcela), DbRotinas.FormatarMoeda(objParcela.ValorParcela));
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.DataVencimentoParcela), DbRotinas.FormatarData(objParcela.DataVencimento));
                sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.ValorEmAberto), DbRotinas.FormatarMoeda(objParcela.ValorAberto));
            }

            sb = sb.Replace(EnumeradoresDescricao.TabelaParcelas(Enumeradores.ChaveModelo.AtendimentoUsuairo), UsuarioAtendente);





            op.Mensagem = sb.ToString();
            op.OK = true;
            return op;
        }

        public static LogServico GravarLogServico(LogServico _log)
        {
            try
            {
                //REGISTRA NO ARQUIVO TXT
                if (_log.DataInicio == null) { _log.DataInicio = DateTime.Now; }
                //REGISTRA NO LOG EMAIL
                LogServico log = new LogServico();
                log.CodTipoCobrancaAcao = _log.CodTipoCobrancaAcao;
                log.CodImportacao = _log.CodImportacao;
                log.TipoServico = _log.TipoServico;
                log.TipoImportacao = _log.TipoImportacao;
                log.Arquivos = _log.Arquivos;
                log.DataInicio = _log.DataInicio;
                log.DataFim = _log.DataFim;
                log.Mensagem = _log.Mensagem;

                Repository<LogServico> _dbLogServico = new Repository<LogServico>();
                return _dbLogServico.CreateOrUpdate(log);
            }
            catch { }
            return _log;
        }
    }
}
