﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoletoNet;
using Cobranca.Db.Rotinas;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Models;
using Cobranca.Rotinas.Remessa;
using System.IO;
using Cobranca.Rotinas.BNet;

namespace Cobranca.Rotinas.BoletoGerar
{
    public class BoletoUtils
    {
        private Repository<ContratoParcela> _ContratoParcela = new Repository<ContratoParcela>();
        private Repository<Cliente> _dbCliente = new Repository<Cliente>();
        private Repository<ContratoCliente> _dbContratoCliente = new Repository<ContratoCliente>();
        private Repository<TabelaBancoPortador> _dbParametroBoleto = new Repository<TabelaBancoPortador>();
        private Repository<TabelaBanco> _dbParamBanco = new Repository<TabelaBanco>();
        private Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();


        public BoletoRetorno GerarBoleto(ContratoParcela parcela)
        {
            try
            {
                BoletoInfo boletoInfo = new BoletoInfo();
                return CriarBoleto(parcela);
                //return MontarBoleto(boletoInfo, parcela);
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao ler detalhe do arquivo de .", ex);
            }


        }

        public BoletoRetorno GerarBoletoPorLinhaDigitavel(ContratoParcela parcela)
        {
            //ROTINA PARA OBTER OS DADOS VIA LINHA DIGITAVEL
            try
            {
                BoletoInfo boletoInfo = this.LerLinhaDigitavelCNAB400(parcela.LinhaDigitavel);
                return MontarBoleto(boletoInfo, parcela);
            }
            catch (Exception ex)
            {
                return new BoletoRetorno(ex);
            }
        }
        public RetornoLinhaDigitavel GerarLinhaDigitavel(ContratoParcelaBoleto boleto)
        {
            RetornoLinhaDigitavel retorno = new RetornoLinhaDigitavel();
            try
            {
                var CodigoBarra = MontarCodigoBarra(boleto);
                return new RetornoLinhaDigitavel() { OK = true, LinhaDigitavel = CodigoBarra.LinhaDigitavel };
            }
            catch (Exception ex)
            {
                return new RetornoLinhaDigitavel() { OK = false, Mensagem = $"Erro ao Gerar linha digitável {ex.Message}" };
            }
        }


        public static CodigoBarra GerarCodigoBarra(ContratoParcelaBoleto boleto)
        {
            var CodigoBarra = new BoletoUtils().MontarCodigoBarra(boleto);
            return CodigoBarra;
        }




        /// <param name="tituloNaView">Título Que aparecerá na Aba do Navegador</param>
        /// <param name="CustomSwitches">Custom WkHtmlToPdf global options</param>
        /// <param name="tituloPDF">Título No Início do PDF</param>
        /// <param name="PretoBranco">Preto e Branco = true</param>
        /// <param name="convertLinhaDigitavelToImage">bool Converter a Linha Digitavel Em Imagem</param>
        /// <returns>byte[], Vetor de bytes do PDF</returns>
        public byte[] MontaBoletoEmBytePDF(BoletoBancario boleto, string tituloNaView = "", string CustomSwitches = "", string tituloPDF = "", bool PretoBranco = false, bool convertLinhaDigitavelToImage = false)
        {
            StringBuilder htmlBoletos = new StringBuilder();
            //var html1 = boleto.MontaHtmlEmbedded(convertLinhaDigitavelToImage, true);
            var html = boleto.MontaHtmlEmbedded(false, true);


            return (new NReco.PdfGenerator.HtmlToPdfConverter() { CustomWkHtmlArgs = CustomSwitches, Grayscale = PretoBranco }).GeneratePdf(html.ToString());
        }
        public RemessaRetorno MontarArquivoRemessaCNAB240(int NumeroArquivo, List<ContratoParcelaBoleto> contratoParcelaBoletos, Stream Arquivo, int CodCedente)
        {
            RemessaCustom remessa = new RemessaCustom();
            RemessaRetorno retorno = new RemessaRetorno();
            try
            {
                var _parametroCedente = _dbParametroBoleto.FindById(CodCedente);
                var _numeroBanco = (short)_parametroCedente.BancoNumero;

                Banco Banco = new Banco(_numeroBanco);
                Cedente cedente = MontarCedente(_parametroCedente);

                var convertRetorno = ConverterContratoParcelaBoletoEmBoletos(contratoParcelaBoletos);
                List<BoletoNet.Boleto> boletos = convertRetorno.Boletos;
                remessa.GerarArquivoRemessa("0", Banco, cedente, boletos, Arquivo, NumeroArquivo, TipoArquivo.CNAB240);
                retorno.OK = true;
                retorno.Mensagem = "Arquivo Gerado!";
                return retorno;

            }
            catch (Exception ex)
            {
                return new RemessaRetorno(ex);
            }

        }



        /// <summary>
        /// Função para completar um string com zeros ou espacos em branco. Pode servir para criar a remessa.
        /// </summary>
        /// <param name="text">O valor recebe os zeros ou espaços em branco</param>
        /// <param name="with">caractere a ser inserido</param>
        /// <param name="size">Tamanho do campo</param>
        /// <param name="left">Indica se caracteres serão inseridos à esquerda ou à direita, o valor default é inicializar pela esquerda (left)</param>
        /// <returns>retorna o text formatado</returns>
        internal static string FormatCode(string text, string with, int length, bool left)
        {
            // caso tamanho da string maior que desejado , corta a mesma , evitando estouro no tamanho 
            if (text.Length > length)
                text = text.Substring(0, length);

            //Esse método já existe, é PadLeft e PadRight da string
            length -= text.Length;
            if (left)
            {
                for (int i = 0; i < length; ++i)
                {
                    text = with + text;
                }
            }
            else
            {
                for (int i = 0; i < length; ++i)
                {
                    text += with;
                }
            }
            return text;
        }
        public string ObterCodigoDaOcorrencia(BoletoNet.Boleto boleto)
        {
            return boleto.Remessa != null && !string.IsNullOrEmpty(boleto.Remessa.CodigoOcorrencia) ? FormatCode(boleto.Remessa.CodigoOcorrencia, 2) : TipoOcorrenciaRemessa.EntradaDeTitulos.Format();
        }
        internal static string FormatCode(string text, string with, int length)
        {
            return FormatCode(text, with, length, false);
        }

        internal static string FormatCode(string text, int length)
        {
            return text.PadLeft(length, '0');
        }
        private ListaBoletosRetorno ConverterContratoParcelaBoletoEmBoletos(List<ContratoParcelaBoleto> cpb)
        {
            ListaBoletosRetorno retorno = new ListaBoletosRetorno();
            try
            {
                List<BoletoNet.Boleto> boletos = new List<BoletoNet.Boleto>();
                foreach (var b in cpb)
                {
                    var _parametroCedente = ObterParametroPortador(b);
                    Cedente cedente = MontarCedente(_parametroCedente);
                    var _nrCarteira = DbRotinas.ConverterParaString(_parametroCedente.Carteira);
                    var _cdCendente = DbRotinas.ConverterParaString(_parametroCedente.Conta);
                    var _numeroBanco = (short)_parametroCedente.BancoNumero;
                    BoletoNet.Boleto boleto = new BoletoNet.Boleto(Convert.ToDateTime(b.DataVencimento), (decimal)b.ValorDocumento, _nrCarteira, _cdCendente, cedente);
                    boleto.NumeroDocumento = b.NumeroDocumento;

                    boleto.NossoNumero = b.NossoNumero != null ? DbRotinas.ConverterParaString(b.NossoNumero) : DbRotinas.ConverterParaString(b.Cod);

                    boleto.EspecieDocumento = DefinirEspecie((EnumBoleto.BancoCodigo)_numeroBanco);
                    boleto.Banco = new Banco(_numeroBanco);
                    boleto.DataDocumento = b.DataCadastro;
                    Sacado sacado = MontarSacado(b.ContratoParcela);
                    boleto.Sacado = sacado;
                    boleto.DataDesconto = boleto.DataVencimento;


                    //Validar a multa com otis.
                    boleto.JurosMora = ((decimal)b.MultaAtrasoPorcentagem / 10);

                    //***********************INFORMACOES QUE DEVERAO FORMAR UMA CHAVE NO ARQUIVO REMESSA**********************
                    boleto.Companhia = DbRotinas.FormatCode(b.Companhia, "0", 4, true);
                    boleto.CodCliente = DbRotinas.FormatCode(b.CodigoCliente, "0", 8, true);
                    boleto.TipoDocumento = b.ContratoParcela.TipoFatura;
                    boleto.Alterar = b.Alterar ?? false;
                    boleto = this.MontarInstruicao(boleto, DbRotinas.ConverterParaDecimal(b.ValorDocumento), DbRotinas.ConverterParaDatetime(b.DataVencimento), _parametroCedente);//OBTEM AS INSTRUÇÕES PARAMETRIZADAS

                    boletos.Add(boleto);
                }
                retorno.Boletos = boletos;
            }
            catch (Exception ex)
            {
                return new ListaBoletosRetorno(new Exception("Erro ao converter o CPBoleto em Boleto", ex));
            }

            return retorno;
        }

        public BoletoRetorno MontarBoleto(BoletoInfo informacao, ContratoParcela p)
        {
            BoletoRetorno retorno = new BoletoRetorno();
            try
            {
                var parcela = _ContratoParcela.FindById(p.Cod);
                var _paramPortador = ObterParametroPortador(parcela);
                TabelaBanco _paramBanco = _paramPortador.TabelaBanco;
                var _numeroBanco = (short)_paramBanco.Numero;
                Cedente cedente = MontarCedente(_paramPortador);

                BoletoNet.Boleto boleto = new BoletoNet.Boleto(Convert.ToDateTime(informacao.DataVencimento), informacao.ValorBoleto, informacao.Carteira, informacao.CedenteCodigo, cedente);
                boleto.NumeroDocumento = informacao.NumeroDocumento;
                boleto.NossoNumero = informacao.NossoNumero;
                boleto.Companhia = parcela.Companhia;
                Sacado sacado = MontarSacado(parcela);
                boleto.Sacado = sacado;

                //Instrucao_Itau instrucao = new Instrucao_Itau();
                //boleto.Instrucoes.Add(new Instrucao(341) { }); // PULA UMA LINHA


                boleto.EspecieDocumento = DefinirEspecie((EnumBoleto.BancoCodigo)_numeroBanco);
                boleto.Banco = new Banco(_numeroBanco);
                BoletoBancario boleto_bancario = new BoletoBancario();
                boleto_bancario.CodigoBanco = _numeroBanco;
                boleto_bancario.Boleto = boleto;
                boleto_bancario.Boleto.Valida();
                //boleto_bancario.Boleto.DataDocumento = (DateTime)boleto.DataDocumento;
                //boleto_bancario.Boleto.DataProcessamento = (DateTime)parcela.DataDocumento;
                boleto_bancario.Boleto.NumeroDocumento = parcela.FluxoPagamento;
                boleto_bancario.Boleto.LocalPagamento = "Pagar em qualquer banco até o vencimento";
                var _ultimoBoleto = parcela.ContratoParcelaBoletoes.Count > 0 ?
                                       parcela
                                       .ContratoParcelaBoletoes
                                           .Where(x => x.Status == (short)Enumeradores.StatusBoleto.Aprovado)
                                           .OrderByDescending(x => x.DataCadastro).First()
                                   : null;


                if (_ultimoBoleto != null)
                {
                    if (_ultimoBoleto.Alterar == true)
                    {
                        boleto_bancario.Boleto.ValorBoleto = DbRotinas.ConverterParaDecimal(_ultimoBoleto.ValorDocumentoAberto) + DbRotinas.ConverterParaDecimal(_ultimoBoleto.ValorJuros) + DbRotinas.ConverterParaDecimal(_ultimoBoleto.ValorMulta);
                        boleto_bancario.Boleto.ValorDesconto = DbRotinas.ConverterParaDecimal(_ultimoBoleto.Imposto) + DbRotinas.ConverterParaDecimal(_ultimoBoleto.ValorDesconto);
                        boleto_bancario.Boleto.ValorCobrado = DbRotinas.ConverterParaDecimal(_ultimoBoleto.ValorDocumento);
                    }
                    else
                    {
                        boleto_bancario.Boleto.ValorBoleto = DbRotinas.ConverterParaDecimal(_ultimoBoleto.ValorDocumento);
                        boleto_bancario.Boleto.ValorDesconto = DbRotinas.ConverterParaDecimal(_ultimoBoleto.ValorDesconto);
                        boleto_bancario.Boleto.ValorCobrado = DbRotinas.ConverterParaDecimal(_ultimoBoleto.ValorDocumento);
                    }

                }
                else
                {
                    boleto_bancario.Boleto.ValorBoleto = DbRotinas.ConverterParaDecimal(informacao.ValorBoleto);
                    boleto_bancario.Boleto.ValorCobrado = DbRotinas.ConverterParaDecimal(informacao.ValorBoleto);
                }
                boleto = this.MontarInstruicao(
                     boleto,
                     boleto_bancario.Boleto.ValorCobrado,
                     DbRotinas.ConverterParaDatetime(boleto_bancario.Boleto.DataVencimento),
                    _paramPortador
                );

                if (_ultimoBoleto != null)
                {
                    //MENSAGEM DIGITADA PELO ATENDENTE.
                    if (!String.IsNullOrEmpty(_ultimoBoleto.MensagemImpressa))
                    {
                        boleto.Instrucoes.Add(new Instrucao(_numeroBanco) { Descricao = "" });
                        boleto.Instrucoes.Add(new Instrucao(_numeroBanco) { Descricao = _ultimoBoleto.MensagemImpressa });
                    }
                }
                cedente.MostrarCNPJnoBoleto = true;
                boleto_bancario.MostrarEnderecoCedente = true;
                boleto_bancario.MostrarCodigoCarteira = true;
                boleto_bancario.OcultarReciboSacado = true;
                boleto_bancario.OcultarReciboSacadoOTIS = false;

                //boleto_bancario.MostrarComprovanteEntrega = false;

                retorno.OK = true;
                retorno.Boleto = boleto_bancario;
                return retorno;
            }
            catch (Exception ex)
            {
                return new BoletoRetorno(ex);
            }

        }

        private IEspecieDocumento DefinirEspecie(EnumBoleto.BancoCodigo banco)
        {
            switch (banco)
            {
                case EnumBoleto.BancoCodigo.Itau:
                    return new EspecieDocumento_Itau("99");
                case EnumBoleto.BancoCodigo.Santander:
                    return new EspecieDocumento_Santander("2");
                default:
                    {
                        return new EspecieDocumento_Itau("99");
                    }
            }

        }

        private CodigoBarra MontarCodigoBarra(ContratoParcelaBoleto BoletoParcela)
        {
            BoletoRetorno retorno = new BoletoRetorno();
            try
            {
                var _paramPortador = ObterParametroPortador(BoletoParcela);
                var _numeroBanco = _paramPortador.TabelaBanco.Numero;
                Cedente cedente = MontarCedente(_paramPortador);
                var _nossoNumero = Utils.FitStringLength(BoletoParcela.NossoNumero.ToString(), 8, 8, '0', 0, true, true, true);
                BoletoNet.Boleto boleto = new BoletoNet.Boleto(Convert.ToDateTime(BoletoParcela.DataVencimento), Convert.ToDecimal(BoletoParcela.ValorDocumento), cedente.Carteira, _nossoNumero, cedente);
                boleto.Banco = new Banco((int)_paramPortador.TabelaBanco?.Numero);
                boleto.ContaBancaria = new ContaBancaria(_paramPortador.Agencia, _paramPortador.AgenciaDigito, _paramPortador.Conta, _paramPortador.ContaDigito);
                BoletoBancario boleto_bancario = new BoletoBancario();
                boleto_bancario.CodigoBanco = (short)_numeroBanco;
                boleto.NumeroDocumento = BoletoParcela.NumeroDocumento;
                boleto_bancario.Boleto = boleto;
                boleto.Banco.FormataCodigoBarra(boleto);
                boleto.Banco.FormataLinhaDigitavel(boleto);

                return boleto.CodigoBarra;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar a linha digitavel.", ex);
            }

        }


        public Sacado MontarSacado(ContratoParcela parcela)
        {
            try
            {
                #region Sacado
                var contratoId = parcela.CodContrato;
                int _codCliente = (int)_dbContratoCliente.All().Where(x => x.CodContrato == contratoId).First().CodCliente;
                var _parametroSacado = _dbCliente.FindById(_codCliente);

                Endereco sacadoEndereco = new Endereco();
                sacadoEndereco.End = _parametroSacado.Endereco ?? "--";
                sacadoEndereco.Numero = _parametroSacado.EnderecoNumero ?? "--";
                sacadoEndereco.CEP = _parametroSacado.CEP ?? "--";
                sacadoEndereco.Bairro = _parametroSacado.Bairro ?? "--";
                sacadoEndereco.Cidade = _parametroSacado.Cidade ?? "--";
                sacadoEndereco.UF = _parametroSacado.UF ?? "--";
                sacadoEndereco.Complemento = _parametroSacado.EnderecoComplemento ?? "--";

                var cpf = DbRotinas.Descriptografar(_parametroSacado.CPF);
                if (cpf.Length < 11)
                {
                    int qtd = 11 - cpf.Length;
                    for (int i = 0; i < qtd; i++)
                    {
                        cpf = "0" + cpf;
                    }
                }
                if (cpf.Length > 11 && cpf.Length < 14)
                {
                    cpf = "0" + cpf;
                }
                var cliente = DbRotinas.Descriptografar(_parametroSacado.Nome);
                Sacado sacado = new Sacado(DbRotinas.ConverterParaString(cpf), DbRotinas.ConverterParaString(cliente), sacadoEndereco);
                #endregion
                return sacado;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro no Sacado .", ex);
            }
        }

        public int GerarNossoNumero(ContratoParcelaBoleto boleto)
        {
            try
            {
                Repository<ContratoParcela> _dbParcela = new Repository<ContratoParcela>();
                Repository<TabelaBanco> _dbBanco = new Repository<TabelaBanco>();
                ContratoParcela parcela = new ContratoParcela();
                if (boleto.ContratoParcela == null)
                {
                    parcela = _dbParcela.FindById(DbRotinas.ConverterParaInt(boleto.CodParcela));
                    boleto.ContratoParcela = parcela;
                }
                else
                {
                    parcela = boleto.ContratoParcela;
                }
                var _paramPortador = ObterParametroPortador(boleto);
                var _paramBanco = _dbBanco.FindById((int)_paramPortador.CodBanco);
                _paramBanco.NossoNumeroAtual++;
                _dbBanco.Update(_paramBanco);

                return DbRotinas.ConverterParaInt(_paramBanco.NossoNumeroAtual);
            }
            catch (Exception ex)
            {
                throw new Exception("erro ao criar nossonumero .", ex);
            }
        }

        public static TabelaBancoPortador ObterParametroPortador(ContratoParcela parcela)
        {
            Repository<TabelaBancoPortador> _dbParametroBoleto = new Repository<TabelaBancoPortador>();
            var _companhiaFiscal = parcela.CompanhiaFiscal;
            var parametros = _dbParametroBoleto.All();

            if (_companhiaFiscal == null)
            {
                parametros.Where(x => x.TabelaRegionalGrupo.CodRegionalCliente == parcela.CodRegionalCliente && x.TabelaBanco.CodCliente == parcela.InstrucaoPagamento).FirstOrDefault();
            }
            else
            {
                parametros.Where(x => x.CompanhiaFiscal == _companhiaFiscal);
                parametros.Where(x => x.CompanhiaFiscal == _companhiaFiscal && x.TabelaBanco.CodCliente == parcela.InstrucaoPagamento).FirstOrDefault();
            }

            return parametros.FirstOrDefault();
            
        }

        public static TabelaBancoPortador ObterParametroPortador(ContratoParcelaBoleto boleto)
        {
            Repository<TabelaBancoPortador> _dbParametroBoleto = new Repository<TabelaBancoPortador>();
            var parcela = ObterParcela((int)boleto.CodParcela);
            var _companhiaFiscal = parcela.CompanhiaFiscal;
            var parametros = _dbParametroBoleto.All();

            if (_companhiaFiscal == null)
            {
                parametros.Where(x => x.TabelaRegionalGrupo.CodRegionalCliente == parcela.CodRegionalCliente && x.TabelaBanco.CodCliente == parcela.InstrucaoPagamento).FirstOrDefault();
            }
            else
            {
                parametros.Where(x => x.CompanhiaFiscal == _companhiaFiscal && x.TabelaBanco.CodCliente == parcela.InstrucaoPagamento).FirstOrDefault();
            }

            return parametros.FirstOrDefault();

        }

        private static ContratoParcela ObterParcela(int CodParcela)
        {
            Repository<ContratoParcela> _dbParcela = new Repository<ContratoParcela>();
            return _dbParcela.FindById(CodParcela);
        }
        public static ContratoParcelaBoleto RegistrarUltimoHistorico(ContratoParcelaBoleto boleto)
        {
            Repository<ContratoParcelaBoletoHistorico> _repHistorico = new Repository<ContratoParcelaBoletoHistorico>();
            Repository<ContratoParcelaBoleto> _repBoleto = new Repository<ContratoParcelaBoleto>();
            var UltimoHistorico = _repHistorico.All().Where(x => x.CodBoleto == boleto.Cod).OrderByDescending(x => x.Cod).FirstOrDefault();
            boleto.CodHistorico = UltimoHistorico.Cod;

            return _repBoleto.Update(boleto);
        }

        public Cedente MontarCedente(TabelaBancoPortador portador)
        {
            try
            {
                #region Cedente
                //Cedente cedente = new Cedente("48079274000144", "ELEVADORES OTIS LTDA", "2938", "00589", "5");
                Cedente cedente = new Cedente(portador.CNPJ, portador.Portador, portador.Agencia, portador.Conta, portador.ContaDigito);
                cedente.Codigo = portador.Conta;
                cedente.Carteira = portador.Carteira.ToString();
                //cedente.Codigo = Convert.ToString(_parametroCedente.CodigoCedente);
                cedente.Endereco = new Endereco();
                cedente.Endereco.End = portador.Endereco != null ? portador.Endereco : "";
                cedente.Endereco.Numero = portador.EnderecoNumero != null ? portador.EnderecoNumero : "";
                cedente.Endereco.Complemento = null;
                cedente.Endereco.Bairro = portador.Bairro;
                cedente.Endereco.Cidade = portador.Cidade;
                cedente.Endereco.UF = portador.UF;
                cedente.Endereco.CEP = portador.CEP;

                #endregion
                return cedente;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao ler detalhe do arquivo de .", ex);
            }
        }

        public BoletoNet.Boleto MontarInstruicao(BoletoNet.Boleto boleto, decimal ValorDocumento, DateTime DataVencimento, TabelaBancoPortador _paramPortador)
        {
            try
            {

                // SE ESTIVER NA INSTRUCAO DO BANCO PORTADOR DEVE CALCULAR O JUROS E A MULTA APENAS PARA INFORMAR NO CAMPO Informacao de responsabilidade do beneficiador
                var _ValorJuros = ((((_paramPortador.JurosAoMesPorcentagem / 30) * 1) / 100) * ValorDocumento);
                var _ValorMulta = (ValorDocumento * (_paramPortador.MultaAtraso / 100));
                var _nrBanco = _paramPortador.TabelaBanco.Numero;
                if (_paramPortador.TabelaBancoPortadorInstrucaos != null)
                {
                    foreach (var instrucao in _paramPortador.TabelaBancoPortadorInstrucaos)
                    {

                        boleto.Instrucoes.Add(
                            new Instrucao((int)_nrBanco)
                            {
                                Descricao = instrucao.Descricao
                                                        .Replace("@DATA_VENCIMENTO", $"{DataVencimento:dd/MM/yyyy}")
                                                        .Replace("@VR_JUROS_POR_DIA", $"{DbRotinas.ConverterParaDecimal(_ValorJuros):C2}")
                                                        .Replace("@VR_MULTA_ATRASO", $"{DbRotinas.ConverterParaDecimal(_ValorMulta):C2}")
                                                        .Replace("@%_JUROS_POR_DIA", $"{DbRotinas.ConverterParaDecimal(_paramPortador.JurosAoMesPorcentagem):N2}%")
                                                        .Replace("@%_MULTA_ATRASO", $"{DbRotinas.ConverterParaDecimal(_paramPortador.MultaAtraso):N2}%")
                            }
                            );
                    }
                }
                return boleto;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao ler detalhe do arquivo de .", ex);
            }
        }

        internal static decimal? CalcularValorMulta(decimal? Valor, decimal? MultaAtraso, DateTime DataVencimento)
        {
            var DtNow = DateTime.Now;
            var DtFirstDayMonth = new DateTime(DtNow.Year, DtNow.Month, 1);
            if (DataVencimento.Date >= DtNow.Date)
            {
                return 0;
            }
            else if (DataVencimento.Date >= DtFirstDayMonth.Date)
            {
                return (Valor * (MultaAtraso / 100));
            }
            else
            {
                return (Valor * (MultaAtraso / 100));
            }
        }

        public BoletoInfo LerLinhaDigitavelCNAB400(string linha)
        {
            #region Disposição Boleto
            //            \*
            //            *------------------------------------------------------
            //              *Campo 1 - AAABC.CCDDX
            //              * AAA - Código do Banco
            //              * B   -Moeda
            //              * CCC - Carteira
            //              * DD - 2 primeiros números Nosso Número
            //              *X - DAC Campo 1(AAABC.CCDD) Mod10
            //              *
            //              * Campo 2 - DDDDD.DEEEEY
            //              * DDDDD.D - Restante Nosso Número
            //              *EEEE - 4 primeiros numeros do número do documento
            //              * Y       -DAC Campo 2(DDDDD.DEEEEY) Mod10
            //              *
            //              * Campo 3 - EEEFF.FFFGHZ
            //              * EEE - Restante do número do documento
            //              * FFFFF   -Código do Cliente
            //              * G       -DAC(Carteira / Nosso Numero(sem DAC) / Numero Documento / Codigo Cliente)
            //              * H - zero
            //              * Z - DAC Campo 3
            //              *
            //              *Campo 4 - K
            //              * K - DAC Código de Barras
            //              *
            //              * Campo 5 - UUUUVVVVVVVVVV
            //              * UUUU - Fator Vencimento
            //              * VVVVVVVVVV -Valor do Título
            //              */
            #endregion
            try
            {
                string linhaDigitavel = linha.Replace(" ", "").Replace(".", "");

                BoletoInfo detalhe = new BoletoInfo();

                var CodBanco = linhaDigitavel.Substring(0, 3);
                if ((EnumBoleto.BancoCodigo)DbRotinas.ConverterParaInt(CodBanco) == EnumBoleto.BancoCodigo.Itau)
                {
                    var Moeda = linhaDigitavel.Substring(3, 1);
                    detalhe.Carteira = linhaDigitavel.Substring(4, 3);
                    detalhe.NossoNumero = linhaDigitavel.Substring(7, 2) + linhaDigitavel.Substring(10, 6);
                    detalhe.NumeroDocumento = linhaDigitavel.Substring(16, 4) + linhaDigitavel.Substring(21, 3);
                    detalhe.CedenteNumeroBoleto = linhaDigitavel.Substring(16, 4) + linhaDigitavel.Substring(21, 3);
                    var CodigoCliente = linhaDigitavel.Substring(24, 5);
                    var fatorVencimento = DbRotinas.ConverterParaInt(linhaDigitavel.Substring(33, 4));
                    DateTime dtVencimento = BNet.UtilsCustom.ConvertFatorVencimentoEmData(fatorVencimento);
                    detalhe.DataVencimento = DbRotinas.ConverterParaString(dtVencimento.Date);
                    detalhe.ValorBoleto = BNet.UtilsCustom.ConverterValorStringEmDecimal(linhaDigitavel);
                }
                if ((EnumBoleto.BancoCodigo)DbRotinas.ConverterParaInt(CodBanco) == EnumBoleto.BancoCodigo.Santander)
                {
                    var Moeda = linhaDigitavel.Substring(3, 1);
                    detalhe.Carteira = linhaDigitavel.Substring(28, 3);
                    detalhe.NossoNumero = linhaDigitavel.Substring(13, 7) + linhaDigitavel.Substring(21, 6);
                    detalhe.NossoNumero = detalhe.NossoNumero.Substring(0, 12);
                    detalhe.NumeroDocumento = linhaDigitavel.Substring(16, 4) + linhaDigitavel.Substring(21, 3);
                    detalhe.CedenteNumeroBoleto = linhaDigitavel.Substring(16, 4) + linhaDigitavel.Substring(21, 3);
                    var CodigoCliente = linhaDigitavel.Substring(24, 5);
                    var fatorVencimento = DbRotinas.ConverterParaInt(linhaDigitavel.Substring(33, 4));
                    DateTime dtVencimento = BNet.UtilsCustom.ConvertFatorVencimentoEmData(fatorVencimento);
                    detalhe.DataVencimento = DbRotinas.ConverterParaString(dtVencimento.Date);
                    detalhe.ValorBoleto = BNet.UtilsCustom.ConverterValorStringEmDecimal(linhaDigitavel);
                }


                return detalhe;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao ler detalhe do arquivo de .", ex);
            }
        }

        public class BoletoRetorno
        {
            public BoletoRetorno()
            {
                this.OK = false;
            }
            public BoletoRetorno(Exception ex)
            {
                this.OK = false;
                this.Mensagem = ex.Message;
            }
            public bool OK { get; set; }
            public string Mensagem { get; set; }
            public BoletoBancario Boleto { get; set; }
        }

        public class ListaBoletosRetorno
        {
            public ListaBoletosRetorno()
            {
                this.OK = false;
            }
            public ListaBoletosRetorno(Exception ex)
            {
                this.OK = false;
                this.Mensagem = ex.Message;
            }
            public bool OK { get; set; }
            public string Mensagem { get; set; }
            public List<BoletoNet.Boleto> Boletos { get; set; }
        }


        public class RemessaRetorno
        {
            public RemessaRetorno()
            {
                this.OK = false;
            }
            public RemessaRetorno(Exception ex)
            {
                this.OK = false;
                this.Mensagem = ex.Message;
            }
            public bool OK { get; set; }
            public string Mensagem { get; set; }
            public byte[] Arquivo { get; set; }
            public StringBuilder ConteudoRemessa { get; set; }
            public List<ContratoParcelaBoleto> BoletosProcessado { get; set; }
            public int CodUsuario { get; set; }
            public int CodLogArquivo { get; set; }
            public string NomeArquivo { get; set; }
            public int LoteAtual { get; set; }
            public int CodBancoPortadorGrupo { get; set; }
        }
        public class RetornoLinhaDigitavel
        {
            public RetornoLinhaDigitavel()
            {
                this.OK = false;
            }
            public RetornoLinhaDigitavel(Exception ex)
            {
                this.OK = false;
                this.Mensagem = ex.Message;
                this.LinhaDigitavel = "";
            }
            public bool OK { get; set; }
            public string Mensagem { get; set; }
            public string LinhaDigitavel { get; set; }
        }

        public BoletoBancario GerandoBoletoTeste()
        {
            var _numeroBanco = (short)341;
            Cedente cedente = new Cedente("48079274000144", "ELEVADORES OTIS LTDA", "2938", "00589", "5");
            BoletoNet.Boleto boleto = new BoletoNet.Boleto(DateTime.Today, (decimal)1.0, "109", null, cedente);
            boleto.NumeroDocumento = "0000100";

            boleto.NossoNumero = "0000100";
            Endereco sacadoEndereco = new Endereco();
            sacadoEndereco.End = "PRAÇA SILVIO ROMERO, 55";
            sacadoEndereco.CEP = "01310-000";
            sacadoEndereco.Bairro = "TATUAPÉ";
            sacadoEndereco.Cidade = "SÃO PAULO";
            sacadoEndereco.UF = "SP";
            Sacado sacado = new Sacado("05808792008719", "JUNIX INFORMÁTICA", sacadoEndereco);
            boleto.Sacado = sacado;


            EspecieDocumento_Itau especie = new EspecieDocumento_Itau("99");
            boleto.EspecieDocumento = especie;

            boleto.Instrucoes.Add(new Instrucao(341) { Descricao = "DOCUMENTO DE TESTE" });
            boleto.Banco = new Banco(_numeroBanco);
            BoletoBancario boleto_bancario = new BoletoBancario();
            boleto_bancario.CodigoBanco = _numeroBanco;
            boleto_bancario.Boleto = boleto;

            boleto_bancario.Boleto.Valida();
            boleto_bancario.Boleto.LocalPagamento = "PAGAVEL EM QUALQUER BANCO";
            boleto_bancario.MostrarCodigoCarteira = true;
            boleto_bancario.OcultarReciboSacado = true;
            boleto_bancario.MostrarEnderecoCedente = true;

            boleto_bancario.MostrarComprovanteEntrega = false;

            return boleto_bancario;
        }


        public BoletoRetorno CriarBoleto(ContratoParcela parcela)
        {
            BoletoRetorno retorno = new BoletoRetorno();
            try
            {
                var _parametroCedente = _dbParametroBoleto.All().FirstOrDefault();
                BoletoHelpers _boleto = new BoletoHelpers((int)_parametroCedente.CodBanco);

                retorno = _boleto.Itau(
                            DbRotinas.ConverterParaString(_parametroCedente.Cod),
                            _parametroCedente.Portador,
                            _parametroCedente.Agencia,
                            _parametroCedente.Conta,
                            _parametroCedente.ContaDigito,
                            _parametroCedente.CNPJ,
                            true,
                            "109",//ceCarteira
                            new Endereco() { End = "Rua Teste" },
                            parcela.FluxoPagamento,
                            "000",//boNossoNumero
                            parcela.DataVencimento,
                            (decimal)parcela.ValorAberto,
                            "1",//boCodigoEspecie
                            new List<Instrucao>(),
                            true, //boMostrarCodigoCarteira
                            true, //boOcultarReciboSacado
                            true, //boMostrarEnderecoCedente
                            true, //boMostrarComprovanteEntrega
                            "Fulano da Silva",
                            "0000000000",
                            new Endereco() { End = "teste" }
                            );
                return retorno;
            }
            catch (Exception ex)
            {
                return new BoletoRetorno(ex);
            }

        }


        public static decimal? CalcularValorJurosSimples(decimal ValorPresente, decimal Taxa, DateTime DataVencimento, DateTime DataBoleto)
        {
            var DtNow = DateTime.Now;
            var QtdDay = (DataBoleto - DataVencimento).Days;
            if (QtdDay <= 0)
            {
                return 0;
            }
            else
            {
                return decimal.Round(((((Taxa / 30) * QtdDay) / 100) * ValorPresente), 2, MidpointRounding.AwayFromZero);
            }
        }

        public static decimal? CalcularValorJurosComposto(decimal ValorPresente, decimal Taxa, int Periodo)
        {
            return ((ValorPresente * (decimal)Math.Pow(1.0 + (DbRotinas.ConverterParaDouble(Taxa) / 100), Periodo)) - ValorPresente);
        }

        internal static decimal? CalcularValorMulta(decimal? Valor, decimal? MultaAtraso, DateTime DataVencimento, DateTime DataBoleto)
        {

            if (DataBoleto.Date <= DataVencimento.Date)
            {
                return 0;
            }
            else if (DataBoleto.Date > DataVencimento.Date)
            {
                return decimal.Round(DbRotinas.ConverterParaDecimal(Valor * (MultaAtraso / 100)), 2, MidpointRounding.AwayFromZero);
            }
            else
            {
                return decimal.Round(DbRotinas.ConverterParaDecimal(Valor * (MultaAtraso / 100)), 2, MidpointRounding.AwayFromZero);
            }
        }

    }


}
