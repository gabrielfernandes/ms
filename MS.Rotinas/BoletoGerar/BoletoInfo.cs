﻿using BoletoNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Rotinas.BoletoGerar
{
  
    public class BoletoInfo
    {
        public List<Instrucao> _instrucoes = new List<Instrucao>();
        //BOLETO
        public string DataVencimento { get; set; }
        public string NossoNumero { get; set; }
        public string NumeroDocumento { get; set; }
        public string Carteira { get; set; }
        public string LocalPagamento { get; set; }

        //CEDENTE
        public string CedenteCodigo { get; set; }
        public string CedenteNumeroBoleto { get; set; }
        public string CedenteCPF { get; set; }
        public string CedenteNome { get; set; }
        public string CedenteAgencia { get; set; }
        public string CedenteDigitoAgencia { get; set; }
        public string CedenteConta { get; set; }
        public string CedenteDigitoConta { get; set; }
        public string CedenteEndereco { get; set; }
        public string CedenteNumero { get; set; }
        public string CedenteComplemento { get; set; }
        public string CedenteBairro { get; set; }
        public string CedenteCidade { get; set; }
        public string CedenteUF { get; set; }
        public string CedenteCEP { get; set; }
        //SACADOR
        public string SacadoCPF { get; set; }
        public string SacadoCpfCnpj { get; set; }
        public string SacadoNome { get; set; }
        public string SacadoEnd { get; set; }
        public string SacadoEmail { get; set; }
        public string SacadoEndereco { get; set; }
        public string SacadoNumero { get; set; }
        public string SacadoBairro { get; set; }
        public string SacadoCidade { get; set; }
        public string SacadoCEP { get; set; }
        public string SacadoUF { get; set; }

        //BANCO
        public short NumeroBanco { get; set; }
        public string DescricaoBoleto { get; set; }
        public decimal ValorCobrado { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal ValorBoleto { get; set; }

    }
}
