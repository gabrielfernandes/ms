﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;

namespace Cobranca.Rotinas.BoletoGerar
{
    public class BoletoRotinas
    {
        public Repository<ContratoParcela> _dbParcela = new Repository<ContratoParcela>();
        public Dados _qry = new Dados();
        public void GerarBoleto(List<ContratoParcelaBoleto> boletos, int usuarioLogadoId, string _observacao)
        {
            foreach (var boleto in boletos)
            {
                ContratoParcela _parcela = boleto.ContratoParcela;

                var _qtdDias = DbRotinas.ConverterParaInt(_parcela.DataEmissaoFatura - DateTime.Now);

                ValidarQtdDiasAtraso(_qtdDias, boleto.Companhia);//VALIDAR DIAS DE REGISTRO BOLETO

                CalcularValorOriginal(boleto, _parcela); //VALOR DOCUMENTO COM DESCONTO JA CALCULADO

                var _paramBanco = BoletoUtils.ObterParametroPortador(boleto);
                //***************************** VALORES PREVISTO **************************************//
                boleto.JurosAoMesPorcentagemPrevisto = _paramBanco.JurosAoMesPorcentagem;
                boleto.MultaAtrasoPorcentagemPrevisto = _paramBanco.MultaAtraso;
                boleto.ValorJurosPrevisto = BoletoUtils.CalcularValorJurosSimples((decimal)boleto.ValorDocumentoOriginal, (decimal)_paramBanco.JurosAoMesPorcentagem, _parcela.DataVencimento, (DateTime)boleto.DataVencimento);
                boleto.ValorMultaPrevisto = BoletoUtils.CalcularValorMulta(boleto.ValorDocumentoOriginal, _paramBanco.MultaAtraso, _parcela.DataVencimento, (DateTime)boleto.DataVencimento);
                boleto.ValorDocumentoPrevisto = (decimal)boleto.ValorJurosPrevisto + (decimal)boleto.ValorMultaPrevisto + ((decimal)_parcela.ValorAberto);

                //***************************** VALORES SOLICITACAO **************************************//
                boleto.ValorJuros = BoletoUtils.CalcularValorJurosSimples((decimal)boleto.ValorDocumentoOriginal, (decimal)boleto.JurosAoMesPorcentagem, _parcela.DataVencimento, (DateTime)boleto.DataVencimento);
                boleto.ValorMulta = BoletoUtils.CalcularValorMulta((decimal)boleto.ValorDocumentoOriginal, boleto.MultaAtrasoPorcentagem, _parcela.DataVencimento, (DateTime)boleto.DataVencimento);
                boleto.ValorDocumento = (decimal)boleto.ValorJuros + (decimal)boleto.ValorMulta + (decimal)boleto.ValorDocumentoOriginal;
                boleto.ValorDocumento = boleto.ValorDocumento - boleto.ValorDesconto;

                Enumeradores.EtapaBoleto Etapa = this.VerificarTipoEtapaSolicitacao(boleto);

                //***************************** VERIFICAR EM QUAL ETAPA O BOLETO DEVERA IR **************************************//
                this.ConfigurarEtapaBoleto(boleto, Etapa, usuarioLogadoId);
            }

            using (junix_cobrancaContext cx = new junix_cobrancaContext())
            {
                foreach (var boleto in boletos)
                {

                    if (boleto.Cod == 0)
                    {
                        boleto.ContratoParcela = null;
                        cx.ContratoParcelaBoletoes.Add(boleto);
                    }
                    else
                    {
                        var boletoExistente = cx.ContratoParcelaBoletoes.Where(x => !x.DataExcluido.HasValue && x.Cod == boleto.Cod).FirstOrDefault();
                        boletoExistente = boleto;
                    }

                    if (boleto.Status == (short)Enumeradores.StatusBoleto.Aprovado)
                    {
                        ProcessarNossoNumero(boleto);
                        var _codigoBarra = BoletoUtils.GerarCodigoBarra(boleto);
                        var parcela = _dbParcela.FindById((int)boleto.CodParcela);


                        //SE FOR NOVO BOLETO DEVE CRIAR NA TABELA DE CONTRATOPARCELA O NOSSO NUMERO
                        if (!DbRotinas.ConverterParaBool(boleto.Alterar))
                        {
                            parcela.NossoNumero = boleto.NossoNumero;
                        }
                        parcela.LinhaDigitavel = _codigoBarra.LinhaDigitavel;

                        _dbParcela.Update(parcela);//ATUALIZA PARCELA

                        TratarBoletosAntigosDaParcela(cx, boleto);
                    }
                }


                cx.SaveChanges();
                List<ContratoParcelaBoletoHistorico> historicos = new List<ContratoParcelaBoletoHistorico>();
                foreach (var item in boletos)
                {
                    Enumeradores.EtapaBoleto Etapa = this.VerificarTipoEtapaSolicitacao(item);
                    ContratoParcelaBoletoHistorico BoletoHistorico = new ContratoParcelaBoletoHistorico
                    {
                        CodUsuario = usuarioLogadoId,
                        CodSintese = this.ObterSinteseEtapa(Etapa),
                        CodBoleto = item.Cod,
                        Observacao = _observacao,
                        DataCadastro = DateTime.Now
                    };
                    historicos.Add(BoletoHistorico);
                }
                cx.ContratoParcelaBoletoHistoricoes.AddRange(historicos);
                cx.SaveChanges();

                foreach (var item in boletos)
                    _qry.ComputaHistoricoBoleto(item.Cod);
            }
        }


        /// <summary>
        /// Método responsável por gerar o nosso número de acordo com a configuração (TabelaBanco)
        /// </summary>
        /// <param name="boleto">boleto</param>
        /// <returns>boleto</returns>
        public ContratoParcelaBoleto ProcessarNossoNumero(ContratoParcelaBoleto boleto)
        {
            Repository<TabelaConstrutora> _repConstrutora = new Repository<TabelaConstrutora>();
            BoletoUtils _boletoUtil = new BoletoUtils();
            ContratoParcela parcela = new ContratoParcela();
            if (boleto.ContratoParcela == null)
                parcela = _dbParcela.FindById(DbRotinas.ConverterParaInt(boleto.CodParcela));
            else
                parcela = boleto.ContratoParcela;

            var param = BoletoUtils.ObterParametroPortador(boleto);

            //VERIFICA SE O BOLETO JA POSSUI O NOSSO NUMERO.
            if (parcela.ValorParcela == parcela.ValorAberto && parcela.NossoNumero > 0 && parcela.DataEmissaoFatura > param.DataInicialRegistroBoleto)
            {
                boleto.Alterar = true;
                boleto.NossoNumero = parcela.NossoNumero;
            }
            else
            {
                boleto.NossoNumero = _boletoUtil.GerarNossoNumero(boleto);
            }

            boleto.ContratoParcela = null;
            return boleto;
        }

        public Nullable<int> ObterSinteseEtapa(Enumeradores.EtapaBoleto tipo)
        {
            Repository<TabelaParametroSintese> _parametroSintese = new Repository<TabelaParametroSintese>();
            var sinteseSolcitar = (from p in _parametroSintese.All()
                                   where p.Etapa == (short)tipo
                                   select p.CodSintesePara).ToList();
            if (sinteseSolcitar.Count() > 0)
            {
                return sinteseSolcitar.First();
            }
            else return null;
        }

        private void TratarBoletosAntigosDaParcela(junix_cobrancaContext cx, ContratoParcelaBoleto boleto)
        {
            var boletoes = cx
                        .ContratoParcelaBoletoes
                           .Where(x =>
                                  !x.DataExcluido.HasValue
                                  && boleto.Cod != x.Cod
                                  && boleto.CodParcela == x.CodParcela
                                );
            List<short> list = new List<short>();
            list.AddRange(new short[] { (short)Enumeradores.StatusBoleto.Cancelado, (short)Enumeradores.StatusBoleto.Reprovado, (short)Enumeradores.StatusBoleto.Obsoleto });
            foreach (var c in boletoes)
            {
                if (!list.Contains((short)c.Status))
                {
                    c.Status = (short)Enumeradores.StatusBoleto.Obsoleto;
                }
            }
        }

        private static void TratarBoletosAntigosDaParcela(ContratoParcelaBoleto boleto)
        {

        }

        // <summary>
        /// Método calcula o valor orinal do boleto.
        /// </summary>
        private static void CalcularValorOriginal(ContratoParcelaBoleto boleto, ContratoParcela parcela)
        {
            boleto.ValorDocumentoOriginal = parcela.ValorAberto == parcela.ValorParcela ? ((decimal)parcela.ValorAberto - DbRotinas.ConverterParaDecimal(parcela.Imposto)) : parcela.ValorAberto;
        }

        /// <summary>
        /// Método para validar o boleto e seus dias. Por definição boletos com + de 360 dias de atraso deverá ser somente emitidos como um novo boleto
        /// </summary>
        /// <param > Linha 1</param>
        /// <param name="QtdDias" > Teste para imprimir informação</param>
        /// <returns>new exeption com msg</returns>
        /// <remarks>Deve ser implementado somenta para verificar boletos ja existentes (alterar)</remarks>
        /// <example>(dataVencimento - DateTime.Now).TotalDays</example>    
        private void ValidarQtdDiasAtraso(int _qtdDias, string _companhia)
        {
            string _companhiaOTIS = "00400";
            string _companhiaSERAL = "00405";
            if (_qtdDias >= 360 && _companhia == _companhiaOTIS)
            {
                throw new Exception("Esta fatura está com mais de 360 dias do seu vencimento e deverá ser emitida somente como um novo boleto.");
            }
            else if (_qtdDias >= 120 && _companhia == _companhiaSERAL)
            {
                throw new Exception("Esta fatura está com mais de 120 dias do seu vencimento e deverá ser emitida somente como um novo boleto.");
            }
        }

        private Enumeradores.EtapaBoleto VerificarTipoEtapaSolicitacao(ContratoParcelaBoleto Boleto)
        {
            Enumeradores.EtapaBoleto tipoSolicitacao = Enumeradores.EtapaBoleto.AtendenteSolicitaComDesconto;
            //SE O BOLETO FOR APENAS PRORROGAÇÂO DO VENCIMENTO O MESMO SERA APROVADO DIRETO
            if (Boleto.JurosAoMesPorcentagemPrevisto == Boleto.JurosAoMesPorcentagem && Boleto.MultaAtrasoPorcentagemPrevisto == Boleto.MultaAtrasoPorcentagem && Boleto.ValorDesconto <= 0)
            {
                tipoSolicitacao = Enumeradores.EtapaBoleto.AtendenteSolicitaProrrogacao;
            }
            else
            {
                tipoSolicitacao = Enumeradores.EtapaBoleto.AtendenteSolicitaComDesconto;
            }
            return tipoSolicitacao;
        }

        /// <summary>
        /// Método para verificar em qual etapa o boleto será processado.
        /// </summary>
        private void ConfigurarEtapaBoleto(ContratoParcelaBoleto boleto, Enumeradores.EtapaBoleto Etapa, int UsuarioLogado)
        {
            if (Etapa == Enumeradores.EtapaBoleto.AtendenteSolicitaProrrogacao)
            {
                boleto.Status = (short)Enumeradores.StatusBoleto.Aprovado;
                boleto.DataStatus = DateTime.Now;
                boleto.DataAprovacao = DateTime.Now;
                boleto.CodUsuarioStatus = UsuarioLogado;
            }
            else
            {
                boleto.Status = (short)Enumeradores.StatusBoleto.SolicitacaoAnalista;
                boleto.DataStatus = DateTime.Now;
                boleto.CodUsuarioStatus = UsuarioLogado;
            }
        }
    }
}
