﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Linq;



namespace Cobranca.Rotinas
{
    public class DeParaModeloAcao
    {

        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<ContratoParcela> _dbContratoParcela = new Repository<ContratoParcela>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<ContratoCliente> _dbContratoCliente = new Repository<ContratoCliente>();
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public string DeParaCampos(string ModeloAcao, int CodContrato = 0, int CodCliente = 0)
        {
            ContratoDb contratoRotinas = new ContratoDb();
            var Modelo = "";
            var CL = _dbCliente.FindById(CodCliente);
            var CC = _dbContratoCliente.All().Where(x => x.CodCliente == CL.Cod && (x.Contrato.ValorAtraso > 0 || x.Contrato.DiasAtraso > 0));
            var contratos = from p in CC.ToList() select new { cod = p.Contrato.Cod, nrContrato = p.Contrato.NumeroContrato };
            List<int> listOfContratoIds = new List<int>(contratos.Select(c => c.cod));
            List<ContratoParcela> parcelas = contratoRotinas.ListarParcelaEmAtraso(listOfContratoIds);
            List<ContratoParcela> parcelasaVencer = contratoRotinas.ListarParcelaAVencer(listOfContratoIds);
            if (!string.IsNullOrEmpty(ModeloAcao) && CL != null)
            {
                Modelo = (ModeloAcao.Replace("@NOME_COMPLETO", CL.Nome)
                                    .Replace("@PRIMEIRO_NOME", CL.Nome.Substring(0).Split(' ').First())
                                    .Replace("@CPF_CNPJ_COMPRADOR", CL.CPF)
                                    .Replace("@NUMERO_CONTRATOS", String.Join<string>(",", contratos.Select(x => x.nrContrato)))
                                    .Replace("@QTD_PARCELAS_ATRASO", DbRotinas.ConverterParaString(parcelas.Count()))
                                    .Replace("@QTD_PARCELAS_A_VENCER", DbRotinas.ConverterParaString(parcelasaVencer.Count()))
                                    .Replace("@VALOR_PARCELA_A_VENCER", String.Format("{0:C}", (from p in parcelasaVencer orderby p.DataVencimento descending select p.ValorParcela).First()))
                                    .Replace("@DATA_PARCELA_A_VENCER", String.Format("{0:dd/MM/yyyy}", (from p in parcelasaVencer orderby p.DataVencimento descending select p.DataVencimento).First()))
                                    .Replace("@VALOR_PARCELA_MAIS_ANTIGA", String.Format("{0:C}", (from p in parcelas orderby p.DataVencimento descending select p.ValorParcela).First()))
                                    .Replace("@DATA_PARCELA_MAIS_ANTIGA", String.Format("{0:dd/MM/yyyy}", (from p in parcelas orderby p.DataVencimento descending select p.DataVencimento).First()))
                                    .Replace("@NUMERO_PARCELAS", String.Join<string>(",", parcelas.Select(x => x.FluxoPagamento)))
                                    .Replace("@SALDO_DEVEDOR", String.Format("{0:C}", parcelas.Sum(p => p.ValorParcela)))
                                    .Replace("@SALDO_ATUALIZADO", String.Format("{0:C}", parcelas.Sum(p => p.ValorAtualizado.HasValue ? p.ValorAtualizado : p.ValorParcela)))
                                    );
            }
            return Modelo;
        }

        public string DeParaCamposPorCliente(string ModeloAcao, int CodContrato = 0, int CodCliente = 0)
        {
            ContratoDb contratoRotinas = new ContratoDb();
            var Modelo = "";
            var CL = _dbCliente.FindById(CodCliente);
            var CC = _dbContratoCliente.All().Where(x => x.CodCliente == CL.Cod && (x.Contrato.ValorAtraso > 0 || x.Contrato.DiasAtraso > 0));
            var contratos = from p in CC.ToList() select new { cod = p.Contrato.Cod, nrContrato = p.Contrato.NumeroContrato };
            List<int> listOfContratoIds = new List<int>(contratos.Select(c => c.cod));
            List<ContratoParcela> parcelas = contratoRotinas.ListarParcelaEmAtraso(listOfContratoIds);
            List<ContratoParcela> parcelasaVencer = contratoRotinas.ListarParcelaAVencer(listOfContratoIds);
            if (!string.IsNullOrEmpty(ModeloAcao) && CL != null)
            {
                Modelo = (ModeloAcao.Replace("@NOME_COMPLETO", CL.Nome)
                                    .Replace("@PRIMEIRO_NOME", CL.Nome.Substring(0).Split(' ').First())
                                    .Replace("@CPF_CNPJ_COMPRADOR", CL.CPF)
                                    .Replace("@NUMERO_CONTRATOS", String.Join<string>(",", contratos.Select(x => x.nrContrato)))
                                    .Replace("@QTD_PARCELAS_ATRASO", DbRotinas.ConverterParaString(parcelas.Count()))
                                    .Replace("@QTD_PARCELAS_A_VENCER", DbRotinas.ConverterParaString(parcelasaVencer.Count()))
                                    .Replace("@VALOR_PARCELA_A_VENCER", String.Format("{0:C}", (from p in parcelasaVencer orderby p.DataVencimento descending select p.ValorParcela).First()))
                                    .Replace("@DATA_PARCELA_A_VENCER", String.Format("{0:dd/MM/yyyy}", (from p in parcelasaVencer orderby p.DataVencimento descending select p.DataVencimento).First()))
                                    .Replace("@VALOR_PARCELA_MAIS_ANTIGA", String.Format("{0:C}", (from p in parcelas orderby p.DataVencimento descending select p.ValorParcela).First()))
                                    .Replace("@DATA_PARCELA_MAIS_ANTIGA", String.Format("{0:dd/MM/yyyy}", (from p in parcelas orderby p.DataVencimento descending select p.DataVencimento).First()))
                                    .Replace("@NUMERO_PARCELAS", String.Join<string>(",", parcelas.Select(x => x.FluxoPagamento)))
                                    .Replace("@SALDO_DEVEDOR", String.Format("{0:C}", parcelas.Sum(p => p.ValorParcela)))
                                    .Replace("@SALDO_ATUALIZADO", String.Format("{0:C}", parcelas.Sum(p => p.ValorAtualizado.HasValue ? p.ValorAtualizado : p.ValorParcela)))
                                    );
            }
            return Modelo;

        }

        public string DeParaCamposTeste(string ModeloAcao)
        {
            var dados = _dbConstrutora.All().FirstOrDefault();
            var Modelo = (ModeloAcao.Replace("@NOME_COMPRADOR", "Teste Comprador")
                                    .Replace("@NUMERO_CONTRATO", "123456789")
                                    .Replace("@DESCRICAO_UNIDADE", "Empreendimento Junix - Bloco Junix - 0101 Junix")
                                    .Replace("@EMPRENDIMENTO - BLOCO - UNIDADE", "Empreendimento Junix - Bloco Junix - 0101 Junix")
                                    .Replace("@CPF_CNPJ_COMPRADOR", "010.010.010-10")
                                    .Replace("@SALDO_DEVEDOR", "R$ 600,00")
                                    .Replace("@NOME_AVALISTA", "Teste Avalista")
                                    .Replace("@CPF_CNPJ_AVALISTA", "101.101.101-01")
                                    .Replace("@DATA_VENCIMENTO_PARCELA", string.Format("{0:dd/MM/yyyy}", DateTime.Today))
                                    .Replace("@SALDO_DEVEDOR", string.Format("{0:dd/MM/yyyy}", DateTime.Today))
                                    .Replace("@VALOR_PARCELA", "R$ 550,00")
                                    );
            return Modelo;
        }

    }
}
