﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Rotinas.Modelos
{
    public class ContratoRepasse
    {                
        public string TipoVenda { get; set; }
        public DateTime? PrevisaoSGI { get; set; }
        public string CodSAP { get; internal set; }
        public string StatusRepasse { get; internal set; }

        public bool OK { get; set; }
        public string Mensagem { get; set; }
    }
}
