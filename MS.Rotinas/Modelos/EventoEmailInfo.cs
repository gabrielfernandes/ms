﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Rotinas.Modelos
{
    public class EventoEmailInfo
    {
        public int CodContrato { get; set; }
        public int CodParcela { get; set; }
        public int Cod { get; set; }
        public int CodClientePrincipal { get; set; }
        public int CodRegional { get; set; }
        public int CodTipoCobrancaAcao { get; set; }
    }
}
