﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Rotinas
{
    public class ServicoFinanceiroTenda
    {
        private string p1;
        private string p2;
        private string p3;
        private string p4;
        private string p5;
        private string p6;
        private string p7;
        private string p8;
        private string p9;
        private string p10;
        private string p11;

        public string AbaSegundaViaBoleto { get; set; }
        public string AbaPosicaoFinanceira { get; set; }
        public string AbaParcelasEmAtraso { get; set; }
        public string AbaAntecipacaoParcelas { get; set; }
        public string AbaMemoriaCalculo { get; set; }
        public string AbaIR_QAS { get; set; }
        public string SaldoDevedor { get; set; }
        public string AbaParcelaServico { get; set; }

        public ServicoFinanceiroTenda(
            string CodCliente,
            string CodUnidadeEconomica,
            string DescUnidadeEconomica,
            string CodTorre,
            string CodUnidadeCliente,
            string CodUnidadeSap,
            string CodEmpresa,
            string Documento,
            string DescEmpresa,
            string Usuario,
            string Email
            )
        {
            CodCliente = string.Format("{0:0000000000}", Convert.ToInt32(CodCliente));
            CodUnidadeEconomica = CodUnidadeEconomica.ToString();
            DescUnidadeEconomica = DescUnidadeEconomica.ToString();
            CodTorre = string.Format("{0:00000000}", Convert.ToInt32(CodTorre));
            CodUnidadeCliente = ConverterCodigoGafisa(CodUnidadeCliente).ToString();
            CodUnidadeSap = string.Format("{0:00000000}", Convert.ToInt32(CodUnidadeSap));
            //CodEmpresa = string.Format("{0:0000}", ConverterCodigoGafisa(CodEmpresa));
            CodEmpresa = string.Format("{0:0000}", 70);
            Documento = "99999999999";
            DescEmpresa = "JUNIX";
            Usuario = Usuario.ToString();
            Email = Email.ToString();

            string parametros = string.Format(
                "{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}",
                CodCliente
                , CodUnidadeEconomica
                , DescUnidadeEconomica
                , CodTorre
                , CodUnidadeCliente
                , CodUnidadeSap
                , CodEmpresa
                , Documento
                , DescEmpresa
                , Usuario
                , Email);

            string parametrosCriptorgrafados = CriptografarString(parametros);

            CarregarAbaSegundaViaBoleto(parametrosCriptorgrafados);
            CarregarAbaPosicaoFinanceira(parametrosCriptorgrafados);
            CarregarAbaParcelasEmAtraso(parametrosCriptorgrafados);
            CarregarAbaAntecipacaoParcelas(parametrosCriptorgrafados);
            CarregarAbaMemoriaCalculo(parametrosCriptorgrafados);
            CarregarAbaIR_QAS(parametrosCriptorgrafados);
            CarregarSaldoDevedor(parametrosCriptorgrafados);
            CarregarAbaParcelaServico(parametrosCriptorgrafados);

        }




        private string ConverterCodigoGafisa(object valor)
        {
            string codigoResposta = valor.ToString();
            try
            {
                int codigoInteiro = Convert.ToInt32(valor);
                if (codigoInteiro > 0)
                    return string.Format("{0:0000}", Convert.ToInt32(codigoInteiro));
            }
            catch (Exception ex)
            {

            }
            return codigoResposta;
        }

        private void CarregarAbaSegundaViaBoleto(string parametrosCriptorgrafados)
        {
            string url = System.Configuration.ConfigurationSettings.AppSettings["Gafisa.Sistemas.Financeiro.SegundaViadeBoleto_PROD"].ToString();
            if (!url.Contains("http://"))
            {
                url = "http://" + url;
            }
            AbaSegundaViaBoleto = string.Format("{0}?param={1}", url, parametrosCriptorgrafados);
        }
        private void CarregarAbaPosicaoFinanceira(string parametrosCriptorgrafados)
        {
            string url = System.Configuration.ConfigurationSettings.AppSettings["Gafisa.Sistemas.Financeiro.PosicaoFinanceira_PROD"].ToString();
            if (!url.Contains("http://"))
            {
                url = "http://" + url;
            }
            AbaPosicaoFinanceira = string.Format("{0}?param={1}", url, parametrosCriptorgrafados);
        }
        private void CarregarAbaAntecipacaoParcelas(string parametrosCriptorgrafados)
        {
            string url = System.Configuration.ConfigurationSettings.AppSettings["Gafisa.Sistemas.Financeiro.AntecipacaoParcelas_PROD"].ToString();
            if (!url.Contains("http://"))
            {
                url = "http://" + url;
            }
            AbaAntecipacaoParcelas = string.Format("{0}?param={1}", url, parametrosCriptorgrafados);
        }

        private void CarregarAbaParcelasEmAtraso(string parametrosCriptorgrafados)
        {
            string url = System.Configuration.ConfigurationSettings.AppSettings["Gafisa.Sistemas.Financeiro.ParcelasAtraso_PROD"].ToString();
            if (!url.Contains("http://"))
            {
                url = "http://" + url;
            }
            AbaParcelasEmAtraso = string.Format("{0}?param={1}", url, parametrosCriptorgrafados);
        }
        private void CarregarAbaMemoriaCalculo(string parametrosCriptorgrafados)
        {
            string url = System.Configuration.ConfigurationSettings.AppSettings["Gafisa.Sistemas.Financeiro.MemoriaCalculo_PROD"].ToString();
            if (!url.Contains("http://"))
            {
                url = "http://" + url;
            }
            AbaMemoriaCalculo = string.Format("{0}?param={1}", url, parametrosCriptorgrafados);
        }
        private void CarregarAbaIR_QAS(string parametrosCriptorgrafados)
        {
            string url = System.Configuration.ConfigurationSettings.AppSettings["Gafisa.Sistemas.Financeiro.IR_PROD"].ToString();
            if (!url.Contains("http://"))
            {
                url = "http://" + url;
            }
            AbaIR_QAS = string.Format("{0}?param={1}", url, parametrosCriptorgrafados);
        }
        private void CarregarSaldoDevedor(string parametrosCriptorgrafados)
        {
            string url = System.Configuration.ConfigurationSettings.AppSettings["Gafisa.Sistemas.Financeiro.SaldoDevedor_QAS"].ToString();
            if (!url.Contains("http://"))
            {
                url = "http://" + url;
            }
            SaldoDevedor = string.Format("{0}?param={1}", url, parametrosCriptorgrafados);
        }
        private void CarregarAbaParcelaServico(string parametrosCriptorgrafados)
        {
            string url = System.Configuration.ConfigurationSettings.AppSettings["Gafisa.Sistemas.Financeiro.ParcelaServico_PROD"].ToString();
            if (!url.Contains("http://"))
            {
                url = "http://" + url;
            }
            AbaParcelaServico = string.Format("{0}?param={1}", url, parametrosCriptorgrafados);
        }

        private string CriptografarString(string parametros)
        {

            try
            {
                Criptografia RotinaCriptografia = new Criptografia();
                return RotinaCriptografia.Encrypt(parametros, "OPTOUT0000000001");
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
        private string DescriptografarString(string parametros)
        {

            try
            {
                Criptografia RotinaCriptografia = new Criptografia();
                return RotinaCriptografia.Decrypt(parametros, "OPTOUT0000000001");
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
    }
}
