﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoletoNet;
using System.IO;
using Cobranca.Rotinas.BNet;
using Cobranca.Rotinas.Net.Util;

namespace Cobranca.Rotinas.Remessa
{
    public class RemessaCustom : AbstractBanco, IBanco
    {
        public void GerarArquivoRemessa(string numeroConvenio, IBanco banco, Cedente cedente, List<BoletoNet.Boleto> boletos, Stream arquivo, int numeroArquivoRemessa, TipoArquivo tpArquivo)
        {
            try
            {
                int numeroRegistro = 2;
                int numeroRegistroLote = 1;
                string strline;
                decimal vlTitulosTotal = 0;
                int qtdTitulosTotal = Db.Rotinas.DbRotinas.ConverterParaInt(boletos.Count());
                Codigo = banco.Codigo;
                //GERAR O HEADER (CABEÇALHO)
                StreamWriter incluiLinha = new StreamWriter(arquivo, Encoding.GetEncoding("ISO-8859-1"));
                strline = GerarHeaderRemessa(numeroConvenio, cedente, tpArquivo, numeroArquivoRemessa);
                incluiLinha.WriteLine(strline);

                // GERAR O HEADER LOTE (CABEÇALHO ADICIONAL)
                strline = GerarHeaderLoteRemessa(numeroConvenio, cedente, tpArquivo, numeroArquivoRemessa);
                incluiLinha.WriteLine(strline);

                //PREENCHER OS DETALHES
                foreach (BoletoNet.Boleto boleto in boletos)
                {
                    boleto.Banco = banco;

                    //VERIFICA SE O BOLETO É ALTERACAO
                    if (boleto.Alterar)
                    {
                        //GERAR O DETALHE P
                        strline = GerarDetalheAlterarcaoDadosSegmentoPRemessa240(boleto, numeroRegistroLote, "");
                        incluiLinha.WriteLine(strline);
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE Q
                        strline = GerarDetalheAlterarcaoDadosSegmentoQRemessa240(boleto, numeroRegistroLote, "");
                        incluiLinha.WriteLine(strline);
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE P
                        strline = GerarDetalheAlterarcaoVencimentoSegmentoPRemessa240(boleto, numeroRegistroLote, "");
                        incluiLinha.WriteLine(strline);
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE Q
                        strline = GerarDetalheAlterarcaoVencimentoSegmentoQRemessa240(boleto, numeroRegistroLote, "");
                        incluiLinha.WriteLine(strline);
                        numeroRegistroLote++;
                        numeroRegistro++;

                    }
                    else
                    {
                        //GERAR O DETALHE P
                        strline = GerarDetalheSegmentoPRemessa240(boleto, numeroRegistroLote, "");
                        incluiLinha.WriteLine(strline);
                        numeroRegistroLote++;
                        numeroRegistro++;

                        //GERAR O DETALHE Q
                        strline = GerarDetalheSegmentoQRemessa(boleto, numeroRegistroLote, tpArquivo);
                        incluiLinha.WriteLine(strline);
                        numeroRegistroLote++;
                        numeroRegistro++;
                        qtdTitulosTotal++;
                    }
                }

                numeroRegistro++;
                //GERAR O TRAILER LOTE (RODAPE LOTE)
                strline = GerarTrailerLoteRemessa(numeroRegistro, tpArquivo, cedente, vlTitulosTotal, qtdTitulosTotal);
                incluiLinha.WriteLine(strline);
                numeroRegistro++;

                //GERAR O TRAILER (RODAPE)
                strline = GerarTrailerCustomRemessa(numeroRegistro, tpArquivo, cedente, vlTitulosTotal, qtdTitulosTotal);
                incluiLinha.WriteLine(strline);

                incluiLinha.Close();
                incluiLinha.Dispose();
                incluiLinha = null;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar arquivo remessa.", ex);
            }
        }

        #region HEADER
        /// <summary>
        /// HEADER do arquivo CNAB
        /// Gera o HEADER do arquivo remessa de acordo com o lay-out informado
        /// </summary>
        public override string GerarHeaderRemessa(string numeroConvenio, Cedente cedente, TipoArquivo tipoArquivo, int numeroArquivoRemessa)
        {
            try
            {
                string _header = " ";

                base.GerarHeaderRemessa("0", cedente, tipoArquivo, numeroArquivoRemessa);

                switch (tipoArquivo)
                {

                    case TipoArquivo.CNAB240:
                        _header = GerarHeaderRemessaCNAB240(cedente, numeroArquivoRemessa);
                        break;
                    case TipoArquivo.CNAB400:
                        _header = GerarHeaderRemessaCNAB400(0, cedente, numeroArquivoRemessa);
                        break;
                    case TipoArquivo.Outro:
                        throw new Exception("Tipo de arquivo inexistente.");
                }

                return _header;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do HEADER do arquivo de REMESSA.", ex);
            }
        }

        /// <summary>
        /// HEADER do arquivo CNAB
        /// Gera o HEADER do arquivo remessa de acordo com o lay-out informado
        /// </summary>
        public string GerarHeaderLoteRemessa(string numeroConvenio, Cedente cedente, TipoArquivo tipoArquivo, int numeroArquivoRemessa)
        {
            try
            {
                string _header = " ";

                switch (tipoArquivo)
                {
                    case TipoArquivo.CNAB240:
                        _header = GerarHeaderLoteRemessaCNAB240(cedente, numeroArquivoRemessa);
                        break;
                    case TipoArquivo.CNAB400:
                        _header = "";
                        break;
                    case TipoArquivo.Outro:
                        throw new Exception("Tipo de arquivo inexistente.");
                }

                return _header;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do HEADER do arquivo de REMESSA.", ex);
            }
        }

        /// <summary>
        ///POS INI/FINAL    TAMANHO Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
        ///------------------------------------------------------------------------------------------------------------------------------------------------
        ///001 - 003	    3	    Z0012	        Nº do Banco da Companhia. 	        CODIGO DO BCO NA COMPENSACAO                
        ///004 - 007	    4	    S0001	        UDV - Valor Definido pelo Us.       LOTE DE SERVICO                             0000
        ///008 - 008        01	    Z0065           Tipo de Linha do Formatador         REGISTRO HEADER DO ARQUIVO                   
        ///009 - 017        09	    S0002           Em branco                           USO EXCLUSIVO NEXXERA                        
        ///018 - 018        01	    Z0001           Cód.Pessoa Fís./Jurídica            TIPO DE INSCRICAO DA EMPRESA                29739737000102
        ///019 - 032        14	    S0001           UDV - Valor Definido pelo Us.       CNPJ EMPRESA DEBITADA                        
        ///033 - 052        20	    S0002           Em branco                           CODIGO DO CONVENIO DO BANCO                  
        ///053 - 057        05	    Z0003           Agência Bancária da Companhia       NUMERO DA AGENCIA DEBITADA                   
        ///058 - 058        01	    Z0005           Dígito da Agência Banc.da Cia       DIGITO VERIFICADOR AGENCIA                   
        ///059 - 070        12	    Z0004           Nº da Cta Bancária da Cia           NUMERO DA C/C DEBITADA                       
        ///071 - 071        01	    S0002           Em branco                           DIGITO VERIFICADOR CONTA CORRENTE            
        ///072 - 072        01	    Z0006           Dígito da Conta Banc.da Cia         DAC DA AGENCIA/CTA DEBITADA                  
        ///073 - 102        30	    Z0009           Nome da Companhia                   NOME DA EMPRESA                              
        ///103 - 110        08	    Z0013           Nome do Banco da Companhia          NOME DO BANCO                                
        ///111 - 132        22	    S0002           Em branco                           NOME DO BANCO                               NEXXERA
        ///133 - 142        10	    S0001           UDV - Valor Definido pelo Us.       COMPLEMENTO DE REGISTROS                    1
        ///143 - 143        01	    S0001           UDV - Valor Definido pelo Us.       CODIGO REMESSA/RETORNO                       
        ///144 - 151        08	    Z0010           Data Criação do Arq.Formatado       DATA DA GERACAO DO ARQUIVO                   
        ///152 - 157        06	    Z0011           Hora Criação do Arq.Formatado       HORA DE GERACAO DO ARQUIVO                   
        ///158 - 164        07	    Z0058           Nº do Arquivo Bancário              NUMERO SEQUENCIAL DO ARQUIVO                
        ///165 - 167        03	    S0001           UDV - Valor Definido pelo Us.       NUMERO DA VERSAO DO LAYOUT                  020
        ///168 - 172        05	    S0001           UDV - Valor Definido pelo Us.       DENSIDADE DA GRAVACAO DO ARQUIVO            01600 
        ///173 - 191        19	    S0002           Em branco                           PARA USO RESERVADO DO BANCO                  
        ///192 - 211        20	    S0002           Em branco                           PARA USO RESERVADO DA EMPRESA                
        ///212 - 221        10	    S0002           Em branco                           OBSERVACAO DO LAYOUT DO BANCO                
        ///222 - 240        19      S0002	        Em branco                           USO EXCLUSIVO NEXXERA
        /// </summary>
        public string GerarHeaderRemessaCNAB240(Cedente cedente, int numeroArquivoRemessa)
        {
            try
            {
                string header = "341";  // Nº do Banco da Companhia. / CODIGO DO BCO NA COMPENSACAO 
                header += "0000";    // UDV - Valor Definido pelo Us./ LOTE DE SERVICO / 0000
                header += "0";   // Tipo de Linha do Formatador         REGISTRO HEADER DO ARQUIVO                   
                header += Utils.FormatCode("", " ", 9);   // Em branco / USO EXCLUSIVO NEXXERA
                header += (cedente.CPFCNPJ.Length == 11 ? "1" : "2");   // Cód.Pessoa Fís./Jurídica / TIPO DE INSCRICAO DA EMPRESA / 29739737000102                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                header += Utils.FormatCode(cedente.CPFCNPJ, "0", 14, true);  // UDV - Valor Definido pelo Us. / CNPJ EMPRESA DEBITADA                        
                header += Utils.FormatCode("", " ", 20);
                header += "0";    // Em branco / CODIGO DO CONVENIO DO BANCO                                                                                                       
                header += Utils.FormatCode(cedente.ContaBancaria.Agencia, " ", 4, true); // Agência Bancária da Companhia / NUMERO DA AGENCIA DEBITADA                                          
                header += " ";   // Dígito da Agência Banc.da Cia / DIGITO VERIFICADOR AGENCIA                   
                header += "0000000";
                header += Utils.FormatCode(cedente.ContaBancaria.Conta, "0", 5, true);// Nº da Cta Bancária da Cia / NUMERO DA C/C DEBITADA      
                header += " ";  // Em branco DIGITO VERIFICADOR CONTA CORRENTE                                                                                                         
                header += Utils.FormatCode(String.IsNullOrEmpty(cedente.ContaBancaria.DigitoConta) ? " " : cedente.ContaBancaria.DigitoConta, " ", 1, true);// Dígito da Conta Banc.da Cia         DAC DA AGENCIA/CTA DEBITADA                                   
                header += Utils.FitStringLength(cedente.Nome, 30, 30, ' ', 0, true, true, false);// Nome da Companhia / NOME DA EMPRESA                                 
                header += Utils.FormatCode("BCO ITAU", " ", 8); // Nome do Banco da Companhia / NOME DO BANCO                                                                                         
                header += Utils.FormatCode("", " ", 22);//Nome do Banco da Companhia / NOME DO BANCO
                header += Utils.FormatCode("NEXXERA", " ", 10);// Em branco / NOME DO BANCO / NEXXERA                                                               
                header += "1";// UDV - Valor Definido pelo Us. / COMPLEMENTO DE REGISTROS / 1                                                                                                                                              
                header += DateTime.Now.ToString("ddMMyyyyHHmmss");// Data Criação do Arq.Formatado / DATA DA GERACAO DO ARQUIVO // Hora Criação do Arq.Formatado / HORA DE GERACAO DO ARQUIVO                 
                header += Utils.FormatCode(numeroArquivoRemessa.ToString(), "0", 7, true); //  Nº do Arquivo Bancário / NUMERO SEQUENCIAL DO ARQUIVO / 
                header += "020"; // Nº do Arquivo Bancário / NUMERO DA VERSAO DO LAYOUT / 020
                header += "01600"; // UDV - Valor Definido pelo Us. / NUMERO DA VERSAO DO LAYOUT / 01600
                header += Utils.FormatCode("", " ", 19);//Em branco / PARA USO RESERVADO DO BANCO /         
                header += Utils.FormatCode("", " ", 20);//Em branco / PARA USO RESERVADO DA EMPRESA /
                header += Utils.FormatCode("", " ", 10);//Em branco / OBSERVACAO DO LAYOUT DO BANCO /
                header += Utils.FormatCode("", " ", 19);//Em branco / USO EXCLUSIVO NEXXERA /
                header = Utils.SubstituiCaracteresEspeciais(header);


                return header;                                                                                                                                             // Em branco                           USO EXCLUSIVO NEXXERA

            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar HEADER do arquivo de remessa do CNAB240.", ex);
            }
        }

        public string GerarHeaderRemessaCNAB400(int numeroConvenio, Cedente cedente, int numeroArquivoRemessa)
        {
            try
            {
                string complemento = new string(' ', 294);
                string _header;

                _header = "01REMESSA01COBRANCA       ";
                _header += Utils.FitStringLength(cedente.ContaBancaria.Agencia, 4, 4, '0', 0, true, true, true);
                _header += "00";
                _header += Utils.FitStringLength(cedente.ContaBancaria.Conta, 5, 5, '0', 0, true, true, true);
                _header += Utils.FitStringLength(cedente.ContaBancaria.DigitoConta, 1, 1, ' ', 0, true, true, false);
                _header += "        ";
                _header += Utils.FitStringLength(cedente.Nome, 30, 30, ' ', 0, true, true, false).ToUpper();
                _header += "341";
                _header += "BANCO ITAU SA  ";
                _header += DateTime.Now.ToString("ddMMyy");
                _header += complemento;
                _header += "000001";

                _header = Utils.SubstituiCaracteresEspeciais(_header);

                return _header;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar HEADER do arquivo de remessa do CNAB400.", ex);
            }
        }

        /// <summary>
        ///POS INI/FINAL    TAM     Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
        ///------------------------------------------------------------------------------------------------------------------------------------------------
        ///001 - 003        03	    Z0012           Nº do Banco da Companhia            CODIGO DO BANCO NA COMPENSACAO	 
        ///004 - 007        04	    S0001           UDV - Valor Definido pelo           Us.LOTE IDENTIFICACAO DE PAGTOS             0001
        ///008 - 008        01	    S0001           UDV - Valor Definido pelo Us. 	    REGISTRO HEADER DE LOTE	                    1
        ///009 - 009        01	    S0001           UDV - Valor Definido pelo Us. 	    TIPO DE OPERACAO                            R
        ///010 - 011        02	    S0001           UDV - Valor Definido pelo Us. 	    TIPO DE PAGTO                               01
        ///012 - 013        02	    S0002           Em branco                           USO EXCLUSIVO NEXXERA	 
        ///014 - 016        03	    S0001           UDV - Valor Definido pelo Us. 	    N.VERSAO DO LAYOUT DO LOTE                  010
        ///017 - 017        01	    S0002           Em branco USO                       EXCLUSIVO NEXXERA	 
        ///018 - 018        01	    Z0001           Cód. Pessoa Fís./Jurídica           TIPO INSCRICAO EMPRESA DEBITADA
        ///019 - 033        15	    S0001           UDV - Valor Definido pelo Us. 	    CNPJ EMPRESA DEBITADA   29739737000102
        ///034 - 053        20	    S0002           Em branco                           CODIGO CONVERNIO BANCO	 
        ///054 - 058        05	    Z0003           Agência Bancária da Companhia       NUMERO AGENCIA DEBITADA	 
        ///059 - 059        01	    Z0005           Dígito da Agência Banc.da Cia       DIGITO VERIFICADOR DA AGENCIA	 
        ///060 - 071        12	    Z0004           Nº da Cta Bancária da Cia           NUMERO DE C/C DEBITADA	 
        ///072 - 072        01	    S0002           Em branco                           DIGITO VERIFICADOR DA CONTA
        ///073 - 073        01	    Z0006           Dígito da Conta Banc.da Cia         DAC DA AGENCIA/CTA DEBITADA	 
        ///074 - 103        30	    Z0009           Nome da Companhia                   NOME DA EMPRESA
        ///104 - 143        40	    S0002           Em branco                           MENSAGEM 1	 
        ///144 - 183        40	    S0002           Em branco                           MENSAGEM 2	 
        ///184 - 191        08	    Z0058           Nº do Arquivo Bancário              NUMERO DO RETORNO
        ///192 - 199        08	    Z0010           Data Criação do Arq.Formatado       DATA GERACAO DO ARQUIVO
        ///200 - 207        08	    S0003           Zero                                DATA CREDITO 
        ///208 - 214        07	    S0002           Em branco                           CODIGO MODELO PERSONALIZADO	 
        ///215 - 240        26	    S0002           Em branco                           USO EXCLUSIVO NEXXERA
        /// </summary>
        public string GerarHeaderLoteRemessaCNAB240(Cedente cedente, int numeroArquivoRemessa)
        {
            try
            {
                string header = Utils.FormatCode(Codigo.ToString(), "0", 3, true);               //Nº do Banco da Companhia          | CODIGO DO BANCO NA COMPENSACAO	       |  
                header += Utils.FormatCode("0001", "0", 4, true);                                //UDV - Valor Definido pelo         | Us.LOTE IDENTIFICACAO DE PAGTOS         |    0001
                header += Utils.FormatCode("1", " ", 1, true);                                   //UDV - Valor Definido pelo Us. 	 | REGISTRO HEADER DE LOTE	               |    1
                header += Utils.FormatCode("R", " ", 1, true);                                   //UDV - Valor Definido pelo Us. 	 | TIPO DE OPERACAO                        |    R
                header += Utils.FormatCode("01", "0", 2, true);                                  //UDV - Valor Definido pelo Us. 	 | TIPO DE PAGTO                           |    01
                header += Utils.FormatCode("", " ", 2);                                          //Em branco                         | USO EXCLUSIVO NEXXERA	               | 
                header += Utils.FormatCode("010", " ", 3);                                       //UDV - Valor Definido pelo Us. 	 | N.VERSAO DO LAYOUT DO LOTE              |    010
                header += Utils.FormatCode("", " ", 1);                                          //Em branco USO                     | EXCLUSIVO NEXXERA	                   | 
                header += (cedente.CPFCNPJ.Length == 11 ? "1" : "2");                            //Cód. Pessoa Fís./Jurídica         | TIPO INSCRICAO EMPRESA DEBITADA         | 
                header += Utils.FormatCode(cedente.CPFCNPJ, "0", 15, true);                      //UDV - Valor Definido pelo Us. 	 | CNPJ EMPRESA DEBITADA   29739737000102  | 
                header += Utils.FormatCode("", " ", 20);                                         //Em branco                         | CODIGO CONVERNIO BANCO	               | 
                header += Utils.FormatCode(cedente.ContaBancaria.Agencia, "0", 5, true);         //Agência Bancária da Companhia     | NUMERO AGENCIA DEBITADA	               | 
                header += Utils.FormatCode("", " ", 1);                                          //Dígito da Agência Banc.da Cia     | DIGITO VERIFICADOR DA AGENCIA	       | 
                header += Utils.FormatCode(cedente.ContaBancaria.Conta, "0", 12, true);          //Nº da Cta Bancária da Cia         | NUMERO DE C/C DEBITADA	               | 
                header += Utils.FormatCode("", " ", 1);                                          //Em branco                         | DIGITO VERIFICADOR DA CONTA             | 
                header += Utils.FormatCode(String.IsNullOrEmpty(cedente.ContaBancaria.DigitoConta) ? " " : cedente.ContaBancaria.DigitoConta, " ", 1); //Dígito da Conta Banc.da Cia       | DAC DA AGENCIA/CTA DEBITADA	           | 
                header += Utils.FitStringLength(cedente.Nome, 30, 30, ' ', 0, true, true, false);//Nome da Companhia                 | NOME DA EMPRESA                         | 
                header += Utils.FormatCode("", " ", 40);                                         //Em branco   | MENSAGEM 1 | 
                header += Utils.FormatCode("", " ", 40);                                         //Em branco                         | MENSAGEM 2	                           | 
                header += Utils.FormatCode(numeroArquivoRemessa.ToString(), "0", 8, true);       //Nº do Arquivo Bancário            | NUMERO DO RETORNO                       | 
                header += DateTime.Now.ToString("ddMMyyyy");                                     //Data Criação do Arq.Formatado     | DATA GERACAO DO ARQUIVO                 | 
                header += Utils.FormatCode("", "0", 8);                                          //Zero                              | DATA CREDITO                            | 
                header += Utils.FormatCode("", " ", 7);                                          //Em branco                         | CODIGO MODELO PERSONALIZADO	           | 
                header += Utils.FormatCode("", " ", 26);                                         //Em branco                         | USO EXCLUSIVO NEXXERA                   | 
                header = Utils.SubstituiCaracteresEspeciais(header);

                return header;                                                                                                                                             // Em branco                           USO EXCLUSIVO NEXXERA

            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar HEADER do arquivo de remessa do CNAB240.", ex);
            }
        }

        #endregion HEADER




        #region DETALHE

        /// <summary>
        /// DETALHE do arquivo CNAB
        /// Gera o DETALHE do arquivo remessa de acordo com o lay-out informado
        /// </summary>
        public override string GerarDetalheRemessa(BoletoNet.Boleto boleto, int numeroRegistro, TipoArquivo tipoArquivo)
        {
            try
            {
                string _detalhe = " ";

                //base.GerarDetalheRemessa(boleto, numeroRegistro, tipoArquivo);

                switch (tipoArquivo)
                {
                    case TipoArquivo.CNAB240:
                        _detalhe = GerarDetalheRemessaCNAB240(boleto, numeroRegistro, tipoArquivo);
                        break;
                    case TipoArquivo.CNAB400:
                        _detalhe = GerarDetalheRemessaCNAB400(boleto, numeroRegistro, tipoArquivo);
                        break;
                    case TipoArquivo.Outro:
                        throw new Exception("Tipo de arquivo inexistente.");
                }

                return _detalhe;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do DETALHE arquivo de REMESSA.", ex);
            }
        }

        /// <summary>
        ///POS INI/FINAL	DESCRIÇÃO	                   A/N	TAM	DEC	CONTEÚDO	NOTAS
        ///--------------------------------------------------------------------------------
        ///001 - 003	Código do Banco na compensação	    N	003		341	
        ///004 - 007	Lote de serviço	                    N	004		Nota 5 
        ///008 - 008	Registro Detalhe de Lote            N	001     3
        ///009 - 013	Número Sequencial Registro Lote     N	005		Nota 6
        ///014 - 014	Código Segmento Reg. Detalhe   	    A	001		A
        ///015 – 017	Código da Instrução p/ Movimento    N	003		Nota 7
        ///018 - 020	Código da Câmara de Compensação     N	003	    000
        ///021 - 023	Código do Banco                     N	003	    341
        ///024 – 024	Complemento de Registros	        N	001		0
        ///025 – 028	Número Agencia Debitada       	    N	004	    
        ///029 - 029	Complemento de Registros            A	001		Brancos
        ///030 - 036	Complemento de Registros            N	007		0000000
        ///037 - 041	Número da Conta Debitada            N	005     
        ///042 - 042	Complemento de Registros            A	001     Brancos
        ///043 - 043    Dígito Verificador da AG/Conta      N   001     
        ///044 - 073    Nome do Debitado                    A   030     
        ///074 - 088    Nr. do Docum. Atribuído p/ Empresa  A   015     Nota 8
        ///089 - 093    Complemento de Registros            A   005     Brancos
        ///094 - 101    Data para o Lançamento do Débito    N   008     DDMMAAAA
        ///102 - 104    Tipo da Moeda                       A   005     Nota 9
        ///105 - 119	Quantidade da Moeda ou IOF          N	015		Nota 10
        ///120 - 134	Valor do Lançamento p/ Débito       N	015		Nota 10
        ///135 - 154	Complemento de Registros            A	020		Brancos
        ///155 - 162	Complemento de Registros            A	008		Brancos
        ///163 - 177	Complemento de Registros            N	015	    Brancos
        ///178 - 179	Tipo do Encargo por dia de Atraso 	N	002		Nota 12
        ///180 - 196    Valor do Encargo p/ dia de Atraso   N   017     Nota 12
        ///197 - 212	Info. Compl. p/ Histórico C/C       A	016		Nota 13
        ///213 - 216    Complemento de Registros            A   004     Brancos
        ///217 - 230    No. de Insc. do Debitado(CPF/CNPJ)  N   014     
        ///231 - 240    Cód. Ocr. para Retorno              A   010     Brancos
        /// </summary>
        public string GerarDetalheRemessaCNAB240(BoletoNet.Boleto boleto, int numeroRegistro, TipoArquivo tipoArquivo)
        {
            try
            {
                string detalhe = Utils.FormatCode(Codigo.ToString(), "0", 3, true);
                detalhe += Utils.FormatCode("", "0", 4, true);
                detalhe += "3";
                detalhe += Utils.FormatCode("", "0", 5, true);
                detalhe += "A";
                detalhe += Utils.FormatCode("", "0", 3, true);
                detalhe += "000";
                detalhe += Utils.FormatCode(Codigo.ToString(), "0", 3, true);
                detalhe += "0";
                detalhe += Utils.FormatCode("", "0", 4, true);
                detalhe += " ";
                detalhe += Utils.FormatCode("", "0", 7);
                detalhe += Utils.FormatCode("", "4", 5, true);
                detalhe += " ";
                detalhe += Utils.FormatCode("", "0", 1);
                detalhe += Utils.FitStringLength(boleto.Sacado.Nome, 30, 30, ' ', 0, true, true, false);
                detalhe += Utils.FormatCode(boleto.NossoNumero, " ", 15);
                detalhe += Utils.FormatCode("", " ", 5);
                detalhe += DateTime.Now.ToString("ddMMyyyy");
                detalhe += Utils.FormatCode("", " ", 3);
                detalhe += Utils.FormatCode("", "0", 15, true);
                detalhe += Utils.FormatCode("", "0", 15, true);
                detalhe += Utils.FormatCode("", " ", 20);
                detalhe += Utils.FormatCode("", " ", 8);
                detalhe += Utils.FormatCode("", " ", 15);
                detalhe += Utils.FormatCode("", "0", 2, true);
                detalhe += Utils.FormatCode("", "0", 17, true);
                detalhe += Utils.FormatCode("", " ", 16);
                detalhe += Utils.FormatCode("", " ", 4);
                detalhe += Utils.FormatCode(boleto.Cedente.CPFCNPJ, "0", 14, true);
                detalhe += Utils.FormatCode("", " ", 10);
                detalhe = Utils.SubstituiCaracteresEspeciais(detalhe);
                return detalhe;
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao gerar DETALHE do arquivo CNAB240.", e);
            }
        }

        public string GerarDetalheRemessaCNAB400(BoletoNet.Boleto boleto, int numeroRegistro, TipoArquivo tipoArquivo)
        {
            try
            {
                // GerarDetalheRemessa(boleto, numeroRegistro, tipoArquivo);

                // USO DO BANCO - Identificação da operação no Banco (posição 87 a 107)
                string identificaOperacaoBanco = new string(' ', 21);
                string usoBanco = new string(' ', 10);
                string nrDocumento = new string(' ', 25);
                string _detalhe;

                _detalhe = "1";

                // Tipo de inscrição da empresa

                // Normalmente definem o tipo (CPF/CNPJ) e o número de inscrição do cedente. 
                // Se o título for negociado, deverão ser utilizados para indicar o CNPJ/CPF do sacador 
                // (cedente original), uma vez que os cartórios exigem essa informação para efetivação 
                // dos protestos. Para este fim, também poderá ser utilizado o registro tipo “5”.
                // 01 - CPF DO CEDENTE
                // 02 - CNPJ DO CEDENTE
                // 03 - CPF DO SACADOR
                // 04 - CNPJ DO SACADOR
                // O arquivo gerado pelo aplicativo do Banco ITAÚ, sempre atriubuiu 04 para o tipo de inscrição da empresa

                if (boleto.Cedente.CPFCNPJ.Length <= 11)
                    _detalhe += "01";
                else
                    _detalhe += "02";
                _detalhe += Utils.FitStringLength(boleto.Cedente.CPFCNPJ.ToString(), 14, 14, '0', 0, true, true, true);
                _detalhe += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Agencia.ToString(), 4, 4, '0', 0, true, true, true);
                _detalhe += "00";
                _detalhe += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Conta.ToString(), 5, 5, '0', 0, true, true, true);
                _detalhe += Utils.FitStringLength(boleto.Cedente.ContaBancaria.DigitoConta.ToString(), 1, 1, ' ', 0, true, true, false);
                _detalhe += "    "; // Complemento do registro - 4 posições em branco

                // Código da instrução/alegação a ser cancelada

                // Deve ser preenchido na remessa somente quando utilizados, na posição 109-110, os códigos de ocorrência 35 – 
                // Cancelamento de Instrução e 38 – Cedente não concorda com alegação do sacado. Para os demais códigos de 
                // ocorrência este campo deverá ser preenchido com zeros. 
                //Obs.: No arquivo retorno será informado o mesmo código da instrução cancelada, e para o cancelamento de alegação 
                // de sacado não há retorno da informação.

                // Por enquanto o objetivo é apenas gerar o arquivo de remessa e não utilizar o arquivo para enviar instruções
                // para títulos que já estão no banco, portanto o campo será preenchido com zeros.
                _detalhe += "0000";

                //MODIFICADO PARA EXIBIR O FLUXO 
                _detalhe += Utils.FitStringLength(boleto.NumeroDocumento, 25, 25, ' ', 0, true, true, false); //Identificação do título na empresa
                _detalhe += Utils.FitStringLength(boleto.NossoNumero, 8, 8, '0', 0, true, true, true);
                // Quantidade de moeda variável - Preencher com zeros se a moeda for REAL
                // O manual do Banco ITAÚ não diz como preencher caso a moeda não seja o REAL
                if (boleto.Moeda == 9)
                    _detalhe += "0000000000000";

                _detalhe += Utils.FitStringLength(boleto.Carteira, 3, 3, '0', 0, true, true, true);
                _detalhe += Utils.FitStringLength(identificaOperacaoBanco, 21, 21, ' ', 0, true, true, true);
                // Código da carteira
                if (boleto.Moeda == 9)
                    _detalhe += "I"; //O código da carteira só muda para dois tipos, quando a cobrança for em dólar

                _detalhe += Utils.FitStringLength(boleto.Remessa.CodigoOcorrencia, 2, 2, '0', 0, true, true, true);
                //_detalhe += "01"; // Identificação da ocorrência - 01 REMESSA
                _detalhe += Utils.FitStringLength(boleto.NumeroDocumento, 10, 10, ' ', 0, true, true, false);
                _detalhe += boleto.DataVencimento.ToString("ddMMyy");
                _detalhe += Utils.FitStringLength(boleto.ValorBoleto.ToString("0.00").Replace(",", ""), 13, 13, '0', 0, true, true, true);
                _detalhe += "341";
                _detalhe += "00000"; // Agência onde o título será cobrado - no arquivo de remessa, preencher com ZEROS

                _detalhe += Utils.FitStringLength(EspecieDocumento.ValidaCodigo(boleto.EspecieDocumento).ToString(), 2, 2, '0', 0, true, true, true);
                _detalhe += "N"; // Identificação de título, Aceito ou Não aceito

                //A data informada neste campo deve ser a mesma data de emissão do título de crédito 
                //(Duplicata de Serviço / Duplicata Mercantil / Nota Fiscal, etc), que deu origem a esta Cobrança. 
                //Existindo divergência, na existência de protesto, a documentação poderá não ser aceita pelo Cartório.
                _detalhe += boleto.DataDocumento.ToString("ddMMyy");

                switch (boleto.Instrucoes.Count)
                {
                    case 0:
                        _detalhe += "0000"; // Jéferson (jefhtavares) o banco não estava aceitando esses campos em Branco
                        break;
                    case 1:
                        _detalhe += Utils.FitStringLength(boleto.Instrucoes[0].Codigo.ToString(), 2, 2, '0', 0, true, true, true);
                        _detalhe += "00"; // Jéferson (jefhtavares) o banco não estava aceitando esses campos em Branco
                        break;
                    default:
                        _detalhe += Utils.FitStringLength(boleto.Instrucoes[0].Codigo.ToString(), 2, 2, '0', 0, true, true, true);
                        _detalhe += Utils.FitStringLength(boleto.Instrucoes[1].Codigo.ToString(), 2, 2, '0', 0, true, true, true);
                        break;
                }

                //if (boleto.Instrucoes.Count > 1)
                //    _detalhe += Utils.FitStringLength(boleto.Instrucoes[0].Codigo.ToString(), 2, 2, '0', 0, true, true, true);

                //if (boleto.Instrucoes.Count > 2)
                //    _detalhe += Utils.FitStringLength(boleto.Instrucoes[1].Codigo.ToString(), 2, 2, '0', 0, true, true, true);
                //else
                //_detalhe += "  ";
                //    _detalhe += "    ";

                // Juros de 1 dia
                //Se o cliente optar pelo padrão do Banco Itaú ou solicitar o cadastramento permanente na conta corrente, 
                //não haverá a necessidade de informar esse valor.
                //Caso seja expresso em moeda variável, deverá ser preenchido com cinco casas decimais.

                //_detalhe += "0000000000000";
                _detalhe += Utils.FitStringLength(boleto.JurosMora.ToString("0.00").Replace(",", ""), 13, 13, '0', 0, true, true, true);

                // Data limite para desconto
                _detalhe += boleto.DataVencimento.ToString("ddMMyy");
                _detalhe += Utils.FitStringLength(boleto.ValorDesconto.ToString("0.00").Replace(",", ""), 13, 13, '0', 0, true, true, true);
                _detalhe += "0000000000000"; // Valor do IOF
                _detalhe += "0000000000000"; // Valor do Abatimento

                if (boleto.Sacado.CPFCNPJ.Length <= 11)
                    _detalhe += "01";  // CPF
                else
                    _detalhe += "02"; // CNPJ

                _detalhe += Utils.FitStringLength(boleto.Sacado.CPFCNPJ, 14, 14, '0', 0, true, true, true).ToUpper();
                _detalhe += Utils.FitStringLength(boleto.Sacado.Nome.TrimStart(' '), 30, 30, ' ', 0, true, true, false);
                _detalhe += usoBanco;
                _detalhe += Utils.FitStringLength(boleto.Sacado.Endereco.End.TrimStart(' '), 40, 40, ' ', 0, true, true, false).ToUpper();
                _detalhe += Utils.FitStringLength(boleto.Sacado.Endereco.Bairro.TrimStart(' '), 12, 12, ' ', 0, true, true, false).ToUpper();
                _detalhe += Utils.FitStringLength(boleto.Sacado.Endereco.CEP, 8, 8, ' ', 0, true, true, false).ToUpper();
                ;
                _detalhe += Utils.FitStringLength(boleto.Sacado.Endereco.Cidade, 15, 15, ' ', 0, true, true, false).ToUpper();
                _detalhe += Utils.FitStringLength(boleto.Sacado.Endereco.UF, 2, 2, ' ', 0, true, true, false).ToUpper();
                // SACADOR/AVALISTA
                // Normalmente deve ser preenchido com o nome do sacador/avalista. Alternativamente este campo poderá 
                // ter dois outros usos:
                // a) 2o e 3o descontos: para de operar com mais de um desconto(depende de cadastramento prévio do 
                // indicador 19.0 pelo Banco Itaú, conforme item 5)
                // b) Mensagens ao sacado: se utilizados as instruções 93 ou 94 (Nota 11), transcrever a mensagem desejada

                //************************************************************************************************************************
                //*************************************OVERRIDE LINHA DO AVALISTA ********************************************************
                //_detalhe += Utils.FitStringLength(boleto.Sacado.Nome, 30, 30, ' ', 0, true, true, false).ToUpper();
                _detalhe += Utils.FitStringLength("", 30, 30, ' ', 0, true, true, false).ToUpper();
                //************************************************************************************************************************
                //************************************************************************************************************************
                _detalhe += "    "; // Complemento do registro
                _detalhe += boleto.DataVencimento.ToString("ddMMyy");
                // PRAZO - Quantidade de DIAS - ver nota 11(A) - depende das instruções de cobrança 

                if (boleto.Instrucoes.Count > 0)
                {
                    for (int i = 0; i < boleto.Instrucoes.Count; i++)
                    {
                        if (boleto.Instrucoes[i].Codigo == (int)EnumInstrucoes_Itau.Protestar ||
                            boleto.Instrucoes[i].Codigo == (int)EnumInstrucoes_Itau.ProtestarAposNDiasCorridos ||
                            boleto.Instrucoes[i].Codigo == (int)EnumInstrucoes_Itau.ProtestarAposNDiasUteis)
                        {
                            _detalhe += boleto.Instrucoes[i].QuantidadeDias.ToString("00");
                            break;
                        }
                        else if (i == boleto.Instrucoes.Count - 1)
                            _detalhe += "00";
                    }
                }
                else
                {
                    _detalhe += "00";
                }
                _detalhe += " "; // Complemento do registro
                _detalhe += Utils.FitStringLength(numeroRegistro.ToString(), 6, 6, '0', 0, true, true, true);

                _detalhe = Utils.SubstituiCaracteresEspeciais(_detalhe);

                return _detalhe;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar DETALHE do arquivo CNAB400.", ex);
            }
        }


        public string GerarDetalheSegmentoPRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoP;
                _segmentoP = "341";                                                                                                 //03    Z0012   Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO
                _segmentoP += "0001";                                                                                               //04    S0001   UDV -Valor Definido pelo Us.        LOTE DE SERVICO                             0001
                _segmentoP += "3";                                                                                                  //01    Z0065   Tipo de Linha do Formatador         REGISTRO DETALHE DE LOTE
                _segmentoP += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true);                     //05    Z0062   Detalhe/ Seqüência no Segmento      N.SEQUENCIAL REGISTRO LOTE
                _segmentoP += "P";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO SEGMENTO REG DETALHE                 P
                _segmentoP += " ";                                                                                                  //01    S0002   Em branco                           USO EXCLUSIVO NEXXERA
                _segmentoP += ObterCodigoDaOcorrencia(boleto);                                                                      //02    Z0071   Código de Envio do Banco            TIPO DE MOVIMENTO
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Agencia, 6, 6, '0', 0, true, true, true);          //06    Z0003   Agência Bancária da Companhia       Agência Bancária da Companhia
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Conta, 12, 12, '0', 0, true, true, true);          //12    Z0004   Nº da Cta Bancária da Cia NUMERO    DA CONTA CORRENTE
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);//01    Z0006   Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA CONTA
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);//01    Z0006   Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA AG/ CONTA
                _segmentoP += Utils.FitStringLength(boleto.NossoNumero, 20, 20, ' ', 1, true, true, false);                     //20    Z0067   Nº da Duplicata Bancária            IDENTIFICACAO DO TITULO NO BANCO
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO CARTEIRA                             1
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        FORMA DE CADASTRO TITULO BANCO              1
                _segmentoP += "2";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        TIPO DE DOCUMENTO                           2
                _segmentoP += Utils.FitStringLength(boleto.NumeroDocumento, 1, 1, ' ', 1, true, true, false);                     //01    Z0086   Geração do Boleto                   IDENT. EMISSAO BOLETO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                                            //01    Z0086   Geração do Boleto                   IDENT. DA DISTRIBUICAO / ENTREGA
                _segmentoP += Utils.FitStringLength(boleto.NumeroDocumento, 15, 15, '0', 0, true, true, true);                      //15    Z0068   Nº do Documento                     N. DO DOCUMENTO DE COBRANCA
                _segmentoP += Utils.FitStringLength(boleto.DataVencimento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);   //08    Z0049   Data de Vencimento                  DATA DE VENCIMENTO DO TITULO
                _segmentoP += Utils.FitStringLength(boleto.ValorBoleto.ApenasNumeros(), 15, 15, '0', 0, true, true, true);          //15    Z5835   Payment Amount - Withholding        VALOR DO PAGAMENTO
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.Agencia) ? " " : boleto.Cedente.ContaBancaria.Agencia, "0", 5, true); //05    Z0003   Agência Bancária da Companhia       AGENCIA ENCARREGADA DE COBRANCA
                _segmentoP += " ";                                                                                                  //01    Z0005   Dígito da Agência Banc.da Cia       DIGITO VERIFICADOR DA AGENCIA
                _segmentoP += "02";                                                                                                 //02    S0001   UDV -Valor Definido pelo Us.        ESPECIE DE TITULO                           02
                _segmentoP += "N";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        IDENT TITULOS ACEITO / NAO ACEITO           N
                _segmentoP += Utils.FitStringLength(boleto.DataDocumento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);    //08    Z0069   Data da Fatura                      DATA DA EMISSAO DO TITULO
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO DO JUROS DE MORA                     1
                _segmentoP += Utils.FitStringLength(boleto.DataVencimento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);   //08    Z0049   Data de Vencimento                  DATA DE JUROS DE MORA
                _segmentoP += Utils.FitStringLength(boleto.JurosMora.ApenasNumeros(), 15, 15, '0', 0, true, true, true);            //15    Z0075   Juros Diários                       JUROS DE MORA POR DIA/ TAXA
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO DE DESCONTO 1                        1
                _segmentoP += Utils.FitStringLength(boleto.DataDesconto.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);     //08    Z0076   Data de Vcto c / Desconto -         C / R   DATA DO DESCONTO                    1
                _segmentoP += Utils.FitStringLength(boleto.ValorDesconto.ApenasNumeros(), 15, 15, '0', 0, true, true, true);        //15    Z0051   Desconto Obtido                     VALOR/ PERCENTUAL A SER CONCEDIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                                           //15    S0003   Zero                                VALOR DO IOF A SER RECOLHIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                                           //15    S0003   Zero                                VALOR DO ABATIMENTO
                //***********************INFORMACOES QUE DEVERAO FORMAR UMA CHAVE NO ARQUIVO REMESSA********************** //25    Z0070   Código de Usuário do Cliente        IDENTIFICACAO DO TITULO NA EMPRESSA
                //Companhia -- Ficou de verificar
                _segmentoP += Utils.FitStringLength(boleto.Companhia, 5, 5, '0', 0, true, true, true) +
                //Tipo DOcumento
                Utils.FitStringLength(boleto.TipoDocumento.ToString(), 2, 2, '0', 0, true, true, true) +
                //NumeroLinha
                //Utils.FitStringLength(numeroRegistro.ToString(), 2, 2, '0', 0, true, true, true) +
                //FluxoPagamento
                Utils.FitStringLength(boleto.NumeroDocumento, 10, 10, '0', 0, true, true, true) +
                //CodCliente
                Utils.FitStringLength(boleto.CodCliente, 8, 8, '0', 0, true, true, true);
                //********************************************************************************************************
                _segmentoP += Utils.FitStringLength("3", 1, 1, '0', 0, true, true, true);                                           //01    S0001   UDV -Valor Definido pelo            Us.CODIGO DE PROTESTO                       3
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                                            //02    Z0074   Dias para Protesto                  NUMERO DE DIAS PARA PROTESTO
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO PARA BAIXA / DEVOLUCAO               1
                _segmentoP += "001";                                                                                                //03    S0001   UDV -Valor Definido pelo Us.        NUMERO DE DIAS PARA BAIXA / DEVOLUCAO       001
                _segmentoP += "09";                                                                                                 //02    S0001   UDV -Valor Definido pelo Us.        CODIGO DA MOEDA                             09
                _segmentoP += Utils.FitStringLength("", 10, 10, '0', 0, true, true, true);                                          //10    S0003   Zero                                NUMERO DO CONTRATO DA OPER.DE CRED.	 
                _segmentoP += " ";
                _segmentoP = Utils.SubstituiCaracteresEspeciais(_segmentoP);

                return _segmentoP;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        /// <summary>
        /// Obtem o código de ocorrência formatado, utiliza '01' - 'Entrada de titulos como padrão'
        /// </summary>
        /// <param name="boleto">Boleto</param>
        /// <returns>Código da ocorrência</returns>
        public string ObterCodigoDaOcorrencia(BoletoNet.Boleto boleto)
        {
            return boleto.Remessa != null && !string.IsNullOrEmpty(boleto.Remessa.CodigoOcorrencia) ? Utils.FormatCode(boleto.Remessa.CodigoOcorrencia, 2) : TipoOcorrenciaRemessa.EntradaDeTitulos.Format();
        }

        public override string GerarDetalheSegmentoQRemessa(BoletoNet.Boleto boleto, int numeroRegistro, TipoArquivo tipoArquivo)
        {
            try
            {
                string _zeros16 = new string('0', 16);
                string _brancos10 = new string(' ', 10);
                string _brancos28 = new string(' ', 28);
                string _brancos40 = new string(' ', 40);

                string _segmentoQ;

                _segmentoQ = "341";
                _segmentoQ += "0001";
                _segmentoQ += "3";
                _segmentoQ += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true);
                _segmentoQ += "Q";
                _segmentoQ += " ";

                _segmentoQ += ObterCodigoDaOcorrencia(boleto);
                if (boleto.Sacado.CPFCNPJ.Length <= 11)
                    _segmentoQ += "1";
                else
                    _segmentoQ += "2";

                _segmentoQ += Utils.FitStringLength(boleto.Sacado.CPFCNPJ, 15, 15, '0', 0, true, true, true);
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Nome.TrimStart(' '), 30, 30, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += "          ";
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.End.TrimStart(' '), 40, 40, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.Bairro.TrimStart(' '), 15, 15, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.CEP, 8, 8, ' ', 0, true, true, false).ToUpper(); ;
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.Cidade.TrimStart(' '), 15, 15, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.UF, 2, 2, ' ', 0, true, true, false).ToUpper();
                if (boleto.Sacado.CPFCNPJ.Length <= 11)
                    _segmentoQ += "1";
                else
                    _segmentoQ += "2";

                _segmentoQ += Utils.FitStringLength(boleto.Sacado.CPFCNPJ, 15, 15, '0', 0, true, true, true);
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Nome.TrimStart(' '), 30, 30, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += _brancos10;
                _segmentoQ += "000";
                //***********************INFORMACOES QUE DEVERAO FORMAR UMA CHAVE NO ARQUIVO REMESSA********************** //25    Z0070   Código de Usuário do Cliente        IDENTIFICACAO DO TITULO NA EMPRESSA
                //Companhia
                _segmentoQ += Utils.FitStringLength(boleto.Companhia, 5, 5, '0', 0, true, true, true) +
                //Tipo DOcumento
                Utils.FitStringLength(boleto.TipoDocumento.ToString(), 2, 2, '0', 0, true, true, true) +
                //NumeroLinha
                //Utils.FitStringLength(numeroRegistro.ToString(), 2, 2, '0', 0, true, true, true) +
                //FluxoPagamento
                Utils.FitStringLength(boleto.NumeroDocumento, 10, 10, '0', 0, true, true, true) +
                //CodCliente
                Utils.FitStringLength(boleto.CodCliente, 3, 3, '0', 0, true, true, true);
                //********************************************************************************************************
                _segmentoQ += Utils.FitStringLength("", 5, 5, ' ', 0, true, true, false);
                _segmentoQ += "109";

                _segmentoQ = Utils.SubstituiCaracteresEspeciais(_segmentoQ);

                return _segmentoQ;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO Q DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        public string GerarDetalheAlterarcaoDadosSegmentoPRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoP;
                _segmentoP = "341";                                                                                   //3   Z0012 Nº do Banco da Companhia        CODIGO BANCO NA COMPENSACAO                             
                _segmentoP += "0001";                                                                                 //4   S0001 UDV -Valor Definido pelo Us.    LOTE DE SERVICO
                _segmentoP += "3";                                                                                    //1   S0001 UDV -Valor Definido pelo Us.    REGISTRO DETALHE DE LOTE
                _segmentoP += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true);       //5   Z0062 Detalhe/ Seqüência no Segmento  N.SEQUENCIAL REGISTRO LOTE
                _segmentoP += "P";                                                                                    //1   S0001 UDV -Valor Definido pelo Us.    CODIGO SEGMENTO REG DETALHE
                _segmentoP += " ";                                                                                    //1   S0002 Em branco                       USO EXCLUSIVO NEXXERA
                _segmentoP += "31"; //ALTERAÇÃO DE OUTROS DADOS                                                       //2   S0001 UDV -Valor Definido pelo Us.    TIPO DE MOVIMENTO
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Agencia, 5, 5, '0', 0, true, true, true); //5   Z0003 Agência Bancária da Companhia   AGÊNCIA BANCÁRIA DA COMPANHIA
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true); //1   Z0006 Dígito da Conta Banc.da Cia     DIGITO VERIFICADOR DA AGÊNCIA
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Conta, 12, 12, '0', 0, true, true, true);//12  Z0004 Nº da Cta Bancária da Cia       NUMERO DA CONTA CORRENTE
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);//1   Z0006 Dígito da Conta Banc.da Cia     DIGITO VERIFICADOR DA CONTA
                _segmentoP += "1";                                                                                    //1   Z0006 Dígito da Conta Banc.da Cia     DIGITO VERIFICADOR DA AG/ CONTA
                _segmentoP += Utils.FitStringLength(boleto.NossoNumero, 20, 20, ' ', 1, true, true, false);           //20  Z0067 Nº da Duplicata Bancária        IDENTIFICACAO DO TITULO NO BANCO
                _segmentoP += "1";                                                                                    //1   S0001 UDV -Valor Definido pelo Us.    CODIGO CARTEIRA
                _segmentoP += "0";                                                                                    //1   S0003 Zero                            FORMA DE CADASTRO TITULO BANCO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       TIPO DE DOCUMENTO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       IDENT. EMISSAO BOLETO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       IDENT. DA DISTRIBUICAO/ ENTREGA
                _segmentoP += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                           //15  S0002 Em branco                       N. DO DOCUMENTO DE COBRANCA
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DE VENCIMENTO DO TITULO
                _segmentoP += Utils.FitStringLength(boleto.ValorBoleto.ApenasNumeros(), 15, 15, '0', 0, true, true, true); //15  Z60C2 Payment Amount - Withholding    VALOR DO PAGAMENTO
                _segmentoP += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                              //5   S0003 Zero                            AGENCIA ENCARREGADA DE COBRANCA
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco DIGITO VERIFICADOR DA AGENCIA
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                              //2   S0003 Zero                            ESPECIE DE TITULO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco IDENT TITULOS ACEITO/ NAO ACEITO
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DA EMISSAO DO TITULO
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO DO JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DE JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            JUROS DE MORA POR DIA / TAXA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO DE DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DO DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            VALOR / PERCENTUAL A SER CONCEDIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            VALOR DO IOF A SER RECOLHIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            VALOR DO ABATIMENTO
                _segmentoP += Utils.FitStringLength("", 25, 25, ' ', 0, true, true, false);                           //25  S0002 Em branco                       IDENTIFICACAO DO TITULO NA EMPRESSA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO DE PROTESTO
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                              //2   S0003 Zero                            NUMERO DE DIAS PARA PROTESTO
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO PARA BAIXA / DEVOLUCAO
                _segmentoP += Utils.FitStringLength("", 3, 3, ' ', 0, true, true, false);                             //3   S0002 Em branco                       NUMERO DE DIAS PARA BAIXA/ DEVOLUCAO
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                              //2   S0003 Zero                            CODIGO DA MOEDA
                _segmentoP += Utils.FitStringLength("", 10, 10, '0', 0, true, true, true);                            //10  S0003 Zero                            NUMERO DO CONTRATO DA OPER.DE CRED.
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       USO EXCLUSIVO NEXXERA                                                    

                _segmentoP += " ";
                _segmentoP = Utils.SubstituiCaracteresEspeciais(_segmentoP);

                return _segmentoP;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        public string GerarDetalheAlterarcaoDadosSegmentoQRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoQ;
                _segmentoQ = "341";                                                                             //3	    Z0012 Nº do Banco da Companhia          CODIGO BANCO NA COMPENSACAO
                _segmentoQ += Utils.FitStringLength("1", 4, 4, '0', 0, true, true, true);                       //4	    S0001 UDV - Valor Definido pelo Us.     LOTE DE SERVICO
                _segmentoQ += Utils.FitStringLength("3", 1, 1, '0', 0, true, true, true);                       //1	    S0001 UDV - Valor Definido pelo Us. 	REGISTRO DETALHE DE LOTE
                _segmentoQ += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true); //5	    Z0062 Detalhe/Seqüência no Segmento     N.SEQUENCIAL REGISTRO LOTE
                _segmentoQ += "Q";                                                                              //1	    S0001 UDV - Valor Definido pelo Us. 	CODIGO SEGMENTO REG DETALHE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1	    S0002 Em branco                         USO EXCLUSIVO NEXXERA
                _segmentoQ += "31"; //ALTERAÇÃO DE OUTROS DADOS                                                 //2	    S0001 UDV - Valor Definido pelo Us. 	TIPO DE MOVIMENTO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1	    S0003 Zero                              TIPO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15	S0003 Zero                              NUMERO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40	S0002 Em branco                         NOME DO SACADOR
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40	S0002 Em branco                         ENDERECO - SACADO
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15	S0002 Em branco                         BAIRRO
                _segmentoQ += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                        //5	    S0003 Zero                              CEP Benefic./Pagador - 5 díg.
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3	    S0003 Zero                              CEP Benefic./Pagador - 3 díg.
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15	S0002 Em branco                         CIDADE DO BENEFIC./PAGADOR
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2	    S0002 Em branco                         UNIDADE DE FEDERACAO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1	    S0003 Zero                              CÓD.PESSOA FÍS./JUR.BEN./PAG
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15	S0003 Zero                              CPF/CNPJ DO BENEFIC./PAGADOR
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40	S0002 Em branco                         NOME DO SACADOR / AVALISTA
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3	    S0003 Zero                              Nº BANCO DO BENEFIC./PAGADOR
                _segmentoQ += Utils.FitStringLength("", 20, 20, ' ', 0, true, true, false);                     //20	S0002 Em branco                         NOSSO N BANCO CORRESPONDENTE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1	    S0002 Em branco                         FORMA DE ACESSO AO BOLETO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2	    S0002 Em branco                         PRIMEIRA INSTRUCAO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2	    S0002 Em branco                         SEGUNDA INSTRUCAO
                _segmentoQ += Utils.FitStringLength(boleto.Carteira, 3, 3, '0', 0, true, true, true);           //3	    S0001 UDV - Valor Definido pelo Us. 	VARIACAO DA CARTEIRA / CARTEIRA                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                

                _segmentoQ += " ";
                _segmentoQ = Utils.SubstituiCaracteresEspeciais(_segmentoQ);

                return _segmentoQ;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        public string GerarDetalheAlterarcaoVencimentoSegmentoPRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoP;
                _segmentoP = "341";                                                                             //3   Z0012 Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO                                                                                        
                _segmentoP += "0001";                                                                           //4   S0001 UDV -Valor Definido pelo Us.        LOTE DE SERVICO
                _segmentoP += "3";                                                                              //1   S0001 UDV -Valor Definido pelo Us.        REGISTRO DETALHE DE LOTE
                _segmentoP += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true); //5   Z0062 Detalhe/ Seqüência no Segmento      N.SEQUENCIAL REGISTRO LOTE
                _segmentoP += "P";                                                                              //1   S0001 UDV -Valor Definido pelo Us.        CODIGO SEGMENTO REG DETALHE
                _segmentoP += " ";                                                                              //1   S0002 Em branco                           USO EXCLUSIVO NEXXERA
                _segmentoP += "06"; //ALTERACAO DE VENCIMENTO                                                   //2   S0001 UDV -Valor Definido pelo Us.        TIPO DE MOVIMENTO                   
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Agencia, 5, 5, '0', 0, true, true, true); //5   Z0003 Agência Bancária da Companhia       AGÊNCIA BANCÁRIA DA COMPANHIA        
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);  //1   Z0006 Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA AGÊNCIA
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Conta, 12, 12, '0', 0, true, true, true);  //12  Z0004 Nº da Cta Bancária da Cia           NUMERO DA CONTA CORRENTE
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);  //1   Z0006 Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA CONTA
                _segmentoP += "5";                                                                              //1   Z0006 Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA AG/ CONTA                        
                _segmentoP += Utils.FitStringLength(boleto.NossoNumero, 20, 20, ' ', 1, true, true, false);     //20  Z0067 Nº da Duplicata Bancária            IDENTIFICACAO DO TITULO NO BANCO                        
                _segmentoP += "1";                                                                              //1   S0001 UDV -Valor Definido pelo Us.        CODIGO CARTEIRA                        
                _segmentoP += "0";                                                                              //1   S0003 Zero                                FORMA DE CADASTRO TITULO BANCO                        
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                        //1   S0002 Em branco                           TIPO DE DOCUMENTO              
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                        //1   S0002 Em branco                           IDENT. EMISSAO BOLETO                    
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                        //1   S0002 Em branco                           IDENT. DA DISTRIBUICAO/ ENTREGA                    
                _segmentoP += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15  S0002 Em branco                           N. DO DOCUMENTO DE COBRANCA
                _segmentoP += Utils.FitStringLength(boleto.DataVencimento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);//8   Z60C1 Data de Vencimento DATA DE VENCIMENTO DO TITULO                
                _segmentoP += Utils.FitStringLength(boleto.ValorBoleto.ApenasNumeros(), 15, 15, '0', 0, true, true, true);//15  Z60C2 Payment Amount - Withholding  VALOR DO PAGAMENTO
                _segmentoP += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                        //5   S0003 Zero                                AGENCIA ENCARREGADA DE COBRANCA
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1   S0002 Em branco                           DIGITO VERIFICADOR DA AGENCIA
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                        //2   S0003 Zero                                ESPECIE DE TITULO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1   S0002 Em branco                           IDENT TITULOS ACEITO/ NAO ACEITO
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                       //8   S0003 Zero                                DATA DA EMISSAO DO TITULO   
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                       //1   S0003 Zero                                CODIGO DO JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                        //8   S0003 Zero                                DATA DE JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                JUROS DE MORA POR DIA / TAXA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1   S0003 Zero                                CODIGO DE DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                        //8   S0003 Zero                                DATA DO DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                VALOR / PERCENTUAL A SER CONCEDIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                VALOR DO IOF A SER RECOLHIDO  
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                VALOR DO ABATIMENTO
                _segmentoP += Utils.FitStringLength("", 25, 25, ' ', 0, true, true, false);                     //25  Z0070   Código de Usuário do Cliente      IDENTIFICACAO DO TITULO NA EMPRESSA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1   S0003 Zero            CODIGO DE PROTESTO                               
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                        //2   S0003 Zero                                NUMERO DE DIAS PARA PROTESTO                                   
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1   S0003 Zero                                CODIGO PARA BAIXA / DEVOLUCAO                                
                _segmentoP += Utils.FitStringLength("", 3, 3, ' ', 0, true, true, false);                       //3   S0002 Em branco                           NUMERO DE DIAS PARA BAIXA/ DEVOLUCAO                                
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                        //2   S0003 Zero                                CODIGO DA MOEDA                                
                _segmentoP += Utils.FitStringLength("", 10, 10, '0', 0, true, true, true);                      //10  S0003 Zero                                NUMERO DO CONTRATO DA OPER.DE CRED.
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                        //1   S0002 Em branco                           USO EXCLUSIVO NEXXERA

                _segmentoP += " ";
                _segmentoP = Utils.SubstituiCaracteresEspeciais(_segmentoP);

                return _segmentoP;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        public string GerarDetalheAlterarcaoVencimentoSegmentoQRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoQ;
                _segmentoQ = "341";                                                                             //3     Z0012 Nº do Banco da Companhia          CODIGO BANCO NA COMPENSACAO
                _segmentoQ += Utils.FitStringLength("1", 4, 4, '0', 0, true, true, true);                       //4     S0001 UDV -Valor Definido pelo Us.      LOTE DE SERVICO
                _segmentoQ += Utils.FitStringLength("3", 1, 1, '0', 0, true, true, true);                       //1     S0001 UDV -Valor Definido pelo Us.      REGISTRO DETALHE DE LOTE
                _segmentoQ += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true); //5     Z0062 Detalhe/ Seqüência no Segmento    N.SEQUENCIAL REGISTRO LOTE
                _segmentoQ += "Q";                                                                              //1     S0001 UDV -Valor Definido pelo Us.      CODIGO SEGMENTO REG DETALHE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1     S0002 Em branco                         USO EXCLUSIVO NEXXERA
                _segmentoQ += "06"; //ALTERAÇÃO DO VENCIMENTO                                                   //2     S0001 UDV -Valor Definido pelo Us.      TIPO DE MOVIMENTO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1     S0003 Zero                              TIPO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15    S0003 Zero                              NUMERO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40    S0002 Em branco                         NOME DO SACADOR
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40    S0002 Em branco                         ENDERECO -SACADO
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15    S0002 Em branco                         BAIRRO
                _segmentoQ += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                        //5     S0003 Zero                              CEP Benefic./ Pagador - 5 díg.
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3     S0003 Zero                              CEP Benefic./ Pagador - 3 díg.
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15    S0002 Em branco                         CIDADE DO BENEFIC./ PAGADOR
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2     S0002 Em branco                         UNIDADE DE FEDERACAO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1     S0003 Zero                              Cód.Pessoa Fís./ Jur.Ben./ Pag
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15    S0003 Zero                              CPF / CNPJ do Benefic./ Pagador
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40    S0002 Em branco                         NOME DO SACADOR / AVALISTA
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3    S0003 Zero                              Nº Banco do Benefic./ Pagador
                _segmentoQ += Utils.FitStringLength("", 20, 20, ' ', 0, true, true, false);                     //20    S0002 Em branco                         NOSSO N BANCO CORRESPONDENTE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1     S0002 Em branco                         FORMA DE ACESSO AO BOLETO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2     S0002 Em branco                         PRIMEIRA INSTRUCAO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2     S0002 Em branco                         SEGUNDA INSTRUCAO
                _segmentoQ += Utils.FitStringLength(boleto.Carteira, 3, 3, '0', 0, true, true, true);           //3     S0001 UDV -Valor Definido pelo Us.      VARIACAO DA CARTEIRA / CARTEIRA

                _segmentoQ += " ";
                _segmentoQ = Utils.SubstituiCaracteresEspeciais(_segmentoQ);

                return _segmentoQ;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        #endregion DETALHE

        #region TRAILER

        /// <summary>
        /// TRAILER do arquivo CNAB
        /// Gera o TRAILER do arquivo remessa de acordo com o lay-out informado
        /// </summary>
        public string GerarTrailerCustomRemessa(int numeroRegistro, TipoArquivo tipoArquivo, Cedente cedente, decimal vlTitulosTotal, int qtdTitulosTotal)
        {
            try
            {
                string _trailer = " ";

                base.GerarTrailerRemessa(numeroRegistro, tipoArquivo, cedente, vlTitulosTotal);

                switch (tipoArquivo)
                {
                    case TipoArquivo.CNAB240:
                        _trailer = GerarTrailerRemessa240(numeroRegistro);
                        break;
                    case TipoArquivo.CNAB400:
                        _trailer = GerarTrailerRemessa400(numeroRegistro);
                        break;
                    case TipoArquivo.Outro:
                        throw new Exception("Tipo de arquivo inexistente.");
                }

                return _trailer;

            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }
        }

        /// <summary>
        ///POS INI/FINAL    TAM     Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
        ///------------------------------------------------------------------------------------------------------------------------------------------------
        ///001 - 003	    003	    Z0012           Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO	 
        ///004 - 007	    004	    S0001           UDV - Valor Definido pelo Us.       LOTE DE SERVICO	                            9999
        ///008 - 008	    001	    S0001           UDV - Valor Definido pelo Us. 	    REGISTRO TRAILER DE ARQUIVO	                9
        ///009 - 017	    009	    S0002           Em branco                           USO EXCLUSIVO NEXXERA	 
        ///018 - 023	    006	    S0001           UDV - Valor Definido pelo Us. 	    QTDE LOTES DO ARQUIVO	                    000001
        ///024 - 029	    006	    Z0022           Cont. de Linha Arq.Formatado        QTDE REGISTROS DO ARQUIVO
        ///030 - 035	    006	    S0001           UDV - Valor Definido pelo Us. 	    QTDE DE CONTAS P/CONC.(LOTES)               000000
        ///036 - 240	    205	    S0002           Em branco                           USO EXCLUSIVO NEXXERA
        /// </summary>


        public string GerarTrailerRemessa240(int numeroRegistro)
        {
            try
            {
                string _trailer = Utils.FormatCode(Codigo.ToString(), "0", 3, true);                            //003   Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO
                _trailer += Utils.FormatCode("", "9", 4, true);                                                 //004	UDV - Valor Definido pelo Us.       LOTE DE SERVICO	                            9999
                _trailer += Utils.FormatCode("", "9", 1);                                                       //001	UDV - Valor Definido pelo Us. 	    REGISTRO TRAILER DE ARQUIVO	                9
                _trailer += Utils.FormatCode("", " ", 9);                                                       //009	Em branco                           USO EXCLUSIVO NEXXERA	 
                _trailer += Utils.FormatCode("000001", "0", 6, true);                                           //006	UDV - Valor Definido pelo Us. 	    QTDE LOTES DO ARQUIVO	                    000001
                _trailer += Utils.FitStringLength(numeroRegistro.ToString(), 6, 6, '0', 0, true, true, true);   //006	Cont. de Linha Arq.Formatado        QTDE REGISTROS DO ARQUIVO
                _trailer += Utils.FormatCode("", "0", 6);                                                       //006	UDV - Valor Definido pelo Us. 	    QTDE DE CONTAS P/CONC.(LOTES)               000000                        
                _trailer += Utils.FormatCode("", " ", 205);                                                     //205	Em branco                           USO EXCLUSIVO NEXXERA

                _trailer = Utils.SubstituiCaracteresEspeciais(_trailer);

                return _trailer;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do registro TRAILER do arquivo de REMESSA.", ex);
            }
        }

        public string GerarTrailerRemessa400(int numeroRegistro)
        {
            try
            {
                string complemento = new string(' ', 393);
                string _trailer;

                _trailer = "9";
                _trailer += complemento;
                _trailer += Utils.FitStringLength(numeroRegistro.ToString(), 6, 6, '0', 0, true, true, true); // Número sequencial do registro no arquivo.

                _trailer = Utils.SubstituiCaracteresEspeciais(_trailer);

                return _trailer;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do registro TRAILER do arquivo de REMESSA.", ex);
            }
        }
        /// <summary>
        /// TRAILER do arquivo CNAB
        /// Gera o TRAILER do arquivo remessa de acordo com o lay-out informado
        /// </summary>
        public string GerarTrailerLoteRemessa(int numeroRegistro, TipoArquivo tipoArquivo, Cedente cedente, decimal vlTitulosTotal, int qtdTitulosTotal)
        {
            try
            {
                string _trailer = " ";
                switch (tipoArquivo)
                {
                    case TipoArquivo.CNAB240:
                        _trailer = GerarTrailerLoteRemessa240(numeroRegistro, vlTitulosTotal, qtdTitulosTotal);
                        break;
                    case TipoArquivo.CNAB400:
                        _trailer = "";
                        break;
                    case TipoArquivo.Outro:
                        throw new Exception("Tipo de arquivo inexistente.");
                }

                return _trailer;

            }
            catch (Exception ex)
            {
                throw new Exception("", ex);
            }
        }

        /// <summary>
        ///POS INI/FINAL    TAM     Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
        ///------------------------------------------------------------------------------------------------------------------------------------------------
        ///001 - 003	    003      Z0012            Nº do Banco da Companhia           CODIGO DO BANCO NA COMPENSACAO	 
        ///004 - 004	    004      S0001            UDV - Valor Definido pelo Us.      LOTE DE SERVICO	                            0001
        ///008 - 001	    001      S0001            UDV - Valor Definido pelo Us.      REGISTRO TRAILER DE LOTE	                5
        ///009 - 009	    009      S0002            Em branco                          USO EXCLUSIVO NEXXERA	 
        ///018 - 006	    006      Z0063            Seq. de Linha em Segmento          EP QTDE REGISTROS DO LOTE
        ///024 - 006	    006      Z0022            Cont. de Linha Arq.Formatado       QTDE TITULOS EM COBRANCA
        ///030 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
        ///047 - 006	    006      S0003            Zero                               QTDE TITULOS EM COBRANCA	 
        ///053 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
        ///070 - 006	    006      S0003            Zero                               QTDE TITULOS EM COBRANCA	 
        ///076 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
        ///093 - 006	    006      S0003            Zero                               QTDE TITULOS EM COBRANCA	 
        ///099 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
        ///116 - 008	    008      S0002            Em branco                          NUMERO DO AVISO DE LANCAMENTO	 
        ///124 - 117	    117      S0002            Em branco                          USO EXCLUSIVO NEXXERA
        /// </summary>
        public string GerarTrailerLoteRemessa240(int numeroRegistro, decimal vlrTituloTotal, int qtdTituloTotal)
        {
            try
            {
                string _trailer = Utils.FormatCode(Codigo.ToString(), "0", 3, true);          //003   Nº do Banco da Companhia           CODIGO DO BANCO NA COMPENSACAO	      
                _trailer += Utils.FormatCode("0001", "0", 4, true);                           //004   UDV - Valor Definido pelo Us.      LOTE DE SERVICO	                            0001     
                _trailer += Utils.FormatCode("5", "0", 1); ;                                  //001   UDV - Valor Definido pelo Us.      REGISTRO TRAILER DE LOTE	                5     
                _trailer += Utils.FormatCode("", " ", 9);                                     //009   Em branco                          USO EXCLUSIVO NEXXERA	      
                _trailer += Utils.FormatCode(numeroRegistro.ToString(), "0", 6, true);        //006   Seq. de Linha em Segmento          EP QTDE REGISTROS DO LOTE     
                // Totalização da Cobrança Simples                                                        
                _trailer += Utils.FormatCode(qtdTituloTotal.ToString(), "0", 6, true);        //006   Cont. de Linha Arq.Formatado       QTDE TITULOS EM COBRANCA     
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //Totalização cobrança vinculada                                                  
                _trailer += Utils.FormatCode("", "0", 6);                                     //006   Zero                               QTDE TITULOS EM COBRANCA	              
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 

                _trailer += Utils.FormatCode("", "0", 6);                                     //006   Zero                               QTDE TITULOS EM COBRANCA	      
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	                                                                                                                                                                                                                                                                                                                                                                    
                _trailer += Utils.FormatCode("", "0", 6);                                     //006   Zero                               QTDE TITULOS EM COBRANCA	                         
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	  
                _trailer += Utils.FormatCode("", " ", 8);                                     //008   Em branco                          NUMERO DO AVISO DE LANCAMENTO	         
                _trailer += Utils.FormatCode("", " ", 117);                                   //117   Em branco                          USO EXCLUSIVO NEXXERA    
                return _trailer;
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao gerar Trailer de Lote do arquivo de remessa.", e);
            }

        }


        #endregion TRAILER
    }

}
