﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Rotinas
{
    public class ConverterListaParaExcel
    {
        #region CSV file
        /// <summary>
        /// Metodo para Gerar um CSV a partir de qulaque list
        /// </summary>
        /// <param name="list">Lista de qualquer informação.</param>
        /// <param name="csvNameWithExt">Local do Arquivo.</param>
        public static void GerarCSVDeGenericList<T>(List<T> list, string csvNameWithExt)
        {
            if (list == null || list.Count == 0) return;
            string newLine = Environment.NewLine;

            using (var sw = new StreamWriter(csvNameWithExt, false, Encoding.UTF8))
            {
                PropertyInfo[] props = GetProperties(list[0]);
                //PropertyInfo[] propsFiltradas = props.Where(x => colunas.Contains(x.Name)).ToArray();

                foreach (PropertyInfo pi in props)
                {
                    sw.Write(Idioma.Traduzir(pi.Name) + ";");
                }
                sw.Write(newLine);

                foreach (T item in list)
                {
                    foreach (PropertyInfo pi in props)
                    {
                        string whatToWrite =
                        Convert.ToString(item.GetType()
                                             .GetProperty(pi.Name) 
                                             .GetValue(item, null))
                            .Replace(';', ' ') + ';';

                        sw.Write(whatToWrite);
                    }
                    sw.Write(newLine);
                }
            }
        }
        /// <summary>
        /// Metodo para Gerar um CSV a partir de qulaque list
        /// </summary>
        /// <param name="list">Lista de qualquer informação.</param>
        /// <param name="csvNameWithExt">Local do Arquivo.</param>
        /// <param name="columns">A partir das colunas enviadas.</param>
        public static void GerarCSVDeEspecList<T>(List<T> list, string csvNameWithExt, List<CollumnExcel> columns)
        {
            if (list == null || list.Count == 0) return;
            string newLine = Environment.NewLine;

            using (var sw = new StreamWriter(csvNameWithExt, false, Encoding.UTF8))
            {
                PropertyInfo[] props = GetProperties(list[0]);
                CollumnExcel columnTitle = new CollumnExcel();
                foreach (var column in columns)
                {
                    if (props.Select(x => x.Name).Contains(column.Key))
                    {
                        columnTitle = column;
                        sw.Write(Idioma.Traduzir(columnTitle.Text) + ";");
                    }

                }
                sw.Write(newLine);

                foreach (T item in list)
                {
                    foreach (CollumnExcel column in columns)
                    {
                        if (props.Select(x => x.Name).Contains(column.Key))
                        {
                            string whatToWrite =
                            Convert.ToString(item.GetType()
                                                 .GetProperty(column.Key)
                                                 .GetValue(item, null))
                                .Replace(';', ' ') + ';';
                            sw.Write(whatToWrite);
                        }

                    }
                    sw.Write(newLine);
                }
            }
        }
        public static PropertyInfo[] GetProperties(object obj)
        {
            return obj.GetType().GetProperties();
        }
        #endregion

        #region excel file
        public static void ConvertListToExcelFile<T>(List<T> list, string csvNameWithExt, List<CollumnExcel> columns)
        {
            DataTable dt = new DataTable(typeof(T).Name);
            if (list == null || list.Count == 0) return;
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo[] props = GetProperties(list[0]);
            CollumnExcel columnTitle = new CollumnExcel();
            foreach (var column in columns)
            {
                if (props.Select(x => x.Name).Contains(column.Key))
                {
                    columnTitle = column;
                    dt.Columns.Add(Idioma.Traduzir(columnTitle.Text));
                }

            }
            foreach (T item in list)
            {
                DataRow row = dt.NewRow();
                foreach (CollumnExcel column in columns)
                {
                    if (props.Select(x => x.Name).Contains(column.Key))
                    {
                        row[column.Text] = Convert.ToString(item.GetType().GetProperty(column.Key).GetValue(item, null)).Replace(';', ' ');

                    }
                }
                dt.Rows.Add(row);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            bool result = CreateExcelDocument(ds, csvNameWithExt);
            ds.Tables.Remove(dt);
        }
        /// <summary>
        /// Create an Excel file, and write it to a file.
        /// </summary>
        /// <param name="ds">DataSet containing the data to be written to the Excel.</param>
        /// <param name="excelFilename">Name of file to be written.</param>
        /// <returns>True if successful, false if something went wrong.</returns>
        public static bool CreateExcelDocument(DataSet ds, string excelFilename)
        {
            try
            {
                using (SpreadsheetDocument document = SpreadsheetDocument.Create(excelFilename, SpreadsheetDocumentType.Workbook))
                {
                    WriteExcelFile(ds, document);
                }
                //Trace.WriteLine("Successfully created: " + excelFilename);
                return true;
            }
            catch (Exception ex)
            {
                //Trace.WriteLine("Failed, exception thrown: " + ex.Message);
                return false;
            }
        }
        private static void WriteExcelFile(DataSet ds, SpreadsheetDocument spreadsheet)
        {
            //  Create the Excel file contents.  This function is used when creating an Excel file either writing
            //  to a file, or writing to a MemoryStream.
            spreadsheet.AddWorkbookPart();
            spreadsheet.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            //  My thanks to James Miera for the following line of code (which prevents crashes in Excel 2010)
            spreadsheet.WorkbookPart.Workbook.Append(new BookViews(new WorkbookView()));

            //  If we don't add a "WorkbookStylesPart", OLEDB will refuse to connect to this .xlsx file !
            WorkbookStylesPart workbookStylesPart = spreadsheet.WorkbookPart.AddNewPart<WorkbookStylesPart>("rIdStyles");
            //Stylesheet stylesheet = new Stylesheet();
            Stylesheet stylesheet = GenerateStylesheet();
            workbookStylesPart.Stylesheet = stylesheet;

            //  Loop through each of the DataTables in our DataSet, and create a new Excel Worksheet for each.
            uint worksheetNumber = 1;
            foreach (DataTable dt in ds.Tables)
            {
                //  For each worksheet you want to create
                string workSheetID = "rId" + worksheetNumber.ToString();
                string worksheetName = dt.TableName;

                WorksheetPart newWorksheetPart = spreadsheet.WorkbookPart.AddNewPart<WorksheetPart>();
                newWorksheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet();

                // create sheet data
                newWorksheetPart.Worksheet.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.SheetData());

                // save worksheet
                WriteDataTableToExcelWorksheet(dt, newWorksheetPart);
                newWorksheetPart.Worksheet.Save();

                // create the worksheet to workbook relation
                if (worksheetNumber == 1)
                    spreadsheet.WorkbookPart.Workbook.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Sheets());

                spreadsheet.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>().AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Sheet()
                {
                    Id = spreadsheet.WorkbookPart.GetIdOfPart(newWorksheetPart),
                    SheetId = (uint)worksheetNumber,
                    Name = dt.TableName
                });

                worksheetNumber++;
            }

            spreadsheet.WorkbookPart.Workbook.Save();
        }
        private static void WriteDataTableToExcelWorksheet(DataTable dt, WorksheetPart worksheetPart)
        {
            var worksheet = worksheetPart.Worksheet;
            var sheetData = worksheet.GetFirstChild<SheetData>();

    


            string cellValue = "";

            //  Create a Header Row in our Excel file, containing one header for each Column of data in our DataTable.
            //
            //  We'll also create an array, showing which type each column of data is (Text or Numeric), so when we come to write the actual
            //  cells of data, we'll know if to write Text values or Numeric cell values.
            int numberOfColumns = dt.Columns.Count;
            bool[] IsNumericColumn = new bool[numberOfColumns];

            string[] excelColumnNames = new string[numberOfColumns];
            for (int n = 0; n < numberOfColumns; n++)
                excelColumnNames[n] = GetExcelColumnName(n);

            //
            //  Create the Header row in our Excel Worksheet
            //
            uint rowIndex = 1;

            var headerRow = new Row { RowIndex = rowIndex };  // add a row at the top of spreadsheet
            sheetData.Append(headerRow);


            for (int colInx = 0; colInx < numberOfColumns; colInx++)
            {
                DataColumn col = dt.Columns[colInx];
                AppendTextCell(excelColumnNames[colInx] + "1", col.ColumnName, headerRow, 2);
                IsNumericColumn[colInx] = (col.DataType.FullName == "System.Decimal") || (col.DataType.FullName == "System.Int32");
            }

            //
            //  Now, step through each row of data in our DataTable...
            //
            double cellNumericValue = 0;
            foreach (DataRow dr in dt.Rows)
            {
                // ...create a new row, and append a set of this row's data to it.
                ++rowIndex;
                var newExcelRow = new Row { RowIndex = rowIndex };  // add a row at the top of spreadsheet
                sheetData.Append(newExcelRow);

                for (int colInx = 0; colInx < numberOfColumns; colInx++)
                {
                    cellValue = dr.ItemArray[colInx].ToString();

                    // Create cell with data
                    if (IsNumericColumn[colInx])
                    {
                        //  For numeric cells, make sure our input data IS a number, then write it out to the Excel file.
                        //  If this numeric value is NULL, then don't write anything to the Excel file.
                        cellNumericValue = 0;
                        if (double.TryParse(cellValue, out cellNumericValue))
                        {
                            cellValue = cellNumericValue.ToString();
                            AppendNumericCell(excelColumnNames[colInx] + rowIndex.ToString(), cellValue, newExcelRow);
                        }
                    }
                    else
                    {
                        //  For text cells, just write the input data straight out to the Excel file.
                        AppendTextCell(excelColumnNames[colInx] + rowIndex.ToString(), cellValue, newExcelRow);
                    }
                }
            }
        }
        private static Stylesheet GenerateStylesheet()
        {
            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new Font( // Index 0 - default
                    new FontSize() { Val = 10 }

                ),
                new Font( // Index 1 - header
                    new FontSize() { Val = 10 },
                    new Bold(),
                    new Color() { Rgb = "FFFFFF" }

                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.Gray125 }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "969696 " } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header
                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;
        }
        private static void AppendTextCell(string cellReference, string cellStringValue, Row excelRow, uint styleIndex = 0)
        {
            //  Add a new Excel Cell to our Row
            Cell cell = new Cell()
            {
                CellValue = new CellValue(cellStringValue),
                DataType = new EnumValue<CellValues>(CellValues.String),
                StyleIndex = styleIndex
            };
            //Cell cell = new Cell()
            //{
            //    CellReference = cellReference,
            //    DataType = CellValues.String
            //};
            //CellValue cellValue = new CellValue();
            //cellValue.Text = cellStringValue;
            //cell.Append(cellValue);
            excelRow.Append(cell);
        }
        private static void AppendNumericCell(string cellReference, string cellStringValue, Row excelRow, uint styleIndex = 0)
        {
            //  Add a new Excel Cell to our Row
            Cell cell = new Cell()
            {
                CellValue = new CellValue(cellStringValue),
                DataType = new EnumValue<CellValues>(CellValues.String),
                StyleIndex = styleIndex
            };
            //Cell cell = new Cell() { CellReference = cellReference };
            //CellValue cellValue = new CellValue();
            //cellValue.Text = cellStringValue;
            //cell.Append(cellValue);
            excelRow.Append(cell);
        }
        private static string GetExcelColumnName(int columnIndex)
        {
            //  Convert a zero-based column index into an Excel column reference  (A, B, C.. Y, Y, AA, AB, AC... AY, AZ, B1, B2..)
            //
            //  eg  GetExcelColumnName(0) should return "A"
            //      GetExcelColumnName(1) should return "B"
            //      GetExcelColumnName(25) should return "Z"
            //      GetExcelColumnName(26) should return "AA"
            //      GetExcelColumnName(27) should return "AB"
            //      ..etc..
            //
            if (columnIndex < 26)
                return ((char)('A' + columnIndex)).ToString();

            char firstChar = (char)('A' + (columnIndex / 26) - 1);
            char secondChar = (char)('A' + (columnIndex % 26));

            return string.Format("{0}{1}", firstChar, secondChar);
        }
        public class CollumnExcel
        {
            public string Key { get; set; }
            public string Text { get; set; }
        }
        #endregion

    }
}

