﻿using Cobranca.Data.Model;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using System;
using System.Collections.Generic;
using BoletoNet;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cobranca.Rotinas.BNet;
using System.IO;
using BoletoNet.Util;
using Cobranca.Db.Rotinas;
using static Cobranca.Rotinas.BoletoGerar.BoletoUtils;

namespace Cobranca.Rotinas.Service
{
    public class RemessaService
    {
        #region Repository
        #endregion

        /// <summary>
        /// Rotina para criar arquivo remessa.
        /// </summary>
        /// <param name="_periodoInicio">Periodo de inicio para filtrar boletos pela data de aprovacao</param>
        /// <param name="_periodoFim">Periodo de fim para filtrar boletos pela data de aprovacao</param>
        /// <param name="boletoIds">Lista de ids de boletos</param>
        /// <param name="paramGrupo">Parametro Grupo Arquivo</param>
        /// <returns>Retorna linha</returns>
        public RemessaRetorno CriarArquivoRemessa(DateTime? _periodoInicio, DateTime? _periodoFim, List<int> boletoIds, TabelaBancoPortadorGrupo paramGrupo)
        {
            RemessaRetorno op = new RemessaRetorno();
            Repository<TabelaBancoPortador> _repBancoPortador = new Repository<TabelaBancoPortador>();
            Repository<TabelaConstrutora> _repConstrutora = new Repository<TabelaConstrutora>();
            Data.Services.BoletoDapper _boletoDapper = new Data.Services.BoletoDapper();

            try
            {
                List<BoletoDapperModel> boletosDapper = _boletoDapper.ObterBoletos((short)Enumeradores.StatusBoleto.Aprovado, _periodoInicio, _periodoFim, boletoIds, paramGrupo.Cod);
                if (boletosDapper.Count == 0) return op;
                var groupFiscais = boletosDapper.GroupBy(x => x.CompanhiaFiscal).ToList();
                List<ContratoParcelaBoleto> ListaBoletosOK = new List<ContratoParcelaBoleto>();
                List<string> companhiaFiscais = boletosDapper.Select(x => x.CompanhiaFiscal).ToList();
                StringBuilder Conteudo = new StringBuilder();

                foreach (var item in groupFiscais)
                {
                    var paramCedente = _repBancoPortador.FindOne(x => x.CompanhiaFiscal == item.Key);
                    if (paramCedente != null)
                    {
                        if (paramCedente.LoteAtual == null) paramCedente.LoteAtual = paramCedente.Lote;

                        CriarArquivoPorCompanhiaFiscal((int)paramCedente.LoteAtual, paramCedente, item.ToList(), ref Conteudo, ref ListaBoletosOK);

                        //ATUALIZA O NUMERO DO LOTE
                        paramCedente.LoteAtual++;
                        _repBancoPortador.Update(paramCedente);
                    }
                }

                paramGrupo.NumeroLoteAtual++;
                op.OK = true;
                op.CodBancoPortadorGrupo = paramGrupo.Cod;
                op.LoteAtual = (int)paramGrupo.NumeroLoteAtual;
                op.BoletosProcessado = ListaBoletosOK;
                op.ConteudoRemessa = Conteudo;
                op.Arquivo = Encoding.UTF8.GetBytes(Conteudo.ToString());
                //RegistrarBoletosProcessados(ListaBoletosOK);

                return op;
            }
            catch (Exception ex)
            {
                op.Mensagem = ex.Message;
                return op;
            }
        }

        /// <summary>
        /// Rotina para criar arquivo remessa por periodo.
        /// </summary>
        /// <param name="companhia">Companhia(Construtora) para emitir o remessa</param>
        /// <param name="_periodoInicio">Periodo de inicio para filtrar boletos pela data de aprovacao</param>
        /// <param name="_periodoFim">Periodo de fim para filtrar boletos pela data de aprovacao</param>
        /// <returns>Retorna linha</returns>
        public RemessaRetorno CriarArquivoRemessa(DateTime? _periodoInicio, DateTime? _periodoFim, TabelaBancoPortadorGrupo paramGrupo)
        {
            var l = new List<int>();
            return CriarArquivoRemessa(_periodoInicio, _periodoFim, l, paramGrupo);
        }


        public void RegistrarLogRemessa(ref RemessaRetorno op)
        {
            try
            {
                Repository<TabelaBancoPortadorLogArquivo> _dbLog = new Repository<TabelaBancoPortadorLogArquivo>();
                TabelaBancoPortadorLogArquivo log = new TabelaBancoPortadorLogArquivo();
                log.NomeDocumento = op.NomeArquivo;
                log.Arquivo = op.Arquivo;
                log.BoletoTotalArquivo = op.BoletosProcessado.Count();
                log.CodUsuario = op.CodUsuario;
                var entity = _dbLog.CreateOrUpdate(log);
                op.CodLogArquivo = entity.Cod;


                Repository<TabelaBancoPortadorGrupo> _repPortadorGrupo = new Repository<TabelaBancoPortadorGrupo>();
                var entityPortadorGrupo = _repPortadorGrupo.FindById(op.CodBancoPortadorGrupo);
                entityPortadorGrupo.NumeroLoteAtual = op.LoteAtual;
                _repPortadorGrupo.Update(entityPortadorGrupo);
            }
            catch (Exception ex)
            {
                throw new Exception("erro ao registrar o log do lote", ex);
            }

        }

        public int RegistrarLogRemessa(string nomeArquivo, byte[] arquivo, int total, int usuarioId)
        {
            try
            {
                Repository<TabelaBancoPortadorLogArquivo> _dbLog = new Repository<TabelaBancoPortadorLogArquivo>();
                TabelaBancoPortadorLogArquivo log = new TabelaBancoPortadorLogArquivo();
                log.NomeDocumento = nomeArquivo;
                log.Arquivo = arquivo;
                log.BoletoTotalArquivo = total;
                log.CodUsuario = usuarioId;
                var entity = _dbLog.CreateOrUpdate(log);
                return entity.Cod;


                //Repository<TabelaBancoPortadorGrupo> _repPortadorGrupo = new Repository<TabelaBancoPortadorGrupo>();
                //var entityPortadorGrupo = _repPortadorGrupo.FindById(op.CodBancoPortadorGrupo);
                //entityPortadorGrupo.NumeroLoteAtual = op.LoteAtual;
                //_repPortadorGrupo.Update(entityPortadorGrupo);
            }
            catch (Exception ex)
            {
                throw new Exception("erro ao registrar o log do lote", ex);
            }

        }

        /// <summary>
        /// Rotina para criar arquivo remessa.
        /// </summary>
        /// <param name="parametroCedente">informação do banco portador</param>
        /// <param name="boletosDapper">lista de boletos</param>
        /// <param name="conteudo">conteudo do arquivo</param>
        /// <param name="companhiaFiscal">comanhiafiscal</param>
        /// <returns>Retorna linha</returns>
        public void CriarArquivoPorCompanhiaFiscal(int LoteAtual, TabelaBancoPortador parametroCedente, List<BoletoDapperModel> boletosDapper, ref StringBuilder conteudo, ref List<ContratoParcelaBoleto> ListaBoletosOK)
        {
            try
            {
                #region UTILs
                Banco _banco = new Banco((int)parametroCedente.BancoNumero);
                Cedente _cedente = MontarCedente(parametroCedente);
                int numeroRegistro = 2;
                int numeroRegistroLote = 1;
                decimal vlTitulosTotal = 0;
                int qtdTitulosTotal = Db.Rotinas.DbRotinas.ConverterParaInt(boletosDapper.Count());
                string strArchive = "";
                #endregion

                #region HEADER CABEÇALHO
                //GERAR O HEADER (CABEÇALHO)
                string _header = " ";
                _header = GerarHeaderRemessaCNAB240(_cedente, _banco.Codigo, (int)LoteAtual++);
                strArchive += $"{_header}{Environment.NewLine}";
                #endregion

                #region HEADER CABEÇALHO ADICIONAL
                //GERAR O HEADER (CABEÇALHO ADICIONAL)
                string _headerAdicional = " ";
                _headerAdicional = GerarHeaderLoteRemessaCNAB240(_cedente, _banco.Codigo, (int)LoteAtual++);
                strArchive += $"{_headerAdicional}{Environment.NewLine}";
                #endregion

                #region DETALHE CONTEUDO
                List<BoletoNet.Boleto> boletos = new List<BoletoNet.Boleto>();
                boletos = boletosDapper.Select(x => ConvertModelDapperToBoletos(x, _cedente)).ToList();
                //PREENCHER OS DETALHES
                foreach (BoletoNet.Boleto boleto in boletos)
                {
                    try
                    {

                        boleto.Banco = _banco;

                        //VERIFICA SE O BOLETO É ALTERACAO
                        if (boleto.Alterar)
                        {
                            //GERAR O DETALHE P
                            string DetalheAlteracaoP = "";
                            DetalheAlteracaoP = GerarDetalheAlterarcaoDadosSegmentoPRemessa240(boleto, numeroRegistroLote, "");
                            strArchive += $"{DetalheAlteracaoP}{Environment.NewLine}";
                            numeroRegistroLote++;
                            numeroRegistro++;

                            //GERAR O DETALHE Q
                            string DetalheAlteracaoQ = "";
                            DetalheAlteracaoQ = GerarDetalheAlterarcaoDadosSegmentoQRemessa240(boleto, numeroRegistroLote, "");
                            strArchive += $"{DetalheAlteracaoQ}{Environment.NewLine}";
                            numeroRegistroLote++;
                            numeroRegistro++;

                            //GERAR O DETALHE P
                            string DetalheVencimentoP = "";
                            DetalheVencimentoP = GerarDetalheAlterarcaoVencimentoSegmentoPRemessa240(boleto, numeroRegistroLote, "");
                            strArchive += $"{DetalheVencimentoP}{Environment.NewLine}";
                            numeroRegistroLote++;
                            numeroRegistro++;

                            //GERAR O DETALHE Q
                            string DetalheVencimentoQ = "";
                            DetalheVencimentoQ = GerarDetalheAlterarcaoVencimentoSegmentoQRemessa240(boleto, numeroRegistroLote, "");
                            strArchive += $"{DetalheVencimentoQ}{Environment.NewLine}";
                            numeroRegistroLote++;
                            numeroRegistro++;

                        }
                        else
                        {
                            //GERAR O DETALHE P
                            string DetalheP = "";
                            DetalheP = GerarDetalheSegmentoPRemessa240(boleto, numeroRegistroLote, "");
                            strArchive += $"{DetalheP}{Environment.NewLine}";
                            numeroRegistroLote++;
                            numeroRegistro++;

                            //GERAR O DETALHE Q
                            string DetalheQ = "";
                            DetalheQ = GerarDetalheSegmentoQRemessa240(boleto, numeroRegistroLote);
                            strArchive += $"{DetalheQ}{Environment.NewLine}";
                            numeroRegistroLote++;
                            numeroRegistro++;
                            qtdTitulosTotal++;
                        }

                        ListaBoletosOK.Add(new ContratoParcelaBoleto
                        {
                            Cod = boleto.CodBoleto,
                            DataRemessa = DateTime.Now
                        });
                    }
                    catch (Exception ex)
                    {

                        ListaBoletosOK.Add(new ContratoParcelaBoleto
                        {
                            Cod = boleto.CodBoleto,
                            DataRemessa = DateTime.Now,
                            Erro = $"erro ao gerar detalhe da remessa: {ex.Message}"
                        });
                    }
                }
                #endregion

                #region GERAR O TRAILER LOTE (RODAPE LOTE)
                //GERAR O TRAILER LOTE (RODAPE LOTE)
                string _radapeLote = " ";
                _radapeLote = GerarTrailerLoteRemessa240(_banco.Codigo, numeroRegistro, vlTitulosTotal, qtdTitulosTotal);
                strArchive += $"{_radapeLote}{Environment.NewLine}";
                #endregion

                #region GERAR O TRAILER (RODAPE)
                //GERAR O TRAILER (RODAPE)
                string _radape = " ";
                _radape = GerarTrailerRemessa240(_banco.Codigo, numeroRegistro);
                strArchive += $"{_radape}{Environment.NewLine}";
                #endregion

                conteudo.Append($"{strArchive}");
            }
            catch (Exception ex)
            {
                throw new Exception("erro na remessa", ex);
            }
        }

        public void RegistrarBoletosProcessados(ref RemessaRetorno op)
        {
            Repository<ContratoParcelaBoleto> _repBoleto = new Repository<ContratoParcelaBoleto>();
            Repository<ContratoParcelaBoletoLote> _repBoletoLote = new Repository<ContratoParcelaBoletoLote>();
            #region Resgistrar boletos processados
            //Boletos com sucesso
            foreach (var item in op.BoletosProcessado.Where(x => x.Erro == null))
            {
                var entityBoleto = _repBoleto.FindById(item.Cod);
                ContratoParcelaBoletoLote lote = new ContratoParcelaBoletoLote();
                #region lote
                lote.CodLogArquivo = op.CodLogArquivo;
                var entityLote = _repBoletoLote.CreateOrUpdate(lote);
                #endregion

                entityBoleto.DataRemessa = item.DataRemessa;
                entityBoleto.CodUsuarioRemessa = op.CodUsuario;
                if (entityLote != null)
                    entityBoleto.CodLote = entityLote.Cod;
                _repBoleto.Update(entityBoleto);
            }
            //Boletos com erro
            foreach (var item in op.BoletosProcessado.Where(x => x.Erro != null))
            {
                var entityBoleto = _repBoleto.FindById(item.Cod);
                entityBoleto.DataRemessa = item.DataRemessa;
                entityBoleto.CodUsuarioRemessa = op.CodUsuario;
                entityBoleto.Erro = item.Erro;
                _repBoleto.Update(entityBoleto);
            }
            #endregion
        }

        private BoletoNet.Boleto ConvertModelDapperToBoletos(BoletoDapperModel dados, Cedente cedente)
        {
            BoletoNet.Boleto boleto = new BoletoNet.Boleto(Convert.ToDateTime(dados.DataVencimento), (decimal)dados.ValorDocumento, DbRotinas.ConverterParaString(cedente.Carteira), cedente.ContaBancaria.Conta, cedente);
            boleto.NumeroDocumento = dados.NumeroDocumento;

            boleto.CodBoleto = dados.CodBoleto;
            boleto.CompanhiaFiscal = DbRotinas.FormatCode(dados.CompanhiaFiscal, "0", 4, true);

            boleto.NossoNumero = dados.NossoNumero != null ? DbRotinas.ConverterParaString(dados.NossoNumero) : DbRotinas.ConverterParaString(dados.Cod);

            EspecieDocumento_Itau especie = new EspecieDocumento_Itau("99");
            boleto.EspecieDocumento = especie;
            boleto.Banco = new Banco((int)dados.BancoNumero);
            boleto.DataDocumento = (DateTime)dados.DataAprovacao;

            MontarSacado(dados, ref boleto);// PREENCHE INFORMAÇÔES DO SACADO
            boleto.DataDesconto = boleto.DataVencimento;


            //Validar a multa com otis.
            boleto.JurosMora = ((decimal)dados.MultaAtrasoPorcentagem / 10);

            //***********************INFORMACOES QUE DEVERAO FORMAR UMA CHAVE NO ARQUIVO REMESSA**********************
            boleto.Companhia = DbRotinas.FormatCode(dados.Companhia, "0", 4, true);
            boleto.CodCliente = DbRotinas.FormatCode(dados.CodCliente, "0", 8, true);
            boleto.NumeroParcela = DbRotinas.ConverterParaInt(dados.NumeroParcela);
            boleto.TipoDocumento = dados.ParcelaTipoFatura;
            boleto.Alterar = dados.Alterar ?? false;
            MontarInstruicao(dados, ref boleto);//OBTEM AS INSTRUÇÕES PARAMETRIZADAS

            return boleto;

        }

        /// <summary>
        /// Preencher informações do Sacado(Cliente)
        /// </summary>
        /// <param name="dados">BoletoDapperModel dados.</param>
        /// <returns>Retorna linha</returns>
        private void MontarSacado(BoletoDapperModel dados, ref BoletoNet.Boleto boleto)
        {
            try
            {
                #region Sacado
                Endereco sacadoEndereco = new Endereco();
                sacadoEndereco.End = dados.ClienteEnderecoEndereco ?? "--";
                sacadoEndereco.Numero = dados.ClienteEnderecoNumero ?? "--";
                sacadoEndereco.CEP = dados.ClienteEnderecoCEP ?? "--";
                sacadoEndereco.Bairro = dados.ClienteEnderecoBairro ?? "--";
                sacadoEndereco.Cidade = dados.ClienteEnderecoCidade ?? "--";
                sacadoEndereco.UF = dados.ClienteEnderecoUF ?? "--";
                sacadoEndereco.Complemento = dados.ClienteEnderecoComplemento ?? "--";

                var cpf = DbRotinas.Descriptografar(dados.ClienteCPF);
                if (cpf.Length < 11)
                {
                    int qtd = 11 - cpf.Length;
                    for (int i = 0; i < qtd; i++)
                    {
                        cpf = "0" + cpf;
                    }
                }
                if (cpf.Length > 11 && cpf.Length < 14)
                {
                    cpf = "0" + cpf;
                }
                var cliente = DbRotinas.Descriptografar(dados.ClienteNome);
                Sacado sacado = new Sacado(DbRotinas.ConverterParaString(cpf), DbRotinas.ConverterParaString(cliente), sacadoEndereco);
                #endregion

                boleto.Sacado = sacado;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro no Sacado .", ex);
            }
        }

        private void MontarInstruicao(BoletoDapperModel dados, ref BoletoNet.Boleto boleto)
        {
            try
            {
                // SE ESTIVER NA INSTRUCAO DO BANCO PORTADOR DEVE CALCULAR O JUROS E A MULTA APENAS PARA INFORMAR NO CAMPO Informacao de responsabilidade do beneficiador
                var _ValorJuros = ((((dados.JurosAoMesPorcentagem / 30) * 1) / 100) * DbRotinas.ConverterParaDecimal(dados.ValorDocumento));
                var _ValorMulta = (DbRotinas.ConverterParaDecimal(dados.ValorDocumento) * (dados.MultaAtrasoPorcentagem / 100));

                if (boleto.Instrucoes != null)
                {
                    foreach (var instrucao in boleto.Instrucoes)
                    {

                        boleto.Instrucoes.Add(
                            new Instrucao(341)
                            {
                                Descricao = instrucao.Descricao
                                                        .Replace("@DATA_VENCIMENTO", $"{dados.DataVencimento:dd/MM/yyyy}")
                                                        .Replace("@VR_JUROS_POR_DIA", $"{DbRotinas.ConverterParaDecimal(_ValorJuros):C2}")
                                                        .Replace("@VR_MULTA_ATRASO", $"{DbRotinas.ConverterParaDecimal(_ValorMulta):C2}")
                                                        .Replace("@%_JUROS_POR_DIA", $"{DbRotinas.ConverterParaDecimal(dados.JurosAoMesPorcentagem):N2}%")
                                                        .Replace("@%_MULTA_ATRASO", $"{DbRotinas.ConverterParaDecimal(dados.MultaAtrasoPorcentagem):N2}%")
                            }
                            );
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao ler detalhe do arquivo de .", ex);
            }
        }

        /// <summary>
        /// Preenche as informações do cedente (Regional/Filial)
        /// </summary>
        /// <param name="companhiaFiscal">Número da companhia fiscal da regional</param>
        /// <returns>Classe de Cedente preenchida</returns>
        private Cedente MontarCedente(TabelaBancoPortador _parametroCedente)
        {
            try
            {
                #region Cedente

                //Cedente cedente = new Cedente("48079274000144", "ELEVADORES OTIS LTDA", "2938", "00589", "5");
                Cedente cedente = new Cedente(_parametroCedente.CNPJ, _parametroCedente.Portador, _parametroCedente.Agencia, _parametroCedente.Conta, _parametroCedente.ContaDigito);
                cedente.Carteira = _parametroCedente.Carteira.ToString();
                //cedente.Codigo = Convert.ToString(_parametroCedente.CodigoCedente);
                cedente.Endereco = new Endereco();
                cedente.Endereco.End = _parametroCedente.Endereco != null ? _parametroCedente.Endereco : "";
                cedente.Endereco.Numero = _parametroCedente.EnderecoNumero != null ? _parametroCedente.EnderecoNumero : "";
                cedente.Endereco.Complemento = null;
                cedente.Endereco.Bairro = _parametroCedente.Bairro;
                cedente.Endereco.Cidade = _parametroCedente.Cidade;
                cedente.Endereco.UF = _parametroCedente.UF;
                cedente.Endereco.CEP = _parametroCedente.CEP;

                #endregion
                return cedente;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao ler detalhe do arquivo de .", ex);
            }
        }



        #region HEADER

        /// <summary>
        /// Gerar linha de Header
        /// </summary>
        /// <param name="cedente">classe preenchida de cedente.</param>
        /// <param name="nrBanco">Número do Banco</param>
        /// <param name="numeroArquivoRemessa">numero do lote do arquivo remessa</param>
        /// <returns>retorna o header</returns>
        private string GerarHeaderRemessaCNAB240(Cedente cedente, int nrBanco, int numeroArquivoRemessa)
        {
            try
            {
                #region Comentário
                //POS INI/FINAL    TAMANHO Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
                //------------------------------------------------------------------------------------------------------------------------------------------------
                //001 - 003	    3	    Z0012	        Nº do Banco da Companhia. 	        CODIGO DO BCO NA COMPENSACAO                
                //004 - 007	    4	    S0001	        UDV - Valor Definido pelo Us.       LOTE DE SERVICO                             0000
                //008 - 008        01	    Z0065           Tipo de Linha do Formatador         REGISTRO HEADER DO ARQUIVO                   
                //009 - 017        09	    S0002           Em branco                           USO EXCLUSIVO NEXXERA                        
                //018 - 018        01	    Z0001           Cód.Pessoa Fís./Jurídica            TIPO DE INSCRICAO DA EMPRESA                29739737000102
                //019 - 032        14	    S0001           UDV - Valor Definido pelo Us.       CNPJ EMPRESA DEBITADA                        
                //033 - 052        20	    S0002           Em branco                           CODIGO DO CONVENIO DO BANCO                  
                //053 - 057        05	    Z0003           Agência Bancária da Companhia       NUMERO DA AGENCIA DEBITADA                   
                //058 - 058        01	    Z0005           Dígito da Agência Banc.da Cia       DIGITO VERIFICADOR AGENCIA                   
                //059 - 070        12	    Z0004           Nº da Cta Bancária da Cia           NUMERO DA C/C DEBITADA                       
                //071 - 071        01	    S0002           Em branco                           DIGITO VERIFICADOR CONTA CORRENTE            
                //072 - 072        01	    Z0006           Dígito da Conta Banc.da Cia         DAC DA AGENCIA/CTA DEBITADA                  
                //073 - 102        30	    Z0009           Nome da Companhia                   NOME DA EMPRESA                              
                //103 - 110        08	    Z0013           Nome do Banco da Companhia          NOME DO BANCO                                
                //111 - 132        22	    S0002           Em branco                           NOME DO BANCO                               NEXXERA
                //133 - 142        10	    S0001           UDV - Valor Definido pelo Us.       COMPLEMENTO DE REGISTROS                    1
                //143 - 143        01	    S0001           UDV - Valor Definido pelo Us.       CODIGO REMESSA/RETORNO                       
                //144 - 151        08	    Z0010           Data Criação do Arq.Formatado       DATA DA GERACAO DO ARQUIVO                   
                //152 - 157        06	    Z0011           Hora Criação do Arq.Formatado       HORA DE GERACAO DO ARQUIVO                   
                //158 - 164        07	    Z0058           Nº do Arquivo Bancário              NUMERO SEQUENCIAL DO ARQUIVO                
                //165 - 167        03	    S0001           UDV - Valor Definido pelo Us.       NUMERO DA VERSAO DO LAYOUT                  020
                //168 - 172        05	    S0001           UDV - Valor Definido pelo Us.       DENSIDADE DA GRAVACAO DO ARQUIVO            01600 
                //173 - 191        19	    S0002           Em branco                           PARA USO RESERVADO DO BANCO                  
                //192 - 211        20	    S0002           Em branco                           PARA USO RESERVADO DA EMPRESA                
                //212 - 221        10	    S0002           Em branco                           OBSERVACAO DO LAYOUT DO BANCO                
                //222 - 240        19      S0002	        Em branco                           USO EXCLUSIVO NEXXERA
                #endregion

                string header = Utils.FormatCode(nrBanco.ToString(), "0", 3, true); ;  // Nº do Banco da Companhia. / CODIGO DO BCO NA COMPENSACAO 
                header += "0000";    // UDV - Valor Definido pelo Us./ LOTE DE SERVICO / 0000
                header += "0";   // Tipo de Linha do Formatador         REGISTRO HEADER DO ARQUIVO                   
                header += Utils.FormatCode("", " ", 9);   // Em branco / USO EXCLUSIVO NEXXERA
                header += (cedente.CPFCNPJ.Length == 11 ? "1" : "2");   // Cód.Pessoa Fís./Jurídica / TIPO DE INSCRICAO DA EMPRESA / 29739737000102                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                header += Utils.FormatCode(cedente.CPFCNPJ, "0", 14, true);  // UDV - Valor Definido pelo Us. / CNPJ EMPRESA DEBITADA                        
                header += Utils.FormatCode("", " ", 20);
                header += "0";    // Em branco / CODIGO DO CONVENIO DO BANCO                                                                                                       
                header += Utils.FormatCode(cedente.ContaBancaria.Agencia, " ", 4, true); // Agência Bancária da Companhia / NUMERO DA AGENCIA DEBITADA                                          
                header += " ";   // Dígito da Agência Banc.da Cia / DIGITO VERIFICADOR AGENCIA                   
                header += "0000000";
                header += Utils.FormatCode(cedente.ContaBancaria.Conta, "0", 5, true);// Nº da Cta Bancária da Cia / NUMERO DA C/C DEBITADA      
                header += " ";  // Em branco DIGITO VERIFICADOR CONTA CORRENTE                                                                                                         
                header += Utils.FormatCode(String.IsNullOrEmpty(cedente.ContaBancaria.DigitoConta) ? " " : cedente.ContaBancaria.DigitoConta, " ", 1, true);// Dígito da Conta Banc.da Cia         DAC DA AGENCIA/CTA DEBITADA                                   
                header += Utils.FitStringLength(cedente.Nome, 30, 30, ' ', 0, true, true, false);// Nome da Companhia / NOME DA EMPRESA                                 
                header += Utils.FormatCode("BCO ITAU", " ", 8); // Nome do Banco da Companhia / NOME DO BANCO                                                                                         
                header += Utils.FormatCode("", " ", 22);//Nome do Banco da Companhia / NOME DO BANCO
                header += Utils.FormatCode("NEXXERA", " ", 10);// Em branco / NOME DO BANCO / NEXXERA                                                               
                header += "1";// UDV - Valor Definido pelo Us. / COMPLEMENTO DE REGISTROS / 1                                                                                                                                              
                header += DateTime.Now.ToString("ddMMyyyyHHmmss");// Data Criação do Arq.Formatado / DATA DA GERACAO DO ARQUIVO // Hora Criação do Arq.Formatado / HORA DE GERACAO DO ARQUIVO                 
                header += Utils.FormatCode(numeroArquivoRemessa.ToString(), "0", 7, true); //  Nº do Arquivo Bancário / NUMERO SEQUENCIAL DO ARQUIVO / 
                header += "020"; // Nº do Arquivo Bancário / NUMERO DA VERSAO DO LAYOUT / 020
                header += "01600"; // UDV - Valor Definido pelo Us. / NUMERO DA VERSAO DO LAYOUT / 01600
                header += Utils.FormatCode("", " ", 19);//Em branco / PARA USO RESERVADO DO BANCO /         
                header += Utils.FormatCode("", " ", 20);//Em branco / PARA USO RESERVADO DA EMPRESA /
                header += Utils.FormatCode("", " ", 10);//Em branco / OBSERVACAO DO LAYOUT DO BANCO /
                header += Utils.FormatCode("", " ", 19);//Em branco / USO EXCLUSIVO NEXXERA /
                header = Utils.SubstituiCaracteresEspeciais(header);


                return header;                                                                                                                                             // Em branco                           USO EXCLUSIVO NEXXERA

            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar HEADER do arquivo de remessa do CNAB240.", ex);
            }
        }

        /// <summary>
        ///POS INI/FINAL    TAM     Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
        ///------------------------------------------------------------------------------------------------------------------------------------------------
        ///001 - 003        03	    Z0012           Nº do Banco da Companhia            CODIGO DO BANCO NA COMPENSACAO	 
        ///004 - 007        04	    S0001           UDV - Valor Definido pelo           Us.LOTE IDENTIFICACAO DE PAGTOS             0001
        ///008 - 008        01	    S0001           UDV - Valor Definido pelo Us. 	    REGISTRO HEADER DE LOTE	                    1
        ///009 - 009        01	    S0001           UDV - Valor Definido pelo Us. 	    TIPO DE OPERACAO                            R
        ///010 - 011        02	    S0001           UDV - Valor Definido pelo Us. 	    TIPO DE PAGTO                               01
        ///012 - 013        02	    S0002           Em branco                           USO EXCLUSIVO NEXXERA	 
        ///014 - 016        03	    S0001           UDV - Valor Definido pelo Us. 	    N.VERSAO DO LAYOUT DO LOTE                  010
        ///017 - 017        01	    S0002           Em branco USO                       EXCLUSIVO NEXXERA	 
        ///018 - 018        01	    Z0001           Cód. Pessoa Fís./Jurídica           TIPO INSCRICAO EMPRESA DEBITADA
        ///019 - 033        15	    S0001           UDV - Valor Definido pelo Us. 	    CNPJ EMPRESA DEBITADA   29739737000102
        ///034 - 053        20	    S0002           Em branco                           CODIGO CONVERNIO BANCO	 
        ///054 - 058        05	    Z0003           Agência Bancária da Companhia       NUMERO AGENCIA DEBITADA	 
        ///059 - 059        01	    Z0005           Dígito da Agência Banc.da Cia       DIGITO VERIFICADOR DA AGENCIA	 
        ///060 - 071        12	    Z0004           Nº da Cta Bancária da Cia           NUMERO DE C/C DEBITADA	 
        ///072 - 072        01	    S0002           Em branco                           DIGITO VERIFICADOR DA CONTA
        ///073 - 073        01	    Z0006           Dígito da Conta Banc.da Cia         DAC DA AGENCIA/CTA DEBITADA	 
        ///074 - 103        30	    Z0009           Nome da Companhia                   NOME DA EMPRESA
        ///104 - 143        40	    S0002           Em branco                           MENSAGEM 1	 
        ///144 - 183        40	    S0002           Em branco                           MENSAGEM 2	 
        ///184 - 191        08	    Z0058           Nº do Arquivo Bancário              NUMERO DO RETORNO
        ///192 - 199        08	    Z0010           Data Criação do Arq.Formatado       DATA GERACAO DO ARQUIVO
        ///200 - 207        08	    S0003           Zero                                DATA CREDITO 
        ///208 - 214        07	    S0002           Em branco                           CODIGO MODELO PERSONALIZADO	 
        ///215 - 240        26	    S0002           Em branco                           USO EXCLUSIVO NEXXERA
        /// </summary>
        /// <param name="cedente">classe preenchida de cedente.</param>
        /// <param name="nrBanco">Número do Banco</param>
        /// <param name="numeroArquivoRemessa">numero do lote do arquivo remessa</param>
        /// <returns>retorna o header</returns>
        public string GerarHeaderLoteRemessaCNAB240(Cedente cedente, int nrBanco, int numeroArquivoRemessa)
        {
            try
            {
                string header = Utils.FormatCode(nrBanco.ToString(), "0", 3, true);               //Nº do Banco da Companhia          | CODIGO DO BANCO NA COMPENSACAO	       |  
                header += Utils.FormatCode("0001", "0", 4, true);                                //UDV - Valor Definido pelo         | Us.LOTE IDENTIFICACAO DE PAGTOS         |    0001
                header += Utils.FormatCode("1", " ", 1, true);                                   //UDV - Valor Definido pelo Us. 	 | REGISTRO HEADER DE LOTE	               |    1
                header += Utils.FormatCode("R", " ", 1, true);                                   //UDV - Valor Definido pelo Us. 	 | TIPO DE OPERACAO                        |    R
                header += Utils.FormatCode("01", "0", 2, true);                                  //UDV - Valor Definido pelo Us. 	 | TIPO DE PAGTO                           |    01
                header += Utils.FormatCode("", " ", 2);                                          //Em branco                         | USO EXCLUSIVO NEXXERA	               | 
                header += Utils.FormatCode("010", " ", 3);                                       //UDV - Valor Definido pelo Us. 	 | N.VERSAO DO LAYOUT DO LOTE              |    010
                header += Utils.FormatCode("", " ", 1);                                          //Em branco USO                     | EXCLUSIVO NEXXERA	                   | 
                header += (cedente.CPFCNPJ.Length == 11 ? "1" : "2");                            //Cód. Pessoa Fís./Jurídica         | TIPO INSCRICAO EMPRESA DEBITADA         | 
                header += Utils.FormatCode(cedente.CPFCNPJ, "0", 15, true);                      //UDV - Valor Definido pelo Us. 	 | CNPJ EMPRESA DEBITADA   29739737000102  | 
                header += Utils.FormatCode("", " ", 20);                                         //Em branco                         | CODIGO CONVERNIO BANCO	               | 
                header += Utils.FormatCode(cedente.ContaBancaria.Agencia, "0", 5, true);         //Agência Bancária da Companhia     | NUMERO AGENCIA DEBITADA	               | 
                header += Utils.FormatCode("", " ", 1);                                          //Dígito da Agência Banc.da Cia     | DIGITO VERIFICADOR DA AGENCIA	       | 
                header += Utils.FormatCode(cedente.ContaBancaria.Conta, "0", 12, true);          //Nº da Cta Bancária da Cia         | NUMERO DE C/C DEBITADA	               | 
                header += Utils.FormatCode("", " ", 1);                                          //Em branco                         | DIGITO VERIFICADOR DA CONTA             | 
                header += Utils.FormatCode(String.IsNullOrEmpty(cedente.ContaBancaria.DigitoConta) ? " " : cedente.ContaBancaria.DigitoConta, " ", 1); //Dígito da Conta Banc.da Cia       | DAC DA AGENCIA/CTA DEBITADA	           | 
                header += Utils.FitStringLength(cedente.Nome, 30, 30, ' ', 0, true, true, false);//Nome da Companhia                 | NOME DA EMPRESA                         | 
                header += Utils.FormatCode("", " ", 40);                                         //Em branco   | MENSAGEM 1 | 
                header += Utils.FormatCode("", " ", 40);                                         //Em branco                         | MENSAGEM 2	                           | 
                header += Utils.FormatCode(numeroArquivoRemessa.ToString(), "0", 8, true);       //Nº do Arquivo Bancário            | NUMERO DO RETORNO                       | 
                header += DateTime.Now.ToString("ddMMyyyy");                                     //Data Criação do Arq.Formatado     | DATA GERACAO DO ARQUIVO                 | 
                header += Utils.FormatCode("", "0", 8);                                          //Zero                              | DATA CREDITO                            | 
                header += Utils.FormatCode("", " ", 7);                                          //Em branco                         | CODIGO MODELO PERSONALIZADO	           | 
                header += Utils.FormatCode("", " ", 26);                                         //Em branco                         | USO EXCLUSIVO NEXXERA                   | 
                header = Utils.SubstituiCaracteresEspeciais(header);

                return header;                                                                                                                                             // Em branco                           USO EXCLUSIVO NEXXERA

            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar HEADER do arquivo de remessa do CNAB240.", ex);
            }
        }


        #endregion

        #region DETALHE 
        #region NOVO BOLETO
        private string GerarDetalheSegmentoPRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoP;
                _segmentoP = "341";                                                                                                 //03    Z0012   Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO
                _segmentoP += "0001";                                                                                               //04    S0001   UDV -Valor Definido pelo Us.        LOTE DE SERVICO                             0001
                _segmentoP += "3";                                                                                                  //01    Z0065   Tipo de Linha do Formatador         REGISTRO DETALHE DE LOTE
                _segmentoP += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true);                     //05    Z0062   Detalhe/ Seqüência no Segmento      N.SEQUENCIAL REGISTRO LOTE
                _segmentoP += "P";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO SEGMENTO REG DETALHE                 P
                _segmentoP += " ";                                                                                                  //01    S0002   Em branco                           USO EXCLUSIVO NEXXERA
                _segmentoP += ObterCodigoDaOcorrencia(boleto);                                                                      //02    Z0071   Código de Envio do Banco            TIPO DE MOVIMENTO
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Agencia, 6, 6, '0', 0, true, true, true);          //06    Z0003   Agência Bancária da Companhia       Agência Bancária da Companhia
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Conta, 12, 12, '0', 0, true, true, true);          //12    Z0004   Nº da Cta Bancária da Cia NUMERO    DA CONTA CORRENTE
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);//01    Z0006   Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA CONTA
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);//01    Z0006   Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA AG/ CONTA
                _segmentoP += Utils.FitStringLength(boleto.NossoNumero, 20, 20, ' ', 1, true, true, false);                     //20    Z0067   Nº da Duplicata Bancária            IDENTIFICACAO DO TITULO NO BANCO
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO CARTEIRA                             1
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        FORMA DE CADASTRO TITULO BANCO              1
                _segmentoP += "2";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        TIPO DE DOCUMENTO                           2
                _segmentoP += Utils.FitStringLength(boleto.NumeroDocumento, 1, 1, ' ', 1, true, true, false);                     //01    Z0086   Geração do Boleto                   IDENT. EMISSAO BOLETO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                                            //01    Z0086   Geração do Boleto                   IDENT. DA DISTRIBUICAO / ENTREGA
                _segmentoP += Utils.FitStringLength(boleto.NumeroDocumento, 15, 15, '0', 0, true, true, true);                      //15    Z0068   Nº do Documento                     N. DO DOCUMENTO DE COBRANCA
                _segmentoP += Utils.FitStringLength(boleto.DataVencimento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);   //08    Z0049   Data de Vencimento                  DATA DE VENCIMENTO DO TITULO
                _segmentoP += Utils.FitStringLength(boleto.ValorBoleto.ApenasNumeros(), 15, 15, '0', 0, true, true, true);          //15    Z5835   Payment Amount - Withholding        VALOR DO PAGAMENTO
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.Agencia) ? " " : boleto.Cedente.ContaBancaria.Agencia, "0", 5, true); //05    Z0003   Agência Bancária da Companhia       AGENCIA ENCARREGADA DE COBRANCA
                _segmentoP += " ";                                                                                                  //01    Z0005   Dígito da Agência Banc.da Cia       DIGITO VERIFICADOR DA AGENCIA
                _segmentoP += "02";                                                                                                 //02    S0001   UDV -Valor Definido pelo Us.        ESPECIE DE TITULO                           02
                _segmentoP += "N";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        IDENT TITULOS ACEITO / NAO ACEITO           N
                _segmentoP += Utils.FitStringLength(boleto.DataDocumento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);    //08    Z0069   Data da Fatura                      DATA DA EMISSAO DO TITULO
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO DO JUROS DE MORA                     1
                _segmentoP += Utils.FitStringLength(boleto.DataVencimento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);   //08    Z0049   Data de Vencimento                  DATA DE JUROS DE MORA
                _segmentoP += Utils.FitStringLength(boleto.JurosMora.ApenasNumeros(), 15, 15, '0', 0, true, true, true);            //15    Z0075   Juros Diários                       JUROS DE MORA POR DIA/ TAXA
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO DE DESCONTO 1                        1
                _segmentoP += Utils.FitStringLength(boleto.DataDesconto.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);     //08    Z0076   Data de Vcto c / Desconto -         C / R   DATA DO DESCONTO                    1
                _segmentoP += Utils.FitStringLength(boleto.ValorDesconto.ApenasNumeros(), 15, 15, '0', 0, true, true, true);        //15    Z0051   Desconto Obtido                     VALOR/ PERCENTUAL A SER CONCEDIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                                           //15    S0003   Zero                                VALOR DO IOF A SER RECOLHIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                                           //15    S0003   Zero                                VALOR DO ABATIMENTO
                //***********************INFORMACOES QUE DEVERAO FORMAR UMA CHAVE NO ARQUIVO REMESSA********************** //25    Z0070   Código de Usuário do Cliente        IDENTIFICACAO DO TITULO NA EMPRESSA
                //Companhia -- Ficou de verificar
                _segmentoP += Utils.FitStringLength(boleto.CompanhiaFiscal, 5, 5, '0', 0, true, true, true) +
                //Tipo DOcumento
                Utils.FitStringLength(boleto.TipoDocumento.ToString(), 2, 2, '0', 0, true, true, true) +
                //NumeroLinha
                //Utils.FitStringLength(boleto.NumeroParcela.ToString(), 2, 2, '0', 0, true, true, true) +
                //FluxoPagamento
                Utils.FitStringLength(boleto.NumeroDocumento, 10, 10, '0', 0, true, true, true) +
                //CodCliente
                Utils.FitStringLength(boleto.CodCliente, 8, 8, '0', 0, true, true, true);
                //********************************************************************************************************
                _segmentoP += Utils.FitStringLength("3", 1, 1, '0', 0, true, true, true);                                           //01    S0001   UDV -Valor Definido pelo            Us.CODIGO DE PROTESTO                       3
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                                            //02    Z0074   Dias para Protesto                  NUMERO DE DIAS PARA PROTESTO
                _segmentoP += "1";                                                                                                  //01    S0001   UDV -Valor Definido pelo Us.        CODIGO PARA BAIXA / DEVOLUCAO               1
                _segmentoP += "001";                                                                                                //03    S0001   UDV -Valor Definido pelo Us.        NUMERO DE DIAS PARA BAIXA / DEVOLUCAO       001
                _segmentoP += "09";                                                                                                 //02    S0001   UDV -Valor Definido pelo Us.        CODIGO DA MOEDA                             09
                _segmentoP += Utils.FitStringLength("", 10, 10, '0', 0, true, true, true);                                          //10    S0003   Zero                                NUMERO DO CONTRATO DA OPER.DE CRED.	 
                _segmentoP += " ";
                _segmentoP = Utils.SubstituiCaracteresEspeciais(_segmentoP);

                return _segmentoP;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }
        private string GerarDetalheSegmentoQRemessa240(BoletoNet.Boleto boleto, int numeroRegistro)
        {
            try
            {
                string _zeros16 = new string('0', 16);
                string _brancos10 = new string(' ', 10);
                string _brancos28 = new string(' ', 28);
                string _brancos40 = new string(' ', 40);

                string _segmentoQ;

                _segmentoQ = "341";
                _segmentoQ += "0001";
                _segmentoQ += "3";
                _segmentoQ += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true);
                _segmentoQ += "Q";
                _segmentoQ += " ";

                _segmentoQ += ObterCodigoDaOcorrencia(boleto);
                if (boleto.Sacado.CPFCNPJ.Length <= 11)
                    _segmentoQ += "1";
                else
                    _segmentoQ += "2";

                _segmentoQ += Utils.FitStringLength(boleto.Sacado.CPFCNPJ, 15, 15, '0', 0, true, true, true);
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Nome.TrimStart(' '), 30, 30, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += "          ";
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.End.TrimStart(' '), 40, 40, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.Bairro.TrimStart(' '), 15, 15, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.CEP, 8, 8, ' ', 0, true, true, false).ToUpper(); ;
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.Cidade.TrimStart(' '), 15, 15, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Endereco.UF, 2, 2, ' ', 0, true, true, false).ToUpper();
                if (boleto.Sacado.CPFCNPJ.Length <= 11)
                    _segmentoQ += "1";
                else
                    _segmentoQ += "2";

                _segmentoQ += Utils.FitStringLength(boleto.Sacado.CPFCNPJ, 15, 15, '0', 0, true, true, true);
                _segmentoQ += Utils.FitStringLength(boleto.Sacado.Nome.TrimStart(' '), 30, 30, ' ', 0, true, true, false).ToUpper();
                _segmentoQ += _brancos10;
                _segmentoQ += "000";
                //***********************INFORMACOES QUE DEVERAO FORMAR UMA CHAVE NO ARQUIVO REMESSA********************** //25    Z0070   Código de Usuário do Cliente        IDENTIFICACAO DO TITULO NA EMPRESSA
                //Companhia
                _segmentoQ += Utils.FitStringLength(boleto.CompanhiaFiscal, 5, 5, '0', 0, true, true, true) +
                //Tipo DOcumento
                Utils.FitStringLength(boleto.TipoDocumento.ToString(), 2, 2, '0', 0, true, true, true) +
                //NumeroLinha
                //Utils.FitStringLength(boleto.NumeroParcela.ToString(), 2, 2, '0', 0, true, true, true) +
                //FluxoPagamento
                Utils.FitStringLength(boleto.NumeroDocumento, 10, 10, '0', 0, true, true, true) +
                //CodCliente
                Utils.FitStringLength(boleto.CodCliente, 3, 3, '0', 0, true, true, true);
                //********************************************************************************************************
                _segmentoQ += Utils.FitStringLength("", 5, 5, ' ', 0, true, true, false);
                _segmentoQ += "109";

                _segmentoQ = Utils.SubstituiCaracteresEspeciais(_segmentoQ);

                return _segmentoQ;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO Q DO DETALHE do arquivo de REMESSA.", ex);
            }
        }
        #endregion

        #region ALTERACAO
        private string GerarDetalheAlterarcaoDadosSegmentoPRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoP;
                _segmentoP = "341";                                                                                   //3   Z0012 Nº do Banco da Companhia        CODIGO BANCO NA COMPENSACAO                             
                _segmentoP += "0001";                                                                                 //4   S0001 UDV -Valor Definido pelo Us.    LOTE DE SERVICO
                _segmentoP += "3";                                                                                    //1   S0001 UDV -Valor Definido pelo Us.    REGISTRO DETALHE DE LOTE
                _segmentoP += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true);       //5   Z0062 Detalhe/ Seqüência no Segmento  N.SEQUENCIAL REGISTRO LOTE
                _segmentoP += "P";                                                                                    //1   S0001 UDV -Valor Definido pelo Us.    CODIGO SEGMENTO REG DETALHE
                _segmentoP += " ";                                                                                    //1   S0002 Em branco                       USO EXCLUSIVO NEXXERA
                _segmentoP += "31"; //ALTERAÇÃO DE OUTROS DADOS                                                       //2   S0001 UDV -Valor Definido pelo Us.    TIPO DE MOVIMENTO
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Agencia, 5, 5, '0', 0, true, true, true); //5   Z0003 Agência Bancária da Companhia   AGÊNCIA BANCÁRIA DA COMPANHIA
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true); //1   Z0006 Dígito da Conta Banc.da Cia     DIGITO VERIFICADOR DA AGÊNCIA
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Conta, 12, 12, '0', 0, true, true, true);//12  Z0004 Nº da Cta Bancária da Cia       NUMERO DA CONTA CORRENTE
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);//1   Z0006 Dígito da Conta Banc.da Cia     DIGITO VERIFICADOR DA CONTA
                _segmentoP += "1";                                                                                    //1   Z0006 Dígito da Conta Banc.da Cia     DIGITO VERIFICADOR DA AG/ CONTA
                _segmentoP += Utils.FitStringLength(boleto.NossoNumero, 20, 20, ' ', 1, true, true, false);           //20  Z0067 Nº da Duplicata Bancária        IDENTIFICACAO DO TITULO NO BANCO
                _segmentoP += "1";                                                                                    //1   S0001 UDV -Valor Definido pelo Us.    CODIGO CARTEIRA
                _segmentoP += "0";                                                                                    //1   S0003 Zero                            FORMA DE CADASTRO TITULO BANCO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       TIPO DE DOCUMENTO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       IDENT. EMISSAO BOLETO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       IDENT. DA DISTRIBUICAO/ ENTREGA
                _segmentoP += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                           //15  S0002 Em branco                       N. DO DOCUMENTO DE COBRANCA
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DE VENCIMENTO DO TITULO
                _segmentoP += Utils.FitStringLength(boleto.ValorBoleto.ApenasNumeros(), 15, 15, '0', 0, true, true, true); //15  Z60C2 Payment Amount - Withholding    VALOR DO PAGAMENTO
                _segmentoP += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                              //5   S0003 Zero                            AGENCIA ENCARREGADA DE COBRANCA
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco DIGITO VERIFICADOR DA AGENCIA
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                              //2   S0003 Zero                            ESPECIE DE TITULO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco IDENT TITULOS ACEITO/ NAO ACEITO
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DA EMISSAO DO TITULO
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO DO JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DE JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            JUROS DE MORA POR DIA / TAXA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO DE DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                              //8   S0003 Zero                            DATA DO DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            VALOR / PERCENTUAL A SER CONCEDIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            VALOR DO IOF A SER RECOLHIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                            //15  S0003 Zero                            VALOR DO ABATIMENTO
                _segmentoP += Utils.FitStringLength("", 25, 25, ' ', 0, true, true, false);                           //25  S0002 Em branco                       IDENTIFICACAO DO TITULO NA EMPRESSA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO DE PROTESTO
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                              //2   S0003 Zero                            NUMERO DE DIAS PARA PROTESTO
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                              //1   S0003 Zero                            CODIGO PARA BAIXA / DEVOLUCAO
                _segmentoP += Utils.FitStringLength("", 3, 3, ' ', 0, true, true, false);                             //3   S0002 Em branco                       NUMERO DE DIAS PARA BAIXA/ DEVOLUCAO
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                              //2   S0003 Zero                            CODIGO DA MOEDA
                _segmentoP += Utils.FitStringLength("", 10, 10, '0', 0, true, true, true);                            //10  S0003 Zero                            NUMERO DO CONTRATO DA OPER.DE CRED.
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                             //1   S0002 Em branco                       USO EXCLUSIVO NEXXERA                                                    

                _segmentoP += " ";
                _segmentoP = Utils.SubstituiCaracteresEspeciais(_segmentoP);

                return _segmentoP;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        private string GerarDetalheAlterarcaoDadosSegmentoQRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoQ;
                _segmentoQ = "341";                                                                             //3	    Z0012 Nº do Banco da Companhia          CODIGO BANCO NA COMPENSACAO
                _segmentoQ += Utils.FitStringLength("1", 4, 4, '0', 0, true, true, true);                       //4	    S0001 UDV - Valor Definido pelo Us.     LOTE DE SERVICO
                _segmentoQ += Utils.FitStringLength("3", 1, 1, '0', 0, true, true, true);                       //1	    S0001 UDV - Valor Definido pelo Us. 	REGISTRO DETALHE DE LOTE
                _segmentoQ += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true); //5	    Z0062 Detalhe/Seqüência no Segmento     N.SEQUENCIAL REGISTRO LOTE
                _segmentoQ += "Q";                                                                              //1	    S0001 UDV - Valor Definido pelo Us. 	CODIGO SEGMENTO REG DETALHE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1	    S0002 Em branco                         USO EXCLUSIVO NEXXERA
                _segmentoQ += "31"; //ALTERAÇÃO DE OUTROS DADOS                                                 //2	    S0001 UDV - Valor Definido pelo Us. 	TIPO DE MOVIMENTO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1	    S0003 Zero                              TIPO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15	S0003 Zero                              NUMERO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40	S0002 Em branco                         NOME DO SACADOR
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40	S0002 Em branco                         ENDERECO - SACADO
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15	S0002 Em branco                         BAIRRO
                _segmentoQ += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                        //5	    S0003 Zero                              CEP Benefic./Pagador - 5 díg.
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3	    S0003 Zero                              CEP Benefic./Pagador - 3 díg.
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15	S0002 Em branco                         CIDADE DO BENEFIC./PAGADOR
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2	    S0002 Em branco                         UNIDADE DE FEDERACAO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1	    S0003 Zero                              CÓD.PESSOA FÍS./JUR.BEN./PAG
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15	S0003 Zero                              CPF/CNPJ DO BENEFIC./PAGADOR
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40	S0002 Em branco                         NOME DO SACADOR / AVALISTA
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3	    S0003 Zero                              Nº BANCO DO BENEFIC./PAGADOR
                _segmentoQ += Utils.FitStringLength("", 20, 20, ' ', 0, true, true, false);                     //20	S0002 Em branco                         NOSSO N BANCO CORRESPONDENTE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1	    S0002 Em branco                         FORMA DE ACESSO AO BOLETO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2	    S0002 Em branco                         PRIMEIRA INSTRUCAO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2	    S0002 Em branco                         SEGUNDA INSTRUCAO
                _segmentoQ += Utils.FitStringLength(boleto.Carteira, 3, 3, '0', 0, true, true, true);           //3	    S0001 UDV - Valor Definido pelo Us. 	VARIACAO DA CARTEIRA / CARTEIRA                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                

                _segmentoQ += " ";
                _segmentoQ = Utils.SubstituiCaracteresEspeciais(_segmentoQ);

                return _segmentoQ;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        private string GerarDetalheAlterarcaoVencimentoSegmentoPRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoP;
                _segmentoP = "341";                                                                             //3   Z0012 Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO                                                                                        
                _segmentoP += "0001";                                                                           //4   S0001 UDV -Valor Definido pelo Us.        LOTE DE SERVICO
                _segmentoP += "3";                                                                              //1   S0001 UDV -Valor Definido pelo Us.        REGISTRO DETALHE DE LOTE
                _segmentoP += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true); //5   Z0062 Detalhe/ Seqüência no Segmento      N.SEQUENCIAL REGISTRO LOTE
                _segmentoP += "P";                                                                              //1   S0001 UDV -Valor Definido pelo Us.        CODIGO SEGMENTO REG DETALHE
                _segmentoP += " ";                                                                              //1   S0002 Em branco                           USO EXCLUSIVO NEXXERA
                _segmentoP += "06"; //ALTERACAO DE VENCIMENTO                                                   //2   S0001 UDV -Valor Definido pelo Us.        TIPO DE MOVIMENTO                   
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Agencia, 5, 5, '0', 0, true, true, true); //5   Z0003 Agência Bancária da Companhia       AGÊNCIA BANCÁRIA DA COMPANHIA        
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);  //1   Z0006 Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA AGÊNCIA
                _segmentoP += Utils.FitStringLength(boleto.Cedente.ContaBancaria.Conta, 12, 12, '0', 0, true, true, true);  //12  Z0004 Nº da Cta Bancária da Cia           NUMERO DA CONTA CORRENTE
                _segmentoP += Utils.FormatCode(String.IsNullOrEmpty(boleto.Cedente.ContaBancaria.DigitoConta) ? " " : boleto.Cedente.ContaBancaria.DigitoConta, " ", 1, true);  //1   Z0006 Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA CONTA
                _segmentoP += "5";                                                                              //1   Z0006 Dígito da Conta Banc.da Cia         DIGITO VERIFICADOR DA AG/ CONTA                        
                _segmentoP += Utils.FitStringLength(boleto.NossoNumero, 20, 20, ' ', 1, true, true, false);     //20  Z0067 Nº da Duplicata Bancária            IDENTIFICACAO DO TITULO NO BANCO                        
                _segmentoP += "1";                                                                              //1   S0001 UDV -Valor Definido pelo Us.        CODIGO CARTEIRA                        
                _segmentoP += "0";                                                                              //1   S0003 Zero                                FORMA DE CADASTRO TITULO BANCO                        
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                        //1   S0002 Em branco                           TIPO DE DOCUMENTO              
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                        //1   S0002 Em branco                           IDENT. EMISSAO BOLETO                    
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, true);                        //1   S0002 Em branco                           IDENT. DA DISTRIBUICAO/ ENTREGA                    
                _segmentoP += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15  S0002 Em branco                           N. DO DOCUMENTO DE COBRANCA
                _segmentoP += Utils.FitStringLength(boleto.DataVencimento.ToString("ddMMyyyy"), 8, 8, ' ', 0, true, true, false);//8   Z60C1 Data de Vencimento DATA DE VENCIMENTO DO TITULO                
                _segmentoP += Utils.FitStringLength(boleto.ValorBoleto.ApenasNumeros(), 15, 15, '0', 0, true, true, true);//15  Z60C2 Payment Amount - Withholding  VALOR DO PAGAMENTO
                _segmentoP += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                        //5   S0003 Zero                                AGENCIA ENCARREGADA DE COBRANCA
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1   S0002 Em branco                           DIGITO VERIFICADOR DA AGENCIA
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                        //2   S0003 Zero                                ESPECIE DE TITULO
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1   S0002 Em branco                           IDENT TITULOS ACEITO/ NAO ACEITO
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                       //8   S0003 Zero                                DATA DA EMISSAO DO TITULO   
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                       //1   S0003 Zero                                CODIGO DO JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                        //8   S0003 Zero                                DATA DE JUROS DE MORA
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                JUROS DE MORA POR DIA / TAXA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1   S0003 Zero                                CODIGO DE DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 8, 8, '0', 0, true, true, true);                        //8   S0003 Zero                                DATA DO DESCONTO 1
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                VALOR / PERCENTUAL A SER CONCEDIDO
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                VALOR DO IOF A SER RECOLHIDO  
                _segmentoP += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15  S0003 Zero                                VALOR DO ABATIMENTO
                _segmentoP += Utils.FitStringLength("", 25, 25, ' ', 0, true, true, false);                     //25  Z0070   Código de Usuário do Cliente      IDENTIFICACAO DO TITULO NA EMPRESSA
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1   S0003 Zero            CODIGO DE PROTESTO                               
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                        //2   S0003 Zero                                NUMERO DE DIAS PARA PROTESTO                                   
                _segmentoP += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1   S0003 Zero                                CODIGO PARA BAIXA / DEVOLUCAO                                
                _segmentoP += Utils.FitStringLength("", 3, 3, ' ', 0, true, true, false);                       //3   S0002 Em branco                           NUMERO DE DIAS PARA BAIXA/ DEVOLUCAO                                
                _segmentoP += Utils.FitStringLength("", 2, 2, '0', 0, true, true, true);                        //2   S0003 Zero                                CODIGO DA MOEDA                                
                _segmentoP += Utils.FitStringLength("", 10, 10, '0', 0, true, true, true);                      //10  S0003 Zero                                NUMERO DO CONTRATO DA OPER.DE CRED.
                _segmentoP += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                        //1   S0002 Em branco                           USO EXCLUSIVO NEXXERA

                _segmentoP += " ";
                _segmentoP = Utils.SubstituiCaracteresEspeciais(_segmentoP);

                return _segmentoP;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }

        private string GerarDetalheAlterarcaoVencimentoSegmentoQRemessa240(BoletoNet.Boleto boleto, int numeroRegistro, string numeroConvenio)
        {
            try
            {
                string _segmentoQ;
                _segmentoQ = "341";                                                                             //3     Z0012 Nº do Banco da Companhia          CODIGO BANCO NA COMPENSACAO
                _segmentoQ += Utils.FitStringLength("1", 4, 4, '0', 0, true, true, true);                       //4     S0001 UDV -Valor Definido pelo Us.      LOTE DE SERVICO
                _segmentoQ += Utils.FitStringLength("3", 1, 1, '0', 0, true, true, true);                       //1     S0001 UDV -Valor Definido pelo Us.      REGISTRO DETALHE DE LOTE
                _segmentoQ += Utils.FitStringLength(numeroRegistro.ToString(), 5, 5, '0', 0, true, true, true); //5     Z0062 Detalhe/ Seqüência no Segmento    N.SEQUENCIAL REGISTRO LOTE
                _segmentoQ += "Q";                                                                              //1     S0001 UDV -Valor Definido pelo Us.      CODIGO SEGMENTO REG DETALHE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1     S0002 Em branco                         USO EXCLUSIVO NEXXERA
                _segmentoQ += "06"; //ALTERAÇÃO DO VENCIMENTO                                                   //2     S0001 UDV -Valor Definido pelo Us.      TIPO DE MOVIMENTO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1     S0003 Zero                              TIPO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15    S0003 Zero                              NUMERO DE INSCRICAO
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40    S0002 Em branco                         NOME DO SACADOR
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40    S0002 Em branco                         ENDERECO -SACADO
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15    S0002 Em branco                         BAIRRO
                _segmentoQ += Utils.FitStringLength("", 5, 5, '0', 0, true, true, true);                        //5     S0003 Zero                              CEP Benefic./ Pagador - 5 díg.
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3     S0003 Zero                              CEP Benefic./ Pagador - 3 díg.
                _segmentoQ += Utils.FitStringLength("", 15, 15, ' ', 0, true, true, false);                     //15    S0002 Em branco                         CIDADE DO BENEFIC./ PAGADOR
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2     S0002 Em branco                         UNIDADE DE FEDERACAO
                _segmentoQ += Utils.FitStringLength("", 1, 1, '0', 0, true, true, true);                        //1     S0003 Zero                              Cód.Pessoa Fís./ Jur.Ben./ Pag
                _segmentoQ += Utils.FitStringLength("", 15, 15, '0', 0, true, true, true);                      //15    S0003 Zero                              CPF / CNPJ do Benefic./ Pagador
                _segmentoQ += Utils.FitStringLength("", 40, 40, ' ', 0, true, true, false);                     //40    S0002 Em branco                         NOME DO SACADOR / AVALISTA
                _segmentoQ += Utils.FitStringLength("", 3, 3, '0', 0, true, true, true);                        //3    S0003 Zero                              Nº Banco do Benefic./ Pagador
                _segmentoQ += Utils.FitStringLength("", 20, 20, ' ', 0, true, true, false);                     //20    S0002 Em branco                         NOSSO N BANCO CORRESPONDENTE
                _segmentoQ += Utils.FitStringLength("", 1, 1, ' ', 0, true, true, false);                       //1     S0002 Em branco                         FORMA DE ACESSO AO BOLETO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2     S0002 Em branco                         PRIMEIRA INSTRUCAO
                _segmentoQ += Utils.FitStringLength("", 2, 2, ' ', 0, true, true, false);                       //2     S0002 Em branco                         SEGUNDA INSTRUCAO
                _segmentoQ += Utils.FitStringLength(boleto.Carteira, 3, 3, '0', 0, true, true, true);           //3     S0001 UDV -Valor Definido pelo Us.      VARIACAO DA CARTEIRA / CARTEIRA

                _segmentoQ += " ";
                _segmentoQ = Utils.SubstituiCaracteresEspeciais(_segmentoQ);

                return _segmentoQ;

            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do SEGMENTO P DO DETALHE do arquivo de REMESSA.", ex);
            }
        }
        #endregion
        #endregion

        #region TRAILER
        /// <summary>
        /// Gerar trailer do lote
        /// </summary>
        /// <param name="numeroRegistro">Quantidade dos Títulos.</param>
        /// <param name="vlrTituloTotal">Valor Total dos Títulos</param>
        /// <param name="qtdTituloTotal">Quantidade Total dos Títulos</param>
        /// <returns>Retorna o linha de rodapé</returns>
        private string GerarTrailerLoteRemessa240(int nrBanco, int numeroRegistro, decimal vlrTituloTotal, int qtdTituloTotal)
        {
            try
            {
                #region Comentário
                //POS INI/FINAL    TAM     Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
                //------------------------------------------------------------------------------------------------------------------------------------------------
                //001 - 003	    003      Z0012            Nº do Banco da Companhia           CODIGO DO BANCO NA COMPENSACAO	 
                //004 - 004	    004      S0001            UDV - Valor Definido pelo Us.      LOTE DE SERVICO	                            0001
                //008 - 001	    001      S0001            UDV - Valor Definido pelo Us.      REGISTRO TRAILER DE LOTE	                5
                //009 - 009	    009      S0002            Em branco                          USO EXCLUSIVO NEXXERA	 
                //018 - 006	    006      Z0063            Seq. de Linha em Segmento          EP QTDE REGISTROS DO LOTE
                //024 - 006	    006      Z0022            Cont. de Linha Arq.Formatado       QTDE TITULOS EM COBRANCA
                //030 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //047 - 006	    006      S0003            Zero                               QTDE TITULOS EM COBRANCA	 
                //053 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //070 - 006	    006      S0003            Zero                               QTDE TITULOS EM COBRANCA	 
                //076 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //093 - 006	    006      S0003            Zero                               QTDE TITULOS EM COBRANCA	 
                //099 - 017	    017      S0003            Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //116 - 008	    008      S0002            Em branco                          NUMERO DO AVISO DE LANCAMENTO	 
                //124 - 117	    117      S0002            Em branco                          USO EXCLUSIVO NEXXERA
                #endregion
                string _trailer = Utils.FormatCode(nrBanco.ToString(), "0", 3, true);         //003   Nº do Banco da Companhia           CODIGO DO BANCO NA COMPENSACAO	      
                _trailer += Utils.FormatCode("0001", "0", 4, true);                           //004   UDV - Valor Definido pelo Us.      LOTE DE SERVICO	                            0001     
                _trailer += Utils.FormatCode("5", "0", 1); ;                                  //001   UDV - Valor Definido pelo Us.      REGISTRO TRAILER DE LOTE	                5     
                _trailer += Utils.FormatCode("", " ", 9);                                     //009   Em branco                          USO EXCLUSIVO NEXXERA	      
                _trailer += Utils.FormatCode(numeroRegistro.ToString(), "0", 6, true);        //006   Seq. de Linha em Segmento          EP QTDE REGISTROS DO LOTE     
                // Totalização da Cobrança Simples                                                        
                _trailer += Utils.FormatCode(qtdTituloTotal.ToString(), "0", 6, true);        //006   Cont. de Linha Arq.Formatado       QTDE TITULOS EM COBRANCA     
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 
                //Totalização cobrança vinculada                                                  
                _trailer += Utils.FormatCode("", "0", 6);                                     //006   Zero                               QTDE TITULOS EM COBRANCA	              
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	 

                _trailer += Utils.FormatCode("", "0", 6);                                     //006   Zero                               QTDE TITULOS EM COBRANCA	      
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	                                                                                                                                                                                                                                                                                                                                                                    
                _trailer += Utils.FormatCode("", "0", 6);                                     //006   Zero                               QTDE TITULOS EM COBRANCA	                         
                _trailer += Utils.FormatCode("", "0", 17);                                    //017   Zero                               VALOR TOTAL DOS TITULOS EM CARTEIRA	  
                _trailer += Utils.FormatCode("", " ", 8);                                     //008   Em branco                          NUMERO DO AVISO DE LANCAMENTO	         
                _trailer += Utils.FormatCode("", " ", 117);                                   //117   Em branco                          USO EXCLUSIVO NEXXERA    
                return _trailer;
            }
            catch (Exception e)
            {
                throw new Exception("Erro ao gerar Trailer de Lote do arquivo de remessa.", e);
            }

        }

        /// <summary>
        /// Gerar trailer do arquivo remessa
        /// </summary>
        /// <param name="nrBanco">Quantidade dos Títulos.</param>
        /// <param name="numeroRegistro">Valor Total dos Títulos</param>
        /// <returns>Retorna o linha de trailer</returns>
        private string GerarTrailerRemessa240(int nrBanco, int numeroRegistro)
        {
            try
            {
                #region Comentário
                //POS INI/FINAL    TAM     Cd.Atributo     DESCR.ATRIB	                        DESCR.ALFA                                  VR.PREDET
                //------------------------------------------------------------------------------------------------------------------------------------------------
                //001 - 003	    003	    Z0012           Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO	 
                //004 - 007	    004	    S0001           UDV - Valor Definido pelo Us.       LOTE DE SERVICO	                            9999
                //008 - 008	    001	    S0001           UDV - Valor Definido pelo Us. 	    REGISTRO TRAILER DE ARQUIVO	                9
                //009 - 017	    009	    S0002           Em branco                           USO EXCLUSIVO NEXXERA	 
                //018 - 023	    006	    S0001           UDV - Valor Definido pelo Us. 	    QTDE LOTES DO ARQUIVO	                    000001
                //024 - 029	    006	    Z0022           Cont. de Linha Arq.Formatado        QTDE REGISTROS DO ARQUIVO
                //030 - 035	    006	    S0001           UDV - Valor Definido pelo Us. 	    QTDE DE CONTAS P/CONC.(LOTES)               000000
                //036 - 240	    205	    S0002           Em branco                           USO EXCLUSIVO NEXXERA
                #endregion
                string _trailer = Utils.FormatCode(nrBanco.ToString(), "0", 3, true);                           //003   Nº do Banco da Companhia            CODIGO BANCO NA COMPENSACAO
                _trailer += Utils.FormatCode("", "9", 4, true);                                                 //004	UDV - Valor Definido pelo Us.       LOTE DE SERVICO	                            9999
                _trailer += Utils.FormatCode("", "9", 1);                                                       //001	UDV - Valor Definido pelo Us. 	    REGISTRO TRAILER DE ARQUIVO	                9
                _trailer += Utils.FormatCode("", " ", 9);                                                       //009	Em branco                           USO EXCLUSIVO NEXXERA	 
                _trailer += Utils.FormatCode("000001", "0", 6, true);                                           //006	UDV - Valor Definido pelo Us. 	    QTDE LOTES DO ARQUIVO	                    000001
                _trailer += Utils.FitStringLength(numeroRegistro.ToString(), 6, 6, '0', 0, true, true, true);   //006	Cont. de Linha Arq.Formatado        QTDE REGISTROS DO ARQUIVO
                _trailer += Utils.FormatCode("", "0", 6);                                                       //006	UDV - Valor Definido pelo Us. 	    QTDE DE CONTAS P/CONC.(LOTES)               000000                        
                _trailer += Utils.FormatCode("", " ", 205);                                                     //205	Em branco                           USO EXCLUSIVO NEXXERA

                _trailer = Utils.SubstituiCaracteresEspeciais(_trailer);

                return _trailer;
            }
            catch (Exception ex)
            {
                throw new Exception("Erro durante a geração do registro TRAILER do arquivo de REMESSA.", ex);
            }
        }
        #endregion

        #region Utils
        /// <summary>
        /// Obtem o código de ocorrência formatado, utiliza '01' - 'Entrada de titulos como padrão'
        /// </summary>
        /// <param name="boleto">Boleto</param>
        /// <returns>Código da ocorrência</returns>
        public string ObterCodigoDaOcorrencia(BoletoNet.Boleto boleto)
        {
            return boleto.Remessa != null && !string.IsNullOrEmpty(boleto.Remessa.CodigoOcorrencia) ? Utils.FormatCode(boleto.Remessa.CodigoOcorrencia, 2) : TipoOcorrenciaRemessa.EntradaDeTitulos.Format();
        }
        #endregion
    }
}
