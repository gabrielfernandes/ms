﻿//using Cobranca.Data.Model;
//using Cobranca.Data.Services;
using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Rotinas.Importacao
{
    public class ImportacaoParcela
    {
        //private RegionalProdutoDapper
        //_RegionalProduto = new RegionalProdutoDapper();
        //public void ProcessarClienteDapper(StreamReader sr)
        //{
        //    var dados = TxtParcelaEmDataTable(sr);
        //
        //    mapearDados(dados);
        //}
        //
        //
        //private static List<ImportacaoParcelaDapperModel> TxtParcelaEmDataTable(StreamReader sr)
        //{
        //
        //    DataTable dt = new DataTable();
        //    string[] headers = new string[] { };
        //    string linhaHader = sr.ReadLine();
        //    if (linhaHader.Split('\t').Length <= 1)
        //    {
        //        headers = linhaHader.Split(new Char[] { ';' });
        //    }
        //    else
        //    {
        //        headers = linhaHader.Split(new Char[] { '\t' });
        //    }
        //    Int32 c = 0;
        //    foreach (string header in headers)
        //    {
        //        if (dt.Columns.Count <= 29)
        //        {
        //            dt.Columns.Add(header.Trim());
        //        }
        //    }
        //
        //    //string[] rows = sr.ReadLine().Split(';');
        //    var text = sr.ReadToEnd();
        //    string[] stringSeparators = new string[] { "\r\n" };
        //    var rows = text.Split(stringSeparators, StringSplitOptions.None);
        //    // while (true)
        //    foreach (var r in rows)
        //    {
        //        DataRow dr = dt.NewRow();
        //        //string linhatexto = sr.ReadLine();
        //        string linhatexto = r.ToString();
        //        if (linhatexto == null || String.IsNullOrEmpty(linhatexto))
        //        {
        //            break;
        //        }
        //
        //        string[] quebra = new string[] { };
        //        if (linhatexto.Split('\t').Length <= 1)
        //        {
        //            quebra = linhatexto.Split(new Char[] { ';' });
        //        }
        //        else
        //        {
        //            quebra = linhatexto.Split(new Char[] { '\t' });
        //        }
        //        //remove a ultima coluna
        //        if (quebra.Count() == 28)
        //        {
        //            quebra = quebra.Take(quebra.Count() - 1).ToArray();
        //        }
        //
        //        for (int i = 0; i < quebra.Count(); i++)
        //        {
        //            quebra[i] = quebra[i].Replace(";", "").Replace("\t", "").Replace("\"", "");
        //            //VERIFICA OS CAMPOS PARA OFUSCAR OS DADOS ANTES DA IMPORTACAO
        //            if (i == 3 || i == 4 || i == 6 || i == 7 || i == 8 || i == 9 || i == 26)
        //            {
        //                dr[i] = DbRotinas.Criptografar(quebra[i].Trim());
        //            }
        //            else if (i == 2)//DESCRICAO FILIAL
        //            {
        //                dr[i] = DbRotinas.Criptografar(quebra[i].Replace(" SERAL", "").Trim());
        //            }
        //            else if (i == 13 || i == 14 || i == 21 || i == 23)
        //            {
        //                dr[i] = DbRotinas.ConverterParaDatetimeOuNull(quebra[i].Trim());
        //            }
        //            else if (i == 11)
        //            {
        //                dr[i] = DbRotinas.ConverterParaDecimal(quebra[i].Trim());
        //            }
        //            else
        //            {
        //                dr[i] = quebra[i].Trim();
        //            }
        //        }
        //        c = c + 1;
        //
        //
        //        dt.Rows.Add(dr);
        //    }
        //
        //    var lista = (from datarow in dt.AsEnumerable()
        //                 select new ImportacaoParcelaDapperModel
        //                 {
        //                     Companhia = DbRotinas.ConverterParaString(datarow["companhia"]),
        //                     Filial = DbRotinas.ConverterParaString(datarow["Filial"]),
        //                     NumeroContrato = DbRotinas.ConverterParaString(datarow["Contrato"]),
        //                     DescricaodaFilial = DbRotinas.ConverterParaString(datarow["Descricaodafilial"]),
        //                     RotaArea = DbRotinas.ConverterParaString(datarow["rotaarea"]),
        //                     StatusContrato = DbRotinas.ConverterParaString(datarow["Status"]),
        //                     TipoContrato = DbRotinas.ConverterParaString(datarow["Tipodecontrato"]),
        //                     NumeroCliente = DbRotinas.ConverterParaString(datarow["Numerodocliente"]),
        //                     CpfCnpj = DbRotinas.ConverterParaString(datarow["CPFCNPJ"]),
        //                     Descricao = DbRotinas.ConverterParaString(datarow["Descricao"]),
        //                     NumeroFatura = DbRotinas.ConverterParaString(datarow["NumerodaFatura"]),
        //                     ValorFatura = DbRotinas.ConverterParaDecimal(datarow["valorfatura"]),
        //                     ValorAberto = DbRotinas.ConverterParaDecimal(datarow["valoremaberto"]),
        //                     DataVencimento = DbRotinas.ConverterParaDatetimeOuNull(datarow["datavencimento"]),
        //                     DataFechamento = DbRotinas.ConverterParaDatetimeOuNull(datarow["datafechamento"]),
        //                     NumeroParcela = DbRotinas.ConverterParaString(datarow["numerodaparcela"]),
        //                     Governo = DbRotinas.ConverterParaString(datarow["governo"]),
        //                     Corporativo = DbRotinas.ConverterParaString(datarow["corporativo"]),
        //                     ClassificacaoFatura = DbRotinas.ConverterParaString(datarow["classificacaofatura"]),
        //                     NotaFiscal = DbRotinas.ConverterParaString(datarow["notafiscal"]),
        //                     TipoDocumento = DbRotinas.ConverterParaString(datarow["tipodocumento"]),
        //                     DataEmissaoFatura = DbRotinas.ConverterParaDatetimeOuNull(datarow["dataemissaofatura"]),
        //                     InstrumentoPgto = DbRotinas.ConverterParaString(datarow["instrumentopagto"]),
        //                     DataPagamento = DbRotinas.ConverterParaDatetimeOuNull(datarow["datapagamento"]),
        //                     NossoNumero = DbRotinas.ConverterParaString(datarow["nossonumero"]),
        //                     EmpresaCobrança = DbRotinas.ConverterParaString(datarow["empresacobranca"]),
        //                     CodClienteParcela = DbRotinas.ConverterParaString(datarow["an8parcela"]),
        //                     NumeroLinha = DbRotinas.ConverterParaString(datarow["numerodalinha"]),
        //                     Imposto = DbRotinas.ConverterParaDecimal(datarow["impostos"])
        //                 }).ToList();
        //
        //    return lista;
        //}
        //private void mapearDados(List<ImportacaoParcelaDapperModel> dados)
        //{
        //    try
        //    {
        //        List<RegionalProdutoDapperModel> listContratoCliente = _RegionalProduto.GetAllRegionalProdutos();
        //
        //    }
        //    catch (Exception ex)
        //    {
        //
        //        throw new Exception("MapearDados ", ex);
        //    }
        //}

    }
}
