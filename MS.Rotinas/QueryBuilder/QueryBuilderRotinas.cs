﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Rotinas.QueryBuilder
{
    public class QueryBuilderRotinas
    {
        public static List<QueryBuilderRuleModel> ConvertClassToQueryBuilderRules<T>(T builder)
        {
            List<QueryBuilderRuleModel> rules = new List<QueryBuilderRuleModel>();
            foreach (PropertyInfo propertyInfo in builder.GetType().GetProperties())
            {

                object[] attrs = propertyInfo.GetCustomAttributes(true);
                var qryBuilder = attrs.OfType<AttributesValues.IsQueryBuilder>().FirstOrDefault();
                if (qryBuilder != null)
                {
                    if (qryBuilder.Value)
                    {
                        if (attrs.FirstOrDefault() != null)
                        {
                            var value = attrs.OfType<DisplayNameAttribute>().FirstOrDefault();
                            var inputType = attrs.OfType<AttributesValues.RulesInput>().FirstOrDefault();
                            var inputValues = attrs.OfType<AttributesValues.RulesValue>().FirstOrDefault();
                            var operators = attrs.OfType<AttributesValues.RulesOperator>().FirstOrDefault();
                            rules.Add(new QueryBuilderRuleModel(
                                propertyInfo.Name,
                                value.DisplayName,
                                CastPropertyTypeToQueryBuilderType(propertyInfo.PropertyType),
                                inputValues != null ? inputValues.Values.ToArray() : null,
                                inputType != null ? inputType.Name : null,
                                operators != null ? operators.Operators.Select(x => CastOperatorsToQueryBuilderOperators(x)).ToArray() : null
                            ));
                        }
                    }
                }
            }
            return rules;
        }

        public static void AddListToJson(ref string json, List<DictionaryEntry> list)
        {
            StringBuilder str = new StringBuilder();
            var newLine = Environment.NewLine;
            foreach (var item in list)
            {
                str.Append($@"{newLine}{item.Key} : '{item.Value}'".Replace("'", "\""));
            }
            json.Remove((json.Length - 1));
            json = $"{json}{str}";
        }



        public static string CastPropertyTypeToQueryBuilderType(Type type)
        {
            switch (type.Name)
            {
                case "Double":
                case "Decimal":
                    return "double";
                case "String":
                    return "string";
                case "Date":
                case "Datetime":
                    return "date";
                case "Boolean":
                    return "boolean";
                case "Int16":
                case "Int32":
                case "Int64":
                    return "integer";
                default:
                    throw new Exception($"Unexpected data type {type}");
            }
        }

        public static string CastOperatorsToQueryBuilderOperators(string operador)
        {
            switch (operador)
            {
                case "==":
                    return "equal";
                case "!=":
                    return "double";
                case "in":
                    return "in";
                case "not in":
                    return "not_in";
                case "<":
                    return "less";
                case "<=":
                    return "less_or_equal";
                case ">":
                    return "greater";
                case ">=":
                    return "less_or_equal";
                case "between":
                    return "between";
                case "not between":
                    return "not_between";
                case "like":
                    return "contains";
                case "not like":
                    return "not_contains";
                case "is null":
                    return "is_null";
                case "is not null":
                    return "is_not_null";
                default:
                    return null;
            }
        }
    }
}
