﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Cobranca.Rotinas.QueryBuilder
{
    public class QueryBuilderRuleModel
    {
        public QueryBuilderRuleModel(string id, string label, string type, object[] values, string input, string[] operators)
        {
            this.id = id;
            this.label = label;
            this.type = type;
            this.values = values;
            this.input = input;
            this.operators = operators;
        }
        public QueryBuilderRuleModel(string id, string label, string type, List<DictionaryEntry> valuesInList, string input, string[] operators)
        {
            this.id = id;
            this.label = label;
            this.type = type;
            this.valuesCustom = valuesInList;
            this.input = input;
            this.operators = operators;
        }
        public QueryBuilderRuleModel(string id, string label, string type, string[] operators)
        {
            this.id = id;
            this.label = label;
            this.type = type;
            this.operators = operators;
        }

        #region Properts
        /// <summary>
        /// Condition - valores aceitaveis são "and" e "or".
        /// </summary>
        /// <value>
        /// Valor Condicional.
        /// </value>
        public string condition { get; set; }

        /// <summary>
        /// O nome do campo ao qual o filtro se aplica.
        /// </summary>
        /// <value>
        /// O Campo.
        /// </value>
        public string field { get; set; }

        /// <summary>
        /// Obtém ou define o identificador.
        /// </summary>
        /// <value>
        /// O Identificador.
        /// </value>
        public string id { get; set; }


        /// <summary>
        /// Obtém ou define o identificador.
        /// </summary>
        /// <value>
        /// O Identificador.
        /// </value>
        public string label { get; set; }

        /// <summary>
        /// Obtém ou define o input.
        /// </summary>
        /// <value>
        /// O input.
        /// </value>
        public string input { get; set; }


        public string[] operators { get; set; }

        /// <summary>
        /// Obtém ou define o operador.
        /// </summary>
        /// <value>
        /// O operador.
        /// </value>
        //public string Operator { get; set; }

        /// <summary>
        /// Obtém ou define regras de filtros.
        /// </summary>
        /// <value>
        /// O Filtros.
        /// </value>
        public List<QueryBuilderRuleModel> Rules { get; set; }

        /// <summary>
        /// Obtém ou define o tipo. Os valores suportados são "integer", "double", "string", "date", "datetime" e "boolean".
        /// </summary>
        /// <value>
        /// O type.
        /// </value>
        public string type { get; set; }

        /// <summary>
        /// Obtem ou define o valor do filtro.
        /// </summary>
        /// <value>
        /// O Valor.
        /// </value>
        public string value { get; set; }

        public object[] values { get; set; }
        public List<DictionaryEntry> valuesCustom { get; set; }
        #endregion
        public string ToJson()
        {

            StringBuilder strValues = new StringBuilder();
            if (valuesCustom != null)
            {
                this.valuesCustom.ForEach(x => strValues.Append($"{{{Environment.NewLine}{x.Key} : '{x.Value}'}},"));
                strValues = strValues.Remove(strValues.Length - 1, 1);
            }
            else
            {
                foreach (var item in values.ToList()) strValues.Append($"'{item.ToString()}',");
                strValues = strValues.Remove(strValues.Length - 1, 1);
            }

            var newLine = Environment.NewLine;
            StringBuilder json = new StringBuilder();
            json.Append($"{{");
            json.Append($"{newLine}id: '{this.id}',");
            json.Append($"{newLine}label: '{this.label}',");
            json.Append($"{newLine}type: '{this.type}',");
            if (this.input != null) json.Append($"{newLine}input: '{this.input}',");
            json.Append($"{newLine}values: [{strValues.ToString()}],");
            if (this.operators != null) json.Append($"{newLine}operators: ['in', 'not_in']");
            json.Append($"{newLine}}}");
            return json.ToString();
        }
    }


}
