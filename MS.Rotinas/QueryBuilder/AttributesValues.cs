﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Rotinas.QueryBuilder
{
    public abstract class AttributesValues : Attribute
    {
        public class IsQueryBuilder : Attribute
        {
            public bool Value { get; private set; }

            public IsQueryBuilder(bool value)
            {
                this.Value = value;
            }
        }
        public class RulesValue : Attribute
        {
            public string[] Values { get; private set; }

            public RulesValue(params string[] values)
            {
                this.Values = values;
            }
        }
        public class RulesInput : Attribute
        {
            public string Name { get; private set; }

            public RulesInput(string name)
            {
                this.Name = name;
            }
        }


        public class RulesOperator : Attribute
        {
            public string[] Operators { get; private set; }
            //private string[] Accepts { get { return new string[] { "==", "!=", "IN", "NOT IN", "<", "<=", ">", ">=", "BETWEEN", "NOT BETWEEN", "LIKE", "NOT LIKE", "IS NULL", "IS NOT NULL" }; } }

            /// <summary>
            /// Lista de String de Operadores
            /// </summary>
            /// <param name="operators">new string[] {"==", "!=", "IN", "NOT IN", "MENOR", "MENOR=", ">", ">=", "BETWEEN", "NOT BETWEEN", "LIKE", "NOT LIKE", "IS NULL", "IS NOT NULL" }</param>
            public RulesOperator(params string[] operators)
            {
                this.Operators = operators.Select(x=> x.ToLower()).ToArray();
            }
        }
    }
}
