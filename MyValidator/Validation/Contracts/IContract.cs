﻿namespace JunixValidator.Validation
{
    public interface IContract
    {
        ValidationContract Contract { get; }
    }
}
