namespace JunixValidator.Validation
{
    public interface IValidatable
    {
        void Validate();
    }
}