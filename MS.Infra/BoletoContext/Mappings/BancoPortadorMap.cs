﻿using Cobranca.Domain.BoletoContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.BoletoContext.Mappings
{
    public class BancoPortadorMap : EntityTypeConfiguration<BancoPortador>
    {
        public BancoPortadorMap()
        {            
            // Primary Key
            HasKey(t => t.Cod);

            // Table
            ToTable("TabelaBancoPortador");

            Property(t => t.Cod).HasColumnName("Cod");
            Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            this.Property(t => t.Banco.Cod).HasColumnName("CodBanco");
            this.Property(t => t.Portador).HasColumnName("Portador").HasMaxLength(50);
            this.Property(t => t.Agencia).HasColumnName("Agencia").HasMaxLength(50);
            this.Property(t => t.AgenciaDigito).HasColumnName("AgenciaDigito").HasMaxLength(50);
            this.Property(t => t.Conta).HasColumnName("Conta").HasMaxLength(50);
            this.Property(t => t.ContaDigito).HasColumnName("ContaDigito").HasMaxLength(50);
            this.Property(t => t.Ativo).HasColumnName("Ativo");
            this.Property(t => t.CNPJ).HasColumnName("CNPJ").HasMaxLength(50);
            this.Property(t => t.CEP).HasColumnName("CEP").HasMaxLength(50);
            this.Property(t => t.Endereco).HasColumnName("Endereco").HasMaxLength(500);
            this.Property(t => t.Cidade).HasColumnName("Cidade").HasMaxLength(500);
            this.Property(t => t.Estado).HasColumnName("Estado").HasMaxLength(50);
            this.Property(t => t.Bairro).HasColumnName("Bairro").HasMaxLength(50);
            this.Property(t => t.UF).HasColumnName("UF").HasMaxLength(50);
            this.Property(t => t.EnderecoNumero).HasColumnName("EnderecoNumero").HasMaxLength(10);
            this.Property(t => t.EnderecoComplemento).HasColumnName("EnderecoComplemento").HasMaxLength(500);
            this.Property(t => t.BancoNumero).HasColumnName("BancoNumero");
            this.Property(t => t.DocumentoCodigoEspecie).HasColumnName("DocumentoCodigoEspecie");
            this.Property(t => t.Carteira).HasColumnName("Carteira");
            this.Property(t => t.JurosAoMesPorcentagem).HasColumnName("JurosAoMesPorcentagem");
            this.Property(t => t.TaxaPorParcela).HasColumnName("TaxaPorParcela");
            this.Property(t => t.MultaAtraso).HasColumnName("MultaAtraso");
            this.Property(t => t.MoraAtrasoPorcetagemDia).HasColumnName("MoraAtrasoPorcetagemDia");
            this.Property(t => t.Companhia).HasColumnName("Companhia").HasMaxLength(50);
            this.Property(t => t.NossoNumero).HasColumnName("NossoNumero");
            this.Property(t => t.NossoNumeroAtual).HasColumnName("NossoNumeroAtual");
            this.Property(t => t.Lote).HasColumnName("Lote");
            this.Property(t => t.LoteAtual).HasColumnName("LoteAtual");
            this.Property(t => t.CompanhiaFiscal).HasColumnName("CompanhiaFiscal").HasMaxLength(50);
            this.Property(t => t.CodBancoPortadorGrupo).HasColumnName("CodBancoPortadorGrupo");
            this.Property(t => t.DataInicialRegistroBoleto).HasColumnName("DataInicialRegistroBoleto");
            this.Property(t => t.NumeroDiaRegistroBoleto).HasColumnName("NumeroDiaRegistroBoleto");
            this.Property(t => t.CodRegionalGrupo).HasColumnName("CodRegionalGrupo");
            this.Property(t => t.Padrao).HasColumnName("Padrao");
        }

    }
}
