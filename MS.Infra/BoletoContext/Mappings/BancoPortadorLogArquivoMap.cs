﻿using Cobranca.Domain.BoletoContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.BoletoContext.Mappings
{
    public class BancoPortadorLogArquivoMap : EntityTypeConfiguration<BancoPortadorLogArquivo>
    {
        public BancoPortadorLogArquivoMap()
        {
            // Primary Key
            HasKey(t => t.Cod);

            // Table
            ToTable("TabelaBancoPortadorLogArquivo");

            Property(t => t.Cod).HasColumnName("Cod");
            Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            this.Property(t => t.CodBancoPortador).HasColumnName("CodBancoPortador");
            this.Property(t => t.NomeDocumento).HasColumnName("NomeDocumento").HasMaxLength(250);
            this.Property(t => t.NumeroLote).HasColumnName("NumeroLote").HasMaxLength(50);
            this.Property(t => t.TipoArquivo).HasColumnName("TipoArquivo");
            this.Property(t => t.Banco).HasColumnName("Banco").HasMaxLength(150);
            this.Property(t => t.Companhia).HasColumnName("Companhia").HasMaxLength(150);
            this.Property(t => t.Arquivo).HasColumnName("Arquivo");
            this.Property(t => t.BoletoTotalArquivo).HasColumnName("BoletoTotalArquivo");
            this.Property(t => t.DataProcessamento).HasColumnName("DataProcessamento");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Log).HasColumnName("Log").HasMaxLength(500);
        }

    }
}
