﻿using Cobranca.Domain.BoletoContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.BoletoContext.Mappings
{
    public class ParcelaBoletoMap : EntityTypeConfiguration<ParcelaBoleto>
    {
        public ParcelaBoletoMap()
        {            
            // Primary Key
            HasKey(t => t.Cod);

            // Table
            ToTable("ContratoParcelaBoleto");

            Property(t => t.Cod).HasColumnName("Cod");
            Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            //this.Property(t => t.Boleto.NumeroDocumento).HasColumnName("NumeroDocumento").IsRequired();
            //this.Property(t => t.Boleto.NossoNumero).HasColumnName("NossoNumero");
            //this.Property(t => t.Boleto.DataVencimento).HasColumnName("DataVencimento").IsRequired();
            //this.Property(t => t.Boleto.ValorDocumento).HasColumnName("ValorDocumento").IsRequired();
            //this.Property(t => t.Boleto.ValorDesconto).HasColumnName("ValorDesconto");
            //this.Property(t => t.Boleto.Status).HasColumnName("Status");
            //this.Property(t => t.Boleto.DataStatus).HasColumnName("DataStatus");
            //this.Property(t => t.Boleto.DataAprovacao).HasColumnName("DataAprovacao");
            //this.Property(t => t.Boleto.CodUsuario).HasColumnName("CodUsuario");
            //this.Property(t => t.Boleto.MensagemImpressa).HasColumnName("MensagemImpressa").HasMaxLength(250);
            //this.Property(t => t.Boleto.CodUsuarioStatus).HasColumnName("CodUsuarioStatus");

            this.Property(t => t.Parcela.Cod).HasColumnName("CodParcela");
            this.Property(t => t.DataDocumento).HasColumnName("DataDocumento");
            this.Property(t => t.CodHistorico).HasColumnName("CodHistorico");
            this.Property(t => t.JurosAoMesPorcentagem).HasColumnName("JurosAoMesPorcentagem");
            this.Property(t => t.MultaAtrasoPorcentagem).HasColumnName("MultaAtrasoPorcentagem");
            this.Property(t => t.ValorJuros).HasColumnName("ValorJuros");
            this.Property(t => t.ValorMulta).HasColumnName("ValorMulta");
            this.Property(t => t.JurosAoMesPorcentagemPrevisto).HasColumnName("JurosAoMesPorcentagemPrevisto");
            this.Property(t => t.MultaAtrasoPorcentagemPrevisto).HasColumnName("MultaAtrasoPorcentagemPrevisto");
            this.Property(t => t.ValorJurosPrevisto).HasColumnName("ValorJurosPrevisto");
            this.Property(t => t.ValorMultaPrevisto).HasColumnName("ValorMultaPrevisto");
            this.Property(t => t.ValorDocumentoPrevisto).HasColumnName("ValorDocumentoPrevisto");
            this.Property(t => t.ValorDocumentoOriginal).HasColumnName("ValorDocumentoOriginal").IsRequired();
            this.Property(t => t.CodigoCliente).HasColumnName("CodigoCliente");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.Alterar).HasColumnName("Alterar");
            this.Property(t => t.CodLote).HasColumnName("CodLote");
            this.Property(t => t.Imposto).HasColumnName("Imposto");
            this.Property(t => t.ValorDocumentoAberto).HasColumnName("ValorDocumentoAberto");
            this.Property(t => t.InstrucaoPagamento).HasColumnName("InstrucaoPagamento").HasMaxLength(100);
            this.Property(t => t.Boleto.LinhaDigitavel).HasColumnName("LinhaDigitavel").HasMaxLength(250);
        }

    }
}
