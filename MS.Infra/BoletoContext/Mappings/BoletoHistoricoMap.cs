﻿using Cobranca.Domain.BoletoContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.BoletoContext.Mappings
{
    public class BoletoHistoricoMap : EntityTypeConfiguration<BoletoHistorico>
    {
        public BoletoHistoricoMap()
        {            
            // Primary Key
            HasKey(t => t.Cod);

            // Table
            ToTable("ContratoParcelaBoletoHistorico");

            Property(t => t.Cod).HasColumnName("Cod");
            Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            Property(t => t.CodBoleto).HasColumnName("CodBoleto").IsRequired();
            Property(t => t.CodSintese).HasColumnName("CodSintese").IsRequired();
            Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            Property(t => t.Observacao).HasColumnName("Observacao");
        }

    }
}
