﻿using Cobranca.Domain.BoletoContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.BoletoContext.Mappings
{
    public class ParametroSinteseMap : EntityTypeConfiguration<ParametroSintese>
    {
        public ParametroSinteseMap()
        {            
            // Primary Key
            HasKey(t => t.Cod);

            // Table
            ToTable("TabelaParametroSintese");

            Property(t => t.Cod).HasColumnName("Cod");
            Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            Property(t => t.CodSinteseDe).HasColumnName("CodSinteseDe");
            Property(t => t.CodSintesePara).HasColumnName("CodSintesePara");
            Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            Property(t => t.Etapa).HasColumnName("Etapa");
            Property(t => t.Observacao).HasColumnName("Observacao");
            Property(t => t.Tipo).HasColumnName("Tipo");
        }

    }
}
