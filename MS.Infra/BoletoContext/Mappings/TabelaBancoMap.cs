﻿using Cobranca.Domain.BoletoContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.BoletoContext.Mappings
{
    public class TabelaBancoMap : EntityTypeConfiguration<TabelaBanco>
    {
        public TabelaBancoMap()
        {
            // Primary Key
            HasKey(t => t.Cod);

            // Table
            ToTable("TabelaBanco");

            Property(t => t.Cod).HasColumnName("Cod");
            Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            this.Property(t => t.NomeBanco).HasColumnName("NomeBanco").HasMaxLength(250);
            this.Property(t => t.Numero).HasColumnName("Numero");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente").HasMaxLength(250);
            this.Property(t => t.NossoNumero).HasColumnName("NossoNumero");
            this.Property(t => t.NossoNumeroAtual).HasColumnName("NossoNumeroAtual");
            this.Property(t => t.Lote).HasColumnName("Lote");
            this.Property(t => t.LoteAtual).HasColumnName("LoteAtual");
            this.Property(t => t.BancoPadrao).HasColumnName("BancoPadrao");
        }

    }
}
