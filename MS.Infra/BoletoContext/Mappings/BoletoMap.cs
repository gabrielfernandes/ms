﻿using Cobranca.Domain.BoletoContext.ValueObjects;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.BoletoContext.Mappings
{
    public class BoletoMap : ComplexTypeConfiguration<Boleto>
    {
        public BoletoMap()
        {
            Property(t => t.NumeroDocumento).HasColumnName("NumeroDocumento").IsRequired();
            Property(t => t.NossoNumero).HasColumnName("NossoNumero");
            Property(t => t.DataVencimento).HasColumnName("DataVencimento").IsRequired();
            Property(t => t.ValorDocumento).HasColumnName("ValorDocumento").IsRequired();
            Property(t => t.ValorDesconto).HasColumnName("ValorDesconto");
            Property(t => t.Status).HasColumnName("Status");
            Property(t => t.DataStatus).HasColumnName("DataStatus");
            Property(t => t.DataAprovacao).HasColumnName("DataAprovacao");
            Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            Property(t => t.MensagemImpressa).HasColumnName("MensagemImpressa").HasMaxLength(250);
            Property(t => t.CodUsuarioStatus).HasColumnName("CodUsuarioStatus");
            Property(t => t.TipoSolicitacao).HasColumnName("TipoSolicitacao").IsRequired();
        }
    }
}
