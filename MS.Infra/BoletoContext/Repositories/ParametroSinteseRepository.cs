﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Domain.BoletoContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;

namespace Cobranca.Infra.BoletoContext.Repositories
{
    public class ParametroSinteseRepository : BaseRepository<ParametroSintese>, IParametroSinteseRepository
    {

        public ParametroSinteseRepository(DbContext context)
        {
            _context = context;
        }


        public bool ExisteSinteseDePara(int codSinteseDe)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<int>(
                    "SELECT CodSintesePara FROM TabelaParametroSintese tb WHERE  tb.CodSinteseDe = @CodSinteseDe",
                    param: new { CodSinteseDe = codSinteseDe }
                    ).FirstOrDefault() > 0;
            }
        }

        public int ObterSintesePara(int codSinteseDe)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<int>(
                    "SELECT CodSintesePara FROM TabelaParametroSintese tb WHERE  tb.CodSinteseDe = @CodSinteseDe",
                    param: new { CodSinteseDe = codSinteseDe }
                    ).FirstOrDefault();
            }
        }

        public int? ObterSinteseDeEtapa(EBoletoEtapa etapa)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<int>(
                    "SELECT CodSinteseDe FROM TabelaParametroSintese tb WHERE  tb.Etapa = @Etapa",
                    param: new { Etapa = (short)etapa }
                    ).FirstOrDefault();
            }
        }

        public int? ObterSinteseParaEtapa(EBoletoEtapa etapa)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<int>(
                    "SELECT CodSintesePara FROM TabelaParametroSintese tb WHERE  tb.Etapa = @Etapa",
                    param: new { Etapa = (short)etapa }
                    ).FirstOrDefault();
            }
        }

        public EBoletoEtapa? ObterEtapaDaSintese(int codSintese)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return (EBoletoEtapa)(conn
                    .Query<int>(
                    "SELECT Etapa FROM TabelaParametroSintese tb WHERE  tb.CodSintesePara = @CodSintese AND tb.Etapa IS NOT NULL",
                    param: new { CodSintese = codSintese }
                    ).FirstOrDefault());
            }
        }
    }
}
