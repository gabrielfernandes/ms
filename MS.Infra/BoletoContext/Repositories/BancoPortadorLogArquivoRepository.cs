﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Domain.BoletoContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;

namespace Cobranca.Infra.BoletoContext.Repositories
{
    public class BancoPortadorLogArquivoRepository : BaseRepository<BancoPortadorLogArquivo>, IBancoPortadorLogArquivoRepository
    {

        public BancoPortadorLogArquivoRepository(DbContext context)
        {
            _context = context;
        }

    }
}
