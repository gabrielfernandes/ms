﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Domain.BoletoContext.Repositories;
using Cobranca.Helpers;
using Cobranca.Helpers.Models;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;

namespace Cobranca.Infra.BoletoContext.Repositories
{
    public class BoletoRepository : BaseRepository<ParcelaBoleto>, IBoletoRepository
    {

        public BoletoRepository(DbContext context)
        {
            _context = context;
        }

        public BoletoAvulsoQueryResult GetBoletoAvulsoView(int cliente)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<BoletoAvulsoQueryResult>(
                    "SELECT Cod as ClienteId, CodCliente, Nome as NomeCliente, CPF, Email, QtdContrato, DiasAtraso, ValorAtraso FROM Cliente WHERE Cod = @Cod",
                    param: new { Cod = cliente })
                    .FirstOrDefault();
            }
        }

        public DTResult GetBoletosFromListResult(short fluxo, int? usuarioId, int? faseId, int? sinteseId, string filiais, DTParameters parameters)
        {
            var dataQry = DTable.Rotinas.FilterConvertToQuery(parameters);

            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                var results = conn
                    .QueryMultiple(
                    "spGetListBoleto",
                    param: new { Fluxo = fluxo, CodUsuario = usuarioId, CodFase = faseId, CodSintese = sinteseId, FiliailIds = filiais, dataQry.Registro_De, dataQry.Registro_Ate, dataQry.Where, dataQry.Order_by },
                    commandType: CommandType.StoredProcedure);
                var total = results.Read<int>().FirstOrDefault();
                return DTable.Rotinas.ResultDT(total, parameters.Draw, results.Read<BoletoSolicitacaoQueryResult>().ToList());
            }
        }

        public List<BoletoRemessaQueryResult> GetBoletosFromListRemessa(int bancoId, DateTime? periodoDe, DateTime? periodoAte, List<int> boletoIds)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                var results = conn
                    .Query<BoletoRemessaQueryResult>(
                    "spBoletoGetListBoletosRemessaByBoletoIds",
                    param: new { CodBanco = bancoId, CodBoletos = string.Join(",", boletoIds), PeriodoDe = periodoDe, PeriodoAte = periodoAte },
                    commandType: CommandType.StoredProcedure);

                return results.ToList();
            }
        }

        public bool ExisteBoletoPendentePorParcelaId(int codParcela)
        {
            #region qry
            var sql = $"SELECT * FROM ContratoParcelaBoleto " +
                $"WHERE CodParcela = @CodParcela " +
                $"AND " +
                $"(Status = {(short)EBoletoStatusType.SolicitacaoAnalista} " +
                $"OR Status = {(short)EBoletoStatusType.SolicitacaoAtendente} " +
                $"OR Status = {(short)EBoletoStatusType.SolicitacaoSupervisor}" +
                $")";
            #endregion
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<int>(
                    sql,
                    param: new { CodParcela = codParcela })
                    .FirstOrDefault() > 0;
            }
        }

        public bool ExisteBoletoPendenteByContratoId(int codContrato)
        {
            #region qry
            var sql = $"SELECT * FROM ContratoParcelaBoleto " +
                $"WHERE CodContrato = @CodContrato " +
                $"AND " +
                $"(Status = {(short)EBoletoStatusType.SolicitacaoAnalista} " +
                $"OR Status = {(short)EBoletoStatusType.SolicitacaoAtendente} " +
                $"OR Status = {(short)EBoletoStatusType.SolicitacaoSupervisor}" +
                $")";
            #endregion
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<int>(
                    sql,
                    param: new { CodContrato = codContrato })
                    .FirstOrDefault() > 0;
            }
        }

        public bool ParcelaJaPossuiuClassificacaoC6(int codParcela)
        {
            #region qry
            var sql = $"SELECT COUNT(H.Cod) as QtdeC6" +
                $"FROM ContratoParcela CP" +
                $"LEFT JOIN Historico H ON H.CodParcela = CP.Cod" +
                $"LEFT JOIN TabelaSintese S ON S.Cod = H.CodSintese" +
                $"WHERE" +
                $"S.CodigoSinteseCliente = 'C6'" +
                $"AND CP.Cod = @CodParcela";
            #endregion
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<int>(
                    sql,
                    param: new { CodParcela = codParcela })
                    .FirstOrDefault() > 0;
            }
        }

        public bool UpdateLinhaDigitavelParcela(int parcelaId, string linhaDigitavel)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return (conn.Query<int>(
                    "UPDATE ContratoParcela SET LinhaDigitavel = @LinhaDigitavel, DataAlteracao = GETDATE() OUTPUT Inserted.Cod WHERE Cod = @Cod",
                    param: new { Cod = parcelaId, LinhaDigitavel = linhaDigitavel })
                    .FirstOrDefault()) > 0;
            }
        }
        public int CreateOrUpdateNossoNumero(ParcelaBoleto boleto)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn.Query<int>(
                    "UPDATE ContratoParcelaBoleto SET NossoNumero = @NossoNumero, DataAlteracao = GETDATE() OUTPUT Inserted.Cod WHERE Cod = @Cod",
                    param: new { Cod = boleto.Cod, NossoNumero = boleto.Boleto.NossoNumero })
                    .FirstOrDefault();
            }
        }
        public void ReprovarBoletoAntigosDestaParcela(int boletoId, int parcelaId, int sinteseId, string obs)
        {
            var sqlConsultaBoletosAberto = $"SELECT Cod FROM ContratoParcelaBoleto B	WHERE B.DataExcluido IS NULL " +
               $"AND B.Cod != @CodBoleto " +
               $"AND B.CodParcela = @CodParcela " +
               $"AND ( B.Status != {(short)EBoletoStatusType.Reprovado} " +
               $"OR B.Status != {(short)EBoletoStatusType.Cancelado} " +
               $"OR B.Status != {(short)EBoletoStatusType.Obsoleto}) ";

            var sqlInsertHistorico = $"INSERT INTO ContratoParcelaBoletoHistorico " +
                $"( CodBoleto, CodSintese, DataCadastro, Observacao ) " +
                $"OUTPUT Inserted.Cod " +
                $"VALUES(@CodBoleto, @CodSintese, GETDATE(), @Observacao) ";

            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                var ids = conn.Query<int>(
                    sqlConsultaBoletosAberto,
                    param: new { CodBoleto = boletoId, @CodParcela = parcelaId })
                    .ToList();

                foreach (var id in ids)
                {
                    var idHistorico = conn.Query<int>(
                        sqlInsertHistorico,
                        param: new { CodBoleto = id, CodSintese = sinteseId, Observacao = obs })
                        .FirstOrDefault();

                    conn.Query<int>(
                        $"UPDATE ContratoParcelaBoleto SET Status = {EBoletoStatusType.Obsoleto}, CodHistorico = @CodHistorico WHERE Cod = @CodBoleto",
                        param: new { CodBoleto = id, CodHistorico = idHistorico })
                        .Single();
                }
            }
        }
        public void ReprovarBoletoAntigosDesteContrato(int boletoId, int contratoId, int sinteseId, string obs)
        {
            var sqlConsultaBoletosAberto = $"SELECT Cod FROM ContratoParcelaBoleto B	WHERE B.DataExcluido IS NULL " +
                $"AND B.Cod != @CodBoleto " +
                $"AND B.CodContrato = @CodContrato " +
                $"AND ( B.Status != {(short)EBoletoStatusType.Reprovado} " +
                $"OR B.Status != {(short)EBoletoStatusType.Cancelado} " +
                $"OR B.Status != {(short)EBoletoStatusType.Obsoleto}) ";

            var sqlInsertHistorico = $"INSERT INTO ContratoParcelaBoletoHistorico " +
                $"( CodBoleto, CodSintese, DataCadastro, Observacao )" +
                $"OUTPUT Inserted.Cod " +
                $"VALUES(@CodBoleto, @CodSintese, GETDATE(), @Observacao)";

            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                var ids = conn.Query<int>(
                    sqlConsultaBoletosAberto,
                    param: new { CodBoleto = boletoId, @CodContrato = contratoId })
                    .ToList();

                foreach (var id in ids)
                {
                    var idHistorico = conn.Query<int>(
                        sqlInsertHistorico,
                        param: new { CodBoleto = id, CodSintese = sinteseId, Observacao = obs })
                        .FirstOrDefault();

                    conn.Query<int>(
                        $"UPDATE ContratoParcelaBoleto SET Status = {(short)EBoletoStatusType.Obsoleto}, CodHistorico = {idHistorico} WHERE Cod = @CodBoleto",
                        param: new { CodBoleto = id })
                        .FirstOrDefault();
                }
            }
        }

        public void AtualizarBoletosSetRemessaEmitida(List<int> boletoIds, int codLogArquivo, int usuarioId)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                var idLote = conn.Query<int>(
                     "spBoletoLoteInsert",
                      param: new { @CodLogArquivo = codLogArquivo, @CodUsuario = usuarioId, Quantidade = boletoIds.Count() },
                      commandType: CommandType.StoredProcedure)
                      .FirstOrDefault();

                foreach (var idBoleto in boletoIds)
                {
                    var ids = conn.Query<int>(
                      "UPDATE ContratoParcelaBoleto SET DataRemessa = GETDATE(), CodLote = @CodLote, CodUsuarioRemessa = @CodUsuario WHERE Cod = @CodBoleto",
                      param: new { CodBoleto = idBoleto, CodLote = idLote, CodUsuario = usuarioId })
                      .FirstOrDefault();
                }
            }
        }

    }
}
