﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Domain.BoletoContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;

namespace Cobranca.Infra.BoletoContext.Repositories
{
    public class BancoPortadorRepository : BaseRepository<BancoPortador>, IBancoPortadorRepository
    {

        public BancoPortadorRepository(DbContext context)
        {
            _context = context;
        }

        public BancoPortadorQueryResult GetByParcelaId(int codParcela)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                return conn
                    .Query<BancoPortadorQueryResult>(
                    "spBancoPortadorGetByParcela",
                    param: new { CodParcela = codParcela },
                    commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }
        }

        public BancoPortadorQueryResult GetByBancoPortadorId(int bancoPortadorId)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                return conn
                    .Query<BancoPortadorQueryResult>(
                    "spBancoPortadorInfoByBoletoId",
                    param: new { CodBancoPortador = bancoPortadorId },
                    commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }
        }

        public List<BancoPortadorInstrucaoQueryResult> GetInstrucoesByBancoPortadorId(int bancoPortadorId)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                return conn
                    .Query<BancoPortadorInstrucaoQueryResult>(
                    "SELECT * FROM TabelaBancoPortadorInstrucao WHERE CodBancoPortador = @CodBancoPortador",
                    param: new { CodBancoPortador = bancoPortadorId })
                    .ToList();
            }
        }

        public BancoPortadorQueryResult GetBancoPortadorByCodContratoAndNumeroBanco(int codContrato, short numeroContrato)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                return conn
                    .Query<BancoPortadorQueryResult>(
                    "spBancoPortadorGetByCodContratoAndBancoNumero",
                    param: new { CodContrato = codContrato, NumeroContrato = numeroContrato },
                    commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }
        }
        public int GerarNossoNumero(int bancoPortadorId)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<int>(
                    "spBancoPortadorObterNovoNossoNumero",
                    param: new { CodBancoPortador = bancoPortadorId },
                    commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }
        }
        public int GerarNumeroLote(int bancoPortadorId)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<int>(
                    "spBancoPortadorObterLoteAtual",
                    param: new { CodBancoPortador = bancoPortadorId },
                    commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }
        }
        

        public List<BancoPortadorQueryResult> GetListBancoPortadorByRegionalGrupoId(string regionalGrupoIds)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<BancoPortadorQueryResult>(
                    "spBancoPortadorGetByRegionalGrupoId",
                    param: new { CodRegionalGrupo = regionalGrupoIds },
                    commandType: CommandType.StoredProcedure)
                    .ToList();
            }
        }


    }
}
