﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Domain.BoletoContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;

namespace Cobranca.Infra.BoletoContext.Repositories
{
    public class TabelaBancoRepository : BaseRepository<TabelaBanco>, ITabelaBancoRepository
    {

        public TabelaBancoRepository(DbContext context)
        {
            _context = context;
        }

        public List<TabelaBanco> GetAllBancos()
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn.Query<TabelaBanco>("SELECT * FROM TabelaBanco WHERE DataExcluido IS NULL").ToList();
            }
        }
    }
}
