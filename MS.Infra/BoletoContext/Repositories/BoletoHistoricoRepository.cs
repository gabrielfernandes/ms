﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.BoletoContext.Enums;
using Cobranca.Domain.BoletoContext.Queries;
using Cobranca.Domain.BoletoContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;

namespace Cobranca.Infra.BoletoContext.Repositories
{
    public class BoletoHistoricoRepository : BaseRepository<BoletoHistorico>, IBoletoHistoricoRepository
    {
        public BoletoHistoricoRepository(DbContext context)
        {
            _context = context;
        }
        public bool UpdateBoletoSetLastHistory(int codBoleto)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return (conn.Query<int>(
                    "spContratoParcelaBoletoSetLastHistoryByBoletoId",
                    param: new { CodBoleto = codBoleto },
                    commandType: CommandType.StoredProcedure)
                    .FirstOrDefault()) > 0;
            }
        }
        public BoletoHistorico GetLastHistoryByCodBoleto(int codBoleto)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn.Query<BoletoHistorico>(
                    "SELECT TOP 1 * FROM ContratoParcelaBoletoHistorico WHERE CodBoleto = @CodBoleto AND DataExcluido IS NULL ORDER BY Cod DESC",
                    param: new { CodBoleto = codBoleto })
                    .FirstOrDefault();
            }
        }

        public string GetLastObservacaoByCodBoleto(int codBoleto)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn.Query<string>(
                    "SELECT TOP 1 Observacao FROM ContratoParcelaBoletoHistorico WHERE CodBoleto = @CodBoleto AND DataExcluido IS NULL ORDER BY Cod DESC",
                    param: new { CodBoleto = codBoleto })
                    .FirstOrDefault();
            }
        }


    }
}
