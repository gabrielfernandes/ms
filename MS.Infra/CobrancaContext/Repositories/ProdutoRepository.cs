﻿using Cobranca.Domain.CobrancaContext.Commands.ProdutoCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.CobrancaContext.Repositories
{
    public class ProdutoRepository : IProdutoRepository
    {
        private readonly DbContext _context;

        public ProdutoRepository(DbContext context)
        {
            _context = context;
        }


        public TabelaProduto GetById(int cod)
        {
            return _context
                .Produtos
                .FirstOrDefault(x => x.Cod == cod);
        }


        public List<ProdutoCommandResult> Get()
        {
            var query = $"SELECT * FROM [TabelaProduto] " +
                        $"WHERE [DataExcluido] IS NULL ";

            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<ProdutoCommandResult>(query)
                    .ToList();
            }
        }
    }
}
