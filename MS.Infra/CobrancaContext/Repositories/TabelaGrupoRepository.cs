﻿using Cobranca.Domain.CobrancaContext.Commands.RotaCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.CobrancaContext.Repositories
{
    public class TabelaGrupoRepository : BaseRepository<TabelaGrupo>,  ITabelaGrupoRepository
    {
        private readonly DbContext _context;

        public TabelaGrupoRepository(DbContext context)
        {
            _context = context;
        }


        public TabelaGrupo GetById(int cod)
        {
            return _context
                .TabelaGrupos
                .FirstOrDefault(x => x.Cod == cod);
        }


        public List<TabelaGrupo> GetAll()
        {
            var query = $"SELECT * FROM [TabelaGrupo] " +
                        $"WHERE [DataExcluido] IS NULL ";

            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<TabelaGrupo>(query)
                    .ToList();
            }
        }
    }
}
