﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Cobranca.Domain.CobrancaContext.Queries;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;

namespace Cobranca.Infra.BoletoContext.Repositories
{
    public class CartaCobrancaRepository : ICartaCobrancaRepository
    {
        protected DbContext _context;

        public CartaCobrancaRepository(DbContext context)
        {
            _context = context;
        }

    }
}
