﻿using Cobranca.Domain.CobrancaContext.Commands.RemessaCommands.Results;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.CobrancaContext.Repositories
{
    public class RemessaRepository : IRemessaRepository
    {
        private readonly DbContext _context;

        public RemessaRepository(DbContext context)
        {
            _context = context;
        }


        public RemessaCommandResult GetBoletosFromStaus(string fluxopagamento)
        {
            var query = $"SELECT * FROM [Rota] " +
                        $"WHERE [DataExcluido] IS NULL ";

            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<RemessaCommandResult>(query)
                    .FirstOrDefault();
            }
        }
    }
}
