﻿using Cobranca.Domain.CobrancaContext.Commands.RotaCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.CobrancaContext.Repositories
{
    public class RotaRepository : IRotaRepository
    {
        private readonly DbContext _context;

        public RotaRepository(DbContext context)
        {
            _context = context;
        }


        public Rota GetById(int cod)
        {
            return _context
                .Rotas
                .FirstOrDefault(x => x.Cod == cod);
        }


        public List<RotaCommandResult> Get()
        {
            var query = $"SELECT * FROM [Rota] " +
                        $"WHERE [DataExcluido] IS NULL ";

            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<RotaCommandResult>(query)
                    .ToList();
            }
        }
    }
}
