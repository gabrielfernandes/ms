﻿using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared.Repositories;

namespace Cobranca.Infra.BoletoContext.Repositories
{
    public class ClienteRepository : BaseRepository<Cliente>, IClienteRepository
    {

        public ClienteRepository(DbContext context)
        {
            _context = context;
        }


    }
}
