﻿using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.CobrancaContext.Repositories
{
    public class ParametroAtendimentoRepository : BaseRepository<ParametroAtendimento>, IParametroAtendimentoRepository
    {
        public ParametroAtendimentoRepository(DbContext context) => _context = context;

        public bool CheckExist(int codRegional, int codUsuario)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<bool>(
                    "SELECT CASE WHEN EXISTS(SELECT Cod FROM ParametroAtendimento WHERE DataExcluido IS NULL AND CodUsuario = @CodUsuario AND CodRegional = @CodRegional) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END",
                    param: new { CodUsuario = codUsuario, CodRegional = codRegional }
                    ).FirstOrDefault();
            }
        }

        public ParametroAtendimento GetParametroByRegionalAndUsuario(int codRegional, int codUsuario)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<ParametroAtendimento>(
                    "SELECT * FROM ParametroAtendimento WHERE DataExcluido IS NULL AND CodUsuario = @CodUsuario AND CodRegional = @CodRegional",
                    param: new { CodUsuario = codUsuario, CodRegional = codRegional }
                    ).FirstOrDefault();
            }
        }

        /// <summary>
        /// Liberar clientes  (tabela: ClienteAtendimento) onde as condições parametrizadas do usuário não satisfazer mais as condições.
        /// <para>Para que outro atendente possa dar atendimentos a estes.</para>
        /// </summary>  
        /// <param name="usuarioId">Id do Usuario</param>
        public void DesvincularClienteAtendimentoNaoPertencente(int usuarioId)
        {

        }
    }
}
