﻿using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Queries;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.CobrancaContext.Repositories
{
    public class HistoricoRepository : BaseRepository<Historico> , IHistoricoRepository 
    {
        public HistoricoRepository(DbContext context)
        {
            _context = context;
        }        

        public List<HistoricoQueryResult> GetHistoricoByParcelaId(int codParcela)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<HistoricoQueryResult>(
                    "SELECT * FROM Historico WHERE CodParcela = @CodParela AND DataExcluido IS NULL",
                    param: new { CodParela = codParcela }
                    ).ToList();
            }
        }
    }
}
