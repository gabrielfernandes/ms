﻿using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Queries;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.BoletoContext.Repositories
{
    public class ContratoRepository : BaseRepository<Contrato>, IContratoRepository
    {

        public ContratoRepository(DbContext context)
        {
            _context = context;
        }

        public ContratoInfoQueryResult GetContratoInfoById(int codContrato)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<ContratoInfoQueryResult>(
                    "spContratoGetInfoByContratoId",
                    param: new { CodContrato = codContrato },
                    commandType: CommandType.StoredProcedure)
                    .FirstOrDefault();
            }
        }


    }
}
