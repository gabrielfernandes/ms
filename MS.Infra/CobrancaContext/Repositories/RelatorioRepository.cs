﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Cobranca.Domain.CobrancaContext.Queries;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;

namespace Cobranca.Infra.BoletoContext.Repositories
{
    public class RelatorioRepository : IRelatorioRepository
    {
        protected DbContext _context;

        public RelatorioRepository(DbContext context)
        {
            _context = context;
        }

        public List<RelatorioFilialDetalhadoQueryResult> GetReportFilialDetails(string contrato, int? clienteId, string numeroDocumento, DateTime? dataPeriodoDe, DateTime? dataPeriodoAte, bool? aberto, bool? vencido, bool? pago)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn.Query<RelatorioFilialDetalhadoQueryResult>(
                    "spReportCobrancaFilialDetalhado",
                    param: new { @NumeroContrato = contrato,
                        @ClienteId = clienteId,
                        @NumeroDocumento = numeroDocumento,
                        @PeriodoDe = dataPeriodoDe,
                        @PeriodoAte = dataPeriodoAte,
                        @Aberto = aberto,
                        @Vencido = vencido,
                        @Pago = pago },
                    commandType: CommandType.StoredProcedure)
                    .ToList();
            }
        }

    }
}
