﻿using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Queries;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Cobranca.Shared.Repositories;
using Dapper;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.CobrancaContext.Repositories
{
    public class SinteseRepository : BaseRepository<Sintese> , ISinteseRepository 
    {
        public SinteseRepository(DbContext context)
        {
            _context = context;
        }        

        public SinteseQueryResult GetSinteseByCodigoSinteseCliente(string codigoCliente)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<SinteseQueryResult>(
                    "SELECT Cod as CodSintese, CodFase, Sintese as NomeSintese, Ordem, AcaoBloqueada, CodigoSinteseCliente, Justifica FROM TabelaSintese WHERE CodigoSinteseCliente = @CodigoSinteseCliente",
                    param: new { CodigoSinteseCliente = codigoCliente }
                    ).FirstOrDefault();
            }
        }
    }
}
