﻿using Cobranca.Domain.CobrancaContext.Commands.ContratoParcelaBoletoCommands.Results;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Domain.CobrancaContext.Repositories;
using Cobranca.Shared;
using Dapper;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.CobrancaContext.Repositories
{
    public class ContratoParcelaRepository : IContratoParcelaRepository
    {
        private readonly DbContext _context;

        public ContratoParcelaRepository(DbContext context)
        {
            _context = context;
        }

        public void Save(ContratoParcela parcela)
        {
        }

        public ContratoParcela Get(int cod)
        {
            return _context
                .ContratoParcelas
                .FirstOrDefault(x => x.Cod == cod);
        }

        public ContratoParcela GetByFluxoPagamento(string fluxoPagamento)
        {
            return _context
                .ContratoParcelas
                .AsNoTracking()
                .FirstOrDefault(x => x.FluxoPagamento == fluxoPagamento);
        }

        public ParcelaCommandResult Get(string fluxopagamento)
        {
            var query = $"SELECT * FROM [ContratoParcela] " +
                        $"WHERE [DataExcluido] IS NULL " +
                        $"AND [FluxoPagamento] = @fluxopagamento";

            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<ParcelaCommandResult>(query,
                    new { fluxopagamento = fluxopagamento })
                    .FirstOrDefault();
            }
        }
    }
}
