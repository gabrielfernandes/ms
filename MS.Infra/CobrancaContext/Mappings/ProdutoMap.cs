﻿using Cobranca.Domain.CobrancaContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.CobrancaContext.Mappings
{
    public class TabelaProdutoMap : EntityTypeConfiguration<TabelaProduto>
    {
        public TabelaProdutoMap()
        {

            ToTable("TabelaProduto");
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Produto)
                .HasMaxLength(250);

            this.Property(t => t.Descricao)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Produto).HasColumnName("Produto");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
        }

    }
}
