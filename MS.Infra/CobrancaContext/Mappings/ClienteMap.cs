﻿using Cobranca.Domain.CobrancaContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.CobrancaContext.Mappings
{
    public class ClienteMap : EntityTypeConfiguration<Cliente>
    {
        public ClienteMap()
        {
            // Primary Key
            HasKey(t => t.Cod);

            // Table
            ToTable("Cliente");
            
            // Column Mappings
            Property(t => t.Cod).HasColumnName("Cod");
            Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            Property(t => t.Email.Endereco).HasColumnName("Email");
            Property(t => t.Documento.Numero).HasColumnName("CPF");
            Property(t => t.Endereco.CEP).HasColumnName("CEP");
            Property(t => t.Endereco.Logradouro).HasColumnName("Endereco");
            Property(t => t.Endereco.Cidade).HasColumnName("Cidade");
            Property(t => t.Endereco.Estado).HasColumnName("Estado");
            Property(t => t.Endereco.Bairro).HasColumnName("Bairro");
            Property(t => t.Endereco.UF).HasColumnName("UF");
            Property(t => t.Endereco.Numero).HasColumnName("EnderecoNumero");
            Property(t => t.Endereco.Complemento).HasColumnName("EnderecoComplemento");
        }

    }
}
