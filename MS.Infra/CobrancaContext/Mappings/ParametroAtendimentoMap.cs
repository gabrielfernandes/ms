﻿using Cobranca.Domain.CobrancaContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.CobrancaContext.Mappings
{
    public class ParametroAtendimentoMap : EntityTypeConfiguration<ParametroAtendimento>
    {
        public ParametroAtendimentoMap()
        {
            // Table
            ToTable("ParametroAtendimento");

            // Primary Key
            HasKey(t => t.Cod);

            // Column Mappings
            Property(t => t.Cod).HasColumnName("Cod");
            Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            Property(t => t.CodRegional).HasColumnName("CodRegional");
            Property(t => t.Query).HasColumnName("Query");
            Property(t => t.QueryRota).HasColumnName("QueryRota");
        }

    }
}
