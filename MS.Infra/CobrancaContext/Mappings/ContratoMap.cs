﻿using Cobranca.Domain.CobrancaContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.CobrancaContext.Mappings
{
    public class ContratoMap : EntityTypeConfiguration<Contrato>
    {
        public ContratoMap()
        {            
            // Primary Key
            HasKey(t => t.Cod);

            // Table
            ToTable("Contrato");

            Property(t => t.Cod).HasColumnName("Cod");
            Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            Property(t => t.CodAging).HasColumnName("CodAging");
            Property(t => t.CodClientePrincipal).HasColumnName("CodClientePrincipal");
            Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            Property(t => t.CodRegionalGrupo).HasColumnName("CodRegionalGrupo");
            Property(t => t.DiasAtraso).HasColumnName("DiasAtraso");
            Property(t => t.MensagemContrato).HasColumnName("MensagemContrato");
            Property(t => t.NumeroContrato).HasColumnName("NumeroContrato");
            Property(t => t.StatusContrato).HasColumnName("StatusContrato");
            Property(t => t.ValorAtraso).HasColumnName("ValorAtraso");
        }

    }
}
