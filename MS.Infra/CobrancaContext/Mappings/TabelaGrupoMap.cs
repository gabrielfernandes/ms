﻿using Cobranca.Domain.CobrancaContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.CobrancaContext.Mappings
{
    public class TabelaGrupoMap : EntityTypeConfiguration<TabelaGrupo>
    {
        public TabelaGrupoMap()
        {
            // Primary Key
            HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Nome)
                .IsRequired()
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaGrupo");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.AcaoBloqueada).HasColumnName("AcaoBloqueada");
        }

    }
}
