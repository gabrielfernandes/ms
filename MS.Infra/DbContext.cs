﻿using Cobranca.Domain.BoletoContext.Entities;
using Cobranca.Domain.CobrancaContext.Entities;
using Cobranca.Infra.BoletoContext.Mappings;
using Cobranca.Infra.CobrancaContext.Mappings;
using Cobranca.Shared;
using System.Data.Entity;

namespace Cobranca.Infra
{
    public class DbContext : System.Data.Entity.DbContext
    {
        public DbContext() : base(Settings.ConnectionString)
        {
        }

        public DbSet<BoletoHistorico> BoletoHistoricos { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Contrato> Contratos { get; set; }
        public DbSet<ContratoParcela> ContratoParcelas { get; set; }
        public DbSet<ParcelaBoleto> ParcelaBoletos { get; set; }
        public DbSet<ParametroAtendimento> ParametroAtendimentos { get; set; }
        public DbSet<BancoPortador> BancoPortadores { get; set; }
        public DbSet<BancoPortadorLogArquivo> BancoPortadorLogArquivos { get; set; }
        public DbSet<Rota> Rotas { get; set; }
        public DbSet<TabelaProduto> Produtos { get; set; }
        public DbSet<TabelaBanco> TabelaBancos { get; set; }
        public DbSet<TabelaGrupo> TabelaGrupos { get; set; }

        static DbContext()
        {
            Database.SetInitializer<DbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BancoPortadorMap());
            modelBuilder.Configurations.Add(new BancoPortadorLogArquivoMap());
            modelBuilder.Configurations.Add(new BoletoHistoricoMap());
            modelBuilder.Configurations.Add(new BoletoMap());
            modelBuilder.Configurations.Add(new ContratoMap());
            modelBuilder.Configurations.Add(new ClienteMap());
            modelBuilder.Configurations.Add(new ContratoParcelaMap());
            modelBuilder.Configurations.Add(new ParcelaBoletoMap());
            modelBuilder.Configurations.Add(new ParametroAtendimentoMap());
            modelBuilder.Configurations.Add(new ParametroSinteseMap());
            modelBuilder.Configurations.Add(new RotaMap());
            modelBuilder.Configurations.Add(new TabelaProdutoMap());
            modelBuilder.Configurations.Add(new TabelaBancoMap());
            modelBuilder.Configurations.Add(new TabelaGrupoMap());

        }

    }

}
