﻿
using Cobranca.Domain.ServicoContext.Entities;
using Cobranca.Domain.ServicoContext.Queries;
using Cobranca.Domain.ServicoContext.Repositories;
using Cobranca.Shared;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.ServicoContext.Repositories
{
    public class ContratoRepository : IContratoRepository
    {
        private readonly DbContext _context;

        public ContratoRepository(DbContext context)
        {
            _context = context;
        }

        public IEnumerable<ListContratoQueryResult> GetContratoToJoin()
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<ListContratoQueryResult>("SELECT [Cod], [NumeroContrato] FROM [Contrato] WHERE DataExcluido IS NULL", new { });
            }
        }
    }
}
