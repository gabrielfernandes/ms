﻿
using Cobranca.Domain.CobrancaContext.Commands.ProdutoCommands.Results;
using Cobranca.Domain.ServicoContext.Entities;
using Cobranca.Domain.ServicoContext.Enums;
using Cobranca.Domain.ServicoContext.Queries;
using Cobranca.Domain.ServicoContext.Repositories;
using Cobranca.Shared;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.ServicoContext.Repositories
{
    public class ClienteEnderecoRepository : IClienteEnderecoRepository
    {
        private readonly DbContext _context;

        public ClienteEnderecoRepository(DbContext context)
        {
            _context = context;
        }

        public List<ProdutoCommandResult> Get()
        {
            var query = $"SELECT * FROM [TabelaProduto] " +
                        $"WHERE [DataExcluido] IS NULL ";

            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<ProdutoCommandResult>(query)
                    .ToList();
            }
        }

        public IEnumerable<ListClienteEnderecoQueryResult> GetClienteEnderecoToJoin()
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<ListClienteEnderecoQueryResult>("SELECT Cod as CodClienteEndereco, CodEndereco, CodContrato, DataCadastro, DataAlteracao, TipoEndereco FROM ClienteEndereco WHERE DataExcluido IS NULL", new { });
            }
        }

        public void UpdateEnderecosByList(ref List<ClienteEndereco> lista)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                //PERCORRE APENAS OS REGISTROS ONDE O ENDERECO ESTIVER COM COD == 0
                foreach (var item in lista.Where(x => x.CodEnderecoCob > 0))
                {
                    try
                    {
                        conn
                            .Query<int>("spTabelaEnderecoUpdate",
                            param: new
                            {
                                Cod = item.CodClienteEnderecoCob,
                                Logradouro = item.EnderecoCob.Logradouro,
                                Endereco = item.EnderecoCob.Logradouro,
                                Numero = item.EnderecoCob.Numero,
                                Complemento = item.EnderecoCob.Complemento,
                                Bairro = item.EnderecoCob.Bairro,
                                Cidade = item.EnderecoCob.Cidade,
                                Estado = item.EnderecoCob.Estado,
                                UF = item.EnderecoCob.UF,
                                CEP = item.EnderecoCob.CEP,
                                CodImportacao = item.CodImportacao,
                                NumeroContrato = item.Contrato,
                                Tipo = EClienteEnderecoType.Cobranca
                            },
                            commandType: CommandType.StoredProcedure
                            ).Single();
                    }
                    catch (Exception ex)
                    {
                        item.ImportMensagem = $"{item.ImportMensagem} Erro ao executar sql spTabelaEnderecoUpdate:{ex.Message}{ex.InnerException}";
                    }

                }

                foreach (var item in lista.Where(x => x.CodEnderecoObra > 0))
                {
                    try
                    {

                        conn
                            .Query<int>("spTabelaEnderecoUpdate",
                            param: new
                            {
                                Cod = item.CodClienteEnderecoObra,
                                Logradouro = item.EnderecoObra.Logradouro,
                                Endereco = item.EnderecoObra.Logradouro,
                                Numero = item.EnderecoObra.Numero,
                                Complemento = item.EnderecoObra.Complemento,
                                Bairro = item.EnderecoObra.Bairro,
                                Cidade = item.EnderecoObra.Cidade,
                                Estado = item.EnderecoObra.Estado,
                                UF = item.EnderecoObra.UF,
                                CEP = item.EnderecoObra.CEP,
                                CodImportacao = item.CodImportacao,
                                NumeroContrato = item.Contrato,
                                Tipo = EClienteEnderecoType.Obra
                            },
                            commandType: CommandType.StoredProcedure
                            ).Single();
                    }
                    catch (Exception ex)
                    {
                        item.ImportMensagem = $"{item.ImportMensagem} Erro ao executar sql spTabelaEnderecoUpdate:{ex.Message}{ex.InnerException}";
                    }
                }

            }
        }

        public void CreateEnderecosByList(ref List<ClienteEndereco> lista)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                //PERCORRE APENAS OS REGISTROS ONDE O ENDERECO ESTIVER COM COD == 0
                foreach (var item in lista.Where(x => x.CodEnderecoCob == 0))
                {
                    try
                    {
                        item.SetCodEnderecoCob(
                            conn
                            .Query<int>("spTabelaEnderecoInsert",
                            param: new
                            {
                                Logradouro = item.EnderecoCob.Logradouro,
                                Endereco = item.EnderecoCob.Logradouro,
                                Numero = item.EnderecoCob.Numero,
                                Complemento = item.EnderecoCob.Complemento,
                                Bairro = item.EnderecoCob.Bairro,
                                Cidade = item.EnderecoCob.Cidade,
                                Estado = item.EnderecoCob.Estado,
                                UF = item.EnderecoCob.UF,
                                CEP = item.EnderecoCob.CEP,
                                CodImportacao = item.CodImportacao,
                                NumeroContrato = item.Contrato,
                                Tipo = EClienteEnderecoType.Cobranca
                            },
                            commandType: CommandType.StoredProcedure
                            ).Single());
                    }
                    catch (Exception ex)
                    {
                        item.ImportMensagem = $"{item.ImportMensagem} Erro ao executar sql spTabelaEnderecoCreate:{ex.Message}{ex.InnerException}";
                    }

                }

                foreach (var item in lista.Where(x => x.CodEnderecoObra == 0))
                {
                    try
                    {
                        item.SetCodEnderecoObra(conn
                            .Query<int>("spTabelaEnderecoInsert",
                            param: new
                            {
                                Logradouro = item.EnderecoObra.Logradouro,
                                Endereco = item.EnderecoObra.Logradouro,
                                Numero = item.EnderecoObra.Numero,
                                Complemento = item.EnderecoObra.Complemento,
                                Bairro = item.EnderecoObra.Bairro,
                                Cidade = item.EnderecoObra.Cidade,
                                Estado = item.EnderecoObra.Estado,
                                UF = item.EnderecoObra.UF,
                                CEP = item.EnderecoObra.CEP,
                                CodImportacao = item.CodImportacao,
                                NumeroContrato = item.Contrato,
                                Tipo = EClienteEnderecoType.Obra
                            },
                            commandType: CommandType.StoredProcedure
                            ).Single());
                    }
                    catch (Exception ex)
                    {
                        item.ImportMensagem = $"{item.ImportMensagem} Erro ao executar sql spTabelaEnderecoCreate:{ex.Message}{ex.InnerException}";
                    }

                }
            }
        }

        public IEnumerable<ListContratoQueryResult> GetContratoAndClienteToJoin()
        {
            var sql = $"SELECT Contrato.Cod, NumeroContrato, ContratoCliente.CodCliente FROM Contrato " +
                $"LEFT JOIN ContratoCliente ON ContratoCliente.CodContrato = Contrato.Cod AND ContratoCliente.DataExcluido IS NULL " +
                $"WHERE Contrato.DataExcluido IS NULL";
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<ListContratoQueryResult>(sql, new { });
            }
        }

        public void CreateClienteEnderecosByList(ref List<ClienteEndereco> lista)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                //PERCORRE APENAS OS REGISTROS ONDE O ENDERECO ESTIVER COM COD == 0
                foreach (var item in lista.Where(x => x.CodClienteEnderecoCob == 0))
                {
                    try
                    {
                        item.CodClienteEnderecoCob = conn
                            .Query<int>("spClienteEnderecoInsert",
                            param: new
                            {
                                CodCliente = item.CodCliente,
                                CodEndereco = item.CodEnderecoCob,
                                CodContrato = item.CodContrato,
                                TipoEndereco = EClienteEnderecoType.Cobranca,
                            },
                            commandType: CommandType.StoredProcedure
                            ).Single();
                    }
                    catch (Exception ex)
                    {
                        item.ImportMensagem = $"{item.ImportMensagem} Erro ao executar sql spClienteEnderecoInsert:{ex.Message}{ex.InnerException}";
                    }
                }

                foreach (var item in lista.Where(x => x.CodEnderecoCob == 0))
                {
                    try
                    {
                        item.CodClienteEnderecoCob = conn
                            .Query<int>("spClienteEnderecoInsert",
                            param: new
                            {
                                CodCliente = item.CodCliente,
                                CodEndereco = item.CodEnderecoObra,
                                CodContrato = item.CodContrato,
                                TipoEndereco = EClienteEnderecoType.Obra,
                            },
                            commandType: CommandType.StoredProcedure
                            ).Single();
                    }
                    catch (Exception ex)
                    {
                        item.ImportMensagem = $"{item.ImportMensagem} Erro ao executar sql spClienteEnderecoInsert:{ex.Message}{ex.InnerException}";
                    }
                }
            }
        }

        public void UpdateClienteEnderecosByList(ref List<ClienteEndereco> lista)
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();

                //PERCORRE APENAS OS REGISTROS ONDE O ENDERECO ESTIVER COM COD == 0
                foreach (var item in lista.Where(x => x.CodClienteEnderecoCob > 0))
                {
                    try
                    {
                        item.CodClienteEnderecoCob = conn
                            .Query<int>("spClienteEnderecoUpdate",
                            param: new
                            {
                                Cod = item.CodClienteEnderecoCob,
                                CodCliente = item.CodCliente,
                                CodEndereco = item.CodEnderecoCob,
                                CodContrato = item.CodContrato,
                                TipoEndereco = EClienteEnderecoType.Cobranca,
                            },
                            commandType: CommandType.StoredProcedure
                            ).Single();
                    }
                    catch (Exception ex)
                    {
                        item.ImportMensagem = $"{item.ImportMensagem} Erro ao executar sql spClienteEnderecoInsert:{ex.Message}{ex.InnerException}";
                    }
                }

                foreach (var item in lista.Where(x => x.CodEnderecoObra == 0))
                {
                    try
                    {
                        item.CodClienteEnderecoObra = conn
                            .Query<int>("spClienteEnderecoUpdate",
                            param: new
                            {
                                Cod = item.CodClienteEnderecoObra,
                                CodCliente = item.CodCliente,
                                CodEndereco = item.CodEnderecoObra,
                                CodContrato = item.CodContrato,
                                TipoEndereco = EClienteEnderecoType.Obra
                            },
                            commandType: CommandType.StoredProcedure
                            ).Single();
                    }
                    catch (Exception ex)
                    {
                        item.ImportMensagem = $"{item.ImportMensagem} Erro ao executar sql spClienteEnderecoInsert:{ex.Message}{ex.InnerException}";
                    }
                }
            }
        }
    }
}
