﻿
using Cobranca.Domain.CobrancaContext.Commands.ProdutoCommands.Results;
using Cobranca.Domain.ServicoContext.Entities;
using Cobranca.Domain.ServicoContext.Enums;
using Cobranca.Domain.ServicoContext.Queries;
using Cobranca.Domain.ServicoContext.Repositories;
using Cobranca.Shared;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Cobranca.Infra.ServicoContext.Repositories
{
    public class ReguaEventEmailRepository : IReguaEventEmailRepository
    {
        private readonly DbContext _context;

        public ReguaEventEmailRepository(DbContext context)
        {
            _context = context;
        }


        public List<EnventoEmailInfoQueryResult> GetEventsEmail()
        {
            using (var conn = new SqlConnection(Settings.ConnectionString))
            {
                conn.Open();
                return conn
                    .Query<EnventoEmailInfoQueryResult>("spReguaGetListEventEmail"
                    , commandType: CommandType.StoredProcedure
                    //, commandTimeout: 3600
                    ).ToList();
            }
        }
    }
}
