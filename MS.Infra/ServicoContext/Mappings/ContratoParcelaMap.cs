﻿using Cobranca.Domain.CobrancaContext.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Infra.ServicoContext.Mappings
{
    public class ContratoParcelaMap : EntityTypeConfiguration<ContratoParcela>
    {
        public ContratoParcelaMap()
        {

            ToTable("ContratoParcela");
            // Primary Key
            HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.FluxoPagamento)
                .HasMaxLength(255);

            this.Property(t => t.NumeroParcela)
                .HasMaxLength(150);

            this.Property(t => t.Obs)
                .HasMaxLength(250);

            this.Property(t => t.LinhaDigitavel)
                .HasMaxLength(255);

            this.Property(t => t.Endereco)
                .HasMaxLength(250);

            this.Property(t => t.Numero)
                .HasMaxLength(250);

            this.Property(t => t.Complemento)
                .HasMaxLength(250);

            this.Property(t => t.Bairro)
                .HasMaxLength(250);

            this.Property(t => t.CEP)
                .HasMaxLength(250);

            this.Property(t => t.Cidade)
                .HasMaxLength(250);

            this.Property(t => t.Estado)
                .HasMaxLength(250);

            this.Property(t => t.CodSinteseCliente)
                .HasMaxLength(250);

            this.Property(t => t.NumeroNotaFiscal)
                .HasMaxLength(250);

            this.Property(t => t.TipoFatura)
                .HasMaxLength(250);

            this.Property(t => t.InstrucaoPagamento)
                .HasMaxLength(250);

            this.Property(t => t.RotaArea)
                .HasMaxLength(250);
            this.Property(t => t.CodRegionalCliente)
               .HasMaxLength(250);
            this.Property(t => t.Companhia)
                .HasMaxLength(250);
            this.Property(t => t.CompanhiaFiscal)
                .HasMaxLength(250);


            this.Property(t => t.CodigoEmpresaCobranca)
             .HasMaxLength(250);

            this.Property(t => t.CodCliente)
             .HasMaxLength(250);

            this.Property(t => t.RegionalParcela)
             .HasMaxLength(250);

            this.Property(t => t.ClienteParcela)
             .HasMaxLength(250);



            // Table & Column Mappings
            this.ToTable("ContratoParcela");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.CodNatureza).HasColumnName("CodNatureza");
            this.Property(t => t.FluxoPagamento).HasColumnName("FluxoPagamento");
            this.Property(t => t.DataVencimento).HasColumnName("DataVencimento");
            this.Property(t => t.DataPagamento).HasColumnName("DataPagamento");
            this.Property(t => t.ValorParcela).HasColumnName("ValorParcela");
            this.Property(t => t.ValorRecebido).HasColumnName("ValorRecebido");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.NaturezaCliente).HasColumnName("NaturezaCliente");
            this.Property(t => t.StatusParcela).HasColumnName("StatusParcela");
            this.Property(t => t.ValorAberto).HasColumnName("ValorAberto");
            this.Property(t => t.NumeroParcela).HasColumnName("NumeroParcela");
            this.Property(t => t.Juros).HasColumnName("Juros");
            this.Property(t => t.Multa).HasColumnName("Multa");
            this.Property(t => t.ValorAtualizado).HasColumnName("ValorAtualizado");
            this.Property(t => t.DataEmissao).HasColumnName("DataEmissao");
            this.Property(t => t.CodAcordo).HasColumnName("CodAcordo");
            this.Property(t => t.Obs).HasColumnName("Obs");
            this.Property(t => t.FormaPagamento).HasColumnName("FormaPagamento");
            this.Property(t => t.ValorRetornoBanco).HasColumnName("ValorRetornoBanco");
            this.Property(t => t.LogAlteracao).HasColumnName("LogAlteracao");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.DataImportacao).HasColumnName("DataImportacao");
            this.Property(t => t.CodHistorico).HasColumnName("CodHistorico");
            this.Property(t => t.LinhaDigitavel).HasColumnName("LinhaDigitavel");
            this.Property(t => t.CodImportacaoBoleto).HasColumnName("CodImportacaoBoleto");
            this.Property(t => t.DataDocumento).HasColumnName("DataDocumento");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.Numero).HasColumnName("Numero");
            this.Property(t => t.Complemento).HasColumnName("Complemento");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.CEP).HasColumnName("CEP");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.Estado).HasColumnName("Estado");
            this.Property(t => t.CodSinteseCliente).HasColumnName("CodSinteseCliente");
            this.Property(t => t.NumeroNotaFiscal).HasColumnName("NumeroNotaFiscal");
            this.Property(t => t.TipoFatura).HasColumnName("TipoFatura");
            this.Property(t => t.DataFechamento).HasColumnName("DataFechamento");
            this.Property(t => t.InstrucaoPagamento).HasColumnName("InstrucaoPagamento");
            this.Property(t => t.RotaArea).HasColumnName("RotaArea");
            this.Property(t => t.DataEmissaoFatura).HasColumnName("DataEmissaoFatura");
            this.Property(t => t.CodRegionalCliente).HasColumnName("CodRegionalCliente");
            this.Property(t => t.Companhia).HasColumnName("Companhia");
            this.Property(t => t.NossoNumero).HasColumnName("NossoNumero");
            this.Property(t => t.CodigoEmpresaCobranca).HasColumnName("CodigoEmpresaCobranca");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.NumeroLinhaCliente).HasColumnName("NumeroLinhaCliente");
            this.Property(t => t.Imposto).HasColumnName("Imposto");
            this.Property(t => t.ValorAbertoPercentual).HasColumnName("ValorAbertoPercentual");
            this.Property(t => t.CodBoletoAprovado).HasColumnName("CodBoletoAprovado");
            this.Property(t => t.RegionalParcela).HasColumnName("RegionalParcela");
            this.Property(t => t.ClienteParcela).HasColumnName("ClienteParcela");
            this.Property(t => t.CompanhiaFiscal).HasColumnName("CompanhiaFiscal");
        }

    }
}
