﻿namespace Cobranca.Shared.Commands
{
    public interface ICommand
    {
        bool Valid();
    }
}
