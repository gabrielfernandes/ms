﻿using Cobranca.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Cobranca.Data.Services
{
    public class ContratoDapper : DapperService
    {
        

        public ContratoDapperModel GetByContrato(string numeroContrato)
        {
            var result = new ContratoDapperModel();
            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    var r = sqlConnection.Query<ContratoDapperModel>(@"
                    select c.Cod, cc.CodCliente as CodContratoCliente, ccj.Cod as CodContratoClienteConjugue from Contrato c
                    left join ContratoCliente cc on cc.CodContrato = c.Cod and cc.CodCliente = @codCliente and cc.DataExcluido is null
                    left join ContratoCliente ccj on ccj.CodContrato = c.Cod and ccj.CodCliente = @codClienteConjuge and cc.DataExcluido is null
                    where NumeroContrato = @numeroContrato and c.DataExcluido is null", new  { NumeroContrato = numeroContrato });

                    if (r.Count() == 0 || r == null)
                        result = new ContratoDapperModel() { Cod = 0, CodRegionalProduto = 0, NumeroContrato = "" };
                    else
                        result = r.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                var erro = ex.Message;
                throw;
            }
            return result;
        }


    }
}
