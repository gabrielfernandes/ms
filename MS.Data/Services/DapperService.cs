﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace Cobranca.Data.Services
{
    public class DapperService
    {
        public string ConnectionString = ConfigurationManager.ConnectionStrings["junix_cobrancaContext"].ConnectionString.ToString();

        public List<T> GetAll<T>()
        {
            List<T> list = new List<T>();

            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    var _nomeTabela = typeof(T).Name;

                    var result = sqlConnection.Query<T>("select * from " + _nomeTabela + " where dataexcluido IS NULL");

                    foreach (T item in result)
                        list.Add(item);
                }
            }
            catch (Exception ex)
            {
                var erro = ex.Message;
                list = null;
                throw;
            }

            return list;
        }

        public T GetById<T>(int id)
        {
            var result = default(T);
            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    var _nomeTabela = typeof(T).Name;

                    result = sqlConnection.Query<T>("select * from " + _nomeTabela + " where cod = @id", new { id = id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                var erro = ex.Message;
                throw;
            }

            return result;
        }
    }
}
