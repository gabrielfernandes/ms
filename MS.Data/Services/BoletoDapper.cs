﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cobranca.Data.Model;

namespace Cobranca.Data.Services
{
    public class BoletoDapper : DapperService
    {
        public List<BoletoDapperModel> ObterBoletos(
            short Status,
            DateTime? PeriodoDe,
            DateTime? PeriodoAte,
            List<int> BoletoIds,
            int CodBancoPortadorGrupo)
        {
            var list = new List<BoletoDapperModel>();
            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    #region query sql
                    var sql = $@"
                    SELECT 
                        B.Cod
                        ,B.Cod as CodBoleto
                        ,B.CodParcela
                        ,B.NumeroDocumento
                        ,B.NossoNumero
                        ,B.DataDocumento
                        ,B.DataVencimento
                        ,B.ValorDocumento
                        ,B.ValorDesconto
                        ,B.ValorPago
                        ,B.TipoOcorrencia
                        ,B.Status
                        ,B.DataStatus
                        ,B.DataAprovacao
                        ,B.DataCadastro
                        ,B.DataExcluido
                        ,B.DataAlteracao
                        ,B.DataRemessa
                        ,B.DataRetorno
                        ,B.CodUsuario
                        ,B.CodUsuarioRemessa
                        ,B.CodUsuarioRetorno
                        ,B.CodOcorrencia
                        ,B.DescOcorrencia
                        ,B.Erro
                        ,B.JurosAoMesPorcentagem
                        ,B.MultaAtrasoPorcentagem
                        ,B.ValorJuros
                        ,B.ValorMulta
                        ,B.JurosAoMesPorcentagemPrevisto
                        ,B.MultaAtrasoPorcentagemPrevisto
                        ,B.ValorJurosPrevisto
                        ,B.ValorMultaPrevisto
                        ,B.ValorDocumentoPrevisto
                        ,B.ValorDocumentoOriginal
                        ,B.PeriodoEmMes
                        ,B.CodHistorico
                        ,B.CodCliente
                        ,B.Companhia
                        ,B.Alterar
                        ,B.CodLote
                        ,B.CodUsuarioStatus
                        ,B.Imposto
                        ,B.ValorDocumentoAberto
                        ,B.MensagemImpressa
	                    ,CONVERT(int,'341') as BancoNumero
	                    ,CL.CodCliente as ClienteCodCliente
	                    ,CL.Nome as ClienteNome
	                    ,CL.CPF as ClienteCPF
	                    ,CL.Endereco as ClienteEndereco
	                    ,CL.EnderecoNumero as ClienteEnderecoNumero
	                    ,CL.CEP as ClienteEnderecoCEP
	                    ,CL.Bairro as ClienteEnderecoBairro
	                    ,CL.Cidade as ClienteEnderecoCidade
	                    ,CL.UF as ClienteEnderecoUF
	                    ,CL.EnderecoComplemento as ClienteEnderecoComplemento
	                    ,CP.TipoFatura as ParcelaTipoFatura
	                    ,BancoPortadorInstrucao.Cod as Cod
	                    ,BancoPortadorInstrucao.Descricao as Descricao
                        ,CP.CompanhiaFiscal
                        ,CP.TipoFatura as ParcelaTipoFatura
                        ,CP.NumeroParcela as NumeroParcela
                        ,BancoPortadorGrupo.Cod as CodBancoPortadorGrupo
                    FROM ContratoParcelaBoleto B WITH(NOLOCK)
                      INNER JOIN ContratoParcela CP WITH(NOLOCK) ON CP.Cod = B.CodParcela AND CP.DataExcluido IS NULL
                      INNER JOIN Contrato C WITH(NOLOCK) ON C.Cod = CP.CodContrato AND C.DataExcluido IS NULL
                      OUTER APPLY (SELECT TOP 1 * FROM ContratoCliente WITH(NOLOCK) WHERE ContratoCliente.CodContrato = CP.CodContrato AND ContratoCliente.DataExcluido IS NULL)CC
                      LEFT JOIN Cliente CL WITH(NOLOCK) ON CL.Cod = CC.CodCliente 
                      OUTER APPLY (SELECT TOP 1 * FROM TabelaBancoPortador WITH(NOLOCK) WHERE TabelaBancoPortador.CompanhiaFiscal = CP.CompanhiaFiscal AND TabelaBancoPortador.DataExcluido IS NULL)BancoPortador
                      LEFT JOIN TabelaBancoPortadorInstrucao BancoPortadorInstrucao WITH(NOLOCK) ON BancoPortadorInstrucao.CodBancoPortador = BancoPortador.Cod
					  LEFT JOIN TabelaBancoPortadorGrupo BancoPortadorGrupo WITH(NOLOCK) ON BancoPortadorGrupo.Cod = BancoPortador.CodBancoPortadorGrupo
                    WHERE
                        (B.Status = @statusBoleto OR @statusBoleto IS NULL)
                        AND (cast(B.DataAprovacao as date) >= cast(@dataDe as date) OR @dataDe IS NULL)
                        AND (cast(B.DataAprovacao as date) <= cast(@dataAte as date) OR @dataAte IS NULL)
                        AND (BancoPortadorGrupo.Cod = @CodBancoPortadorGrupo OR @CodBancoPortadorGrupo IS NULL)
                        ";
                    if (BoletoIds != null)
                    {
                        sql = sql + $@"AND(B.Cod IN({string.Join(",", BoletoIds)}))";
                    }

                    #endregion

                    var boletoDictionary = new Dictionary<int, BoletoDapperModel>();
                    var result = sqlConnection.Query<BoletoDapperModel, BoletoDapperInfo, BoletoDapperModel>(
                                    sql,
                                    (b, i) => { return tratarInformacao(b, i, ref boletoDictionary); },
                                    param: new { statusBoleto = Status, dataDe = PeriodoDe, dataAte = PeriodoAte, codBancoPortadorGrupo = CodBancoPortadorGrupo },
                                    splitOn: "CodBoleto,CodParcela"
                                  ).AsQueryable();

                    return boletoDictionary.Values.ToList();
                }
            }
            catch (Exception ex)
            {
                var erro = ex.Message;
                list = null;
                throw;
            }

        }

        private BoletoDapperModel tratarInformacao(BoletoDapperModel b, BoletoDapperInfo info, ref Dictionary<int, BoletoDapperModel> boletoDictionary)
        {
            BoletoDapperModel boleto;

            if (!boletoDictionary.TryGetValue(b.CodBoleto, out boleto))
                boletoDictionary.Add(b.CodBoleto, boleto = new BoletoDapperModel(info) { Cod = b.CodBoleto, CodBoleto = b.CodBoleto }
               );

            if (boleto.Instrucoes == null)
                boleto.Instrucoes = new List<BoletoInstrucaoDapperModel>();

            boleto.Instrucoes.Add(new BoletoInstrucaoDapperModel() { Cod = info.Cod, Descricao = info.Descricao });
            return boleto;
        }

    }
}
