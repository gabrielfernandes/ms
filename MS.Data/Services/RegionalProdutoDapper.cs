﻿using Cobranca.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Cobranca.Data.Services
{
    public class RegionalProdutoDapper : DapperService
    {

        public List<RegionalProdutoDapperModel> GetAllRegionalProdutos()
        {
            var result = new List<RegionalProdutoDapperModel>();
            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    result = sqlConnection.Query<RegionalProdutoDapperModel>(@"
                        SELECT DISTINCT RP.Cod as CodRegionalProduto, R.Cod as CodRegional, RGrupo.CodRegionalCliente, P.Produto
                        FROM TabelaRegionalProduto RP  WITH(NOLOCK)
                        LEFT JOIN TabelaProduto P  WITH(NOLOCK) ON P.Cod = RP.CodProduto AND P.DataExcluido IS NULL
                        LEFT JOIN TabelaRegional R WITH(NOLOCK)  ON R.Cod = RP.CodRegional  AND R.DataExcluido IS NULL
                        LEFT JOIN TabelaRegionalGrupo RGrupo WITH(NOLOCK) ON RGrupo.CodRegional = R.Cod AND RGrupo.DataExcluido IS NULL").ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                var erro = ex.Message;
                result = null;
                throw new Exception($"Erro ao executar o comando",ex);
            }
        }

    }
}
