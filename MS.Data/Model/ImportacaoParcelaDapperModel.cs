﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Data.Model
{
    public class ImportacaoParcelaDapperModel
    {
        public string Companhia { get; set; }
        public string Filial { get; set; }
        public string NumeroContrato { get; set; }
        public string DescricaodaFilial { get; set; }
        public string RotaArea { get; set; }
        public string StatusContrato { get; set; }
        public string TipoContrato { get; set; }
        public string NumeroCliente { get; set; }
        public string CpfCnpj { get; set; }
        public string Descricao { get; set; }
        public string NumeroFatura { get; set; }
        public Nullable<Decimal> ValorFatura { get; set; }
        public Nullable<Decimal> ValorAberto { get; set; }
        public Nullable<DateTime> DataVencimento { get; set; }
        public Nullable<DateTime> DataFechamento { get; set; }
        public string NumeroParcela { get; set; }
        public string Governo { get; set; }
        public string Corporativo { get; set; }
        public string ClassificacaoFatura { get; set; }
        public string NotaFiscal { get; set; }
        public string TipoDocumento { get; set; }
        public Nullable<DateTime> DataEmissaoFatura { get; set; }
        public string InstrumentoPgto { get; set; }
        public Nullable<DateTime> DataPagamento { get; set; }
        public string NossoNumero { get; set; }
        public string EmpresaCobrança { get; set; }
        public string CodClienteParcela { get; set; }
        public string NumeroLinha { get; set; }
        public Nullable<Decimal> Imposto { get; set; }
    }
}
