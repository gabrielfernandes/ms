﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Data.Model
{
    public class BoletoDapperModel
    {
        /// <summary> 
		/// Retorna a Id do Boleto
		/// </summary>
        public int Cod
        {
            get { return this._cod; }
            set { this._cod = value; }
        }
        /// <summary> 
        /// Retorna a Id da Parcela
        /// </summary>
        public Nullable<int> CodParcela
        {
            get { return this._codParcela; }
            set { this._codParcela = value; }
        }
        /// <summary> 
        /// Retorna a Id da Acordo
        /// </summary>
        public Nullable<int> CodAcordo
        {
            get { return this._codAcordo; }
            set { this._codAcordo = value; }
        }
        /// <summary> 
        /// Retorna a Id da Contrato
        /// </summary>
        public Nullable<int> CodContrato
        {
            get { return this._codContrato; }
            set { this._codContrato = value; }
        }
        /// <summary> 
        /// Retorna o Numero do Documento
        /// </summary>
        public string NumeroDocumento
        {
            get { return this._numeroDocumento; }
            set { this._numeroDocumento = value; }
        }
        /// <summary> 
        /// Retorna o Nosso numero
        /// </summary>
        public Nullable<int> NossoNumero
        {
            get { return this._nossoNumero; }
            set { this._nossoNumero = value; }
        }
        /// <summary> 
        /// Retorna a Data do Documento
        /// </summary>
        public Nullable<System.DateTime> DataDocumento
        {
            get { return this._dataDocumento; }
            set { this._dataDocumento = value; }
        }
        /// <summary> 
        /// Retorna a Data de Vencimento
        /// </summary>
        public Nullable<System.DateTime> DataVencimento
        {
            get { return this._dataVencimento; }
            set { this._dataVencimento = value; }
        }
        /// <summary> 
        /// Retorna a Data do Credito
        /// </summary>
        public Nullable<System.DateTime> DataCredito
        {
            get { return this._dataCredito; }
            set { this._dataCredito = value; }
        }
        /// <summary> 
        /// Retorna o Valor do Documento
        /// </summary>
        public Nullable<decimal> ValorDocumento
        {
            get { return this._valorDocumento; }
            set { this._valorDocumento = value; }
        }
        /// <summary> 
        /// Retorna o Valor do Desconto
        /// </summary>
        public Nullable<decimal> ValorDesconto
        {
            get { return this._valorDesconto; }
            set { this._valorDesconto = value; }
        }

        /// <summary> 
        /// Retorna o Valor Pago
        /// </summary>
        public Nullable<decimal> ValorPago
        {
            get { return this._valorPago; }
            set { this._valorPago = value; }
        }

        /// <summary> 
        /// Retorna o tipo de ocorrencia
        /// </summary>
        public Nullable<short> TipoOcorrencia
        {
            get { return this._tipoOcorrencia; }
            set { this._tipoOcorrencia = value; }
        }

        /// <summary> 
        /// Retorna o status
        /// </summary>
        public Nullable<short> Status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        /// <summary> 
        /// Retorna da data que foi criado o status
        /// </summary>
        public Nullable<System.DateTime> DataStatus
        {
            get { return this._dataStatus; }
            set { this._dataStatus = value; }
        }

        /// <summary> 
        /// Retorna a data que foi aprovado
        /// </summary>
        public Nullable<System.DateTime> DataAprovacao
        {
            get { return this._dataAprovacao; }
            set { this._dataAprovacao = value; }
        }

        /// <summary> 
        /// Retorna a guid
        /// </summary>
        public string Guid
        {
            get { return this._guid; }
            set { this._guid = value; }
        }

        /// <summary> 
        /// Retorna a instrucao1
        /// </summary>
        public string Instrucao1
        {
            get { return this._instrucao1; }
            set { this._instrucao1 = value; }
        }
        /// <summary> 
        /// Retorna a instrucao2
        /// </summary>
        public string Instrucao2
        {
            get { return this._instrucao2; }
            set { this._instrucao2 = value; }
        }
        /// <summary> 
        /// Retorna a instrucao3
        /// </summary>
        public string Instrucao3
        {
            get { return this._instrucao3; }
            set { this._instrucao3 = value; }
        }

        /// <summary> 
        /// Retorna a data de cadastro
        /// </summary>
        public System.DateTime DataCadastro
        {
            get { return this._dataCadastro; }
            set { this._dataCadastro = value; }
        }
        /// <summary> 
        /// Retorna a data de exclusão
        /// </summary>
        public Nullable<System.DateTime> DataExcluido
        {
            get { return this._dataExcluido; }
            set { this._dataExcluido = value; }
        }

        /// <summary> 
        /// Retorna a data de alteração
        /// </summary>
        public Nullable<System.DateTime> DataAlteracao
        {
            get { return this._dataAlteracao; }
            set { this._dataAlteracao = value; }
        }

        /// <summary> 
        /// Retorna a data da remessa
        /// </summary>
        public Nullable<System.DateTime> DataRemessa
        {
            get { return this._dataRemessa; }
            set { this._dataRemessa = value; }
        }

        /// <summary> 
        /// Retorna a data do retorno bancario
        /// </summary>
        public Nullable<System.DateTime> DataRetorno
        {
            get { return this._dataRetorno; }
            set { this._dataRetorno = value; }
        }

        /// <summary> 
        /// Retorna o usuario que solicitou
        /// </summary>
        public Nullable<int> CodUsuario
        {
            get { return this._codUsuario; }
            set { this._codUsuario = value; }
        }

        /// <summary> 
        /// Retorna o usuario que criou a remessa
        /// </summary>
        public Nullable<int> CodUsuarioRemessa
        {
            get { return this._codUsuarioRemessa; }
            set { this._codUsuarioRemessa = value; }
        }

        /// <summary> 
        /// Retorna o usuario do retorno
        /// </summary>
        public Nullable<int> CodUsuarioRetorno
        {
            get { return this._codUsuarioRetorno; }
            set { this._codUsuarioRetorno = value; }
        }

        /// <summary> 
        /// Retorna o codifo da ocorrencia bancaria
        /// </summary>
        public string CodOcorrencia
        {
            get { return this._CodOcorrencia; }
            set { this._CodOcorrencia = value; }
        }

        /// <summary> 
        /// Retorna a descricao da ocorrencia bancaria
        /// </summary>
        public string DescOcorrencia
        {
            get { return this._descOcorrencia; }
            set { this._descOcorrencia = value; }
        }

        /// <summary> 
        /// Retorna o erro
        /// </summary>
        public string Erro
        {
            get { return this._erro; }
            set { this._erro = value; }
        }

        /// <summary> 
        /// Retorna o juros percentagem
        /// </summary>
        public Nullable<decimal> JurosAoMesPorcentagem
        {
            get { return this._jurosAoMesPorcentagem; }
            set { this._jurosAoMesPorcentagem = value; }
        }
        /// <summary> 
        /// Retorna a multa percentagem
        /// </summary>
        public Nullable<decimal> MultaAtrasoPorcentagem
        {
            get { return this._multaAtrasoPorcentagem; }
            set { this._multaAtrasoPorcentagem = value; }
        }

        /// <summary> 
        /// Retorna o valor do juros
        /// </summary>
        public Nullable<decimal> ValorJuros
        {
            get { return this._valorJuros; }
            set { this._valorJuros = value; }
        }

        /// <summary> 
        /// Retorna o valor da multa
        /// </summary>
        public Nullable<decimal> ValorMulta
        {
            get { return this._valorMulta; }
            set { this._valorMulta = value; }
        }
        /// <summary> 
        /// Retorna o juros percentagem previsto
        /// </summary>
        public Nullable<decimal> JurosAoMesPorcentagemPrevisto
        {
            get { return this._jurosAoMesPorcentagemPrevisto; }
            set { this._jurosAoMesPorcentagemPrevisto = value; }
        }

        /// <summary> 
        /// Retorna a multa percentagem prevista
        /// </summary>
        public Nullable<decimal> MultaAtrasoPorcentagemPrevisto
        {
            get { return this._multaAtrasoPorcentagemPrevisto; }
            set { this._multaAtrasoPorcentagemPrevisto = value; }
        }

        /// <summary> 
        /// Retorna o valor de juros previsto
        /// </summary>
        public Nullable<decimal> ValorJurosPrevisto
        {
            get { return this._valorJurosPrevisto; }
            set { this._valorJurosPrevisto = value; }
        }

        /// <summary> 
        /// Retorna o valor de multa previsto
        /// </summary>
        public Nullable<decimal> ValorMultaPrevisto
        {
            get { return this._valorMultaPrevisto; }
            set { this._valorMultaPrevisto = value; }
        }

        /// <summary> 
        /// Retorna o valor do documento previsto
        /// </summary>
        public Nullable<decimal> ValorDocumentoPrevisto
        {
            get { return this._valorDocumentoPrevisto; }
            set { this._valorDocumentoPrevisto = value; }
        }

        /// <summary> 
        /// Retorna o valor do documento original
        /// </summary>
        public Nullable<decimal> ValorDocumentoOriginal
        {
            get { return this._valorDocumentoOriginal; }
            set { this._valorDocumentoOriginal = value; }
        }

        /// <summary> 
        /// Retorna o periodo em mes
        /// </summary>
        public Nullable<int> PeriodoEmMes
        {
            get { return this._periodoEmMes; }
            set { this._periodoEmMes = value; }
        }

        /// <summary> 
        /// Retorna o codigo do ultimo historico
        /// </summary>
        public Nullable<int> CodHistorico
        {
            get { return this._codHistorico; }
            set { this._codHistorico = value; }
        }

        /// <summary> 
        /// Retorna o id do cliente
        /// </summary>
        public string CodCliente
        {
            get { return this._codCliente; }
            set { this._codCliente = value; }
        }

        /// <summary> 
        /// Retorna a companhia
        /// </summary>
        public string Companhia
        {
            get { return this._companhia; }
            set { this._companhia = value; }
        }

        /// <summary> 
        /// Retorna se o boleto é uma alteração
        /// </summary>
        public Nullable<bool> Alterar
        {
            get { return this._alterar; }
            set { this._alterar = value; }
        }

        /// <summary> 
        /// Retorna o id do lote de remessa emitido
        /// </summary>
        public Nullable<int> CodLote
        {
            get { return this._codLote; }
            set { this._codLote = value; }
        }
        public Nullable<int> CodUsuarioStatus
        {
            get { return this._codUsuarioStatus; }
            set { this._codUsuarioStatus = value; }
        }

        /// <summary> 
        /// Retorna o imposto
        /// </summary>
        public Nullable<decimal> Imposto
        {
            get { return this._imposto; }
            set { this._imposto = value; }
        }

        /// <summary> 
        /// Retorna o valor aberto do documento
        /// </summary>
        public Nullable<decimal> ValorDocumentoAberto
        {
            get { return this._valorDocumentoAberto; }
            set { this._valorDocumentoAberto = value; }
        }

        /// <summary> 
        /// Retorna a mensagem a impressa
        /// </summary>
        public string MensagemImpressa
        {
            get { return this._mensagemImpressa; }
            set { this._mensagemImpressa = value; }
        }

        /// <summary> 
        /// Retorna o numero do banco
        /// </summary>
        public Nullable<int> BancoNumero
        {
            get { return this._bancoNumero; }
            set { this._bancoNumero = value; }
        }

        /// <summary> 
        /// Retorna a companhia fiscal
        /// </summary>
        public string CompanhiaFiscal
        {
            get { return this._companhiaFiscal; }
            set { this._companhiaFiscal = value; }
        }

        /// <summary> 
        /// Retorna o id do boleto
        /// </summary>
        public int CodBoleto
        {
            get { return this._codBoleto; }
            set { this._codBoleto = value; }
        }

        public IList<BoletoInstrucaoDapperModel> Instrucoes
        {
            get { return this._instrucoes; }
            set { this._instrucoes = value; }
        }

        #region CLIENTE
        public string ClienteCodCliente
        {
            get { return this._clienteCodCliente; }
            set { this._clienteCodCliente = value; }
        }
        public string ClienteNome
        {
            get { return this._clienteNome; }
            set { this._clienteNome = value; }
        }
        public string ClientePrimeiroNome
        {
            get { return this._clientePrimeiroNome; }
            set { this._clientePrimeiroNome = value; }
        }
        public string ClienteCPF
        {
            get { return this._clienteCPF; }
            set { this._clienteCPF = value; }
        }
        public string ClienteEnderecoCEP
        {
            get { return this._clienteEnderecoCEP; }
            set { this._clienteEnderecoCEP = value; }
        }
        public string ClienteEnderecoEndereco
        {
            get { return this._clienteEnderecoEndereco; }
            set { this._clienteEnderecoEndereco = value; }
        }
        public string ClienteEnderecoCidade
        {
            get { return this._clienteEnderecoCidade; }
            set { this._clienteEnderecoCidade = value; }
        }
        public string ClienteEnderecoEstado
        {
            get { return this._clienteEnderecoEstado; }
            set { this._clienteEnderecoEstado = value; }
        }
        public string ClienteEnderecoBairro
        {
            get { return this._clienteEnderecoBairro; }
            set { this._clienteEnderecoBairro = value; }
        }
        public string ClienteEnderecoUF
        {
            get { return this._clienteEnderecoUF; }
            set { this._clienteEnderecoUF = value; }
        }
        public string ClienteEnderecoNumero
        {
            get { return this._clienteEnderecoNumero; }
            set { this._clienteEnderecoNumero = value; }
        }
        public string ClienteEnderecoComplemento
        {
            get { return this._clienteEnderecoComplemento; }
            set { this._clienteEnderecoComplemento = value; }
        }
        #endregion

        public string NumeroParcela
        {
            get { return this._numeroParcela; }
            set { this._numeroParcela = value; }
        }

        public int CodBancoPortadorGrupo
        {
            get { return this._codBancoPortadorGrupo; }
            set { this._codBancoPortadorGrupo = value; }
        }

        #region PARCELA
        public string ParcelaTipoFatura
        {
            get { return this._parcelaTipoFatura; }
            set { this._parcelaTipoFatura = value; }
        }
        #endregion

        #region Constructor
        public BoletoDapperModel() { }
        public BoletoDapperModel(BoletoDapperInfo info)
        {
            _cod = info.Cod;
            _codBoleto = info.CodBoleto;
            _codParcela = info.CodParcela;
            _codAcordo = info.CodAcordo;
            _codContrato = info.CodContrato;
            _numeroDocumento = info.NumeroDocumento;
            _nossoNumero = info.NossoNumero;
            _dataDocumento = info.DataDocumento;
            _dataVencimento = info.DataVencimento;
            _dataCredito = info.DataCredito;
            _valorDocumento = info.ValorDocumento;
            _valorDesconto = info.ValorDesconto;
            _valorPago = info.ValorPago;
            _tipoOcorrencia = info.TipoOcorrencia;
            _status = info.Status;
            _dataStatus = info.DataStatus;
            _dataAprovacao = info.DataAprovacao;
            _guid = info.Guid;
            _instrucao1 = info.Instrucao1;
            _instrucao2 = info.Instrucao2;
            _instrucao3 = info.Instrucao3;
            _dataCadastro = info.DataCadastro;
            _dataExcluido = info.DataExcluido;
            _dataAlteracao = info.DataAlteracao;
            _dataRemessa = info.DataRemessa;
            _dataRetorno = info.DataRetorno;
            _codUsuario = info.CodUsuario;
            _codUsuarioRemessa = info.CodUsuarioRemessa;
            _codUsuarioRetorno = info.CodUsuarioRetorno;
            _CodOcorrencia = info.CodOcorrencia;
            _descOcorrencia = info.DescOcorrencia;
            _erro = info.Erro;
            _jurosAoMesPorcentagem = info.JurosAoMesPorcentagem;
            _multaAtrasoPorcentagem = info.MultaAtrasoPorcentagem;
            _valorJuros = info.ValorJuros;
            _valorMulta = info.ValorMulta;
            _valorJurosPrevisto = info.ValorJurosPrevisto;
            _valorMultaPrevisto = info.ValorMultaPrevisto;
            _valorDocumentoPrevisto = info.ValorDocumentoPrevisto;
            _valorDocumentoOriginal = info.ValorDocumentoOriginal;
            _periodoEmMes = info.PeriodoEmMes;
            _codHistorico = info.CodHistorico;
            _codCliente = info.CodCliente;
            _companhia = info.Companhia;
            _alterar = info.Alterar;
            _codLote = info.CodLote;
            _codUsuarioStatus = info.CodUsuarioStatus;
            _imposto = info.Imposto;
            _valorDocumentoAberto = info.ValorDocumentoAberto;
            _mensagemImpressa = info.MensagemImpressa;
            _bancoNumero = info.BancoNumero;
            _companhiaFiscal = info.CompanhiaFiscal;
            _clienteCodCliente = info.ClienteCodCliente;
            _clienteNome = info.ClienteNome;
            _clientePrimeiroNome = info.ClientePrimeiroNome;
            _clienteCPF = info.ClienteCPF;
            _clienteEnderecoCEP = info.ClienteEnderecoCEP;
            _clienteEnderecoEndereco = info.ClienteEnderecoEndereco;
            _clienteEnderecoCidade = info.ClienteEnderecoCidade;
            _clienteEnderecoEstado = info.ClienteEnderecoEstado;
            _clienteEnderecoBairro = info.ClienteEnderecoBairro;
            _clienteEnderecoUF = info.ClienteEnderecoUF;
            _clienteEnderecoNumero = info.ClienteEnderecoNumero;
            _clienteEnderecoComplemento = info.ClienteEnderecoComplemento;
            _parcelaTipoFatura = info.ParcelaTipoFatura;
            _numeroParcela = info.NumeroParcela;
        }
        #endregion

        #region local variavel
        private int _cod;
        private Nullable<int> _codParcela;
        private Nullable<int> _codAcordo;
        private Nullable<int> _codContrato;
        private string _numeroDocumento;
        private Nullable<int> _nossoNumero;
        private Nullable<System.DateTime> _dataDocumento;
        private Nullable<System.DateTime> _dataVencimento;
        private Nullable<System.DateTime> _dataCredito;
        private Nullable<decimal> _valorDocumento;
        private Nullable<decimal> _valorDesconto;
        private Nullable<decimal> _valorPago;
        private Nullable<short> _tipoOcorrencia;
        private Nullable<short> _status;
        private Nullable<DateTime> _dataStatus;
        private Nullable<DateTime> _dataAprovacao;
        private string _guid;
        private string _instrucao1;
        private string _instrucao2;
        private string _instrucao3;
        private DateTime _dataCadastro;
        private Nullable<DateTime> _dataExcluido;
        private Nullable<DateTime> _dataAlteracao;
        private Nullable<DateTime> _dataRemessa;
        private Nullable<DateTime> _dataRetorno;
        private Nullable<int> _codUsuario;
        private Nullable<int> _codUsuarioRemessa;
        private Nullable<int> _codUsuarioRetorno;
        private string _CodOcorrencia;
        private string _descOcorrencia;
        private string _erro;
        private Nullable<decimal> _jurosAoMesPorcentagem;
        private Nullable<decimal> _multaAtrasoPorcentagem;
        private Nullable<decimal> _valorJuros;
        private Nullable<decimal> _valorMulta;
        private Nullable<decimal> _jurosAoMesPorcentagemPrevisto;
        private Nullable<decimal> _multaAtrasoPorcentagemPrevisto;
        private Nullable<decimal> _valorJurosPrevisto;
        private Nullable<decimal> _valorMultaPrevisto;
        private Nullable<decimal> _valorDocumentoPrevisto;
        private Nullable<decimal> _valorDocumentoOriginal;
        private Nullable<int> _periodoEmMes;
        private Nullable<int> _codHistorico;
        private string _codCliente;
        private string _companhia;
        private Nullable<bool> _alterar;
        private Nullable<int> _codLote;
        private Nullable<int> _codUsuarioStatus;
        private Nullable<decimal> _imposto;
        private Nullable<decimal> _valorDocumentoAberto;
        private string _mensagemImpressa;
        private Nullable<int> _bancoNumero;
        private string _companhiaFiscal;
        private int _codBoleto;
        private IList<BoletoInstrucaoDapperModel> _instrucoes;
        private string _clienteCodCliente;
        private string _clienteNome;
        private string _clientePrimeiroNome;
        private string _clienteCPF;
        private string _clienteEnderecoCEP;
        private string _clienteEnderecoEndereco;
        private string _clienteEnderecoCidade;
        private string _clienteEnderecoEstado;
        private string _clienteEnderecoBairro;
        private string _clienteEnderecoUF;
        private string _clienteEnderecoNumero;
        private string _clienteEnderecoComplemento;
        private string _parcelaTipoFatura;
        private string _numeroParcela;
        private int _codBancoPortadorGrupo;
        #endregion
    }

    public class BoletoInstrucaoDapperModel
    {
        public int Cod { get; set; }
        public string Descricao { get; set; }

    }

    public class BoletoDapperInfo
    {
        public int Cod { get; set; }
        public Nullable<int> CodParcela { get; set; }
        public Nullable<int> CodAcordo { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public string NumeroDocumento { get; set; }
        public Nullable<int> NossoNumero { get; set; }
        public Nullable<System.DateTime> DataDocumento { get; set; }
        public Nullable<System.DateTime> DataVencimento { get; set; }
        public Nullable<System.DateTime> DataCredito { get; set; }
        public Nullable<decimal> ValorDocumento { get; set; }
        public Nullable<decimal> ValorDesconto { get; set; }
        public Nullable<decimal> ValorPago { get; set; }
        public Nullable<short> TipoOcorrencia { get; set; }
        public Nullable<short> Status { get; set; }
        public Nullable<System.DateTime> DataStatus { get; set; }
        public Nullable<System.DateTime> DataAprovacao { get; set; }
        public string Guid { get; set; }
        public string Instrucao1 { get; set; }
        public string Instrucao2 { get; set; }
        public string Instrucao3 { get; set; }
        public System.DateTime DataCadastro { get; set; }
        public Nullable<System.DateTime> DataExcluido { get; set; }
        public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<System.DateTime> DataRemessa { get; set; }
        public Nullable<System.DateTime> DataRetorno { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public Nullable<int> CodUsuarioRemessa { get; set; }
        public Nullable<int> CodUsuarioRetorno { get; set; }
        public string CodOcorrencia { get; set; }
        public string DescOcorrencia { get; set; }
        public string Erro { get; set; }
        public Nullable<decimal> JurosAoMesPorcentagem { get; set; }
        public Nullable<decimal> MultaAtrasoPorcentagem { get; set; }
        public Nullable<decimal> ValorJuros { get; set; }
        public Nullable<decimal> ValorMulta { get; set; }

        public Nullable<decimal> JurosAoMesPorcentagemPrevisto { get; set; }
        public Nullable<decimal> MultaAtrasoPorcentagemPrevisto { get; set; }
        public Nullable<decimal> ValorJurosPrevisto { get; set; }
        public Nullable<decimal> ValorMultaPrevisto { get; set; }
        public Nullable<decimal> ValorDocumentoPrevisto { get; set; }
        public Nullable<decimal> ValorDocumentoOriginal { get; set; }
        public Nullable<int> PeriodoEmMes { get; set; }
        public Nullable<int> CodHistorico { get; set; }
        public string CodCliente { get; set; }
        public string Companhia { get; set; }
        public Nullable<bool> Alterar { get; set; }
        public Nullable<int> CodLote { get; set; }
        public Nullable<int> CodUsuarioStatus { get; set; }
        public Nullable<decimal> Imposto { get; set; }
        public Nullable<decimal> ValorDocumentoAberto { get; set; }
        public string MensagemImpressa { get; set; }
        public Nullable<int> BancoNumero { get; set; }
        public string CompanhiaFiscal { get; set; }
        public int CodBoleto { get; set; }
        public string Descricao { get; set; }

        public string ClienteCodCliente { get; set; }
        public string ClienteNome { get; set; }
        public string ClientePrimeiroNome { get; set; }
        public string ClienteCPF { get; set; }
        public string ClienteEnderecoCEP { get; set; }
        public string ClienteEnderecoEndereco { get; set; }
        public string ClienteEnderecoCidade { get; set; }
        public string ClienteEnderecoEstado { get; set; }
        public string ClienteEnderecoBairro { get; set; }
        public string ClienteEnderecoUF { get; set; }
        public string ClienteEnderecoNumero { get; set; }
        public string ClienteEnderecoComplemento { get; set; }
        public string ParcelaTipoFatura { get; set; }
        public string NumeroParcela { get; set; }
    }
}
