﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Data.Model
{
    [Table("RegionalProduto")]
    public class RegionalProdutoDapperModel
    {
        [Key]
        public int Cod { get; set; }
        public Nullable<int> CodRegional { get; set; }
        public string CodRegionalCliente { get; set; }
        public string Produto { get; set; }
        public Nullable<int> CodRegionalProduto { get; set; }
        public Nullable<DateTime> DataExcluido { get; set; }

    }
}
