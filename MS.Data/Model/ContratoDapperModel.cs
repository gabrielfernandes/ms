﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Data.Model
{
    [Table("Contrato")]
    public class ContratoDapperModel
    {
        [Key]
        public int Cod { get; set; }
        public string NumeroContrato { get; set; }
        public Nullable<int> CodRegionalProduto { get; set; }
        public Nullable<DateTime> DataExcluido { get; set; }

    }
}
