﻿using Cobranca.Db.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    public class PropostaStatusDb
    {
        Repository<Proposta> _dbProposta = new Repository<Proposta>();
        Repository<PropostaStatusHistorico> _dbPropostaHistorico = new Repository<PropostaStatusHistorico>();

        public void AlterarStatusProposta( int CodPorposta, Enumeradores.StatusProposta status, int CodUsuario, string Observacao )
        {
            var proposta = _dbProposta.FindById(CodPorposta);

            if(proposta != null)
            {
                proposta.Status = (short)status;

                PropostaStatusHistorico historico = new PropostaStatusHistorico();
                historico.CodProposta = CodPorposta;
                historico.CodUsuario = CodUsuario;
                historico.Status = (short)status;
                historico.Observacao = Observacao;

                _dbProposta.CreateOrUpdate(proposta);
                _dbPropostaHistorico.CreateOrUpdate(historico);
            }
        }
    }
}
