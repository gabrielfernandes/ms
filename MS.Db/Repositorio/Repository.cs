﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Cobranca.Db.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Data.Entity.Core.Objects;
using System.Reflection;

namespace Cobranca.Db.Repositorio
{
    public class Repository<TEntity> : IRepository<TEntity>, IDisposable
        where TEntity : BaseEntity
        //where TContext : DbContext
    {
        protected junix_cobrancaContext Context;


        public Repository(DbContext dbContext)
        {
            Context = dbContext as junix_cobrancaContext;
        }

        public Repository()
        {
            //DbContext dbContext = new junix_cobrancaContext();
            Context = new junix_cobrancaContext();
            //((IObjectContextAdapter)Context).ObjectContext.ObjectMaterialized += new ObjectMaterializedEventHandler(ObjectMaterialized);

        }

        public virtual TEntity Create()
        {
            return Context.Set<TEntity>().Create();
        }

        public virtual TEntity Create(TEntity entity)
        {
            return Context.Set<TEntity>().Add(entity);
        }

        public virtual TEntity Update(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            this.SaveChanges();
            return entity;
        }

        public virtual void Delete(long id)
        {
            var item = Context.Set<TEntity>().Find(id);

            item.DataExcluido = DateTime.Now;

            this.CreateOrUpdate(item);

            this.SaveChanges();

            //Context.Set<TEntity>().Remove(item);
        }

        public virtual void Delete(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public virtual void Delete(Expression<Func<TEntity, bool>> where)
        {
            var objects = Context.Set<TEntity>().Where(where).ToList();
            foreach (var item in objects)
            {
                item.DataExcluido = DateTime.Now;
                this.CreateOrUpdate(item);
            }
            this.SaveChanges();
        }
        public virtual void Remove(Expression<Func<TEntity, bool>> where)
        {
            var objects = Context.Set<TEntity>().Where(where).AsEnumerable();
            foreach (var item in objects)
            {
                Context.Set<TEntity>().Remove(item);
            }
        }


        public virtual TEntity FindById(int id)
        {
            //return DecryptEntity(Context.Set<TEntity>().Find(id));
            return Context.Set<TEntity>().Find(id);
        }

        public virtual TEntity FindOne(Expression<Func<TEntity, bool>> where = null)
        {
           //return DecryptEntity(FindAll(where).FirstOrDefault());
           return FindAll(where).FirstOrDefault();
        }

        public IQueryable<T> Set<T>() where T : class
        {
            return Context.Set<T>();
        }

        public virtual IQueryable<TEntity> FindAll(Expression<Func<TEntity, bool>> where = null)
        {
            return null != where ? Context.Set<TEntity>().Where(x => !x.DataExcluido.HasValue).Where(where) : Context.Set<TEntity>().Where(x => !x.DataExcluido.HasValue);
        }

        public virtual IQueryable<TEntity> All()
        {
            //var list = Context.Set<TEntity>().Where(x => !x.DataExcluido.HasValue).ToList();
            //List<TEntity> listReturn = new List<TEntity>();
            //foreach (var l in list)
            //{
            //
            //    listReturn.Add(DecryptEntity(l));
            //}
            //
            //return listReturn.AsQueryable();
            return Context.Set<TEntity>().Where(x => !x.DataExcluido.HasValue);
        }

      


        public virtual bool SaveChanges()
        {
            return 0 < Context.SaveChanges();

        }

        /// <summary>
        /// Releases all resources used by the Entities
        /// </summary>
        public void Dispose()
        {
            if (null != Context)
            {
                Context.Dispose();
            }
        }

        public virtual TEntity CreateOrUpdate(TEntity entity)
        {

            if (entity.Cod == 0)
            {
                entity.DataCadastro = DateTime.Now;
                entity = this.Create(entity);
            }
            else
            {
                var baseEntity = Context.Set<TEntity>().AsNoTracking().FirstOrDefault(x => x.Cod == entity.Cod);
                entity.DataAlteracao = DateTime.Now;

                DbEntityEntry<TEntity> entityEntry = Context.Entry(entity);

                if (entityEntry.State == EntityState.Detached)
                {
                    Context.Set<TEntity>().Attach(entity);

                    entity.DataCadastro = baseEntity.DataCadastro;
                    entityEntry.State = EntityState.Modified;
                }
            }

            this.SaveChanges();
            return entity;
        }
       

        public virtual System.Data.Entity.Validation.DbEntityValidationResult ValidateEntity(TEntity entity)
        {
            DbEntityEntry<TEntity> entityEntry = Context.Entry(entity);
            //System.Data.Entity.Validation.DbEntityValidationResult Errors = new System.Data.Entity.Validation.DbEntityValidationResult(entity,new List<

            System.Data.Entity.Validation.DbEntityValidationResult Errors = new System.Data.Entity.Validation.DbEntityValidationResult(entityEntry, new List<System.Data.Entity.Validation.DbValidationError>());


            return Errors;
        }


        public virtual IEnumerable<DictionaryEntry> SelectList(Expression<Func<TEntity, bool>> where = null)
        {

            List<TEntity> list = new List<TEntity>();
            return new List<DictionaryEntry>();
            //if (where != null)
            //{
            //    list = this.All().Where(where).ToList();
            //}
            //else
            //    list = this.All().Where(x => !x.DataExclusao.HasValue).ToList();

            //list = list.OrderBy(x => x.Nome).ToList();

            //var item = (TEntity)Activator.CreateInstance(typeof(TEntity));

            //list.Insert(0, item);

            //var selectList = from p in list select new DictionaryEntry(p.Nome, p.Id);

            //return selectList;
        }

        //public virtual SelectList SelectList(Expression<Func<TEntity, bool>> where = null)
        //{

        //    List<TEntity> list = new List<TEntity>();
        //    if (where != null)
        //    {
        //        list = this.All().Where(where).ToList();
        //    }
        //    else
        //        list = this.All().Where(x => !x.DataExclusao.HasValue).ToList();

        //    var item = (TEntity)Activator.CreateInstance(typeof(TEntity));

        //    list.Insert(0, item);

        //    var selectList = new SelectList(list, "Id", "Nome");


        //    return selectList;
        //}

        //#region Encryption
        //public int SaveChanges()
        //{
        //    var contextAdapter = ((IObjectContextAdapter)Context);

        //    contextAdapter.ObjectContext.DetectChanges(); //force this. Sometimes entity state needs a handle jiggle

        //    var pendingEntities = contextAdapter.ObjectContext.ObjectStateManager
        //        .GetObjectStateEntries(EntityState.Added | EntityState.Modified)
        //        .Where(en => !en.IsRelationship).ToList();

        //    foreach (var entry in pendingEntities) //Encrypt all pending changes
        //        EncryptEntity(entry.Entity);

        //    int result = Context.SaveChanges();

        //    foreach (var entry in pendingEntities) //Decrypt updated entities for continued use
        //        Decrypt(entry.Entity);

        //    return result;
        //}
        //public async Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken)
        //{
        //    var contextAdapter = ((IObjectContextAdapter)this);

        //    contextAdapter.ObjectContext.DetectChanges(); //force this. Sometimes entity state needs a handle jiggle

        //    var pendingEntities = contextAdapter.ObjectContext.ObjectStateManager
        //        .GetObjectStateEntries(EntityState.Added | EntityState.Modified)
        //        .Where(en => !en.IsRelationship).ToList();

        //    foreach (var entry in pendingEntities) //Encrypt all pending changes
        //        EncryptEntity(entry.Entity);

        //    var result = await Context.SaveChangesAsync(cancellationToken);

        //    foreach (var entry in pendingEntities) //Decrypt updated entities for continued use
        //        Decrypt(entry.Entity);

        //    return result;
        //}

        //void ObjectMaterialized(object sender, ObjectMaterializedEventArgs e)
        //{
        //    //var context = sender as ObjectContext;
        //    //if (context == null)
        //    //    return;
        //    //Decrypt(e.Entity);
        //    //foreach (ObjectStateEntry entry in context.ObjectStateManager.GetObjectStateEntries(EntityState.  | EntityState.Modified))
        //    //{
               
        //    //}
        
        //}
        //private void EncryptEntity(object entity)
        //{
        //    //Get all the properties that are encryptable and encrypt them
        //    var encryptedProperties = entity.GetType().GetProperties()
        //        .Where(p => p.PropertyType == typeof(String));

        //    foreach (var property in encryptedProperties)
        //    {
        //        string value = property.GetValue(entity) as string;
        //        if (!String.IsNullOrEmpty(value))
        //        {
        //            string encryptedValue = Encrypt(value);
        //            property.SetValue(entity, encryptedValue);
        //        }
        //    }
        //}

        //private TEntity DecryptEntity(TEntity entity)
        //{
        //    //Get all the properties that are encryptable and decyrpt them
        //    var tipo = typeof(Encrypted).GetType();
        //    var encryptedProperties = entity.GetType().GetProperties()
        //       .Where(p => p.PropertyType == typeof(String));

        //    foreach (var property in encryptedProperties)
        //    {
        //        string encryptedValue = property.GetValue(entity) as string;
        //        if (!String.IsNullOrEmpty(encryptedValue))
        //        {
        //            string value = Encrypt(encryptedValue);
        //            Context.Entry(entity).Property(property.Name).OriginalValue = value;
        //            Context.Entry(entity).Property(property.Name).IsModified = false;
        //        }

        //    }
        //    return entity;
        //}
        //private void Decrypt(object entity)
        //{
        //    //Get all the properties that are encryptable and decyrpt them
        //    var tipo = typeof(Encrypted).GetType();
        //    var encryptedProperties = entity.GetType().GetProperties()
        //       .Where(p => p.PropertyType == typeof(String));

        //    foreach (var property in encryptedProperties)
        //    {
        //        string encryptedValue = property.GetValue(entity) as string;
        //        if (!String.IsNullOrEmpty(encryptedValue))
        //        {
        //            string value = Encrypt(encryptedValue);
        //            Context.Entry(entity).Property(property.Name).OriginalValue = value;
        //            Context.Entry(entity).Property(property.Name).IsModified = false;
        //        }

        //    }
        //}

        //private static string Encrypt(string Senha)
        //{
        //    string Chave = "XINUJ-EZTECRSFD";
        //    string NovaSenha;

        //    for (int x = 0; x < Chave.Length; x++)
        //    {
        //        NovaSenha = "";
        //        for (int y = 0; y < Senha.Length; y++)
        //        {
        //            NovaSenha += Convert.ToChar(((Chave[x]) ^ (Senha[y])));
        //        }
        //        Senha = NovaSenha;
        //    }
        //    return Senha;
        //}
        //#endregion Encryption

    }
}