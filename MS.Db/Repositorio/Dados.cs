﻿using Cobranca.Db.Models;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Db.Rotinas;
using Cobranca.Db.Suporte;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    public class Dados
    {
        //
        protected junix_cobrancaContext Context;
        public Dados()
        {
            Context = new junix_cobrancaContext();
        }

        public List<ContratoDados> ListarContratosPaginadas(int inicio, int tamanho)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            parametros.Add(new System.Data.SqlClient.SqlParameter("@RegistroDe", inicio));
            parametros.Add(new System.Data.SqlClient.SqlParameter("@RegistroAte", inicio + tamanho));

            var dados = Context.Database.SqlQuery<ContratoDados>("SP_CONTRATO_DADOS @RegistroDe, @RegistroAte",
                new SqlParameter("RegistroDe", inicio)
                , new SqlParameter("RegistroAte", inicio + tamanho)
                );
            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ContratoDados> ListarContratosHome(

            int? CodTipoCobranca,
            string CodConstrutora,
            string CodRegional,
            string CodProduto,
            int? CodAgingCliente
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ContratoDados>(@"EXECUTE [dbo].[SP_CONSULTA_DADOS_HOME] 
                @CodTipoCobranca
                ,@CodConstrutora
                ,@CodRegional
                ,@CodProduto
                ,@CodAging"
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodAging", DbRotinas.ConverterDbValue(CodAgingCliente))

                );
            return dados.ToList();
        }

        public List<CalculaParcelaContrato> ListarValorParcela(
           decimal? ValorProduto,
           int? QtdParcela
           )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<CalculaParcelaContrato>(@"EXECUTE [dbo].[SP_CONSULTA_GERADOR_PARCELA] 
                @QtdParcela
                ,@ValorProduto"
                , new SqlParameter("@QtdParcela", DbRotinas.ConverterDbValue(QtdParcela))
                , new SqlParameter("@ValorProduto", DbRotinas.ConverterDbValue(ValorProduto))
                );
            return dados.ToList();
        }
        public List<ParcelaAcordo> ListarParcelaAcordo(string variavel, string juros, string multa, string honorario, string desconto, int? tipo, string qtdParcela)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ParcelaAcordo>(@"EXECUTE [dbo].[SP_CONSULTA_PARCELA_ACORDO] 
                     @VARIAVEL
                   , @JUROS
                   , @MULTA
                   , @HONORARIO
                   , @DESCONTO
                   , @TIPO
                   , @QUANTIDADE"
                , new SqlParameter("@VARIAVEL", DbRotinas.ConverterDbValue(variavel))
                  , new SqlParameter("@JUROS", DbRotinas.ConverterDbValue(juros))
                    , new SqlParameter("@MULTA", DbRotinas.ConverterDbValue(multa))
                      , new SqlParameter("@HONORARIO", DbRotinas.ConverterDbValue(honorario))
                        , new SqlParameter("@DESCONTO", DbRotinas.ConverterDbValue(desconto))
                          , new SqlParameter("@TIPO", tipo)
                              , new SqlParameter("@QUANTIDADE", Convert.ToString(qtdParcela))
                               );
            return dados.ToList();
        }

        public List<HistoricoDados> ListarHistoricoesContratoHome(

             int? CodTipoCobranca,
            string CodConstrutora,
            string CodRegional,
            string CodProduto,
            string CodRotas,
            int? CodAgingCliente,
            DateTime? PeriodoDe,
            DateTime? PeriodoAte,
            int? CodUsuario,
            int? CodPerfilTipo
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<SqlParameter>();
            Context.Database.CommandTimeout = 120;
            var dados = Context.Database.SqlQuery<HistoricoDados>(@"EXECUTE [dbo].[SP_CONSULTA_DADOS_HOME_HISTORICO_CONTRATO] 
                @CodTipoCobranca
                ,@CodConstrutora
                ,@CodRegional
                ,@CodProduto
                ,@CodRota
                ,@CodAging
                ,@PeriodoDe
                ,@PeriodoAte
                ,@CodUsuario
                ,@CodPerfilTipo"
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodRota", DbRotinas.ConverterDbValue(CodRotas))
                , new SqlParameter("@CodAging", DbRotinas.ConverterDbValue(CodAgingCliente))
                , new SqlParameter("@PeriodoDe", DbRotinas.ConverterDbValue(PeriodoDe))
                , new SqlParameter("@PeriodoAte", DbRotinas.ConverterDbValue(PeriodoAte))
                , new SqlParameter("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario))
                , new SqlParameter("@CodPerfilTipo", DbRotinas.ConverterDbValue(CodPerfilTipo))

                );
            return dados.ToList();
        }
        public List<HistoricoDados> ListarHistoricoesParcelaHome(

             int? CodTipoCobranca,
            string CodConstrutora,
            string CodRegional,
            string CodProduto,
            string CodRotas,
            string CodAgingCliente,
            DateTime? PeriodoDe,
            DateTime? PeriodoAte,
            int? CodUsuario,
            int? CodPerfilTipo
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<SqlParameter>();

            var dados = Context.Database.SqlQuery<HistoricoDados>(@"EXECUTE [dbo].[SP_CONSULTA_DADOS_HOME_HISTORICO_PARCELA] 
                @CodTipoCobranca
                ,@CodConstrutora
                ,@CodRegional
                ,@CodProduto
                ,@CodRota
                ,@CodAging
                ,@PeriodoDe
                ,@PeriodoAte
                ,@CodUsuario
                ,@CodPerfilTipo"
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodRota", DbRotinas.ConverterDbValue(CodRotas))
                , new SqlParameter("@CodAging", DbRotinas.ConverterDbValue(CodAgingCliente))
                , new SqlParameter("@PeriodoDe", DbRotinas.ConverterDbValue(PeriodoDe))
                , new SqlParameter("@PeriodoAte", DbRotinas.ConverterDbValue(PeriodoAte))
                , new SqlParameter("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario))
                , new SqlParameter("@CodPerfilTipo", DbRotinas.ConverterDbValue(CodPerfilTipo))

                );
            return dados.ToList();
        }
        public List<HistoricoDados> ListarHistoricoesClienteHome(

             int? CodTipoCobranca,
            string CodConstrutora,
            string CodRegional,
            string CodProduto,
            int? CodAgingCliente,
            DateTime? PeriodoDe,
            DateTime? PeriodoAte
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<SqlParameter>();

            var dados = Context.Database.SqlQuery<HistoricoDados>(@"EXECUTE [dbo].[SP_CONSULTA_DADOS_HOME_HISTORICO_CLIENTE] 
                @CodTipoCobranca
                ,@CodConstrutora
                ,@CodRegional
                ,@CodProduto
                ,@CodAging
                ,@PeriodoDe
                ,@PeriodoAte"
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodAging", DbRotinas.ConverterDbValue(CodAgingCliente))
                , new SqlParameter("@PeriodoDe", DbRotinas.ConverterDbValue(PeriodoDe))
                , new SqlParameter("@PeriodoAte", DbRotinas.ConverterDbValue(PeriodoAte))

                );
            return dados.ToList();
        }
        public List<HistoricoDados> ListarHistoricoesBoletoHome(

             int? CodTipoCobranca,
            string CodConstrutora,
            string CodRegional,
            string CodProduto,
            string CodRotas,
            string CodAgingCliente,
            DateTime? PeriodoDe,
            DateTime? PeriodoAte,
            int? CodUsuario,
            int? CodPerfilTipo,
            int? TipoFluxo
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<SqlParameter>();

            var dados = Context.Database.SqlQuery<HistoricoDados>(@"EXECUTE [dbo].[SP_CONSULTA_DADOS_HOME_HISTORICO_BOLETO] 
                @CodTipoCobranca
                ,@CodConstrutora
                ,@CodRegional
                ,@CodProduto
                ,@CodRota
                ,@CodAging
                ,@PeriodoDe
                ,@PeriodoAte
                ,@CodUsuario
                ,@CodPerfilTipo
                ,@TipoFluxo"
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodRota", DbRotinas.ConverterDbValue(CodRotas))
                , new SqlParameter("@CodAging", DbRotinas.ConverterDbValue(CodAgingCliente))
                , new SqlParameter("@PeriodoDe", DbRotinas.ConverterDbValue(PeriodoDe))
                , new SqlParameter("@PeriodoAte", DbRotinas.ConverterDbValue(PeriodoAte))
                , new SqlParameter("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario))
                , new SqlParameter("@CodPerfilTipo", DbRotinas.ConverterDbValue(CodPerfilTipo))
                , new SqlParameter("@TipoFluxo", DbRotinas.ConverterDbValue(TipoFluxo))

                );
            return dados.ToList();
        }
        public int executarCondicosTipoCobranca(int cod, int codLog)
        {
            var dados = Context.Database.ExecuteSqlCommand("SP_EXECCUTAR_CONDICOES_POR_TIPO_COBRANCA @CodigoTipoCobranca, @CodLogProcessamento",
               new SqlParameter("@CodigoTipoCobranca", DbRotinas.ConverterDbValue(cod))
               , new SqlParameter("@CodLogProcessamento", DbRotinas.ConverterDbValue(codLog)));

            return dados;
        }

        public void executarAtualizaValorParcela(int cod)
        {
            var dados = Context.Database.ExecuteSqlCommand(@"[dbo].[SP_ATUALIZA_VALORATUALIZADO] @CODPARCELA",
               new SqlParameter("@CODPARCELA", DbRotinas.ConverterDbValue(cod)));
        }
        public List<CasosEnviarEmail> executarServicoEmail()
        {
            var dados = Context.Database.SqlQuery<CasosEnviarEmail>(@"[dbo].[SP_SERVICO_PROCESSA_EMAIL]");
            return dados.ToList();
        }
        public List<ContratoParcelaBoletoTotais> ObterTotaisBoletoSolicitacao(int? CodUsuario, short? TipoFluxo, string Filiais)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<SqlParameter>();

            var dados = Context.Database.SqlQuery<ContratoParcelaBoletoTotais>(@"EXECUTE [dbo].[SP_QUANTIDADE_BOLETO_FASE_SINTESE] 
                @CodUsuario
                ,@TipoFluxo
                ,@CodFilial"
                , new SqlParameter("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario))
                , new SqlParameter("@TipoFluxo", DbRotinas.ConverterDbValue(TipoFluxo))
                , new SqlParameter("@CodFilial", DbRotinas.ConverterDbValue(Filiais))
                );
            return dados.ToList();
        }
        public void executarProcessarAcaoCobranca()
        {
            Context.Database.CommandTimeout = 120;
            var dados = Context.Database.ExecuteSqlCommand("SP_PROCESSAR_ACAO_COBRANCA");
        }
        public void computarHistoricoPorContrato(int cod)
        {
            try
            {
                var dados = Context.Database.ExecuteSqlCommand("SP_COMPUTAR_HISTORICO_POR_CONTRATO @CodContrato",
               new SqlParameter("@CodContrato", DbRotinas.ConverterDbValue(cod)));
            }
            catch { }

        }

        public List<ImportacaoConsultar> ListarImportacao(short Tipo = 0)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            var dados = Context.Database.SqlQuery<ImportacaoConsultar>(@"EXECUTE [dbo].[SP_IMPORTACAO_CONSULTAR] 
                     @Tipo"
                , new SqlParameter("@Tipo", DbRotinas.ConverterDbValue(Tipo))
                );
            return dados.ToList();
        }

        //
        public void executarCongelamentoDiarioDados()
        {

            try
            {
                var dados = Context.Database.ExecuteSqlCommand("SP_COMPUTAR_VALORES_DIARIOS");
            }
            catch (Exception)
            {
            }

        }

        public List<BoletoSolicitacaoDados> ListarDadosBoletoRemessa(DateTime? PeriodoDe, DateTime? PeriodoAte, int? CodBancoPortador)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            Context.Database.CommandTimeout = 120;
            var dados = Context.Database.SqlQuery<BoletoSolicitacaoDados>(@"EXECUTE [dbo].[SP_CONSULTA_DADOS_BOLETO_REMESSA] 
                @PeriodoDe          
                ,@PeriodoAte
                ,@CodBancoPortador",
                new SqlParameter("@PeriodoDe", DbRotinas.ConverterDbValue(PeriodoDe))
                , new SqlParameter("@PeriodoAte", DbRotinas.ConverterDbValue(PeriodoAte))
                , new SqlParameter("@CodBancoPortador", DbRotinas.ConverterDbValue(CodBancoPortador))
                );
            return dados.ToList();
        }


        public List<BoletoSolicitacaoDados> ListarDadosBoleto(int? CodUsuario, int? CodFase, int? CodSintese, string Filiais)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            Context.Database.CommandTimeout = 120;
            var dados = Context.Database.SqlQuery<BoletoSolicitacaoDados>(@"EXECUTE [dbo].[SP_CONSULTA_DADOS_SOLICITACAO_BOLETO] 
                @CodUsuario          
                ,@CodFase
                ,@CodSintese
                ,@CodFilial",
                new SqlParameter("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario))
                , new SqlParameter("@CodFase", DbRotinas.ConverterDbValue(CodFase))
                , new SqlParameter("@CodSintese", DbRotinas.ConverterDbValue(CodSintese))
                , new SqlParameter("@CodFilial", DbRotinas.ConverterDbValue(Filiais))
                );
            return dados.ToList();
        }

        public List<ContratoDados> ListarContratosDados(
            string CodigoVenda,
            string CodigoCliente,
            string NomeComprador,
            string PrimeiroNome,
            string CPF_CNPJ,
            int? CodConstrutora,
            int? CodDonoCarteira,
            int? CodRegional,
            int? CodTipoCobranca,
            int? CodTipoCarteira,
            int? CodGestao,
            int? CodEscritorio,
            int? CodEmpreendimento,
            int? CodBloco,
            int? CodUnidade,
            bool? Bloqueado,
            string CodAgingCliente,
            string AndamentoDaObra,
            string NumeroContrato,
            int? CodSintese,
            string CodProduto,
            int? CodFase,
            int? CodGrupo,
            int? CodTipoBloqueio

            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ContratoDados>(@"EXECUTE [dbo].[SP_CONTRATO_DADOS] 
                @CodigoVenda
                ,@CodigoCliente
                ,@NomeComprador
                ,@CPF_CNPJ
                ,@CodConstrutora
                ,@CodDonoCarteira
                ,@CodRegional
                ,@CodTipoCobranca
                ,@CodTipoCarteira
                ,@CodGestao
                ,@CodEscritorio
                ,@CodEmpreendimento
                ,@CodBloco
                ,@CodUnidade
                ,@Bloqueado
                ,@CodAgingCliente
                ,@AndamentoDaObra
                ,@NumeroContrato
                ,@CodSintese
                ,@CodProduto
                ,@CodFase
                ,@CodGrupo
                ,@CodTipoBloqueio",
                new SqlParameter("@CodigoVenda", DbRotinas.ConverterDbValue(CodigoVenda))
                , new SqlParameter("@CodigoCliente", DbRotinas.ConverterDbValue(CodigoCliente))
                , new SqlParameter("@NomeComprador", DbRotinas.ConverterDbValue(NomeComprador))
                , new SqlParameter("@CPF_CNPJ", DbRotinas.ConverterDbValue(CPF_CNPJ))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodDonoCarteira", DbRotinas.ConverterDbValue(CodDonoCarteira))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodTipoCarteira", DbRotinas.ConverterDbValue(CodTipoCarteira))
                , new SqlParameter("@CodGestao", DbRotinas.ConverterDbValue(CodGestao))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodEmpreendimento", DbRotinas.ConverterDbValue(CodEmpreendimento))
                , new SqlParameter("@CodBloco", DbRotinas.ConverterDbValue(CodBloco))
                , new SqlParameter("@CodUnidade", DbRotinas.ConverterDbValue(CodUnidade))
                , new SqlParameter("@Bloqueado", DBNull.Value)
                , new SqlParameter("@CodAgingCliente", DbRotinas.ConverterDbValue(CodAgingCliente))
                , new SqlParameter("@AndamentoDaObra", DBNull.Value)
                , new SqlParameter("@NumeroContrato", DbRotinas.ConverterDbValue(NumeroContrato))
                , new SqlParameter("@CodSintese", DbRotinas.ConverterDbValue(CodSintese))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodFase", DbRotinas.ConverterDbValue(CodFase))
                , new SqlParameter("@CodGrupo", DbRotinas.ConverterDbValue(CodGrupo))
                , new SqlParameter("@CodTipoBloqueio", DbRotinas.ConverterDbValue(CodTipoBloqueio))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ContratoDados> ListarContratosDadosCliente(
            string CodigoCliente,
            string NomeComprador,
            string PrimeiroNome,
            string CPF_CNPJ,
            int? CodConstrutora,
            int? CodRegional,
            int? CodTipoCobranca,
            bool? Bloqueado,
            string CodAgingCliente,
            string AndamentoDaObra,
            string NumeroContrato,
            int? CodSintese,
            string CodProduto,
            int? CodFase,
            int? CodGrupo,
            int? CodTipoBloqueio,
            int? CodSinteseParcela,
            int? ClienteId,
            string CodContratos,
            string CodParcelas

          )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();
            Context.Database.CommandTimeout = 120;
            var dados = Context.Database.SqlQuery<ContratoDados>(@"EXECUTE [dbo].[SP_CONTRATO_DADOS_CLIENTE] 
               @CodigoCliente          
                ,@NomeComprador
                ,@PrimeiroNome
                ,@CPF_CNPJ
                ,@CodConstrutora
                ,@CodRegional
                ,@CodTipoCobranca
                ,@Bloqueado
                ,@CodAgingCliente
                ,@AndamentoDaObra
                ,@NumeroContrato
                ,@CodSintese
                ,@CodProduto
                ,@CodFase
                ,@CodGrupo
                ,@CodTipoBloqueio
                ,@ClienteId
                ,@CodContratos
                ,@CodParcelas",
                new SqlParameter("@CodigoCliente", DbRotinas.ConverterDbValue(CodigoCliente))
                , new SqlParameter("@NomeComprador", DbRotinas.ConverterDbValue(NomeComprador))
                , new SqlParameter("@PrimeiroNome", DbRotinas.ConverterDbValue(PrimeiroNome))
                , new SqlParameter("@CPF_CNPJ", DbRotinas.ConverterDbValue(CPF_CNPJ))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@Bloqueado", DBNull.Value)
                , new SqlParameter("@CodAgingCliente", DbRotinas.ConverterDbValue(CodAgingCliente))
                , new SqlParameter("@AndamentoDaObra", DBNull.Value)
                , new SqlParameter("@NumeroContrato", DbRotinas.ConverterDbValue(NumeroContrato))
                , new SqlParameter("@CodSintese", DbRotinas.ConverterDbValue(CodSintese))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodFase", DbRotinas.ConverterDbValue(CodFase))
                , new SqlParameter("@CodGrupo", DbRotinas.ConverterDbValue(CodGrupo))
                , new SqlParameter("@CodTipoBloqueio", DbRotinas.ConverterDbValue(CodTipoBloqueio))
                , new SqlParameter("@ClienteId", DbRotinas.ConverterDbValue(ClienteId))
                , new SqlParameter("@CodContratos", DbRotinas.ConverterDbValue(CodContratos))
                , new SqlParameter("@CodParcelas", DbRotinas.ConverterDbValue(CodParcelas))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ContratoDados> ListarClienteContratoPorSintese(
            string CodigoCliente,
            string NomeComprador,
            string PrimeiroNome,
            string CPF_CNPJ,
            int? CodConstrutora,
            int? CodRegional,
            int? CodTipoCobranca,
            bool? Bloqueado,
            string CodAgingCliente,
            string AndamentoDaObra,
            string NumeroContrato,
            int? CodSintese,
            string CodProduto,
            int? CodFase,
            int? CodGrupo,
            int? CodTipoBloqueio,
            int? CodSinteseParcela,
            int? ClienteId,
            int? CodUsuario,
            string CodContratos,
            string CodParcelas

          )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ContratoDados>(@"EXECUTE [dbo].[SP_CLIENTE_DADOS_POR_SINTESE_CONTRATO] 
               @CodigoCliente              
                ,@NomeComprador
                ,@PrimeiroNome
                ,@CPF_CNPJ
                ,@CodConstrutora
                ,@CodRegional
                ,@CodTipoCobranca
                ,@Bloqueado
                ,@CodAgingCliente
                ,@AndamentoDaObra
                ,@NumeroContrato
                ,@CodSintese
                ,@CodProduto
                ,@CodFase
                ,@CodGrupo
                ,@CodTipoBloqueio 
                ,@ClienteId
                ,@CodUsuario
                ,@CodContratos
                ,@CodParcelas",
                new SqlParameter("@CodigoCliente", DbRotinas.ConverterDbValue(CodigoCliente))
                , new SqlParameter("@NomeComprador", DbRotinas.ConverterDbValue(NomeComprador))
                , new SqlParameter("@PrimeiroNome", DbRotinas.ConverterDbValue(PrimeiroNome))
                , new SqlParameter("@CPF_CNPJ", DbRotinas.ConverterDbValue(CPF_CNPJ))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@Bloqueado", DBNull.Value)
                , new SqlParameter("@CodAgingCliente", DbRotinas.ConverterDbValue(CodAgingCliente))
                , new SqlParameter("@AndamentoDaObra", DBNull.Value)
                , new SqlParameter("@NumeroContrato", DbRotinas.ConverterDbValue(NumeroContrato))
                , new SqlParameter("@CodSintese", DbRotinas.ConverterDbValue(CodSintese))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodFase", DbRotinas.ConverterDbValue(CodFase))
                , new SqlParameter("@CodGrupo", DbRotinas.ConverterDbValue(CodGrupo))
                , new SqlParameter("@CodTipoBloqueio", DbRotinas.ConverterDbValue(CodTipoBloqueio))
                , new SqlParameter("@ClienteId", DbRotinas.ConverterDbValue(ClienteId))
                , new SqlParameter("@CodSinteseParcela", DbRotinas.ConverterDbValue(CodSinteseParcela))
                , new SqlParameter("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario))
                , new SqlParameter("@CodContratos", DbRotinas.ConverterDbValue(CodContratos))
                , new SqlParameter("@CodParcelas", DbRotinas.ConverterDbValue(CodParcelas))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ContratoDados> ListarClienteDadosPorParcela(
           string CodigoCliente,
           string NomeComprador,
           string PrimeiroNome,
           string CPF_CNPJ,
           int? CodConstrutora,
           int? CodRegional,
           int? CodTipoCobranca,
           bool? Bloqueado,
           string CodAgingCliente,
           string AndamentoDaObra,
           string NumeroContrato,
           int? CodSintese,
           string CodProduto,
           int? CodFase,
           int? CodGrupo,
           int? CodTipoBloqueio,
           int? CodSinteseParcela,
           int? CodUsuario,
           int? CodPefilTipo

         )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ContratoDados>(@"EXECUTE [dbo].[SP_CLIENTE_DADOS_POR_SINTESE_PARCELA] 
                @CodigoCliente
                ,@NomeComprador
                ,@PrimeiroNome
                ,@CPF_CNPJ
                ,@CodConstrutora
                ,@CodRegional
                ,@CodTipoCobranca
                ,@Bloqueado
                ,@CodAgingCliente
                ,@AndamentoDaObra
                ,@NumeroContrato
                ,@CodSintese
                ,@CodProduto
                ,@CodFase
                ,@CodGrupo
                ,@CodTipoBloqueio
                ,@CodSinteseParcela
                ,@CodUsuario
                ,@CodPerfilTipo",
                 new SqlParameter("@CodigoCliente", DbRotinas.ConverterDbValue(CodigoCliente))
                , new SqlParameter("@NomeComprador", DbRotinas.ConverterDbValue(NomeComprador))
                , new SqlParameter("@PrimeiroNome", DbRotinas.ConverterDbValue(PrimeiroNome))
                , new SqlParameter("@CPF_CNPJ", DbRotinas.ConverterDbValue(CPF_CNPJ))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@Bloqueado", DBNull.Value)
                , new SqlParameter("@CodAgingCliente", DbRotinas.ConverterDbValue(CodAgingCliente))
                , new SqlParameter("@AndamentoDaObra", DBNull.Value)
                , new SqlParameter("@NumeroContrato", DbRotinas.ConverterDbValue(NumeroContrato))
                , new SqlParameter("@CodSintese", DbRotinas.ConverterDbValue(CodSintese))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodFase", DbRotinas.ConverterDbValue(CodFase))
                , new SqlParameter("@CodGrupo", DbRotinas.ConverterDbValue(CodGrupo))
                , new SqlParameter("@CodTipoBloqueio", DbRotinas.ConverterDbValue(CodTipoBloqueio))
                , new SqlParameter("@CodSinteseParcela", DbRotinas.ConverterDbValue(CodSinteseParcela))
                , new SqlParameter("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario))
                , new SqlParameter("@CodPerfilTipo", DbRotinas.ConverterDbValue(CodPefilTipo))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }
        public List<ContratoDados> ListarClienteParcelaDadosPorParcela(
           int CodCliente,
           string NomeComprador,
           string PrimeiroNome,
           string CPF_CNPJ,
           int? CodConstrutora,
           int? CodRegional,
           int? CodTipoCobranca,
           bool? Bloqueado,
           string CodAgingCliente,
           string AndamentoDaObra,
           string NumeroContrato,
           int? CodSintese,
           string CodProduto,
           int? CodFase,
           int? CodGrupo,
           int? CodTipoBloqueio,
           int? CodSinteseParcela,
           int? CodUsuario,
           int? CodPefilTipo

         )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ContratoDados>(@"EXECUTE [dbo].[SP_CLIENTE_PARCELA_DADOS_POR_SINTESE_PARCELA] 
                @CodCliente
                ,@NomeComprador
                ,@PrimeiroNome
                ,@CPF_CNPJ
                ,@CodConstrutora
                ,@CodRegional
                ,@CodTipoCobranca
                ,@Bloqueado
                ,@CodAgingCliente
                ,@AndamentoDaObra
                ,@NumeroContrato
                ,@CodSintese
                ,@CodProduto
                ,@CodFase
                ,@CodGrupo
                ,@CodTipoBloqueio
                ,@CodSinteseParcela
                ,@CodUsuario
                ,@CodPerfilTipo",
                 new SqlParameter("@CodCliente", DbRotinas.ConverterDbValue(CodCliente))
                , new SqlParameter("@NomeComprador", DbRotinas.ConverterDbValue(NomeComprador))
                , new SqlParameter("@PrimeiroNome", DbRotinas.ConverterDbValue(PrimeiroNome))
                , new SqlParameter("@CPF_CNPJ", DbRotinas.ConverterDbValue(CPF_CNPJ))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@Bloqueado", DBNull.Value)
                , new SqlParameter("@CodAgingCliente", DbRotinas.ConverterDbValue(CodAgingCliente))
                , new SqlParameter("@AndamentoDaObra", DBNull.Value)
                , new SqlParameter("@NumeroContrato", DbRotinas.ConverterDbValue(NumeroContrato))
                , new SqlParameter("@CodSintese", DbRotinas.ConverterDbValue(CodSintese))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodFase", DbRotinas.ConverterDbValue(CodFase))
                , new SqlParameter("@CodGrupo", DbRotinas.ConverterDbValue(CodGrupo))
                , new SqlParameter("@CodTipoBloqueio", DbRotinas.ConverterDbValue(CodTipoBloqueio))
                , new SqlParameter("@CodSinteseParcela", DbRotinas.ConverterDbValue(CodSinteseParcela))
                , new SqlParameter("@CodUsuario", DbRotinas.ConverterDbValue(CodUsuario))
                , new SqlParameter("@CodPerfilTipo", DbRotinas.ConverterDbValue(CodPefilTipo))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }
        public List<RelatorioDados> ListarRelatorioDados(
            int? CodConstrutora,
            int? CodRegional,
            int? CodEscritorio,
            int? CodProduto
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<RelatorioDados>(@"EXECUTE [dbo].[SP_RELATORIO_GERAL] 
                @CodConstrutora
                ,@CodRegional
                ,@CodEscritorio
                ,@CodProduto",
                new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<RelatorioDados> ListarRelatorioContatado(
         int? CodConstrutora,
            int? CodRegional,
            int? CodEscritorio,
            int? CodProduto
         )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<RelatorioDados>(@"EXECUTE [dbo].[SP_RELATORIO_CLIENTES_CONTATADO] 
                 @CodConstrutora
                ,@CodRegional
                ,@CodEscritorio
                ,@CodProduto",
                new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<RelatorioCreditoDados> ListarRelatorioCredito(
           int? CodConstrutora,
           int? CodRegional,
           int? CodEscritorio,
           int? CodProduto
           )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<RelatorioCreditoDados>(@"EXECUTE [dbo].[SP_RELATORIO_CREDITO] 
                @CodConstrutora
                ,@CodRegional
                ,@CodEscritorio
                ,@CodProduto",
                new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<RelatorioCreditoDados> ListarRelatorioAcordos(
           int? CodConstrutora,
           int? CodRegional,
           int? CodEscritorio,
           int? CodProduto
           )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<RelatorioCreditoDados>(@"EXECUTE [dbo].[SP_RELATORIO_ACORDO] 
                @CodConstrutora
                ,@CodRegional
                ,@CodEscritorio
                ,@CodProduto",
                new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<RelatorioAcaoCobrancaDados> ListarRelatorioAcoesCobranca(
          int? CodConstrutora,
          int? CodRegional,
          int? CodEscritorio,
          int? CodProduto
          )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<RelatorioAcaoCobrancaDados>(@"EXECUTE [dbo].[SP_RELATORIO_ACOES_COBRANCA] 
                @CodConstrutora
                ,@CodRegional
                ,@CodEscritorio
                ,@CodProduto",
                new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<RelatorioDados> ListarRelatorioAcordo(
           int? CodConstrutora,
           int? CodRegional,
           int? CodEscritorio,
           int? CodProduto
           )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<RelatorioDados>(@"EXECUTE [dbo].[SP_RELATORIO_GERAL] 
                @CodConstrutora
                ,@CodRegional
                ,@CodEscritorio
                ,@CodProduto",
                new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<RelatorioParcelaDados> ListarRelatorioParcela(
           int? CodConstrutora,
           int? CodRegional,
           int? CodEscritorio,
           int? CodProduto
           )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<RelatorioParcelaDados>(@"EXECUTE [dbo].[SP_RELATORIO_PARCELAS] 
                @CodConstrutora
                ,@CodRegional
                ,@CodEscritorio
                ,@CodProduto",
                new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ContratoParcelaDados> ExtrairClassificacaoParcela(
        int? CodFase,
        int? CodSintese,
        DateTime? De,
        DateTime? Ate
        )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();
            Context.Database.CommandTimeout = 900; //15 min
            var dados = Context.Database.SqlQuery<ContratoParcelaDados>(@"EXECUTE [dbo].[SP_EXPORTAR_CLASSIFICACAO_PARCELA] 
                @CodFase
                ,@CodSintese
                ,@De
                ,@Ate",
                new SqlParameter("@CodFase", DbRotinas.ConverterDbValue(CodFase))
                , new SqlParameter("@CodSintese", DbRotinas.ConverterDbValue(CodSintese))
                , new SqlParameter("@De", DbRotinas.ConverterDbValue(De))
                , new SqlParameter("@Ate", DbRotinas.ConverterDbValue(Ate))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ContratoParcelaDados> ExtrairPersonalizada(int? CodFase,
        int? CodSintese,
        DateTime? De,
        DateTime? Ate
        )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();
            Context.Database.CommandTimeout = 360;
            var dados = Context.Database.SqlQuery<ContratoParcelaDados>(@"EXECUTE [dbo].[SP_EXPORTAR_PERSONALIZADA]  
                @CodFase
                ,@CodSintese
                ,@De
                ,@Ate",
                new SqlParameter("@CodFase", DbRotinas.ConverterDbValue(CodFase))
                , new SqlParameter("@CodSintese", DbRotinas.ConverterDbValue(CodSintese))
                , new SqlParameter("@De", DbRotinas.ConverterDbValue(De))
                , new SqlParameter("@Ate", DbRotinas.ConverterDbValue(Ate))
                );
            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ContratoParcelaDados> ExtrairNegativacao(int? CodFase, int? CodSintese, DateTime? De, DateTime? Ate)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();
            Context.Database.CommandTimeout = 180;
            var dados = Context.Database.SqlQuery<ContratoParcelaDados>(@"EXECUTE [dbo].[SP_EXPORTAR_NEGATIVACAO] 
                @CodFase
                ,@CodSintese
                ,@De
                ,@Ate",
                new SqlParameter("@CodFase", DbRotinas.ConverterDbValue(CodFase))
                , new SqlParameter("@CodSintese", DbRotinas.ConverterDbValue(CodSintese))
                , new SqlParameter("@De", DbRotinas.ConverterDbValue(De))
                , new SqlParameter("@Ate", DbRotinas.ConverterDbValue(Ate))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }
        public List<ContratoDados> ListarContratosAcaoCobrancaHistoricoDados(

            int? TipoAcaoCobranca,
            int? CodTipoCobranca,
            int? CodAcaoCobranca,
            short? SituacaoTipo
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ContratoDados>(@"EXECUTE [dbo].[SP_CONTRATO_ACAO_COBRANCA_HISTORICO_DADOS] 
                @TipoAcaoCobranca
                ,@CodTipoCobranca
                ,@CodAcaoCobranca
                ,@SituacaoTipo",
                new SqlParameter("@TipoAcaoCobranca", DbRotinas.ConverterDbValue(TipoAcaoCobranca))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodAcaoCobranca", DbRotinas.ConverterDbValue(CodAcaoCobranca))
                , new SqlParameter("@SituacaoTipo", DbRotinas.ConverterDbValue(SituacaoTipo))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<CobrancaAcaoBoletoDados> ListarContratosAcaoCobrancaHistoricoDadosEmitidos(

            int? TipoAcaoCobranca,
            int? CodTipoCobranca,
            int? CodAcaoCobranca,
            short? SituacaoTipo
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<CobrancaAcaoBoletoDados>(@"EXECUTE [dbo].[SP_CONTRATO_ACAO_COBRANCA_HISTORICO_DADOS_EMITIDOS]
                @TipoAcaoCobranca
                ,@CodTipoCobranca
                ,@CodAcaoCobranca
                ,@SituacaoTipo",
                new SqlParameter("@TipoAcaoCobranca", DbRotinas.ConverterDbValue(TipoAcaoCobranca))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodAcaoCobranca", DbRotinas.ConverterDbValue(CodAcaoCobranca))
                , new SqlParameter("@SituacaoTipo", DbRotinas.ConverterDbValue(SituacaoTipo))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ContratoDados> ListarContratosParcelaRemessa(

            int? TipoAcaoCobranca,
            int? CodTipoCobranca,
            int? CodAcaoCobranca,
            short? SituacaoTipo
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ContratoDados>(@"EXECUTE [dbo].[SP_CONTRATO_ACAO_COBRANCA_HISTORICO_PARCELAS]
                @TipoAcaoCobranca
                ,@CodTipoCobranca
                ,@CodAcaoCobranca
                ,@SituacaoTipo",
                new SqlParameter("@TipoAcaoCobranca", DbRotinas.ConverterDbValue(TipoAcaoCobranca))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodAcaoCobranca", DbRotinas.ConverterDbValue(CodAcaoCobranca))
                , new SqlParameter("@SituacaoTipo", DbRotinas.ConverterDbValue(SituacaoTipo))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<RelatorioClienteDados> ListarRelatorioCliente(
           int? CodConstrutora,
           int? CodRegional,
           int? CodEscritorio,
           int? CodProduto
           )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<RelatorioClienteDados>(@"EXECUTE [dbo].[SP_RELATORIO_CLIENTES] 
                @CodConstrutora
                ,@CodRegional
                ,@CodEscritorio
                ,@CodProduto",
                new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ExportarEmail> ListarExportarEmail(

            int? TipoAcaoCobranca,
            int? CodTipoCobranca,
            int? CodAcaoCobranca
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ExportarEmail>(@"EXECUTE [dbo].[SP_EXPORTAR_EMAIL] 
               @TipoAcaoCobranca
                ,@CodTipoCobranca
                ,@CodAcaoCobranca",
                new SqlParameter("@TipoAcaoCobranca", DbRotinas.ConverterDbValue(TipoAcaoCobranca))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodAcaoCobranca", DbRotinas.ConverterDbValue(CodAcaoCobranca))
                );
            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ExportarCarta> ListarExportarCarta(

            int? TipoAcaoCobranca,
            int? CodTipoCobranca,
            int? CodAcaoCobranca
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ExportarCarta>(@"EXECUTE [dbo].[SP_EXPORTAR_CARTA] 
               @TipoAcaoCobranca
                ,@CodTipoCobranca
                ,@CodAcaoCobranca",
                new SqlParameter("@TipoAcaoCobranca", DbRotinas.ConverterDbValue(TipoAcaoCobranca))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodAcaoCobranca", DbRotinas.ConverterDbValue(CodAcaoCobranca))
                );
            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ExportarSms> ListarExportarSms(
            string CodigoVenda,
            string CodigoCliente,
            string NomeComprador,
            string CPF_CNPJ,
            int? CodConstrutora,
            int? CodDonoCarteira,
            int? CodRegional,
            int? CodTipoCobranca,
            int? CodTipoCarteira,
            int? CodGestao,
            int? CodEscritorio,
            int? CodEmpreendimento,
            int? CodBloco,
            int? CodUnidade,
            bool? Bloqueado,
            string CodAgingCliente,
            string AndamentoDaObra
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ExportarSms>(@"EXECUTE [dbo].[SP_EXPORTAR_SMS] 
                @CodigoVenda
                ,@CodigoCliente
                ,@NomeComprador
                ,@CPF_CNPJ
                ,@CodConstrutora
                ,@CodDonoCarteira
                ,@CodRegional
                ,@CodTipoCobranca
                ,@CodTipoCarteira
                ,@CodGestao
                ,@CodEscritorio
                ,@CodEmpreendimento
                ,@CodBloco
                ,@CodUnidade
                ,@Bloqueado
                ,@CodAgingCliente
                ,@AndamentoDaObra",
                new SqlParameter("@CodigoVenda", DbRotinas.ConverterDbValue(CodigoVenda))
                , new SqlParameter("@CodigoCliente", DbRotinas.ConverterDbValue(CodigoCliente))
                , new SqlParameter("@NomeComprador", DbRotinas.ConverterDbValue(NomeComprador))
                , new SqlParameter("@CPF_CNPJ", DbRotinas.ConverterDbValue(CPF_CNPJ))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutora))
                , new SqlParameter("@CodDonoCarteira", DbRotinas.ConverterDbValue(CodDonoCarteira))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodTipoCarteira", DbRotinas.ConverterDbValue(CodTipoCarteira))
                , new SqlParameter("@CodGestao", DbRotinas.ConverterDbValue(CodGestao))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodEmpreendimento", DbRotinas.ConverterDbValue(CodEmpreendimento))
                , new SqlParameter("@CodBloco", DbRotinas.ConverterDbValue(CodBloco))
                , new SqlParameter("@CodUnidade", DbRotinas.ConverterDbValue(CodUnidade))
                , new SqlParameter("@Bloqueado", DBNull.Value)
                , new SqlParameter("@CodAgingCliente", DbRotinas.ConverterDbValue(CodAgingCliente))
                , new SqlParameter("@AndamentoDaObra", DBNull.Value)
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ExportarSms> ListarExportarSmsTxt(
            string CodigoVenda,
            int? CodigoCliente,
            string NomeComprador,
            string CPF_CNPJ,
            int? CodTipoCobranca,
            int? CodEscritorio,
            int? CodEmpreendimento,
            int? CodBloco,
            int? CodUnidade,
            int? CodAgingCliente
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ExportarSms>(@"EXECUTE [dbo].[SP_EXPORTAR_SMS_TXT] 
                @CodigoVenda
                ,@CodigoCliente
                ,@NomeComprador
                ,@CPF_CNPJ
                ,@CodTipoCobranca
                ,@CodEscritorio
                ,@CodEmpreendimento
                ,@CodBloco
                ,@CodUnidade
                ,@CodAgingCliente",
                new SqlParameter("@CodigoVenda", DbRotinas.ConverterDbValue(CodigoVenda))
                , new SqlParameter("@CodigoCliente", DbRotinas.ConverterDbValue(CodigoCliente))
                , new SqlParameter("@NomeComprador", DbRotinas.ConverterDbValue(NomeComprador))
                , new SqlParameter("@CPF_CNPJ", DbRotinas.ConverterDbValue(CPF_CNPJ))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodEmpreendimento", DbRotinas.ConverterDbValue(CodEmpreendimento))
                , new SqlParameter("@CodBloco", DbRotinas.ConverterDbValue(CodBloco))
                , new SqlParameter("@CodUnidade", DbRotinas.ConverterDbValue(CodUnidade))
                , new SqlParameter("@Bloqueado", DBNull.Value)
                , new SqlParameter("@CodAgingCliente", DbRotinas.ConverterDbValue(CodAgingCliente))
                );

            return dados.ToList();
        }

        public List<ContatoDados> ListarContatosSms(int TipoContato, int? CodTipoCobranca)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ContatoDados>(@"EXECUTE [dbo].[SP_LISTAR_CONTATOS_POR_TIPO_COBRANCA] 
                @CodTipoContato 
                ,@CodTipoCobranca",
                new SqlParameter("@CodTipoContato", DbRotinas.ConverterDbValue(TipoContato))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                );

            return dados.ToList();
        }
        public List<PerguntaAnaliseDocumental> ListarPerguntas(int? CodDocumento, int? CodTipoDocumento)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();
            var dados = Context.Database.SqlQuery<PerguntaAnaliseDocumental>(@"EXECUTE [dbo].[SP_LISTAR_PERGUNTAS] 
                @CodDocumento,
                @CodTipoDocumento",
                new SqlParameter("@CodDocumento", DbRotinas.ConverterDbValue(CodDocumento))
                , new SqlParameter("@CodTipoDocumento", DbRotinas.ConverterDbValue(CodTipoDocumento)));
            return dados.ToList();
        }
        public List<ExportarSerasa> ListarExportarSerasa(

            int? TipoAcaoCobranca,
            int? CodTipoCobranca,
            int? CodAcaoCobranca
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ExportarSerasa>(@"EXECUTE [dbo].[SP_EXPORTAR_SERASA_TXT] 
                 @TipoAcaoCobranca
                ,@CodTipoCobranca
                ,@CodAcaoCobranca",
                new SqlParameter("@TipoAcaoCobranca", DbRotinas.ConverterDbValue(TipoAcaoCobranca))
                , new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodAcaoCobranca", DbRotinas.ConverterDbValue(CodAcaoCobranca))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<HomeDados> FiltrarRelatorioAging(

            int? CodTipoCobranca,
            string CodConstrutoras,
            int? CodEmpreendimento,
            string CodRegional,
            string CodProduto,
            int? CodBloco,
            int? CodEscritorio,
            int? CodAging
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<HomeDados>(@"EXECUTE [dbo].[SP_GRAFICO_AGING] 
                @CodTipoCobranca
                ,@CodConstrutora
                ,@CodEmpreendimento
                ,@CodRegional
                ,@CodProduto
                ,@CodBloco
                ,@CodEscritorio
                ,@CodAging",
                new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodConstrutora", DbRotinas.ConverterDbValue(CodConstrutoras))
                , new SqlParameter("@CodEmpreendimento", DbRotinas.ConverterDbValue(CodEmpreendimento))
                , new SqlParameter("@CodRegional", DbRotinas.ConverterDbValue(CodRegional))
                , new SqlParameter("@CodProduto", DbRotinas.ConverterDbValue(CodProduto))
                , new SqlParameter("@CodBloco", DbRotinas.ConverterDbValue(CodBloco))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodAging", DbRotinas.ConverterDbValue(CodAging))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<HomeDados> FiltrarRelatorioFase(

            int? CodTipoCobranca,
            int? CodEmpreendimento,
            int? CodBloco,
            int? CodEscritorio,
            int? CodAging
            )
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<HomeDados>(@"EXECUTE [dbo].[SP_GRAFICO_AGING] 
                @CodTipoCobranca
                ,@CodEmpreendimento
                ,@CodBloco
                ,@CodEscritorio
                ,@CodAging",
                new SqlParameter("@CodTipoCobranca", DbRotinas.ConverterDbValue(CodTipoCobranca))
                , new SqlParameter("@CodEmpreendimento", DbRotinas.ConverterDbValue(CodEmpreendimento))
                , new SqlParameter("@CodBloco", DbRotinas.ConverterDbValue(CodBloco))
                , new SqlParameter("@CodEscritorio", DbRotinas.ConverterDbValue(CodEscritorio))
                , new SqlParameter("@CodAging", DbRotinas.ConverterDbValue(CodAging))
                );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public List<ClienteParcelaCredito> ListarCreditoDados()
        {
            var dados = Context.Database.SqlQuery<ClienteParcelaCredito>(@"EXECUTE [dbo].[SP_CLIENTE_PARCELA_CREDITO]");
            return dados.ToList();
        }

        public List<DistribuicaoResultado> DistribuicaoExecutarQuery(int Cod, string Query)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();
            parametros.Add(new System.Data.SqlClient.SqlParameter("@Cod", Cod));
            parametros.Add(new System.Data.SqlClient.SqlParameter("@Query", Query));

            var dados = Context.Database.SqlQuery<DistribuicaoResultado>("SP_DISTRIBUICAO_CONSULTAR @Cod, @Query",
                new SqlParameter("@Cod", Cod)
                , new SqlParameter("@Query", Query)
                );

            return dados.ToList();
        }
        public List<CasosAtendimento> ConsultaCasosAtendimento(int usuario, int CodRegional, int CodCliente = 0)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();
            Context.Database.CommandTimeout = 120;
            var dados = Context.Database.SqlQuery<CasosAtendimento>("SP_CONSULTA_TELA_ATENDIMENTO @CodCliente, @CodUsuario, @CodRegional",
                new SqlParameter("@CodCliente", CodCliente),
                new SqlParameter("@CodUsuario", usuario),
                new SqlParameter("@CodRegional", CodRegional)
                );

            return dados.ToList();
        }
        public List<CasosAtendimento> ConsultaCasosAtendimentoPorCliente(int usuario, int CodCliente = 0)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<CasosAtendimento>("SP_CONSULTA_TELA_ATENDIMENTO_POR_CLIENTE @CodCliente, @CodUsuario",
                new SqlParameter("@CodCliente", CodCliente),
                new SqlParameter("@CodUsuario", usuario)
                );

            return dados.ToList();
        }
        public List<ScriptAtendimento> ConsultaScript(int CodContrato = 0)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<ScriptAtendimento>(@"EXECUTE [dbo].[SP_CONTRATO_SCRIPT] @CODCONTRATO", new SqlParameter("@CODCONTRATO", CodContrato));

            return dados.ToList();
        }
        public void ComputaValoresCliente(int CodCliente = 0)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            Context.Database.SqlQuery<ScriptAtendimento>(@"EXECUTE [dbo].[SP_COMPUTAR_VALORES_DIARIOS_POR_CLIENTE] @CodCliente", new SqlParameter("@CodCliente", CodCliente));
        }

        public List<DistribuicaoResultado> DistribuicaoProcessarPorEscritorio(int Cod, string Query)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<DistribuicaoResultado>("SP_DISTRIBUICAO_EXECUTAR_POR_ESCRITORIO @Cod, @Query",
                new SqlParameter("@Cod", Cod)
                , new SqlParameter("@Query", Query)
                );

            return dados.ToList();
        }
        public void DistribuicaoPrepararContratos(int CodUsuario)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            Context.Database.ExecuteSqlCommand("SP_DISTRIBUICAO_PREPARAR_BASE @CodUsuario",
                new SqlParameter("@CodUsuario", CodUsuario)
            );
        }
        public List<AgendaOperador> ObterAgendaPorOperador(int? UsuarioId)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<AgendaOperador>(@"EXECUTE [dbo].[SP_OBTER_AGENDA_OPERADOR] 
                @UsuarioId"
                , new SqlParameter("@UsuarioId", DbRotinas.ConverterDbValue(UsuarioId))
                );
            return dados.ToList();
        }

        public List<Historico> ObterHistoricoPorCliente(int? CodCliente, int? CodSintese)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            var dados = Context.Database.SqlQuery<Historico>(@"EXECUTE [dbo].[SP_CONTRATO_HISTORICO_POR_CLIENTE] 
                @CodCliente,
                @CodSintese"
                , new SqlParameter("@CodCliente", DbRotinas.ConverterDbValue(CodCliente))
                , new SqlParameter("@CodSintese", DbRotinas.ConverterDbValue(CodSintese))
                );
            return dados.ToList();
        }
        public List<ContratoParcela> ObterParcelaPorCliente(int? CodCliente, int? CodSinteseContrato, int? CodSinteseParcela)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            var dados = Context.Database.SqlQuery<ContratoParcela>(@"EXECUTE [dbo].[SP_CONTRATO_PARCELA_POR_CLIENTE] 
                @CodCliente,
                @CodSinteseContrato,
                @CodSinteseParcela"
                , new SqlParameter("@CodCliente", DbRotinas.ConverterDbValue(CodCliente))
                , new SqlParameter("@CodSinteseContrato", DbRotinas.ConverterDbValue(CodSinteseContrato))
                , new SqlParameter("@CodSinteseParcela", DbRotinas.ConverterDbValue(CodSinteseParcela))
                );
            return dados.ToList();
        }

        public List<ContratoParcelaDados> ListarParcelasPorContrato(string CodContrato, int? CodSinteseContrato, int? CodSinteseParcela)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            var dados = Context.Database.SqlQuery<ContratoParcelaDados>(@"EXECUTE [dbo].[SP_CONTRATO_PARCELA_POR_CONTRATO] 
                @CodContrato, 
                @CodSinteseContrato,
                @CodSinteseParcela"
                , new SqlParameter("@CodContrato", DbRotinas.ConverterDbValue(CodContrato))
                , new SqlParameter("@CodSinteseContrato", DbRotinas.ConverterDbValue(CodSinteseContrato))
                , new SqlParameter("@CodSinteseParcela", DbRotinas.ConverterDbValue(CodSinteseParcela))
                );
            return dados.ToList();
        }

        public List<HeaderContratoParcelaBoletoDados> HeaderContratoParcelaBoleto(int? CodBoleto)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            var dados = Context.Database.SqlQuery<HeaderContratoParcelaBoletoDados>(@"EXECUTE [dbo].[SP_CONTRATO_PARCELA_BOLETO_HEADER_DADOS] 
                @CodBoleto", new SqlParameter("@CodBoleto", DbRotinas.ConverterDbValue(CodBoleto))
                );
            return dados.ToList();
        }

        public List<ParcelaAcordo> ObterParcelaAcordoPorCliente(int? CodCliente)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            var dados = Context.Database.SqlQuery<ParcelaAcordo>(@"EXECUTE [dbo].[SP_PARCELA_ACORDO_POR_CLIENTE] 
                @CodCliente"
                , new SqlParameter("@CodCliente", DbRotinas.ConverterDbValue(CodCliente))
                );
            return dados.ToList();
        }

        public void executarImportacaoContrato(string Filial, string Produto,
            string NumeroContrato, string NumeroCliente, string NomeCliente, string CPF,
            string NumeroFatura, decimal? ValorFatura, decimal? ValorAberto,
            DateTime? DataVencimento, int? NumeroParcela, decimal? ValorPago, DateTime? DataPagamento, int? Status,
            string Email, string TelResidencial, string TelComercial, string TelCelular, string CEP, string Endereco, string Bairro, string UF, string Cidade)
        {
            Context.Database.CommandTimeout = 120;
            var dados = Context.Database.ExecuteSqlCommand("SP_IMPORTACAO_CONTRATO @NUMEROCONTRATO, @NUMEROFATURA, @CODFILIAL, @PRODUTO, @DESCRICAOCLIENTE, @CPF, @NUMEROCLIENTE, @VALORFATURA, @VALORABERTO, @DATAVENCIMENTO, @DATAPAGAMENTO, @STATUS, @EMAIL, @TELEFONECOMERCIAL, @TELEFONERESIDENCIAL, @TELEFONECELULAR, @CEP, @ENDERECO, @BAIRRO, @UF, @CIDADE",
                  new SqlParameter("@NUMEROCONTRATO", DbRotinas.ConverterDbValue(NumeroContrato))
                , new SqlParameter("@NUMEROFATURA", DbRotinas.ConverterDbValue(NumeroFatura))
                , new SqlParameter("@CODFILIAL", DbRotinas.ConverterDbValue(Filial))
                , new SqlParameter("@PRODUTO", DbRotinas.ConverterDbValue(Produto))
                , new SqlParameter("@DESCRICAOCLIENTE", DbRotinas.ConverterDbValue(NomeCliente))
                , new SqlParameter("@CPF", DbRotinas.ConverterDbValue(CPF))
                , new SqlParameter("@NUMEROCLIENTE", DbRotinas.ConverterDbValue(NumeroCliente))
                , new SqlParameter("@VALORFATURA", DbRotinas.ConverterDbValue(ValorFatura))
                , new SqlParameter("@VALORABERTO", DbRotinas.ConverterDbValue(ValorAberto))
                , new SqlParameter("@DATAVENCIMENTO", DbRotinas.ConverterDbValue(DataVencimento))
                , new SqlParameter("@DATAPAGAMENTO", DbRotinas.ConverterDbValue(DataPagamento))
                , new SqlParameter("@STATUS", DbRotinas.ConverterDbValue(Status))
                , new SqlParameter("@EMAIL", DbRotinas.ConverterDbValue(Email))
                , new SqlParameter("@TELEFONECOMERCIAL", DbRotinas.ConverterDbValue(TelComercial))
                , new SqlParameter("@TELEFONERESIDENCIAL", DbRotinas.ConverterDbValue(TelResidencial))
                , new SqlParameter("@TELEFONECELULAR", DbRotinas.ConverterDbValue(TelCelular))
                , new SqlParameter("@CEP", DbRotinas.ConverterDbValue(CEP))
                , new SqlParameter("@ENDERECO", DbRotinas.ConverterDbValue(Endereco))
                , new SqlParameter("@BAIRRO", DbRotinas.ConverterDbValue(Bairro))
                , new SqlParameter("@UF", DbRotinas.ConverterDbValue(UF))
                , new SqlParameter("@CIDADE", DbRotinas.ConverterDbValue(Cidade))
               );
        }

        public void executarImportacaoCliente(string NumeroCliente, string DescricaoCliente, string CPF,
            string Endereco, string Numero, string Complemento, string Bairro, string CEP, string Cidade, string Estado, string TP, string DDD, string NumeroTelefone, string Email)
        {
            Context.Database.CommandTimeout = 120;
            var dados = Context.Database.ExecuteSqlCommand("SP_IMPORTACAO_CONTRATO @NUMEROCLIENTE, @DESCRICAOCLIENTE, @CPF, @ENDERECO, @NUMERO, @COMPLEMENTO, @BAIRRO, @CEP, @CIDADE, @ESTADO, @TP, @DDD, @NUMEROTELEFONE, @EMAIL",
                  new SqlParameter("@NUMEROCLIENTE", DbRotinas.ConverterDbValue(NumeroCliente))
                , new SqlParameter("@DESCRICAOCLIENTE", DbRotinas.ConverterDbValue(DescricaoCliente))
                , new SqlParameter("@CPF", DbRotinas.ConverterDbValue(CPF))
                , new SqlParameter("@ENDERECO", DbRotinas.ConverterDbValue(Endereco))
                , new SqlParameter("@NUMERO", DbRotinas.ConverterDbValue(Numero))
                , new SqlParameter("@COMPLEMENTO", DbRotinas.ConverterDbValue(Complemento))
                , new SqlParameter("@BAIRRO", DbRotinas.ConverterDbValue(NumeroCliente))
                , new SqlParameter("@CEP", DbRotinas.ConverterDbValue(CEP))
                , new SqlParameter("@CIDADE", DbRotinas.ConverterDbValue(Cidade))
                , new SqlParameter("@ESTADO", DbRotinas.ConverterDbValue(Estado))
                , new SqlParameter("@TP", DbRotinas.ConverterDbValue(TP))
                , new SqlParameter("@DDD", DbRotinas.ConverterDbValue(DDD))
                , new SqlParameter("@NUMEROTELEFONE", DbRotinas.ConverterDbValue(NumeroTelefone))
                , new SqlParameter("@EMAIL", DbRotinas.ConverterDbValue(Email))
               );
        }
        public List<QtdeImportacao> QtdeImportacao()
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            var dados = Context.Database.SqlQuery<QtdeImportacao>(@"EXECUTE [dbo].[SP_QUANTIDADE_IMPORTACAO]");
            return dados.ToList();
        }

        public List<HistoricoDados> ListarHistoricoCliente(int ClienteId)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            var dados = Context.Database.SqlQuery<HistoricoDados>(@"EXECUTE [dbo].[SP_DADOS_HISTORICO_CLIENTE]
            @CodCliente"
            , new SqlParameter("@CodCliente", DbRotinas.ConverterDbValue(ClienteId))
            );


            //int total = dados.Count();
            //return dados.Skip(inicio).Take(tamanho).ToList();
            return dados.ToList();
        }

        public OperacaoDbRetorno ProcessarCliente(int codImportacao)
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            junix_cobrancaContext Context = new junix_cobrancaContext();

            int totalResitrosPendentes = 0;
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CLIENTE_PROCESSAR_PAGINADO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));

                        while (totalResitrosPendentes > 0 || !op.OK)
                        {
                            try
                            {
                                totalResitrosPendentes = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());
                                if (totalResitrosPendentes == 0)
                                {
                                    op.OK = true;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                op = new OperacaoDbRetorno(ex);
                            }

                        }

                    }
                }
                catch (Exception ex)
                {
                    op = new OperacaoDbRetorno(ex);
                }
                finally
                {
                    connection.Close();

                }

                return op;
            }
        }
        public OperacaoDbRetorno ProcessarValoresDiarios()
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = true };
            junix_cobrancaContext Context = new junix_cobrancaContext();

            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_COMPUTAR_VALORES_DIARIOS", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.ExecuteScalar();
                    }
                }
                catch (Exception ex)
                {
                    op = new OperacaoDbRetorno(ex);
                }
                finally
                {
                    connection.Close();

                }

                return op;
            }
        }
        public OperacaoDbRetorno ProcessarClienteEndereco(int codImportacao)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            int totalResitrosPendentes = 0;

            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_CLIENTE_ENDERECO_PROCESSAR_PAGINADO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        while (totalResitrosPendentes > 0 || !op.OK)
                        {
                            try
                            {
                                totalResitrosPendentes = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());
                                if (totalResitrosPendentes == 0)
                                {
                                    op.OK = true;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                op = new OperacaoDbRetorno(ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
                return op;
            }
        }

        public OperacaoDbRetorno ProcessarParcelaEtapaContrato(int codImportacao)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            int totalResitrosPendentes = 0;
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_PARCELA_PROCESSAR_PAGINADO_ETAPA_CONTRATO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        while (totalResitrosPendentes > 0 || !op.OK)
                        {
                            try
                            {
                                totalResitrosPendentes = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());

                                op.OK = true;
                                break;
                            }
                            catch (Exception ex)
                            {
                                op = new OperacaoDbRetorno(ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
                return op;
            }
        }
        public OperacaoDbRetorno ProcessarParcelaEtapaProcessarParcela(int codImportacao)
        {
            LogServico _log = new LogServico();
            junix_cobrancaContext Context = new junix_cobrancaContext();
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            int totalResitrosPendentes = 0;
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_PARCELA_PROCESSAR_PAGINADO_ETAPA_PARCELA", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        while (totalResitrosPendentes > 0 || !op.OK)
                        {
                            try
                            {
                                totalResitrosPendentes = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());
                                if (totalResitrosPendentes == 0)
                                {
                                    op.OK = true;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {

                                op = new OperacaoDbRetorno(ex);
                                op.Mensagem = $"mensagem: {ex.Message} - InnerException: {ex.InnerException}";
                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
                return op;
            }
        }
        public OperacaoDbRetorno ProcessarParcelaEtapaHistorico(int codImportacao)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            int totalHistoricoPendente = 0;
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_PARCELA_COMPUTAR_DADOS_HISTORICO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodigoImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        totalHistoricoPendente = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());

                        if (totalHistoricoPendente == 0)
                        {
                            op.OK = true;
                        }
                        else
                        {
                            op.Mensagem = $"{totalHistoricoPendente} parcelas com historicos não inseridos";
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
                return op;
            }
        }
        public OperacaoDbRetorno ProcessarParcelaEtapaComputarInformacoes(int codImportacao)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_PARCELA_COMPUTAR_DADOS", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodigoImportacao", DbRotinas.ConverterDbValue(codImportacao));
                        sqlCmd.ExecuteScalar();
                        op.OK = true;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Erro ao computar as informações{ex.Message}");
                }
                finally
                {
                    connection.Close();
                }
                return op;
            }
        }
        public OperacaoDbRetorno ProcessarCredito(int codImportacao)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            int totalResitrosPendentes = 0;
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("[SP_IMPORTACAO_CREDITO_PROCESSAR_PAGINADO]", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));

                        while (totalResitrosPendentes > 0 || !op.OK)
                        {
                            try
                            {
                                totalResitrosPendentes = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());
                                if (totalResitrosPendentes == 0)
                                {
                                    op.OK = true;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                op = new OperacaoDbRetorno(ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
                return op;
            }
        }

        public OperacaoDbRetorno ProcessarBoleto(int codImportacao)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            int totalResitrosPendentes = 0;
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_IMPORTACAO_BOLETO_PROCESSAR_PAGINADO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodImportacao", DbRotinas.ConverterDbValue(codImportacao));

                        while (totalResitrosPendentes > 0 || !op.OK)
                        {
                            try
                            {
                                totalResitrosPendentes = DbRotinas.ConverterParaInt(sqlCmd.ExecuteScalar());
                                if (totalResitrosPendentes == 0)
                                {
                                    op.OK = true;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {
                                op = new OperacaoDbRetorno(ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
                return op;
            }
        }

        public List<RelatorioAtendimentoDados> RelatorioAtendimentoAgendados(string atendentes, string codCliente, string cnpj, string numeroContrato, DateTime? de, DateTime? ate)
        {
            junix_cobrancaContext Context = new junix_cobrancaContext();
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            Context.Database.CommandTimeout = 120;
            var dados = Context.Database.SqlQuery<RelatorioAtendimentoDados>("SP_RELATORIO_ATENDIMENTO_AGENDADOS @UsuarioId, @CodCliente, @Cnpj, @NumeroContrato, @PeriodoDe, @PeriodoAte",
                  new SqlParameter("@UsuarioId", DbRotinas.ConverterDbValue(atendentes))
                , new SqlParameter("@CodCliente", DbRotinas.ConverterDbValue(codCliente))
                , new SqlParameter("@Cnpj", DbRotinas.ConverterDbValue(cnpj))
                , new SqlParameter("@NumeroContrato", DbRotinas.ConverterDbValue(numeroContrato))
                , new SqlParameter("@PeriodoDe", DbRotinas.ConverterDbValue(de))
                , new SqlParameter("@PeriodoAte", DbRotinas.ConverterDbValue(ate))
               );
            return dados.ToList();
        }

        public OperacaoDbRetorno ComputaHistoricoBoleto(int CodBoleto)
        {

            junix_cobrancaContext Context = new junix_cobrancaContext();
            OperacaoDbRetorno op = new OperacaoDbRetorno() { OK = false };
            using (var connection = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                connection.Open();
                try
                {
                    using (var sqlCmd = new SqlCommand("SP_COMPUTA_HISTORICO_BOLETO", connection))
                    {
                        sqlCmd.CommandTimeout = 3600;
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@CodBoleto", DbRotinas.ConverterDbValue(CodBoleto));

                        try
                        {
                            sqlCmd.ExecuteScalar();
                            op.OK = true;
                        }
                        catch (Exception ex)
                        {
                            op = new OperacaoDbRetorno(ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
                return op;
            }
        }


        public List<RelatorioAtendimentoDados> ListarAtendimentos()
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();
            var dados = Context.Database.SqlQuery<RelatorioAtendimentoDados>(@"EXECUTE [dbo].[SP_RELATORIO_ATENDIMENTO_AGENDADOS]");
            return dados.ToList();
        }

    }
}
