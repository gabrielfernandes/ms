﻿using Cobranca.Db.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{


    public class ContratoDb
    {
        Repository<Contrato> _dbContrato = new Repository<Contrato>();
        Repository<ContratoCliente> _dbContratoCliente = new Repository<ContratoCliente>();
        Repository<AcordoContratoParcela> _dbAcordo = new Repository<AcordoContratoParcela>();

        Repository<ContratoParcela> _dbContratoParcela = new Repository<ContratoParcela>();

        public List<ContratoParcela> ListarParcela(List<int> ContratosIds)
        {
            List<ContratoParcela> parcelas = _dbContratoParcela.FindAll(p => ContratosIds.Contains((int)p.CodContrato)).ToList();
            return parcelas;
        }

        public List<ContratoParcela> ListarParcelaEmAtraso(List<int> ContratosIds)
        {
            //OBTEM TODOS OS ACORDOS
            var acordos = _dbAcordo.FindAll(a => !a.DataExcluido.HasValue).ToList();
            List<int> acordosIds = new List<int>(acordos.Select(c => (int)c.CodContratoParcela));
            //OBTER TODAS PARCELAS PELO CONTRATO
            List<ContratoParcela> parcelas = _dbContratoParcela.FindAll(p => ContratosIds.Contains((int)p.CodContrato)
            && !p.DataExcluido.HasValue
            && !acordosIds.Contains(p.Cod)
            && !p.DataPagamento.HasValue
            && DbFunctions.TruncateTime(p.DataVencimento) < DbFunctions.TruncateTime(DateTime.Now)).ToList();
            return parcelas;
        }

        public List<ContratoParcela> ListarParcelaAVencer(List<int> ContratosIds)
        {
            //OBTEM TODOS OS ACORDOS
            var acordos = _dbAcordo.FindAll(a => !a.DataExcluido.HasValue).ToList();
            List<int> acordosIds = new List<int>(acordos.Select(c => (int)c.CodContratoParcela));
            //OBTER TODAS PARCELAS PELO CONTRATO
            List<ContratoParcela> parcelas = _dbContratoParcela.FindAll(p => ContratosIds.Contains((int)p.CodContrato)
            && !p.DataExcluido.HasValue
            && !acordosIds.Contains(p.Cod)
            && !p.DataPagamento.HasValue
            && DbFunctions.TruncateTime(p.DataVencimento) >= DbFunctions.TruncateTime(DateTime.Now)).ToList();
            return parcelas;
        }

        public List<Contrato> ListarContratoEmAtraso(int ClienteId)
        {
            //Obter todos os Contratos do Cliente
            var CC = _dbContratoCliente.FindAll(x => x.CodCliente == ClienteId);
            var contratos = from p in CC.ToList() select new { p.Contrato.Cod, p.Contrato.DataExcluido, p.Contrato.NumeroContrato };
            List<int> ContratosIds = new List<int>(contratos.Select(c => c.Cod));
            //Pegar todos acordos
            var acordos = _dbAcordo.FindAll(a => !a.DataExcluido.HasValue).ToList();
            //Obter as Parcelas Pelos ContratosIds
            List<ContratoParcela> parcelas = _dbContratoParcela.FindAll(p => ContratosIds.Contains((int)p.CodContrato)
            && !p.DataExcluido.HasValue
            && !acordos.Exists(a => a.CodContratoParcela == p.Cod)
            && !p.DataPagamento.HasValue
            && DbFunctions.TruncateTime(p.DataVencimento) < DbFunctions.TruncateTime(DateTime.Now)).ToList();
            List<int> parcelaIds = new List<int>(parcelas.Select(c => c.Cod));

            //Obter Todos os Contratos Em Atraso Pela lista de Parcelas Em Atraso
            List<int> ContratosIdsEmAtraso = new List<int>((from c in contratos.Where(x => parcelaIds.Contains(x.Cod)).ToList() select new { c.Cod }).Select(c => c.Cod));
            List<Contrato> listaContratosEmAtraso = _dbContrato.FindAll(c => ContratosIdsEmAtraso.Contains((int)c.Cod)).ToList();
            return listaContratosEmAtraso;
        }

    }
}
