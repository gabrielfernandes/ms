﻿using Cobranca.Db.Models;
using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    public class FaseSinteseDb
    {

        public Repository<TabelaSintese>
            _dbTabelaSintese = new Repository<TabelaSintese>();

        public List<TabelaSintese> ListarHistorico(Enumeradores.TipoHistorico tipoHistorico)
        {
            var all = _dbTabelaSintese.All().Where(x =>
           x.TabelaFase.TipoHistorico == (short)tipoHistorico
           && !x.TabelaFase.DataExcluido.HasValue
           && !x.TabelaFase.TabelaTipoCobranca.DataExcluido.HasValue).ToList();


            List<int> fasesDescriptografadas = new List<int>(); ;



            foreach (var item in all)
            {
                if (!fasesDescriptografadas.Contains(item.CodFase))
                {
                    fasesDescriptografadas.Add(item.CodFase);
                    item.TabelaFase.Fase = DbRotinas.Descriptografar(item.TabelaFase.Fase);
                }

                item.Sintese = DbRotinas.Descriptografar(item.Sintese);
            }

            return all.ToList();
        }

    }
}
