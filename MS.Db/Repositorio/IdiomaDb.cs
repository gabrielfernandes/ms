﻿using Cobranca.Db.Models;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Db.Rotinas;
using Cobranca.Db.Suporte;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    public class IdiomaDb
    {
        protected junix_cobrancaContext Context;
        public IdiomaDb()
        {
            Context = new junix_cobrancaContext();
        }

        public List<IdiomaInfo> Listagem()
        {
            var dados = Context.Database.SqlQuery<IdiomaInfo>(@"SELECT * FROM TabelaIdioma WITH(NOLOCK)");
            return dados.ToList();
        }

        public void SalvarTraducao(IdiomaInfo idiomaInfo)
        {
            using (SqlConnection conn = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SP_IDIOMA_SALVAR", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                conn.Open();

                try
                {
                    cmd.Parameters.AddWithValue("@Cod", DbRotinas.ConverterDbValue(idiomaInfo.Cod));
                    cmd.Parameters.AddWithValue("@esES", DbRotinas.ConverterDbValue(idiomaInfo.esES));
                    cmd.Parameters.AddWithValue("@enUS", DbRotinas.ConverterDbValue(idiomaInfo.enUS));
                    cmd.Parameters.AddWithValue("@ptBR", DbRotinas.ConverterDbValue(idiomaInfo.ptBR));

                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw;   
                }
                finally
                {
                    conn.Close();
                }
            }

        }
        public void CriarTraducao(IdiomaInfo idiomaInfo)
        {
            using (SqlConnection conn = new SqlConnection(Context.Database.Connection.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SP_IDIOMA_SALVAR", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                conn.Open();

                try
                {
                    cmd.Parameters.AddWithValue("@Cod", DbRotinas.ConverterDbValue(idiomaInfo.Cod));
                    cmd.Parameters.AddWithValue("@esES", DbRotinas.ConverterDbValue(idiomaInfo.esES));
                    cmd.Parameters.AddWithValue("@enUS", DbRotinas.ConverterDbValue(idiomaInfo.enUS));
                    cmd.Parameters.AddWithValue("@ptBR", DbRotinas.ConverterDbValue(idiomaInfo.ptBR));

                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }

        }

        public OperacaoDbRetorno AlterarIdioma(string name, string pk, string value)
        {
            OperacaoDbRetorno op = new OperacaoDbRetorno();

            using (SqlConnection conn = new SqlConnection(Context.Database.Connection.ConnectionString))
            {

                string query = $"UPDATE TabelaIdioma SET {name} = '{value}' WHERE Cod = '{pk}' ";

                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.CommandType = System.Data.CommandType.Text;
                conn.Open();

                try
                {   
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    op = new OperacaoDbRetorno(ex);
                }
                finally
                {
                    conn.Close();
                }
            }
            return op;
        }
    }
}
