﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    class Encrypted : Attribute
    {
        [Encrypted] // here setting up custom attribute for encrypted Property 
        public string EncryptedProperty { get; set; }
    }
}
