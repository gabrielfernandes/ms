﻿using Cobranca.Db.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    public class UnidadeStatusDb
    {
        Repository<TabelaUnidade> _dbUnidade = new Repository<TabelaUnidade>();
        Repository<TabelaUnidadeStatusHistorico> _dbUnidadeHistorico = new Repository<TabelaUnidadeStatusHistorico>();
        Repository<TabelaUnidadeReserva> _dbUnidadeReserva = new Repository<TabelaUnidadeReserva>();

        
        public void ReservarUnidade(TabelaUnidadeReserva reserva, int codUsuario, string Obs)
        {

          AlterarStatusUnidade(Enumeradores.StatusUnidade.Reservada, Convert.ToInt32(reserva.CodUnidade), codUsuario, Obs);
           reserva.DataReserva = System.DateTime.Now;
           reserva.StatusReserva = (short)Enumeradores.StatusUnidade.Reservada;
          _dbUnidadeReserva.CreateOrUpdate(reserva);
          
        }

        public TabelaUnidadeStatusHistorico AlterarStatusUnidade(Enumeradores.StatusUnidade status, int CodUnidade, int CodUsuario, string Obs)
        {
            var unidade = _dbUnidade.FindById(CodUnidade);
            unidade.Status = (short)status;
            
            //dados do histórico
            TabelaUnidadeStatusHistorico historico = new TabelaUnidadeStatusHistorico();
            historico.Status = (short)status;
            historico.CodUnidade = CodUnidade;
            historico.CodUsuario = CodUsuario;
            historico.Observacao = Obs;

            _dbUnidade.CreateOrUpdate(unidade);
            var entity = _dbUnidadeHistorico.CreateOrUpdate(historico);
            return entity;
        }
    }
}
