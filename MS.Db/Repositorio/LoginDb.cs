﻿using Cobranca.Db.Models;
using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    public class LoginDb
    {

        protected junix_cobrancaContext Context;
        public LoginDb()
        {
            Context = new junix_cobrancaContext();
        }



        public Usuario SingIn(string Login,string Senha)
        {
            List<System.Data.SqlClient.SqlParameter> parametros = new List<System.Data.SqlClient.SqlParameter>();

            var dados = Context.Database.SqlQuery<Usuario>(@"EXECUTE [dbo].[SP_USUARIO_AUTENTICAR] 
                @Login
                ,@Senha",
                new SqlParameter("@Login", DbRotinas.ConverterDbValue(Login))
                , new SqlParameter("@Senha", DbRotinas.ConverterDbValue(Senha))
                );
            
            return dados.FirstOrDefault();
        }
    }
}
