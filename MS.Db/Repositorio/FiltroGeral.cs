﻿using Cobranca.Db.Models;
using Cobranca.Db.Models.Pesquisa;
using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    public class FiltroGeral
    {
        Repository<Contrato> _dbContrato = new Repository<Contrato>();
        Repository<ContratoParcela> _dbContratoParcela = new Repository<ContratoParcela>();
        Repository<TabelaRegionalGrupo> _dbRegionalGrupo = new Repository<TabelaRegionalGrupo>();
        Repository<TabelaRegionalProduto> _dbRegionalProduto = new Repository<TabelaRegionalProduto>();

        public FiltroGeralDados TratarFiltroGeral(FiltroGeralDados filtro)
        {
            if (filtro.NumeroFatura != null)
            {
                filtro.CodParcelas = _dbContratoParcela
                    .FindAll(x => x.FluxoPagamento == filtro.NumeroFatura && !x.DataExcluido.HasValue)
                    .Select(x => x.Cod)
                    .ToList();
            }

            if (filtro.NumeroContrato != null)
            {
                filtro.CodContratos = _dbContrato
                     .FindAll(x => x.NumeroContrato == filtro.NumeroContratoEncrypt && !x.DataExcluido.HasValue)
                     .Select(x => x.Cod)
                     .ToList();
            }

            if (filtro.CodRegional > 0)
            {
                var regionaisProduto = _dbRegionalProduto.FindAll(x => x.CodRegional == filtro.CodRegional && !x.DataExcluido.HasValue).Select(x=> x.Cod).ToList();

                filtro.CodContratos = _dbContrato
                     .FindAll(x => regionaisProduto.Contains((int)x.CodRegionalProduto) && !x.DataExcluido.HasValue)
                     .Select(x => x.Cod)
                     .ToList();
            }
            return filtro;
        }

    }
}
