﻿using Cobranca.Db.Models;
using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    public class FilialRepositorio
    {
        Repository<TabelaRegional> _dbRegional;
        Repository<UsuarioRegional> _dbUsuarioRegional;
        Repository<Usuario> _dbUsuario;
        public FilialRepositorio()
        {
            _dbRegional = new Repository<TabelaRegional>();
            _dbUsuarioRegional = new Repository<UsuarioRegional>();
            _dbUsuario = new Repository<Usuario>();
        }


        /// <summary>
        /// RETORNA A LISTA DE REGIONAIS VINCULADAS AO USUARIO
        /// </summary>
        /// <param name="where"></param>
        /// <param name="CodUsuario"></param>
        /// <returns></returns>
        public List<TabelaRegional> ListagemPorUsuario(Expression<Func<TabelaRegional, bool>> where = null, int CodUsuario = 0)
        {
            var usuario = _dbUsuario.FindById(CodUsuario);

            bool admin = usuario.PerfilTipo == (short)Enumeradores.PerfilTipo.Admin;

            IQueryable<TabelaRegional> all;

            if (!admin)
                all = _dbUsuarioRegional.FindAll(x => x.CodUsuario == CodUsuario).Select(x => x.TabelaRegional);
            else
                all = _dbRegional.All();

            if (where != null)
                all = all.Where(where);

            List<TabelaRegional> lista = all.ToList();
            foreach (var item in lista)
            {
                item.Regional = DbRotinas.Descriptografar(item.Regional);
            }

            return lista;
        }

    }
}
