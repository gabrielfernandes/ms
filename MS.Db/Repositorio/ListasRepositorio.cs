﻿using Cobranca.Db.Models;
using Cobranca.Db.Rotinas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Repositorio
{
    public class ListasRepositorio
    {
        public Repository<Contrato> _dbContrato = new Repository<Contrato>();
        public Repository<TabelaConstrutora> _dbConstrutora = new Repository<TabelaConstrutora>();
        public Repository<TabelaEmpreendimento> _dbEmpreendimento = new Repository<TabelaEmpreendimento>();
        public Repository<TabelaBloco> _dbBloco = new Repository<TabelaBloco>();
        public Repository<TabelaUnidade> _dbUnidade = new Repository<TabelaUnidade>();
        public Repository<TabelaRegional> _dbRegional = new Repository<TabelaRegional>();
        public Repository<TabelaTipoCobranca> _dbTipoCobranca = new Repository<TabelaTipoCobranca>();
        public Repository<Usuario> _dbUsuario = new Repository<Usuario>();
        public Repository<TabelaBanco> _dbBanco = new Repository<TabelaBanco>();
        public Repository<TabelaGestao> _dbGestao = new Repository<TabelaGestao>();
        public Repository<TabelaEmpresa> _dbEmpresa = new Repository<TabelaEmpresa>();
        public Repository<TabelaTipoBloqueio> _dbTipoBloqueio = new Repository<TabelaTipoBloqueio>();
        public Repository<TabelaFase> _dbFase = new Repository<TabelaFase>();
        public Repository<TabelaMotivoAtraso> _dbMotivoAtraso = new Repository<TabelaMotivoAtraso>();
        public Repository<TabelaResolucao> _dbResolucao = new Repository<TabelaResolucao>();
        public Repository<TabelaCampanha> _dbCampanha = new Repository<TabelaCampanha>();
        public Repository<TabelaCheckListNome> _dbCheckListNome = new Repository<TabelaCheckListNome>();



        public List<DictionaryEntry> Regional()
        {
            List<TabelaRegional> lista = new List<TabelaRegional>();
            lista = _dbRegional.All().OrderBy(x => x.Regional).ToList();
            lista.Insert(0, new TabelaRegional());
            var list = from p in lista select new DictionaryEntry(p.Regional, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> Construtoras()
        {
            List<TabelaConstrutora> listaContrutora = new List<TabelaConstrutora>();
            listaContrutora = _dbConstrutora.All().ToList();
            foreach (var item in listaContrutora)
            {
                item.NomeConstrutora = DbRotinas.Descriptografar(item.NomeConstrutora);
            }
            listaContrutora.OrderBy(x => x.NomeConstrutora).ToList();
            listaContrutora.Insert(0, new TabelaConstrutora());
            var list = from p in listaContrutora select new DictionaryEntry(p.NomeConstrutora, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> Empreendimentos(int CodRegional = 0)
        {
            List<TabelaEmpreendimento> listaContrutora = new List<TabelaEmpreendimento>();
            listaContrutora = _dbEmpreendimento.All().OrderBy(x => x.NomeEmpreendimento).ToList();
            if (CodRegional > 0)
            {
                listaContrutora = listaContrutora.Where(x => x.CodRegional == CodRegional).ToList();
            }

            listaContrutora.Insert(0, new TabelaEmpreendimento());
            var list = from p in listaContrutora select new DictionaryEntry(p.NomeEmpreendimento, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> Bloco(int CodEmpreendimento = 0)
        {
            List<TabelaBloco> listaContrutora = new List<TabelaBloco>();
            listaContrutora = _dbBloco.All().OrderBy(x => x.NomeBloco).ToList();
            if (CodEmpreendimento > 0)
            {
                listaContrutora.Where(x => x.CodEmpreend == CodEmpreendimento).ToList();
            }
            listaContrutora.Insert(0, new TabelaBloco());
            var list = from p in listaContrutora select new DictionaryEntry(p.NomeBloco, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> TipoCobranca()
        {
            List<TabelaTipoCobranca> listaTipoCobranca = new List<TabelaTipoCobranca>();
            listaTipoCobranca = _dbTipoCobranca.All().Where(x => x.TipoFluxo == (short)Enumeradores.TipoFluxo.Cobranca).ToList();
            foreach (var item in listaTipoCobranca)
            {
                item.TipoCobranca = DbRotinas.Descriptografar(item.TipoCobranca);
            }
            listaTipoCobranca = listaTipoCobranca.OrderBy(x => x.TipoCobranca).ToList();
            listaTipoCobranca.Insert(0, new TabelaTipoCobranca());
            var list = from p in listaTipoCobranca select new DictionaryEntry(p.TipoCobranca, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> Campanha()
        {
            List<TabelaCampanha> listaCampanha = new List<TabelaCampanha>();
            listaCampanha = _dbCampanha.All().OrderBy(x => x.Cod).ToList();
            listaCampanha.Insert(0, new TabelaCampanha());
            var list = from p in listaCampanha select new DictionaryEntry(p.Campanha, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> TipoBloqueio()
        {
            List<TabelaTipoBloqueio> lsita = new List<TabelaTipoBloqueio>();
            lsita = _dbTipoBloqueio.All().OrderBy(x => x.TipoBloqueio).ToList();
            lsita.Insert(0, new TabelaTipoBloqueio());
            var list = from p in lsita select new DictionaryEntry(p.TipoBloqueio, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> TipoDespesa()
        {

            Repository<TabelaTipoDespesa> db = new Repository<TabelaTipoDespesa>();

            List<TabelaTipoDespesa> lsita = new List<TabelaTipoDespesa>();
            lsita = db.All().OrderBy(x => x.TipoDespesa).ToList();
            lsita.Insert(0, new TabelaTipoDespesa());
            var list = from p in lsita select new DictionaryEntry(p.TipoDespesa, p.Cod);
            return list.ToList();
        }
        public List<DictionaryEntry> Aging()
        {

            Repository<TabelaAging> db = new Repository<TabelaAging>();

            List<TabelaAging> lsita = new List<TabelaAging>();
            lsita = db.All().OrderBy(x => x.AtrasoDe).ToList();
            lsita.Insert(0, new TabelaAging());
            var list = from p in lsita select new DictionaryEntry(p.Aging, p.Cod);
            return list.ToList();
        }
        public List<DictionaryEntry> FaseSintese()
        {

            Repository<TabelaSintese> db = new Repository<TabelaSintese>();

            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            var dados = db.All().ToList();
            foreach (var item in dados)
            {
                item.TabelaFase.Fase = DbRotinas.Descriptografar(item.TabelaFase.Fase);
                item.Sintese = DbRotinas.Descriptografar(item.Sintese);
            }
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            foreach (var item in dados)
            {
                lista.Add(new DictionaryEntry(string.Format("{0} - {1}", item.TabelaFase.Fase, item.Sintese), item.Cod));
            }

            //var list = from p in l select new DictionaryEntry(p.TabelaFase.Fase + " - " + p.Sintese, p.Cod);
            return lista;
        }


        public List<DictionaryEntry> TipoDocumento()
        {

            Repository<TabelaTipoDocumento> db = new Repository<TabelaTipoDocumento>();

            List<TabelaTipoDocumento> lsita = new List<TabelaTipoDocumento>();
            lsita = db.All().OrderBy(x => x.TipoDocumento).ToList();
            //lsita.Insert(0, new TabelaTipoDocumento());
            var list = from p in lsita select new DictionaryEntry(p.TipoDocumento, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> Clientes()
        {

            Repository<Cliente> db = new Repository<Cliente>();

            List<Cliente> lista = new List<Cliente>();
            lista = db.All().ToList();
            foreach (var item in lista)
            {
                item.Nome = DbRotinas.Descriptografar(item.Nome);
            }
            lista = lista.OrderBy(x => x.Nome).ToList();
            lista.Insert(0, new Cliente());
            var list = from p in lista select new DictionaryEntry(p.Nome, p.Cod);
            return list.ToList();
        }

        public List<DictionaryEntry> Fases()
        {

            //int CodTipoCobranca .Where(x=>x.CodTipoCobranca == CodTipoCobranca)
            List<TabelaFase> lista = new List<TabelaFase>();
            lista = _dbFase.All().ToList();
            foreach (var item in lista)
            {
                item.Fase = DbRotinas.Descriptografar(item.Fase);
            }
            lista = lista.OrderBy(x => x.Ordem).ToList();
            lista.Insert(0, new TabelaFase());
            var list = from p in lista select new DictionaryEntry(p.Fase, p.Cod);
            return list.ToList();
        }
        public List<DictionaryEntry> CheckList()
        {

            //int CodTipoCobranca .Where(x=>x.CodTipoCobranca == CodTipoCobranca)
            List<TabelaCheckListNome> lista = new List<TabelaCheckListNome>();
            lista = _dbCheckListNome.All().OrderBy(x => x.CheckListNome).ToList();
            lista.Insert(0, new TabelaCheckListNome());
            var list = from p in lista select new DictionaryEntry(p.CheckListNome, p.Cod);
            return list.ToList();
        }
        public List<DictionaryEntry> Banco()
        {

            //int CodTipoCobranca .Where(x=>x.CodTipoCobranca == CodTipoCobranca)
            List<TabelaBanco> lista = new List<TabelaBanco>();
            lista = _dbBanco.All().OrderBy(x => x.NomeBanco).ToList();
            lista.Insert(0, new TabelaBanco());
            var list = from p in lista select new DictionaryEntry(p.NomeBanco, p.Cod);
            return list.ToList();
        }


        public List<DictionaryEntry> Resulucao()
        {
            List<TabelaResolucao> lista = new List<TabelaResolucao>();
            lista = _dbResolucao.All().ToList();
            foreach (var item in lista)
            {
                item.Resolucao = DbRotinas.Descriptografar(item.Resolucao);
            }
            lista = lista.OrderBy(x => x.Resolucao).ToList();
            lista.Insert(0, new TabelaResolucao());
            var list = from p in lista select new DictionaryEntry(p.Resolucao, p.Cod);
            return list.ToList();
        }
        public List<DictionaryEntry> MotivoAtraso()
        {
            List<TabelaMotivoAtraso> lista = new List<TabelaMotivoAtraso>();
            lista = _dbMotivoAtraso.All().ToList();
            foreach (var item in lista)
            {
                item.MotivoAtraso = DbRotinas.Descriptografar(item.MotivoAtraso);
            }
            lista = lista.OrderBy(x => x.MotivoAtraso).ToList();
            lista.Insert(0, new TabelaMotivoAtraso());
            var list = from p in lista select new DictionaryEntry(p.MotivoAtraso, p.Cod);
            return list.ToList();
        }
    }
}
