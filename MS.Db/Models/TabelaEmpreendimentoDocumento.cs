using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaEmpreendimentoDocumento : BaseEntity
    {
        public TabelaEmpreendimentoDocumento()
        {
            this.TabelaEmpreendimentoDocumentoChecklists = new List<TabelaEmpreendimentoDocumentoChecklist>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodEmpreendimento { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<short> Tipo { get; set; }
         public string ArquivoObservacao { get; set; }
         public string ArquivoCaminho { get; set; }
         public string ArquivoNome { get; set; }
         public string ArquivoGuid { get; set; }
         public Nullable<System.DateTime> DataVigencia { get; set; }
        public virtual TabelaEmpreendimento TabelaEmpreendimento { get; set; }
        public virtual ICollection<TabelaEmpreendimentoDocumentoChecklist> TabelaEmpreendimentoDocumentoChecklists { get; set; }
    }
}
