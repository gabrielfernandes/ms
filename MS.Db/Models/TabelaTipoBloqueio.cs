using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoBloqueio : BaseEntity
    {
        public TabelaTipoBloqueio()
        {
            this.Contratoes = new List<Contrato>();
        }

       //  public int Cod { get; set; }
         public string TipoBloqueio { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
    }
}
