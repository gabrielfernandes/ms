using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class Usuario : BaseEntity
    {
        public Usuario()
        {
            this.ChatAtendimentoes = new List<ChatAtendimento>();
            this.ChatAtendimentoMensagems = new List<ChatAtendimentoMensagem>();
            this.ChatAtentimentoHistoricoes = new List<ChatAtentimentoHistorico>();
            this.ClienteAtendimentoes = new List<ClienteAtendimento>();
            this.ClienteContatoes = new List<ClienteContato>();
            this.ClienteEnderecoes = new List<ClienteEndereco>();
            this.ClienteHistoricoes = new List<ClienteHistorico>();
            this.Contratoes = new List<Contrato>();
            this.Contratoes1 = new List<Contrato>();
            this.Contratoes2 = new List<Contrato>();
            this.Contratoes3 = new List<Contrato>();
            this.Contratoes4 = new List<Contrato>();
            this.Contratoes5 = new List<Contrato>();
            this.Contratoes6 = new List<Contrato>();
            this.ContratoAcaoCobrancaHistoricoes = new List<ContratoAcaoCobrancaHistorico>();
            this.ContratoAcordoes = new List<ContratoAcordo>();
            this.ContratoHistoricoes = new List<ContratoHistorico>();
            this.ContratoHistoricoes1 = new List<ContratoHistorico>();
            this.Historicoes = new List<Historico>();
            this.Importacaos = new List<Importacao>();
            this.ParametroAtendimentoes = new List<ParametroAtendimento>();
            this.Propostas = new List<Proposta>();
            this.Propostas1 = new List<Proposta>();
            this.Propostas2 = new List<Proposta>();
            this.PropostaHistoricoes = new List<PropostaHistorico>();
            this.PropostaProspectHistoricoes = new List<PropostaProspectHistorico>();
            this.PropostaStatusHistoricoes = new List<PropostaStatusHistorico>();
            this.TabelaEmpreendimentoes = new List<TabelaEmpreendimento>();
            this.TabelaEmpreendimentoes1 = new List<TabelaEmpreendimento>();
            this.TabelaEmpreendimentoes2 = new List<TabelaEmpreendimento>();
            this.TabelaEquipeUsuarios = new List<TabelaEquipeUsuario>();
            this.TabelaMuralAvisoDestinatarios = new List<TabelaMuralAvisoDestinatario>();
            this.TabelaParametroAtendimentoUsuarios = new List<TabelaParametroAtendimentoUsuario>();
            this.TabelaTipoCobrancaLogProcessamentoes = new List<TabelaTipoCobrancaLogProcessamento>();
            this.TabelaUnidadeReservas = new List<TabelaUnidadeReserva>();
            this.TabelaUnidadeStatusHistoricoes = new List<TabelaUnidadeStatusHistorico>();
            this.UsuarioRegionals = new List<UsuarioRegional>();
            this.ContratoDocumentoes = new List<ContratoDocumento>();
            this.ContratoParcelaBoletoHistoricoes = new List<ContratoParcelaBoletoHistorico>();
        }

        //  public int Cod { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string SenhaTemporaria { get; set; }
        public Nullable<short> PerfilTipo { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<System.DateTime> DataPrimeiroAcesso { get; set; }
        public Nullable<System.DateTime> DataUltimoAcesso { get; set; }
        public Nullable<System.DateTime> DataBloqueado { get; set; }
        public string MotivoBloqueio { get; set; }
        public string IP { get; set; }
        public Nullable<bool> DonoDeNegocio { get; set; }
        public Nullable<bool> DonoDeCarteira { get; set; }
        public Nullable<bool> Responsavel { get; set; }
        public Nullable<bool> Admin { get; set; }
        public Nullable<bool> ImportarDados { get; set; }
        public Nullable<bool> ExportarDados { get; set; }
        public Nullable<int> CodConstrutora { get; set; }
        public Nullable<int> CodEscritorio { get; set; }
        public Nullable<int> CodEmpresaVenda { get; set; }
        public Nullable<bool> SistemaCobranca { get; set; }
        public Nullable<bool> SistemaProposta { get; set; }
        public Nullable<bool> SistemaCredito { get; set; }
        public Nullable<bool> SistemaRepasse { get; set; }
        public Nullable<bool> SistemaRecebiveis { get; set; }
        public Nullable<short> TipoPessoa { get; set; }
        public string TelefoneResidencial { get; set; }
        public string TelefoneComercial { get; set; }
        public string TelefoneCelular { get; set; }
        public string Endereco { get; set; }
        public string CEP { get; set; }
        public Nullable<int> EnderecoNumero { get; set; }
        public string EnderecoComplemento { get; set; }
        public string EnderecoCidade { get; set; }
        public string EnderecoBairro { get; set; }
        public string EnderecoEstado { get; set; }
        public string CNPJ { get; set; }
        public Nullable<bool> PermissaoProspect { get; set; }
        public Nullable<bool> PermissaoProposta { get; set; }
        public string CPF { get; set; }
        public Nullable<int> CodRegional { get; set; }
        public Nullable<short> IdiomaTipo { get; set; }
        public Nullable<bool> PermissaoReguaSoberana { get; set; }
        public Nullable<bool> Bloqueado { get; set; }
        public virtual ICollection<ChatAtendimento> ChatAtendimentoes { get; set; }
        public virtual ICollection<ChatAtendimentoMensagem> ChatAtendimentoMensagems { get; set; }
        public virtual ICollection<ChatAtentimentoHistorico> ChatAtentimentoHistoricoes { get; set; }
        public virtual ICollection<ClienteAtendimento> ClienteAtendimentoes { get; set; }
        public virtual ICollection<ClienteContato> ClienteContatoes { get; set; }
        public virtual ICollection<ClienteEndereco> ClienteEnderecoes { get; set; }
        public virtual ICollection<ClienteHistorico> ClienteHistoricoes { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual ICollection<Contrato> Contratoes1 { get; set; }
        public virtual ICollection<Contrato> Contratoes2 { get; set; }
        public virtual ICollection<Contrato> Contratoes3 { get; set; }
        public virtual ICollection<Contrato> Contratoes4 { get; set; }
        public virtual ICollection<Contrato> Contratoes5 { get; set; }
        public virtual ICollection<Contrato> Contratoes6 { get; set; }
        public virtual ICollection<ContratoAcaoCobrancaHistorico> ContratoAcaoCobrancaHistoricoes { get; set; }

        public virtual ICollection<ContratoDocumento> ContratoDocumentoes { get; set; }
        public virtual ICollection<ContratoAcordo> ContratoAcordoes { get; set; }
        public virtual ICollection<ContratoHistorico> ContratoHistoricoes { get; set; }
        public virtual ICollection<ContratoHistorico> ContratoHistoricoes1 { get; set; }
        public virtual ICollection<Historico> Historicoes { get; set; }
        public virtual ICollection<Importacao> Importacaos { get; set; }
        public virtual ICollection<ParametroAtendimento> ParametroAtendimentoes { get; set; }
        public virtual ICollection<Proposta> Propostas { get; set; }
        public virtual ICollection<Proposta> Propostas1 { get; set; }
        public virtual ICollection<Proposta> Propostas2 { get; set; }
        public virtual ICollection<PropostaHistorico> PropostaHistoricoes { get; set; }
        public virtual ICollection<PropostaProspectHistorico> PropostaProspectHistoricoes { get; set; }
        public virtual ICollection<PropostaStatusHistorico> PropostaStatusHistoricoes { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes1 { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes2 { get; set; }
        public virtual TabelaEmpresaVenda TabelaEmpresaVenda { get; set; }
        public virtual ICollection<TabelaEquipeUsuario> TabelaEquipeUsuarios { get; set; }
        public virtual TabelaEscritorio TabelaEscritorio { get; set; }
        public virtual ICollection<TabelaMuralAvisoDestinatario> TabelaMuralAvisoDestinatarios { get; set; }
        public virtual ICollection<TabelaParametroAtendimentoUsuario> TabelaParametroAtendimentoUsuarios { get; set; }
        public virtual TabelaRegional TabelaRegional { get; set; }
        public virtual ICollection<TabelaTipoCobrancaLogProcessamento> TabelaTipoCobrancaLogProcessamentoes { get; set; }
        public virtual ICollection<TabelaUnidadeReserva> TabelaUnidadeReservas { get; set; }
        public virtual ICollection<TabelaUnidadeStatusHistorico> TabelaUnidadeStatusHistoricoes { get; set; }
        public virtual ICollection<UsuarioRegional> UsuarioRegionals { get; set; }
        public virtual ICollection<ContratoParcelaBoletoHistorico> ContratoParcelaBoletoHistoricoes { get; set; }
    }
}
