using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ContratoHistorico : BaseEntity
    {
        public ContratoHistorico()
        {
            this.Contratoes = new List<Contrato>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodContrato { get; set; }
         public Nullable<int> CodSintese { get; set; }
         public Nullable<int> CodMotivoAtraso { get; set; }
         public Nullable<int> CodResolucao { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public string Observacoes { get; set; }
         public Nullable<System.DateTime> DataAgenda { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<System.DateTime> DataAtendimento { get; set; }
         public Nullable<int> LoginAtendimento { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual TabelaMotivoAtraso TabelaMotivoAtraso { get; set; }
        public virtual TabelaResolucao TabelaResolucao { get; set; }
        public virtual TabelaSintese TabelaSintese { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Usuario Usuario1 { get; set; }
    }
}
