using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoEmpreendimentoFoto : BaseEntity
    {
        public TabelaTipoEmpreendimentoFoto()
        {
            this.TabelaEmpreendimentoFotoes = new List<TabelaEmpreendimentoFoto>();
        }

       //  public int Cod { get; set; }
         public string TipoFoto { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<TabelaEmpreendimentoFoto> TabelaEmpreendimentoFotoes { get; set; }
    }
}
