using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class Rota : BaseEntity
    {
        public Rota()
        {
            this.ParametroAtendimentoRotas = new List<ParametroAtendimentoRota>();
            this.RotaRegionals = new List<RotaRegional>();
        }

       //  public int Cod { get; set; }
         public string Nome { get; set; }
         public string Descricao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<ParametroAtendimentoRota> ParametroAtendimentoRotas { get; set; }
        public virtual ICollection<RotaRegional> RotaRegionals { get; set; }
    }
}
