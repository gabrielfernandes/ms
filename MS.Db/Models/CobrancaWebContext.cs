using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Cobranca.Db.Models.Mapping;

namespace Cobranca.Db.Models
{
    public partial class CobrancaWebContext : DbContext
    {
        static CobrancaWebContext()
        {
            Database.SetInitializer<CobrancaWebContext>(null);
        }

        public CobrancaWebContext()
            : base("Name=CobrancaWebContext")
        {
        }

        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new UsuarioMap());
        }
    }
}
