using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class temp_importacao_credito : BaseEntity
    {
         public int id { get; set; }
         public string numero_documento { get; set; }
         public string numero_cliente { get; set; }
         public string descricao { get; set; }
         public string data_credito { get; set; }
         public string valor_credito { get; set; }
         public string observação { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<bool> fgProcessados { get; set; }
    }
}
