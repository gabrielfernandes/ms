using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoCobrancaAcao : BaseEntity
    {
        public TabelaTipoCobrancaAcao()
        {
            this.ContratoAcaoCobrancaHistoricoes = new List<ContratoAcaoCobrancaHistorico>();
            this.TabelaParametroAtendimentoes = new List<TabelaParametroAtendimento>();
            this.TabelaTipoCobrancaAcaoCondicaos = new List<TabelaTipoCobrancaAcaoCondicao>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodTipoCobranca { get; set; }
         public Nullable<int> CodAcaoCobranca { get; set; }
         public Nullable<int> CodModelo { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string Titulo { get; set; }
         public string Modelo { get; set; }
         public Nullable<int> PeriodoEmDias { get; set; }
         public Nullable<short> CondicaoTipo { get; set; }
         public Nullable<short> CampoTipo { get; set; }
         public string CondicaoQuery { get; set; }
         public Nullable<short> AcaoTipo { get; set; }
         public Nullable<short> AcaoHistoricoTipo { get; set; }
         public Nullable<int> CodFase { get; set; }
         public Nullable<int> CodSintese { get; set; }
         public string AcaoHistoricoObservacao { get; set; }
         public Nullable<bool> AgruparProcessosNegativacao { get; set; }
         public Nullable<int> DiasAtrasoDeNegativacao { get; set; }
         public Nullable<int> DiasAtrasoAteNegativacao { get; set; }
         public Nullable<int> CodFilial { get; set; }
         public Nullable<int> CodTipoCobrancaAcaoSoberana { get; set; }
        public virtual ICollection<ContratoAcaoCobrancaHistorico> ContratoAcaoCobrancaHistoricoes { get; set; }
        public virtual TabelaAcaoCobranca TabelaAcaoCobranca { get; set; }
        public virtual ICollection<TabelaParametroAtendimento> TabelaParametroAtendimentoes { get; set; }
        public virtual TabelaTipoCobranca TabelaTipoCobranca { get; set; }
        public virtual ICollection<TabelaTipoCobrancaAcaoCondicao> TabelaTipoCobrancaAcaoCondicaos { get; set; }
    }
}
