using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaUnidade : BaseEntity
    {
        public TabelaUnidade()
        {
            this.Contratoes = new List<Contrato>();
            this.Propostas = new List<Proposta>();
            this.TabelaUnidadeStatusHistoricoes = new List<TabelaUnidadeStatusHistorico>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodBloco { get; set; }
         public string NumeroUnidade { get; set; }
         public string CodUnidadeCliente { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> Andar { get; set; }
         public Nullable<decimal> Valor { get; set; }
         public Nullable<decimal> ValorAvaliacao { get; set; }
         public Nullable<short> Status { get; set; }
         public Nullable<int> QtdDormitorio { get; set; }
         public Nullable<int> CodTipoUnidade { get; set; }
         public Nullable<System.DateTime> DataReserva { get; set; }
         public string LoginReserva { get; set; }
         public Nullable<decimal> Metragem { get; set; }
         public Nullable<bool> PNE { get; set; }
         public Nullable<int> Vagas { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual ICollection<Proposta> Propostas { get; set; }
        public virtual TabelaBloco TabelaBloco { get; set; }
        public virtual TabelaTipoUnidade TabelaTipoUnidade { get; set; }
        public virtual ICollection<TabelaUnidadeStatusHistorico> TabelaUnidadeStatusHistoricoes { get; set; }
    }
}
