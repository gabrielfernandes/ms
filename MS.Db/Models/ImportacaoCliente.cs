using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ImportacaoCliente : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodImportacao { get; set; }
         public string NumeroCliente { get; set; }
         public string DescricaoCliente { get; set; }
         public string CpfCnpj { get; set; }
         public string Endereco { get; set; }
         public string Numero { get; set; }
         public string Complemento { get; set; }
         public string Bairro { get; set; }
         public string CEP { get; set; }
         public string Cidade { get; set; }
         public string Estado { get; set; }
         public string TP { get; set; }
         public string DDD { get; set; }
         public string NumeroTelefone { get; set; }
         public string Email { get; set; }
         public Nullable<short> Status { get; set; }
         public string Mensagem { get; set; }
         public Nullable<System.DateTime> DataProcessamento { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<bool> Duplicado { get; set; }
        public virtual Importacao Importacao { get; set; }
    }
}
