using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ParametroPlataforma : BaseEntity
    {
        //  public int Cod { get; set; }
        public byte[] Logo { get; set; }
        public string TituloPagina { get; set; }
        public Nullable<short> LayoutTipo { get; set; }
        public string EmailRemetente { get; set; }
        public string NomeRemetente { get; set; }
        public Nullable<int> SMTPPorta { get; set; }
        public string SMTPServidor { get; set; }
        public string SMTPUsuario { get; set; }
        public string SMTPSenha { get; set; }
        public Nullable<bool> fgSSL { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public string CaminhoLogo { get; set; }
        public Nullable<short> Layout { get; set; }
        public string Icone { get; set; }
        public string TmNow { get; set; }
        public string TmOff { get; set; }
        public string NomeEmpresa { get; set; }
        public string UrlSistema { get; set; }
    }
}
