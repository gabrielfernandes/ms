using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoCobranca : BaseEntity
    {
        public TabelaTipoCobranca()
        {
            this.Contratoes = new List<Contrato>();
            this.ContratoEscritorioHistoricoes = new List<ContratoEscritorioHistorico>();
            this.ContratoTipoCobrancaHistoricoes = new List<ContratoTipoCobrancaHistorico>();
            this.TabelaFases = new List<TabelaFase>();
            this.TabelaSintese = new List<TabelaSintese>();
            this.TabelaTipoCobrancaAcaos = new List<TabelaTipoCobrancaAcao>();
            this.TabelaTipoCobrancaCondicaos = new List<TabelaTipoCobrancaCondicao>();
            this.TabelaTipoCobrancaPosicaos = new List<TabelaTipoCobrancaPosicao>();
        }

       //  public int Cod { get; set; }
         public string TipoCobranca { get; set; }
         public Nullable<int> QtdDias { get; set; }
         public Nullable<short> TipoImpacto { get; set; }
         public Nullable<short> TipoEsforco { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<short> TipoFluxo { get; set; }
         public string CondicaoQuery { get; set; }
         public Nullable<int> Ordem { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual ICollection<ContratoEscritorioHistorico> ContratoEscritorioHistoricoes { get; set; }
        public virtual ICollection<ContratoTipoCobrancaHistorico> ContratoTipoCobrancaHistoricoes { get; set; }
        public virtual ICollection<TabelaFase> TabelaFases { get; set; }
        public virtual ICollection<TabelaSintese> TabelaSintese { get; set; }
        public virtual ICollection<TabelaTipoCobrancaAcao> TabelaTipoCobrancaAcaos { get; set; }
        public virtual ICollection<TabelaTipoCobrancaCondicao> TabelaTipoCobrancaCondicaos { get; set; }
        public virtual ICollection<TabelaTipoCobrancaPosicao> TabelaTipoCobrancaPosicaos { get; set; }
    }
}
