using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaAcaoCobranca : BaseEntity
    {
        public TabelaAcaoCobranca()
        {
            this.TabelaCampanhaAcaos = new List<TabelaCampanhaAcao>();
            this.TabelaModeloAcaos = new List<TabelaModeloAcao>();
            this.TabelaTipoCobrancaAcaos = new List<TabelaTipoCobrancaAcao>();
        }

       //  public int Cod { get; set; }
         public string AcaoCobranca { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<short> Tipo { get; set; }
        public virtual ICollection<TabelaCampanhaAcao> TabelaCampanhaAcaos { get; set; }
        public virtual ICollection<TabelaModeloAcao> TabelaModeloAcaos { get; set; }
        public virtual ICollection<TabelaTipoCobrancaAcao> TabelaTipoCobrancaAcaos { get; set; }
    }
}
