﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class EmpreendimentoDados
    {
        public string NomeEmpreendimento { get; set; }
        public string NomeBloco { get; set; }
        public string NumeroUnidade { get; set; }
    }
}
