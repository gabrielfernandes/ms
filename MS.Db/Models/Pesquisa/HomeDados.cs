﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class HomeDados
    {
        public int CodAging { get; set; }
        public string Aging { get; set; }
        public decimal? ValorAtraso { get; set; }
        public int? QtdContrato { get; set; }
        public int? QtdParcela { get; set; }
        public string Cor { get; set; }
    }
}
