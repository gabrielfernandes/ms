﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class BoletoSolicitacaoDados
    {
        public Nullable<int> CodParcela { get; set; }
        public Nullable<int> CodBoleto { get; set; }
        public string Nome { get; set; }
        public string Regional { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public Nullable<int> CodHistorico { get; set; }
        public Nullable<int> CodClientePrincipal { get; set; }
        public string CodigoCliente { get; set; }
        public string TipoCobranca { get; set; }
        public string Fase { get; set; }
        public string Sintese { get; set; }
        public string NumeroDocumento { get; set; }
        public Nullable<DateTime> DataVencimento { get; set; }
        public Nullable<DateTime> DataDocumento { get; set; }
        public Nullable<Decimal> JurosAoMesPorcentagem { get; set; }
        public Nullable<Decimal> JurosAoMesPorcentagemPrevisto { get; set; }
        public Nullable<Decimal> ValorJuros { get; set; }
        public Nullable<Decimal> ValorJurosPrevisto { get; set; }
        public Nullable<Decimal> MultaAtrasoPorcentagem { get; set; }
        public Nullable<Decimal> MultaAtrasoPorcentagemPrevisto { get; set; }
        public Nullable<Decimal> ValorMulta { get; set; }
        public Nullable<Decimal> ValorMultaPrevisto { get; set; }
        public Nullable<Decimal> ValorDocumentoOriginal { get; set; }
        public Nullable<Decimal> ValorDocumentoPrevisto { get; set; }
        public Nullable<Decimal> ValorDocumento { get; set; }
        public string Observacao { get; set; }
        public Nullable<short> TipoFluxo { get; set; }
        public Nullable<short> Status { get; set; }
        public string Usuario { get; set; }
        public Nullable<DateTime> DataCadastro { get; set; }
        public Nullable<DateTime> DataAprovacao { get; set; }
        public Nullable<int> NossoNumero { get; set; }
        public Nullable<DateTime> DataRemessa { get; set; }
        public Nullable<bool> Alterar { get; set; }
        public Nullable<int> CodLogArquivo { get; set; }
        public string Erro { get; set; }



    }
}
