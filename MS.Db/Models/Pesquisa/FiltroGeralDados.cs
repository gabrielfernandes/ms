﻿using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class FiltroGeralDados
    {


        public bool Bloqueado { get; set; }
        public string CPF_CNPJ { get; set; }
        public string CPF_CNPJEncrypt { get { return DbRotinas.Criptografar(CPF_CNPJ); } }
        public int? ClienteId { get; set; }
        public List<int> CodContratos { get; set; }
        public int CodAging { get; set; }
        public string CodigoCliente { get; set; }
        public string CodigoClienteEncrypt { get { return DbRotinas.Criptografar(CodigoCliente); } }
        public int CodTipoCobranca { get; set; }
        public int CodConstrutora { get; set; }
        public int CodRegional { get; set; }
        public string CodProduto { get; set; }
        public string[] CodProdutoArray { get { return CodProduto != null ? CodProduto.Split(',').Select(x => x).ToArray() : null; } }
        public int CodSintese { get; set; }
        public short? CodPerfilTipo { get; set; }
        public string CodProdutos { get; set; }
        public string CodRegionais { get; set; }
        public string[] CodRegionaisArray { get { return CodRegionais != null ? CodRegionais.Split(',').Select(x => x).ToArray() : null; } }
        public string CodConstrutoras { get; set; }
        public string CodAgings { get; set; }
        public string CodRotas { get; set; }
        public int? CodUsuarioAtendimento { get; set; }
        public int CodFase { get; set; }
        public int CodGrupo { get; set; }
        public int? CodTipoBloqueio { get; set; }
        public string CodContrato { get; set; }
        public string CodContratoParcela { get; set; }
        public int CodSinteseParcela { get; set; }
        public int CodSinteseBoleto { get; set; }
        public int? CodUsuarioLogado { get; set; }
        public string[] ConstrutorasIds { get; set; }
        public List<int> CodParcelas { get; set; }
        public int CodUsuario { get; set; }
        public DateTime? DataAgenda { get; set; }
        public string DataAgendaDesc { get { return $"{DataAgenda:dd/MM/yyyy}"; } }
        public long FaseId { get; set; }
        public string NomeUsuario { get; set; }
        public string NomeCliente { get; set; }
        public string NomeClienteEncrypt { get { return DbRotinas.Criptografar(NomeCliente); } }
        public short Status { get; set; }
        public string[] ProdutosIds { get; set; }
        public DateTime? PeriodoDe { get; set; }
        public DateTime? PeriodoAte { get; set; }
        public string PrimeiroNome { get; set; }
        public string PrimeiroNomeEncrypt { get { return DbRotinas.Criptografar(PrimeiroNome); } }
        public string[] RegionalIds { get; set; }
        public string CodAgingCliente { get; set; }
        public string[] CodAgingClienteArray { get { return CodAgingCliente != null ? CodAgingCliente.Split(',').Select(x => x).ToArray() : null; } }
        public string[] AgingIds { get; set; }
        public string AndamentoDaObra { get; set; }
        public string NumeroContrato { get; set; }
        public string NumeroContratoEncrypt { get { return NumeroContrato != null ? DbRotinas.Criptografar(NumeroContrato.ToUpper()) : null; } }
        public string[] NumerosContratosCriptografadoArray { get { return NumeroContrato.Split(',').Select(x => DbRotinas.Descriptografar(x.ToUpper())).ToArray(); } }
        public string NumeroFatura { get; set; }
        public string[] NumerosFaturasArray { get { return NumeroFatura.Split(',').ToArray(); } }
        public string NotaFiscal { get; set; }
        public string[] NotaFiscaisArray { get { return NotaFiscal.Split(',').ToArray(); } }


    }
}
