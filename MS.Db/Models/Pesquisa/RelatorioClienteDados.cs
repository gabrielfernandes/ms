﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class RelatorioClienteDados
    {
        public string NomeConstrutora { get; set; }
        public string Regional { get; set; }
        public string Cliente { get; set; }
        public string CPF { get; set; }

        public string CNPJ { get; set; }

        public int TotalResidencial { get; set; }
        public int TotalComercial { get; set; }
        public int TotalCelular { get; set; }
        public int TotalEmail { get; set; }
        public int TotalContato { get; set; }
    }
}
