﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class RelatorioCreditoDados
    {
        public string NomeConstrutora { get; set; }
        public string NomeConstrutoraEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(NomeConstrutora); } }
        public string Regional { get; set; }
        public string RegionalEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(Regional); } }
        public string Cliente { get; set; }
        public string ClienteEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(Cliente); } }
        public string CodCliente { get; set; }
        public string CodClienteEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(CodCliente); } }
        public string CPF { get; set; }
        public string CPFEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(CPF); } }
        public short TipoAcordo { get; set; }
        public string Observacao { get; set; }
        public int TotalParcelas { get; set; }
        public int TotalParcelaPagas { get; set; }
        public int TotalParcelaAtrasada { get; set; }
        public decimal? ValorDivida { get; set; }
        public string ValorDividaDescricao { get { return string.Format("{0:C}", this.ValorDivida); } }
        public decimal? ValorAcordo { get; set; }
        public string ValorAcordoDescricao { get { return string.Format("{0:C}", this.ValorAcordo); } }
        public decimal? ValorRecebido { get; set; }
        public string ValorRecebidoDescricao { get { return string.Format("{0:C}", this.ValorRecebido); } }
        public decimal? ValorAtraso { get; set; }
        public string ValorAtrasoDescricao { get { return string.Format("{0:C}", this.ValorAtraso); } }
        public string StatusSituacao { get { return EnumeradoresDescricao.TipoAcordo(this.TipoAcordo); } }


    }
}
