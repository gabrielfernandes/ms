﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class ExportarEmail
    {
        public string Cliente { get; set; }
        public string Email { get; set; }

    }
}
