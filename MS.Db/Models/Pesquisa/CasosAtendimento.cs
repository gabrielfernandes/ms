﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class CasosAtendimento
    {
        public int? CodContrato { get; set; }

        public int? CodCliente { get; set; }

        public DateTime? DataAtendimento { get; set; }
        public int? CodAcaoCobrancaHistorico { get; set; }
        public int? CodParcela { get; set; }

    }
}
