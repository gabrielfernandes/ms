﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class AgendaOperador
    {
        public int HistoricoId { get; set; }
        public int ContratoId { get; set; }
        public string NomeCliente { get; set; }
        public DateTime DataAgenda { get; set; }
        public string Sintese { get; set; }
        public string MotivoAtraso { get; set; }
        public short Status { get; set; }


    }
}
