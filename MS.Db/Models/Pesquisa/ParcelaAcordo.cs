﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class ParcelaAcordo
    {
        public int? CodContrato { get; set; }
        public int? Cod { get; set; }
        public string FluxoPagamento { get; set; }
        public decimal? CodAcordo { get; set; }
        public decimal? ValorParcela { get; set; }
        public DateTime? DataCadastro { get; set; }
        public DateTime? DataVencimento { get; set; }
        public decimal? Juros { get; set; }
        public decimal? Multa { get; set; }
        public decimal? Honorario { get; set; }
        public decimal? Desconto{ get; set; }

        public string Quantidade { get; set; }
        public decimal? ValorParcial { get; set; }
        public decimal? Total { get; set; }
    }
}
