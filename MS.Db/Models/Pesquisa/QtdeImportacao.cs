﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class QtdeImportacao
    {
        public int? Contrato { get; set; }
        public int? Cliente { get; set; }
        public int? Parcela { get; set; }

    }
}
