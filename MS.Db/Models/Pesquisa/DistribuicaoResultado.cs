﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class DistribuicaoResultado
    {
        public int Cod { get; set; }
        public int Total { get; set; }
        public decimal ValorTotal { get; set; }
        public string ValorTotalDesc { get { return string.Format("R$ {0:n2}", this.ValorTotal); } }
        public string TotalDesc { get { return string.Format("Total de {0} casos.", this.Total); } }
    }
}
