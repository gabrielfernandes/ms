﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class CasosEnviarEmail
    {
        public int? Cod { get; set; }
        public int? CodCliente { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Modelo { get; set; }
     
    }
}
