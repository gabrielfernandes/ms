﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class HeaderContratoParcelaBoletoDados
    {
        public string CodCliente { get; set; }
        public string Cliente { get; set; }
        public string CpfCnpj { get; set; }
        public string NumeroContrato { get; set; }
        public string NumeroDocumento { get; set; }
        public Nullable<System.DateTime> DataVencimento { get; set; }
        public Nullable<System.Decimal> ValorParcela { get; set; }
        public Nullable<System.Decimal> ValorAberto { get; set; }
        public string TipoFatura { get; set; }
        public string CodigoSinteseCliente { get; set; }

    }
}
