﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class CalculaParcelaContrato
    {
        public int CodContrato { get; set; }
        public decimal? ValorProduto { get; set; }
        public decimal? ValorParcela { get; set; }
        public int? QtdParcelas { get; set; }
        public decimal? ValorTotal { get; set; }
        public decimal? Resto { get; set; }


    }
}
