﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class RelatorioAcaoCobrancaDados
    {
        public short? Tipo { get; set; }
        public int? Qtd { get; set; }
        public string StatusSituacao { get { return EnumeradoresDescricao.TipoAcaoCobranca(this.Tipo); } }


    }
}
