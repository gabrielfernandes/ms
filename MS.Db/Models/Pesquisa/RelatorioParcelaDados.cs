﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class RelatorioParcelaDados
    {
        public string NomeConstrutora { get; set; }
        public string NomeConstrutoraEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(NomeConstrutora); } }
        public string Regional { get; set; }
        public string RegionalEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(Regional); } }
        public string Cliente { get; set; }
        public string ClienteEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(Cliente); } }
        public string CPF { get; set; }
        public string CPFEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(CPF); } }
        public int? Quantidade { get; set; }
        public decimal? ValorTotalParcela { get; set; }
        public decimal? ValorTotalEmAberto { get; set; }
        public decimal? ValorTotalPago { get; set; }
        public decimal? ValorTotalAtraso { get; set; }
        public string ValorTotalParcelaDescricao { get { return string.Format("{0:C}", this.ValorTotalParcela); } }
        public string ValorTotalEmAbertoDescricao { get { return string.Format("{0:C}", this.ValorTotalEmAberto); } }
        public string ValorTotalPagoDescricao { get { return string.Format("{0:C}", this.ValorTotalPago); } }
        public string ValorTotalAtrasoDescricao { get { return string.Format("{0:C}", this.ValorTotalAtraso); } }
    }

    public class RelatorioAtendimentoDados
    {

        //return Db.Rotinas.DbRotinas.Descriptografar(NomeConstrutora); 
        public string CodCliente { get; set; }
        public string CNPJ { get; set; }
        public string NumeroContrato { get; set; }
        public string NumeroDocumento { get; set; }
        public DateTime? DataEmissaoFatura { get; set; }
        public string NumeroFatura { get; set; }
        public DateTime? DataVencimento { get; set; }
        public string FluxoPagamento { get; set; }
        public string NumeroNotaFiscal { get; set; }
        public decimal? VarlorParcela { get; set; }
        public decimal? ValorAberto { get; set; }
        public string NumeroParcela { get; set; }
        public string RotaArea { get; set; }
        public string Filial { get; set; }
        public DateTime? DataFechamento { get; set; }
        public DateTime? DataPagamento { get; set; }
        public string Status { get; set; }
        public string Fase { get; set; }
        public string Sintese { get; set; }
        public string Observacao { get; set; }
        public string Contatos { get; set; }
        public DateTime? DataAgenda { get; set; }
        public string Cia { get; set; }
        public string Responsavel { get; set; }        
        public string DataEmissaoFaturaDesc { get { return string.Format("{0:dd/MM/yyyy}", this.DataEmissaoFatura); } }
        public string DataFechamentoDesc { get { return string.Format("{0:dd/MM/yyyy}", this.DataFechamento); } }
        public string DataPagamentoDesc { get { return string.Format("{0:dd/MM/yyyy}", this.DataPagamento); } }
        public string DataAgendaDesc { get { return string.Format("{0:dd/MM/yyyy}", this.DataAgenda); } }
        public string DataVencimentoDesc { get { return string.Format("{0:dd/MM/yyyy}", this.DataVencimento); } }        
        public string MotivoAtraso { get; set; }
    }
}
