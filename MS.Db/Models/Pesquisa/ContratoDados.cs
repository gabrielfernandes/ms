﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class ContratoDados
    {
        public string CodCliente { get; set; }
        public int? CodClienteAcao { get; set; }
        public string NomeConstrutora { get; set; }
        public string Regional { get; set; }
        public string Descricao { get; set; }
        public string NumeroContrato { get; set; }
        public int CodContrato { get; set; }
        public int CodSintese { get; set; }
        public string Cliente { get; set; }
        public string Aging { get; set; }
        public int? CodAging { get; set; }
        public decimal? ValorAtraso { get; set; }
        public DateTime? DataAgenda { get; set; }
        public string UltimaObs { get; set; }
        public string Login { get; set; }
        public string Sintese { get; set; }
        public string CodigoSinteseCliente { get; set; }
        public string Fase { get; set; }
        public int? CodTipoCobranca { get; set; }
        public string CPF { get; set; }
        public int CodContratoAcaoCobrancaHistorico { get; set; }
        public int? DiasFase { get; set; }
        public int? DiasSintese { get; set; }
        public short? SituacaoTipo { get; set; }
        public string Status { get; set; }
        public string HistObservacao { get; set; }
        public int CodFase { get; set; }
        public decimal? ValorTotal { get; set; }
        public decimal? ValorAVencer { get; set; }
        public decimal? ValorPago { get; set; }
        public int? Qtde { get; set; }
        public int? QtdContrato { get; set; }
        public int? QtdParcela { get; set; }
        public int? QtdCliente { get; set; }
        public int? QtdClienteFase { get; set; }
        public int? DiasAtraso { get; set; }
        public int? Cod { get; set; }

        public string Email { get; set; }
        public int CodContratoParcela { get; set; }
        public string NomeUsuario { get; set; }
        public string Observacoes { get; set; }
        public DateTime? DataCadastro { get; set; }
        



        public long? NumeroTratado { get; set; }
        public string StatusValidacao { get; set; }
        public short NumeroStatus { get; set; }
    }
}
