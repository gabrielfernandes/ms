﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class PerguntaAnaliseDocumental
    {
        public short? Campo { get; set; }
        public string TipoDocumento { get; set; }
        public string Pergunta { get; set; }
        public int? CodDocumento { get; set; }
        public Int16? TipoPergunta { get; set; }
        public int? CodAnaliseDocumental { get; set; }
        public string Obs { get; set; }
        public bool? Confere { get; set; }
        public decimal? Valor { get; set; }
    }
}
