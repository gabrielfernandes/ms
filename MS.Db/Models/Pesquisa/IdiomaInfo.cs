﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    [Serializable]
    public class IdiomaInfo
    {
        public string Cod { get; set; }
        public string ptBR { get; set; }
        public string enUS { get; set; }
        public string esES { get; set; }
    }
}
