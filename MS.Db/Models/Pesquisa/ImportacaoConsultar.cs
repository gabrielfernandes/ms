﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class ImportacaoConsultar
    {
        public Nullable<int> Cod { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Nome { get; set; }
        public Nullable<int> TotalRegistros { get; set; }
        public Nullable<int> TotalRegistrosOK { get; set; }
        public Nullable<int> TotalRegistrosErro { get; set; }
        public Nullable<short> Status { get; set; }
        public Nullable<System.DateTime> DataProcessamentoAgendado { get; set; }
        public Nullable<System.DateTime> DataProcessado { get; set; }
        public Nullable<int> TotalRegistrosInconsistentes { get; set; }
        public Nullable<short> Tipo { get; set; }
        public string NomeArquivo { get; set; }
    }
}
