﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class CobrancaAcaoBoletoDados
    {
        public string NomeConstrutora { get; set; }
        public string Regional { get; set; }
        public string Descricao { get; set; }
        public string NumeroContrato { get; set; }
        public int CodContrato { get; set; }
        public string Cliente { get; set; }
        public string Aging { get; set; }
        public int? CodAging { get; set; }
        public DateTime? DataAgenda { get; set; }
        public string DataAgendaDesc { get { return string.Format("{0:dd/MM/yyyy}", DataAgenda); } }
        public string UltimaObs { get; set; }
        public string Login { get; set; }
        public string Sintese { get; set; }
        public string Fase { get; set; }
        public int? CodTipoCobranca { get; set; }
        public string CPF { get; set; }
        public string CodCliente { get; set; }
        public string TelefoneSMS { get; set; }
        public short? SituacaoTipo { get; set; }
        public string SituacaoTipoNome { get; set; }
        public decimal? ValorTotal { get; set; }
        public int? DiasAtraso { get; set; }
        public int? Cod { get; set; }

        public string Email { get; set; }
        public string Celular { get; set; }
        public string TelefoneComercial { get; set; }
        public string TelefoneResidencial { get; set; }
        public int? CodContratoParcela { get; set; }

        public int? CodParcela { get; set; }
        public int? NumeroDocumento { get; set; }
        public int? NossoNumero { get; set; }
        public string NossoNumeroDesc { get { return string.Format("{0:00000000}", NossoNumero); } }
        public DateTime? DataDocumento { get; set; }
        public string DataDocumentoDesc { get { return string.Format("{0:dd/MM/yyyy}", DataDocumento); } }
        public DateTime? DataVencimento { get; set; }
        public string DataVencimentoDesc { get { return string.Format("{0:dd/MM/yyyy}", DataVencimento); } }
        public DateTime? DataCredito { get; set; }
        public string DataCreditoDesc { get { return string.Format("{0:dd/MM/yyyy}", DataCredito); } }
        public decimal? ValorDocumento { get; set; }
        public string ValorDocumentoDescricao { get { return string.Format("{0:C}", this.ValorDocumento); } }
        public decimal? ValorDesconto { get; set; }
        public string ValorDescontoDescricao { get { return string.Format("{0:C}", this.ValorDesconto); } }
        public string Guid { get; set; }

    }
}
