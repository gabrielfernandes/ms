﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class HistoricoRepasseDados
    {
        public string Data { get; set; }
        public string Fase { get; set; }
        public string Sintese { get; set; }
        public string Obs { get; set; }
        public string Login { get; set; }

    }
}
