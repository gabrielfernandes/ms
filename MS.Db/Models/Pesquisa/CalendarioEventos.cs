﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class CalendarioEventos
    {
        public string id { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string description { get; set; }
        public string className { get; set; }
        public string icon { get; set; }
        public string url { get; set; }

    }
}
