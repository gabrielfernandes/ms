﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class ContratoParcelaDados
    {
        public Nullable<int> CodContrato { get; set; }
        public Nullable<int> CodNatureza { get; set; }
        public string FluxoPagamento { get; set; }
        public Nullable<System.DateTime> DataVencimento { get; set; }
        public Nullable<System.DateTime> DataFechamento { get; set; }

        public Nullable<System.DateTime> DataCadastro { get; set; }
        public Nullable<System.DateTime> DataExcluido { get; set; }
        public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<System.DateTime> DataPagamento { get; set; }
        public Nullable<decimal> ValorParcela { get; set; }
        public Nullable<decimal> ValorRecebido { get; set; }
        public Nullable<int> NaturezaCliente { get; set; }
        public Nullable<short> StatusParcela { get; set; }
        public Nullable<decimal> ValorAberto { get; set; }
        public string NumeroParcela { get; set; }
        public Nullable<decimal> Juros { get; set; }
        public Nullable<decimal> Multa { get; set; }
        public Nullable<decimal> ValorAtualizado { get; set; }
        public Nullable<System.DateTime> DataEmissao { get; set; }

        public Nullable<System.DateTime> DataEmissaoFatura { get; set; }


        public Nullable<int> CodParcela { get; set; }
        public string Status { get; set; }
        public string StatusContrato { get; set; }

        public Nullable<int> CodFase { get; set; }
        public Nullable<int> CodSintese { get; set; }
        public string Fase { get; set; }

        public string Sintese { get; set; }
        public string HistoricoObs { get; set; }
        public Nullable<int> CodAging { get; set; }
        public string LinhaDigitavel { get; set; }
        public string CodCliente { get; set; }
        public string NumeroContrato { get; set; }
        public string CodRegionalCliente { get; set; }
        public string Companhia { get; set; }
        public string CodSinteseCliente { get; set; }
        public string NF { get; set; }
        public string TipoFatura { get; set; }
        public string RotaArea { get; set; }
        public string Filial { get; set; }
        public string Governo { get; set; }
        public string InstrucaoPagamento { get; set; }
        public int? NossoNumero { get; set; }
        public string Corporativo { get; set; }
        public string CodigoSinteseCliente { get; set; }
        public string CodigoEmpresaCobranca { get; set; }
        public Nullable<int> DiasAtraso { get; set; }
        public string ClienteParcela { get; set; }
        public string CompanhiaFiscal { get; set; }
        public string ClienteVIP { get; set; }

        public Nullable<short> TipoAcaoCobranca { get; set; }
        public Nullable<System.DateTime> AcaoDtGerada { get; set; }
        public string Linha { get; set; }
    }
}
