﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class AcaoCobrancaDados
    {
        public string NomeEmpreendimento { get; set; }
        public string NomeBloco { get; set; }
        public string NumeroUnidade { get; set; }
        public string NumeroContrato { get; set; }
        public int CodContrato { get; set; }
        public string Cliente { get; set; }
        public string Aging { get; set; }
        public decimal? ValorAtraso { get; set; }
        public DateTime? DataAgenda { get; set; }
        public string DataAgendaDesc { get { return string.Format("{0:dd/MM/yyyy}", DataAgenda); } }
        public string UltimaObs { get; set; }
        public string Login { get; set; }
        public string Sintese { get; set; }
        public string Fase { get; set; }
        public int? CodTipoCobranca { get; set; }
        public string CPF { get; set; }
        public string CodCliente { get; set; }

        public int CodContratoAcaoCobrancaHistorico { get; set; }

        public string TelefoneSMS { get; set; }

    }
}
