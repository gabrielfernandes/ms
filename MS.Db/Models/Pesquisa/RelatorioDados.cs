﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class RelatorioDados
    {
        public string NomeConstrutora { get; set; }
        public string Regional { get; set; }

        public string Descricao { get; set; }
        public string NomeEmpreendimento { get; set; }
        public string NomeBloco { get; set; }
        public string NumeroUnidade { get; set; }
        public string NumeroContrato { get; set; }
        public int CodContrato { get; set; }
        public int CodSintese { get; set; }
        public string Cliente { get; set; }
        public string Aging { get; set; }
        public int? CodAging { get; set; }
        public decimal? ValorAtraso { get; set; }
        public DateTime? DataAgenda { get; set; }
        public string DataAgendaDesc { get { return string.Format("{0:dd/MM/yyyy}", DataAgenda); } }
        public string UltimaObs { get; set; }
        public string Login { get; set; }
        public string Sintese { get; set; }
        public string Fase { get; set; }
        public int? CodTipoCobranca { get; set; }
        public string CPF { get; set; }
        public string CodCliente { get; set; }
        public int CodContratoAcaoCobrancaHistorico { get; set; }
        public int? DiasFase { get; set; }
        public int? DiasSintese { get; set; }
        public string TelefoneSMS { get; set; }
        public string ValorAtrasoDescricao { get { return string.Format("{0:C}", this.ValorAtraso); } }
        public short? SituacaoTipo { get; set; }
        public string SituacaoTipoNome { get; set; }
        public string Status { get; set; }

        public int CodFase { get; set; }
        public decimal? ValorTotal { get; set; }
        public int Qtde { get; set; }

        public string AcaoSituacao { get { return this.SituacaoTipo != (short)Enumeradores.SituacaoHistorico.Enviado ? this.Status : "Pronto para envio"; } }


    }
}
