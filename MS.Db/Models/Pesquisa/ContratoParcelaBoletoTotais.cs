﻿using Cobranca.Db.Rotinas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class ContratoParcelaBoletoTotais
    {
        public int? QtdBoleto { get; set; }
        public int? CodSintese { get; set; }
        public int? CodFase { get; set; }
        public string Sintese { get; set; }
        public string SinteseEncrypt { get { return DbRotinas.Criptografar(Sintese); } }
        public string Fase { get; set; }
        public string FaseEncrypt { get { return DbRotinas.Criptografar(Fase); } }

        public Nullable<short> TipoFluxo { get; set; }

    }
}
