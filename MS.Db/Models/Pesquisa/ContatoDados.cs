﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class ContatoDados
    {
        public int CodContrato { get; set; }
        public string NomeCliente { get; set; }
        public string NumeroContato { get; set; }
        public string DataProcessamento { get; set; }

    }
}
