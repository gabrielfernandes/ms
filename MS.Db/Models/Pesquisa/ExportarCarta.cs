﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class ExportarCarta
    {
        public string Nome { get; set; }
        public string Rg { get; set; }
        public string CPF { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string CEP { get; set; }
        public string Cliente { get; set; }
        public string Empreendimento { get; set; }
        public string Empresa { get; set; }
        public string Divisao { get; set; }
        public string Torre { get; set; }
        public string  Unidade { get; set; }
        public string Codigo { get; set; }

    }
}
