﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class HistoricoDados
    {
        public int Cod { get; set; }
        public Nullable<int> CodCliente { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public Nullable<int> CodParcela { get; set; }
        public Nullable<int> CodSintese { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public System.DateTime DataCadastro { get; set; }
        public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<System.DateTime> DataExcluido { get; set; }
        public string Observacao { get; set; }
        public string ObservacaoEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(Observacao); } }
        public Nullable<int> CodMotivoAtraso { get; set; }
        public Nullable<int> CodResolucao { get; set; }

        public string MotivoAtraso { get; set; }
        public string MotivoAtrasoEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(MotivoAtraso); } }

        public short TipoHistorico { get; set; }
        public string Resolucao { get; set; }
        public string ResolucaoEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(Resolucao); } }
        public string Sintese { get; set; }
        public string SinteseEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(Sintese); } }
        public string Fase { get; set; }
        public string FaseEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(Fase); } }


        public string NomeUsuario { get; set; }
        public DateTime? DataAgenda { get; set; }
        public string DataAgendaDesc { get { return string.Format("{0:dd/MM/yyyy}", DataAgenda); } }
        public string DataCadastroDesc { get { return string.Format("{0:dd/MM/yyyy}", DataCadastro); } }

        public string NumeroContrato { get; set; }
        public string NumeroContratoEncrypted { get { return Db.Rotinas.DbRotinas.Descriptografar(NumeroContrato); } }
        public Decimal? ValorParcela { get; set; }
        public string ValorParcelaDesc { get { return string.Format("{0:C}", ValorParcela); } }
        public string FluxoPagamento { get; set; }


        public Nullable<int> CodFase { get; set; }
        public Nullable<int> DiasAtraso { get; set; }
        public Nullable<int> CodAging { get; set; }
        public Nullable<int> Qtde { get; set; }
        public Nullable<int> QtdContrato { get; set; }
        public Nullable<int> QtdCliente { get; set; }
        public Nullable<Decimal> ValorTotal { get; set; }
        public Nullable<Decimal> ValorPago { get; set; }
        public Nullable<Decimal> ValorAVencer { get; set; }
        public Nullable<Decimal> ValorRecebido { get; set; }
        public Nullable<Decimal> ValorAtualizado { get; set; }
        public Nullable<Decimal> ValorAberto { get; set; }

        public string Aging { get; set; }
        public string CorAging { get; set; }
        public Nullable<int> CodBoleto { get; set; }
        public DateTime DataVencimento { get; set; }
        public Nullable<DateTime> DataFechamento { get; set; }
        public Nullable<DateTime> DataPagamento { get; set; }

        public Nullable<Decimal> ValorMulta { get; set; }
        public Nullable<Decimal> ValorJuros { get; set; }
        public Nullable<Decimal> ValorDocumento { get; set; }
        public Nullable<Decimal> ValorDocumentoPrevisto { get; set; }
        public Nullable<int> CodHistorico { get; set; }
    }
}

