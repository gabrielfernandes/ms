﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models.Pesquisa
{
    public class PropostaDados
    {
        public string NomeEmpreendimento { get; set; }
        public string NomeBloco { get; set; }
        public string NumeroUnidade { get; set; }
        public string NumeroProposta { get; set; }
        public int CodProposta { get; set; }
        public string Cliente { get; set; }
        public DateTime? DataAgenda { get; set; }
        public string DataAgendaDesc { get { return string.Format("{0:dd/MM/yyyy}", DataAgenda); } }
        public string UltimaObs { get; set; }
        public string Login { get; set; }
        public string Sintese { get; set; }
        public string Fase { get; set; }
        public int? CodTipoCobranca { get; set; }
    }
}
