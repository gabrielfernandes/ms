using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaVendaEmpreendimento : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodEmpreendimento { get; set; }
         public Nullable<int> CodTabelaVenda { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaEmpreendimento TabelaEmpreendimento { get; set; }
        public virtual TabelaVenda TabelaVenda { get; set; }
    }
}
