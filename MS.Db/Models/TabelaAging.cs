using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaAging : BaseEntity
    {
        public TabelaAging()
        {
            this.Clientes = new List<Cliente>();
            this.Contratoes = new List<Contrato>();
        }

       //  public int Cod { get; set; }
         public string Aging { get; set; }
         public Nullable<int> AtrasoDe { get; set; }
         public Nullable<int> AtrasoAte { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string Cor { get; set; }
        public virtual ICollection<Cliente> Clientes { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
    }
}
