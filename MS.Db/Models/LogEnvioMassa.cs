using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class LogEnvioMassa : BaseEntity
    {
        public LogEnvioMassa()
        {
            this.LogEmails = new List<LogEmail>();
            this.LogMensagems = new List<LogMensagem>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public Nullable<int> TotalRegistro { get; set; }
         public Nullable<System.DateTime> DataEnvio { get; set; }
         public string Filtro { get; set; }
         public Nullable<int> CodModelo { get; set; }
         public string DescricaoModelo { get; set; }
         public Nullable<short> TipoEnvio { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
        public virtual ICollection<LogEmail> LogEmails { get; set; }
        public virtual ICollection<LogMensagem> LogMensagems { get; set; }
    }
}
