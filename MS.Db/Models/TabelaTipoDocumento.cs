using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoDocumento : BaseEntity
    {
        public TabelaTipoDocumento()
        {
            this.ContratoDocumentoes = new List<ContratoDocumento>();
            this.PropostaDocumentoes = new List<PropostaDocumento>();
            this.TabelaAnaliseDocumentals = new List<TabelaAnaliseDocumental>();
            this.TabelaCheckLists = new List<TabelaCheckList>();
        }

       //  public int Cod { get; set; }
         public string TipoDocumento { get; set; }
         public Nullable<bool> Obrigatorio { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<ContratoDocumento> ContratoDocumentoes { get; set; }
        public virtual ICollection<PropostaDocumento> PropostaDocumentoes { get; set; }
        public virtual ICollection<TabelaAnaliseDocumental> TabelaAnaliseDocumentals { get; set; }
        public virtual ICollection<TabelaCheckList> TabelaCheckLists { get; set; }
    }
}
