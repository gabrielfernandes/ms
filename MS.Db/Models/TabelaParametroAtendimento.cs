using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaParametroAtendimento : BaseEntity
    {
        public TabelaParametroAtendimento()
        {
            this.TabelaParametroAtendimentoUsuarios = new List<TabelaParametroAtendimentoUsuario>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodSintese { get; set; }
         public Nullable<int> CodTipoCobrancaAcao { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string Login { get; set; }
         public string CondicaoQuery { get; set; }
         public string NomeEstrategia { get; set; }
        public virtual TabelaSintese TabelaSintese { get; set; }
        public virtual TabelaTipoCobrancaAcao TabelaTipoCobrancaAcao { get; set; }
        public virtual ICollection<TabelaParametroAtendimentoUsuario> TabelaParametroAtendimentoUsuarios { get; set; }
    }
}
