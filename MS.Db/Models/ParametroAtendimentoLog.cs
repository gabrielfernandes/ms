using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ParametroAtendimentoLog : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodAtendimento { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string Descricao { get; set; }
        public virtual ParametroAtendimento ParametroAtendimento { get; set; }
    }
}
