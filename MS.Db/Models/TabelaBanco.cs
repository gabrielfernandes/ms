using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class TabelaBanco : BaseEntity
    {
        public TabelaBanco()
        {
            this.ProspectInformacaoBancarias = new List<ProspectInformacaoBancaria>();
            this.TabelaBancoPortadors = new List<TabelaBancoPortador>();
            this.TabelaBancoTipoOperacaos = new List<TabelaBancoTipoOperacao>();
            this.TabelaCheckLists = new List<TabelaCheckList>();
            this.TabelaEmpreendimentoes = new List<TabelaEmpreendimento>();
            this.TabelaEmpreendimentoes1 = new List<TabelaEmpreendimento>();
            this.TabelaParametroBoletoes = new List<TabelaParametroBoleto>();
        }

        //  public int Cod { get; set; }
        public string NomeBanco { get; set; }
        public Nullable<int> Numero { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public string CodCliente { get; set; }
        public Nullable<int> NossoNumero { get; set; }
        public Nullable<int> NossoNumeroAtual { get; set; }
        public Nullable<int> Lote { get; set; }
        public Nullable<int> LoteAtual { get; set; }
        public Nullable<bool> BancoPadrao { get; set; }
        public virtual ICollection<ProspectInformacaoBancaria> ProspectInformacaoBancarias { get; set; }
        public virtual ICollection<TabelaBancoPortador> TabelaBancoPortadors { get; set; }
        public virtual ICollection<TabelaBancoTipoOperacao> TabelaBancoTipoOperacaos { get; set; }
        public virtual ICollection<TabelaCheckList> TabelaCheckLists { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes1 { get; set; }
        public virtual ICollection<TabelaParametroBoleto> TabelaParametroBoletoes { get; set; }
    }
}
