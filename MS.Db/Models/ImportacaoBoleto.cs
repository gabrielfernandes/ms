using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ImportacaoBoleto : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodImportacao { get; set; }
         public string Filial { get; set; }
         public string NumeroFatura { get; set; }
         public string NumeroCliente { get; set; }
         public string DataVencimento { get; set; }
         public string ValorFatura { get; set; }
         public string LinhaDigitavel { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<int> CodParcela { get; set; }
         public string Status { get; set; }
         public string Mensagem { get; set; }
         public Nullable<System.DateTime> DataProcessamento { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string DataDocumento { get; set; }
         public string Endereco { get; set; }
         public string Numero { get; set; }
         public string Complemento { get; set; }
         public string Bairro { get; set; }
         public string CEP { get; set; }
         public string Cidade { get; set; }
         public string Estado { get; set; }
        public virtual Importacao Importacao { get; set; }
    }
}
