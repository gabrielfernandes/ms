﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models
{
    [Serializable]
    public abstract class BaseEntity
    {

        public int Cod { get; set; }

        public System.DateTime DataCadastro { get; set; }

        public Nullable<System.DateTime> DataAlteracao { get; set; }

        public Nullable<System.DateTime> DataExcluido { get; set; }


    }
}
