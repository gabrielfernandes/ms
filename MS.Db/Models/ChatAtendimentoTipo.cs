using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ChatAtendimentoTipo : BaseEntity
    {
        public ChatAtendimentoTipo()
        {
            this.ChatAtendimentoes = new List<ChatAtendimento>();
        }

       //  public int Cod { get; set; }
         public string Nome { get; set; }
         public string Descricao { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<ChatAtendimento> ChatAtendimentoes { get; set; }
    }
}
