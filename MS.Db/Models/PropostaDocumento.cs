using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class PropostaDocumento : BaseEntity
    {
        public PropostaDocumento()
        {
            this.PropostaAnaliseDocumentals = new List<PropostaAnaliseDocumental>();
            this.PropostaDocumentoChecklists = new List<PropostaDocumentoChecklist>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodTipoDocumento { get; set; }
         public Nullable<int> CodProposta { get; set; }
         public Nullable<int> CodProspect { get; set; }
         public string ArquivoCaminho { get; set; }
         public string ArquivoNome { get; set; }
         public string ArquivoGuid { get; set; }
         public string ArquivoObservacao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<short> Status { get; set; }
         public Nullable<System.DateTime> DataVigencia { get; set; }
        public virtual Proposta Proposta { get; set; }
        public virtual ICollection<PropostaAnaliseDocumental> PropostaAnaliseDocumentals { get; set; }
        public virtual PropostaProspect PropostaProspect { get; set; }
        public virtual TabelaTipoDocumento TabelaTipoDocumento { get; set; }
        public virtual ICollection<PropostaDocumentoChecklist> PropostaDocumentoChecklists { get; set; }
    }
}
