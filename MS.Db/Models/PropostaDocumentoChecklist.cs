using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class PropostaDocumentoChecklist : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodDocumento { get; set; }
         public Nullable<int> CodChecklist { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<short> StatusAnalise { get; set; }
        public virtual PropostaDocumento PropostaDocumento { get; set; }
        public virtual TabelaCheckList TabelaCheckList { get; set; }
    }
}
