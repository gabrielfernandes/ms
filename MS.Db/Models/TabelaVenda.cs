using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaVenda : BaseEntity
    {
        public TabelaVenda()
        {
            this.Propostas = new List<Proposta>();
            this.TabelaVendaEmpreendimentoes = new List<TabelaVendaEmpreendimento>();
            this.TabelaVendaFluxoes = new List<TabelaVendaFluxo>();
        }

       //  public int Cod { get; set; }
         public string Nome { get; set; }
         public Nullable<System.DateTime> VigenciaDe { get; set; }
         public Nullable<System.DateTime> VigenciaAte { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<decimal> PercentualComissao { get; set; }
         public Nullable<decimal> PercentualMinimoEntrada { get; set; }
        public virtual ICollection<Proposta> Propostas { get; set; }
        public virtual ICollection<TabelaVendaEmpreendimento> TabelaVendaEmpreendimentoes { get; set; }
        public virtual ICollection<TabelaVendaFluxo> TabelaVendaFluxoes { get; set; }
    }
}
