using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaEquipe : BaseEntity
    {
        public TabelaEquipe()
        {
            this.TabelaEquipeUsuarios = new List<TabelaEquipeUsuario>();
            this.TabelaParametroAtendimentoUsuarios = new List<TabelaParametroAtendimentoUsuario>();
        }

       //  public int Cod { get; set; }
         public string Equipe { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<TabelaEquipeUsuario> TabelaEquipeUsuarios { get; set; }
        public virtual ICollection<TabelaParametroAtendimentoUsuario> TabelaParametroAtendimentoUsuarios { get; set; }
    }
}
