using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class GrupoCliente : BaseEntity
    {
        public GrupoCliente()
        {
            this.GrupoClienteCNPJs = new List<GrupoClienteCNPJ>();
        }

       //  public int Cod { get; set; }
         public string NomeGrupo { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<GrupoClienteCNPJ> GrupoClienteCNPJs { get; set; }
    }
}
