using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ProspectInformacaoPatrimonial : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodProspect { get; set; }
         public Nullable<short> Tipo { get; set; }
         public Nullable<short> TipoPatrimonio { get; set; }
         public Nullable<decimal> ValorMercado { get; set; }
         public Nullable<short> Situacao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> PrazoRestante { get; set; }
         public Nullable<decimal> ValorParcela { get; set; }
        public virtual PropostaProspect PropostaProspect { get; set; }
    }
}
