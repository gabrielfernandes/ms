using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Cobranca.Db.Models.Mapping;

namespace Cobranca.Db.Models
{
    public partial class otis_cobflowContext : DbContext
    {
        static otis_cobflowContext()
        {
            Database.SetInitializer<otis_cobflowContext>(null);
        }

        public otis_cobflowContext()
            : base("Name=otis_cobflowContext")
        {
        }

        public DbSet<Acordo> Acordoes { get; set; }
        public DbSet<AcordoContratoParcela> AcordoContratoParcelas { get; set; }
        public DbSet<AcordoParcela> AcordoParcelas { get; set; }
        public DbSet<ChatAtendimento> ChatAtendimentoes { get; set; }
        public DbSet<ChatAtendimentoMensagem> ChatAtendimentoMensagems { get; set; }
        public DbSet<ChatAtendimentoResultado> ChatAtendimentoResultadoes { get; set; }
        public DbSet<ChatAtendimentoTipo> ChatAtendimentoTipoes { get; set; }
        public DbSet<ChatAtentimentoHistorico> ChatAtentimentoHistoricoes { get; set; }
        public DbSet<ChatMensagensPronta> ChatMensagensProntas { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<ClienteAtendimento> ClienteAtendimentoes { get; set; }
        public DbSet<ClienteContato> ClienteContatoes { get; set; }
        public DbSet<ClienteContatoEmpresarial> ClienteContatoEmpresarials { get; set; }
        public DbSet<ClienteDocumento> ClienteDocumentoes { get; set; }
        public DbSet<ClienteEndereco> ClienteEnderecoes { get; set; }
        public DbSet<ClienteHistorico> ClienteHistoricoes { get; set; }
        public DbSet<ClienteParcelaCredito> ClienteParcelaCreditoes { get; set; }
        public DbSet<Contrato> Contratoes { get; set; }
        public DbSet<ContratoAcaoCobrancaHistorico> ContratoAcaoCobrancaHistoricoes { get; set; }
        public DbSet<ContratoAcordo> ContratoAcordoes { get; set; }
        public DbSet<ContratoAvalista> ContratoAvalistas { get; set; }
        public DbSet<ContratoCliente> ContratoClientes { get; set; }
        public DbSet<ContratoDespesa> ContratoDespesas { get; set; }
        public DbSet<ContratoDocumento> ContratoDocumentoes { get; set; }
        public DbSet<ContratoEscritorioHistorico> ContratoEscritorioHistoricoes { get; set; }
        public DbSet<ContratoHistorico> ContratoHistoricoes { get; set; }
        public DbSet<ContratoParcela> ContratoParcelas { get; set; }
        public DbSet<ContratoParcelaAcordo> ContratoParcelaAcordoes { get; set; }
        public DbSet<ContratoParcelaBoleto> ContratoParcelaBoletoes { get; set; }
        public DbSet<ContratoRecebimento> ContratoRecebimentoes { get; set; }
        public DbSet<ContratoTipoCobrancaHistorico> ContratoTipoCobrancaHistoricoes { get; set; }
        public DbSet<Historico> Historicoes { get; set; }
        public DbSet<Importacao> Importacaos { get; set; }
        public DbSet<ImportacaoBoleto> ImportacaoBoletoes { get; set; }
        public DbSet<ImportacaoCliente> ImportacaoClientes { get; set; }
        public DbSet<ImportacaoCredito> ImportacaoCreditoes { get; set; }
        public DbSet<ImportacaoLayout> ImportacaoLayouts { get; set; }
        public DbSet<ImportacaoParcela> ImportacaoParcelas { get; set; }
        public DbSet<LogEmail> LogEmails { get; set; }
        public DbSet<ParametroAtendimento> ParametroAtendimentoes { get; set; }
        public DbSet<ParametroAtendimentoLog> ParametroAtendimentoLogs { get; set; }
        public DbSet<ParametroAtendimentoProduto> ParametroAtendimentoProdutoes { get; set; }
        public DbSet<ParametroAtendimentoRota> ParametroAtendimentoRotas { get; set; }
        public DbSet<ParametroPlataforma> ParametroPlataformas { get; set; }
        public DbSet<Proposta> Propostas { get; set; }
        public DbSet<PropostaAnaliseDocumental> PropostaAnaliseDocumentals { get; set; }
        public DbSet<PropostaDocumento> PropostaDocumentoes { get; set; }
        public DbSet<PropostaDocumentoChecklist> PropostaDocumentoChecklists { get; set; }
        public DbSet<PropostaHistorico> PropostaHistoricoes { get; set; }
        public DbSet<PropostaProspect> PropostaProspects { get; set; }
        public DbSet<PropostaProspectHistorico> PropostaProspectHistoricoes { get; set; }
        public DbSet<PropostaStatusHistorico> PropostaStatusHistoricoes { get; set; }
        public DbSet<PropostaValore> PropostaValores { get; set; }
        public DbSet<PropostaValoresPadrao> PropostaValoresPadraos { get; set; }
        public DbSet<ProspectCompromissoFinanceiro> ProspectCompromissoFinanceiroes { get; set; }
        public DbSet<ProspectFGT> ProspectFGTS { get; set; }
        public DbSet<ProspectInformacaoBancaria> ProspectInformacaoBancarias { get; set; }
        public DbSet<ProspectInformacaoPatrimonial> ProspectInformacaoPatrimonials { get; set; }
        public DbSet<ProspectReferencia> ProspectReferencias { get; set; }
        public DbSet<ProspectRenda> ProspectRendas { get; set; }
        public DbSet<Rota> Rotas { get; set; }
        public DbSet<RotaRegional> RotaRegionals { get; set; }
        public DbSet<sysdiagram> sysdiagrams { get; set; }
        public DbSet<TabelaAcaoCobranca> TabelaAcaoCobrancas { get; set; }
        public DbSet<TabelaAging> TabelaAgings { get; set; }
        public DbSet<TabelaAnaliseDocumental> TabelaAnaliseDocumentals { get; set; }
        public DbSet<TabelaAnaliseDocumentalAlternativa> TabelaAnaliseDocumentalAlternativas { get; set; }
        public DbSet<TabelaBanco> TabelaBancoes { get; set; }
        public DbSet<TabelaBancoPortador> TabelaBancoPortadors { get; set; }
        public DbSet<TabelaBancoTipoOperacao> TabelaBancoTipoOperacaos { get; set; }
        public DbSet<TabelaBancoTipoOperacaoEmpreend> TabelaBancoTipoOperacaoEmpreends { get; set; }
        public DbSet<TabelaBloco> TabelaBlocoes { get; set; }
        public DbSet<TabelaCampanha> TabelaCampanhas { get; set; }
        public DbSet<TabelaCampanhaAcao> TabelaCampanhaAcaos { get; set; }
        public DbSet<TabelaCampanhaCondicao> TabelaCampanhaCondicaos { get; set; }
        public DbSet<TabelaCampanhaEmail> TabelaCampanhaEmails { get; set; }
        public DbSet<TabelaCheckList> TabelaCheckLists { get; set; }
        public DbSet<TabelaCheckListNome> TabelaCheckListNomes { get; set; }
        public DbSet<TabelaConstrutora> TabelaConstrutoras { get; set; }
        public DbSet<TabelaDia> TabelaDias { get; set; }
        public DbSet<TabelaEmpreendimento> TabelaEmpreendimentoes { get; set; }
        public DbSet<TabelaEmpreendimentoDocumento> TabelaEmpreendimentoDocumentoes { get; set; }
        public DbSet<TabelaEmpreendimentoDocumentoChecklist> TabelaEmpreendimentoDocumentoChecklists { get; set; }
        public DbSet<TabelaEmpreendimentoFoto> TabelaEmpreendimentoFotoes { get; set; }
        public DbSet<TabelaEmpresa> TabelaEmpresas { get; set; }
        public DbSet<TabelaEmpresaVenda> TabelaEmpresaVendas { get; set; }
        public DbSet<TabelaEmpresaVendaUnidade> TabelaEmpresaVendaUnidades { get; set; }
        public DbSet<TabelaEndereco> TabelaEnderecoes { get; set; }
        public DbSet<TabelaEquipe> TabelaEquipes { get; set; }
        public DbSet<TabelaEquipeUsuario> TabelaEquipeUsuarios { get; set; }
        public DbSet<TabelaEscritorio> TabelaEscritorios { get; set; }
        public DbSet<TabelaEstado> TabelaEstadoes { get; set; }
        public DbSet<TabelaFase> TabelaFases { get; set; }
        public DbSet<TabelaGestao> TabelaGestaos { get; set; }
        public DbSet<TabelaGrupo> TabelaGrupoes { get; set; }
        public DbSet<TabelaGrupoCliente> TabelaGrupoClientes { get; set; }
        public DbSet<TabelaIdioma> TabelaIdiomas { get; set; }
        public DbSet<TabelaModeloAcao> TabelaModeloAcaos { get; set; }
        public DbSet<TabelaMotivoAtraso> TabelaMotivoAtrasoes { get; set; }
        public DbSet<TabelaMuralAviso> TabelaMuralAvisoes { get; set; }
        public DbSet<TabelaMuralAvisoDestinatario> TabelaMuralAvisoDestinatarios { get; set; }
        public DbSet<TabelaNatureza> TabelaNaturezas { get; set; }
        public DbSet<TabelaParametroAtendimento> TabelaParametroAtendimentoes { get; set; }
        public DbSet<TabelaParametroAtendimentoUsuario> TabelaParametroAtendimentoUsuarios { get; set; }
        public DbSet<TabelaParametroBoleto> TabelaParametroBoletoes { get; set; }
        public DbSet<TabelaParametroTaxa> TabelaParametroTaxas { get; set; }
        public DbSet<TabelaProduto> TabelaProdutoes { get; set; }
        public DbSet<TabelaRegional> TabelaRegionals { get; set; }
        public DbSet<TabelaRegionalProduto> TabelaRegionalProdutoes { get; set; }
        public DbSet<TabelaResolucao> TabelaResolucaos { get; set; }
        public DbSet<TabelaSintese> TabelaSintese { get; set; }
        public DbSet<TabelaTipoBloqueio> TabelaTipoBloqueios { get; set; }
        public DbSet<TabelaTipoCarteira> TabelaTipoCarteiras { get; set; }
        public DbSet<TabelaTipoCobranca> TabelaTipoCobrancas { get; set; }
        public DbSet<TabelaTipoCobrancaAcao> TabelaTipoCobrancaAcaos { get; set; }
        public DbSet<TabelaTipoCobrancaAcaoCondicao> TabelaTipoCobrancaAcaoCondicaos { get; set; }
        public DbSet<TabelaTipoCobrancaCondicao> TabelaTipoCobrancaCondicaos { get; set; }
        public DbSet<TabelaTipoCobrancaLogProcessamento> TabelaTipoCobrancaLogProcessamentoes { get; set; }
        public DbSet<TabelaTipoCobrancaPosicao> TabelaTipoCobrancaPosicaos { get; set; }
        public DbSet<TabelaTipoDespesa> TabelaTipoDespesas { get; set; }
        public DbSet<TabelaTipoDocumento> TabelaTipoDocumentoes { get; set; }
        public DbSet<TabelaTipoEmpreendimentoFoto> TabelaTipoEmpreendimentoFotoes { get; set; }
        public DbSet<TabelaTipoImovel> TabelaTipoImovels { get; set; }
        public DbSet<TabelaTipoParcela> TabelaTipoParcelas { get; set; }
        public DbSet<TabelaTipoUnidade> TabelaTipoUnidades { get; set; }
        public DbSet<TabelaUnidade> TabelaUnidades { get; set; }
        public DbSet<TabelaUnidadeReserva> TabelaUnidadeReservas { get; set; }
        public DbSet<TabelaUnidadeStatusHistorico> TabelaUnidadeStatusHistoricoes { get; set; }
        public DbSet<TabelaVenda> TabelaVendas { get; set; }
        public DbSet<TabelaVendaEmpreendimento> TabelaVendaEmpreendimentoes { get; set; }
        public DbSet<TabelaVendaFluxo> TabelaVendaFluxoes { get; set; }
        public DbSet<temp_importacao_credito> temp_importacao_credito { get; set; }
        public DbSet<TempImportacao> TempImportacaos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<UsuarioAcessoLog> UsuarioAcessoLogs { get; set; }
        public DbSet<UsuarioRegional> UsuarioRegionals { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AcordoMap());
            modelBuilder.Configurations.Add(new AcordoContratoParcelaMap());
            modelBuilder.Configurations.Add(new AcordoParcelaMap());
            modelBuilder.Configurations.Add(new ChatAtendimentoMap());
            modelBuilder.Configurations.Add(new ChatAtendimentoMensagemMap());
            modelBuilder.Configurations.Add(new ChatAtendimentoResultadoMap());
            modelBuilder.Configurations.Add(new ChatAtendimentoTipoMap());
            modelBuilder.Configurations.Add(new ChatAtentimentoHistoricoMap());
            modelBuilder.Configurations.Add(new ChatMensagensProntaMap());
            modelBuilder.Configurations.Add(new ClienteMap());
            modelBuilder.Configurations.Add(new ClienteAtendimentoMap());
            modelBuilder.Configurations.Add(new ClienteContatoMap());
            modelBuilder.Configurations.Add(new ClienteContatoEmpresarialMap());
            modelBuilder.Configurations.Add(new ClienteDocumentoMap());
            modelBuilder.Configurations.Add(new ClienteEnderecoMap());
            modelBuilder.Configurations.Add(new ClienteHistoricoMap());
            modelBuilder.Configurations.Add(new ClienteParcelaCreditoMap());
            modelBuilder.Configurations.Add(new ContratoMap());
            modelBuilder.Configurations.Add(new ContratoAcaoCobrancaHistoricoMap());
            modelBuilder.Configurations.Add(new ContratoAcordoMap());
            modelBuilder.Configurations.Add(new ContratoAvalistaMap());
            modelBuilder.Configurations.Add(new ContratoClienteMap());
            modelBuilder.Configurations.Add(new ContratoDespesaMap());
            modelBuilder.Configurations.Add(new ContratoDocumentoMap());
            modelBuilder.Configurations.Add(new ContratoEscritorioHistoricoMap());
            modelBuilder.Configurations.Add(new ContratoHistoricoMap());
            modelBuilder.Configurations.Add(new ContratoParcelaMap());
            modelBuilder.Configurations.Add(new ContratoParcelaAcordoMap());
            modelBuilder.Configurations.Add(new ContratoParcelaBoletoMap());
            modelBuilder.Configurations.Add(new ContratoRecebimentoMap());
            modelBuilder.Configurations.Add(new ContratoTipoCobrancaHistoricoMap());
            modelBuilder.Configurations.Add(new HistoricoMap());
            modelBuilder.Configurations.Add(new ImportacaoMap());
            modelBuilder.Configurations.Add(new ImportacaoBoletoMap());
            modelBuilder.Configurations.Add(new ImportacaoClienteMap());
            modelBuilder.Configurations.Add(new ImportacaoCreditoMap());
            modelBuilder.Configurations.Add(new ImportacaoLayoutMap());
            modelBuilder.Configurations.Add(new ImportacaoParcelaMap());
            modelBuilder.Configurations.Add(new LogEmailMap());
            modelBuilder.Configurations.Add(new ParametroAtendimentoMap());
            modelBuilder.Configurations.Add(new ParametroAtendimentoLogMap());
            modelBuilder.Configurations.Add(new ParametroAtendimentoProdutoMap());
            modelBuilder.Configurations.Add(new ParametroAtendimentoRotaMap());
            modelBuilder.Configurations.Add(new ParametroPlataformaMap());
            modelBuilder.Configurations.Add(new PropostaMap());
            modelBuilder.Configurations.Add(new PropostaAnaliseDocumentalMap());
            modelBuilder.Configurations.Add(new PropostaDocumentoMap());
            modelBuilder.Configurations.Add(new PropostaDocumentoChecklistMap());
            modelBuilder.Configurations.Add(new PropostaHistoricoMap());
            modelBuilder.Configurations.Add(new PropostaProspectMap());
            modelBuilder.Configurations.Add(new PropostaProspectHistoricoMap());
            modelBuilder.Configurations.Add(new PropostaStatusHistoricoMap());
            modelBuilder.Configurations.Add(new PropostaValoreMap());
            modelBuilder.Configurations.Add(new PropostaValoresPadraoMap());
            modelBuilder.Configurations.Add(new ProspectCompromissoFinanceiroMap());
            modelBuilder.Configurations.Add(new ProspectFGTMap());
            modelBuilder.Configurations.Add(new ProspectInformacaoBancariaMap());
            modelBuilder.Configurations.Add(new ProspectInformacaoPatrimonialMap());
            modelBuilder.Configurations.Add(new ProspectReferenciaMap());
            modelBuilder.Configurations.Add(new ProspectRendaMap());
            modelBuilder.Configurations.Add(new RotaMap());
            modelBuilder.Configurations.Add(new RotaRegionalMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new TabelaAcaoCobrancaMap());
            modelBuilder.Configurations.Add(new TabelaAgingMap());
            modelBuilder.Configurations.Add(new TabelaAnaliseDocumentalMap());
            modelBuilder.Configurations.Add(new TabelaAnaliseDocumentalAlternativaMap());
            modelBuilder.Configurations.Add(new TabelaBancoMap());
            modelBuilder.Configurations.Add(new TabelaBancoPortadorMap());
            modelBuilder.Configurations.Add(new TabelaBancoTipoOperacaoMap());
            modelBuilder.Configurations.Add(new TabelaBancoTipoOperacaoEmpreendMap());
            modelBuilder.Configurations.Add(new TabelaBlocoMap());
            modelBuilder.Configurations.Add(new TabelaCampanhaMap());
            modelBuilder.Configurations.Add(new TabelaCampanhaAcaoMap());
            modelBuilder.Configurations.Add(new TabelaCampanhaCondicaoMap());
            modelBuilder.Configurations.Add(new TabelaCampanhaEmailMap());
            modelBuilder.Configurations.Add(new TabelaCheckListMap());
            modelBuilder.Configurations.Add(new TabelaCheckListNomeMap());
            modelBuilder.Configurations.Add(new TabelaConstrutoraMap());
            modelBuilder.Configurations.Add(new TabelaDiaMap());
            modelBuilder.Configurations.Add(new TabelaEmpreendimentoMap());
            modelBuilder.Configurations.Add(new TabelaEmpreendimentoDocumentoMap());
            modelBuilder.Configurations.Add(new TabelaEmpreendimentoDocumentoChecklistMap());
            modelBuilder.Configurations.Add(new TabelaEmpreendimentoFotoMap());
            modelBuilder.Configurations.Add(new TabelaEmpresaMap());
            modelBuilder.Configurations.Add(new TabelaEmpresaVendaMap());
            modelBuilder.Configurations.Add(new TabelaEmpresaVendaUnidadeMap());
            modelBuilder.Configurations.Add(new TabelaEnderecoMap());
            modelBuilder.Configurations.Add(new TabelaEquipeMap());
            modelBuilder.Configurations.Add(new TabelaEquipeUsuarioMap());
            modelBuilder.Configurations.Add(new TabelaEscritorioMap());
            modelBuilder.Configurations.Add(new TabelaEstadoMap());
            modelBuilder.Configurations.Add(new TabelaFaseMap());
            modelBuilder.Configurations.Add(new TabelaGestaoMap());
            modelBuilder.Configurations.Add(new TabelaGrupoMap());
            modelBuilder.Configurations.Add(new TabelaGrupoClienteMap());
            modelBuilder.Configurations.Add(new TabelaIdiomaMap());
            modelBuilder.Configurations.Add(new TabelaModeloAcaoMap());
            modelBuilder.Configurations.Add(new TabelaMotivoAtrasoMap());
            modelBuilder.Configurations.Add(new TabelaMuralAvisoMap());
            modelBuilder.Configurations.Add(new TabelaMuralAvisoDestinatarioMap());
            modelBuilder.Configurations.Add(new TabelaNaturezaMap());
            modelBuilder.Configurations.Add(new TabelaParametroAtendimentoMap());
            modelBuilder.Configurations.Add(new TabelaParametroAtendimentoUsuarioMap());
            modelBuilder.Configurations.Add(new TabelaParametroBoletoMap());
            modelBuilder.Configurations.Add(new TabelaParametroTaxaMap());
            modelBuilder.Configurations.Add(new TabelaProdutoMap());
            modelBuilder.Configurations.Add(new TabelaRegionalMap());
            modelBuilder.Configurations.Add(new TabelaRegionalProdutoMap());
            modelBuilder.Configurations.Add(new TabelaResolucaoMap());
            modelBuilder.Configurations.Add(new TabelaSinteseMap());
            modelBuilder.Configurations.Add(new TabelaTipoBloqueioMap());
            modelBuilder.Configurations.Add(new TabelaTipoCarteiraMap());
            modelBuilder.Configurations.Add(new TabelaTipoCobrancaMap());
            modelBuilder.Configurations.Add(new TabelaTipoCobrancaAcaoMap());
            modelBuilder.Configurations.Add(new TabelaTipoCobrancaAcaoCondicaoMap());
            modelBuilder.Configurations.Add(new TabelaTipoCobrancaCondicaoMap());
            modelBuilder.Configurations.Add(new TabelaTipoCobrancaLogProcessamentoMap());
            modelBuilder.Configurations.Add(new TabelaTipoCobrancaPosicaoMap());
            modelBuilder.Configurations.Add(new TabelaTipoDespesaMap());
            modelBuilder.Configurations.Add(new TabelaTipoDocumentoMap());
            modelBuilder.Configurations.Add(new TabelaTipoEmpreendimentoFotoMap());
            modelBuilder.Configurations.Add(new TabelaTipoImovelMap());
            modelBuilder.Configurations.Add(new TabelaTipoParcelaMap());
            modelBuilder.Configurations.Add(new TabelaTipoUnidadeMap());
            modelBuilder.Configurations.Add(new TabelaUnidadeMap());
            modelBuilder.Configurations.Add(new TabelaUnidadeReservaMap());
            modelBuilder.Configurations.Add(new TabelaUnidadeStatusHistoricoMap());
            modelBuilder.Configurations.Add(new TabelaVendaMap());
            modelBuilder.Configurations.Add(new TabelaVendaEmpreendimentoMap());
            modelBuilder.Configurations.Add(new TabelaVendaFluxoMap());
            modelBuilder.Configurations.Add(new temp_importacao_creditoMap());
            modelBuilder.Configurations.Add(new TempImportacaoMap());
            modelBuilder.Configurations.Add(new UsuarioMap());
            modelBuilder.Configurations.Add(new UsuarioAcessoLogMap());
            modelBuilder.Configurations.Add(new UsuarioRegionalMap());
        }
    }
}
