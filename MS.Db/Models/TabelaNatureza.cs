using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaNatureza : BaseEntity
    {
        public TabelaNatureza()
        {
            this.Contratoes = new List<Contrato>();
            this.ContratoParcelas = new List<ContratoParcela>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodTipoCarteira { get; set; }
         public Nullable<int> CodNaturezaCliente { get; set; }
         public string Natureza { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual ICollection<ContratoParcela> ContratoParcelas { get; set; }
        public virtual TabelaTipoCarteira TabelaTipoCarteira { get; set; }
    }
}
