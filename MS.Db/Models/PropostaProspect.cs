using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cobranca.Db.Models
{
    public partial class PropostaProspect : BaseEntity
    {
        public PropostaProspect()
        {
            this.PropostaDocumentoes = new List<PropostaDocumento>();
            this.ProspectCompromissoFinanceiroes = new List<ProspectCompromissoFinanceiro>();
            this.ProspectInformacaoPatrimonials = new List<ProspectInformacaoPatrimonial>();
            this.ProspectRendas = new List<ProspectRenda>();
            this.PropostaProspect1 = new List<PropostaProspect>();
            this.PropostaProspectHistoricoes = new List<PropostaProspectHistorico>();
            this.ProspectFGTS = new List<ProspectFGT>();
            this.ProspectInformacaoBancarias = new List<ProspectInformacaoBancaria>();
            this.ProspectReferencias = new List<ProspectReferencia>();
        }


        //  public int Cod { get; set; }
        public Nullable<int> CodProposta { get; set; }
        public string Nome { get; set; }
        public string PrimeiroNome { get; set; }
        public string CPF { get; set; }
        public Nullable<short> TipoDocumento { get; set; }
        public string RG { get; set; }
        public string CNPJ { get; set; }
        public Nullable<short> EstadoCivil { get; set; }
        public string Profissao { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DataNascimento { get; set; }
        public string Nacionalidade { get; set; }
        public string Naturalidade { get; set; }
        public string Orgao { get; set; }
        public string RegimeCasamento { get; set; }
        public Nullable<System.DateTime> DataCasamento { get; set; }
        public Nullable<short> Sexo { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<System.DateTime> DataEmissao { get; set; }
        public string TelefoneResidencial { get; set; }
        public string TelefoneComercial { get; set; }
        public string Celular { get; set; }
        public string CelularComercial { get; set; }
        public string CEP { get; set; }
        public string Endereco { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Bairro { get; set; }
        public string UF { get; set; }
        public string EnderecoNumero { get; set; }
        public string EnderecoComplemento { get; set; }
        public Nullable<short> Tipo { get; set; }
        public string NomePai { get; set; }
        public string NomeMae { get; set; }
        public Nullable<int> NumeroDependente { get; set; }
        public string Cartorio { get; set; }
        public Nullable<short> CategoriaProfissional { get; set; }
        public Nullable<int> CodConjuge { get; set; }
        public Nullable<decimal> FGTS { get; set; }
        public Nullable<decimal> CartaoCredito { get; set; }
        public Nullable<decimal> AplicacaoFinanceira { get; set; }
        public Nullable<decimal> Aluguel { get; set; }
        public Nullable<short> Escolaridade { get; set; }
        public Nullable<int> Ordem { get; set; }
        public Nullable<short> TipoComprador { get; set; }
        public virtual Proposta Proposta { get; set; }
        public virtual ICollection<PropostaDocumento> PropostaDocumentoes { get; set; }
        public virtual ICollection<ProspectCompromissoFinanceiro> ProspectCompromissoFinanceiroes { get; set; }
        public virtual ICollection<ProspectInformacaoPatrimonial> ProspectInformacaoPatrimonials { get; set; }
        public virtual ICollection<ProspectRenda> ProspectRendas { get; set; }
        public virtual ICollection<PropostaProspect> PropostaProspect1 { get; set; }
        public virtual PropostaProspect PropostaProspect2 { get; set; }
        public virtual ICollection<PropostaProspectHistorico> PropostaProspectHistoricoes { get; set; }
        public virtual ICollection<ProspectFGT> ProspectFGTS { get; set; }
        public virtual ICollection<ProspectInformacaoBancaria> ProspectInformacaoBancarias { get; set; }
        public virtual ICollection<ProspectReferencia> ProspectReferencias { get; set; }
    }
}
