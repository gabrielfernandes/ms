using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaBancoTipoOperacaoEmpreend : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodBancoTipoOperacao { get; set; }
         public Nullable<decimal> TaxaJuros { get; set; }
         public Nullable<int> CodEmpreend { get; set; }
        public virtual TabelaBancoTipoOperacao TabelaBancoTipoOperacao { get; set; }
        public virtual TabelaEmpreendimento TabelaEmpreendimento { get; set; }
    }
}
