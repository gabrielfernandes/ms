using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaAcaoCobrancaMap : EntityTypeConfiguration<TabelaAcaoCobranca>
    {
        public TabelaAcaoCobrancaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.AcaoCobranca)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaAcaoCobranca");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.AcaoCobranca).HasColumnName("AcaoCobranca");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
        }
    }
}
