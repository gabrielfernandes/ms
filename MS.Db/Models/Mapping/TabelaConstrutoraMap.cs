using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaConstrutoraMap : EntityTypeConfiguration<TabelaConstrutora>
    {
        public TabelaConstrutoraMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeConstrutora)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.CNPJ)
                .HasMaxLength(50);

            this.Property(t => t.InscricaoEstadual)
                .HasMaxLength(50);

            this.Property(t => t.Login)
                .HasMaxLength(20);

            this.Property(t => t.senha)
                .HasMaxLength(10);

            this.Property(t => t.CaminhoSite)
                .HasMaxLength(50);

            this.Property(t => t.CodCliente)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaConstrutora");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodEndereco).HasColumnName("CodEndereco");
            this.Property(t => t.NomeConstrutora).HasColumnName("NomeConstrutora");
            this.Property(t => t.CNPJ).HasColumnName("CNPJ");
            this.Property(t => t.InscricaoEstadual).HasColumnName("InscricaoEstadual");
            this.Property(t => t.Login).HasColumnName("Login");
            this.Property(t => t.senha).HasColumnName("senha");
            this.Property(t => t.CaminhoSite).HasColumnName("CaminhoSite");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");

            // Relationships
            this.HasOptional(t => t.TabelaEndereco)
                .WithMany(t => t.TabelaConstrutoras)
                .HasForeignKey(d => d.CodEndereco);

        }
    }
}
