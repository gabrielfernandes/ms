using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaCheckListMap : EntityTypeConfiguration<TabelaCheckList>
    {
        public TabelaCheckListMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Documento)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaCheckList");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCheckListNome).HasColumnName("CodCheckListNome");
            this.Property(t => t.CodTipoDocumento).HasColumnName("CodTipoDocumento");
            this.Property(t => t.CodBanco).HasColumnName("CodBanco");
            this.Property(t => t.Documento).HasColumnName("Documento");
            this.Property(t => t.EstadoCivil).HasColumnName("EstadoCivil");
            this.Property(t => t.CategoriaProfissional).HasColumnName("CategoriaProfissional");
            this.Property(t => t.Fgts).HasColumnName("Fgts");
            this.Property(t => t.CartaoCredito).HasColumnName("CartaoCredito");
            this.Property(t => t.AplicacaoFinanceira).HasColumnName("AplicacaoFinanceira");
            this.Property(t => t.Aluguel).HasColumnName("Aluguel");
            this.Property(t => t.Obrigatorio).HasColumnName("Obrigatorio");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaBanco)
                .WithMany(t => t.TabelaCheckLists)
                .HasForeignKey(d => d.CodBanco);
            this.HasRequired(t => t.TabelaCheckListNome)
                .WithMany(t => t.TabelaCheckLists)
                .HasForeignKey(d => d.CodCheckListNome);
            this.HasOptional(t => t.TabelaTipoDocumento)
                .WithMany(t => t.TabelaCheckLists)
                .HasForeignKey(d => d.CodTipoDocumento);

        }
    }
}
