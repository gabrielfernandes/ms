using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class UsuarioRegionalMap : EntityTypeConfiguration<UsuarioRegional>
    {
        public UsuarioRegionalMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("UsuarioRegional");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.CodRegional).HasColumnName("CodRegional");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasRequired(t => t.TabelaRegional)
                .WithMany(t => t.UsuarioRegionals)
                .HasForeignKey(d => d.CodRegional);
            this.HasRequired(t => t.Usuario)
                .WithMany(t => t.UsuarioRegionals)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
