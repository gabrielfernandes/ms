using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoParcelaBoletoHistoricoMap : EntityTypeConfiguration<ContratoParcelaBoletoHistorico>
    {
        public ContratoParcelaBoletoHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContratoParcelaBoletoHistorico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodSintese).HasColumnName("CodSintese");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.CodMotivoAtraso).HasColumnName("CodMotivoAtraso");
            this.Property(t => t.CodResolucao).HasColumnName("CodResolucao");
            this.Property(t => t.DataAgenda).HasColumnName("DataAgenda");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.CodBoleto).HasColumnName("CodBoleto");

            // Relationships
            this.HasOptional(t => t.ContratoParcelaBoleto)
                .WithMany(t => t.ContratoParcelaBoletoHistoricoes)
                .HasForeignKey(d => d.CodBoleto);
            this.HasOptional(t => t.TabelaSintese)
                .WithMany(t => t.ContratoParcelaBoletoHistoricoes)
                .HasForeignKey(d => d.CodSintese);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ContratoParcelaBoletoHistoricoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
