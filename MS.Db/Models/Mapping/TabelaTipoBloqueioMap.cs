using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoBloqueioMap : EntityTypeConfiguration<TabelaTipoBloqueio>
    {
        public TabelaTipoBloqueioMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TipoBloqueio)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaTipoBloqueio");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.TipoBloqueio).HasColumnName("TipoBloqueio");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
