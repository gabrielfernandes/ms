using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoUnidadeMap : EntityTypeConfiguration<TabelaTipoUnidade>
    {
        public TabelaTipoUnidadeMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TipoUnidade)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TabelaTipoUnidade");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.TipoUnidade).HasColumnName("TipoUnidade");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
