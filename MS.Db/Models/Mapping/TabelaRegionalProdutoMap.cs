using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaRegionalProdutoMap : EntityTypeConfiguration<TabelaRegionalProduto>
    {
        public TabelaRegionalProdutoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaRegionalProduto");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodRegional).HasColumnName("CodRegional");
            this.Property(t => t.CodProduto).HasColumnName("CodProduto");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaProduto)
                .WithMany(t => t.TabelaRegionalProdutoes)
                .HasForeignKey(d => d.CodProduto);
            this.HasOptional(t => t.TabelaRegional)
                .WithMany(t => t.TabelaRegionalProdutoes)
                .HasForeignKey(d => d.CodRegional);

        }
    }
}
