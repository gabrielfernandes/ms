using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEnderecoMap : EntityTypeConfiguration<TabelaEndereco>
    {
        public TabelaEnderecoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Logradouro)
                .HasMaxLength(100);

            this.Property(t => t.Endereco)
                .HasMaxLength(255);

            this.Property(t => t.Numero)
                .HasMaxLength(100);

            this.Property(t => t.Complemento)
                .HasMaxLength(255);

            this.Property(t => t.Bairro)
                .HasMaxLength(255);

            this.Property(t => t.Cidade)
                .HasMaxLength(255);

            this.Property(t => t.Estado)
                .HasMaxLength(255);

            this.Property(t => t.UF)
                .HasMaxLength(2);

            this.Property(t => t.CEP)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaEndereco");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Logradouro).HasColumnName("Logradouro");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.Numero).HasColumnName("Numero");
            this.Property(t => t.Complemento).HasColumnName("Complemento");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.Estado).HasColumnName("Estado");
            this.Property(t => t.UF).HasColumnName("UF");
            this.Property(t => t.CEP).HasColumnName("CEP");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.DataImportacao).HasColumnName("DataImportacao");
        }
    }
}
