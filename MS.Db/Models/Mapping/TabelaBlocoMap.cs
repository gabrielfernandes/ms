using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaBlocoMap : EntityTypeConfiguration<TabelaBloco>
    {
        public TabelaBlocoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CodBlocoCliente)
                .HasMaxLength(50);

            this.Property(t => t.NomeBloco)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaBloco");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodEmpreend).HasColumnName("CodEmpreend");
            this.Property(t => t.CodBlocoCliente).HasColumnName("CodBlocoCliente");
            this.Property(t => t.NomeBloco).HasColumnName("NomeBloco");
            this.Property(t => t.QuantidadeUnidade).HasColumnName("QuantidadeUnidade");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataHabitese).HasColumnName("DataHabitese");
            this.Property(t => t.DataPromessaEntrega).HasColumnName("DataPromessaEntrega");

            // Relationships
            this.HasRequired(t => t.TabelaEmpreendimento)
                .WithMany(t => t.TabelaBlocoes)
                .HasForeignKey(d => d.CodEmpreend);

        }
    }
}
