using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class HistoricoMap : EntityTypeConfiguration<Historico>
    {
        public HistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("Historico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.CodParcela).HasColumnName("CodParcela");
            this.Property(t => t.CodSintese).HasColumnName("CodSintese");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.CodMotivoAtraso).HasColumnName("CodMotivoAtraso");
            this.Property(t => t.CodResolucao).HasColumnName("CodResolucao");
            this.Property(t => t.DataAgenda).HasColumnName("DataAgenda");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.CodAcaoCobranca).HasColumnName("CodAcaoCobranca");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.Historicoes)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.Historicoes)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.ContratoParcela)
                .WithMany(t => t.Historicoes)
                .HasForeignKey(d => d.CodParcela);
            this.HasOptional(t => t.ContratoAcaoCobrancaHistorico)
                .WithMany(t => t.Historicoes)
                .HasForeignKey(d => d.CodAcaoCobranca);
            this.HasOptional(t => t.TabelaSintese)
                .WithMany(t => t.Historicoes)
                .HasForeignKey(d => d.CodSintese);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.Historicoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
