using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoAcaoCobrancaHistoricoMap : EntityTypeConfiguration<ContratoAcaoCobrancaHistorico>
    {
        public ContratoAcaoCobrancaHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Status)
                .HasMaxLength(500);

            this.Property(t => t.TituloAcao)
                .HasMaxLength(255);

            this.Property(t => t.CondicaoExecutada);

            // Table & Column Mappings
            this.ToTable("ContratoAcaoCobrancaHistorico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.EnvioAutomatico).HasColumnName("EnvioAutomatico");
            this.Property(t => t.CodTipoCobrancaAcao).HasColumnName("CodTipoCobrancaAcao");
            this.Property(t => t.Conteudo).HasColumnName("Conteudo");
            this.Property(t => t.DataEnvio).HasColumnName("DataEnvio");
            this.Property(t => t.DataEnvioAgendado).HasColumnName("DataEnvioAgendado");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.SituacaoTipo).HasColumnName("SituacaoTipo");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.TituloAcao).HasColumnName("TituloAcao");
            this.Property(t => t.CodParcela).HasColumnName("CodParcela");
            this.Property(t => t.DataAtendimentoAgendado).HasColumnName("DataAtendimentoAgendado");
            this.Property(t => t.CondicaoExecutada).HasColumnName("CondicaoExecutada");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ContratoAcaoCobrancaHistoricoes)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ContratoAcaoCobrancaHistoricoes)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.TabelaTipoCobrancaAcao)
                .WithMany(t => t.ContratoAcaoCobrancaHistoricoes)
                .HasForeignKey(d => d.CodTipoCobrancaAcao);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ContratoAcaoCobrancaHistoricoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
