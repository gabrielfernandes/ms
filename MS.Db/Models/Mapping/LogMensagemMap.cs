using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class LogMensagemMap : EntityTypeConfiguration<LogMensagem>
    {
        public LogMensagemMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeDestinatario)
                .HasMaxLength(350);

            this.Property(t => t.Celular)
                .HasMaxLength(50);

            this.Property(t => t.Conteudo)
                .HasMaxLength(150);

            this.Property(t => t.RetornoIntegracao)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("LogMensagem");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.NomeDestinatario).HasColumnName("NomeDestinatario");
            this.Property(t => t.Celular).HasColumnName("Celular");
            this.Property(t => t.Conteudo).HasColumnName("Conteudo");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.TentativaEnvio).HasColumnName("TentativaEnvio");
            this.Property(t => t.Mensagem).HasColumnName("Mensagem");
            this.Property(t => t.DataEnvio).HasColumnName("DataEnvio");
            this.Property(t => t.RetornoIntegracao).HasColumnName("RetornoIntegracao");
            this.Property(t => t.StatusIntegracao).HasColumnName("StatusIntegracao");
            this.Property(t => t.DataEnvioIntegracao).HasColumnName("DataEnvioIntegracao");
            this.Property(t => t.DataAgendamentoIntegracao).HasColumnName("DataAgendamentoIntegracao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.CodEnvioMassa).HasColumnName("CodEnvioMassa");

            // Relationships
            this.HasOptional(t => t.LogEnvioMassa)
                .WithMany(t => t.LogMensagems)
                .HasForeignKey(d => d.CodEnvioMassa);

        }
    }
}
