using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class LogEnvioMassaMap : EntityTypeConfiguration<LogEnvioMassa>
    {
        public LogEnvioMassaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Filtro)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("LogEnvioMassa");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.TotalRegistro).HasColumnName("TotalRegistro");
            this.Property(t => t.DataEnvio).HasColumnName("DataEnvio");
            this.Property(t => t.Filtro).HasColumnName("Filtro");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DescricaoModelo).HasColumnName("DescricaoModelo");
            this.Property(t => t.TipoEnvio).HasColumnName("TipoEnvio");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
        }
    }
}
