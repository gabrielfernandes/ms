using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaFaseMap : EntityTypeConfiguration<TabelaFase>
    {
        public TabelaFaseMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Fase)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.LoginCadastro)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("TabelaFase");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodTipoCobranca).HasColumnName("CodTipoCobranca");
            this.Property(t => t.Fase).HasColumnName("Fase");
            this.Property(t => t.Ordem).HasColumnName("Ordem");
            this.Property(t => t.Dias).HasColumnName("Dias");
            this.Property(t => t.LoginCadastro).HasColumnName("LoginCadastro");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodCampanha).HasColumnName("CodCampanha");
            this.Property(t => t.TipoHistorico).HasColumnName("TipoHistorico");

            // Relationships
            this.HasOptional(t => t.TabelaCampanha)
                .WithMany(t => t.TabelaFases)
                .HasForeignKey(d => d.CodCampanha);
            this.HasOptional(t => t.TabelaTipoCobranca)
                .WithMany(t => t.TabelaFases)
                .HasForeignKey(d => d.CodTipoCobranca);

        }
    }
}
