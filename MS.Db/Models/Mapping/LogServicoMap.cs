using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class LogServicoMap : EntityTypeConfiguration<LogServico>
    {
        public LogServicoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Arquivos)
                .HasMaxLength(550);

            this.Property(t => t.Mensagem)
                .HasMaxLength(550);

            // Table & Column Mappings
            this.ToTable("LogServico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodTipoCobrancaAcao).HasColumnName("CodTipoCobrancaAcao");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.TipoServico).HasColumnName("TipoServico");
            this.Property(t => t.TipoImportacao).HasColumnName("TipoImportacao");
            this.Property(t => t.Arquivos).HasColumnName("Arquivos");
            this.Property(t => t.DataInicio).HasColumnName("DataInicio");
            this.Property(t => t.DataFim).HasColumnName("DataFim");
            this.Property(t => t.Mensagem).HasColumnName("Mensagem");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
