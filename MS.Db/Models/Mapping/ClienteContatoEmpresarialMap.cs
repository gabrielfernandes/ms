using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ClienteContatoEmpresarialMap : EntityTypeConfiguration<ClienteContatoEmpresarial>
    {
        public ClienteContatoEmpresarialMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeContato)
                .HasMaxLength(250);

            this.Property(t => t.Profissao)
                .HasMaxLength(250);

            this.Property(t => t.TelefoneResidencial)
                .HasMaxLength(50);

            this.Property(t => t.TelefoneComercial)
                .HasMaxLength(50);

            this.Property(t => t.TelefoneCelular)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.Departamento)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ClienteContatoEmpresarial");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.NomeContato).HasColumnName("NomeContato");
            this.Property(t => t.Profissao).HasColumnName("Profissao");
            this.Property(t => t.TelefoneResidencial).HasColumnName("TelefoneResidencial");
            this.Property(t => t.TelefoneComercial).HasColumnName("TelefoneComercial");
            this.Property(t => t.TelefoneCelular).HasColumnName("TelefoneCelular");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Departamento).HasColumnName("Departamento");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ClienteContatoEmpresarials)
                .HasForeignKey(d => d.CodCliente);

        }
    }
}
