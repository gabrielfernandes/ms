using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoParcelaBoletoMap : EntityTypeConfiguration<ContratoParcelaBoleto>
    {
        public ContratoParcelaBoletoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            this.Property(t => t.NumeroDocumento)
                .HasMaxLength(50);
            // Properties
            this.Property(t => t.Instrucao1)
                .HasMaxLength(98);

            this.Property(t => t.Instrucao2)
                .HasMaxLength(98);

            this.Property(t => t.Instrucao3)
                .HasMaxLength(98);

            this.Property(t => t.CodOcorrencia)
                .HasMaxLength(50);

            this.Property(t => t.DescOcorrencia)
                .HasMaxLength(250);

            this.Property(t => t.Erro)
                .HasMaxLength(500);

            this.Property(t => t.MensagemImpressa)
                .HasMaxLength(250);

            this.Property(t => t.InstrucaoPagamento)
                .HasMaxLength(100);

            this.Property(t => t.LinhaDigitavel)
              .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("ContratoParcelaBoleto");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodParcela).HasColumnName("CodParcela");
            this.Property(t => t.CodAcordo).HasColumnName("CodAcordo");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.NumeroDocumento).HasColumnName("NumeroDocumento");
            this.Property(t => t.NossoNumero).HasColumnName("NossoNumero");
            this.Property(t => t.DataDocumento).HasColumnName("DataDocumento");
            this.Property(t => t.DataVencimento).HasColumnName("DataVencimento");
            this.Property(t => t.DataCredito).HasColumnName("DataCredito");
            this.Property(t => t.ValorDocumento).HasColumnName("ValorDocumento");
            this.Property(t => t.ValorDesconto).HasColumnName("ValorDesconto");
            this.Property(t => t.ValorPago).HasColumnName("ValorPago");
            this.Property(t => t.TipoOcorrencia).HasColumnName("TipoOcorrencia");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DataStatus).HasColumnName("DataStatus");
            this.Property(t => t.DataAprovacao).HasColumnName("DataAprovacao");
            this.Property(t => t.Guid).HasColumnName("Guid");
            this.Property(t => t.Instrucao1).HasColumnName("Instrucao1");
            this.Property(t => t.Instrucao2).HasColumnName("Instrucao2");
            this.Property(t => t.Instrucao3).HasColumnName("Instrucao3");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataRemessa).HasColumnName("DataRemessa");
            this.Property(t => t.DataRetorno).HasColumnName("DataRetorno");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.CodUsuarioRemessa).HasColumnName("CodUsuarioRemessa");
            this.Property(t => t.CodUsuarioRetorno).HasColumnName("CodUsuarioRetorno");
            this.Property(t => t.CodOcorrencia).HasColumnName("CodOcorrencia");
            this.Property(t => t.DescOcorrencia).HasColumnName("DescOcorrencia");
            this.Property(t => t.CodHistorico).HasColumnName("CodHistorico");
            this.Property(t => t.Erro).HasColumnName("Erro");
            this.Property(t => t.JurosAoMesPorcentagem).HasColumnName("JurosAoMesPorcentagem");
            this.Property(t => t.MultaAtrasoPorcentagem).HasColumnName("MultaAtrasoPorcentagem");
            this.Property(t => t.ValorJuros).HasColumnName("ValorJuros");
            this.Property(t => t.ValorMulta).HasColumnName("ValorMulta");
            this.Property(t => t.JurosAoMesPorcentagemPrevisto).HasColumnName("JurosAoMesPorcentagemPrevisto");
            this.Property(t => t.MultaAtrasoPorcentagemPrevisto).HasColumnName("MultaAtrasoPorcentagemPrevisto");
            this.Property(t => t.ValorJurosPrevisto).HasColumnName("ValorJurosPrevisto");
            this.Property(t => t.ValorMultaPrevisto).HasColumnName("ValorMultaPrevisto");
            this.Property(t => t.ValorDocumentoPrevisto).HasColumnName("ValorDocumentoPrevisto");
            this.Property(t => t.ValorDocumentoOriginal).HasColumnName("ValorDocumentoOriginal");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.Companhia).HasColumnName("Companhia");
            this.Property(t => t.Alterar).HasColumnName("Alterar");
            this.Property(t => t.CodLote).HasColumnName("CodLote");
            this.Property(t => t.CodUsuarioStatus).HasColumnName("CodUsuarioStatus");
            this.Property(t => t.Imposto).HasColumnName("Imposto");
            this.Property(t => t.ValorDocumentoAberto).HasColumnName("ValorDocumentoAberto");
            this.Property(t => t.MensagemImpressa).HasColumnName("MensagemImpressa");
            this.Property(t => t.InstrucaoPagamento).HasColumnName("InstrucaoPagamento");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.LinhaDigitavel).HasColumnName("LinhaDigitavel");
            this.Property(t => t.TipoSolicitacao).HasColumnName("TipoSolicitacao");
            // Relationships
            this.HasOptional(t => t.Acordo)
                .WithMany(t => t.ContratoParcelaBoletoes)
                .HasForeignKey(d => d.CodAcordo);
            this.HasOptional(t => t.ContratoParcela)
                .WithMany(t => t.ContratoParcelaBoletoes)
                .HasForeignKey(d => d.CodParcela);
            this.HasOptional(t => t.ContratoParcelaBoletoHistorico)
                .WithMany(t => t.ContratoParcelaBoletoes)
                .HasForeignKey(d => d.CodHistorico);
            this.HasOptional(t => t.ContratoParcelaBoletoLote)
                .WithMany(t => t.ContratoParcelaBoletoes)
                .HasForeignKey(d => d.CodLote);

        }
    }
}
