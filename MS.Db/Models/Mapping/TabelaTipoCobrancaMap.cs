using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoCobrancaMap : EntityTypeConfiguration<TabelaTipoCobranca>
    {
        public TabelaTipoCobrancaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TipoCobranca)
                .HasMaxLength(2550);

            // Table & Column Mappings
            this.ToTable("TabelaTipoCobranca");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.TipoCobranca).HasColumnName("TipoCobranca");
            this.Property(t => t.QtdDias).HasColumnName("QtdDias");
            this.Property(t => t.TipoImpacto).HasColumnName("TipoImpacto");
            this.Property(t => t.TipoEsforco).HasColumnName("TipoEsforco");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.TipoFluxo).HasColumnName("TipoFluxo");
            this.Property(t => t.CondicaoQuery).HasColumnName("CondicaoQuery");
            this.Property(t => t.Ordem).HasColumnName("Ordem");
        }
    }
}
