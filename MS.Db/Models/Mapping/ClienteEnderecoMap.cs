using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ClienteEnderecoMap : EntityTypeConfiguration<ClienteEndereco>
    {
        public ClienteEnderecoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ClienteEndereco");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.CodEndereco).HasColumnName("CodEndereco");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.Principal).HasColumnName("Principal");
            this.Property(t => t.EnderecoHOT).HasColumnName("EnderecoHOT");
            this.Property(t => t.EnderecoHOTCodUsuario).HasColumnName("EnderecoHOTCodUsuario");
            this.Property(t => t.EnderecoHOTData).HasColumnName("EnderecoHOTData");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.TipoEndereco).HasColumnName("TipoEndereco");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ClienteEnderecoes)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.TabelaEndereco)
                .WithMany(t => t.ClienteEnderecoes)
                .HasForeignKey(d => d.CodEndereco);
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ClienteEnderecoes)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ClienteEnderecoes)
                .HasForeignKey(d => d.EnderecoHOTCodUsuario);

        }
    }
}
