using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ProspectFGTMap : EntityTypeConfiguration<ProspectFGT>
    {
        public ProspectFGTMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ProspectFGTS");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodProspect).HasColumnName("CodProspect");
            this.Property(t => t.ValorFGTS).HasColumnName("ValorFGTS");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.PropostaProspect)
                .WithMany(t => t.ProspectFGTS)
                .HasForeignKey(d => d.CodProspect);

        }
    }
}
