using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class PropostaMap : EntityTypeConfiguration<Proposta>
    {
        public PropostaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("Proposta");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodUnidade).HasColumnName("CodUnidade");
            this.Property(t => t.ValorRendaFamiliar).HasColumnName("ValorRendaFamiliar");
            this.Property(t => t.CodTabelaVenda).HasColumnName("CodTabelaVenda");
            this.Property(t => t.ValorFGTS).HasColumnName("ValorFGTS");
            this.Property(t => t.CodCorretor).HasColumnName("CodCorretor");
            this.Property(t => t.CodGerente).HasColumnName("CodGerente");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataProposta).HasColumnName("DataProposta");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.ValorUnidade).HasColumnName("ValorUnidade");
            this.Property(t => t.CodEmpresaVenda).HasColumnName("CodEmpresaVenda");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.ValorMaximoFinanciamento).HasColumnName("ValorMaximoFinanciamento");
            this.Property(t => t.ValorMaximoRepasse).HasColumnName("ValorMaximoRepasse");
            this.Property(t => t.ValorFinanciamentoTotal).HasColumnName("ValorFinanciamentoTotal");
            this.Property(t => t.PrazoFinanciamento).HasColumnName("PrazoFinanciamento");
            this.Property(t => t.ValorFalta).HasColumnName("ValorFalta");
            this.Property(t => t.ValorTotalGeral).HasColumnName("ValorTotalGeral");
            this.Property(t => t.ValorCalculadoFinanciamento).HasColumnName("ValorCalculadoFinanciamento");
            this.Property(t => t.DataInicioRepasse).HasColumnName("DataInicioRepasse");
            this.Property(t => t.ValorTotalRepasse).HasColumnName("ValorTotalRepasse");
            this.Property(t => t.ValorTotalSubsidio).HasColumnName("ValorTotalSubsidio");

            // Relationships
            this.HasOptional(t => t.TabelaEmpresaVenda)
                .WithMany(t => t.Propostas)
                .HasForeignKey(d => d.CodEmpresaVenda);
            this.HasOptional(t => t.TabelaUnidade)
                .WithMany(t => t.Propostas)
                .HasForeignKey(d => d.CodUnidade);
            this.HasOptional(t => t.TabelaVenda)
                .WithMany(t => t.Propostas)
                .HasForeignKey(d => d.CodTabelaVenda);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.Propostas)
                .HasForeignKey(d => d.CodGerente);
            this.HasOptional(t => t.Usuario1)
                .WithMany(t => t.Propostas1)
                .HasForeignKey(d => d.CodCorretor);
            this.HasOptional(t => t.Usuario2)
                .WithMany(t => t.Propostas2)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
