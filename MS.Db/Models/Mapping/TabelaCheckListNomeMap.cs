using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaCheckListNomeMap : EntityTypeConfiguration<TabelaCheckListNome>
    {
        public TabelaCheckListNomeMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CheckListNome)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaCheckListNome");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CheckListNome).HasColumnName("CheckListNome");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CheckListProspect).HasColumnName("CheckListProspect");
            this.Property(t => t.TipoCheckList).HasColumnName("TipoCheckList");
        }
    }
}
