using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEscritorioMap : EntityTypeConfiguration<TabelaEscritorio>
    {
        public TabelaEscritorioMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Escritorio)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaEscritorio");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Escritorio).HasColumnName("Escritorio");
            this.Property(t => t.CodEndereco).HasColumnName("CodEndereco");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.QueryDistribuicao).HasColumnName("QueryDistribuicao");

            // Relationships
            this.HasOptional(t => t.TabelaEndereco)
                .WithMany(t => t.TabelaEscritorios)
                .HasForeignKey(d => d.CodEndereco);

        }
    }
}
