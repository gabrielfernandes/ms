using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaBancoPortadorMap : EntityTypeConfiguration<TabelaBancoPortador>
    {
        public TabelaBancoPortadorMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Portador)
                .HasMaxLength(50);

            this.Property(t => t.Agencia)
                .HasMaxLength(50);

            this.Property(t => t.AgenciaDigito)
                .HasMaxLength(50);

            this.Property(t => t.Conta)
                .HasMaxLength(50);

            this.Property(t => t.ContaDigito)
                .HasMaxLength(50);

            this.Property(t => t.CNPJ)
                .HasMaxLength(50);

            this.Property(t => t.CEP)
                .HasMaxLength(50);

            this.Property(t => t.Endereco)
                .HasMaxLength(500);

            this.Property(t => t.Cidade)
                .HasMaxLength(500);

            this.Property(t => t.Estado)
                .HasMaxLength(50);

            this.Property(t => t.Bairro)
                .HasMaxLength(50);

            this.Property(t => t.UF)
                .HasMaxLength(50);

            this.Property(t => t.EnderecoNumero)
                .HasMaxLength(10);

            this.Property(t => t.EnderecoComplemento)
                .HasMaxLength(500);

            this.Property(t => t.Companhia)
                .HasMaxLength(50);
            this.Property(t => t.CompanhiaFiscal)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaBancoPortador");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodBanco).HasColumnName("CodBanco");
            this.Property(t => t.Portador).HasColumnName("Portador");
            this.Property(t => t.Agencia).HasColumnName("Agencia");
            this.Property(t => t.AgenciaDigito).HasColumnName("AgenciaDigito");
            this.Property(t => t.Conta).HasColumnName("Conta");
            this.Property(t => t.ContaDigito).HasColumnName("ContaDigito");
            this.Property(t => t.Ativo).HasColumnName("Ativo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.CNPJ).HasColumnName("CNPJ");
            this.Property(t => t.CEP).HasColumnName("CEP");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.Estado).HasColumnName("Estado");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.UF).HasColumnName("UF");
            this.Property(t => t.EnderecoNumero).HasColumnName("EnderecoNumero");
            this.Property(t => t.EnderecoComplemento).HasColumnName("EnderecoComplemento");
            this.Property(t => t.BancoNumero).HasColumnName("BancoNumero");
            this.Property(t => t.DocumentoCodigoEspecie).HasColumnName("DocumentoCodigoEspecie");
            this.Property(t => t.Carteira).HasColumnName("Carteira");
            this.Property(t => t.JurosAoMesPorcentagem).HasColumnName("JurosAoMesPorcentagem");
            this.Property(t => t.TaxaPorParcela).HasColumnName("TaxaPorParcela");
            this.Property(t => t.MultaAtraso).HasColumnName("MultaAtraso");
            this.Property(t => t.MoraAtrasoPorcetagemDia).HasColumnName("MoraAtrasoPorcetagemDia");
            this.Property(t => t.Companhia).HasColumnName("Companhia");
            this.Property(t => t.NossoNumero).HasColumnName("NossoNumero");
            this.Property(t => t.NossoNumeroAtual).HasColumnName("NossoNumeroAtual");
            this.Property(t => t.Lote).HasColumnName("Lote");
            this.Property(t => t.LoteAtual).HasColumnName("LoteAtual");
            this.Property(t => t.CompanhiaFiscal).HasColumnName("CompanhiaFiscal");
            this.Property(t => t.CodBancoPortadorGrupo).HasColumnName("CodBancoPortadorGrupo");
            this.Property(t => t.DataInicialRegistroBoleto).HasColumnName("DataInicialRegistroBoleto");
            this.Property(t => t.NumeroDiaRegistroBoleto).HasColumnName("NumeroDiaRegistroBoleto");
            this.Property(t => t.CodRegionalGrupo).HasColumnName("CodRegionalGrupo");
            this.Property(t => t.Padrao).HasColumnName("Padrao");


            // Relationships
            this.HasOptional(t => t.TabelaBanco)
                .WithMany(t => t.TabelaBancoPortadors)
                .HasForeignKey(d => d.CodBanco);

            this.HasOptional(t => t.TabelaRegionalGrupo)
                .WithMany(t => t.TabelaBancoPortadors)
                .HasForeignKey(d => d.CodRegionalGrupo);

            this.HasOptional(t => t.TabelaBancoPortadorGrupo)
               .WithMany(t => t.TabelaBancoPortadors)
               .HasForeignKey(d => d.CodBancoPortadorGrupo);
        }
    }
}
