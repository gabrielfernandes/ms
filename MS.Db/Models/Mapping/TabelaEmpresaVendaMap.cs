using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEmpresaVendaMap : EntityTypeConfiguration<TabelaEmpresaVenda>
    {
        public TabelaEmpresaVendaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.EmpresaVendas)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaEmpresaVendas");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.EmpresaVendas).HasColumnName("EmpresaVendas");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
