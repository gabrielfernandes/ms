using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaAnaliseDocumentalMap : EntityTypeConfiguration<TabelaAnaliseDocumental>
    {
        public TabelaAnaliseDocumentalMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Pergunta)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaAnaliseDocumental");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodTipoDocumento).HasColumnName("CodTipoDocumento");
            this.Property(t => t.Campo).HasColumnName("Campo");
            this.Property(t => t.Pergunta).HasColumnName("Pergunta");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.TipoPergunta).HasColumnName("TipoPergunta");

            // Relationships
            this.HasOptional(t => t.TabelaTipoDocumento)
                .WithMany(t => t.TabelaAnaliseDocumentals)
                .HasForeignKey(d => d.CodTipoDocumento);

        }
    }
}
