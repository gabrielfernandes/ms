using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaGrupoClienteMap : EntityTypeConfiguration<TabelaGrupoCliente>
    {
        public TabelaGrupoClienteMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CNPJ)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TabelaGrupoCliente");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodGrupo).HasColumnName("CodGrupo");
            this.Property(t => t.CNPJ).HasColumnName("CNPJ");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.AcaoBloqueada).HasColumnName("AcaoBloqueada");

            // Relationships
            this.HasOptional(t => t.TabelaGrupo)
                .WithMany(t => t.TabelaGrupoClientes)
                .HasForeignKey(d => d.CodGrupo);

        }
    }
}
