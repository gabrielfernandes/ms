using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoEmpreendimentoFotoMap : EntityTypeConfiguration<TabelaTipoEmpreendimentoFoto>
    {
        public TabelaTipoEmpreendimentoFotoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TipoFoto)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaTipoEmpreendimentoFoto");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.TipoFoto).HasColumnName("TipoFoto");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
