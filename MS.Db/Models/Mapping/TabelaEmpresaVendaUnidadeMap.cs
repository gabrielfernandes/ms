using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEmpresaVendaUnidadeMap : EntityTypeConfiguration<TabelaEmpresaVendaUnidade>
    {
        public TabelaEmpresaVendaUnidadeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.Cod, t.DataCadastro });

            // Properties
            this.Property(t => t.Cod)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("TabelaEmpresaVendaUnidade");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodEmpresaVenda).HasColumnName("CodEmpresaVenda");
            this.Property(t => t.CodUnidade).HasColumnName("CodUnidade");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
