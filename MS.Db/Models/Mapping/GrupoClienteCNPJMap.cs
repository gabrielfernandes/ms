using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class GrupoClienteCNPJMap : EntityTypeConfiguration<GrupoClienteCNPJ>
    {
        public GrupoClienteCNPJMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CNPJ)
                .HasMaxLength(60);

            // Table & Column Mappings
            this.ToTable("GrupoClienteCNPJ");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodGrupoCliente).HasColumnName("CodGrupoCliente");
            this.Property(t => t.CNPJ).HasColumnName("CNPJ");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.GrupoCliente)
                .WithMany(t => t.GrupoClienteCNPJs)
                .HasForeignKey(d => d.CodGrupoCliente);

        }
    }
}
