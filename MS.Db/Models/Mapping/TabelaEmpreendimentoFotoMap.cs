using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEmpreendimentoFotoMap : EntityTypeConfiguration<TabelaEmpreendimentoFoto>
    {
        public TabelaEmpreendimentoFotoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.ArquivoObservacao)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoCaminho)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoNome)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoGuid)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaEmpreendimentoFoto");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodEmpreendimento).HasColumnName("CodEmpreendimento");
            this.Property(t => t.CodTipoEmpreendimentoFoto).HasColumnName("CodTipoEmpreendimentoFoto");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.ArquivoObservacao).HasColumnName("ArquivoObservacao");
            this.Property(t => t.ArquivoCaminho).HasColumnName("ArquivoCaminho");
            this.Property(t => t.ArquivoNome).HasColumnName("ArquivoNome");
            this.Property(t => t.ArquivoGuid).HasColumnName("ArquivoGuid");

            // Relationships
            this.HasOptional(t => t.TabelaEmpreendimento)
                .WithMany(t => t.TabelaEmpreendimentoFotoes)
                .HasForeignKey(d => d.CodEmpreendimento);
            this.HasOptional(t => t.TabelaTipoEmpreendimentoFoto)
                .WithMany(t => t.TabelaEmpreendimentoFotoes)
                .HasForeignKey(d => d.CodTipoEmpreendimentoFoto);

        }
    }
}
