using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaSinteseMap : EntityTypeConfiguration<TabelaSintese>
    {
        public TabelaSinteseMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Sintese)
                .IsRequired()
                .HasMaxLength(130);

            this.Property(t => t.LoginCadastro)
                .HasMaxLength(25);

            this.Property(t => t.CodigoSinteseCliente)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaSintese");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodFase).HasColumnName("CodFase");
            this.Property(t => t.CodTipoCobranca).HasColumnName("CodTipoCobranca");
            this.Property(t => t.Sintese).HasColumnName("Sintese");
            this.Property(t => t.Ordem).HasColumnName("Ordem");
            this.Property(t => t.Dias).HasColumnName("Dias");
            this.Property(t => t.LoginCadastro).HasColumnName("LoginCadastro");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.TarefaAtendimento).HasColumnName("TarefaAtendimento");
            this.Property(t => t.AcaoBloqueada).HasColumnName("AcaoBloqueada");
            this.Property(t => t.CodigoSinteseCliente).HasColumnName("CodigoSinteseCliente");
            this.Property(t => t.Justifica).HasColumnName("Justifica");

            // Relationships
            this.HasRequired(t => t.TabelaFase)
                .WithMany(t => t.TabelaSintese)
                .HasForeignKey(d => d.CodFase);
            this.HasOptional(t => t.TabelaTipoCobranca)
                .WithMany(t => t.TabelaSintese)
                .HasForeignKey(d => d.CodTipoCobranca);

        }
    }
}
