using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoCobrancaCondicaoMap : EntityTypeConfiguration<TabelaTipoCobrancaCondicao>
    {
        public TabelaTipoCobrancaCondicaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Valor)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TabelaTipoCobrancaCondicao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodTipoCobranca).HasColumnName("CodTipoCobranca");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.Condicao).HasColumnName("Condicao");
            this.Property(t => t.Valor).HasColumnName("Valor");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Excecao).HasColumnName("Excecao");

            // Relationships
            this.HasOptional(t => t.TabelaTipoCobranca)
                .WithMany(t => t.TabelaTipoCobrancaCondicaos)
                .HasForeignKey(d => d.CodTipoCobranca);

        }
    }
}
