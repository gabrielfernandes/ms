using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoTipoCobrancaHistoricoMap : EntityTypeConfiguration<ContratoTipoCobrancaHistorico>
    {
        public ContratoTipoCobrancaHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContratoTipoCobrancaHistorico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.CodTipoCobranca).HasColumnName("CodTipoCobranca");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodLogProcessamento).HasColumnName("CodLogProcessamento");

            // Relationships
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ContratoTipoCobrancaHistoricoes)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.TabelaTipoCobranca)
                .WithMany(t => t.ContratoTipoCobrancaHistoricoes)
                .HasForeignKey(d => d.CodTipoCobranca);

        }
    }
}
