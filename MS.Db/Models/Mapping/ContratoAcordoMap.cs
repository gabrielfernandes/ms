using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoAcordoMap : EntityTypeConfiguration<ContratoAcordo>
    {
        public ContratoAcordoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContratoAcordo");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.TipoAcordo).HasColumnName("TipoAcordo");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Observacao).HasColumnName("Observacao");

            // Relationships
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ContratoAcordoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
