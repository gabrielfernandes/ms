using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaMuralAvisoMap : EntityTypeConfiguration<TabelaMuralAviso>
    {
        public TabelaMuralAvisoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TituloAviso)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaMuralAviso");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.DataEnvio).HasColumnName("DataEnvio");
            this.Property(t => t.EnviarEmail).HasColumnName("EnviarEmail");
            this.Property(t => t.TituloAviso).HasColumnName("TituloAviso");
            this.Property(t => t.TextoAviso).HasColumnName("TextoAviso");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
