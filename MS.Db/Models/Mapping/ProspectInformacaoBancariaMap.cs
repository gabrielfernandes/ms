using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ProspectInformacaoBancariaMap : EntityTypeConfiguration<ProspectInformacaoBancaria>
    {
        public ProspectInformacaoBancariaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ProspectInformacaoBancaria");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodProspect).HasColumnName("CodProspect");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.CodBanco).HasColumnName("CodBanco");
            this.Property(t => t.ValorLimite).HasColumnName("ValorLimite");
            this.Property(t => t.Bandeira).HasColumnName("Bandeira");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.TipoInfoBancaria).HasColumnName("TipoInfoBancaria");

            // Relationships
            this.HasOptional(t => t.PropostaProspect)
                .WithMany(t => t.ProspectInformacaoBancarias)
                .HasForeignKey(d => d.CodProspect);
            this.HasOptional(t => t.TabelaBanco)
                .WithMany(t => t.ProspectInformacaoBancarias)
                .HasForeignKey(d => d.CodBanco);

        }
    }
}
