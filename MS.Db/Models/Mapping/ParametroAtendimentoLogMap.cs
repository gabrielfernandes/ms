using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ParametroAtendimentoLogMap : EntityTypeConfiguration<ParametroAtendimentoLog>
    {
        public ParametroAtendimentoLogMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ParametroAtendimentoLog");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodAtendimento).HasColumnName("CodAtendimento");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Descricao).HasColumnName("Descricao");

            // Relationships
            this.HasOptional(t => t.ParametroAtendimento)
                .WithMany(t => t.ParametroAtendimentoLogs)
                .HasForeignKey(d => d.CodAtendimento);

        }
    }
}
