using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEquipeUsuarioMap : EntityTypeConfiguration<TabelaEquipeUsuario>
    {
        public TabelaEquipeUsuarioMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaEquipeUsuario");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodEquipe).HasColumnName("CodEquipe");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaEquipe)
                .WithMany(t => t.TabelaEquipeUsuarios)
                .HasForeignKey(d => d.CodEquipe);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.TabelaEquipeUsuarios)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
