using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ImportacaoCreditoMap : EntityTypeConfiguration<ImportacaoCredito>
    {
        public ImportacaoCreditoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NumeroDocumento)
                .HasMaxLength(255);

            this.Property(t => t.NumeroCliente)
                .HasMaxLength(255);

            this.Property(t => t.Descricao)
                .HasMaxLength(255);

            this.Property(t => t.DataCredito)
                .HasMaxLength(255);

            this.Property(t => t.ValorCredito)
                .HasMaxLength(255);

            this.Property(t => t.Observacao)
                .HasMaxLength(255);

            this.Property(t => t.Mensagem)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("ImportacaoCredito");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.NumeroDocumento).HasColumnName("NumeroDocumento");
            this.Property(t => t.NumeroCliente).HasColumnName("NumeroCliente");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.DataCredito).HasColumnName("DataCredito");
            this.Property(t => t.ValorCredito).HasColumnName("ValorCredito");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Mensagem).HasColumnName("Mensagem");
            this.Property(t => t.DataProcessamento).HasColumnName("DataProcessamento");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");

            // Relationships
            this.HasOptional(t => t.Importacao)
                .WithMany(t => t.ImportacaoCreditoes)
                .HasForeignKey(d => d.CodImportacao);

        }
    }
}
