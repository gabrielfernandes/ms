using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaParametroTaxaMap : EntityTypeConfiguration<TabelaParametroTaxa>
    {
        public TabelaParametroTaxaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaParametroTaxa");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.MultaElegivel).HasColumnName("MultaElegivel");
            this.Property(t => t.MultaDe).HasColumnName("MultaDe");
            this.Property(t => t.MultaAte).HasColumnName("MultaAte");
            this.Property(t => t.JurosElegivel).HasColumnName("JurosElegivel");
            this.Property(t => t.JurosDe).HasColumnName("JurosDe");
            this.Property(t => t.JurosAte).HasColumnName("JurosAte");
            this.Property(t => t.HonorarioPercentualElegivel).HasColumnName("HonorarioPercentualElegivel");
            this.Property(t => t.HonorarioPercentualDe).HasColumnName("HonorarioPercentualDe");
            this.Property(t => t.HonorarioPercentualAte).HasColumnName("HonorarioPercentualAte");
            this.Property(t => t.HonorarioValorElegivel).HasColumnName("HonorarioValorElegivel");
            this.Property(t => t.HonorarioValorDe).HasColumnName("HonorarioValorDe");
            this.Property(t => t.HonorarioValorAte).HasColumnName("HonorarioValorAte");
            this.Property(t => t.DescontoPercentualElegivel).HasColumnName("DescontoPercentualElegivel");
            this.Property(t => t.DescontoPercentualValor).HasColumnName("DescontoPercentualValor");
            this.Property(t => t.JurosParcelaElegivel).HasColumnName("JurosParcelaElegivel");
            this.Property(t => t.JurosParcelaValorDe).HasColumnName("JurosParcelaValorDe");
            this.Property(t => t.JurosParcelaValorAte).HasColumnName("JurosParcelaValorAte");
            this.Property(t => t.TipoCorrecao).HasColumnName("TipoCorrecao");
            this.Property(t => t.ParcelaNaoMensaisElegivel).HasColumnName("ParcelaNaoMensaisElegivel");
            this.Property(t => t.UtilizaJurosCompostoElegivel).HasColumnName("UtilizaJurosCompostoElegivel");
            this.Property(t => t.CalculaJurosCorrecaoElegivel).HasColumnName("CalculaJurosCorrecaoElegivel");
            this.Property(t => t.CalculaMultaCorrecaoElegivel).HasColumnName("CalculaMultaCorrecaoElegivel");
            this.Property(t => t.CalculaMultaSobreJurosElegivel).HasColumnName("CalculaMultaSobreJurosElegivel");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
