using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ClienteContatoMap : EntityTypeConfiguration<ClienteContato>
    {
        public ClienteContatoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Contato)
                .HasMaxLength(250);

            this.Property(t => t.NomeContato)
                .HasMaxLength(250);

            this.Property(t => t.Descricao)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ClienteContato");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Contato).HasColumnName("Contato");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.ContatoHOT).HasColumnName("ContatoHOT");
            this.Property(t => t.ContatoHOTCodUsuario).HasColumnName("ContatoHOTCodUsuario");
            this.Property(t => t.ContatoHOTData).HasColumnName("ContatoHOTData");
            this.Property(t => t.NomeContato).HasColumnName("NomeContato");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.CodTipoCadastro).HasColumnName("CodTipoCadastro");
            this.Property(t => t.Descricao).HasColumnName("Descricao");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ClienteContatoes)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.TabelaTipoCadastro)
                .WithMany(t => t.ClienteContatoes)
                .HasForeignKey(d => d.CodTipoCadastro);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ClienteContatoes)
                .HasForeignKey(d => d.ContatoHOTCodUsuario);

        }
    }
}
