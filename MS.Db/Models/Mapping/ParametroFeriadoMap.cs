using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ParametroFeriadoMap : EntityTypeConfiguration<ParametroFeriado>
    {
        public ParametroFeriadoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ParametroFeriado");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Data).HasColumnName("Data");
            this.Property(t => t.Feriado).HasColumnName("Feriado");
            this.Property(t => t.Ativo).HasColumnName("Ativo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
