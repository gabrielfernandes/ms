using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoDocumentoMap : EntityTypeConfiguration<ContratoDocumento>
    {
        public ContratoDocumentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.ArquivoCaminho)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoNome)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoGuid)
                .HasMaxLength(255);

            this.Property(t => t.ArquivoObservacao)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ContratoDocumento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodTipoDocumento).HasColumnName("CodTipoDocumento");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.ArquivoCaminho).HasColumnName("ArquivoCaminho");
            this.Property(t => t.ArquivoNome).HasColumnName("ArquivoNome");
            this.Property(t => t.ArquivoGuid).HasColumnName("ArquivoGuid");
            this.Property(t => t.ArquivoObservacao).HasColumnName("ArquivoObservacao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ContratoDocumentoes)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ContratoDocumentoes)
                .HasForeignKey(d => d.CodUsuario);
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ContratoDocumentoes)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.TabelaTipoDocumento)
                .WithMany(t => t.ContratoDocumentoes)
                .HasForeignKey(d => d.CodTipoDocumento);

        }
    }
}
