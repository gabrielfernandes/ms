using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class PropostaHistoricoMap : EntityTypeConfiguration<PropostaHistorico>
    {
        public PropostaHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Observacoes)
                .HasMaxLength(550);

            // Table & Column Mappings
            this.ToTable("PropostaHistorico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodProposta).HasColumnName("CodProposta");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.CodSintese).HasColumnName("CodSintese");
            this.Property(t => t.Observacoes).HasColumnName("Observacoes");
            this.Property(t => t.DataAgenda).HasColumnName("DataAgenda");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.Proposta)
                .WithMany(t => t.PropostaHistoricoes)
                .HasForeignKey(d => d.CodProposta);
            this.HasOptional(t => t.TabelaSintese)
                .WithMany(t => t.PropostaHistoricoes)
                .HasForeignKey(d => d.CodSintese);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.PropostaHistoricoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
