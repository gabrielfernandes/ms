using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class LogEmailMap : EntityTypeConfiguration<LogEmail>
    {
        public LogEmailMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.EmailRemetente)
                .HasMaxLength(550);

            this.Property(t => t.EmailDestinatario)
                .HasMaxLength(550);

            this.Property(t => t.NomeDestinatario)
                .HasMaxLength(550);

            this.Property(t => t.NomeRemetente)
                .HasMaxLength(350);

            this.Property(t => t.Assunto)
                .HasMaxLength(350);

            this.Property(t => t.EmailCopia)
                .HasMaxLength(550);

            // Table & Column Mappings
            this.ToTable("LogEmail");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.EmailRemetente).HasColumnName("EmailRemetente");
            this.Property(t => t.EmailDestinatario).HasColumnName("EmailDestinatario");
            this.Property(t => t.NomeDestinatario).HasColumnName("NomeDestinatario");
            this.Property(t => t.NomeRemetente).HasColumnName("NomeRemetente");
            this.Property(t => t.Assunto).HasColumnName("Assunto");
            this.Property(t => t.Conteudo).HasColumnName("Conteudo");
            this.Property(t => t.HTML).HasColumnName("HTML");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.TentativaEnvio).HasColumnName("TentativaEnvio");
            this.Property(t => t.Mensagem).HasColumnName("Mensagem");
            this.Property(t => t.DataEnvio).HasColumnName("DataEnvio");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.EmailCopia).HasColumnName("EmailCopia");
            this.Property(t => t.CodEnvioMassa).HasColumnName("CodEnvioMassa");
        }
    }
}
