using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ImportacaoClienteMap : EntityTypeConfiguration<ImportacaoCliente>
    {
        public ImportacaoClienteMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NumeroCliente)
                .HasMaxLength(255);

            this.Property(t => t.DescricaoCliente)
                .HasMaxLength(255);

            this.Property(t => t.CpfCnpj)
                .HasMaxLength(255);

            this.Property(t => t.Endereco)
                .HasMaxLength(255);

            this.Property(t => t.Numero)
                .HasMaxLength(255);

            this.Property(t => t.Complemento)
                .HasMaxLength(255);

            this.Property(t => t.Bairro)
                .HasMaxLength(255);

            this.Property(t => t.CEP)
                .HasMaxLength(255);

            this.Property(t => t.Cidade)
                .HasMaxLength(255);

            this.Property(t => t.Estado)
                .HasMaxLength(255);

            this.Property(t => t.TP)
                .HasMaxLength(255);

            this.Property(t => t.DDD)
                .HasMaxLength(255);

            this.Property(t => t.NumeroTelefone)
                .HasMaxLength(255);

            this.Property(t => t.Email)
                .HasMaxLength(255);

            this.Property(t => t.Mensagem)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("ImportacaoCliente");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.NumeroCliente).HasColumnName("NumeroCliente");
            this.Property(t => t.DescricaoCliente).HasColumnName("DescricaoCliente");
            this.Property(t => t.CpfCnpj).HasColumnName("CpfCnpj");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.Numero).HasColumnName("Numero");
            this.Property(t => t.Complemento).HasColumnName("Complemento");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.CEP).HasColumnName("CEP");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.Estado).HasColumnName("Estado");
            this.Property(t => t.TP).HasColumnName("TP");
            this.Property(t => t.DDD).HasColumnName("DDD");
            this.Property(t => t.NumeroTelefone).HasColumnName("NumeroTelefone");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Mensagem).HasColumnName("Mensagem");
            this.Property(t => t.DataProcessamento).HasColumnName("DataProcessamento");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.Duplicado).HasColumnName("Duplicado");

            // Relationships
            this.HasOptional(t => t.Importacao)
                .WithMany(t => t.ImportacaoClientes)
                .HasForeignKey(d => d.CodImportacao);

        }
    }
}
