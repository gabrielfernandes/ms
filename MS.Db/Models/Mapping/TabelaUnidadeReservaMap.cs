using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaUnidadeReservaMap : EntityTypeConfiguration<TabelaUnidadeReserva>
    {
        public TabelaUnidadeReservaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Nome)
                .HasMaxLength(255);

            this.Property(t => t.Email)
                .HasMaxLength(255);

            this.Property(t => t.Celular)
                .HasMaxLength(50);

            this.Property(t => t.Telefone)
                .HasMaxLength(50);

            this.Property(t => t.CPF)
                .HasMaxLength(50);

            this.Property(t => t.CNPJ)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaUnidadeReserva");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodUnidade).HasColumnName("CodUnidade");
            this.Property(t => t.CodUnidadeStatusHistorico).HasColumnName("CodUnidadeStatusHistorico");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataReserva).HasColumnName("DataReserva");
            this.Property(t => t.StatusReserva).HasColumnName("StatusReserva");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Celular).HasColumnName("Celular");
            this.Property(t => t.Telefone).HasColumnName("Telefone");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.DataExpiracao).HasColumnName("DataExpiracao");
            this.Property(t => t.CPF).HasColumnName("CPF");
            this.Property(t => t.CNPJ).HasColumnName("CNPJ");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaUnidadeStatusHistorico)
                .WithMany(t => t.TabelaUnidadeReservas)
                .HasForeignKey(d => d.CodUnidadeStatusHistorico);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.TabelaUnidadeReservas)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
