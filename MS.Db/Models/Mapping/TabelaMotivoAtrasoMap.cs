using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaMotivoAtrasoMap : EntityTypeConfiguration<TabelaMotivoAtraso>
    {
        public TabelaMotivoAtrasoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.MotivoAtraso)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TabelaMotivoAtraso");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.MotivoAtraso).HasColumnName("MotivoAtraso");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
