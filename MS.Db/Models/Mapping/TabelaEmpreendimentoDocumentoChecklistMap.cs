using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEmpreendimentoDocumentoChecklistMap : EntityTypeConfiguration<TabelaEmpreendimentoDocumentoChecklist>
    {
        public TabelaEmpreendimentoDocumentoChecklistMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaEmpreendimentoDocumentoChecklist");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodDocumento).HasColumnName("CodDocumento");
            this.Property(t => t.CodChecklist).HasColumnName("CodChecklist");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaCheckList)
                .WithMany(t => t.TabelaEmpreendimentoDocumentoChecklists)
                .HasForeignKey(d => d.CodChecklist);
            this.HasOptional(t => t.TabelaEmpreendimentoDocumento)
                .WithMany(t => t.TabelaEmpreendimentoDocumentoChecklists)
                .HasForeignKey(d => d.CodDocumento);

        }
    }
}
