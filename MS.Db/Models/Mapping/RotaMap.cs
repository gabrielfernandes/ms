using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class RotaMap : EntityTypeConfiguration<Rota>
    {
        public RotaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Nome)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Descricao)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Rota");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
