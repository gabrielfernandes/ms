using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaResolucaoMap : EntityTypeConfiguration<TabelaResolucao>
    {
        public TabelaResolucaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Resolucao)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TabelaResolucao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Resolucao).HasColumnName("Resolucao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
