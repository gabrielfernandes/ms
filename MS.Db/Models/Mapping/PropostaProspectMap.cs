using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class PropostaProspectMap : EntityTypeConfiguration<PropostaProspect>
    {
        public PropostaProspectMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Nome)
                .HasMaxLength(50);

            this.Property(t => t.PrimeiroNome)
                .HasMaxLength(50);

            this.Property(t => t.CPF)
                .HasMaxLength(20);

            this.Property(t => t.RG)
                .HasMaxLength(20);

            this.Property(t => t.CNPJ)
                .HasMaxLength(30);

            this.Property(t => t.Profissao)
                .HasMaxLength(30);

            this.Property(t => t.Email)
                .HasMaxLength(150);

            this.Property(t => t.Nacionalidade)
                .HasMaxLength(50);

            this.Property(t => t.Naturalidade)
                .HasMaxLength(50);

            this.Property(t => t.Orgao)
                .HasMaxLength(50);

            this.Property(t => t.RegimeCasamento)
                .HasMaxLength(50);

            this.Property(t => t.TelefoneResidencial)
                .HasMaxLength(50);

            this.Property(t => t.TelefoneComercial)
                .HasMaxLength(50);

            this.Property(t => t.Celular)
                .HasMaxLength(50);

            this.Property(t => t.CelularComercial)
                .HasMaxLength(50);

            this.Property(t => t.CEP)
                .HasMaxLength(50);

            this.Property(t => t.Endereco)
                .HasMaxLength(500);

            this.Property(t => t.Cidade)
                .HasMaxLength(500);

            this.Property(t => t.Estado)
                .HasMaxLength(50);

            this.Property(t => t.Bairro)
                .HasMaxLength(50);

            this.Property(t => t.UF)
                .HasMaxLength(50);

            this.Property(t => t.EnderecoNumero)
                .HasMaxLength(10);

            this.Property(t => t.EnderecoComplemento)
                .HasMaxLength(500);

            this.Property(t => t.NomePai)
                .HasMaxLength(250);

            this.Property(t => t.NomeMae)
                .HasMaxLength(250);

            this.Property(t => t.Cartorio)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("PropostaProspect");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodProposta).HasColumnName("CodProposta");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.PrimeiroNome).HasColumnName("PrimeiroNome");
            this.Property(t => t.CPF).HasColumnName("CPF");
            this.Property(t => t.TipoDocumento).HasColumnName("TipoDocumento");
            this.Property(t => t.RG).HasColumnName("RG");
            this.Property(t => t.CNPJ).HasColumnName("CNPJ");
            this.Property(t => t.EstadoCivil).HasColumnName("EstadoCivil");
            this.Property(t => t.Profissao).HasColumnName("Profissao");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.DataNascimento).HasColumnName("DataNascimento");
            this.Property(t => t.Nacionalidade).HasColumnName("Nacionalidade");
            this.Property(t => t.Naturalidade).HasColumnName("Naturalidade");
            this.Property(t => t.Orgao).HasColumnName("Orgao");
            this.Property(t => t.RegimeCasamento).HasColumnName("RegimeCasamento");
            this.Property(t => t.DataCasamento).HasColumnName("DataCasamento");
            this.Property(t => t.Sexo).HasColumnName("Sexo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataEmissao).HasColumnName("DataEmissao");
            this.Property(t => t.TelefoneResidencial).HasColumnName("TelefoneResidencial");
            this.Property(t => t.TelefoneComercial).HasColumnName("TelefoneComercial");
            this.Property(t => t.Celular).HasColumnName("Celular");
            this.Property(t => t.CelularComercial).HasColumnName("CelularComercial");
            this.Property(t => t.CEP).HasColumnName("CEP");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.Estado).HasColumnName("Estado");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.UF).HasColumnName("UF");
            this.Property(t => t.EnderecoNumero).HasColumnName("EnderecoNumero");
            this.Property(t => t.EnderecoComplemento).HasColumnName("EnderecoComplemento");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.NomePai).HasColumnName("NomePai");
            this.Property(t => t.NomeMae).HasColumnName("NomeMae");
            this.Property(t => t.NumeroDependente).HasColumnName("NumeroDependente");
            this.Property(t => t.Cartorio).HasColumnName("Cartorio");
            this.Property(t => t.CategoriaProfissional).HasColumnName("CategoriaProfissional");
            this.Property(t => t.CodConjuge).HasColumnName("CodConjuge");
            this.Property(t => t.FGTS).HasColumnName("FGTS");
            this.Property(t => t.CartaoCredito).HasColumnName("CartaoCredito");
            this.Property(t => t.AplicacaoFinanceira).HasColumnName("AplicacaoFinanceira");
            this.Property(t => t.Aluguel).HasColumnName("Aluguel");
            this.Property(t => t.Escolaridade).HasColumnName("Escolaridade");
            this.Property(t => t.Ordem).HasColumnName("Ordem");
            this.Property(t => t.TipoComprador).HasColumnName("TipoComprador");

            // Relationships
            this.HasOptional(t => t.Proposta)
                .WithMany(t => t.PropostaProspects)
                .HasForeignKey(d => d.CodProposta);
            this.HasOptional(t => t.PropostaProspect2)
                .WithMany(t => t.PropostaProspect1)
                .HasForeignKey(d => d.CodConjuge);

        }
    }
}
