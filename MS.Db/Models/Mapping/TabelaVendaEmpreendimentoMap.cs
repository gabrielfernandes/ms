using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaVendaEmpreendimentoMap : EntityTypeConfiguration<TabelaVendaEmpreendimento>
    {
        public TabelaVendaEmpreendimentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaVendaEmpreendimento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodEmpreendimento).HasColumnName("CodEmpreendimento");
            this.Property(t => t.CodTabelaVenda).HasColumnName("CodTabelaVenda");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaEmpreendimento)
                .WithMany(t => t.TabelaVendaEmpreendimentoes)
                .HasForeignKey(d => d.CodEmpreendimento);
            this.HasOptional(t => t.TabelaVenda)
                .WithMany(t => t.TabelaVendaEmpreendimentoes)
                .HasForeignKey(d => d.CodTabelaVenda);

        }
    }
}
