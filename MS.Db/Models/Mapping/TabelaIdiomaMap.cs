using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaIdiomaMap : EntityTypeConfiguration<TabelaIdioma>
    {
        public TabelaIdiomaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Cod)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("TabelaIdioma");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.ptBR).HasColumnName("ptBR");
            this.Property(t => t.enUS).HasColumnName("enUS");
            this.Property(t => t.esES).HasColumnName("esES");
        }
    }
}
