using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ImportacaoParcelaMap : EntityTypeConfiguration<ImportacaoParcela>
    {
        public ImportacaoParcelaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Companhia)
                .HasMaxLength(255);

            this.Property(t => t.Filial)
                .HasMaxLength(255);

            this.Property(t => t.FilialDescricao)
                .HasMaxLength(255);

            this.Property(t => t.RotaArea)
                .HasMaxLength(255);

            this.Property(t => t.Status)
                .HasMaxLength(255);

            this.Property(t => t.TipoContrato)
                .HasMaxLength(255);

            this.Property(t => t.NumeroCliente)
                .HasMaxLength(255);

            this.Property(t => t.CPFCNPJ)
                .HasMaxLength(255);

            this.Property(t => t.Descricao)
                .HasMaxLength(255);

            this.Property(t => t.NumeroFatura)
                .HasMaxLength(255);

            this.Property(t => t.ValorFatura)
                .HasMaxLength(255);

            this.Property(t => t.ValorAberto)
                .HasMaxLength(255);

            this.Property(t => t.DataVencimento)
                .HasMaxLength(255);

            this.Property(t => t.NumeroParcela)
                .HasMaxLength(255);

            this.Property(t => t.Governo)
                .HasMaxLength(255);

            this.Property(t => t.Corporativo)
                .HasMaxLength(255);

            this.Property(t => t.Mensagem)
                .HasMaxLength(255);

            this.Property(t => t.NumeroContrato)
                .HasMaxLength(255);

            this.Property(t => t.CodSinteseCliente)
                .HasMaxLength(255);

            this.Property(t => t.NotaFiscal)
                .HasMaxLength(250);

            this.Property(t => t.TipoDocumento)
                .HasMaxLength(250);

            this.Property(t => t.InstrumentoPgto)
                .HasMaxLength(250);

            this.Property(t => t.StatusContrato)
                .HasMaxLength(255);

        
            // Table & Column Mappings
            this.ToTable("ImportacaoParcela");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.Companhia).HasColumnName("Companhia");
            this.Property(t => t.Filial).HasColumnName("Filial");
            this.Property(t => t.FilialDescricao).HasColumnName("FilialDescricao");
            this.Property(t => t.RotaArea).HasColumnName("RotaArea");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.TipoContrato).HasColumnName("TipoContrato");
            this.Property(t => t.NumeroCliente).HasColumnName("NumeroCliente");
            this.Property(t => t.CPFCNPJ).HasColumnName("CPFCNPJ");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.NumeroFatura).HasColumnName("NumeroFatura");
            this.Property(t => t.ValorFatura).HasColumnName("ValorFatura");
            this.Property(t => t.ValorAberto).HasColumnName("ValorAberto");
            this.Property(t => t.DataVencimento).HasColumnName("DataVencimento");
            this.Property(t => t.NumeroParcela).HasColumnName("NumeroParcela");
            this.Property(t => t.Governo).HasColumnName("Governo");
            this.Property(t => t.Corporativo).HasColumnName("Corporativo");
            this.Property(t => t.Mensagem).HasColumnName("Mensagem");
            this.Property(t => t.DataProcessamento).HasColumnName("DataProcessamento");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.NumeroContrato).HasColumnName("NumeroContrato");
            this.Property(t => t.DataFechamento).HasColumnName("DataFechamento");
            this.Property(t => t.CodParcela).HasColumnName("CodParcela");
            this.Property(t => t.CodSinteseCliente).HasColumnName("CodSinteseCliente");
            this.Property(t => t.NotaFiscal).HasColumnName("NotaFiscal");
            this.Property(t => t.TipoDocumento).HasColumnName("TipoDocumento");
            this.Property(t => t.InstrumentoPgto).HasColumnName("InstrumentoPgto");
            this.Property(t => t.DataPagamento).HasColumnName("DataPagamento");
            this.Property(t => t.StatusContrato).HasColumnName("StatusContrato");
            this.Property(t => t.DataEmissao).HasColumnName("DataEmissao");

            // Relationships
            this.HasOptional(t => t.Importacao)
                .WithMany(t => t.ImportacaoParcelas)
                .HasForeignKey(d => d.CodImportacao);

        }
    }
}
