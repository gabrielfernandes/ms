using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaBancoMap : EntityTypeConfiguration<TabelaBanco>
    {
        public TabelaBancoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeBanco)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaBanco");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.NomeBanco).HasColumnName("NomeBanco");
            this.Property(t => t.Numero).HasColumnName("Numero");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.NossoNumero).HasColumnName("NossoNumero");
            this.Property(t => t.NossoNumeroAtual).HasColumnName("NossoNumeroAtual");
            this.Property(t => t.Lote).HasColumnName("Lote");
            this.Property(t => t.LoteAtual).HasColumnName("LoteAtual");
            this.Property(t => t.BancoPadrao).HasColumnName("BancoPadrao");

        }
    }
}
