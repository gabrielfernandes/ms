using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoClienteMap : EntityTypeConfiguration<ContratoCliente>
    {
        public ContratoClienteMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContratoCliente");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.Ordem).HasColumnName("Ordem");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ContratoClientes)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ContratoClientes)
                .HasForeignKey(d => d.CodContrato);

        }
    }
}
