using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaAgingMap : EntityTypeConfiguration<TabelaAging>
    {
        public TabelaAgingMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Aging)
                .HasMaxLength(255);

            this.Property(t => t.Cor)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaAging");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Aging).HasColumnName("Aging");
            this.Property(t => t.AtrasoDe).HasColumnName("AtrasoDe");
            this.Property(t => t.AtrasoAte).HasColumnName("AtrasoAte");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Cor).HasColumnName("Cor");
        }
    }
}
