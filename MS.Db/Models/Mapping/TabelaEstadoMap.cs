using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEstadoMap : EntityTypeConfiguration<TabelaEstado>
    {
        public TabelaEstadoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Estado)
                .HasMaxLength(50);

            this.Property(t => t.Sigla)
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("TabelaEstado");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Estado).HasColumnName("Estado");
            this.Property(t => t.Sigla).HasColumnName("Sigla");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
