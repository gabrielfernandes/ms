using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ProspectInformacaoPatrimonialMap : EntityTypeConfiguration<ProspectInformacaoPatrimonial>
    {
        public ProspectInformacaoPatrimonialMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ProspectInformacaoPatrimonial");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodProspect).HasColumnName("CodProspect");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.TipoPatrimonio).HasColumnName("TipoPatrimonio");
            this.Property(t => t.ValorMercado).HasColumnName("ValorMercado");
            this.Property(t => t.Situacao).HasColumnName("Situacao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.PrazoRestante).HasColumnName("PrazoRestante");
            this.Property(t => t.ValorParcela).HasColumnName("ValorParcela");

            // Relationships
            this.HasOptional(t => t.PropostaProspect)
                .WithMany(t => t.ProspectInformacaoPatrimonials)
                .HasForeignKey(d => d.CodProspect);

        }
    }
}
