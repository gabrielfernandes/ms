using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoCadastroMap : EntityTypeConfiguration<TabelaTipoCadastro>
    {
        public TabelaTipoCadastroMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Descricao)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaTipoCadastro");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
