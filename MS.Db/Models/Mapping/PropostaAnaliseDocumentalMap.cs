using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class PropostaAnaliseDocumentalMap : EntityTypeConfiguration<PropostaAnaliseDocumental>
    {
        public PropostaAnaliseDocumentalMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("PropostaAnaliseDocumental");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodAnaliseDocumental).HasColumnName("CodAnaliseDocumental");
            this.Property(t => t.CodPropostaDocumento).HasColumnName("CodPropostaDocumento");
            this.Property(t => t.Valor).HasColumnName("Valor");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.Confere).HasColumnName("Confere");

            // Relationships
            this.HasOptional(t => t.PropostaDocumento)
                .WithMany(t => t.PropostaAnaliseDocumentals)
                .HasForeignKey(d => d.CodPropostaDocumento);
            this.HasOptional(t => t.TabelaAnaliseDocumental)
                .WithMany(t => t.PropostaAnaliseDocumentals)
                .HasForeignKey(d => d.CodAnaliseDocumental);

        }
    }
}
