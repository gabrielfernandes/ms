using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ClienteMap : EntityTypeConfiguration<Cliente>
    {
        public ClienteMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CodCliente)
                .HasMaxLength(55);

            this.Property(t => t.Nome)
                .HasMaxLength(255);

            this.Property(t => t.PrimeiroNome)
                .HasMaxLength(50);

            this.Property(t => t.CPF)
                .HasMaxLength(20);

            this.Property(t => t.RG)
                .HasMaxLength(20);

            this.Property(t => t.CNPJ)
                .HasMaxLength(30);

            this.Property(t => t.EstadoCivil)
                .HasMaxLength(25);

            this.Property(t => t.Profissao)
                .HasMaxLength(60);

            this.Property(t => t.Email)
                .HasMaxLength(150);

            this.Property(t => t.Nacionalidade)
                .HasMaxLength(50);

            this.Property(t => t.Naturalidade)
                .HasMaxLength(50);

            this.Property(t => t.Orgao)
                .HasMaxLength(50);

            this.Property(t => t.RegimeCasamento)
                .HasMaxLength(50);

            this.Property(t => t.CodClienteConjugeSAP)
                .HasMaxLength(55);

            this.Property(t => t.CelularPrincipal)
                .HasMaxLength(50);

            this.Property(t => t.CEP)
                .HasMaxLength(50);

            this.Property(t => t.Endereco)
                .HasMaxLength(500);

            this.Property(t => t.Cidade)
                .HasMaxLength(500);

            this.Property(t => t.Estado)
                .HasMaxLength(50);

            this.Property(t => t.Bairro)
                .HasMaxLength(50);

            this.Property(t => t.UF)
                .HasMaxLength(50);

            this.Property(t => t.EnderecoNumero)
                .HasMaxLength(10);

            this.Property(t => t.EnderecoComplemento)
                .HasMaxLength(500);

            this.Property(t => t.TelefoneResidencial)
                .HasMaxLength(50);

            this.Property(t => t.TelefoneComercial)
                .HasMaxLength(50);

            this.Property(t => t.Celular)
                .HasMaxLength(50);

            this.Property(t => t.CelularComercial)
                .HasMaxLength(50);

            this.Property(t => t.NomeResponsavel)
                .HasMaxLength(250);

            this.Property(t => t.InformacaoAdicional)
                .HasMaxLength(500);

            this.Property(t => t.Corporativo)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Cliente");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.PrimeiroNome).HasColumnName("PrimeiroNome");
            this.Property(t => t.CPF).HasColumnName("CPF");
            this.Property(t => t.RG).HasColumnName("RG");
            this.Property(t => t.CNPJ).HasColumnName("CNPJ");
            this.Property(t => t.EstadoCivil).HasColumnName("EstadoCivil");
            this.Property(t => t.Profissao).HasColumnName("Profissao");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.DataNascimento).HasColumnName("DataNascimento");
            this.Property(t => t.Nacionalidade).HasColumnName("Nacionalidade");
            this.Property(t => t.Naturalidade).HasColumnName("Naturalidade");
            this.Property(t => t.Orgao).HasColumnName("Orgao");
            this.Property(t => t.RegimeCasamento).HasColumnName("RegimeCasamento");
            this.Property(t => t.DataCasamento).HasColumnName("DataCasamento");
            this.Property(t => t.Sexo).HasColumnName("Sexo");
            this.Property(t => t.CodClienteConjuge).HasColumnName("CodClienteConjuge");
            this.Property(t => t.CodClienteConjugeSAP).HasColumnName("CodClienteConjugeSAP");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CelularPrincipal).HasColumnName("CelularPrincipal");
            this.Property(t => t.Negativado).HasColumnName("Negativado");
            this.Property(t => t.PessoaTipo).HasColumnName("PessoaTipo");
            this.Property(t => t.CEP).HasColumnName("CEP");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.Estado).HasColumnName("Estado");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.UF).HasColumnName("UF");
            this.Property(t => t.EnderecoNumero).HasColumnName("EnderecoNumero");
            this.Property(t => t.EnderecoComplemento).HasColumnName("EnderecoComplemento");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.TelefoneResidencial).HasColumnName("TelefoneResidencial");
            this.Property(t => t.TelefoneComercial).HasColumnName("TelefoneComercial");
            this.Property(t => t.Celular).HasColumnName("Celular");
            this.Property(t => t.CelularComercial).HasColumnName("CelularComercial");
            this.Property(t => t.DataAtendimento).HasColumnName("DataAtendimento");
            this.Property(t => t.CodParcelaAntiga).HasColumnName("CodParcelaAntiga");
            this.Property(t => t.DataFinalizacaoAtendimento).HasColumnName("DataFinalizacaoAtendimento");
            this.Property(t => t.CodUsuarioAtendimento).HasColumnName("CodUsuarioAtendimento");
            this.Property(t => t.CodUltimoHistorico).HasColumnName("CodUltimoHistorico");
            this.Property(t => t.QtdContrato).HasColumnName("QtdContrato");
            this.Property(t => t.CodAging).HasColumnName("CodAging");
            this.Property(t => t.DiasAtraso).HasColumnName("DiasAtraso");
            this.Property(t => t.DiasFase).HasColumnName("DiasFase");
            this.Property(t => t.DiasSintese).HasColumnName("DiasSintese");
            this.Property(t => t.ValorAtraso).HasColumnName("ValorAtraso");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.DataImportacao).HasColumnName("DataImportacao");
            this.Property(t => t.NomeResponsavel).HasColumnName("NomeResponsavel");
            this.Property(t => t.InformacaoAdicional).HasColumnName("InformacaoAdicional");
            this.Property(t => t.Corporativo).HasColumnName("Corporativo");

            // Relationships
            this.HasOptional(t => t.Cliente2)
                .WithMany(t => t.Cliente1)
                .HasForeignKey(d => d.CodClienteConjuge);
            this.HasOptional(t => t.TabelaAging)
                .WithMany(t => t.Clientes)
                .HasForeignKey(d => d.CodAging);

        }
    }
}
