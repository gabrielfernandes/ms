using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoEscritorioHistoricoMap : EntityTypeConfiguration<ContratoEscritorioHistorico>
    {
        public ContratoEscritorioHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContratoEscritorioHistorico");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.CodEscritorio).HasColumnName("CodEscritorio");
            this.Property(t => t.CodTipoCobranca).HasColumnName("CodTipoCobranca");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Cod).HasColumnName("Cod");

            // Relationships
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ContratoEscritorioHistoricoes)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.TabelaEscritorio)
                .WithMany(t => t.ContratoEscritorioHistoricoes)
                .HasForeignKey(d => d.CodEscritorio);
            this.HasOptional(t => t.TabelaTipoCobranca)
                .WithMany(t => t.ContratoEscritorioHistoricoes)
                .HasForeignKey(d => d.CodTipoCobranca);

        }
    }
}
