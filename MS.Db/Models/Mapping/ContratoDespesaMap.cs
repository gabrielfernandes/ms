using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoDespesaMap : EntityTypeConfiguration<ContratoDespesa>
    {
        public ContratoDespesaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContratoDespesa");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.CodTipoDespesa).HasColumnName("CodTipoDespesa");
            this.Property(t => t.Valor).HasColumnName("Valor");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.Antecipacao).HasColumnName("Antecipacao");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Data).HasColumnName("Data");

            // Relationships
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ContratoDespesas)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.TabelaTipoDespesa)
                .WithMany(t => t.ContratoDespesas)
                .HasForeignKey(d => d.CodTipoDespesa);

        }
    }
}
