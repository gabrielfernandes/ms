using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaParametroAtendimentoUsuarioMap : EntityTypeConfiguration<TabelaParametroAtendimentoUsuario>
    {
        public TabelaParametroAtendimentoUsuarioMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaParametroAtendimentoUsuario");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodParametroAtendimento).HasColumnName("CodParametroAtendimento");
            this.Property(t => t.CodEquipeUsuario).HasColumnName("CodEquipeUsuario");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodEquipe).HasColumnName("CodEquipe");

            // Relationships
            this.HasOptional(t => t.TabelaEquipe)
                .WithMany(t => t.TabelaParametroAtendimentoUsuarios)
                .HasForeignKey(d => d.CodEquipe);
            this.HasOptional(t => t.TabelaEquipeUsuario)
                .WithMany(t => t.TabelaParametroAtendimentoUsuarios)
                .HasForeignKey(d => d.CodEquipeUsuario);
            this.HasOptional(t => t.TabelaParametroAtendimento)
                .WithMany(t => t.TabelaParametroAtendimentoUsuarios)
                .HasForeignKey(d => d.CodParametroAtendimento);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.TabelaParametroAtendimentoUsuarios)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
