using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ClienteDocumentoMap : EntityTypeConfiguration<ClienteDocumento>
    {
        public ClienteDocumentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NumeroDocumento)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("ClienteDocumento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.NumeroDocumento).HasColumnName("NumeroDocumento");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.DataExpedicao).HasColumnName("DataExpedicao");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ClienteDocumentoes)
                .HasForeignKey(d => d.CodCliente);

        }
    }
}
