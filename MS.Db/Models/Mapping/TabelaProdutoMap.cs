using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaProdutoMap : EntityTypeConfiguration<TabelaProduto>
    {
        public TabelaProdutoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Produto)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaProduto");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodRegional).HasColumnName("CodRegional");
            this.Property(t => t.Produto).HasColumnName("Produto");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
        }
    }
}
