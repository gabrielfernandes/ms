using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ImportacaoClienteEnderecoMap : EntityTypeConfiguration<ImportacaoClienteEndereco>
    {
        public ImportacaoClienteEnderecoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Contrato)
                .HasMaxLength(255);

            this.Property(t => t.EnderecoCob)
                .HasMaxLength(255);

            this.Property(t => t.NumeroCob)
                .HasMaxLength(255);

            this.Property(t => t.ComplementoCob)
                .HasMaxLength(255);

            this.Property(t => t.BairroCob)
                .HasMaxLength(255);

            this.Property(t => t.CEPCob)
                .HasMaxLength(255);

            this.Property(t => t.CidadeCob)
                .HasMaxLength(255);

            this.Property(t => t.EstadoCob)
                .HasMaxLength(255);

            this.Property(t => t.EnderecoObra)
                .HasMaxLength(255);

            this.Property(t => t.NumeroObra)
                .HasMaxLength(255);

            this.Property(t => t.ComplementoObra)
                .HasMaxLength(255);

            this.Property(t => t.BairroObra)
                .HasMaxLength(255);

            this.Property(t => t.CepObra)
                .HasMaxLength(255);

            this.Property(t => t.CidadeObra)
                .HasMaxLength(255);

            this.Property(t => t.EstadoObra)
                .HasMaxLength(255);

            this.Property(t => t.Mensagem)
                .HasMaxLength(255);

            this.Property(t => t.MensagemContrato)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("ImportacaoClienteEndereco");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.Contrato).HasColumnName("Contrato");
            this.Property(t => t.EnderecoCob).HasColumnName("EnderecoCob");
            this.Property(t => t.NumeroCob).HasColumnName("NumeroCob");
            this.Property(t => t.ComplementoCob).HasColumnName("ComplementoCob");
            this.Property(t => t.BairroCob).HasColumnName("BairroCob");
            this.Property(t => t.CEPCob).HasColumnName("CEPCob");
            this.Property(t => t.CidadeCob).HasColumnName("CidadeCob");
            this.Property(t => t.EstadoCob).HasColumnName("EstadoCob");
            this.Property(t => t.EnderecoObra).HasColumnName("EnderecoObra");
            this.Property(t => t.NumeroObra).HasColumnName("NumeroObra");
            this.Property(t => t.ComplementoObra).HasColumnName("ComplementoObra");
            this.Property(t => t.BairroObra).HasColumnName("BairroObra");
            this.Property(t => t.CepObra).HasColumnName("CepObra");
            this.Property(t => t.CidadeObra).HasColumnName("CidadeObra");
            this.Property(t => t.EstadoObra).HasColumnName("EstadoObra");
            this.Property(t => t.Mensagem).HasColumnName("Mensagem");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DataProcessamento).HasColumnName("DataProcessamento");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.CodEnderecoCob).HasColumnName("CodEnderecoCob");
            this.Property(t => t.CodEnderecoObra).HasColumnName("CodEnderecoObra");
            this.Property(t => t.MensagemContrato).HasColumnName("MensagemContrato");

            // Relationships
            this.HasOptional(t => t.Importacao)
                .WithMany(t => t.ImportacaoClienteEnderecoes)
                .HasForeignKey(d => d.CodImportacao);

        }
    }
}
