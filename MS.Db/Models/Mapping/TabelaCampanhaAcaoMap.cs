using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaCampanhaAcaoMap : EntityTypeConfiguration<TabelaCampanhaAcao>
    {
        public TabelaCampanhaAcaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaCampanhaAcao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCampanha).HasColumnName("CodCampanha");
            this.Property(t => t.CodAcao).HasColumnName("CodAcao");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.Excecao).HasColumnName("Excecao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaAcaoCobranca)
                .WithMany(t => t.TabelaCampanhaAcaos)
                .HasForeignKey(d => d.CodAcao);
            this.HasOptional(t => t.TabelaCampanha)
                .WithMany(t => t.TabelaCampanhaAcaos)
                .HasForeignKey(d => d.CodCampanha);

        }
    }
}
