using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaModeloAcaoMap : EntityTypeConfiguration<TabelaModeloAcao>
    {
        public TabelaModeloAcaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Titulo)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaModeloAcao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodAcaoCobranca).HasColumnName("CodAcaoCobranca");
            this.Property(t => t.Modelo).HasColumnName("Modelo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Titulo).HasColumnName("Titulo");

            // Relationships
            this.HasOptional(t => t.TabelaAcaoCobranca)
                .WithMany(t => t.TabelaModeloAcaos)
                .HasForeignKey(d => d.CodAcaoCobranca);

        }
    }
}
