using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ProspectCompromissoFinanceiroMap : EntityTypeConfiguration<ProspectCompromissoFinanceiro>
    {
        public ProspectCompromissoFinanceiroMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.EmpresaCredora)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("ProspectCompromissoFinanceiro");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodProspect).HasColumnName("CodProspect");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.QtdPrestacaoRestante).HasColumnName("QtdPrestacaoRestante");
            this.Property(t => t.NumeroParcela).HasColumnName("NumeroParcela");
            this.Property(t => t.Valor).HasColumnName("Valor");
            this.Property(t => t.EmpresaCredora).HasColumnName("EmpresaCredora");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.PropostaProspect)
                .WithMany(t => t.ProspectCompromissoFinanceiroes)
                .HasForeignKey(d => d.CodProspect);

        }
    }
}
