using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class temp_importacao_creditoMap : EntityTypeConfiguration<temp_importacao_credito>
    {
        public temp_importacao_creditoMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.numero_documento)
                .HasMaxLength(50);

            this.Property(t => t.numero_cliente)
                .HasMaxLength(255);

            this.Property(t => t.descricao)
                .HasMaxLength(255);

            this.Property(t => t.data_credito)
                .HasMaxLength(255);

            this.Property(t => t.valor_credito)
                .HasMaxLength(255);

            this.Property(t => t.observação)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("temp_importacao_credito");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.numero_documento).HasColumnName("numero_documento");
            this.Property(t => t.numero_cliente).HasColumnName("numero_cliente");
            this.Property(t => t.descricao).HasColumnName("descricao");
            this.Property(t => t.data_credito).HasColumnName("data_credito");
            this.Property(t => t.valor_credito).HasColumnName("valor_credito");
            this.Property(t => t.observação).HasColumnName("observação");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.fgProcessados).HasColumnName("fgProcessados");
        }
    }
}
