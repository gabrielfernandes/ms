using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ProspectRendaMap : EntityTypeConfiguration<ProspectRenda>
    {
        public ProspectRendaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Cargo)
                .HasMaxLength(250);

            this.Property(t => t.TelefoneComercial)
                .HasMaxLength(20);

            this.Property(t => t.NomeEmpresa)
                .HasMaxLength(250);

            this.Property(t => t.CNPJ)
                .HasMaxLength(50);

            this.Property(t => t.Cep)
                .HasMaxLength(15);

            this.Property(t => t.Endereco)
                .HasMaxLength(350);

            this.Property(t => t.EnderecoNumero)
                .HasMaxLength(50);

            this.Property(t => t.EnderecoComplmento)
                .HasMaxLength(250);

            this.Property(t => t.Bairro)
                .HasMaxLength(100);

            this.Property(t => t.Cidade)
                .HasMaxLength(100);

            this.Property(t => t.UF)
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("ProspectRenda");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodProspect).HasColumnName("CodProspect");
            this.Property(t => t.TipoRenda).HasColumnName("TipoRenda");
            this.Property(t => t.Cargo).HasColumnName("Cargo");
            this.Property(t => t.DataAdmissao).HasColumnName("DataAdmissao");
            this.Property(t => t.TelefoneComercial).HasColumnName("TelefoneComercial");
            this.Property(t => t.ValorBruto).HasColumnName("ValorBruto");
            this.Property(t => t.NomeEmpresa).HasColumnName("NomeEmpresa");
            this.Property(t => t.CNPJ).HasColumnName("CNPJ");
            this.Property(t => t.Cep).HasColumnName("Cep");
            this.Property(t => t.EnderecoTipo).HasColumnName("EnderecoTipo");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.EnderecoNumero).HasColumnName("EnderecoNumero");
            this.Property(t => t.EnderecoComplmento).HasColumnName("EnderecoComplmento");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.UF).HasColumnName("UF");
            this.Property(t => t.DocumentoComprobatorio).HasColumnName("DocumentoComprobatorio");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.PropostaProspect)
                .WithMany(t => t.ProspectRendas)
                .HasForeignKey(d => d.CodProspect);

        }
    }
}
