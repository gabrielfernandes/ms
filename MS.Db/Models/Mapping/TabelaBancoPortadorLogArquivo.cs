using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaBancoPortadorLogArquivoMap : EntityTypeConfiguration<TabelaBancoPortadorLogArquivo>
    {
        public TabelaBancoPortadorLogArquivoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeDocumento)
                .HasMaxLength(250);

            this.Property(t => t.NumeroLote)
                .HasMaxLength(50);

            this.Property(t => t.Banco)
                .HasMaxLength(150);

            this.Property(t => t.Companhia)
                .HasMaxLength(150);

            // Table & Column Mappings
            this.ToTable("TabelaBancoPortadorLogArquivo");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodBancoPortador).HasColumnName("CodBancoPortador");
            this.Property(t => t.NomeDocumento).HasColumnName("NomeDocumento");
            this.Property(t => t.NumeroLote).HasColumnName("NumeroLote");
            this.Property(t => t.TipoArquivo).HasColumnName("TipoArquivo");
            this.Property(t => t.Banco).HasColumnName("Banco");
            this.Property(t => t.Companhia).HasColumnName("Companhia");
            this.Property(t => t.Arquivo).HasColumnName("Arquivo");
            this.Property(t => t.BoletoTotalArquivo).HasColumnName("BoletoTotalArquivo");
            this.Property(t => t.DataProcessamento).HasColumnName("DataProcessamento");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Log).HasColumnName("Log");

            // Relationships
            this.HasOptional(t => t.TabelaBancoPortador)
                .WithMany(t => t.TabelaBancoPortadorLogArquivos)
                .HasForeignKey(d => d.CodBancoPortador);

        }
    }
}
