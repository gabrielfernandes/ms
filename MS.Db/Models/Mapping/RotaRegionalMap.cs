using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class RotaRegionalMap : EntityTypeConfiguration<RotaRegional>
    {
        public RotaRegionalMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("RotaRegional");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodRota).HasColumnName("CodRota");
            this.Property(t => t.CodRegional).HasColumnName("CodRegional");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.Rota)
                .WithMany(t => t.RotaRegionals)
                .HasForeignKey(d => d.CodRota);
            this.HasOptional(t => t.TabelaRegional)
                .WithMany(t => t.RotaRegionals)
                .HasForeignKey(d => d.CodRegional);

        }
    }
}
