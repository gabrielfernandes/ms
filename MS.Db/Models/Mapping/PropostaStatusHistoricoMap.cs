using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class PropostaStatusHistoricoMap : EntityTypeConfiguration<PropostaStatusHistorico>
    {
        public PropostaStatusHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("PropostaStatusHistorico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodProposta).HasColumnName("CodProposta");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.Proposta)
                .WithMany(t => t.PropostaStatusHistoricoes)
                .HasForeignKey(d => d.CodProposta);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.PropostaStatusHistoricoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
