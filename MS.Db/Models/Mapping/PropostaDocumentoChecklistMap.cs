using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class PropostaDocumentoChecklistMap : EntityTypeConfiguration<PropostaDocumentoChecklist>
    {
        public PropostaDocumentoChecklistMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("PropostaDocumentoChecklist");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodDocumento).HasColumnName("CodDocumento");
            this.Property(t => t.CodChecklist).HasColumnName("CodChecklist");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.StatusAnalise).HasColumnName("StatusAnalise");

            // Relationships
            this.HasOptional(t => t.PropostaDocumento)
                .WithMany(t => t.PropostaDocumentoChecklists)
                .HasForeignKey(d => d.CodDocumento);
            this.HasOptional(t => t.TabelaCheckList)
                .WithMany(t => t.PropostaDocumentoChecklists)
                .HasForeignKey(d => d.CodChecklist);

        }
    }
}
