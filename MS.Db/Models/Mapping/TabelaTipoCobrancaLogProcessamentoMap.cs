using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoCobrancaLogProcessamentoMap : EntityTypeConfiguration<TabelaTipoCobrancaLogProcessamento>
    {
        public TabelaTipoCobrancaLogProcessamentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Guid)
                .HasMaxLength(500);

            this.Property(t => t.DescricaoOrdem)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("TabelaTipoCobrancaLogProcessamento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Guid).HasColumnName("Guid");
            this.Property(t => t.DescricaoOrdem).HasColumnName("DescricaoOrdem");

            // Relationships
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.TabelaTipoCobrancaLogProcessamentoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
