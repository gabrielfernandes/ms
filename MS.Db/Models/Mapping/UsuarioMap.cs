using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class UsuarioMap : EntityTypeConfiguration<Usuario>
    {
        public UsuarioMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Nome)
                .HasMaxLength(255);

            this.Property(t => t.Email)
                .HasMaxLength(100);

            this.Property(t => t.Login)
                .HasMaxLength(50);

            this.Property(t => t.Senha)
                .HasMaxLength(100);

            this.Property(t => t.SenhaTemporaria)
                .HasMaxLength(100);

            this.Property(t => t.MotivoBloqueio)
                .HasMaxLength(100);

            this.Property(t => t.IP)
                .HasMaxLength(100);

            this.Property(t => t.TelefoneResidencial)
                .HasMaxLength(50);

            this.Property(t => t.TelefoneComercial)
                .HasMaxLength(50);

            this.Property(t => t.TelefoneCelular)
                .HasMaxLength(50);

            this.Property(t => t.Endereco)
                .HasMaxLength(500);

            this.Property(t => t.CEP)
                .HasMaxLength(50);

            this.Property(t => t.EnderecoComplemento)
                .HasMaxLength(500);

            this.Property(t => t.EnderecoCidade)
                .HasMaxLength(255);

            this.Property(t => t.EnderecoBairro)
                .HasMaxLength(255);

            this.Property(t => t.EnderecoEstado)
                .HasMaxLength(255);

            this.Property(t => t.CNPJ)
                .HasMaxLength(500);

            this.Property(t => t.CPF)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Usuario");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Login).HasColumnName("Login");
            this.Property(t => t.Senha).HasColumnName("Senha");
            this.Property(t => t.SenhaTemporaria).HasColumnName("SenhaTemporaria");
            this.Property(t => t.PerfilTipo).HasColumnName("PerfilTipo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataPrimeiroAcesso).HasColumnName("DataPrimeiroAcesso");
            this.Property(t => t.DataUltimoAcesso).HasColumnName("DataUltimoAcesso");
            this.Property(t => t.DataBloqueado).HasColumnName("DataBloqueado");
            this.Property(t => t.MotivoBloqueio).HasColumnName("MotivoBloqueio");
            this.Property(t => t.IP).HasColumnName("IP");
            this.Property(t => t.DonoDeNegocio).HasColumnName("DonoDeNegocio");
            this.Property(t => t.DonoDeCarteira).HasColumnName("DonoDeCarteira");
            this.Property(t => t.Responsavel).HasColumnName("Responsavel");
            this.Property(t => t.Admin).HasColumnName("Admin");
            this.Property(t => t.ImportarDados).HasColumnName("ImportarDados");
            this.Property(t => t.ExportarDados).HasColumnName("ExportarDados");
            this.Property(t => t.CodConstrutora).HasColumnName("CodConstrutora");
            this.Property(t => t.CodEscritorio).HasColumnName("CodEscritorio");
            this.Property(t => t.CodEmpresaVenda).HasColumnName("CodEmpresaVenda");
            this.Property(t => t.SistemaCobranca).HasColumnName("SistemaCobranca");
            this.Property(t => t.SistemaProposta).HasColumnName("SistemaProposta");
            this.Property(t => t.SistemaCredito).HasColumnName("SistemaCredito");
            this.Property(t => t.SistemaRepasse).HasColumnName("SistemaRepasse");
            this.Property(t => t.SistemaRecebiveis).HasColumnName("SistemaRecebiveis");
            this.Property(t => t.TipoPessoa).HasColumnName("TipoPessoa");
            this.Property(t => t.TelefoneResidencial).HasColumnName("TelefoneResidencial");
            this.Property(t => t.TelefoneComercial).HasColumnName("TelefoneComercial");
            this.Property(t => t.TelefoneCelular).HasColumnName("TelefoneCelular");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.CEP).HasColumnName("CEP");
            this.Property(t => t.EnderecoNumero).HasColumnName("EnderecoNumero");
            this.Property(t => t.EnderecoComplemento).HasColumnName("EnderecoComplemento");
            this.Property(t => t.EnderecoCidade).HasColumnName("EnderecoCidade");
            this.Property(t => t.EnderecoBairro).HasColumnName("EnderecoBairro");
            this.Property(t => t.EnderecoEstado).HasColumnName("EnderecoEstado");
            this.Property(t => t.CNPJ).HasColumnName("CNPJ");
            this.Property(t => t.PermissaoProspect).HasColumnName("PermissaoProspect");
            this.Property(t => t.PermissaoProposta).HasColumnName("PermissaoProposta");
            this.Property(t => t.CPF).HasColumnName("CPF");
            this.Property(t => t.CodRegional).HasColumnName("CodRegional");
            this.Property(t => t.IdiomaTipo).HasColumnName("IdiomaTipo");
            this.Property(t => t.PermissaoReguaSoberana).HasColumnName("PermissaoReguaSoberana");
            this.Property(t => t.Bloqueado).HasColumnName("Bloqueado");

            // Relationships
            this.HasOptional(t => t.TabelaEmpresaVenda)
                .WithMany(t => t.Usuarios)
                .HasForeignKey(d => d.CodEmpresaVenda);
            this.HasOptional(t => t.TabelaEscritorio)
                .WithMany(t => t.Usuarios)
                .HasForeignKey(d => d.CodEscritorio);
            this.HasOptional(t => t.TabelaRegional)
                .WithMany(t => t.Usuarios)
                .HasForeignKey(d => d.CodRegional);

        }
    }
}
