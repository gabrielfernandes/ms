using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class AcordoContratoParcelaMap : EntityTypeConfiguration<AcordoContratoParcela>
    {
        public AcordoContratoParcelaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("AcordoContratoParcela");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodAcordo).HasColumnName("CodAcordo");
            this.Property(t => t.CodContratoParcela).HasColumnName("CodContratoParcela");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.Acordo)
                .WithMany(t => t.AcordoContratoParcelas)
                .HasForeignKey(d => d.CodAcordo);
            this.HasOptional(t => t.ContratoParcela)
                .WithMany(t => t.AcordoContratoParcelas)
                .HasForeignKey(d => d.CodContratoParcela);

        }
    }
}
