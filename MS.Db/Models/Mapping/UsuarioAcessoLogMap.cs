using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class UsuarioAcessoLogMap : EntityTypeConfiguration<UsuarioAcessoLog>
    {
        public UsuarioAcessoLogMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.IP)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("UsuarioAcessoLog");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataAcesso).HasColumnName("DataAcesso");
            this.Property(t => t.IP).HasColumnName("IP");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
        }
    }
}
