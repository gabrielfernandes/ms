using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEmpreendimentoDocumentoMap : EntityTypeConfiguration<TabelaEmpreendimentoDocumento>
    {
        public TabelaEmpreendimentoDocumentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.ArquivoObservacao)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoCaminho)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoNome)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoGuid)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaEmpreendimentoDocumento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodEmpreendimento).HasColumnName("CodEmpreendimento");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.ArquivoObservacao).HasColumnName("ArquivoObservacao");
            this.Property(t => t.ArquivoCaminho).HasColumnName("ArquivoCaminho");
            this.Property(t => t.ArquivoNome).HasColumnName("ArquivoNome");
            this.Property(t => t.ArquivoGuid).HasColumnName("ArquivoGuid");
            this.Property(t => t.DataVigencia).HasColumnName("DataVigencia");

            // Relationships
            this.HasOptional(t => t.TabelaEmpreendimento)
                .WithMany(t => t.TabelaEmpreendimentoDocumentoes)
                .HasForeignKey(d => d.CodEmpreendimento);

        }
    }
}
