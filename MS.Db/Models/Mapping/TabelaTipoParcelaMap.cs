using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoParcelaMap : EntityTypeConfiguration<TabelaTipoParcela>
    {
        public TabelaTipoParcelaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TipoParcela)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaTipoParcela");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.TipoParcela).HasColumnName("TipoParcela");
            this.Property(t => t.QtdMes).HasColumnName("QtdMes");
            this.Property(t => t.TipoPeriodo).HasColumnName("TipoPeriodo");
        }
    }
}
