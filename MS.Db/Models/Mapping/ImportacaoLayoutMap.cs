using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ImportacaoLayoutMap : EntityTypeConfiguration<ImportacaoLayout>
    {
        public ImportacaoLayoutMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CodEmpreendimentoCliente)
                .HasMaxLength(50);

            this.Property(t => t.CodBlocoCliente)
                .HasMaxLength(50);

            this.Property(t => t.NumeroUnidade)
                .HasMaxLength(50);

            this.Property(t => t.NumeroContrato)
                .HasMaxLength(50);

            this.Property(t => t.CodCliente)
                .HasMaxLength(50);

            this.Property(t => t.NomeCliente)
                .HasMaxLength(50);

            this.Property(t => t.CPF)
                .HasMaxLength(50);

            this.Property(t => t.FluxoPagamento)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ImportacaoLayout");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodEmpreendimentoCliente).HasColumnName("CodEmpreendimentoCliente");
            this.Property(t => t.CodBlocoCliente).HasColumnName("CodBlocoCliente");
            this.Property(t => t.NumeroUnidade).HasColumnName("NumeroUnidade");
            this.Property(t => t.NumeroContrato).HasColumnName("NumeroContrato");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.NomeCliente).HasColumnName("NomeCliente");
            this.Property(t => t.CPF).HasColumnName("CPF");
            this.Property(t => t.ValorVenda).HasColumnName("ValorVenda");
            this.Property(t => t.FluxoPagamento).HasColumnName("FluxoPagamento");
            this.Property(t => t.ValorParcela).HasColumnName("ValorParcela");
            this.Property(t => t.ValorAberto).HasColumnName("ValorAberto");
            this.Property(t => t.DataVencimento).HasColumnName("DataVencimento");
            this.Property(t => t.DataPagamento).HasColumnName("DataPagamento");
            this.Property(t => t.ValorPagamento).HasColumnName("ValorPagamento");
        }
    }
}
