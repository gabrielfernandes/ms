using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ParametroPlataformaMap : EntityTypeConfiguration<ParametroPlataforma>
    {
        public ParametroPlataformaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TituloPagina)
                .HasMaxLength(50);

            this.Property(t => t.EmailRemetente)
                .HasMaxLength(500);

            this.Property(t => t.NomeRemetente)
                .HasMaxLength(500);

            this.Property(t => t.SMTPServidor)
                .HasMaxLength(255);

            this.Property(t => t.SMTPUsuario)
                .HasMaxLength(255);

            this.Property(t => t.SMTPSenha)
                .HasMaxLength(255);

            this.Property(t => t.CaminhoLogo)
                .HasMaxLength(500);

            
            this.Property(t => t.NomeEmpresa)
                .HasMaxLength(150);

            this.Property(t => t.UrlSistema)
                .HasMaxLength(150);

            this.Property(t => t.TmNow)
                .HasMaxLength(500);
            this.Property(t => t.TmOff)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ParametroPlataforma");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Logo).HasColumnName("Logo");
            this.Property(t => t.TituloPagina).HasColumnName("TituloPagina");
            this.Property(t => t.LayoutTipo).HasColumnName("LayoutTipo");
            this.Property(t => t.EmailRemetente).HasColumnName("EmailRemetente");
            this.Property(t => t.NomeRemetente).HasColumnName("NomeRemetente");
            this.Property(t => t.SMTPPorta).HasColumnName("SMTPPorta");
            this.Property(t => t.SMTPServidor).HasColumnName("SMTPServidor");
            this.Property(t => t.SMTPUsuario).HasColumnName("SMTPUsuario");
            this.Property(t => t.SMTPSenha).HasColumnName("SMTPSenha");
            this.Property(t => t.fgSSL).HasColumnName("fgSSL");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CaminhoLogo).HasColumnName("CaminhoLogo");
            this.Property(t => t.Layout).HasColumnName("Layout");
            this.Property(t => t.Icone).HasColumnName("Icone");
            this.Property(t => t.TmNow).HasColumnName("TmNow");
            this.Property(t => t.TmOff).HasColumnName("TmOff");
            this.Property(t => t.NomeEmpresa).HasColumnName("NomeEmpresa");
            this.Property(t => t.UrlSistema).HasColumnName("UrlSistema");
        }
    }
}
