using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoRecebimentoMap : EntityTypeConfiguration<ContratoRecebimento>
    {
        public ContratoRecebimentoMap()
        {
            // Primary Key
            this.HasKey(t => new { t.CodContrato, t.FluxoPagamento, t.DtVencimento });

            // Properties
            this.Property(t => t.CodContrato)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.FluxoPagamento)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Natureza)
                .HasMaxLength(30);

            this.Property(t => t.LoginImportacao)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("ContratoRecebimento");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.FluxoPagamento).HasColumnName("FluxoPagamento");
            this.Property(t => t.DtVencimento).HasColumnName("DtVencimento");
            this.Property(t => t.DtRecebimento).HasColumnName("DtRecebimento");
            this.Property(t => t.Natureza).HasColumnName("Natureza");
            this.Property(t => t.ValorParcela).HasColumnName("ValorParcela");
            this.Property(t => t.ValorRecebimento).HasColumnName("ValorRecebimento");
            this.Property(t => t.DataImportacao).HasColumnName("DataImportacao");
            this.Property(t => t.LoginImportacao).HasColumnName("LoginImportacao");
        }
    }
}
