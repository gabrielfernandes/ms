using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaVendaFluxoMap : EntityTypeConfiguration<TabelaVendaFluxo>
    {
        public TabelaVendaFluxoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaVendaFluxo");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodTabelaVenda).HasColumnName("CodTabelaVenda");
            this.Property(t => t.CodTipoParcela).HasColumnName("CodTipoParcela");
            this.Property(t => t.Ordem).HasColumnName("Ordem");
            this.Property(t => t.PercentualMinimo).HasColumnName("PercentualMinimo");
            this.Property(t => t.Valor).HasColumnName("Valor");

            // Relationships
            this.HasOptional(t => t.TabelaTipoParcela)
                .WithMany(t => t.TabelaVendaFluxoes)
                .HasForeignKey(d => d.CodTipoParcela);
            this.HasOptional(t => t.TabelaVenda)
                .WithMany(t => t.TabelaVendaFluxoes)
                .HasForeignKey(d => d.CodTabelaVenda);

        }
    }
}
