using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ClienteAtendimentoMap : EntityTypeConfiguration<ClienteAtendimento>
    {
        public ClienteAtendimentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ClienteAtendimento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataAtendimento).HasColumnName("DataAtendimento");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataFinalizacaoAtendimento).HasColumnName("DataFinalizacaoAtendimento");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ClienteAtendimentoes)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ClienteAtendimentoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
