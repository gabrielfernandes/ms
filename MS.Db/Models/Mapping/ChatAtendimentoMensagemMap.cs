using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ChatAtendimentoMensagemMap : EntityTypeConfiguration<ChatAtendimentoMensagem>
    {
        public ChatAtendimentoMensagemMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Nome)
                .HasMaxLength(255);

            this.Property(t => t.Arquivo)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoNome)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ChatAtendimentoMensagem");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataRecebida).HasColumnName("DataRecebida");
            this.Property(t => t.DataLida).HasColumnName("DataLida");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.CodigoAtentimento).HasColumnName("CodigoAtentimento");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.Arquivo).HasColumnName("Arquivo");
            this.Property(t => t.ArquivoNome).HasColumnName("ArquivoNome");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.ChatAtendimento)
                .WithMany(t => t.ChatAtendimentoMensagems)
                .HasForeignKey(d => d.CodigoAtentimento);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ChatAtendimentoMensagems)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
