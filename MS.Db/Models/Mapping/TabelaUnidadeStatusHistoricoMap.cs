using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaUnidadeStatusHistoricoMap : EntityTypeConfiguration<TabelaUnidadeStatusHistorico>
    {
        public TabelaUnidadeStatusHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Observacao)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TabelaUnidadeStatusHistorico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodUnidade).HasColumnName("CodUnidade");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaUnidade)
                .WithMany(t => t.TabelaUnidadeStatusHistoricoes)
                .HasForeignKey(d => d.CodUnidade);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.TabelaUnidadeStatusHistoricoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
