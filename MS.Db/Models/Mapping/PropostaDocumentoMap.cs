using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class PropostaDocumentoMap : EntityTypeConfiguration<PropostaDocumento>
    {
        public PropostaDocumentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.ArquivoCaminho)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoNome)
                .HasMaxLength(500);

            this.Property(t => t.ArquivoGuid)
                .HasMaxLength(255);

            this.Property(t => t.ArquivoObservacao)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("PropostaDocumento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodTipoDocumento).HasColumnName("CodTipoDocumento");
            this.Property(t => t.CodProposta).HasColumnName("CodProposta");
            this.Property(t => t.CodProspect).HasColumnName("CodProspect");
            this.Property(t => t.ArquivoCaminho).HasColumnName("ArquivoCaminho");
            this.Property(t => t.ArquivoNome).HasColumnName("ArquivoNome");
            this.Property(t => t.ArquivoGuid).HasColumnName("ArquivoGuid");
            this.Property(t => t.ArquivoObservacao).HasColumnName("ArquivoObservacao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DataVigencia).HasColumnName("DataVigencia");

            // Relationships
            this.HasOptional(t => t.Proposta)
                .WithMany(t => t.PropostaDocumentoes)
                .HasForeignKey(d => d.CodProposta);
            this.HasOptional(t => t.PropostaProspect)
                .WithMany(t => t.PropostaDocumentoes)
                .HasForeignKey(d => d.CodProspect);
            this.HasOptional(t => t.TabelaTipoDocumento)
                .WithMany(t => t.PropostaDocumentoes)
                .HasForeignKey(d => d.CodTipoDocumento);

        }
    }
}
