using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaRegionalMap : EntityTypeConfiguration<TabelaRegional>
    {
        public TabelaRegionalMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Regional)
                .HasMaxLength(255);

            this.Property(t => t.CodRegionalCliente)
                .HasMaxLength(100);

            this.Property(t => t.Endereco)
                .HasMaxLength(255);

            this.Property(t => t.Numero)
                .HasMaxLength(255);

            this.Property(t => t.Complemento)
                .HasMaxLength(255);

            this.Property(t => t.Cidade)
                .HasMaxLength(255);

            this.Property(t => t.Bairro)
                .HasMaxLength(255);

            this.Property(t => t.Estado)
                .HasMaxLength(255);

            this.Property(t => t.Pais)
                .HasMaxLength(255);

            this.Property(t => t.Telefone)
                .HasMaxLength(255);

            this.Property(t => t.Email)
                .HasMaxLength(255);

            this.Property(t => t.Responsavel)
                .HasMaxLength(255);

            this.Property(t => t.CEP)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaRegional");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodConstrutora).HasColumnName("CodConstrutora");
            this.Property(t => t.CodEndereco).HasColumnName("CodEndereco");
            this.Property(t => t.Regional).HasColumnName("Regional");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodRegionalCliente).HasColumnName("CodRegionalCliente");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.Numero).HasColumnName("Numero");
            this.Property(t => t.Complemento).HasColumnName("Complemento");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.Estado).HasColumnName("Estado");
            this.Property(t => t.Pais).HasColumnName("Pais");
            this.Property(t => t.Telefone).HasColumnName("Telefone");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Responsavel).HasColumnName("Responsavel");
            this.Property(t => t.CEP).HasColumnName("CEP");
            this.Property(t => t.TipoRegiao).HasColumnName("TipoRegiao");

            // Relationships
            this.HasOptional(t => t.TabelaConstrutora)
                .WithMany(t => t.TabelaRegionals)
                .HasForeignKey(d => d.CodConstrutora);
            this.HasOptional(t => t.TabelaEndereco)
                .WithMany(t => t.TabelaRegionals)
                .HasForeignKey(d => d.CodEndereco);

        }
    }
}
