using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoDespesaMap : EntityTypeConfiguration<TabelaTipoDespesa>
    {
        public TabelaTipoDespesaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TipoDespesa)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaTipoDespesa");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.TipoDespesa).HasColumnName("TipoDespesa");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
