using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ClienteParcelaCreditoMap : EntityTypeConfiguration<ClienteParcelaCredito>
    {
        public ClienteParcelaCreditoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NumeroDocumento)
                .HasMaxLength(100);

            this.Property(t => t.FluxoPagamento)
                .HasMaxLength(255);

            this.Property(t => t.Observacao)
                .HasMaxLength(500);

            this.Property(t => t.GuidImportacao)
                .HasMaxLength(500);

            this.Property(t => t.Descricao)
               .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ClienteParcelaCredito");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.NumeroDocumento).HasColumnName("NumeroDocumento");
            this.Property(t => t.FluxoPagamento).HasColumnName("FluxoPagamento");
            this.Property(t => t.DataPagamento).HasColumnName("DataPagamento");
            this.Property(t => t.ValorRecebido).HasColumnName("ValorRecebido");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.GuidImportacao).HasColumnName("GuidImportacao");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.DataImportacao).HasColumnName("DataImportacao");
            this.Property(t => t.SaldoCredito).HasColumnName("SaldoCredito");
            this.Property(t => t.Descricao).HasColumnName("Descricao");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ClienteParcelaCreditoes)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ClienteParcelaCreditoes)
                .HasForeignKey(d => d.CodContrato);

        }
    }
}
