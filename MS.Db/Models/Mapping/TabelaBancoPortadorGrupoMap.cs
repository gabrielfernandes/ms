using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaBancoPortadorGrupoMap : EntityTypeConfiguration<TabelaBancoPortadorGrupo>
    {
        public TabelaBancoPortadorGrupoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeArquivo)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaBancoPortadorGrupo");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.NomeArquivo).HasColumnName("NomeArquivo");
            this.Property(t => t.NumeroLote).HasColumnName("NumeroLote");
            this.Property(t => t.NumeroLoteAtual).HasColumnName("NumeroLoteAtual");

            
        }
    }
}
