using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaDiaMap : EntityTypeConfiguration<TabelaDia>
    {
        public TabelaDiaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomePrazo)
                .HasMaxLength(350);

            // Table & Column Mappings
            this.ToTable("TabelaDias");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.NomePrazo).HasColumnName("NomePrazo");
            this.Property(t => t.DiasDe).HasColumnName("DiasDe");
            this.Property(t => t.DiasAte).HasColumnName("DiasAte");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
