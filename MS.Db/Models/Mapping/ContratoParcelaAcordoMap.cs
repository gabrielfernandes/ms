using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoParcelaAcordoMap : EntityTypeConfiguration<ContratoParcelaAcordo>
    {
        public ContratoParcelaAcordoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.FluxoPagamento)
                .HasMaxLength(250);

            this.Property(t => t.Variavel)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ContratoParcelaAcordo");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodContratoParcela).HasColumnName("CodContratoParcela");
            this.Property(t => t.FluxoPagamento).HasColumnName("FluxoPagamento");
            this.Property(t => t.DataVencimento).HasColumnName("DataVencimento");
            this.Property(t => t.DataPagamento).HasColumnName("DataPagamento");
            this.Property(t => t.ValorParcela).HasColumnName("ValorParcela");
            this.Property(t => t.ValorRecebido).HasColumnName("ValorRecebido");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.Juros).HasColumnName("Juros");
            this.Property(t => t.Multa).HasColumnName("Multa");
            this.Property(t => t.Honorario).HasColumnName("Honorario");
            this.Property(t => t.Desconto).HasColumnName("Desconto");
            this.Property(t => t.Total).HasColumnName("Total");
            this.Property(t => t.Variavel).HasColumnName("Variavel");
            this.Property(t => t.CodContratoAcordo).HasColumnName("CodContratoAcordo");

            // Relationships
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ContratoParcelaAcordoes)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.ContratoAcordo)
                .WithMany(t => t.ContratoParcelaAcordoes)
                .HasForeignKey(d => d.CodContratoAcordo);
            this.HasOptional(t => t.ContratoParcela)
                .WithMany(t => t.ContratoParcelaAcordoes)
                .HasForeignKey(d => d.CodContratoParcela);

        }
    }
}
