using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ParametroAtendimentoMap : EntityTypeConfiguration<ParametroAtendimento>
    {
        public ParametroAtendimentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ParametroAtendimento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.CodRegional).HasColumnName("CodRegional");

            // Relationships
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ParametroAtendimentoes)
                .HasForeignKey(d => d.CodUsuario);
            this.HasOptional(t => t.TabelaRegional)
                .WithMany(t => t.ParametroAtendimentoes)
                .HasForeignKey(d => d.CodRegional);

        }
    }
}
