using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaParametroAtendimentoMap : EntityTypeConfiguration<TabelaParametroAtendimento>
    {
        public TabelaParametroAtendimentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeEstrategia)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaParametroAtendimento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodSintese).HasColumnName("CodSintese");
            this.Property(t => t.CodTipoCobrancaAcao).HasColumnName("CodTipoCobrancaAcao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Login).HasColumnName("Login");
            this.Property(t => t.CondicaoQuery).HasColumnName("CondicaoQuery");
            this.Property(t => t.NomeEstrategia).HasColumnName("NomeEstrategia");

            // Relationships
            this.HasOptional(t => t.TabelaSintese)
                .WithMany(t => t.TabelaParametroAtendimentoes)
                .HasForeignKey(d => d.CodSintese);
            this.HasOptional(t => t.TabelaTipoCobrancaAcao)
                .WithMany(t => t.TabelaParametroAtendimentoes)
                .HasForeignKey(d => d.CodTipoCobrancaAcao);

        }
    }
}
