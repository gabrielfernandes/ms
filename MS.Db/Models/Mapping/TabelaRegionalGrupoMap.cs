using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaRegionalGrupoMap : EntityTypeConfiguration<TabelaRegionalGrupo>
    {
        public TabelaRegionalGrupoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CodRegionalCliente)
                .HasMaxLength(50);
            // Properties
            this.Property(t => t.Descricao)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("TabelaRegionalGrupo");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodRegional).HasColumnName("CodRegional");
            this.Property(t => t.CodConstrutora).HasColumnName("CodConstrutora");
            this.Property(t => t.CodRegionalCliente).HasColumnName("CodRegionalCliente");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.TipoRegiao).HasColumnName("TipoRegiao");

            // Relationships
            this.HasOptional(t => t.TabelaConstrutora)
                .WithMany(t => t.TabelaRegionalGrupoes)
                .HasForeignKey(d => d.CodConstrutora);
            this.HasOptional(t => t.TabelaRegional)
                .WithMany(t => t.TabelaRegionalGrupoes)
                .HasForeignKey(d => d.CodRegional);

        }
    }
}
