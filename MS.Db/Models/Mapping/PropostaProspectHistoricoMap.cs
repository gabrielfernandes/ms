using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class PropostaProspectHistoricoMap : EntityTypeConfiguration<PropostaProspectHistorico>
    {
        public PropostaProspectHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("PropostaProspectHistorico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodPropostaProspect).HasColumnName("CodPropostaProspect");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodSintese).HasColumnName("CodSintese");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");

            // Relationships
            this.HasOptional(t => t.PropostaProspect)
                .WithMany(t => t.PropostaProspectHistoricoes)
                .HasForeignKey(d => d.CodPropostaProspect);
            this.HasOptional(t => t.TabelaSintese)
                .WithMany(t => t.PropostaProspectHistoricoes)
                .HasForeignKey(d => d.CodSintese);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.PropostaProspectHistoricoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
