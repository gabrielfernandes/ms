using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ChatAtendimentoMap : EntityTypeConfiguration<ChatAtendimento>
    {
        public ChatAtendimentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Nome)
                .HasMaxLength(255);

            this.Property(t => t.Email)
                .HasMaxLength(500);

            this.Property(t => t.Telefone)
                .HasMaxLength(50);

            this.Property(t => t.IP)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ChatAtendimento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.Descricao).HasColumnName("Descricao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAtendimento).HasColumnName("DataAtendimento");
            this.Property(t => t.DataEncerrado).HasColumnName("DataEncerrado");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Telefone).HasColumnName("Telefone");
            this.Property(t => t.IP).HasColumnName("IP");
            this.Property(t => t.CodResultado).HasColumnName("CodResultado");
            this.Property(t => t.CodAtendimentoTipo).HasColumnName("CodAtendimentoTipo");
            this.Property(t => t.CodProposta).HasColumnName("CodProposta");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.ChatAtendimentoResultado)
                .WithMany(t => t.ChatAtendimentoes)
                .HasForeignKey(d => d.CodResultado);
            this.HasOptional(t => t.ChatAtendimentoTipo)
                .WithMany(t => t.ChatAtendimentoes)
                .HasForeignKey(d => d.CodAtendimentoTipo);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ChatAtendimentoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
