using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoCobrancaAcaoMap : EntityTypeConfiguration<TabelaTipoCobrancaAcao>
    {
        public TabelaTipoCobrancaAcaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Titulo)
                .HasMaxLength(2550);

            // Table & Column Mappings
            this.ToTable("TabelaTipoCobrancaAcao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodTipoCobranca).HasColumnName("CodTipoCobranca");
            this.Property(t => t.CodAcaoCobranca).HasColumnName("CodAcaoCobranca");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Titulo).HasColumnName("Titulo");
            this.Property(t => t.Modelo).HasColumnName("Modelo");
            this.Property(t => t.PeriodoEmDias).HasColumnName("PeriodoEmDias");
            this.Property(t => t.CondicaoTipo).HasColumnName("CondicaoTipo");
            this.Property(t => t.CampoTipo).HasColumnName("CampoTipo");
            this.Property(t => t.CondicaoQuery).HasColumnName("CondicaoQuery");
            this.Property(t => t.AcaoTipo).HasColumnName("AcaoTipo");
            this.Property(t => t.AcaoHistoricoTipo).HasColumnName("AcaoHistoricoTipo");
            this.Property(t => t.CodFase).HasColumnName("CodFase");
            this.Property(t => t.CodSintese).HasColumnName("CodSintese");
            this.Property(t => t.AcaoHistoricoObservacao).HasColumnName("AcaoHistoricoObservacao");
            this.Property(t => t.AgruparProcessosNegativacao).HasColumnName("AgruparProcessosNegativacao");
            this.Property(t => t.DiasAtrasoDeNegativacao).HasColumnName("DiasAtrasoDeNegativacao");
            this.Property(t => t.DiasAtrasoAteNegativacao).HasColumnName("DiasAtrasoAteNegativacao");
            this.Property(t => t.CodFilial).HasColumnName("CodFilial");
            this.Property(t => t.CodTipoCobrancaAcaoSoberana).HasColumnName("CodTipoCobrancaAcaoSoberana");

            // Relationships
            this.HasOptional(t => t.TabelaAcaoCobranca)
                .WithMany(t => t.TabelaTipoCobrancaAcaos)
                .HasForeignKey(d => d.CodAcaoCobranca);
            this.HasOptional(t => t.TabelaTipoCobranca)
                .WithMany(t => t.TabelaTipoCobrancaAcaos)
                .HasForeignKey(d => d.CodTipoCobranca);

        }
    }
}
