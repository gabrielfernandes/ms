using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ImportacaoMap : EntityTypeConfiguration<Importacao>
    {
        public ImportacaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeArquivo)
                .HasMaxLength(250);

            this.Property(t => t.GUID)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Importacao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.TotalRegistros).HasColumnName("TotalRegistros");
            this.Property(t => t.TotalRegistrosOK).HasColumnName("TotalRegistrosOK");
            this.Property(t => t.TotalRegistrosErro).HasColumnName("TotalRegistrosErro");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DataProcessamentoAgendado).HasColumnName("DataProcessamentoAgendado");
            this.Property(t => t.DataProcessado).HasColumnName("DataProcessado");
            this.Property(t => t.TotalRegistrosInconsistentes).HasColumnName("TotalRegistrosInconsistentes");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.NomeArquivo).HasColumnName("NomeArquivo");
            this.Property(t => t.GUID).HasColumnName("GUID");
            this.Property(t => t.CodImportacaoGeral).HasColumnName("CodImportacaoGeral");
            this.Property(t => t.Mensagem).HasColumnName("Mensagem");
            this.Property(t => t.TotalRegistrosProcessado).HasColumnName("TotalRegistrosProcessado");

            // Relationships
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.Importacaos)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
