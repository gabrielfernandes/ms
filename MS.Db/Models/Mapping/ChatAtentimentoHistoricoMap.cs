using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ChatAtentimentoHistoricoMap : EntityTypeConfiguration<ChatAtentimentoHistorico>
    {
        public ChatAtentimentoHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ChatAtentimentoHistorico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodSintese).HasColumnName("CodSintese");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataAgenda).HasColumnName("DataAgenda");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.ChatAtendimento)
                .WithMany(t => t.ChatAtentimentoHistoricoes)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ChatAtentimentoHistoricoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
