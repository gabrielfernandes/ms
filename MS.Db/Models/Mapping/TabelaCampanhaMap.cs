using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaCampanhaMap : EntityTypeConfiguration<TabelaCampanha>
    {
        public TabelaCampanhaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Campanha)
                .HasMaxLength(255);

            this.Property(t => t.Produto)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaCampanha");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Campanha).HasColumnName("Campanha");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataInicio).HasColumnName("DataInicio");
            this.Property(t => t.DataTerminio).HasColumnName("DataTerminio");
            this.Property(t => t.Produto).HasColumnName("Produto");
            this.Property(t => t.Condicao).HasColumnName("Condicao");
            this.Property(t => t.Excecao).HasColumnName("Excecao");
        }
    }
}
