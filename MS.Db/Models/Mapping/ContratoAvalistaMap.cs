using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoAvalistaMap : EntityTypeConfiguration<ContratoAvalista>
    {
        public ContratoAvalistaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeAvalista)
                .HasMaxLength(250);

            this.Property(t => t.CPF)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(200);

            this.Property(t => t.Profissao)
                .HasMaxLength(100);

            this.Property(t => t.NumeroDocumento)
                .HasMaxLength(50);

            this.Property(t => t.TelefoneResidencial)
                .HasMaxLength(15);

            this.Property(t => t.TelefoneCelular)
                .HasMaxLength(15);

            this.Property(t => t.Cep)
                .HasMaxLength(15);

            this.Property(t => t.Endereco)
                .HasMaxLength(250);

            this.Property(t => t.EnderecoNumero)
                .HasMaxLength(50);

            this.Property(t => t.EnderecoComplemento)
                .HasMaxLength(50);

            this.Property(t => t.Bairro)
                .HasMaxLength(250);

            this.Property(t => t.Cidade)
                .HasMaxLength(250);

            this.Property(t => t.UF)
                .HasMaxLength(2);

            this.Property(t => t.Nacionalidade)
                .HasMaxLength(50);

            this.Property(t => t.Naturalidade)
                .HasMaxLength(50);

            this.Property(t => t.TelefoneComercial)
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("ContratoAvalista");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.NomeAvalista).HasColumnName("NomeAvalista");
            this.Property(t => t.CPF).HasColumnName("CPF");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.DataNascimento).HasColumnName("DataNascimento");
            this.Property(t => t.EstadoCivil).HasColumnName("EstadoCivil");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Profissao).HasColumnName("Profissao");
            this.Property(t => t.NumeroDocumento).HasColumnName("NumeroDocumento");
            this.Property(t => t.TelefoneResidencial).HasColumnName("TelefoneResidencial");
            this.Property(t => t.TelefoneCelular).HasColumnName("TelefoneCelular");
            this.Property(t => t.Cep).HasColumnName("Cep");
            this.Property(t => t.EnderecoTipo).HasColumnName("EnderecoTipo");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.EnderecoNumero).HasColumnName("EnderecoNumero");
            this.Property(t => t.EnderecoComplemento).HasColumnName("EnderecoComplemento");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.UF).HasColumnName("UF");
            this.Property(t => t.Renda).HasColumnName("Renda");
            this.Property(t => t.Negativado).HasColumnName("Negativado");
            this.Property(t => t.Obs).HasColumnName("Obs");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.Nacionalidade).HasColumnName("Nacionalidade");
            this.Property(t => t.Naturalidade).HasColumnName("Naturalidade");
            this.Property(t => t.TelefoneComercial).HasColumnName("TelefoneComercial");
            this.Property(t => t.Sexo).HasColumnName("Sexo");
            this.Property(t => t.CodConjuge).HasColumnName("CodConjuge");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ContratoAvalistas)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ContratoAvalistas)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.ContratoAvalista2)
                .WithMany(t => t.ContratoAvalista1)
                .HasForeignKey(d => d.CodConjuge);

        }
    }
}
