using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class GrupoClienteMap : EntityTypeConfiguration<GrupoCliente>
    {
        public GrupoClienteMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeGrupo)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("GrupoCliente");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.NomeGrupo).HasColumnName("NomeGrupo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
