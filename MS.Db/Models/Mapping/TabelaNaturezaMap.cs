using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaNaturezaMap : EntityTypeConfiguration<TabelaNatureza>
    {
        public TabelaNaturezaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Natureza)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaNatureza");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodTipoCarteira).HasColumnName("CodTipoCarteira");
            this.Property(t => t.CodNaturezaCliente).HasColumnName("CodNaturezaCliente");
            this.Property(t => t.Natureza).HasColumnName("Natureza");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaTipoCarteira)
                .WithMany(t => t.TabelaNaturezas)
                .HasForeignKey(d => d.CodTipoCarteira);

        }
    }
}
