using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ParametroAtendimentoRotaMap : EntityTypeConfiguration<ParametroAtendimentoRota>
    {
        public ParametroAtendimentoRotaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ParametroAtendimentoRota");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodRota).HasColumnName("CodRota");
            this.Property(t => t.CodParametroAtendimento).HasColumnName("CodParametroAtendimento");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.ParametroAtendimento)
                .WithMany(t => t.ParametroAtendimentoRotas)
                .HasForeignKey(d => d.CodParametroAtendimento);
            this.HasOptional(t => t.Rota)
                .WithMany(t => t.ParametroAtendimentoRotas)
                .HasForeignKey(d => d.CodRota);

        }
    }
}
