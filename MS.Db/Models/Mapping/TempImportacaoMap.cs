using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TempImportacaoMap : EntityTypeConfiguration<TempImportacao>
    {
        public TempImportacaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CodCliente)
                .HasMaxLength(300);

            this.Property(t => t.Nome)
                .HasMaxLength(300);

            this.Property(t => t.NumeroContrato)
                .HasMaxLength(300);

            this.Property(t => t.Erro)
                .HasMaxLength(300);

            // Table & Column Mappings
            this.ToTable("TempImportacao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.NumeroContrato).HasColumnName("NumeroContrato");
            this.Property(t => t.Erro).HasColumnName("Erro");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
        }
    }
}
