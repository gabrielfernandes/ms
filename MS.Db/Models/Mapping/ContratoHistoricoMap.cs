using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoHistoricoMap : EntityTypeConfiguration<ContratoHistorico>
    {
        public ContratoHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContratoHistorico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodContrato).HasColumnName("CodContrato");
            this.Property(t => t.CodSintese).HasColumnName("CodSintese");
            this.Property(t => t.CodMotivoAtraso).HasColumnName("CodMotivoAtraso");
            this.Property(t => t.CodResolucao).HasColumnName("CodResolucao");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Observacoes).HasColumnName("Observacoes");
            this.Property(t => t.DataAgenda).HasColumnName("DataAgenda");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataAtendimento).HasColumnName("DataAtendimento");
            this.Property(t => t.LoginAtendimento).HasColumnName("LoginAtendimento");

            // Relationships
            this.HasOptional(t => t.Contrato)
                .WithMany(t => t.ContratoHistoricoes)
                .HasForeignKey(d => d.CodContrato);
            this.HasOptional(t => t.TabelaMotivoAtraso)
                .WithMany(t => t.ContratoHistoricoes)
                .HasForeignKey(d => d.CodMotivoAtraso);
            this.HasOptional(t => t.TabelaResolucao)
                .WithMany(t => t.ContratoHistoricoes)
                .HasForeignKey(d => d.CodResolucao);
            this.HasOptional(t => t.TabelaSintese)
                .WithMany(t => t.ContratoHistoricoes)
                .HasForeignKey(d => d.CodSintese);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ContratoHistoricoes)
                .HasForeignKey(d => d.CodUsuario);
            this.HasOptional(t => t.Usuario1)
                .WithMany(t => t.ContratoHistoricoes1)
                .HasForeignKey(d => d.LoginAtendimento);

        }
    }
}
