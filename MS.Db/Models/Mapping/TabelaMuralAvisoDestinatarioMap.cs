using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaMuralAvisoDestinatarioMap : EntityTypeConfiguration<TabelaMuralAvisoDestinatario>
    {
        public TabelaMuralAvisoDestinatarioMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaMuralAvisoDestinatario");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodMuralAviso).HasColumnName("CodMuralAviso");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaMuralAviso)
                .WithMany(t => t.TabelaMuralAvisoDestinatarios)
                .HasForeignKey(d => d.CodMuralAviso);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.TabelaMuralAvisoDestinatarios)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
