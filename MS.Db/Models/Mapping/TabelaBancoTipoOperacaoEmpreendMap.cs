using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaBancoTipoOperacaoEmpreendMap : EntityTypeConfiguration<TabelaBancoTipoOperacaoEmpreend>
    {
        public TabelaBancoTipoOperacaoEmpreendMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaBancoTipoOperacaoEmpreend");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodBancoTipoOperacao).HasColumnName("CodBancoTipoOperacao");
            this.Property(t => t.TaxaJuros).HasColumnName("TaxaJuros");
            this.Property(t => t.CodEmpreend).HasColumnName("CodEmpreend");

            // Relationships
            this.HasOptional(t => t.TabelaBancoTipoOperacao)
                .WithMany(t => t.TabelaBancoTipoOperacaoEmpreends)
                .HasForeignKey(d => d.CodBancoTipoOperacao);
            this.HasOptional(t => t.TabelaEmpreendimento)
                .WithMany(t => t.TabelaBancoTipoOperacaoEmpreends)
                .HasForeignKey(d => d.CodEmpreend);

        }
    }
}
