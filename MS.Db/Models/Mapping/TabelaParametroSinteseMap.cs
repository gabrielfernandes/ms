using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaParametroSinteseMap : EntityTypeConfiguration<TabelaParametroSintese>
    {
        public TabelaParametroSinteseMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Observacao)
                .HasMaxLength(250);
            

            // Table & Column Mappings
            this.ToTable("TabelaParametroSintese");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodSinteseDe).HasColumnName("CodSinteseDe");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Etapa).HasColumnName("Etapa");
            this.Property(t => t.Tipo).HasColumnName("Tipo");
            this.Property(t => t.Observacao).HasColumnName("Observacao");
            this.Property(t => t.CodSintesePara).HasColumnName("CodSintesePara");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasRequired(t => t.TabelaSinteseDe)
                .WithMany(t => t.TabelaParametroSintesoesDe)
                .HasForeignKey(d => d.CodSinteseDe);
            this.HasRequired(t => t.TabelaSintesePara)
                .WithMany(t => t.TabelaParametroSintesoesPara)
                .HasForeignKey(d => d.CodSintesePara);

        }
    }
}
