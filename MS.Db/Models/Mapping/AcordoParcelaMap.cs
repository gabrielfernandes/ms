using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class AcordoParcelaMap : EntityTypeConfiguration<AcordoParcela>
    {
        public AcordoParcelaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.FluxoPagamento)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("AcordoParcela");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodAcordo).HasColumnName("CodAcordo");
            this.Property(t => t.CodNatureza).HasColumnName("CodNatureza");
            this.Property(t => t.FluxoPagamento).HasColumnName("FluxoPagamento");
            this.Property(t => t.DataVencimento).HasColumnName("DataVencimento");
            this.Property(t => t.DataPagamento).HasColumnName("DataPagamento");
            this.Property(t => t.ValorParcela).HasColumnName("ValorParcela");
            this.Property(t => t.ValorRecebido).HasColumnName("ValorRecebido");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.StatusParcela).HasColumnName("StatusParcela");
            this.Property(t => t.ValorAberto).HasColumnName("ValorAberto");
            this.Property(t => t.Juros).HasColumnName("Juros");
            this.Property(t => t.Multa).HasColumnName("Multa");
            this.Property(t => t.DataEmissao).HasColumnName("DataEmissao");
            this.Property(t => t.Honorario).HasColumnName("Honorario");
            this.Property(t => t.Desconto).HasColumnName("Desconto");
            this.Property(t => t.ValorAtualizado).HasColumnName("ValorAtualizado");
            this.Property(t => t.Quantidade).HasColumnName("Quantidade");
            this.Property(t => t.ValorTotal).HasColumnName("ValorTotal");

            // Relationships
            this.HasOptional(t => t.Acordo)
                .WithMany(t => t.AcordoParcelas)
                .HasForeignKey(d => d.CodAcordo);

        }
    }
}
