using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ParametroAtendimentoProdutoMap : EntityTypeConfiguration<ParametroAtendimentoProduto>
    {
        public ParametroAtendimentoProdutoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ParametroAtendimentoProduto");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodProduto).HasColumnName("CodProduto");
            this.Property(t => t.CodParametroAtendimento).HasColumnName("CodParametroAtendimento");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.ParametroAtendimento)
                .WithMany(t => t.ParametroAtendimentoProdutoes)
                .HasForeignKey(d => d.CodParametroAtendimento);
            this.HasOptional(t => t.TabelaProduto)
                .WithMany(t => t.ParametroAtendimentoProdutoes)
                .HasForeignKey(d => d.CodProduto);

        }
    }
}
