using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEmpresaMap : EntityTypeConfiguration<TabelaEmpresa>
    {
        public TabelaEmpresaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CodEmpresaCliente)
                .HasMaxLength(50);

            this.Property(t => t.Empresa)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaEmpresa");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodEmpresaCliente).HasColumnName("CodEmpresaCliente");
            this.Property(t => t.CodConstrutora).HasColumnName("CodConstrutora");
            this.Property(t => t.CodGestao).HasColumnName("CodGestao");
            this.Property(t => t.Empresa).HasColumnName("Empresa");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaConstrutora)
                .WithMany(t => t.TabelaEmpresas)
                .HasForeignKey(d => d.CodConstrutora);
            this.HasOptional(t => t.TabelaGestao)
                .WithMany(t => t.TabelaEmpresas)
                .HasForeignKey(d => d.CodGestao);

        }
    }
}
