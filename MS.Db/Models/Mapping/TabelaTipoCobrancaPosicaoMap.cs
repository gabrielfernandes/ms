using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoCobrancaPosicaoMap : EntityTypeConfiguration<TabelaTipoCobrancaPosicao>
    {
        public TabelaTipoCobrancaPosicaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaTipoCobrancaPosicao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodTipoCobranca).HasColumnName("CodTipoCobranca");
            this.Property(t => t.NumeroDias).HasColumnName("NumeroDias");
            this.Property(t => t.CodFilial).HasColumnName("CodFilial");

            // Relationships
            this.HasOptional(t => t.TabelaTipoCobranca)
                .WithMany(t => t.TabelaTipoCobrancaPosicaos)
                .HasForeignKey(d => d.CodTipoCobranca);

        }
    }
}
