using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaAnaliseDocumentalAlternativaMap : EntityTypeConfiguration<TabelaAnaliseDocumentalAlternativa>
    {
        public TabelaAnaliseDocumentalAlternativaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Alternativa)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaAnaliseDocumentalAlternativa");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodAnaliseDocumental).HasColumnName("CodAnaliseDocumental");
            this.Property(t => t.Alternativa).HasColumnName("Alternativa");
            this.Property(t => t.Valor).HasColumnName("Valor");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaAnaliseDocumental)
                .WithMany(t => t.TabelaAnaliseDocumentalAlternativas)
                .HasForeignKey(d => d.CodAnaliseDocumental);

        }
    }
}
