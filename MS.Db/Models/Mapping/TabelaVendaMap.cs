using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaVendaMap : EntityTypeConfiguration<TabelaVenda>
    {
        public TabelaVendaMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Nome)
                .HasMaxLength(2500);

            // Table & Column Mappings
            this.ToTable("TabelaVenda");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Nome).HasColumnName("Nome");
            this.Property(t => t.VigenciaDe).HasColumnName("VigenciaDe");
            this.Property(t => t.VigenciaAte).HasColumnName("VigenciaAte");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.PercentualComissao).HasColumnName("PercentualComissao");
            this.Property(t => t.PercentualMinimoEntrada).HasColumnName("PercentualMinimoEntrada");
        }
    }
}
