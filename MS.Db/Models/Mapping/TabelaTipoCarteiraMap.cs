using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoCarteiraMap : EntityTypeConfiguration<TabelaTipoCarteira>
    {
        public TabelaTipoCarteiraMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TipoCarteira)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaTipoCarteira");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.TipoCarteira).HasColumnName("TipoCarteira");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
