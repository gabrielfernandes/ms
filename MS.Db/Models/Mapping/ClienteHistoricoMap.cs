using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ClienteHistoricoMap : EntityTypeConfiguration<ClienteHistorico>
    {
        public ClienteHistoricoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ClienteHistorico");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.CodSintese).HasColumnName("CodSintese");
            this.Property(t => t.CodMotivoAtraso).HasColumnName("CodMotivoAtraso");
            this.Property(t => t.CodResolucao).HasColumnName("CodResolucao");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Observacoes).HasColumnName("Observacoes");
            this.Property(t => t.DataAgenda).HasColumnName("DataAgenda");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.ClienteHistoricoes)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.TabelaMotivoAtraso)
                .WithMany(t => t.ClienteHistoricoes)
                .HasForeignKey(d => d.CodMotivoAtraso);
            this.HasOptional(t => t.TabelaResolucao)
                .WithMany(t => t.ClienteHistoricoes)
                .HasForeignKey(d => d.CodResolucao);
            this.HasOptional(t => t.TabelaSintese)
                .WithMany(t => t.ClienteHistoricoes)
                .HasForeignKey(d => d.CodSintese);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.ClienteHistoricoes)
                .HasForeignKey(d => d.CodUsuario);

        }
    }
}
