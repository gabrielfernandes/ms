using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaCampanhaEmailMap : EntityTypeConfiguration<TabelaCampanhaEmail>
    {
        public TabelaCampanhaEmailMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaCampanhaEmail");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodCampanha).HasColumnName("CodCampanha");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.CodModelo).HasColumnName("CodModelo");
            this.Property(t => t.Excecao).HasColumnName("Excecao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");

            // Relationships
            this.HasOptional(t => t.Cliente)
                .WithMany(t => t.TabelaCampanhaEmails)
                .HasForeignKey(d => d.CodCliente);
            this.HasOptional(t => t.TabelaCampanha)
                .WithMany(t => t.TabelaCampanhaEmails)
                .HasForeignKey(d => d.CodCampanha);
            this.HasOptional(t => t.TabelaModeloAcao)
                .WithMany(t => t.TabelaCampanhaEmails)
                .HasForeignKey(d => d.CodModelo);

        }
    }
}
