using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoMap : EntityTypeConfiguration<Contrato>
    {
        public ContratoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NumeroContrato)
                .HasMaxLength(50);

            this.Property(t => t.UnidadeRes)
                .HasMaxLength(100);

            this.Property(t => t.AndamentodaObra)
                .HasMaxLength(500);

            this.Property(t => t.NumeroProcessoAcaoRe)
                .HasMaxLength(255);

            this.Property(t => t.Justificativa)
                .HasMaxLength(500);

            this.Property(t => t.TipoVenda)
                .HasMaxLength(500);

            this.Property(t => t.StatusRepasse)
                .HasMaxLength(500);

            this.Property(t => t.CodigoCliente)
                .HasMaxLength(550);

            this.Property(t => t.StatusContrato)
                .HasMaxLength(255);

            this.Property(t => t.MensagemContrato)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Contrato");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodUnidade).HasColumnName("CodUnidade");
            this.Property(t => t.DataImportacao).HasColumnName("DataImportacao");
            this.Property(t => t.CodUsuarioImportacao).HasColumnName("CodUsuarioImportacao");
            this.Property(t => t.CodUsuarioDonoCarteira).HasColumnName("CodUsuarioDonoCarteira");
            this.Property(t => t.CodTipoCobranca).HasColumnName("CodTipoCobranca");
            this.Property(t => t.CodUsuarioTipoCobranca).HasColumnName("CodUsuarioTipoCobranca");
            this.Property(t => t.CodGestao).HasColumnName("CodGestao");
            this.Property(t => t.CodTipoBloqueio).HasColumnName("CodTipoBloqueio");
            this.Property(t => t.CodTipoCarteira).HasColumnName("CodTipoCarteira");
            this.Property(t => t.CodNatureza).HasColumnName("CodNatureza");
            this.Property(t => t.CodUsuarioBloqueio).HasColumnName("CodUsuarioBloqueio");
            this.Property(t => t.CodEmpresa).HasColumnName("CodEmpresa");
            this.Property(t => t.CodUsuarioAcaoRe).HasColumnName("CodUsuarioAcaoRe");
            this.Property(t => t.CodUsuarioNotificacao).HasColumnName("CodUsuarioNotificacao");
            this.Property(t => t.CodUsuarioCadastro).HasColumnName("CodUsuarioCadastro");
            this.Property(t => t.CodEscritorio).HasColumnName("CodEscritorio");
            this.Property(t => t.DataTipoCobranca).HasColumnName("DataTipoCobranca");
            this.Property(t => t.DataBloqueio).HasColumnName("DataBloqueio");
            this.Property(t => t.DataChaves).HasColumnName("DataChaves");
            this.Property(t => t.DataIPTU).HasColumnName("DataIPTU");
            this.Property(t => t.NumeroContrato).HasColumnName("NumeroContrato");
            this.Property(t => t.DataContrato).HasColumnName("DataContrato");
            this.Property(t => t.ValorVenda).HasColumnName("ValorVenda");
            this.Property(t => t.UnidadeRes).HasColumnName("UnidadeRes");
            this.Property(t => t.PercentualEmpresa).HasColumnName("PercentualEmpresa");
            this.Property(t => t.PercentualSocio).HasColumnName("PercentualSocio");
            this.Property(t => t.PercentualParceiro).HasColumnName("PercentualParceiro");
            this.Property(t => t.ValorIPTU).HasColumnName("ValorIPTU");
            this.Property(t => t.ValorCondominio).HasColumnName("ValorCondominio");
            this.Property(t => t.DataCondominio).HasColumnName("DataCondominio");
            this.Property(t => t.AndamentodaObra).HasColumnName("AndamentodaObra");
            this.Property(t => t.MetroQuadrado).HasColumnName("MetroQuadrado");
            this.Property(t => t.DAF).HasColumnName("DAF");
            this.Property(t => t.Chaves).HasColumnName("Chaves");
            this.Property(t => t.Posse).HasColumnName("Posse");
            this.Property(t => t.NotificacaoTipo).HasColumnName("NotificacaoTipo");
            this.Property(t => t.AcaoRe).HasColumnName("AcaoRe");
            this.Property(t => t.NumeroProcessoAcaoRe).HasColumnName("NumeroProcessoAcaoRe");
            this.Property(t => t.DataAcaoRe).HasColumnName("DataAcaoRe");
            this.Property(t => t.DataNotificacao).HasColumnName("DataNotificacao");
            this.Property(t => t.Bloqueado).HasColumnName("Bloqueado");
            this.Property(t => t.Justificativa).HasColumnName("Justificativa");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.TipoContrato).HasColumnName("TipoContrato");
            this.Property(t => t.CodClientePrincipal).HasColumnName("CodClientePrincipal");
            this.Property(t => t.CodContratoHistorico).HasColumnName("CodContratoHistorico");
            this.Property(t => t.ValorAtraso).HasColumnName("ValorAtraso");
            this.Property(t => t.CodProposta).HasColumnName("CodProposta");
            this.Property(t => t.CodAging).HasColumnName("CodAging");
            this.Property(t => t.DiasAtraso).HasColumnName("DiasAtraso");
            this.Property(t => t.Avalista).HasColumnName("Avalista");
            this.Property(t => t.Juridico).HasColumnName("Juridico");
            this.Property(t => t.TipoVenda).HasColumnName("TipoVenda");
            this.Property(t => t.StatusRepasse).HasColumnName("StatusRepasse");
            this.Property(t => t.PrevisaoSGI).HasColumnName("PrevisaoSGI");
            this.Property(t => t.CodLogProcessamento).HasColumnName("CodLogProcessamento");
            this.Property(t => t.DataAtendimento).HasColumnName("DataAtendimento");
            this.Property(t => t.CodUsuarioDistribuicaoCarteira).HasColumnName("CodUsuarioDistribuicaoCarteira");
            this.Property(t => t.DataDistribuicao).HasColumnName("DataDistribuicao");
            this.Property(t => t.CodRegionalProduto).HasColumnName("CodRegionalProduto");
            this.Property(t => t.CodigoCliente).HasColumnName("CodigoCliente");
            this.Property(t => t.DiasSintese).HasColumnName("DiasSintese");
            this.Property(t => t.DiasFase).HasColumnName("DiasFase");
            this.Property(t => t.ValorAVencer).HasColumnName("ValorAVencer");
            this.Property(t => t.ValorPago).HasColumnName("ValorPago");
            this.Property(t => t.ValorTotal).HasColumnName("ValorTotal");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.DataFinalizacao).HasColumnName("DataFinalizacao");
            this.Property(t => t.CodUsuarioAtendimento).HasColumnName("CodUsuarioAtendimento");
            this.Property(t => t.CodHistorico).HasColumnName("CodHistorico");
            this.Property(t => t.Governo).HasColumnName("Governo");
            this.Property(t => t.StatusContrato).HasColumnName("StatusContrato");
            this.Property(t => t.MensagemContrato).HasColumnName("MensagemContrato");
            this.Property(t => t.CodRegionalGrupo).HasColumnName("CodRegionalGrupo");



            // Relationships
            this.HasOptional(t => t.ContratoHistorico)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodContratoHistorico);
            this.HasOptional(t => t.Historico)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodHistorico);
            this.HasOptional(t => t.Proposta)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodProposta);
            this.HasOptional(t => t.TabelaAging)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodAging);
            this.HasOptional(t => t.TabelaEmpresa)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodEmpresa);
            this.HasOptional(t => t.TabelaEscritorio)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodEscritorio);
            this.HasOptional(t => t.TabelaNatureza)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodNatureza);
            this.HasOptional(t => t.TabelaRegionalProduto)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodRegionalProduto);
            this.HasOptional(t => t.TabelaTipoBloqueio)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodTipoBloqueio);
            this.HasOptional(t => t.TabelaTipoCarteira)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodTipoCarteira);
            this.HasOptional(t => t.TabelaTipoCobranca)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodTipoCobranca);
            this.HasOptional(t => t.TabelaTipoCobrancaLogProcessamento)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodLogProcessamento);
            this.HasOptional(t => t.TabelaUnidade)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodUnidade);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.Contratoes)
                .HasForeignKey(d => d.CodUsuarioAcaoRe);
            this.HasOptional(t => t.Usuario1)
                .WithMany(t => t.Contratoes1)
                .HasForeignKey(d => d.CodUsuarioBloqueio);
            this.HasOptional(t => t.Usuario2)
                .WithMany(t => t.Contratoes2)
                .HasForeignKey(d => d.CodUsuarioCadastro);
            this.HasOptional(t => t.Usuario3)
                .WithMany(t => t.Contratoes3)
                .HasForeignKey(d => d.CodUsuarioDonoCarteira);
            this.HasOptional(t => t.Usuario4)
                .WithMany(t => t.Contratoes4)
                .HasForeignKey(d => d.CodUsuarioImportacao);
            this.HasOptional(t => t.Usuario5)
                .WithMany(t => t.Contratoes5)
                .HasForeignKey(d => d.CodUsuarioNotificacao);
            this.HasOptional(t => t.Usuario6)
                .WithMany(t => t.Contratoes6)
                .HasForeignKey(d => d.CodUsuarioTipoCobranca);

        }
    }
}
