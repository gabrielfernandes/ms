using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoCobrancaAcaoCondicaoMap : EntityTypeConfiguration<TabelaTipoCobrancaAcaoCondicao>
    {
        public TabelaTipoCobrancaAcaoCondicaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("TabelaTipoCobrancaAcaoCondicao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodTipoCobrancaAcao).HasColumnName("CodTipoCobrancaAcao");
            this.Property(t => t.CondicaoTipo).HasColumnName("CondicaoTipo");
            this.Property(t => t.CampoTipo).HasColumnName("CampoTipo");
            this.Property(t => t.OperadorLogicoTipo).HasColumnName("OperadorLogicoTipo");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaTipoCobrancaAcao)
                .WithMany(t => t.TabelaTipoCobrancaAcaoCondicaos)
                .HasForeignKey(d => d.CodTipoCobrancaAcao);

        }
    }
}
