using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaBancoPortadorInstrucaoMap : EntityTypeConfiguration<TabelaBancoPortadorInstrucao>
    {
        public TabelaBancoPortadorInstrucaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CodigoInstrucao)
                .HasMaxLength(50);

            this.Property(t => t.Numero)
                .HasMaxLength(50);

            this.Property(t => t.Descricao)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("TabelaBancoPortadorInstrucao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodBancoPortador).HasColumnName("CodBancoPortador");
            this.Property(t => t.CodigoInstrucao).HasColumnName("CodigoInstrucao");
            this.Property(t => t.Numero).HasColumnName("Numero");
            this.Property(t => t.Descricao).HasColumnName("Descricao");

            // Relationships
            this.HasOptional(t => t.TabelaBancoPortador)
                .WithMany(t => t.TabelaBancoPortadorInstrucaos)
                .HasForeignKey(d => d.CodBancoPortador);

        }
    }
}
