using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoImovelMap : EntityTypeConfiguration<TabelaTipoImovel>
    {
        public TabelaTipoImovelMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TipoImovel)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("TabelaTipoImovel");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.TipoImovel).HasColumnName("TipoImovel");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
