using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaBancoTipoOperacaoMap : EntityTypeConfiguration<TabelaBancoTipoOperacao>
    {
        public TabelaBancoTipoOperacaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeTipoOperacao)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaBancoTipoOperacao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.ValorVendaMinimo).HasColumnName("ValorVendaMinimo");
            this.Property(t => t.ValorVendaMaximo).HasColumnName("ValorVendaMaximo");
            this.Property(t => t.CodBanco).HasColumnName("CodBanco");
            this.Property(t => t.ValorFinanciamentoMinimo).HasColumnName("ValorFinanciamentoMinimo");
            this.Property(t => t.ValorFiananciamentoMaximo).HasColumnName("ValorFiananciamentoMaximo");
            this.Property(t => t.NomeTipoOperacao).HasColumnName("NomeTipoOperacao");
            this.Property(t => t.PercentualComprometimentoRenda).HasColumnName("PercentualComprometimentoRenda");
            this.Property(t => t.PercentualAvaliacao).HasColumnName("PercentualAvaliacao");
            this.Property(t => t.TaxaJuros).HasColumnName("TaxaJuros");
            this.Property(t => t.PrazoMinimo).HasColumnName("PrazoMinimo");
            this.Property(t => t.PrazoMaximo).HasColumnName("PrazoMaximo");
            this.Property(t => t.IdadeMaxima).HasColumnName("IdadeMaxima");
            this.Property(t => t.DataVigenciaDe).HasColumnName("DataVigenciaDe");
            this.Property(t => t.DataVigenciaAte).HasColumnName("DataVigenciaAte");
            this.Property(t => t.CodTipoImovel).HasColumnName("CodTipoImovel");
            this.Property(t => t.SistemaAmortizacao).HasColumnName("SistemaAmortizacao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.TabelaBanco)
                .WithMany(t => t.TabelaBancoTipoOperacaos)
                .HasForeignKey(d => d.CodBanco);
            this.HasOptional(t => t.TabelaTipoImovel)
                .WithMany(t => t.TabelaBancoTipoOperacaos)
                .HasForeignKey(d => d.CodTipoImovel);

        }
    }
}
