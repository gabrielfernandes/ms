using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaParametroBoletoMap : EntityTypeConfiguration<TabelaParametroBoleto>
    {
        public TabelaParametroBoletoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Cod)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Portador)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaParametroBoleto");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodBanco).HasColumnName("CodBanco");
            this.Property(t => t.Portador).HasColumnName("Portador");
            this.Property(t => t.NumeroAgencia).HasColumnName("NumeroAgencia");
            this.Property(t => t.DigitoAgencia).HasColumnName("DigitoAgencia");
            this.Property(t => t.NumeroConta).HasColumnName("NumeroConta");
            this.Property(t => t.DigitoConta).HasColumnName("DigitoConta");
            this.Property(t => t.Ativo).HasColumnName("Ativo");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");

            // Relationships
            this.HasOptional(t => t.TabelaBanco)
                .WithMany(t => t.TabelaParametroBoletoes)
                .HasForeignKey(d => d.CodBanco);

        }
    }
}
