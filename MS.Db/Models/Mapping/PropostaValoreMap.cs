using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class PropostaValoreMap : EntityTypeConfiguration<PropostaValore>
    {
        public PropostaValoreMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("PropostaValores");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodProposta).HasColumnName("CodProposta");
            this.Property(t => t.CodTipoParcela).HasColumnName("CodTipoParcela");
            this.Property(t => t.ValorParcela).HasColumnName("ValorParcela");
            this.Property(t => t.Quantidade).HasColumnName("Quantidade");
            this.Property(t => t.ValorTotal).HasColumnName("ValorTotal");
            this.Property(t => t.DataInicio).HasColumnName("DataInicio");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");

            // Relationships
            this.HasOptional(t => t.Proposta)
                .WithMany(t => t.PropostaValores)
                .HasForeignKey(d => d.CodProposta);
            this.HasOptional(t => t.TabelaTipoParcela)
                .WithMany(t => t.PropostaValores)
                .HasForeignKey(d => d.CodTipoParcela);

        }
    }
}
