using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ContratoParcelaBoletoLoteMap : EntityTypeConfiguration<ContratoParcelaBoletoLote>
    {
        public ContratoParcelaBoletoLoteMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContratoParcelaBoletoLote");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodUsuario).HasColumnName("CodUsuario");
            this.Property(t => t.Quantidade).HasColumnName("Quantidade");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.NumeroLote).HasColumnName("NumeroLote");
            this.Property(t => t.CodLogArquivo).HasColumnName("CodLogArquivo");

        }
    }
}
