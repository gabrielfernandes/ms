using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaUnidadeMap : EntityTypeConfiguration<TabelaUnidade>
    {
        public TabelaUnidadeMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NumeroUnidade)
                .HasMaxLength(50);

            this.Property(t => t.CodUnidadeCliente)
                .HasMaxLength(50);

            this.Property(t => t.LoginReserva)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaUnidade");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodBloco).HasColumnName("CodBloco");
            this.Property(t => t.NumeroUnidade).HasColumnName("NumeroUnidade");
            this.Property(t => t.CodUnidadeCliente).HasColumnName("CodUnidadeCliente");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.Andar).HasColumnName("Andar");
            this.Property(t => t.Valor).HasColumnName("Valor");
            this.Property(t => t.ValorAvaliacao).HasColumnName("ValorAvaliacao");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.QtdDormitorio).HasColumnName("QtdDormitorio");
            this.Property(t => t.CodTipoUnidade).HasColumnName("CodTipoUnidade");
            this.Property(t => t.DataReserva).HasColumnName("DataReserva");
            this.Property(t => t.LoginReserva).HasColumnName("LoginReserva");
            this.Property(t => t.Metragem).HasColumnName("Metragem");
            this.Property(t => t.PNE).HasColumnName("PNE");
            this.Property(t => t.Vagas).HasColumnName("Vagas");

            // Relationships
            this.HasOptional(t => t.TabelaBloco)
                .WithMany(t => t.TabelaUnidades)
                .HasForeignKey(d => d.CodBloco);
            this.HasOptional(t => t.TabelaTipoUnidade)
                .WithMany(t => t.TabelaUnidades)
                .HasForeignKey(d => d.CodTipoUnidade);

        }
    }
}
