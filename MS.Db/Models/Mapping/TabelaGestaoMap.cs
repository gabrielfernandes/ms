using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaGestaoMap : EntityTypeConfiguration<TabelaGestao>
    {
        public TabelaGestaoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.NomeGestao)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaGestao");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.NomeGestao).HasColumnName("NomeGestao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
