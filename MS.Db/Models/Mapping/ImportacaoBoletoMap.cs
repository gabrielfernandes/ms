using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class ImportacaoBoletoMap : EntityTypeConfiguration<ImportacaoBoleto>
    {
        public ImportacaoBoletoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Filial)
                .HasMaxLength(255);

            this.Property(t => t.NumeroFatura)
                .HasMaxLength(255);

            this.Property(t => t.NumeroCliente)
                .HasMaxLength(255);

            this.Property(t => t.DataVencimento)
                .HasMaxLength(255);

            this.Property(t => t.ValorFatura)
                .HasMaxLength(255);

            this.Property(t => t.LinhaDigitavel)
                .HasMaxLength(255);

            this.Property(t => t.Status)
                .HasMaxLength(255);

            this.Property(t => t.Mensagem)
                .HasMaxLength(255);

            this.Property(t => t.DataDocumento)
                .HasMaxLength(255);

            this.Property(t => t.Endereco)
                .HasMaxLength(255);

            this.Property(t => t.Numero)
                .HasMaxLength(255);

            this.Property(t => t.Complemento)
                .HasMaxLength(255);

            this.Property(t => t.Bairro)
                .HasMaxLength(255);

            this.Property(t => t.CEP)
                .HasMaxLength(255);

            this.Property(t => t.Cidade)
                .HasMaxLength(255);

            this.Property(t => t.Estado)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("ImportacaoBoleto");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodImportacao).HasColumnName("CodImportacao");
            this.Property(t => t.Filial).HasColumnName("Filial");
            this.Property(t => t.NumeroFatura).HasColumnName("NumeroFatura");
            this.Property(t => t.NumeroCliente).HasColumnName("NumeroCliente");
            this.Property(t => t.DataVencimento).HasColumnName("DataVencimento");
            this.Property(t => t.ValorFatura).HasColumnName("ValorFatura");
            this.Property(t => t.LinhaDigitavel).HasColumnName("LinhaDigitavel");
            this.Property(t => t.CodCliente).HasColumnName("CodCliente");
            this.Property(t => t.CodParcela).HasColumnName("CodParcela");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Mensagem).HasColumnName("Mensagem");
            this.Property(t => t.DataProcessamento).HasColumnName("DataProcessamento");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.DataDocumento).HasColumnName("DataDocumento");
            this.Property(t => t.Endereco).HasColumnName("Endereco");
            this.Property(t => t.Numero).HasColumnName("Numero");
            this.Property(t => t.Complemento).HasColumnName("Complemento");
            this.Property(t => t.Bairro).HasColumnName("Bairro");
            this.Property(t => t.CEP).HasColumnName("CEP");
            this.Property(t => t.Cidade).HasColumnName("Cidade");
            this.Property(t => t.Estado).HasColumnName("Estado");

            // Relationships
            this.HasOptional(t => t.Importacao)
                .WithMany(t => t.ImportacaoBoletoes)
                .HasForeignKey(d => d.CodImportacao);

        }
    }
}
