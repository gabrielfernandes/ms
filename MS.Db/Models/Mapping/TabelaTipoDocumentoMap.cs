using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaTipoDocumentoMap : EntityTypeConfiguration<TabelaTipoDocumento>
    {
        public TabelaTipoDocumentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.TipoDocumento)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("TabelaTipoDocumento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.TipoDocumento).HasColumnName("TipoDocumento");
            this.Property(t => t.Obrigatorio).HasColumnName("Obrigatorio");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
