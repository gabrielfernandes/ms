using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEquipeMap : EntityTypeConfiguration<TabelaEquipe>
    {
        public TabelaEquipeMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.Equipe)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("TabelaEquipe");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.Equipe).HasColumnName("Equipe");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
        }
    }
}
