using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cobranca.Db.Models.Mapping
{
    public class TabelaEmpreendimentoMap : EntityTypeConfiguration<TabelaEmpreendimento>
    {
        public TabelaEmpreendimentoMap()
        {
            // Primary Key
            this.HasKey(t => t.Cod);

            // Properties
            this.Property(t => t.CodEmpreendimentoCliente)
                .HasMaxLength(15);

            this.Property(t => t.NomeEmpreendimento)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TabelaEmpreendimento");
            this.Property(t => t.Cod).HasColumnName("Cod");
            this.Property(t => t.CodConstrutora).HasColumnName("CodConstrutora");
            this.Property(t => t.CodRegional).HasColumnName("CodRegional");
            this.Property(t => t.CodEmpreendimentoCliente).HasColumnName("CodEmpreendimentoCliente");
            this.Property(t => t.CodUsuarioDonoDeNegocio).HasColumnName("CodUsuarioDonoDeNegocio");
            this.Property(t => t.CodUsuarioDonoDeCarteira).HasColumnName("CodUsuarioDonoDeCarteira");
            this.Property(t => t.CodUsuarioResponsavel).HasColumnName("CodUsuarioResponsavel");
            this.Property(t => t.CodBancoFinanciador).HasColumnName("CodBancoFinanciador");
            this.Property(t => t.CodEndereco).HasColumnName("CodEndereco");
            this.Property(t => t.CodBancoPiloto).HasColumnName("CodBancoPiloto");
            this.Property(t => t.NomeEmpreendimento).HasColumnName("NomeEmpreendimento");
            this.Property(t => t.DataCND).HasColumnName("DataCND");
            this.Property(t => t.DataAverbacao).HasColumnName("DataAverbacao");
            this.Property(t => t.DataPastaMae).HasColumnName("DataPastaMae");
            this.Property(t => t.DataAssembleia).HasColumnName("DataAssembleia");
            this.Property(t => t.DataHabitese).HasColumnName("DataHabitese");
            this.Property(t => t.DataMatricula).HasColumnName("DataMatricula");
            this.Property(t => t.DataConclusaoConstrutacao).HasColumnName("DataConclusaoConstrutacao");
            this.Property(t => t.DataPrevCND).HasColumnName("DataPrevCND");
            this.Property(t => t.DataPrevAverbacao).HasColumnName("DataPrevAverbacao");
            this.Property(t => t.DataPrevPastaMae).HasColumnName("DataPrevPastaMae");
            this.Property(t => t.DataPrevAssembleia).HasColumnName("DataPrevAssembleia");
            this.Property(t => t.DataPrevHabitese).HasColumnName("DataPrevHabitese");
            this.Property(t => t.DataPrevMatricula).HasColumnName("DataPrevMatricula");
            this.Property(t => t.DataPrevConclusaoConstrucao).HasColumnName("DataPrevConclusaoConstrucao");
            this.Property(t => t.DataCadastro).HasColumnName("DataCadastro");
            this.Property(t => t.DataExcluido).HasColumnName("DataExcluido");
            this.Property(t => t.DataAlteracao).HasColumnName("DataAlteracao");
            this.Property(t => t.AndamentoObra).HasColumnName("AndamentoObra");
            this.Property(t => t.CodTipoImovel).HasColumnName("CodTipoImovel");
            this.Property(t => t.CodCheckListNome).HasColumnName("CodCheckListNome");
            this.Property(t => t.CodCheckListEmpreendimento).HasColumnName("CodCheckListEmpreendimento");

            // Relationships
            this.HasOptional(t => t.TabelaBanco)
                .WithMany(t => t.TabelaEmpreendimentoes)
                .HasForeignKey(d => d.CodBancoFinanciador);
            this.HasOptional(t => t.TabelaBanco1)
                .WithMany(t => t.TabelaEmpreendimentoes1)
                .HasForeignKey(d => d.CodBancoPiloto);
            this.HasOptional(t => t.TabelaCheckListNome)
                .WithMany(t => t.TabelaEmpreendimentoes)
                .HasForeignKey(d => d.CodCheckListNome);
            this.HasOptional(t => t.TabelaCheckListNome1)
                .WithMany(t => t.TabelaEmpreendimentoes1)
                .HasForeignKey(d => d.CodCheckListEmpreendimento);
            this.HasRequired(t => t.TabelaConstrutora)
                .WithMany(t => t.TabelaEmpreendimentoes)
                .HasForeignKey(d => d.CodConstrutora);
            this.HasOptional(t => t.TabelaEndereco)
                .WithMany(t => t.TabelaEmpreendimentoes)
                .HasForeignKey(d => d.CodEndereco);
            this.HasOptional(t => t.TabelaRegional)
                .WithMany(t => t.TabelaEmpreendimentoes)
                .HasForeignKey(d => d.CodRegional);
            this.HasOptional(t => t.TabelaTipoImovel)
                .WithMany(t => t.TabelaEmpreendimentoes)
                .HasForeignKey(d => d.CodTipoImovel);
            this.HasOptional(t => t.Usuario)
                .WithMany(t => t.TabelaEmpreendimentoes)
                .HasForeignKey(d => d.CodUsuarioDonoDeNegocio);
            this.HasOptional(t => t.Usuario1)
                .WithMany(t => t.TabelaEmpreendimentoes1)
                .HasForeignKey(d => d.CodUsuarioDonoDeCarteira);
            this.HasOptional(t => t.Usuario2)
                .WithMany(t => t.TabelaEmpreendimentoes2)
                .HasForeignKey(d => d.CodUsuarioResponsavel);

        }
    }
}
