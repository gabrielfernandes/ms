using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class TabelaBancoPortadorGrupo : BaseEntity
    {
        public TabelaBancoPortadorGrupo()
        {
            this.TabelaBancoPortadors = new List<TabelaBancoPortador>();
        }

        //  public int Cod { get; set; }
        public string NomeArquivo { get; set; }
        public Nullable<int> NumeroLote { get; set; }
        public Nullable<int> NumeroLoteAtual { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }

        public virtual ICollection<TabelaBancoPortador> TabelaBancoPortadors { get; set; }
    }
}
