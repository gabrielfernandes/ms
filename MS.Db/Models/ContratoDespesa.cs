using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ContratoDespesa : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodContrato { get; set; }
         public Nullable<int> CodTipoDespesa { get; set; }
         public Nullable<decimal> Valor { get; set; }
         public string Observacao { get; set; }
         public Nullable<bool> Antecipacao { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public Nullable<System.DateTime> Data { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual TabelaTipoDespesa TabelaTipoDespesa { get; set; }
    }
}
