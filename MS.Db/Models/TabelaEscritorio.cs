using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaEscritorio : BaseEntity
    {
        public TabelaEscritorio()
        {
            this.Contratoes = new List<Contrato>();
            this.ContratoEscritorioHistoricoes = new List<ContratoEscritorioHistorico>();
            this.Usuarios = new List<Usuario>();
        }

       //  public int Cod { get; set; }
         public string Escritorio { get; set; }
         public Nullable<int> CodEndereco { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string QueryDistribuicao { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual ICollection<ContratoEscritorioHistorico> ContratoEscritorioHistoricoes { get; set; }
        public virtual TabelaEndereco TabelaEndereco { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
