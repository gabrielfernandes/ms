using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class TabelaRegionalGrupo : BaseEntity
    {
        public TabelaRegionalGrupo()
        {
            this.TabelaBancoPortadors = new List<TabelaBancoPortador>();
        }

        //  public int Cod { get; set; }
        public Nullable<int> CodRegional { get; set; }
        public Nullable<int> CodConstrutora { get; set; }
        public string CodRegionalCliente { get; set; }
        public string Descricao { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<short> TipoRegiao { get; set; }
        public virtual TabelaConstrutora TabelaConstrutora { get; set; }
        public virtual TabelaRegional TabelaRegional { get; set; }
        public virtual ICollection<TabelaBancoPortador> TabelaBancoPortadors { get; set; }
    }
}
