using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class AcordoParcela : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodAcordo { get; set; }
         public Nullable<int> CodNatureza { get; set; }
         public string FluxoPagamento { get; set; }
         public Nullable<System.DateTime> DataVencimento { get; set; }
         public Nullable<System.DateTime> DataPagamento { get; set; }
         public Nullable<decimal> ValorParcela { get; set; }
         public Nullable<decimal> ValorRecebido { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<short> StatusParcela { get; set; }
         public Nullable<decimal> ValorAberto { get; set; }
         public Nullable<decimal> Juros { get; set; }
         public Nullable<decimal> Multa { get; set; }
         public Nullable<System.DateTime> DataEmissao { get; set; }
         public Nullable<decimal> Honorario { get; set; }
         public Nullable<decimal> Desconto { get; set; }
         public Nullable<decimal> ValorAtualizado { get; set; }
         public Nullable<int> Quantidade { get; set; }
         public Nullable<decimal> ValorTotal { get; set; }
        public virtual Acordo Acordo { get; set; }
    }
}
