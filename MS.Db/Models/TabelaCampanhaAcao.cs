using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaCampanhaAcao : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodCampanha { get; set; }
         public Nullable<int> CodAcao { get; set; }
         public Nullable<int> CodModelo { get; set; }
         public Nullable<bool> Excecao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaAcaoCobranca TabelaAcaoCobranca { get; set; }
        public virtual TabelaCampanha TabelaCampanha { get; set; }
    }
}
