using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class LogEmail : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<short> Tipo { get; set; }
         public string EmailRemetente { get; set; }
         public string EmailDestinatario { get; set; }
         public string NomeDestinatario { get; set; }
         public string NomeRemetente { get; set; }
         public string Assunto { get; set; }
         public string Conteudo { get; set; }
         public Nullable<bool> HTML { get; set; }
         public Nullable<short> Status { get; set; }
         public Nullable<int> TentativaEnvio { get; set; }
         public string Mensagem { get; set; }
         public Nullable<System.DateTime> DataEnvio { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string EmailCopia { get; set; }
         public Nullable<int> CodEnvioMassa { get; set; }
    }
}
