using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class Cliente : BaseEntity
    {
        public Cliente()
        {
            this.Cliente1 = new List<Cliente>();
            this.ClienteAtendimentoes = new List<ClienteAtendimento>();
            this.ClienteContatoes = new List<ClienteContato>();
            this.ClienteContatoEmpresarials = new List<ClienteContatoEmpresarial>();
            this.ClienteDocumentoes = new List<ClienteDocumento>();
            this.ClienteEnderecoes = new List<ClienteEndereco>();
            this.ClienteHistoricoes = new List<ClienteHistorico>();
            this.ClienteParcelaCreditoes = new List<ClienteParcelaCredito>();
            this.ContratoAcaoCobrancaHistoricoes = new List<ContratoAcaoCobrancaHistorico>();
            this.ContratoAvalistas = new List<ContratoAvalista>();
            this.ContratoClientes = new List<ContratoCliente>();
            this.ContratoDocumentoes = new List<ContratoDocumento>();
            this.Historicoes = new List<Historico>();
            this.TabelaCampanhaEmails = new List<TabelaCampanhaEmail>();
        }

        //  public int Cod { get; set; }
        public string CodCliente { get; set; }
        public string Nome { get; set; }
        public string PrimeiroNome { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public string CNPJ { get; set; }
        public string EstadoCivil { get; set; }
        public string Profissao { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DataNascimento { get; set; }
        public string Nacionalidade { get; set; }
        public string Naturalidade { get; set; }
        public string Orgao { get; set; }
        public string RegimeCasamento { get; set; }
        public Nullable<System.DateTime> DataCasamento { get; set; }
        public Nullable<short> Sexo { get; set; }
        public Nullable<int> CodClienteConjuge { get; set; }
        public string CodClienteConjugeSAP { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public string CelularPrincipal { get; set; }
        public Nullable<bool> Negativado { get; set; }
        public Nullable<short> PessoaTipo { get; set; }
        public string CEP { get; set; }
        public string Endereco { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Bairro { get; set; }
        public string UF { get; set; }
        public string EnderecoNumero { get; set; }
        public string EnderecoComplemento { get; set; }
        public Nullable<short> Tipo { get; set; }
        public string TelefoneResidencial { get; set; }
        public string TelefoneComercial { get; set; }
        public string Celular { get; set; }
        public string CelularComercial { get; set; }
        public Nullable<System.DateTime> DataAtendimento { get; set; }
        public Nullable<int> CodParcelaAntiga { get; set; }
        public Nullable<System.DateTime> DataFinalizacaoAtendimento { get; set; }
        public Nullable<int> CodUsuarioAtendimento { get; set; }
        public Nullable<int> CodUltimoHistorico { get; set; }
        public Nullable<int> QtdContrato { get; set; }
        public Nullable<int> CodAging { get; set; }
        public Nullable<int> DiasAtraso { get; set; }
        public Nullable<int> DiasFase { get; set; }
        public Nullable<int> DiasSintese { get; set; }
        public Nullable<decimal> ValorAtraso { get; set; }
        public Nullable<int> CodImportacao { get; set; }
        public Nullable<System.DateTime> DataImportacao { get; set; }
        public string NomeResponsavel { get; set; }
        public string InformacaoAdicional { get; set; }
        public string Corporativo { get; set; }
        public virtual ICollection<Cliente> Cliente1 { get; set; }
        public virtual Cliente Cliente2 { get; set; }
        public virtual TabelaAging TabelaAging { get; set; }
        public virtual ICollection<ClienteAtendimento> ClienteAtendimentoes { get; set; }
        public virtual ICollection<ClienteContato> ClienteContatoes { get; set; }
        public virtual ICollection<ClienteContatoEmpresarial> ClienteContatoEmpresarials { get; set; }
        public virtual ICollection<ClienteDocumento> ClienteDocumentoes { get; set; }
        public virtual ICollection<ClienteEndereco> ClienteEnderecoes { get; set; }
        public virtual ICollection<ClienteHistorico> ClienteHistoricoes { get; set; }
        public virtual ICollection<ClienteParcelaCredito> ClienteParcelaCreditoes { get; set; }
        public virtual ICollection<ContratoAcaoCobrancaHistorico> ContratoAcaoCobrancaHistoricoes { get; set; }
        public virtual ICollection<ContratoAvalista> ContratoAvalistas { get; set; }
        public virtual ICollection<ContratoCliente> ContratoClientes { get; set; }
        public virtual ICollection<ContratoDocumento> ContratoDocumentoes { get; set; }
        public virtual ICollection<Historico> Historicoes { get; set; }
        public virtual ICollection<TabelaCampanhaEmail> TabelaCampanhaEmails { get; set; }
    }
}
