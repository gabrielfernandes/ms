using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaFase : BaseEntity
    {
        public TabelaFase()
        {
            this.TabelaSintese = new List<TabelaSintese>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodTipoCobranca { get; set; }
         public string Fase { get; set; }
         public Nullable<int> Ordem { get; set; }
         public Nullable<int> Dias { get; set; }
         public string LoginCadastro { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodCampanha { get; set; }
         public Nullable<short> TipoHistorico { get; set; }
        public virtual TabelaCampanha TabelaCampanha { get; set; }
        public virtual TabelaTipoCobranca TabelaTipoCobranca { get; set; }
        public virtual ICollection<TabelaSintese> TabelaSintese { get; set; }
    }
}
