﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Models
{
    public static class Enumeradores
    {


        public enum ReguaFilialTipo
        {
            SP = 1,
            SUL = 2,
            NRT = 3,
            CTR = 4
        }

        public enum IdiomaTipo
        {
            pt_BR = 1,
            en_US = 2,
            es_ES = 3
        }

        public enum CalendarioStatus
        {
            Agendado = 1,
            Atrasado = 2

        }
        public enum ChaveModelo
        {
            ValorParcela = 1,
            DataVencimentoParcela = 2,
            NomeComprador = 3,
            CPF_CPNPJ_Comprador = 4,
            NumeroContrato = 5,
            SaldoDevedor = 6,
            NumeroParcela = 7,
            NumeroFilial = 8,
            DescricaoFilial = 9,
            ResponsavelFilial = 10,
            NumeroContato = 11,
            TabelaParcelas = 12,
            AtendimentoUsuairo = 13,
            ValorEmAberto = 14,
            UsuarioLogin = 15,
            UsuarioSenha = 16,

        }
        public enum LayoutSistema
        {
            Padrao = 0,
            Green = 1,
            Purple = 2,
            Blue = 3,
            Gray = 4,
            Blue2 = 5
        }
        public enum TipoServico
        {
            Iniciar_Servico = 1,
            Importacao = 2,
            Classificacao = 3,
            Regua = 4,
            Regua_Email = 5,
            Regua_Contato = 6,
            Regua_Email_boleto = 7,
            Regua_Negativacao = 8,
            Emails_Disparar = 9,
            Finalizar_Servico = 10,
            Rotinas_Servico = 11,
            Boletos_remessa = 12

        }

        public enum TipoImportacao
        {
            Cliente = 1,
            Parcela = 2,
            Credito = 3,
            Boleto = 4,
            ClienteEndereco = 5
        }
        public enum ChatMensagemTipo
        {
            Corretor = 1,
            Cliente = 2,
            Automatica = 3
        }
        public enum TipoPessoa
        {
            Fisica = 1,
            Juridica = 2
        }
        public enum TipoHistorico
        {
            Contrato = 1,
            Cliente = 2,
            Parcela = 3,
            Boleto = 4
        }
        public enum PesquisaTipoParcela
        {
            Todos = 1,
            EmAtraso = 2,
            aVencer = 3,
            Pago = 4
        }
        public enum CompradorTipo
        {
            Comprador = 1,
            Conjuge = 2
        }
        public enum TipoPeriodo
        {
            Unica = 1,
            Mensal = 2,
            Trimestral = 3,
            Semestral = 4,
            Anual = 5
        }
        public enum DocumentoComprobatorio
        {
            Alugueis = 1,
            ContratoPrestacaoServico = 2,
            DeclaracaoCooperativa = 3,
            ExtratoBancario = 4
        }


        public enum StatusImportacao
        {
            ProntoParaProcessar = 1,
            AguardandoProcessamento = 2,
            Processando = 3,
            ProcessamentoFinalizado = 4,
            Erro = 5
        }
        public enum tabsHomeCobranca
        {
            Status = 1,
            Desempenho = 2,
            Solicitacao = 3
        }
        public enum InstrucaoBoleto
        {
            Protestar = 9,
            NaoProtestar = 10,
            ProtestoFinsFalimentares = 42,
            ProtestarAposNDiasCorridos = 81,
            ProtestarAposNDiasUteis = 82,
            NaoReceberAposNDias = 91,
            DevolverAposNDias = 92,
            OutrasInstrucoes_ExibeMensagem_MoraDiaria = 900,
            OutrasInstrucoes_ExibeMensagem_MultaVencimento = 901

        }

        public enum InstrucaoBancoPortador
        {
            CobrarAtrasoDia = 1,
            CobrarMultaValor = 2,
            ConcederDesconto = 3,
            ConcederDescontoValordia = 4,
            DispensarJuros = 5,
            BancoAutorizado = 6

        }

        public enum PerfilTipo
        {
            Admin = 1,
            Corretor = 2,
            SupervisorVenda = 3,
            Atendimento = 4,
            Analista = 5,
            Regional = 6,
            Escritorio = 7,
            RegionalAdmin = 8,
            SupervisorAnalista = 9

        }
        public enum DocumentoTipo
        {
            CPF = 1,
            CNPJ = 2,
            RG = 3,
            CREA = 4,
            CRM = 5,
            OAB = 6,
            RNE = 7,
            RG_CPF = 8,
            RG_CNH = 9
        }
        public enum SimNao
        {
            Nao = 0,
            Sim = 1
        }
        public enum Sexo
        {
            Masculino = 1,
            Feminino = 2
        }
        public enum StatusDocumento
        {
            Aprovado = 1,
            Reprovado = 2,
            EmAnalise = 3
        }
        public enum StatusUnidade
        {
            Disponivel = 1,
            Indisponivel = 2,
            Bloqueada = 7,
            Permuta = 3,
            Vendida = 5,
            Andamento = 6,
            Reservada = 7
        }
        public enum CategoriaProfissional
        {
            Assalariado = 1,
            Aposentado = 2,
            DoLar = 3,
            Empresario = 4,
            Comerciante = 5,
            Industrial = 6,
            FuncionarioPublico = 7,
            ProdutorRural = 8,
            ProfissionalAutonomo = 9,
            ProfissionalLiberal = 10
        }
        public enum StatusProposta
        {
            Aprovada = 1,
            Cancelada = 2,
            ComPendencia = 3,
            Negada = 4,
            Revisao = 5,
            Solicitada = 6,
            EnviadoParaAnaliseCredito = 7,
            AnaliseCreditoNegada = 8,
            PropostaCadastrada = 9,
            SemProposta = 10
        }
        public enum ContatoTipo
        {
            TelefoneResidencial = 1,
            TelefoneComercial = 2,
            TelefoneCelular = 3,
            Email = 4,
            Fax = 5
        }


        public enum SexoTipo
        {
            Masculino = 1,
            Feminino = 2,
            Empresa = 3
        }
        public enum EnderecoTipo
        {
            Outros = 0,
            Rua = 1,
            Avenida = 2,
            Alameda = 3,
            Beco = 4,
            Condomínio = 5,
            Campo = 6,
            Estrada = 7,
            Largo = 8,
            Praça = 9,
            Praia = 10,
            Rodovia = 11,
            Quadra = 12,
            Travessa = 13
        }
        public enum ClienteEnderecoTipo
        {
            Cliente = 1,
            Cobranca = 2,
            Obra = 3

        }
        public enum ImpactoEsforcoTipo
        {
            Baixo = 1,
            Medio = 2,
            Alto = 3
        }

        public enum EtapaBoleto
        {
            AtendenteSolicitaComDesconto = 1,
            AtendenteSolicitaProrrogacao = 2,
            SupervisorAprovado = 3,
            AnalistaReprova = 4,
            SupervisorReprova = 5,
            PrazoExpira = 6,
            AtendenteSolicitaBoletoAvulso = 7,
            ReprovarObsoletos = 8
        }
        public enum TipoAcordo
        {
            AcordoEmpresa = 1,
            AcordoCliente = 2
        }
        public enum EstadoCivil
        {
            Solteiro = 1,
            Casado = 2,
            Divorciado = 3,
            Viuvo = 4,
            CasadoComunhaoUniversalBens = 5,
            CasadoParticipacaoFinalAquetos = 6,
            CasadoSeparacaoTotalBens = 7,
            UniaoEstavel = 8

        }
        public enum AndamentoObraTipo
        {
            Cancelado = 1,
            Construcao = 2,
            EmObra = 3,
            Lancamento = 4
        }
        public enum TipoContrato
        {
            Proposta = 1,
            AnaliseCredito = 2,
            ContratoDefinitivo = 2
        }
        public enum TipoFluxo
        {
            Proposta = 1,
            Cobranca = 2,
            PlantaoVendas = 3,
            AnaliseCredito = 4,
            AnalistaBoleto = 5,
            SupervisorBoleto = 6,
            FilialBoleto = 7
        }
        public enum SistemaModulo
        {

            Cobranca = 1

        }
        public enum Condicao
        {
            Igual = 1,
            Maior = 2,
            Menor = 3,
            Diferente = 4
        }
        public enum CondicaoTipo
        {
            ValorSaldo = 1,
            DiasAtraso = 2,
            TipoBloqueio = 3,
            Aging = 4,
            StatusFaseSintese = 5,
            //StatusSintese = 6,
            ValorParcela = 7,
            ValorContrato = 8,
            TipoContrato = 9,
            Empreendimento = 10,
            AndamentoObra = 11,
            DataEntregaEmpreendimento = 12,
            DataEntregaChaves = 13
        }
        public enum CondicaoTipoOperadorLogico
        {
            E = 1,
            OU = 2
        }
        public enum AcaoTipo
        {
            Cliente = 1,
            Avalista = 2,
            Todos = 3
        }
        public enum HistoricoTipo
        {
            Cliente = 2,
            Contrato = 1,
            Parcela = 3
        }
        public enum StatusParcela
        {
            Contrato = 1,
            EmNegociacao = 2
        }
        public enum Escolaridade
        {
            NaoAlfabetizado = 1,
            EnsinoFundamentalIncompleto = 2,
            EnsinoFundamentalCompleto = 3,
            MedioIncompleto = 4,
            MedioCompleto = 5,
            SuperiorIncompleto = 7,
            SuperiorCompleto = 8,
            Especializacao = 9,
            Mestrado = 10,
            Doutorado = 11
        }
        public enum TipoAcaoCobranca
        {
            SMS = 1,
            EMAIL = 2,
            CARTA = 3,
            CONTATO = 4,
            NEGATIVACAO_SERASA = 5,
            URA = 6,
            ARQUIVO_REMESSA = 7,
            EMAIL_SEGUNDA_VIA_BOLETO = 8,
            EMAIL_SENHA_USUARIO_NOVO = 9,
            EMAIL_SENHA_USUARIO_REENVIAR = 10,
            EMAIL_BOLETO_SOLICITADO = 11
        }

        public enum ReservaStatus
        {
            Reservado = 1,
            Derrubada = 2
        }
        public enum SituacaoHistorico
        {
            Enviado = 1,
            DadosInvalidos = 2
        }

        public enum ReferenciaParentescoTipo
        {
            PaiMae = 1,
            Avo = 2,
            Amigo = 3,
            Irmao = 4,
            Tio = 5
        }

        public enum InformacaoFinanceiroTipo
        {
            Comum = 1,
            Especial = 2,
            Juridica = 3,
            Poupanca = 4,
            Fundo = 5,
            Previdencia = 6,
            Aplicacao = 7,
            MasterCard = 8,
            Visa = 9,
            Diners = 10,
            Amex = 11,
            Outro = 12
        }



        public enum CompromissoFinanceiroTipo
        {
            Emprestimo = 1,
            Financiamento = 2,
            Crediario = 3,
            Consorcio = 4
        }
        public enum PatrimonioTipo
        {
            Imovel = 1,
            Veiculo = 2
        }
        public enum ImovelTipo
        {
            Apartamento = 1,
            Casa = 2,
            Terreno = 3
        }
        public enum VeiculoTipo
        {
            Automovel = 1,
            Caminhao = 2,
            Motocicleta = 3
        }
        public enum ContaTipo
        {
            Comum = 1,
            Especial = 2,
            Juridica = 3
        }
        public enum InvestimentoTipo
        {
            Poupanca = 1,
            Fundos = 2,
            PrevidenciaPrivada = 3,
            AplicacoesFinanceira = 4
        }
        public enum GrauParentesco
        {
            PaiMae = 1,
            Avo = 2,
            Amigo = 3,
            Irmao = 4,
            Tio = 5
        }

        public enum BancoCodigoOcorrencia
        {
            PedidoBaixa = 2,
            AlteracaoVencimento = 6,
            AlteracaoUsoEmpresa = 7,
            AlteracaoNossoNumero = 8,
            Protestar = 9,
            NaoProtestar = 10,
            AlteracaOutrosDados = 31,
            AlteracaoVencimentoEsustarProtesto = 37,
            BeneficiarioSolicitaDispensaJuros = 47
        }
        public enum RendaTipo
        {
            Formal = 1,
            Informal = 2
        }
        public enum Situacao
        {
            Quitado = 1,
            Financiado = 2
        }

        public enum InformacaoBancariaTipo
        {
            ContaCorrente = 1,
            Investimento = 2,
            CartaoCredito = 3
        }

        public enum CheckListTipo
        {
            Proposta = 1,
            Empreendimento = 2
        }
        public enum DocumentoStatus
        {
            Aprovado = 1,
            AprovadoRestricao = 2,
            ReprovadoDocIlegivel = 3,
            ReprovadoDocAusente = 4,
            ReprovadoCadastroDivergente = 5
        }
        public enum RespostaTipo
        {
            Texto = 1,
            Valor = 2
        }

        public enum StatusCredito
        {
            Recebido = 1,
            NaoRecebido = 2
        }
        public enum BancoPortadorLogArquivo
        {
            Remessa = 1,
            Retorno = 2

        }
        public enum StatusBoleto
        {
            SolicitacaoAtendente = 1,
            SolicitacaoAnalista = 2,
            SolicitacaoSupervisor = 3,
            Aprovado = 4,
            Reprovado = 5,
            Cancelado = 6,
            Obsoleto = 7

        }
        public enum TipoRelatorio
        {
            ParcelaEmAtraso = 1,
            ParcelaPaga = 2,
            ParcelaAberto = 3,
            ClienteSemContato = 4,
            ClienteSemEmail = 5,
            ClienteSemCpfCnpj = 6,
            ClienteContatado = 7,
            AcordosNaoRealizado = 8,
            ReguaParcelaSemAcoes = 9,
            AtendimentoAgendados = 10,
            Atendimento = 11,
            ParcelaSemAtendimento = 12,
            Teste = 13,
            AgingFilial = 14,
            GestaoBoleto = 15,
            GestaoCarta = 16,
            GestaoFilial = 17
        }

        public enum FilaAtendimentoTipoParcela
        {
            Todos = 1,
            Atraso = 2,
            aVencer = 3,
            Pago = 4
        }

        public enum NegociacaoTipoParcela
        {
            Atrasada = 1,
            aVencer = 2,
            Paga = 3,
            Cancelada = 4
        }
        public enum LogEmailStatus
        {
            AguardandoEnvio = 1,
            Enviado = 2,
            FalhaNoEnvio = 3,
            NaoEnviar = 4
        }

        public enum StatusFatura
        {
            EmDia = 1,
            Vencida = 2
        }

        public enum VersaoEmpresa
        {
            Junix = 0,
            Otis = 1
        }
        public enum StatusImportacaoParcela
        {
            EmpreendimentoInconsitente = 1,
            BlocoInconsitente = 2,
            UnidadeInconsitente = 3,
            ContratoIncosistente = 4,
            Pronto = 5,
            Erro = 6,
            Processado = 7

        }


    }

    public static class EnumeradoresDescricao
    {
        public static string IdiomaTipo(Enumeradores.IdiomaTipo tipo)
        {
            switch (tipo)
            {
                case Enumeradores.IdiomaTipo.pt_BR:
                    return "pt_BR";
                case Enumeradores.IdiomaTipo.en_US:
                    return "en_US";
                case Enumeradores.IdiomaTipo.es_ES:
                    return "es_ES";
                default:
                    return string.Empty;
            }
        }
        public static string Mes(short mes)
        {
            switch (mes)
            {
                case 1:
                    {
                        return "Janeiro";
                    }
                case 2:
                    {
                        return "Fevereiro";
                    }
                case 3:
                    {
                        return "Março";
                    }
                case 4:
                    {
                        return "Abril";
                    }
                case 5:
                    {
                        return "Maio";
                    }
                case 6:
                    {
                        return "Junho";
                    }
                case 7:
                    {
                        return "Junlho";
                    }
                case 8:
                    {
                        return "Agosto";
                    }
                case 9:
                    {
                        return "Setembro";
                    }
                case 10:
                    {
                        return "Outubro";
                    }
                case 11:
                    {
                        return "Novembro";
                    }
                case 12:
                    {
                        return "Dezembro";
                    }
                default:
                    return "--";
            }

        }

        public static string BancoCodigoOcorrencia(Enumeradores.BancoCodigoOcorrencia tp)
        {
            switch (tp)
            {
                case Enumeradores.BancoCodigoOcorrencia.PedidoBaixa:
                    return "02";
                case Enumeradores.BancoCodigoOcorrencia.AlteracaoVencimento:
                    return "06";
                case Enumeradores.BancoCodigoOcorrencia.AlteracaoUsoEmpresa:
                    return "07";
                case Enumeradores.BancoCodigoOcorrencia.AlteracaoNossoNumero:
                    return "08";
                case Enumeradores.BancoCodigoOcorrencia.Protestar:
                    return "09";
                case Enumeradores.BancoCodigoOcorrencia.NaoProtestar:
                    return "10";
                case Enumeradores.BancoCodigoOcorrencia.AlteracaOutrosDados:
                    return "31";
                case Enumeradores.BancoCodigoOcorrencia.AlteracaoVencimentoEsustarProtesto:
                    return "37";
                case Enumeradores.BancoCodigoOcorrencia.BeneficiarioSolicitaDispensaJuros:
                    return "47";
                default:
                    return "01";
            }
        }
        public static string TipoImportacaoURL(Enumeradores.TipoImportacao tp)
        {
            switch (tp)
            {
                case Enumeradores.TipoImportacao.Boleto:
                    return "/ImportacaoBoleto/ImportLista";
                case Enumeradores.TipoImportacao.Cliente:
                    return "/ImportacaoCliente/ImportLista";
                case Enumeradores.TipoImportacao.ClienteEndereco:
                    return "/ImportacaoClienteEndereco/ImportLista";
                case Enumeradores.TipoImportacao.Credito:
                    return "/ImportacaoCredito/ImportLista";
                case Enumeradores.TipoImportacao.Parcela:
                    return "/ImportacaoParcela/ImportLista";
                default:
                    return "3";
            }
        }
        public static string TipoImportacao(Enumeradores.TipoImportacao tp)
        {
            switch (tp)
            {
                case Enumeradores.TipoImportacao.Boleto:
                    return "Boletos";
                case Enumeradores.TipoImportacao.Cliente:
                    return "Clientes";
                case Enumeradores.TipoImportacao.ClienteEndereco:
                    return "Cliente Endereços";
                case Enumeradores.TipoImportacao.Credito:
                    return "Creditos";
                case Enumeradores.TipoImportacao.Parcela:
                    return "Parcelas";
                default:
                    return "--";
            }
        }
        public static string GrauParentesco(short tipo = 0)
        {
            Enumeradores.GrauParentesco tp = (Enumeradores.GrauParentesco)tipo;
            switch (tp)
            {
                case Enumeradores.GrauParentesco.PaiMae:
                    return "Pai / Mãe";
                case Enumeradores.GrauParentesco.Avo:
                    return "Avô / Avó";
                case Enumeradores.GrauParentesco.Amigo:
                    return "Amigo(a)";
                case Enumeradores.GrauParentesco.Irmao:
                    return "Irmão(a)";
                case Enumeradores.GrauParentesco.Tio:
                    return "Tio(a)";
                default:
                    return "--";
            }
        }
        public static string CompradorTipo(short tipo = 0)
        {
            Enumeradores.CompradorTipo tp = (Enumeradores.CompradorTipo)tipo;
            switch (tp)
            {
                case Enumeradores.CompradorTipo.Comprador:
                    return "Comprador";
                case Enumeradores.CompradorTipo.Conjuge:
                    return "Conjuge";
                default:
                    return "--";
            }
        }
        public static string TipoAcordo(short tipo = 0)
        {
            Enumeradores.TipoAcordo tp = (Enumeradores.TipoAcordo)tipo;
            switch (tp)
            {
                case Enumeradores.TipoAcordo.AcordoEmpresa:
                    return "Acordo Empresa";
                case Enumeradores.TipoAcordo.AcordoCliente:
                    return "Acordo Cliente";
                default:
                    return "--";
            }
        }
        public static string RespostaTipo(short tipo = 0)
        {
            Enumeradores.RespostaTipo tp = (Enumeradores.RespostaTipo)tipo;
            switch (tp)
            {
                case Enumeradores.RespostaTipo.Texto:
                    return "Texto";
                case Enumeradores.RespostaTipo.Valor:
                    return "Valor";
                default:
                    return "--";
            }
        }

        public static string TipoHistorico(short tipo = 0)
        {
            Enumeradores.TipoHistorico tp = (Enumeradores.TipoHistorico)tipo;
            switch (tp)
            {
                case Enumeradores.TipoHistorico.Contrato:
                    return "Contrato";
                case Enumeradores.TipoHistorico.Parcela:
                    return "Parcela";
                case Enumeradores.TipoHistorico.Cliente:
                    return "Cliente";
                case Enumeradores.TipoHistorico.Boleto:
                    return "Boleto";
                default:
                    return "--";
            }
        }
        public static string TipoPessoa(short tipo = 0)
        {
            Enumeradores.TipoPessoa tp = (Enumeradores.TipoPessoa)tipo;
            switch (tp)
            {
                case Enumeradores.TipoPessoa.Fisica:
                    return "Física";
                case Enumeradores.TipoPessoa.Juridica:
                    return "Jurídica";
                default:
                    return "--";
            }
        }
        public static string StatusDocumento(short tipo = 0)
        {
            Enumeradores.StatusDocumento tp = (Enumeradores.StatusDocumento)tipo;
            switch (tp)
            {
                case Enumeradores.StatusDocumento.Aprovado:
                    return "Aprovado";
                case Enumeradores.StatusDocumento.Reprovado:
                    return "Reprovado";
                case Enumeradores.StatusDocumento.EmAnalise:
                    return "Em Analise";
                default:
                    return "--";
            }
        }
        public static string TipoServico(short tipo = 0)
        {
            Enumeradores.TipoServico tp = (Enumeradores.TipoServico)tipo;
            switch (tp)
            {
                case Enumeradores.TipoServico.Classificacao:
                    return "Classificação de Faturas";
                case Enumeradores.TipoServico.Emails_Disparar:
                    return "E-Mails";
                case Enumeradores.TipoServico.Finalizar_Servico:
                    return "Serviço Finalizado";
                case Enumeradores.TipoServico.Importacao:
                    return "Importação";
                case Enumeradores.TipoServico.Iniciar_Servico:
                    return "Iniciar Serviço";
                case Enumeradores.TipoServico.Regua:
                    return "Regua Contato";
                case Enumeradores.TipoServico.Regua_Contato:
                    return "Regua Contato";
                case Enumeradores.TipoServico.Regua_Email:
                    return "Regua E-Mail";
                case Enumeradores.TipoServico.Regua_Email_boleto:
                    return "Regua E-Mail Boleto";
                case Enumeradores.TipoServico.Regua_Negativacao:
                    return "Regua SERASA";
                case Enumeradores.TipoServico.Rotinas_Servico:
                    return "Rotinas Interna";
                default:
                    return "--";
            }
        }
        public static string TipoServicoIcon(short tipo = 0)
        {
            Enumeradores.TipoServico tp = (Enumeradores.TipoServico)tipo;
            switch (tp)
            {
                case Enumeradores.TipoServico.Classificacao:
                    return "fa-check-square-o";
                case Enumeradores.TipoServico.Emails_Disparar:
                    return "fa-send-o";
                case Enumeradores.TipoServico.Finalizar_Servico:
                    return "fa-gears";
                case Enumeradores.TipoServico.Importacao:
                    return "fa-upload";
                case Enumeradores.TipoServico.Iniciar_Servico:
                    return "fa-gears";
                case Enumeradores.TipoServico.Regua:
                    return "fa-sliders";
                case Enumeradores.TipoServico.Regua_Contato:
                    return "fa-address-book-o";
                case Enumeradores.TipoServico.Regua_Email:
                    return "fa-envelope-o";
                case Enumeradores.TipoServico.Regua_Email_boleto:
                    return "fa-barcode";
                case Enumeradores.TipoServico.Regua_Negativacao:
                    return "fa-id-card-o";
                case Enumeradores.TipoServico.Rotinas_Servico:
                    return "fa-gears";
                default:
                    return "--";
            }
        }
        public static string RendaTipo(short tipo = 0)
        {
            Enumeradores.RendaTipo tp = (Enumeradores.RendaTipo)tipo;
            switch (tp)
            {
                case Enumeradores.RendaTipo.Formal:
                    return "Formal";
                case Enumeradores.RendaTipo.Informal:
                    return "Informal";
                default:
                    return "--";
            }
        }
        public static string CalendarioStatus(short status)
        {
            var tp = (Enumeradores.CalendarioStatus)status;

            switch (tp)
            {
                case Enumeradores.CalendarioStatus.Agendado:
                    return "Agendado";
                case Enumeradores.CalendarioStatus.Atrasado:
                    return "Atrasado";
                default:
                    return "";
            }
        }
        public static string Situacao(short tipo = 0)
        {
            Enumeradores.Situacao tp = (Enumeradores.Situacao)tipo;
            switch (tp)
            {
                case Enumeradores.Situacao.Financiado:
                    return "Financiado";
                case Enumeradores.Situacao.Quitado:
                    return "Quitado";
                default:
                    return "--";
            }
        }
        public static string InvestimentoTipo(short? tipo = 0)
        {
            Enumeradores.InvestimentoTipo tp = (Enumeradores.InvestimentoTipo)tipo;
            switch (tp)
            {
                case Enumeradores.InvestimentoTipo.Poupanca:
                    return "Poupança";
                case Enumeradores.InvestimentoTipo.Fundos:
                    return "Fundos";
                case Enumeradores.InvestimentoTipo.PrevidenciaPrivada:
                    return "Previdência Privada";
                case Enumeradores.InvestimentoTipo.AplicacoesFinanceira:
                    return "Aplicações Financeiras";
                default:
                    return "--";
            }
        }
        public static string ContaTipo(short? tipo = 0)
        {
            Enumeradores.ContaTipo tp = (Enumeradores.ContaTipo)tipo;
            switch (tp)
            {
                case Enumeradores.ContaTipo.Comum:
                    return "Comum";
                case Enumeradores.ContaTipo.Especial:
                    return "Especial";
                case Enumeradores.ContaTipo.Juridica:
                    return "Jurídica";
                default:
                    return "--";
            }
        }
        public static string ImovelTipo(short tipo = 0)
        {
            Enumeradores.ImovelTipo tp = (Enumeradores.ImovelTipo)tipo;
            switch (tp)
            {
                case Enumeradores.ImovelTipo.Apartamento:
                    return "Apartamento";
                case Enumeradores.ImovelTipo.Casa:
                    return "Casa";
                case Enumeradores.ImovelTipo.Terreno:
                    return "Terreno";
                default:
                    return "--";
            }
        }
        public static string VeiculoTipo(short tipo = 0)
        {
            Enumeradores.VeiculoTipo tp = (Enumeradores.VeiculoTipo)tipo;
            switch (tp)
            {
                case Enumeradores.VeiculoTipo.Automovel:
                    return "Automóvel";
                case Enumeradores.VeiculoTipo.Caminhao:
                    return "Caminhão";
                case Enumeradores.VeiculoTipo.Motocicleta:
                    return "Motocicleta";
                default:
                    return "--";
            }
        }
        public static string PatrimonioTipo(short tipo = 0)
        {
            Enumeradores.PatrimonioTipo tp = (Enumeradores.PatrimonioTipo)tipo;
            switch (tp)
            {
                case Enumeradores.PatrimonioTipo.Imovel:
                    return "Imóvel";
                case Enumeradores.PatrimonioTipo.Veiculo:
                    return "Veículo";
                default:
                    return "--";
            }
        }
        public static string CompromissoFinanceiroTipo(short? tipo = 0)
        {
            if (tipo == null)
                return "--";
            Enumeradores.CompromissoFinanceiroTipo tp = (Enumeradores.CompromissoFinanceiroTipo)tipo;
            switch (tp)
            {
                case Enumeradores.CompromissoFinanceiroTipo.Consorcio:
                    return "Consórcio";
                case Enumeradores.CompromissoFinanceiroTipo.Crediario:
                    return "Crediário";
                case Enumeradores.CompromissoFinanceiroTipo.Emprestimo:
                    return "Empréstimo";
                case Enumeradores.CompromissoFinanceiroTipo.Financiamento:
                    return "Financiamento";
                default:
                    return "--";
            }
        }
        public static string CondicaoTipo(short? tipo)
        {
            if (!tipo.HasValue)
                return "--";

            Enumeradores.CondicaoTipo tp = (Enumeradores.CondicaoTipo)tipo;
            switch (tp)
            {
                case Enumeradores.CondicaoTipo.ValorSaldo:
                    return "Valor devedor";
                case Enumeradores.CondicaoTipo.DiasAtraso:
                    return "Dias de atraso";
                case Enumeradores.CondicaoTipo.TipoBloqueio:
                    return "Tipo de bloqueio";
                case Enumeradores.CondicaoTipo.Aging:
                    return "AGING";
                case Enumeradores.CondicaoTipo.StatusFaseSintese:
                    return "Status Fase - Sintese";
                //case Enumeradores.CondicaoTipo.StatusSintese:
                //    return "Status - Sintese";
                case Enumeradores.CondicaoTipo.ValorParcela:
                    return "Valor da parcela";
                case Enumeradores.CondicaoTipo.ValorContrato:
                    return "Valor do contrato";
                case Enumeradores.CondicaoTipo.TipoContrato:
                    return "Tipo de contrato";
                case Enumeradores.CondicaoTipo.Empreendimento:
                    return "Empreendimento";
                case Enumeradores.CondicaoTipo.AndamentoObra:
                    return "Andamento Obra";
                case Enumeradores.CondicaoTipo.DataEntregaEmpreendimento:
                    return "Data entrega do empreendimento";
                case Enumeradores.CondicaoTipo.DataEntregaChaves:
                    return "Data entrega de chaves";
                default:
                    return "--";
            }
        }
        public static string ClienteEnderecoTipo(short? tipo)
        {
            if (!tipo.HasValue)
                return "--";

            Enumeradores.ClienteEnderecoTipo tp = (Enumeradores.ClienteEnderecoTipo)tipo;
            switch (tp)
            {
                case Enumeradores.ClienteEnderecoTipo.Cliente:
                    return "Endereço Cliente";
                case Enumeradores.ClienteEnderecoTipo.Cobranca:
                    return "Endereço Cobrança";
                case Enumeradores.ClienteEnderecoTipo.Obra:
                    return "Endereço Obra";

                default:
                    return "--";
            }
        }

        public static string Condicao(short? tipo = 0)
        {
            if (!tipo.HasValue)
                return "--";

            Enumeradores.Condicao tp = (Enumeradores.Condicao)tipo;

            switch (tp)
            {
                case Enumeradores.Condicao.Igual:
                    return "Igual";
                case Enumeradores.Condicao.Maior:
                    return "Maior";
                case Enumeradores.Condicao.Menor:
                    return "Menor";
                case Enumeradores.Condicao.Diferente:
                    return "Diferente";
                default:
                    return "--";
            }
        }
        public static string DocumentoComprobatorio(short? tipo = 0)
        {
            if (!tipo.HasValue)
                return "--";

            Enumeradores.DocumentoComprobatorio tp = (Enumeradores.DocumentoComprobatorio)tipo;

            switch (tp)
            {
                case Enumeradores.DocumentoComprobatorio.Alugueis:
                    return "Alugueis";
                case Enumeradores.DocumentoComprobatorio.ContratoPrestacaoServico:
                    return "Contrato Prestação de Serviço";
                case Enumeradores.DocumentoComprobatorio.DeclaracaoCooperativa:
                    return "Declaração Cooperativa/Sindicato/Associação";
                case Enumeradores.DocumentoComprobatorio.ExtratoBancario:
                    return "Extrato Bancario";
                default:
                    return "--";
            }
        }
        public static string AndamentoObraTipo(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }

            Enumeradores.AndamentoObraTipo tp = (Enumeradores.AndamentoObraTipo)tipo;

            switch (tp)
            {
                case Enumeradores.AndamentoObraTipo.Cancelado:
                    return "Cancelado";
                case Enumeradores.AndamentoObraTipo.Construcao:
                    return "Construção";
                case Enumeradores.AndamentoObraTipo.EmObra:
                    return "Em Obra No Prazo";
                case Enumeradores.AndamentoObraTipo.Lancamento:
                    return "Lançamento";
                default:
                    return "--";
            }
        }
        public static string DocumentoTipo(short tipo = 0)
        {
            Enumeradores.DocumentoTipo tp = (Enumeradores.DocumentoTipo)tipo;

            switch (tp)
            {
                case Enumeradores.DocumentoTipo.CPF:
                    return "CPF";
                case Enumeradores.DocumentoTipo.CNPJ:
                    return "CNPJ";
                case Enumeradores.DocumentoTipo.RG:
                    return "RG";
                case Enumeradores.DocumentoTipo.CREA:
                    return "CREA";
                case Enumeradores.DocumentoTipo.CRM:
                    return "CRM";
                case Enumeradores.DocumentoTipo.OAB:
                    return "OAB";
                case Enumeradores.DocumentoTipo.RNE:
                    return "RNE";
                case Enumeradores.DocumentoTipo.RG_CNH:
                    return "RG ou CNH";
                case Enumeradores.DocumentoTipo.RG_CPF:
                    return "RG ou CPF";
                default:
                    return "--";

            }
        }
        public static string EstadoCivil(short tipo = 0)
        {
            Enumeradores.EstadoCivil tp = (Enumeradores.EstadoCivil)tipo;

            switch (tp)
            {
                case Enumeradores.EstadoCivil.Solteiro:
                    return "Solteiro(a)";
                case Enumeradores.EstadoCivil.Casado:
                    return "Casado - Comunhão Parcial de Bens";
                case Enumeradores.EstadoCivil.Divorciado:
                    return "Divorciado(a)";
                case Enumeradores.EstadoCivil.Viuvo:
                    return "Viuvo(a)";
                case Enumeradores.EstadoCivil.CasadoComunhaoUniversalBens:
                    return "Casado - Comunhão Universal de Bens";
                case Enumeradores.EstadoCivil.CasadoParticipacaoFinalAquetos:
                    return "Casado - Parcitipação Final nos Aquetos";
                case Enumeradores.EstadoCivil.CasadoSeparacaoTotalBens:
                    return "Casado - Separação Total de Bens";
                case Enumeradores.EstadoCivil.UniaoEstavel:
                    return "União Estavel";
                default:
                    return "--";
            }
        }
        public static string Escolaridade(short tipo = 0)
        {
            Enumeradores.Escolaridade tp = (Enumeradores.Escolaridade)tipo;

            switch (tp)
            {
                case Enumeradores.Escolaridade.Doutorado:
                    return "Doutorado";
                case Enumeradores.Escolaridade.EnsinoFundamentalCompleto:
                    return "Ensino Fundamental Completo";
                case Enumeradores.Escolaridade.EnsinoFundamentalIncompleto:
                    return "Ensino Fundamental Incompleto";
                case Enumeradores.Escolaridade.Especializacao:
                    return "Especialização";
                case Enumeradores.Escolaridade.MedioCompleto:
                    return "Médio Completo";
                case Enumeradores.Escolaridade.MedioIncompleto:
                    return "Médio Incompleto";
                case Enumeradores.Escolaridade.Mestrado:
                    return "Mestrado";
                case Enumeradores.Escolaridade.NaoAlfabetizado:
                    return "Não Alfabetizado";
                case Enumeradores.Escolaridade.SuperiorCompleto:
                    return "Superior Completo";
                case Enumeradores.Escolaridade.SuperiorIncompleto:
                    return "Superior Incompleto";
                default:
                    return "--";
            }
        }
        public static string CategoriaProfissional(short? tipo)
        {
            if (!tipo.HasValue)
                return "--";

            Enumeradores.CategoriaProfissional tp = (Enumeradores.CategoriaProfissional)tipo;
            switch (tp)
            {
                case Enumeradores.CategoriaProfissional.Aposentado:
                    return "Aposentado";
                case Enumeradores.CategoriaProfissional.Assalariado:
                    return "Assalariado";
                case Enumeradores.CategoriaProfissional.Comerciante:
                    return "Comerciante";
                case Enumeradores.CategoriaProfissional.DoLar:
                    return "Do Lar";
                case Enumeradores.CategoriaProfissional.Empresario:
                    return "Empresário";
                case Enumeradores.CategoriaProfissional.FuncionarioPublico:
                    return "Funcionario Publico";
                case Enumeradores.CategoriaProfissional.Industrial:
                    return "Industrial";
                case Enumeradores.CategoriaProfissional.ProdutorRural:
                    return "Produtor Rural";
                case Enumeradores.CategoriaProfissional.ProfissionalAutonomo:
                    return "Profissional Autonomo";
                case Enumeradores.CategoriaProfissional.ProfissionalLiberal:
                    return "Profissional Liberal";
                default:
                    return "--";
            }
        }
        public static string ContatoTipo(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }

            Enumeradores.ContatoTipo tp = (Enumeradores.ContatoTipo)tipo;

            switch (tp)
            {
                case Enumeradores.ContatoTipo.TelefoneResidencial:
                    return "Residêncial";
                case Enumeradores.ContatoTipo.TelefoneComercial:
                    return "Comercial";
                case Enumeradores.ContatoTipo.TelefoneCelular:
                    return "Celular";
                case Enumeradores.ContatoTipo.Email:
                    return "Email";
                case Enumeradores.ContatoTipo.Fax:
                    return "Fax";
                default:
                    return "--";
            }
        }
        public static string EnderecoTipo(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }

            Enumeradores.EnderecoTipo tp = (Enumeradores.EnderecoTipo)tipo;

            switch (tp)
            {
                case Enumeradores.EnderecoTipo.Alameda:
                    return "Alameda";
                case Enumeradores.EnderecoTipo.Avenida:
                    return "Avenida";
                case Enumeradores.EnderecoTipo.Beco:
                    return "Beco";
                case Enumeradores.EnderecoTipo.Campo:
                    return "Campo";
                case Enumeradores.EnderecoTipo.Estrada:
                    return "Estrada";
                case Enumeradores.EnderecoTipo.Largo:
                    return "Largo";
                case Enumeradores.EnderecoTipo.Praça:
                    return "Praça";
                case Enumeradores.EnderecoTipo.Praia:
                    return "Praia";
                case Enumeradores.EnderecoTipo.Quadra:
                    return "Quadra";
                case Enumeradores.EnderecoTipo.Rodovia:
                    return "Rodovia";
                case Enumeradores.EnderecoTipo.Rua:
                    return "Rua";
                case Enumeradores.EnderecoTipo.Travessa:
                    return "Travessa";
                default:
                    return "--";
            }
        }
        public static string PerfilTipo(Enumeradores.PerfilTipo tp)
        {
            switch (tp)
            {
                case Enumeradores.PerfilTipo.Admin:
                    return "Admin";
                case Enumeradores.PerfilTipo.Corretor:
                    return "Corretor";
                case Enumeradores.PerfilTipo.SupervisorVenda:
                    return "Supervisor de Venda";
                case Enumeradores.PerfilTipo.Analista:
                    return "Analista";
                case Enumeradores.PerfilTipo.Atendimento:
                    return "Atendimento";
                case Enumeradores.PerfilTipo.Regional:
                    return "Filial";
                case Enumeradores.PerfilTipo.Escritorio:
                    return "Escritório";
                case Enumeradores.PerfilTipo.RegionalAdmin:
                    return "Filial Admin";
                case Enumeradores.PerfilTipo.SupervisorAnalista:
                    return "Supervisor do Analista";
                default:
                    return "--";
            }
        }




        public static string InstrucaoBoleto(Enumeradores.InstrucaoBoleto tp)
        {
            switch (tp)
            {
                case Enumeradores.InstrucaoBoleto.Protestar:
                    return "Protestar";
                case Enumeradores.InstrucaoBoleto.NaoProtestar:
                    return "Não protestar";
                case Enumeradores.InstrucaoBoleto.ProtestoFinsFalimentares:
                    return "Protesto para fins falimentares";
                case Enumeradores.InstrucaoBoleto.ProtestarAposNDiasCorridos:
                    return "Protestar após @Quantidade dias corridos do vencimento";
                case Enumeradores.InstrucaoBoleto.ProtestarAposNDiasUteis:
                    return "Protestar após @Quantidade dias úteis do vencimento";
                case Enumeradores.InstrucaoBoleto.NaoReceberAposNDias:
                    return "Não receber após @Quantidade dias do vencimento";
                case Enumeradores.InstrucaoBoleto.DevolverAposNDias:
                    return "Devolver após @Quantidade dias do vencimento";
                default:
                    return "--";
            }
        }
        public static string InstrucaoBancoPortador(Enumeradores.InstrucaoBancoPortador tp)
        {
            switch (tp)
            {
                case Enumeradores.InstrucaoBancoPortador.CobrarAtrasoDia:
                    return "Após (DD/MM/AAAA), cobrar R$(valor) por dia de Atraso";
                case Enumeradores.InstrucaoBancoPortador.CobrarMultaValor:
                    return "Após (DD/MM/AAAA), cobrar multa de R$(valor)";
                case Enumeradores.InstrucaoBancoPortador.ConcederDesconto:
                    return "Até (DD/MM/AAAA) conceder desconto de R$(valor)";
                case Enumeradores.InstrucaoBancoPortador.ConcederDescontoValordia:
                    return "Ate (DD/MM/AAAA) conceder desconto de R$(valor) por dia de antecipação";
                case Enumeradores.InstrucaoBancoPortador.DispensarJuros:
                    return "Dispensar juros de mora até (DD/MM/AAAA)";
                case Enumeradores.InstrucaoBancoPortador.BancoAutorizado:
                    return "Banco autorizado a receber até (DD/MM/AAAA)";
                default:
                    return "--";
            }
        }




        public static string SexoTipo(Enumeradores.SexoTipo tp)
        {
            switch (tp)
            {
                case Enumeradores.SexoTipo.Masculino:
                    return "Masculino";
                case Enumeradores.SexoTipo.Feminino:
                    return "Feminino";
                case Enumeradores.SexoTipo.Empresa:
                    return "Empresa";
                default:
                    return "--";
            }
        }
        public static string NegociacaoTipoParcela(short tipo)
        {
            Enumeradores.NegociacaoTipoParcela tp = (Enumeradores.NegociacaoTipoParcela)tipo;
            switch (tp)
            {
                case Enumeradores.NegociacaoTipoParcela.Atrasada:
                    return "Vencida";
                case Enumeradores.NegociacaoTipoParcela.aVencer:
                    return "À Vencer";
                case Enumeradores.NegociacaoTipoParcela.Paga:
                    return "Paga";
                case Enumeradores.NegociacaoTipoParcela.Cancelada:
                    return "Cancelada";
                default:
                    return "--";
            }
        }
        public static string StatusParcela(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }
            Enumeradores.StatusParcela tp = (Enumeradores.StatusParcela)tipo;
            switch (tp)
            {
                case Enumeradores.StatusParcela.Contrato:
                    return "Contrato";
                case Enumeradores.StatusParcela.EmNegociacao:
                    return "Em Negociação";
                default:
                    return "--";
            }
        }
        public static string SimNao(short? tipo)
        {
            if (tipo == null)
            {
                tipo = 0;
            }
            Enumeradores.SimNao tp = (Enumeradores.SimNao)tipo;
            switch (tp)
            {
                case Enumeradores.SimNao.Nao:
                    return "Não";
                case Enumeradores.SimNao.Sim:
                    return "Sim";
                default:
                    return "--";
            }
        }
        public static string Sexo(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }
            Enumeradores.Sexo tp = (Enumeradores.Sexo)tipo;
            switch (tp)
            {
                case Enumeradores.Sexo.Masculino:
                    return "Masculino";
                case Enumeradores.Sexo.Feminino:
                    return "Feminino";
                default:
                    return "--";
            }
        }
        public static string StatusUnidade(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }
            Enumeradores.StatusUnidade tp = (Enumeradores.StatusUnidade)tipo;
            switch (tp)
            {
                case Enumeradores.StatusUnidade.Bloqueada:
                    return "Bloqueada";
                case Enumeradores.StatusUnidade.Disponivel:
                    return "Disponivel";
                case Enumeradores.StatusUnidade.Indisponivel:
                    return "Indisponivel";
                case Enumeradores.StatusUnidade.Permuta:
                    return "Permuta";
                case Enumeradores.StatusUnidade.Vendida:
                    return "Vendida";
                case Enumeradores.StatusUnidade.Andamento:
                    return "Andamento";
                //case Enumeradores.StatusUnidade.Reservada:
                //    return "Reservada";
                default:
                    return "--";
            }
        }
        public static string TipoFluxo(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }
            Enumeradores.TipoFluxo tp = (Enumeradores.TipoFluxo)tipo;
            switch (tp)
            {
                case Enumeradores.TipoFluxo.AnaliseCredito:
                    return "Crédito";
                case Enumeradores.TipoFluxo.AnalistaBoleto:
                    return "Analista";
                case Enumeradores.TipoFluxo.Cobranca:
                    return "Cobrança";
                case Enumeradores.TipoFluxo.FilialBoleto:
                    return "Filial";
                case Enumeradores.TipoFluxo.PlantaoVendas:
                    return "Plantao";
                case Enumeradores.TipoFluxo.Proposta:
                    return "Proposta";
                case Enumeradores.TipoFluxo.SupervisorBoleto:
                    return "Supervisor";
                //case Enumeradores.StatusUnidade.Reservada:
                //    return "Reservada";
                default:
                    return "--";
            }
        }
        public static string StatusProposta(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }
            Enumeradores.StatusProposta tp = (Enumeradores.StatusProposta)tipo;
            switch (tp)
            {
                case Enumeradores.StatusProposta.Aprovada:
                    return "Aprovada";
                case Enumeradores.StatusProposta.Cancelada:
                    return "Cancelada";
                case Enumeradores.StatusProposta.ComPendencia:
                    return "Com Pendência";
                case Enumeradores.StatusProposta.Solicitada:
                    return "Solicitada";
                case Enumeradores.StatusProposta.Negada:
                    return "Negada";
                case Enumeradores.StatusProposta.Revisao:
                    return "Revisao";
                case Enumeradores.StatusProposta.PropostaCadastrada:
                    return "Cadastrada";
                case Enumeradores.StatusProposta.EnviadoParaAnaliseCredito:
                    return "Envido para analise";
                case Enumeradores.StatusProposta.SemProposta:
                    return "SemProposta";
                default:
                    return "--";
            }
        }
        public static object EtapaBoleto(short? tipo)
        {
            if (tipo == null)
                return "--";

            Enumeradores.EtapaBoleto tp = (Enumeradores.EtapaBoleto)tipo;
            switch (tp)
            {
                case Enumeradores.EtapaBoleto.AtendenteSolicitaComDesconto:
                    return "Solicita um Boleto Novo";
                case Enumeradores.EtapaBoleto.AtendenteSolicitaProrrogacao:
                    return "Solicita um Boleto Existente";
                case Enumeradores.EtapaBoleto.SupervisorAprovado:
                    return "Boleto Aprovado";
                case Enumeradores.EtapaBoleto.AnalistaReprova:
                    return "Analista Reprova";
                case Enumeradores.EtapaBoleto.SupervisorReprova:
                    return "Supervisor Reprova";
                case Enumeradores.EtapaBoleto.PrazoExpira:
                    return "Prazo Expirou";
                case Enumeradores.EtapaBoleto.AtendenteSolicitaBoletoAvulso:
                    return "Boleto Avulso";
                case Enumeradores.EtapaBoleto.ReprovarObsoletos:
                    return "Reprovar Obsoletos";
                default:
                    return "--";
            }
        }
        
        public static object ImpactoEsforcoTipo(short? tipo)
        {
            if (tipo == null)
                return "--";

            Enumeradores.ImpactoEsforcoTipo tp = (Enumeradores.ImpactoEsforcoTipo)tipo;
            switch (tp)
            {
                case Enumeradores.ImpactoEsforcoTipo.Baixo:
                    return "Baixo";
                case Enumeradores.ImpactoEsforcoTipo.Medio:
                    return "Medio";
                case Enumeradores.ImpactoEsforcoTipo.Alto:
                    return "Alto";
                default:
                    return "--";
            }
        }
        public static string TipoAcaoCobranca(short? tipo)
        {
            if (tipo == null)
                return "--";

            Enumeradores.TipoAcaoCobranca tp = (Enumeradores.TipoAcaoCobranca)tipo;
            switch (tp)
            {
                case Enumeradores.TipoAcaoCobranca.CARTA:
                    return "Carta";
                case Enumeradores.TipoAcaoCobranca.CONTATO:
                    return "Atendimento";
                case Enumeradores.TipoAcaoCobranca.EMAIL:
                    return "Email";
                case Enumeradores.TipoAcaoCobranca.NEGATIVACAO_SERASA:
                    return "Negativação";
                case Enumeradores.TipoAcaoCobranca.SMS:
                    return "Sms";
                case Enumeradores.TipoAcaoCobranca.URA:
                    return "URA";
                case Enumeradores.TipoAcaoCobranca.ARQUIVO_REMESSA:
                    return "Arquivo Remessa";
                case Enumeradores.TipoAcaoCobranca.EMAIL_SEGUNDA_VIA_BOLETO:
                    return "Email 2ª via de boleto";
                case Enumeradores.TipoAcaoCobranca.EMAIL_SENHA_USUARIO_NOVO:
                    return "Email Novo Usuário";
                case Enumeradores.TipoAcaoCobranca.EMAIL_SENHA_USUARIO_REENVIAR:
                    return "Email Reenviar Senha";
                case Enumeradores.TipoAcaoCobranca.EMAIL_BOLETO_SOLICITADO:
                    return "Email Boleto Solicitado";
                default:
                    return "--";
            }
        }
        public static object SituacaoHistorico(short? tipo)
        {
            if (tipo == null)
                return "--";

            Enumeradores.SituacaoHistorico tp = (Enumeradores.SituacaoHistorico)tipo;
            switch (tp)
            {
                case Enumeradores.SituacaoHistorico.Enviado:
                    return "Enviado";
                case Enumeradores.SituacaoHistorico.DadosInvalidos:
                    return "inconsistente";
                default:
                    return "--";
            }
        }

        public static string ReferenciaParentescoTipo(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }

            Enumeradores.ReferenciaParentescoTipo tp = (Enumeradores.ReferenciaParentescoTipo)tipo;

            switch (tp)
            {
                case Enumeradores.ReferenciaParentescoTipo.Amigo:
                    return "Amigo(a)";
                case Enumeradores.ReferenciaParentescoTipo.Avo:
                    return "Avó /Avô";
                case Enumeradores.ReferenciaParentescoTipo.Irmao:
                    return "Irmão(a)";
                case Enumeradores.ReferenciaParentescoTipo.PaiMae:
                    return "Pai / Mãe";
                case Enumeradores.ReferenciaParentescoTipo.Tio:
                    return "Tio(a)";
                default:
                    return "--";
            }
        }

        public static string FinanceiroContaTipo(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }

            Enumeradores.InformacaoFinanceiroTipo tp = (Enumeradores.InformacaoFinanceiroTipo)tipo;

            switch (tp)
            {
                case Enumeradores.InformacaoFinanceiroTipo.Comum:
                    return "Comum";
                case Enumeradores.InformacaoFinanceiroTipo.Especial:
                    return "Especial";
                case Enumeradores.InformacaoFinanceiroTipo.Juridica:
                    return "Jurídica";
                default:
                    return "--";
            }
        }

        public static string InvestimentoContaTipo(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }

            Enumeradores.InformacaoFinanceiroTipo tp = (Enumeradores.InformacaoFinanceiroTipo)tipo;

            switch (tp)
            {
                case Enumeradores.InformacaoFinanceiroTipo.Aplicacao:
                    return "Aplicação";
                case Enumeradores.InformacaoFinanceiroTipo.Fundo:
                    return "Fundo";
                case Enumeradores.InformacaoFinanceiroTipo.Poupanca:
                    return "Poupança";
                case Enumeradores.InformacaoFinanceiroTipo.Previdencia:
                    return "Previdência";
                default:
                    return "--";
            }
        }

        public static string CartaoBandeiraTipo(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }

            Enumeradores.InformacaoFinanceiroTipo tp = (Enumeradores.InformacaoFinanceiroTipo)tipo;

            switch (tp)
            {
                case Enumeradores.InformacaoFinanceiroTipo.MasterCard:
                    return "MASTERCARD";
                case Enumeradores.InformacaoFinanceiroTipo.Visa:
                    return "VISA";
                case Enumeradores.InformacaoFinanceiroTipo.Diners:
                    return "DINERS";
                case Enumeradores.InformacaoFinanceiroTipo.Amex:
                    return "AMEX";
                case Enumeradores.InformacaoFinanceiroTipo.Outro:
                    return "OUTROS";
                default:
                    return "--";
            }
        }

        public static string CheckListTipo(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }

            Enumeradores.CheckListTipo tp = (Enumeradores.CheckListTipo)tipo;

            switch (tp)
            {
                case Enumeradores.CheckListTipo.Empreendimento:
                    return "Empreendimento";
                case Enumeradores.CheckListTipo.Proposta:
                    return "Proposta";
                default:
                    return "--";
            }
        }
        public static string DocumentoStatus(short tipo = 0)
        {
            Enumeradores.DocumentoStatus tp = (Enumeradores.DocumentoStatus)tipo;
            switch (tp)
            {
                case Enumeradores.DocumentoStatus.Aprovado:
                    return "Aprovado";
                case Enumeradores.DocumentoStatus.AprovadoRestricao:
                    return "Aprovado com Restrição";
                case Enumeradores.DocumentoStatus.ReprovadoDocIlegivel:
                    return "Reprovado - Doc. Ilegível";
                case Enumeradores.DocumentoStatus.ReprovadoDocAusente:
                    return "Reprovado - Doc. Ausente";
                case Enumeradores.DocumentoStatus.ReprovadoCadastroDivergente:
                    return "Reprovado - Cadastro Divergente";
                default:
                    return "--";
            }
        }

        public static string StatusFatura(short? tipo)
        {
            if (!tipo.HasValue)
            {
                return "--";
            }

            Enumeradores.StatusFatura tp = (Enumeradores.StatusFatura)tipo;

            switch (tp)
            {
                case Enumeradores.StatusFatura.EmDia:
                    return "Em Dia";
                case Enumeradores.StatusFatura.Vencida:
                    return "Vencida";
                default:
                    return "--";
            }
        }
        internal static string StatusImportacaoParcela(short status)
        {
            switch ((Enumeradores.StatusImportacaoParcela)status)
            {
                case Enumeradores.StatusImportacaoParcela.EmpreendimentoInconsitente:
                    return "Inconsistência de Empreendimento";
                case Enumeradores.StatusImportacaoParcela.BlocoInconsitente:
                    return "Inconsistência de Bloco";
                case Enumeradores.StatusImportacaoParcela.UnidadeInconsitente:
                    return "Inconsistência de Unidade";
                case Enumeradores.StatusImportacaoParcela.ContratoIncosistente:
                    return "Inconsistência de Contrato";
                case Enumeradores.StatusImportacaoParcela.Pronto:
                    return "Pronto para processamento";
                default:
                    return "--";
            }
        }

        public static string TabelaParcelas(Enumeradores.ChaveModelo tipo)
        {
            switch (tipo)
            {
                case Enumeradores.ChaveModelo.ValorParcela:
                    return "@VALOR_PARCELA";
                case Enumeradores.ChaveModelo.DataVencimentoParcela:
                    return "@DATA_VENCIMENTO_PARCELA";
                case Enumeradores.ChaveModelo.NomeComprador:
                    return "@NOME_COMPRADOR";
                case Enumeradores.ChaveModelo.CPF_CPNPJ_Comprador:
                    return "@CPF_CNPJ_COMPRADO";
                case Enumeradores.ChaveModelo.NumeroContrato:
                    return "@NUMERO_CONTRATO";
                case Enumeradores.ChaveModelo.SaldoDevedor:
                    return "@SALDO_DEVEDOR";
                case Enumeradores.ChaveModelo.NumeroParcela:
                    return "@NUMERO_PARCELA";
                case Enumeradores.ChaveModelo.NumeroFilial:
                    return "@NUMERO_FILIAL";
                case Enumeradores.ChaveModelo.DescricaoFilial:
                    return "@DESCRICAO_FILIAL";
                case Enumeradores.ChaveModelo.ResponsavelFilial:
                    return "@NOME_RESPONSAVEL_FILIAL";
                case Enumeradores.ChaveModelo.NumeroContato:
                    return "@VALOR_PARCELA";
                case Enumeradores.ChaveModelo.TabelaParcelas:
                    return "@TABELA_PARCELAS";
                case Enumeradores.ChaveModelo.ValorEmAberto:
                    return "@VALOR_EM_ABERTO";
                case Enumeradores.ChaveModelo.AtendimentoUsuairo:
                    return "@NOME_ATENDENTE";
                default:
                    return string.Empty;
            }
        }


        public static string ReguaFilialTipo(int tp)
        {
            Enumeradores.ReguaFilialTipo tipo = (Enumeradores.ReguaFilialTipo)tp;

            switch (tipo)
            {
                case Enumeradores.ReguaFilialTipo.SP:
                    return "SP";
                case Enumeradores.ReguaFilialTipo.SUL:
                    return "SUL";
                case Enumeradores.ReguaFilialTipo.NRT:
                    return "NRT";
                case Enumeradores.ReguaFilialTipo.CTR:
                    return "CTR";
                default:
                    return "";
            }
        }


    }

    public static class EnumeradoresLista
    {


        public static List<DictionaryEntry> ListaChaveModelo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            lista.Add(new DictionaryEntry() { Key = "@CPF_CNPJ_COMPRADOR", Value = (short)Enumeradores.ChaveModelo.CPF_CPNPJ_Comprador });
            lista.Add(new DictionaryEntry() { Key = "@DATA_VENCIMENTO_PARCELA", Value = (short)Enumeradores.ChaveModelo.DataVencimentoParcela });
            lista.Add(new DictionaryEntry() { Key = "@DESCRICAO_FILIAL", Value = (short)Enumeradores.ChaveModelo.DescricaoFilial });
            lista.Add(new DictionaryEntry() { Key = "@NOME_COMPRADOR", Value = (short)Enumeradores.ChaveModelo.NomeComprador });
            lista.Add(new DictionaryEntry() { Key = "@NUMERO_CONTRATO", Value = (short)Enumeradores.ChaveModelo.NumeroContato });
            lista.Add(new DictionaryEntry() { Key = "@NUMERO_FILIAL", Value = (short)Enumeradores.ChaveModelo.NumeroFilial });
            lista.Add(new DictionaryEntry() { Key = "@NUMERO_PARCELA", Value = (short)Enumeradores.ChaveModelo.NumeroParcela });
            lista.Add(new DictionaryEntry() { Key = "@NOME_RESPONSAVEL_FILIAL", Value = (short)Enumeradores.ChaveModelo.ResponsavelFilial });
            lista.Add(new DictionaryEntry() { Key = "@SALDO_DEVEDOR", Value = (short)Enumeradores.ChaveModelo.SaldoDevedor });
            lista.Add(new DictionaryEntry() { Key = "@TABELA_PARCELAS", Value = (short)Enumeradores.ChaveModelo.TabelaParcelas });
            lista.Add(new DictionaryEntry() { Key = "@VALOR_PARCELA", Value = (short)Enumeradores.ChaveModelo.ValorParcela });

            return lista;
        }
        public static List<DictionaryEntry> ListaChaveModeloUsuario()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            lista.Add(new DictionaryEntry() { Key = "@LOGIN", Value = (short)Enumeradores.ChaveModelo.CPF_CPNPJ_Comprador });
            lista.Add(new DictionaryEntry() { Key = "@SENHA", Value = (short)Enumeradores.ChaveModelo.DataVencimentoParcela });
            return lista;
        }

        public static List<DictionaryEntry> LayoutSistema()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = "Padrão", Value = (short)Enumeradores.LayoutSistema.Padrao });
            lista.Add(new DictionaryEntry() { Key = "Azul", Value = (short)Enumeradores.LayoutSistema.Blue });
            lista.Add(new DictionaryEntry() { Key = "Azul Dark", Value = (short)Enumeradores.LayoutSistema.Blue2 });
            lista.Add(new DictionaryEntry() { Key = "Verde", Value = (short)Enumeradores.LayoutSistema.Green });
            lista.Add(new DictionaryEntry() { Key = "Roxo", Value = (short)Enumeradores.LayoutSistema.Purple });
            lista.Add(new DictionaryEntry() { Key = "Cinza", Value = (short)Enumeradores.LayoutSistema.Gray });
            //
            return lista;
        }
        public static List<DictionaryEntry> DocumentoStatus()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoStatus((short)Enumeradores.DocumentoStatus.Aprovado), Value = (short)Enumeradores.DocumentoStatus.Aprovado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoStatus((short)Enumeradores.DocumentoStatus.AprovadoRestricao), Value = (short)Enumeradores.DocumentoStatus.AprovadoRestricao });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoStatus((short)Enumeradores.DocumentoStatus.ReprovadoDocIlegivel), Value = (short)Enumeradores.DocumentoStatus.ReprovadoDocIlegivel });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoStatus((short)Enumeradores.DocumentoStatus.ReprovadoDocAusente), Value = (short)Enumeradores.DocumentoStatus.ReprovadoDocAusente });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoStatus((short)Enumeradores.DocumentoStatus.ReprovadoCadastroDivergente), Value = (short)Enumeradores.DocumentoStatus.ReprovadoCadastroDivergente });
            return lista;
        }
        public static List<DictionaryEntry> RespostaTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.RespostaTipo((short)Enumeradores.RespostaTipo.Texto), Value = (short)Enumeradores.RespostaTipo.Texto });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.RespostaTipo((short)Enumeradores.RespostaTipo.Valor), Value = (short)Enumeradores.RespostaTipo.Valor });
            return lista;
        }
        public static List<DictionaryEntry> DocumentoTipo(Enumeradores.TipoPessoa tpPessoa)
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            switch (tpPessoa)
            {
                case Enumeradores.TipoPessoa.Fisica:
                    {
                        lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoTipo((short)Enumeradores.DocumentoTipo.CPF), Value = (short)Enumeradores.DocumentoTipo.CPF });
                        lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoTipo((short)Enumeradores.DocumentoTipo.RG), Value = (short)Enumeradores.DocumentoTipo.RG });
                        lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoTipo((short)Enumeradores.DocumentoTipo.RG_CNH), Value = (short)Enumeradores.DocumentoTipo.RG_CNH });
                        lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoTipo((short)Enumeradores.DocumentoTipo.RG_CPF), Value = (short)Enumeradores.DocumentoTipo.RG_CPF });

                        break;
                    }
                case Enumeradores.TipoPessoa.Juridica:
                    {
                        lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoTipo((short)Enumeradores.DocumentoTipo.CNPJ), Value = (short)Enumeradores.DocumentoTipo.CNPJ });

                        break;
                    }
                default:
                    break;
            }

            return lista;
        }
        public static List<DictionaryEntry> DocumentoTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoTipo((short)Enumeradores.DocumentoTipo.CREA), Value = (short)Enumeradores.DocumentoTipo.CREA });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoTipo((short)Enumeradores.DocumentoTipo.CRM), Value = (short)Enumeradores.DocumentoTipo.CRM });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoTipo((short)Enumeradores.DocumentoTipo.OAB), Value = (short)Enumeradores.DocumentoTipo.OAB });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoTipo((short)Enumeradores.DocumentoTipo.RG), Value = (short)Enumeradores.DocumentoTipo.RG });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoTipo((short)Enumeradores.DocumentoTipo.RNE), Value = (short)Enumeradores.DocumentoTipo.RNE });

            return lista;
        }
        public static List<DictionaryEntry> CompradorTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CompradorTipo((short)Enumeradores.CompradorTipo.Comprador), Value = (short)Enumeradores.CompradorTipo.Comprador });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CompradorTipo((short)Enumeradores.CompradorTipo.Conjuge), Value = (short)Enumeradores.CompradorTipo.Conjuge });

            return lista;
        }
        public static List<DictionaryEntry> RendaTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.RendaTipo((short)Enumeradores.RendaTipo.Formal), Value = (short)Enumeradores.RendaTipo.Formal });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.RendaTipo((short)Enumeradores.RendaTipo.Informal), Value = (short)Enumeradores.RendaTipo.Informal });

            return lista;
        }
        public static List<DictionaryEntry> TipoAcordo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcordo((short)Enumeradores.TipoAcordo.AcordoCliente), Value = (short)Enumeradores.TipoAcordo.AcordoCliente });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcordo((short)Enumeradores.TipoAcordo.AcordoEmpresa), Value = (short)Enumeradores.TipoAcordo.AcordoEmpresa });
            return lista;
        }
        public static List<DictionaryEntry> Situacao()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Situacao((short)Enumeradores.Situacao.Financiado), Value = (short)Enumeradores.Situacao.Financiado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Situacao((short)Enumeradores.Situacao.Quitado), Value = (short)Enumeradores.Situacao.Quitado });

            return lista;
        }
        public static List<DictionaryEntry> VeiculoTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.VeiculoTipo((short)Enumeradores.VeiculoTipo.Automovel), Value = (short)Enumeradores.VeiculoTipo.Automovel });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.VeiculoTipo((short)Enumeradores.VeiculoTipo.Caminhao), Value = (short)Enumeradores.VeiculoTipo.Caminhao });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.VeiculoTipo((short)Enumeradores.VeiculoTipo.Motocicleta), Value = (short)Enumeradores.VeiculoTipo.Motocicleta });

            return lista;
        }
        public static List<DictionaryEntry> StatusDocumento()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusDocumento((short)Enumeradores.StatusDocumento.Aprovado), Value = (short)Enumeradores.StatusDocumento.Aprovado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusDocumento((short)Enumeradores.StatusDocumento.Reprovado), Value = (short)Enumeradores.StatusDocumento.Reprovado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusDocumento((short)Enumeradores.StatusDocumento.EmAnalise), Value = (short)Enumeradores.StatusDocumento.EmAnalise });
            return lista;
        }
        public static List<DictionaryEntry> ImovelTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ImovelTipo((short)Enumeradores.ImovelTipo.Apartamento), Value = (short)Enumeradores.ImovelTipo.Apartamento });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ImovelTipo((short)Enumeradores.ImovelTipo.Casa), Value = (short)Enumeradores.ImovelTipo.Casa });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ImovelTipo((short)Enumeradores.ImovelTipo.Terreno), Value = (short)Enumeradores.ImovelTipo.Terreno });

            return lista;
        }
        public static List<DictionaryEntry> GrauParentesco()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.GrauParentesco((short)Enumeradores.GrauParentesco.Amigo), Value = (short)Enumeradores.GrauParentesco.Amigo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.GrauParentesco((short)Enumeradores.GrauParentesco.Avo), Value = (short)Enumeradores.GrauParentesco.Avo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.GrauParentesco((short)Enumeradores.GrauParentesco.Irmao), Value = (short)Enumeradores.GrauParentesco.Irmao });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.GrauParentesco((short)Enumeradores.GrauParentesco.PaiMae), Value = (short)Enumeradores.GrauParentesco.PaiMae });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.GrauParentesco((short)Enumeradores.GrauParentesco.Tio), Value = (short)Enumeradores.GrauParentesco.Tio });

            return lista;
        }
        public static List<DictionaryEntry> ContaTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ContaTipo((short)Enumeradores.ContaTipo.Comum), Value = (short)Enumeradores.ContaTipo.Comum });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ContaTipo((short)Enumeradores.ContaTipo.Especial), Value = (short)Enumeradores.ContaTipo.Especial });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ContaTipo((short)Enumeradores.ContaTipo.Juridica), Value = (short)Enumeradores.ContaTipo.Juridica });

            return lista;
        }
        public static List<DictionaryEntry> InvestimentoTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.InvestimentoTipo((short)Enumeradores.InvestimentoTipo.Poupanca), Value = (short)Enumeradores.InvestimentoTipo.Poupanca });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.InvestimentoTipo((short)Enumeradores.InvestimentoTipo.Fundos), Value = (short)Enumeradores.InvestimentoTipo.Fundos });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.InvestimentoTipo((short)Enumeradores.InvestimentoTipo.PrevidenciaPrivada), Value = (short)Enumeradores.InvestimentoTipo.PrevidenciaPrivada });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.InvestimentoTipo((short)Enumeradores.InvestimentoTipo.AplicacoesFinanceira), Value = (short)Enumeradores.InvestimentoTipo.AplicacoesFinanceira });

            return lista;
        }
        public static List<DictionaryEntry> CompromissoFinanceiroTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CompromissoFinanceiroTipo((short)Enumeradores.CompromissoFinanceiroTipo.Consorcio), Value = (short)Enumeradores.CompromissoFinanceiroTipo.Consorcio });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CompromissoFinanceiroTipo((short)Enumeradores.CompromissoFinanceiroTipo.Crediario), Value = (short)Enumeradores.CompromissoFinanceiroTipo.Crediario });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CompromissoFinanceiroTipo((short)Enumeradores.CompromissoFinanceiroTipo.Emprestimo), Value = (short)Enumeradores.CompromissoFinanceiroTipo.Emprestimo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CompromissoFinanceiroTipo((short)Enumeradores.CompromissoFinanceiroTipo.Financiamento), Value = (short)Enumeradores.CompromissoFinanceiroTipo.Financiamento });

            return lista;
        }
        public static List<DictionaryEntry> PatrimonioTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.PatrimonioTipo((short)Enumeradores.PatrimonioTipo.Imovel), Value = (short)Enumeradores.PatrimonioTipo.Imovel });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.PatrimonioTipo((short)Enumeradores.PatrimonioTipo.Veiculo), Value = (short)Enumeradores.PatrimonioTipo.Veiculo });

            return lista;
        }
        public static List<DictionaryEntry> Escolaridade()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Escolaridade((short)Enumeradores.Escolaridade.Doutorado), Value = (short)Enumeradores.Escolaridade.Doutorado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Escolaridade((short)Enumeradores.Escolaridade.EnsinoFundamentalCompleto), Value = (short)Enumeradores.Escolaridade.EnsinoFundamentalCompleto });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Escolaridade((short)Enumeradores.Escolaridade.EnsinoFundamentalIncompleto), Value = (short)Enumeradores.Escolaridade.EnsinoFundamentalIncompleto });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Escolaridade((short)Enumeradores.Escolaridade.Especializacao), Value = (short)Enumeradores.Escolaridade.Especializacao });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Escolaridade((short)Enumeradores.Escolaridade.MedioCompleto), Value = (short)Enumeradores.Escolaridade.MedioCompleto });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Escolaridade((short)Enumeradores.Escolaridade.MedioIncompleto), Value = (short)Enumeradores.Escolaridade.MedioIncompleto });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Escolaridade((short)Enumeradores.Escolaridade.Mestrado), Value = (short)Enumeradores.Escolaridade.Mestrado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Escolaridade((short)Enumeradores.Escolaridade.NaoAlfabetizado), Value = (short)Enumeradores.Escolaridade.NaoAlfabetizado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Escolaridade((short)Enumeradores.Escolaridade.SuperiorCompleto), Value = (short)Enumeradores.Escolaridade.SuperiorCompleto });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Escolaridade((short)Enumeradores.Escolaridade.SuperiorIncompleto), Value = (short)Enumeradores.Escolaridade.SuperiorIncompleto });
            return lista;
        }
        public static List<DictionaryEntry> CategoriaProfissional()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CategoriaProfissional((short)Enumeradores.CategoriaProfissional.Aposentado), Value = (short)Enumeradores.CategoriaProfissional.Aposentado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CategoriaProfissional((short)Enumeradores.CategoriaProfissional.Assalariado), Value = (short)Enumeradores.CategoriaProfissional.Assalariado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CategoriaProfissional((short)Enumeradores.CategoriaProfissional.Comerciante), Value = (short)Enumeradores.CategoriaProfissional.Comerciante });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CategoriaProfissional((short)Enumeradores.CategoriaProfissional.DoLar), Value = (short)Enumeradores.CategoriaProfissional.DoLar });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CategoriaProfissional((short)Enumeradores.CategoriaProfissional.Empresario), Value = (short)Enumeradores.CategoriaProfissional.Empresario });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CategoriaProfissional((short)Enumeradores.CategoriaProfissional.FuncionarioPublico), Value = (short)Enumeradores.CategoriaProfissional.FuncionarioPublico });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CategoriaProfissional((short)Enumeradores.CategoriaProfissional.Industrial), Value = (short)Enumeradores.CategoriaProfissional.Industrial });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CategoriaProfissional((short)Enumeradores.CategoriaProfissional.ProdutorRural), Value = (short)Enumeradores.CategoriaProfissional.ProdutorRural });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CategoriaProfissional((short)Enumeradores.CategoriaProfissional.ProfissionalAutonomo), Value = (short)Enumeradores.CategoriaProfissional.ProfissionalAutonomo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CategoriaProfissional((short)Enumeradores.CategoriaProfissional.ProfissionalLiberal), Value = (short)Enumeradores.CategoriaProfissional.ProfissionalLiberal });
            return lista;
        }
        public static List<DictionaryEntry> AndamentoObraTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.AndamentoObraTipo((short)Enumeradores.AndamentoObraTipo.Cancelado), Value = (short)Enumeradores.AndamentoObraTipo.Cancelado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.AndamentoObraTipo((short)Enumeradores.AndamentoObraTipo.Construcao), Value = (short)Enumeradores.AndamentoObraTipo.Construcao });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.AndamentoObraTipo((short)Enumeradores.AndamentoObraTipo.EmObra), Value = (short)Enumeradores.AndamentoObraTipo.EmObra });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.AndamentoObraTipo((short)Enumeradores.AndamentoObraTipo.Lancamento), Value = (short)Enumeradores.AndamentoObraTipo.Lancamento });
            return lista;
        }
        public static List<DictionaryEntry> EtapaBoleto()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EtapaBoleto((short)Enumeradores.EtapaBoleto.AtendenteSolicitaProrrogacao), Value = (short)Enumeradores.EtapaBoleto.AtendenteSolicitaProrrogacao });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EtapaBoleto((short)Enumeradores.EtapaBoleto.AtendenteSolicitaComDesconto), Value = (short)Enumeradores.EtapaBoleto.AtendenteSolicitaComDesconto });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EtapaBoleto((short)Enumeradores.EtapaBoleto.SupervisorAprovado), Value = (short)Enumeradores.EtapaBoleto.SupervisorAprovado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EtapaBoleto((short)Enumeradores.EtapaBoleto.AnalistaReprova), Value = (short)Enumeradores.EtapaBoleto.AnalistaReprova });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EtapaBoleto((short)Enumeradores.EtapaBoleto.SupervisorReprova), Value = (short)Enumeradores.EtapaBoleto.SupervisorReprova });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EtapaBoleto((short)Enumeradores.EtapaBoleto.PrazoExpira), Value = (short)Enumeradores.EtapaBoleto.PrazoExpira });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EtapaBoleto((short)Enumeradores.EtapaBoleto.AtendenteSolicitaBoletoAvulso), Value = (short)Enumeradores.EtapaBoleto.AtendenteSolicitaBoletoAvulso });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EtapaBoleto((short)Enumeradores.EtapaBoleto.ReprovarObsoletos), Value = (short)Enumeradores.EtapaBoleto.ReprovarObsoletos });

            return lista;
        }
        public static List<DictionaryEntry> ImpactoEsforcoTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ImpactoEsforcoTipo((short)Enumeradores.ImpactoEsforcoTipo.Baixo), Value = (short)Enumeradores.ImpactoEsforcoTipo.Baixo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ImpactoEsforcoTipo((short)Enumeradores.ImpactoEsforcoTipo.Medio), Value = (short)Enumeradores.ImpactoEsforcoTipo.Medio });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ImpactoEsforcoTipo((short)Enumeradores.ImpactoEsforcoTipo.Alto), Value = (short)Enumeradores.ImpactoEsforcoTipo.Alto });
            return lista;
        }
        public static List<DictionaryEntry> TipoHistorico()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoHistorico((short)Enumeradores.TipoHistorico.Contrato), Value = (short)Enumeradores.TipoHistorico.Contrato });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoHistorico((short)Enumeradores.TipoHistorico.Parcela), Value = (short)Enumeradores.TipoHistorico.Parcela });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoHistorico((short)Enumeradores.TipoHistorico.Cliente), Value = (short)Enumeradores.TipoHistorico.Cliente });

            return lista;
        }

        public static List<DictionaryEntry> TipoPessoa()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoPessoa((short)Enumeradores.TipoPessoa.Fisica), Value = (short)Enumeradores.TipoPessoa.Fisica });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoPessoa((short)Enumeradores.TipoPessoa.Juridica), Value = (short)Enumeradores.TipoPessoa.Juridica });

            return lista;
        }
        public static List<DictionaryEntry> DocumentoComprobatorio()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoComprobatorio((short)Enumeradores.DocumentoComprobatorio.Alugueis), Value = (short)Enumeradores.DocumentoComprobatorio.Alugueis });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoComprobatorio((short)Enumeradores.DocumentoComprobatorio.ContratoPrestacaoServico), Value = (short)Enumeradores.DocumentoComprobatorio.ContratoPrestacaoServico });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoComprobatorio((short)Enumeradores.DocumentoComprobatorio.DeclaracaoCooperativa), Value = (short)Enumeradores.DocumentoComprobatorio.DeclaracaoCooperativa });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.DocumentoComprobatorio((short)Enumeradores.DocumentoComprobatorio.ExtratoBancario), Value = (short)Enumeradores.DocumentoComprobatorio.ExtratoBancario });

            return lista;
        }
        public static dynamic Sexo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Sexo((short)Enumeradores.Sexo.Masculino), Value = (short)Enumeradores.Sexo.Masculino });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Sexo((short)Enumeradores.Sexo.Feminino), Value = (short)Enumeradores.Sexo.Feminino });
            return lista;
        }
        public static dynamic StatusUnidade()
        {

            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusUnidade((short)Enumeradores.StatusUnidade.Bloqueada), Value = (short)Enumeradores.StatusUnidade.Bloqueada });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusUnidade((short)Enumeradores.StatusUnidade.Disponivel), Value = (short)Enumeradores.StatusUnidade.Disponivel });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusUnidade((short)Enumeradores.StatusUnidade.Indisponivel), Value = (short)Enumeradores.StatusUnidade.Indisponivel });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusUnidade((short)Enumeradores.StatusUnidade.Permuta), Value = (short)Enumeradores.StatusUnidade.Permuta });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusUnidade((short)Enumeradores.StatusUnidade.Vendida), Value = (short)Enumeradores.StatusUnidade.Vendida });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusUnidade((short)Enumeradores.StatusUnidade.Andamento), Value = (short)Enumeradores.StatusUnidade.Andamento });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusUnidade((short)Enumeradores.StatusUnidade.Reservada), Value = (short)Enumeradores.StatusUnidade.Reservada });
            return lista;
        }

        public static dynamic StatusPrposta()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            lista.Add(new DictionaryEntry() { Key = "Cadastramento", Value = 1 });
            lista.Add(new DictionaryEntry() { Key = "Em Análise", Value = 2 });
            lista.Add(new DictionaryEntry() { Key = "Revisão", Value = 3 });
            lista.Add(new DictionaryEntry() { Key = "Aprovado", Value = 4 });
            lista.Add(new DictionaryEntry() { Key = "Reprovado", Value = 5 });

            return lista;
        }
        public static dynamic StatusProposta()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            lista.Add(new DictionaryEntry() { Key = "Cadastrada", Value = 9 });
            lista.Add(new DictionaryEntry() { Key = "Solicitada Análise", Value = 6 });
            lista.Add(new DictionaryEntry() { Key = "Com Pendencia", Value = 3 });
            lista.Add(new DictionaryEntry() { Key = "Aprovada", Value = 1 });
            lista.Add(new DictionaryEntry() { Key = "Cancelada", Value = 2 });

            //lista.Add(new DictionaryEntry() { Key = "Negada", Value = 4 });
            //lista.Add(new DictionaryEntry() { Key = "Revisão", Value = 5 });
            //lista.Add(new DictionaryEntry() { Key = "Enviado Para Análise de Crédito", Value = 7 });
            //lista.Add(new DictionaryEntry() { Key = "Análise de Crédito Negada", Value = 8 });


            return lista;
        }

        public static dynamic StatusPropostaAnalista()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            //lista.Add(new DictionaryEntry() { Key = "Cadastrada", Value = 9 });
            lista.Add(new DictionaryEntry() { Key = "Análise Solicitada", Value = 6 });
            lista.Add(new DictionaryEntry() { Key = "Com Pendencia", Value = 3 });
            lista.Add(new DictionaryEntry() { Key = "Aprovada", Value = 1 });
            lista.Add(new DictionaryEntry() { Key = "Cancelada", Value = 2 });
            //lista.Add(new DictionaryEntry() { Key = "Negada", Value = 4 });
            //lista.Add(new DictionaryEntry() { Key = "Revisão", Value = 5 });
            //lista.Add(new DictionaryEntry() { Key = "Enviado Para Análise de Crédito", Value = 7 });
            //lista.Add(new DictionaryEntry() { Key = "Análise de Crédito Negada", Value = 8 });
            return lista;
        }
        public static dynamic StatusPropostaCorretor()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "Cadastrada", Value = 9 });
            lista.Add(new DictionaryEntry() { Key = "Com Pendencia", Value = 3 });
            lista.Add(new DictionaryEntry() { Key = "Aprovada", Value = 1 });
            lista.Add(new DictionaryEntry() { Key = "Cancelada", Value = 2 });
            //lista.Add(new DictionaryEntry() { Key = "Negada", Value = 4 });
            //lista.Add(new DictionaryEntry() { Key = "Revisão", Value = 5 });
            //lista.Add(new DictionaryEntry() { Key = "Solicitada Análise", Value = 6 });
            //lista.Add(new DictionaryEntry() { Key = "Enviado Para Análise de Crédito", Value = 7 });
            //lista.Add(new DictionaryEntry() { Key = "Análise de Crédito Negada", Value = 8 });
            return lista;
        }

        
        public static dynamic StatusPropostaSupervisor()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();

            lista.Add(new DictionaryEntry() { Key = "Aprovada", Value = 1 });
            lista.Add(new DictionaryEntry() { Key = "Cancelada", Value = 2 });
            lista.Add(new DictionaryEntry() { Key = "Com Pendencia", Value = 3 });
            //lista.Add(new DictionaryEntry() { Key = "Negada", Value = 4 });
            //lista.Add(new DictionaryEntry() { Key = "Revisão", Value = 5 });
            lista.Add(new DictionaryEntry() { Key = "Solicitada Análise", Value = 6 });
            //lista.Add(new DictionaryEntry() { Key = "Análise de Crédito Negada", Value = 8 });
            lista.Add(new DictionaryEntry() { Key = "Cadastrada", Value = 9 });
            return lista;
        }
        public static List<DictionaryEntry> ContatoTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ContatoTipo((short)Enumeradores.ContatoTipo.Email), Value = (short)Enumeradores.ContatoTipo.Email });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ContatoTipo((short)Enumeradores.ContatoTipo.TelefoneCelular), Value = (short)Enumeradores.ContatoTipo.TelefoneCelular });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ContatoTipo((short)Enumeradores.ContatoTipo.TelefoneComercial), Value = (short)Enumeradores.ContatoTipo.TelefoneComercial });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ContatoTipo((short)Enumeradores.ContatoTipo.TelefoneResidencial), Value = (short)Enumeradores.ContatoTipo.TelefoneResidencial });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ContatoTipo((short)Enumeradores.ContatoTipo.Fax), Value = (short)Enumeradores.ContatoTipo.Fax });
            return lista;
        }
        public static List<DictionaryEntry> EstadoCivil()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EstadoCivil((short)Enumeradores.EstadoCivil.Casado), Value = (short)Enumeradores.EstadoCivil.Casado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EstadoCivil((short)Enumeradores.EstadoCivil.CasadoComunhaoUniversalBens), Value = (short)Enumeradores.EstadoCivil.CasadoComunhaoUniversalBens });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EstadoCivil((short)Enumeradores.EstadoCivil.CasadoParticipacaoFinalAquetos), Value = (short)Enumeradores.EstadoCivil.CasadoParticipacaoFinalAquetos });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EstadoCivil((short)Enumeradores.EstadoCivil.CasadoSeparacaoTotalBens), Value = (short)Enumeradores.EstadoCivil.CasadoSeparacaoTotalBens });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EstadoCivil((short)Enumeradores.EstadoCivil.Divorciado), Value = (short)Enumeradores.EstadoCivil.Divorciado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EstadoCivil((short)Enumeradores.EstadoCivil.Solteiro), Value = (short)Enumeradores.EstadoCivil.Solteiro });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EstadoCivil((short)Enumeradores.EstadoCivil.Viuvo), Value = (short)Enumeradores.EstadoCivil.Viuvo });
            return lista;
        }
        public static List<DictionaryEntry> StatusParcela()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusParcela((short)Enumeradores.StatusParcela.Contrato), Value = (short)Enumeradores.StatusParcela.Contrato });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.StatusParcela((short)Enumeradores.StatusParcela.EmNegociacao), Value = (short)Enumeradores.StatusParcela.EmNegociacao });
            return lista;
        }
        public static List<DictionaryEntry> EnderecoTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Alameda), Value = (short)Enumeradores.EnderecoTipo.Alameda });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Avenida), Value = (short)Enumeradores.EnderecoTipo.Avenida });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Beco), Value = (short)Enumeradores.EnderecoTipo.Beco });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Campo), Value = (short)Enumeradores.EnderecoTipo.Campo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Estrada), Value = (short)Enumeradores.EnderecoTipo.Estrada });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Largo), Value = (short)Enumeradores.EnderecoTipo.Largo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Praça), Value = (short)Enumeradores.EnderecoTipo.Praça });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Praia), Value = (short)Enumeradores.EnderecoTipo.Praia });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Quadra), Value = (short)Enumeradores.EnderecoTipo.Quadra });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Rodovia), Value = (short)Enumeradores.EnderecoTipo.Rodovia });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Rua), Value = (short)Enumeradores.EnderecoTipo.Rua });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.EnderecoTipo((short)Enumeradores.EnderecoTipo.Travessa), Value = (short)Enumeradores.EnderecoTipo.Travessa });
            return lista;
        }
        public static dynamic ListaCondicaoTipos()
        {
            //aging e etc
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.Aging), Value = (short)Enumeradores.CondicaoTipo.Aging });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.DiasAtraso), Value = (short)Enumeradores.CondicaoTipo.DiasAtraso });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.Empreendimento), Value = (short)Enumeradores.CondicaoTipo.Empreendimento });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.StatusFaseSintese), Value = (short)Enumeradores.CondicaoTipo.StatusFaseSintese });
            //lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.StatusSintese), Value = (short)Enumeradores.CondicaoTipo.StatusSintese });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.TipoBloqueio), Value = (short)Enumeradores.CondicaoTipo.TipoBloqueio });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.TipoContrato), Value = (short)Enumeradores.CondicaoTipo.TipoContrato });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.ValorContrato), Value = (short)Enumeradores.CondicaoTipo.ValorContrato });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.ValorParcela), Value = (short)Enumeradores.CondicaoTipo.ValorParcela });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.ValorSaldo), Value = (short)Enumeradores.CondicaoTipo.ValorSaldo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.AndamentoObra), Value = (short)Enumeradores.CondicaoTipo.AndamentoObra });

            return lista;
        }
        public static dynamic ListaCondicaoTiposAcao()
        {
            //aging e etc
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.DiasAtraso), Value = (short)Enumeradores.CondicaoTipo.DiasAtraso });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.DataEntregaChaves), Value = (short)Enumeradores.CondicaoTipo.DataEntregaChaves });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CondicaoTipo((short)Enumeradores.CondicaoTipo.DataEntregaEmpreendimento), Value = (short)Enumeradores.CondicaoTipo.DataEntregaEmpreendimento });
            return lista;
        }
        public static dynamic ListaCondicaoOperadorLogico()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "E", Value = (short)Enumeradores.CondicaoTipoOperadorLogico.E });
            lista.Add(new DictionaryEntry() { Key = "OU", Value = (short)Enumeradores.CondicaoTipoOperadorLogico.OU });
            return lista;
        }
        public static dynamic ListaCondicao()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Condicao((short)Enumeradores.Condicao.Igual), Value = (short)Enumeradores.Condicao.Igual });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Condicao((short)Enumeradores.Condicao.Diferente), Value = (short)Enumeradores.Condicao.Diferente });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Condicao((short)Enumeradores.Condicao.Maior), Value = (short)Enumeradores.Condicao.Maior });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.Condicao((short)Enumeradores.Condicao.Menor), Value = (short)Enumeradores.Condicao.Menor });
            return lista;
        }
        public static List<DictionaryEntry> TipoAcaoCobranca()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.CARTA), Value = (short)Enumeradores.TipoAcaoCobranca.CARTA });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.CONTATO), Value = (short)Enumeradores.TipoAcaoCobranca.CONTATO });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.EMAIL), Value = (short)Enumeradores.TipoAcaoCobranca.EMAIL });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.NEGATIVACAO_SERASA), Value = (short)Enumeradores.TipoAcaoCobranca.NEGATIVACAO_SERASA });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.SMS), Value = (short)Enumeradores.TipoAcaoCobranca.SMS });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.URA), Value = (short)Enumeradores.TipoAcaoCobranca.URA });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.ARQUIVO_REMESSA), Value = (short)Enumeradores.TipoAcaoCobranca.ARQUIVO_REMESSA });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.EMAIL_SEGUNDA_VIA_BOLETO), Value = (short)Enumeradores.TipoAcaoCobranca.EMAIL_SEGUNDA_VIA_BOLETO });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.EMAIL_SENHA_USUARIO_NOVO), Value = (short)Enumeradores.TipoAcaoCobranca.EMAIL_SENHA_USUARIO_NOVO });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.EMAIL_SENHA_USUARIO_REENVIAR), Value = (short)Enumeradores.TipoAcaoCobranca.EMAIL_SENHA_USUARIO_REENVIAR });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.TipoAcaoCobranca((short)Enumeradores.TipoAcaoCobranca.EMAIL_BOLETO_SOLICITADO), Value = (short)Enumeradores.TipoAcaoCobranca.EMAIL_BOLETO_SOLICITADO });

            return lista;
        }
        public static dynamic ListaAcaoTipo()
        {
            //aging e etc
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "Cliente", Value = (short)Enumeradores.AcaoTipo.Cliente });
            lista.Add(new DictionaryEntry() { Key = "Avalista", Value = (short)Enumeradores.AcaoTipo.Avalista });
            lista.Add(new DictionaryEntry() { Key = "Cliente e Avalista", Value = (short)Enumeradores.AcaoTipo.Todos });
            return lista;
        }
        public static dynamic HistoricoTipo(bool exibeCliente = false)
        {
            //aging e etc
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "--", Value = 0 });
            if (exibeCliente)
                lista.Add(new DictionaryEntry() { Key = "Cliente", Value = (short)Enumeradores.HistoricoTipo.Cliente });

            lista.Add(new DictionaryEntry() { Key = "Contrato", Value = (short)Enumeradores.HistoricoTipo.Contrato });
            lista.Add(new DictionaryEntry() { Key = "Parcela", Value = (short)Enumeradores.HistoricoTipo.Parcela });
            return lista;
        }
        public static List<DictionaryEntry> SituacaoHistorico()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.SituacaoHistorico((short)Enumeradores.SituacaoHistorico.Enviado), Value = (short)Enumeradores.SituacaoHistorico.Enviado });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.SituacaoHistorico((short)Enumeradores.SituacaoHistorico.DadosInvalidos), Value = (short)Enumeradores.SituacaoHistorico.DadosInvalidos });
            return lista;
        }
        public static List<DictionaryEntry> SituacaoHistoricoAcaoCobranca()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "Prontos a Enviar", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.SituacaoHistorico((short)Enumeradores.SituacaoHistorico.DadosInvalidos), Value = (short)Enumeradores.SituacaoHistorico.DadosInvalidos });
            return lista;
        }
        public static List<DictionaryEntry> ReferenciaParentescoTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ReferenciaParentescoTipo((short)Enumeradores.ReferenciaParentescoTipo.Amigo), Value = (short)Enumeradores.ReferenciaParentescoTipo.Amigo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ReferenciaParentescoTipo((short)Enumeradores.ReferenciaParentescoTipo.Avo), Value = (short)Enumeradores.ReferenciaParentescoTipo.Avo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ReferenciaParentescoTipo((short)Enumeradores.ReferenciaParentescoTipo.Irmao), Value = (short)Enumeradores.ReferenciaParentescoTipo.Irmao });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ReferenciaParentescoTipo((short)Enumeradores.ReferenciaParentescoTipo.PaiMae), Value = (short)Enumeradores.ReferenciaParentescoTipo.PaiMae });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.ReferenciaParentescoTipo((short)Enumeradores.ReferenciaParentescoTipo.Tio), Value = (short)Enumeradores.ReferenciaParentescoTipo.Tio });
            return lista;
        }
        public static List<DictionaryEntry> FinanceiraContaTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.FinanceiroContaTipo((short)Enumeradores.InformacaoFinanceiroTipo.Comum), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Comum });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.FinanceiroContaTipo((short)Enumeradores.InformacaoFinanceiroTipo.Especial), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Especial });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.FinanceiroContaTipo((short)Enumeradores.InformacaoFinanceiroTipo.Juridica), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Juridica });
            return lista;
        }
        public static List<DictionaryEntry> InvestimentoContaTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.InvestimentoContaTipo((short)Enumeradores.InformacaoFinanceiroTipo.Aplicacao), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Aplicacao });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.InvestimentoContaTipo((short)Enumeradores.InformacaoFinanceiroTipo.Fundo), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Fundo });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.InvestimentoContaTipo((short)Enumeradores.InformacaoFinanceiroTipo.Poupanca), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Poupanca });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.InvestimentoContaTipo((short)Enumeradores.InformacaoFinanceiroTipo.Previdencia), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Previdencia });
            return lista;
        }
        public static List<DictionaryEntry> CartaoBandeiraTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CartaoBandeiraTipo((short)Enumeradores.InformacaoFinanceiroTipo.MasterCard), Value = (short)Enumeradores.InformacaoFinanceiroTipo.MasterCard });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CartaoBandeiraTipo((short)Enumeradores.InformacaoFinanceiroTipo.Visa), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Visa });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CartaoBandeiraTipo((short)Enumeradores.InformacaoFinanceiroTipo.Diners), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Diners });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CartaoBandeiraTipo((short)Enumeradores.InformacaoFinanceiroTipo.Amex), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Amex });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CartaoBandeiraTipo((short)Enumeradores.InformacaoFinanceiroTipo.Outro), Value = (short)Enumeradores.InformacaoFinanceiroTipo.Outro });
            return lista;
        }
        public static List<DictionaryEntry> CheckListTipo()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "", Value = 0 });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CheckListTipo((short)Enumeradores.CheckListTipo.Empreendimento), Value = (short)Enumeradores.CheckListTipo.Empreendimento });
            lista.Add(new DictionaryEntry() { Key = EnumeradoresDescricao.CheckListTipo((short)Enumeradores.CheckListTipo.Proposta), Value = (short)Enumeradores.CheckListTipo.Proposta });
            return lista;
        }
        public static List<DictionaryEntry> PreecherDropdownEmBranco()
        {
            List<DictionaryEntry> lista = new List<DictionaryEntry>();
            lista.Add(new DictionaryEntry() { Key = "--", Value = 0 });
            return lista;
        }


    }
}

