using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoCarteira : BaseEntity
    {
        public TabelaTipoCarteira()
        {
            this.Contratoes = new List<Contrato>();
            this.TabelaNaturezas = new List<TabelaNatureza>();
        }

       //  public int Cod { get; set; }
         public string TipoCarteira { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual ICollection<TabelaNatureza> TabelaNaturezas { get; set; }
    }
}
