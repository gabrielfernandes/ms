using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaAnaliseDocumental : BaseEntity
    {
        public TabelaAnaliseDocumental()
        {
            this.PropostaAnaliseDocumentals = new List<PropostaAnaliseDocumental>();
            this.TabelaAnaliseDocumentalAlternativas = new List<TabelaAnaliseDocumentalAlternativa>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodTipoDocumento { get; set; }
         public Nullable<short> Campo { get; set; }
         public string Pergunta { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<short> TipoPergunta { get; set; }
        public virtual ICollection<PropostaAnaliseDocumental> PropostaAnaliseDocumentals { get; set; }
        public virtual TabelaTipoDocumento TabelaTipoDocumento { get; set; }
        public virtual ICollection<TabelaAnaliseDocumentalAlternativa> TabelaAnaliseDocumentalAlternativas { get; set; }
    }
}
