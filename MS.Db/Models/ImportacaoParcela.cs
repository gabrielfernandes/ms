using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ImportacaoParcela : BaseEntity
    {
        //  public int Cod { get; set; }
        public Nullable<int> CodImportacao { get; set; }
        public string Companhia { get; set; }
        public string Filial { get; set; }
        public string FilialDescricao { get; set; }
        public string RotaArea { get; set; }
        public string Status { get; set; }
        public string TipoContrato { get; set; }
        public string NumeroCliente { get; set; }
        public string CPFCNPJ { get; set; }
        public string Descricao { get; set; }
        public string NumeroFatura { get; set; }
        public string ValorFatura { get; set; }
        public string ValorAberto { get; set; }
        public string DataVencimento { get; set; }
        public string NumeroParcela { get; set; }
        public string Governo { get; set; }
        public string Corporativo { get; set; }
        public string Mensagem { get; set; }
        public Nullable<System.DateTime> DataProcessamento { get; set; }
        //  public Nullable<System.DateTime> DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public string NumeroContrato { get; set; }
        public Nullable<System.DateTime> DataFechamento { get; set; }
        public Nullable<int> CodParcela { get; set; }
        public string CodSinteseCliente { get; set; }
        public string NotaFiscal { get; set; }
        public string TipoDocumento { get; set; }
        public string InstrumentoPgto { get; set; }
        public Nullable<System.DateTime> DataPagamento { get; set; }
        public string StatusContrato { get; set; }
        public Nullable<System.DateTime> DataEmissao { get; set; }
        public virtual Importacao Importacao { get; set; }
    }
}
