using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ProspectFGT : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodProspect { get; set; }
         public Nullable<decimal> ValorFGTS { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual PropostaProspect PropostaProspect { get; set; }
    }
}
