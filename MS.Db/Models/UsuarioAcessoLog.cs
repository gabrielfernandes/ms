using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class UsuarioAcessoLog : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public Nullable<System.DateTime> DataAcesso { get; set; }
         public string IP { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
    }
}
