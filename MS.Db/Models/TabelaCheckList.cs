using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaCheckList : BaseEntity
    {
        public TabelaCheckList()
        {
            this.PropostaDocumentoChecklists = new List<PropostaDocumentoChecklist>();
            this.TabelaEmpreendimentoDocumentoChecklists = new List<TabelaEmpreendimentoDocumentoChecklist>();
        }

       //  public int Cod { get; set; }
         public int CodCheckListNome { get; set; }
         public Nullable<int> CodTipoDocumento { get; set; }
         public Nullable<int> CodBanco { get; set; }
         public string Documento { get; set; }
         public Nullable<short> EstadoCivil { get; set; }
         public Nullable<short> CategoriaProfissional { get; set; }
         public Nullable<bool> Fgts { get; set; }
         public Nullable<bool> CartaoCredito { get; set; }
         public Nullable<bool> AplicacaoFinanceira { get; set; }
         public Nullable<bool> Aluguel { get; set; }
         public Nullable<bool> Obrigatorio { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<PropostaDocumentoChecklist> PropostaDocumentoChecklists { get; set; }
        public virtual TabelaBanco TabelaBanco { get; set; }
        public virtual TabelaCheckListNome TabelaCheckListNome { get; set; }
        public virtual TabelaTipoDocumento TabelaTipoDocumento { get; set; }
        public virtual ICollection<TabelaEmpreendimentoDocumentoChecklist> TabelaEmpreendimentoDocumentoChecklists { get; set; }
    }
}
