using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ContratoDocumento : BaseEntity
    {
        //  public int Cod { get; set; }
        public Nullable<int> CodTipoDocumento { get; set; }
        public Nullable<int> CodCliente { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public string ArquivoCaminho { get; set; }
        public string ArquivoNome { get; set; }
        public string ArquivoGuid { get; set; }
        public string ArquivoObservacao { get; set; }

        public Nullable<int> CodUsuario { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual TabelaTipoDocumento TabelaTipoDocumento { get; set; }
    }
}
