using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ContratoAvalista : BaseEntity
    {
        public ContratoAvalista()
        {
            this.ContratoAvalista1 = new List<ContratoAvalista>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodContrato { get; set; }
         public string NomeAvalista { get; set; }
         public string CPF { get; set; }
         public Nullable<short> Tipo { get; set; }
         public Nullable<System.DateTime> DataNascimento { get; set; }
         public Nullable<short> EstadoCivil { get; set; }
         public string Email { get; set; }
         public string Profissao { get; set; }
         public string NumeroDocumento { get; set; }
         public string TelefoneResidencial { get; set; }
         public string TelefoneCelular { get; set; }
         public string Cep { get; set; }
         public Nullable<short> EnderecoTipo { get; set; }
         public string Endereco { get; set; }
         public string EnderecoNumero { get; set; }
         public string EnderecoComplemento { get; set; }
         public string Bairro { get; set; }
         public string Cidade { get; set; }
         public string UF { get; set; }
         public Nullable<decimal> Renda { get; set; }
         public Nullable<bool> Negativado { get; set; }
         public string Obs { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public string Nacionalidade { get; set; }
         public string Naturalidade { get; set; }
         public string TelefoneComercial { get; set; }
         public Nullable<short> Sexo { get; set; }
         public Nullable<int> CodConjuge { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual ICollection<ContratoAvalista> ContratoAvalista1 { get; set; }
        public virtual ContratoAvalista ContratoAvalista2 { get; set; }
    }
}
