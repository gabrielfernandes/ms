using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaEmpresa : BaseEntity
    {
        public TabelaEmpresa()
        {
            this.Contratoes = new List<Contrato>();
        }

       //  public int Cod { get; set; }
         public string CodEmpresaCliente { get; set; }
         public Nullable<int> CodConstrutora { get; set; }
         public Nullable<int> CodGestao { get; set; }
         public string Empresa { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual TabelaConstrutora TabelaConstrutora { get; set; }
        public virtual TabelaGestao TabelaGestao { get; set; }
    }
}
