using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ContratoParcelaAcordo : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodContratoParcela { get; set; }
         public string FluxoPagamento { get; set; }
         public Nullable<System.DateTime> DataVencimento { get; set; }
         public Nullable<System.DateTime> DataPagamento { get; set; }
         public Nullable<decimal> ValorParcela { get; set; }
         public Nullable<decimal> ValorRecebido { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodContrato { get; set; }
         public Nullable<decimal> Juros { get; set; }
         public Nullable<decimal> Multa { get; set; }
         public Nullable<decimal> Honorario { get; set; }
         public Nullable<decimal> Desconto { get; set; }
         public Nullable<decimal> Total { get; set; }
         public string Variavel { get; set; }
         public Nullable<int> CodContratoAcordo { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual ContratoAcordo ContratoAcordo { get; set; }
        public virtual ContratoParcela ContratoParcela { get; set; }
    }
}
