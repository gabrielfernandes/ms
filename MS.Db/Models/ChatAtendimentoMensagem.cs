using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ChatAtendimentoMensagem : BaseEntity
    {
       //  public int Cod { get; set; }
         public string Nome { get; set; }
         public string Descricao { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
         public Nullable<System.DateTime> DataRecebida { get; set; }
         public Nullable<System.DateTime> DataLida { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public Nullable<int> CodigoAtentimento { get; set; }
         public Nullable<short> Tipo { get; set; }
         public string Arquivo { get; set; }
         public string ArquivoNome { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ChatAtendimento ChatAtendimento { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
