using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cobranca.Db.Models 
{
    public partial class Proposta : BaseEntity
    {
        public Proposta()
        {
            this.Contratoes = new List<Contrato>();
            this.PropostaDocumentoes = new List<PropostaDocumento>();
            this.PropostaHistoricoes = new List<PropostaHistorico>();
            this.PropostaProspects = new List<PropostaProspect>();
            this.PropostaStatusHistoricoes = new List<PropostaStatusHistorico>();
            this.PropostaValores = new List<PropostaValore>();
            this.PropostaValoresPadraos = new List<PropostaValoresPadrao>();
        }
        
        [NotMapped]
        public const int _SinteseInicialProcesso = 7;

        //  public int Cod { get; set; }
        public Nullable<int> CodUnidade { get; set; }
         public Nullable<decimal> ValorRendaFamiliar { get; set; }
         public Nullable<int> CodTabelaVenda { get; set; }
         public Nullable<decimal> ValorFGTS { get; set; }
         public Nullable<int> CodCorretor { get; set; }
         public Nullable<int> CodGerente { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<System.DateTime> DataProposta { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public Nullable<decimal> ValorUnidade { get; set; }
         public Nullable<int> CodEmpresaVenda { get; set; }
         public Nullable<short> Status { get; set; }
         public Nullable<decimal> ValorMaximoFinanciamento { get; set; }
         public Nullable<decimal> ValorMaximoRepasse { get; set; }
         public Nullable<decimal> ValorFinanciamentoTotal { get; set; }
         public Nullable<int> PrazoFinanciamento { get; set; }
         public Nullable<decimal> ValorFalta { get; set; }
         public Nullable<decimal> ValorTotalGeral { get; set; }
         public Nullable<decimal> ValorCalculadoFinanciamento { get; set; }
         public Nullable<System.DateTime> DataInicioRepasse { get; set; }
         public Nullable<decimal> ValorTotalRepasse { get; set; }
         public Nullable<decimal> ValorTotalSubsidio { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual TabelaEmpresaVenda TabelaEmpresaVenda { get; set; }
        public virtual TabelaUnidade TabelaUnidade { get; set; }
        public virtual TabelaVenda TabelaVenda { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Usuario Usuario1 { get; set; }
        public virtual Usuario Usuario2 { get; set; }
        public virtual ICollection<PropostaDocumento> PropostaDocumentoes { get; set; }
        public virtual ICollection<PropostaHistorico> PropostaHistoricoes { get; set; }
        public virtual ICollection<PropostaProspect> PropostaProspects { get; set; }
        public virtual ICollection<PropostaStatusHistorico> PropostaStatusHistoricoes { get; set; }
        public virtual ICollection<PropostaValore> PropostaValores { get; set; }
        public virtual ICollection<PropostaValoresPadrao> PropostaValoresPadraos { get; set; }
    }
}
