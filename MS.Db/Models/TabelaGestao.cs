using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaGestao : BaseEntity
    {
        public TabelaGestao()
        {
            this.TabelaEmpresas = new List<TabelaEmpresa>();
        }

       //  public int Cod { get; set; }
         public string NomeGestao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<TabelaEmpresa> TabelaEmpresas { get; set; }
    }
}
