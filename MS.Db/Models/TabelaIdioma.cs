using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaIdioma
    {
         public string Cod { get; set; }
         public string ptBR { get; set; }
         public string enUS { get; set; }
         public string esES { get; set; }
    }
}
