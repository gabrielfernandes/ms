using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class PropostaHistorico : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodProposta { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public Nullable<int> CodSintese { get; set; }
         public string Observacoes { get; set; }
         public Nullable<System.DateTime> DataAgenda { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual Proposta Proposta { get; set; }
        public virtual TabelaSintese TabelaSintese { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
