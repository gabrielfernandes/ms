using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ClienteEndereco : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<int> CodEndereco { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<short> Tipo { get; set; }
         public Nullable<bool> Principal { get; set; }
         public Nullable<bool> EnderecoHOT { get; set; }
         public Nullable<int> EnderecoHOTCodUsuario { get; set; }
         public Nullable<System.DateTime> EnderecoHOTData { get; set; }
         public Nullable<int> CodContrato { get; set; }
         public Nullable<short> TipoEndereco { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual TabelaEndereco TabelaEndereco { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
