using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaRegionalProduto : BaseEntity
    {
        public TabelaRegionalProduto()
        {
            this.Contratoes = new List<Contrato>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodRegional { get; set; }
         public Nullable<int> CodProduto { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual TabelaProduto TabelaProduto { get; set; }
        public virtual TabelaRegional TabelaRegional { get; set; }
    }
}
