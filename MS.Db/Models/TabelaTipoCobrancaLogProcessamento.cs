using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoCobrancaLogProcessamento : BaseEntity
    {
        public TabelaTipoCobrancaLogProcessamento()
        {
            this.Contratoes = new List<Contrato>();
        }

       //  public int Cod { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public string Guid { get; set; }
         public string DescricaoOrdem { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
