using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaModeloAcao : BaseEntity
    {
        public TabelaModeloAcao()
        {
            this.TabelaCampanhaEmails = new List<TabelaCampanhaEmail>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodAcaoCobranca { get; set; }
         public string Modelo { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string Titulo { get; set; }
        public virtual TabelaAcaoCobranca TabelaAcaoCobranca { get; set; }
        public virtual ICollection<TabelaCampanhaEmail> TabelaCampanhaEmails { get; set; }
    }
}
