using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class Importacao : BaseEntity
    {
        public Importacao()
        {
            this.ImportacaoBoletoes = new List<ImportacaoBoleto>();
            this.ImportacaoClientes = new List<ImportacaoCliente>();
            this.ImportacaoClienteEnderecoes = new List<ImportacaoClienteEndereco>();
            this.ImportacaoCreditoes = new List<ImportacaoCredito>();
            this.ImportacaoParcelas = new List<ImportacaoParcela>();
        }

       //  public int Cod { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public Nullable<int> TotalRegistros { get; set; }
         public Nullable<int> TotalRegistrosOK { get; set; }
         public Nullable<int> TotalRegistrosErro { get; set; }
         public Nullable<short> Status { get; set; }
         public Nullable<System.DateTime> DataProcessamentoAgendado { get; set; }
         public Nullable<System.DateTime> DataProcessado { get; set; }
         public Nullable<int> TotalRegistrosInconsistentes { get; set; }
         public Nullable<short> Tipo { get; set; }
         public string NomeArquivo { get; set; }
         public string GUID { get; set; }
         public Nullable<int> CodImportacaoGeral { get; set; }
         public string Mensagem { get; set; }
         public Nullable<int> TotalRegistrosProcessado { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<ImportacaoBoleto> ImportacaoBoletoes { get; set; }
        public virtual ICollection<ImportacaoCliente> ImportacaoClientes { get; set; }
        public virtual ICollection<ImportacaoClienteEndereco> ImportacaoClienteEnderecoes { get; set; }
        public virtual ICollection<ImportacaoCredito> ImportacaoCreditoes { get; set; }
        public virtual ICollection<ImportacaoParcela> ImportacaoParcelas { get; set; }
    }
}
