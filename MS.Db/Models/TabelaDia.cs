using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaDia : BaseEntity
    {
       //  public int Cod { get; set; }
         public string NomePrazo { get; set; }
         public Nullable<int> DiasDe { get; set; }
         public Nullable<int> DiasAte { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
    }
}
