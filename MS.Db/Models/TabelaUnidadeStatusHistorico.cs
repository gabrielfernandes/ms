using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaUnidadeStatusHistorico : BaseEntity
    {
        public TabelaUnidadeStatusHistorico()
        {
            this.TabelaUnidadeReservas = new List<TabelaUnidadeReserva>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodUnidade { get; set; }
         public Nullable<short> Status { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public string Observacao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaUnidade TabelaUnidade { get; set; }
        public virtual ICollection<TabelaUnidadeReserva> TabelaUnidadeReservas { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
