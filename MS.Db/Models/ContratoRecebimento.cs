using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ContratoRecebimento : BaseEntity
    {
         public int CodContrato { get; set; }
         public string FluxoPagamento { get; set; }
         public System.DateTime DtVencimento { get; set; }
         public Nullable<System.DateTime> DtRecebimento { get; set; }
         public string Natureza { get; set; }
         public Nullable<decimal> ValorParcela { get; set; }
         public Nullable<decimal> ValorRecebimento { get; set; }
         public Nullable<System.DateTime> DataImportacao { get; set; }
         public string LoginImportacao { get; set; }
    }
}
