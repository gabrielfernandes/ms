using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class Historico : BaseEntity
    {
        public Historico()
        {
            this.Contratoes = new List<Contrato>();
            this.ContratoParcelas = new List<ContratoParcela>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<int> CodContrato { get; set; }
         public Nullable<int> CodParcela { get; set; }
         public Nullable<int> CodSintese { get; set; }
         public Nullable<int> CodUsuario { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
         public string Observacao { get; set; }
         public Nullable<int> CodMotivoAtraso { get; set; }
         public Nullable<int> CodResolucao { get; set; }
         public Nullable<System.DateTime> DataAgenda { get; set; }
         public Nullable<int> CodImportacao { get; set; }
        public Nullable<int> CodAcaoCobranca { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual ICollection<Contrato> Contratoes { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual ICollection<ContratoParcela> ContratoParcelas { get; set; }
        public virtual ContratoParcela ContratoParcela { get; set; }
        public virtual ContratoAcaoCobrancaHistorico ContratoAcaoCobrancaHistorico { get; set; }
        public virtual TabelaSintese TabelaSintese { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
