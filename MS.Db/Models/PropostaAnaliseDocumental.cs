using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class PropostaAnaliseDocumental : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodAnaliseDocumental { get; set; }
         public Nullable<int> CodPropostaDocumento { get; set; }
         public Nullable<decimal> Valor { get; set; }
         public Nullable<short> Status { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string Observacao { get; set; }
         public Nullable<bool> Confere { get; set; }
        public virtual PropostaDocumento PropostaDocumento { get; set; }
        public virtual TabelaAnaliseDocumental TabelaAnaliseDocumental { get; set; }
    }
}
