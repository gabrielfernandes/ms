using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ProspectInformacaoBancaria : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodProspect { get; set; }
         public Nullable<short> Tipo { get; set; }
         public Nullable<int> CodBanco { get; set; }
         public Nullable<decimal> ValorLimite { get; set; }
         public Nullable<short> Bandeira { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<short> TipoInfoBancaria { get; set; }
        public virtual PropostaProspect PropostaProspect { get; set; }
        public virtual TabelaBanco TabelaBanco { get; set; }
    }
}
