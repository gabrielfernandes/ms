using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ImportacaoLayout : BaseEntity
    {
       //  public int Cod { get; set; }
         public string CodEmpreendimentoCliente { get; set; }
         public string CodBlocoCliente { get; set; }
         public string NumeroUnidade { get; set; }
         public string NumeroContrato { get; set; }
         public string CodCliente { get; set; }
         public string NomeCliente { get; set; }
         public string CPF { get; set; }
         public Nullable<decimal> ValorVenda { get; set; }
         public string FluxoPagamento { get; set; }
         public Nullable<decimal> ValorParcela { get; set; }
         public Nullable<decimal> ValorAberto { get; set; }
         public Nullable<System.DateTime> DataVencimento { get; set; }
         public Nullable<System.DateTime> DataPagamento { get; set; }
         public Nullable<decimal> ValorPagamento { get; set; }
    }
}
