using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoCadastro : BaseEntity
    {
        public TabelaTipoCadastro()
        {
            this.ClienteContatoes = new List<ClienteContato>();
        }

       //  public int Cod { get; set; }
         public string Descricao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<ClienteContato> ClienteContatoes { get; set; }
    }
}
