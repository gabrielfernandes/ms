using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class TabelaBancoPortadorLogArquivo : BaseEntity
    {
        //  public int Cod { get; set; }
        public Nullable<int> CodBancoPortador { get; set; }
        public string NomeDocumento { get; set; }
        public string NumeroLote { get; set; }
        public Nullable<short> TipoArquivo { get; set; }
        public string Banco { get; set; }
        public string Companhia { get; set; }
        public byte[] Arquivo { get; set; }
        public Nullable<int> BoletoTotalArquivo { get; set; }
        public Nullable<DateTime> DataProcessamento { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public string Log { get; set; }
        //public System.DateTime DataCadastro { get; set; }
        //public Nullable<System.DateTime> DataAlteracao { get; set; }
        //public Nullable<System.DateTime> DataExcluido { get; set; }
        public virtual TabelaBancoPortador TabelaBancoPortador { get; set; }
    }
}
