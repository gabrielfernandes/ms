using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class PropostaValoresPadrao : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodProposta { get; set; }
         public Nullable<int> CodTipoParcela { get; set; }
         public Nullable<decimal> ValorParcela { get; set; }
         public Nullable<int> Quantidade { get; set; }
         public Nullable<decimal> ValorTotal { get; set; }
         public Nullable<System.DateTime> DataInicio { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual Proposta Proposta { get; set; }
    }
}
