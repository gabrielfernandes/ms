using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ContratoEscritorioHistorico : BaseEntity
    {
         public Nullable<int> CodContrato { get; set; }
         public Nullable<int> CodEscritorio { get; set; }
         public Nullable<int> CodTipoCobranca { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
       //  public int Cod { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual TabelaEscritorio TabelaEscritorio { get; set; }
        public virtual TabelaTipoCobranca TabelaTipoCobranca { get; set; }
    }
}
