using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaCheckListNome : BaseEntity
    {
        public TabelaCheckListNome()
        {
            this.TabelaCheckLists = new List<TabelaCheckList>();
            this.TabelaEmpreendimentoes = new List<TabelaEmpreendimento>();
            this.TabelaEmpreendimentoes1 = new List<TabelaEmpreendimento>();
        }

       //  public int Cod { get; set; }
         public string CheckListNome { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<bool> CheckListProspect { get; set; }
         public Nullable<short> TipoCheckList { get; set; }
        public virtual ICollection<TabelaCheckList> TabelaCheckLists { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes1 { get; set; }
    }
}
