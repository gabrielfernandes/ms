using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ContratoParcelaBoletoHistorico : BaseEntity
    {
        public ContratoParcelaBoletoHistorico()
        {
            this.ContratoParcelaBoletoes = new List<ContratoParcelaBoleto>();

        }

        //  public int Cod { get; set; }
        public Nullable<int> CodSintese { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        public string Observacao { get; set; }
        public Nullable<int> CodMotivoAtraso { get; set; }
        public Nullable<int> CodResolucao { get; set; }
        public Nullable<System.DateTime> DataAgenda { get; set; }
        public Nullable<int> CodImportacao { get; set; }
        public Nullable<int> CodBoleto { get; set; }
        public virtual ContratoParcelaBoleto ContratoParcelaBoleto { get; set; }
        public virtual TabelaSintese TabelaSintese { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<ContratoParcelaBoleto> ContratoParcelaBoletoes { get; set; }
    }
}
