using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class GrupoClienteCNPJ : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodGrupoCliente { get; set; }
         public string CNPJ { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual GrupoCliente GrupoCliente { get; set; }
    }
}
