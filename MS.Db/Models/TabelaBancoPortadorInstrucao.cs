using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class TabelaBancoPortadorInstrucao : BaseEntity
    {
        //public int Cod { get; set; }
        public Nullable<int> CodBancoPortador { get; set; }
        public string CodigoInstrucao { get; set; }
        public string Numero { get; set; }
        public string Descricao { get; set; }
        //public System.DateTime DataCadastro { get; set; }
        //public Nullable<System.DateTime> DataAlteracao { get; set; }
        //public Nullable<System.DateTime> DataExcluido { get; set; }
        public virtual TabelaBancoPortador TabelaBancoPortador { get; set; }
    }
}
