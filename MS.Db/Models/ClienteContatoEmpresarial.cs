using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ClienteContatoEmpresarial : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public string NomeContato { get; set; }
         public string Profissao { get; set; }
         public string TelefoneResidencial { get; set; }
         public string TelefoneComercial { get; set; }
         public string TelefoneCelular { get; set; }
         public string Email { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string Departamento { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}
