using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class Contrato : BaseEntity
    {
        public Contrato()
        {
            this.ClienteEnderecoes = new List<ClienteEndereco>();
            this.ClienteParcelaCreditoes = new List<ClienteParcelaCredito>();
            this.ContratoAcaoCobrancaHistoricoes = new List<ContratoAcaoCobrancaHistorico>();
            this.ContratoAvalistas = new List<ContratoAvalista>();
            this.ContratoClientes = new List<ContratoCliente>();
            this.ContratoDespesas = new List<ContratoDespesa>();
            this.ContratoDocumentoes = new List<ContratoDocumento>();
            this.ContratoEscritorioHistoricoes = new List<ContratoEscritorioHistorico>();
            this.ContratoHistoricoes = new List<ContratoHistorico>();
            this.ContratoParcelas = new List<ContratoParcela>();
            this.ContratoParcelaAcordoes = new List<ContratoParcelaAcordo>();
            this.ContratoTipoCobrancaHistoricoes = new List<ContratoTipoCobrancaHistorico>();
            this.Historicoes = new List<Historico>();
        }

        //  public int Cod { get; set; }
        public Nullable<int> CodUnidade { get; set; }
        public Nullable<System.DateTime> DataImportacao { get; set; }
        public Nullable<int> CodUsuarioImportacao { get; set; }
        public Nullable<int> CodUsuarioDonoCarteira { get; set; }
        public Nullable<int> CodTipoCobranca { get; set; }
        public Nullable<int> CodUsuarioTipoCobranca { get; set; }
        public Nullable<int> CodGestao { get; set; }
        public Nullable<int> CodTipoBloqueio { get; set; }
        public Nullable<int> CodTipoCarteira { get; set; }
        public Nullable<int> CodNatureza { get; set; }
        public Nullable<int> CodUsuarioBloqueio { get; set; }
        public Nullable<int> CodEmpresa { get; set; }
        public Nullable<int> CodUsuarioAcaoRe { get; set; }
        public Nullable<int> CodUsuarioNotificacao { get; set; }
        public Nullable<int> CodUsuarioCadastro { get; set; }
        public Nullable<int> CodEscritorio { get; set; }
        public Nullable<System.DateTime> DataTipoCobranca { get; set; }
        public Nullable<System.DateTime> DataBloqueio { get; set; }
        public Nullable<System.DateTime> DataChaves { get; set; }
        public Nullable<System.DateTime> DataIPTU { get; set; }
        public string NumeroContrato { get; set; }
        public Nullable<System.DateTime> DataContrato { get; set; }
        public Nullable<decimal> ValorVenda { get; set; }
        public string UnidadeRes { get; set; }
        public Nullable<decimal> PercentualEmpresa { get; set; }
        public Nullable<decimal> PercentualSocio { get; set; }
        public Nullable<decimal> PercentualParceiro { get; set; }
        public Nullable<decimal> ValorIPTU { get; set; }
        public Nullable<decimal> ValorCondominio { get; set; }
        public Nullable<System.DateTime> DataCondominio { get; set; }
        public string AndamentodaObra { get; set; }
        public Nullable<decimal> MetroQuadrado { get; set; }
        public Nullable<bool> DAF { get; set; }
        public Nullable<bool> Chaves { get; set; }
        public Nullable<bool> Posse { get; set; }
        public Nullable<short> NotificacaoTipo { get; set; }
        public Nullable<bool> AcaoRe { get; set; }
        public string NumeroProcessoAcaoRe { get; set; }
        public Nullable<System.DateTime> DataAcaoRe { get; set; }
        public Nullable<System.DateTime> DataNotificacao { get; set; }
        public Nullable<bool> Bloqueado { get; set; }
        public string Justificativa { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<short> TipoContrato { get; set; }
        public Nullable<int> CodClientePrincipal { get; set; }
        public Nullable<int> CodContratoHistorico { get; set; }
        public Nullable<decimal> ValorAtraso { get; set; }
        public Nullable<int> CodProposta { get; set; }
        public Nullable<int> CodAging { get; set; }
        public Nullable<int> DiasAtraso { get; set; }
        public Nullable<bool> Avalista { get; set; }
        public Nullable<bool> Juridico { get; set; }
        public string TipoVenda { get; set; }
        public string StatusRepasse { get; set; }
        public Nullable<System.DateTime> PrevisaoSGI { get; set; }
        public Nullable<int> CodLogProcessamento { get; set; }
        public Nullable<System.DateTime> DataAtendimento { get; set; }
        public Nullable<int> CodUsuarioDistribuicaoCarteira { get; set; }
        public Nullable<System.DateTime> DataDistribuicao { get; set; }
        public Nullable<int> CodRegionalProduto { get; set; }
        public string CodigoCliente { get; set; }
        public Nullable<int> DiasSintese { get; set; }
        public Nullable<int> DiasFase { get; set; }
        public Nullable<decimal> ValorAVencer { get; set; }
        public Nullable<decimal> ValorPago { get; set; }
        public Nullable<decimal> ValorTotal { get; set; }
        public Nullable<int> CodImportacao { get; set; }
        public Nullable<System.DateTime> DataFinalizacao { get; set; }
        public Nullable<int> CodUsuarioAtendimento { get; set; }
        public Nullable<int> CodHistorico { get; set; }
        public Nullable<short> Governo { get; set; }
        public string StatusContrato { get; set; }
        public string MensagemContrato { get; set; }
        public Nullable<int> CodRegionalGrupo { get; set; }
        public virtual ICollection<ClienteEndereco> ClienteEnderecoes { get; set; }
        public virtual ICollection<ClienteParcelaCredito> ClienteParcelaCreditoes { get; set; }
        public virtual ContratoHistorico ContratoHistorico { get; set; }
        public virtual Historico Historico { get; set; }
        public virtual Proposta Proposta { get; set; }
        public virtual TabelaAging TabelaAging { get; set; }
        public virtual TabelaEmpresa TabelaEmpresa { get; set; }
        public virtual TabelaEscritorio TabelaEscritorio { get; set; }
        public virtual TabelaNatureza TabelaNatureza { get; set; }
        public virtual TabelaRegionalProduto TabelaRegionalProduto { get; set; }
        public virtual TabelaTipoBloqueio TabelaTipoBloqueio { get; set; }
        public virtual TabelaTipoCarteira TabelaTipoCarteira { get; set; }
        public virtual TabelaTipoCobranca TabelaTipoCobranca { get; set; }
        public virtual TabelaTipoCobrancaLogProcessamento TabelaTipoCobrancaLogProcessamento { get; set; }
        public virtual TabelaUnidade TabelaUnidade { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Usuario Usuario1 { get; set; }
        public virtual Usuario Usuario2 { get; set; }
        public virtual Usuario Usuario3 { get; set; }
        public virtual Usuario Usuario4 { get; set; }
        public virtual Usuario Usuario5 { get; set; }
        public virtual Usuario Usuario6 { get; set; }
        public virtual ICollection<ContratoAcaoCobrancaHistorico> ContratoAcaoCobrancaHistoricoes { get; set; }
        public virtual ICollection<ContratoAvalista> ContratoAvalistas { get; set; }
        public virtual ICollection<ContratoCliente> ContratoClientes { get; set; }
        public virtual ICollection<ContratoDespesa> ContratoDespesas { get; set; }
        public virtual ICollection<ContratoDocumento> ContratoDocumentoes { get; set; }
        public virtual ICollection<ContratoEscritorioHistorico> ContratoEscritorioHistoricoes { get; set; }
        public virtual ICollection<ContratoHistorico> ContratoHistoricoes { get; set; }
        public virtual ICollection<ContratoParcela> ContratoParcelas { get; set; }
        public virtual ICollection<ContratoParcelaAcordo> ContratoParcelaAcordoes { get; set; }
        public virtual ICollection<ContratoTipoCobrancaHistorico> ContratoTipoCobrancaHistoricoes { get; set; }
        public virtual ICollection<Historico> Historicoes { get; set; }
    }
}
