using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaParametroAtendimentoUsuario : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodParametroAtendimento { get; set; }
         public Nullable<int> CodEquipeUsuario { get; set; }
         public Nullable<int> CodUsuario { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodEquipe { get; set; }
        public virtual TabelaEquipe TabelaEquipe { get; set; }
        public virtual TabelaEquipeUsuario TabelaEquipeUsuario { get; set; }
        public virtual TabelaParametroAtendimento TabelaParametroAtendimento { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
