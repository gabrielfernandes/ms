using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ContratoParcelaBoleto : BaseEntity
    {
        public ContratoParcelaBoleto()
        {
            this.ContratoParcelaBoletoHistoricoes = new List<ContratoParcelaBoletoHistorico>();
        }
        //  public int Cod { get; set; }
        public Nullable<int> CodParcela { get; set; }
        public Nullable<int> CodAcordo { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public string NumeroDocumento { get; set; }
        public Nullable<int> NossoNumero { get; set; }
        public Nullable<System.DateTime> DataDocumento { get; set; }
        public Nullable<System.DateTime> DataVencimento { get; set; }
        public Nullable<System.DateTime> DataCredito { get; set; }
        public Nullable<decimal> ValorDocumento { get; set; }
        public Nullable<decimal> ValorDesconto { get; set; }
        public Nullable<decimal> ValorPago { get; set; }
        public Nullable<short> TipoOcorrencia { get; set; }
        public Nullable<short> Status { get; set; }
        public Nullable<System.DateTime> DataStatus { get; set; }
        public Nullable<System.DateTime> DataAprovacao { get; set; }
        public string Guid { get; set; }
        public string Instrucao1 { get; set; }
        public string Instrucao2 { get; set; }
        public string Instrucao3 { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<System.DateTime> DataRemessa { get; set; }
        public Nullable<System.DateTime> DataRetorno { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public Nullable<int> CodUsuarioRemessa { get; set; }
        public Nullable<int> CodUsuarioRetorno { get; set; }
        public string CodOcorrencia { get; set; }
        public string DescOcorrencia { get; set; }
        public string Erro { get; set; }
        public Nullable<decimal> JurosAoMesPorcentagem { get; set; }
        public Nullable<decimal> MultaAtrasoPorcentagem { get; set; }
        public Nullable<decimal> ValorJuros { get; set; }
        public Nullable<decimal> ValorMulta { get; set; }

        public Nullable<decimal> JurosAoMesPorcentagemPrevisto { get; set; }
        public Nullable<decimal> MultaAtrasoPorcentagemPrevisto { get; set; }
        public Nullable<decimal> ValorJurosPrevisto { get; set; }
        public Nullable<decimal> ValorMultaPrevisto { get; set; }
        public Nullable<decimal> ValorDocumentoPrevisto { get; set; }
        public Nullable<decimal> ValorDocumentoOriginal { get; set; }
        public Nullable<int> PeriodoEmMes { get; set; }
        public Nullable<int> CodHistorico { get; set; }
        public string CodigoCliente { get; set; }
        public string Companhia { get; set; }
        public Nullable<bool> Alterar { get; set; }
        public Nullable<int> CodLote { get; set; }
        public Nullable<int> CodUsuarioStatus { get; set; }
        public Nullable<decimal> Imposto { get; set; }
        public Nullable<decimal> ValorDocumentoAberto { get; set; }
        public string MensagemImpressa { get; set; }
        public string InstrucaoPagamento { get; set; }
        public int? CodCliente { get; set; }
        public string LinhaDigitavel { get; set; }
        public short? TipoSolicitacao { get; set; }
        public virtual Acordo Acordo { get; set; }
        public virtual ContratoParcela ContratoParcela { get; set; }
        public virtual ContratoParcelaBoletoHistorico ContratoParcelaBoletoHistorico { get; set; }
        public virtual ContratoParcelaBoletoLote ContratoParcelaBoletoLote { get; set; }

        public virtual ICollection<ContratoParcelaBoletoHistorico> ContratoParcelaBoletoHistoricoes { get; set; }
    }
}
