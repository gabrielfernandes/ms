using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ContratoParcelaBoletoLote : BaseEntity
    {
        public ContratoParcelaBoletoLote()
        {
            this.ContratoParcelaBoletoes = new List<ContratoParcelaBoleto>();
        }

        //  public int Cod { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public Nullable<int> Quantidade { get; set; }
        public Nullable<int> NumeroLote { get; set; }
        public Nullable<int> CodLogArquivo { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        public virtual ICollection<ContratoParcelaBoleto> ContratoParcelaBoletoes { get; set; }
    }
}
