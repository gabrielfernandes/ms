using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoUnidade : BaseEntity
    {
        public TabelaTipoUnidade()
        {
            this.TabelaUnidades = new List<TabelaUnidade>();
        }

       //  public int Cod { get; set; }
         public string TipoUnidade { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<TabelaUnidade> TabelaUnidades { get; set; }
    }
}
