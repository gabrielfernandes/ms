using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoCobrancaPosicao : BaseEntity
    {
       //  public int Cod { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodTipoCobranca { get; set; }
         public Nullable<int> NumeroDias { get; set; }
         public Nullable<int> CodFilial { get; set; }
        public virtual TabelaTipoCobranca TabelaTipoCobranca { get; set; }
    }
}
