using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoDespesa : BaseEntity
    {
        public TabelaTipoDespesa()
        {
            this.ContratoDespesas = new List<ContratoDespesa>();
        }

       //  public int Cod { get; set; }
         public string TipoDespesa { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<ContratoDespesa> ContratoDespesas { get; set; }
    }
}
