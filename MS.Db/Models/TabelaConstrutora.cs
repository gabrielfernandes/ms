using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class TabelaConstrutora : BaseEntity
    {
        public TabelaConstrutora()
        {
            this.TabelaEmpreendimentoes = new List<TabelaEmpreendimento>();
            this.TabelaEmpresas = new List<TabelaEmpresa>();
            this.TabelaRegionals = new List<TabelaRegional>();
            this.TabelaRegionalGrupoes = new List<TabelaRegionalGrupo>();
        }

        //  public int Cod { get; set; }
        public Nullable<int> CodEndereco { get; set; }
        public string NomeConstrutora { get; set; }
        public string CNPJ { get; set; }
        public string InscricaoEstadual { get; set; }
        public string Login { get; set; }
        public string senha { get; set; }
        public string CaminhoSite { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public string CodCliente { get; set; }
        public virtual TabelaEndereco TabelaEndereco { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes { get; set; }
        public virtual ICollection<TabelaEmpresa> TabelaEmpresas { get; set; }
        public virtual ICollection<TabelaRegional> TabelaRegionals { get; set; }
        public virtual ICollection<TabelaRegionalGrupo> TabelaRegionalGrupoes { get; set; }
    }
}
