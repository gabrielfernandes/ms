using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ClienteAtendimento : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public Nullable<System.DateTime> DataAtendimento { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<System.DateTime> DataFinalizacaoAtendimento { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
