using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaResolucao : BaseEntity
    {
        public TabelaResolucao()
        {
            this.ClienteHistoricoes = new List<ClienteHistorico>();
            this.ContratoHistoricoes = new List<ContratoHistorico>();
        }

       //  public int Cod { get; set; }
         public string Resolucao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<ClienteHistorico> ClienteHistoricoes { get; set; }
        public virtual ICollection<ContratoHistorico> ContratoHistoricoes { get; set; }
    }
}
