using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoCobrancaCondicao : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodTipoCobranca { get; set; }
         public Nullable<short> Tipo { get; set; }
         public Nullable<short> Condicao { get; set; }
         public string Valor { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<bool> Excecao { get; set; }
        public virtual TabelaTipoCobranca TabelaTipoCobranca { get; set; }

        [NotMapped]
        public string ValorDescricao { get; set; }
    }
}
