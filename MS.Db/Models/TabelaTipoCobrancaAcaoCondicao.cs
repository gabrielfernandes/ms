using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoCobrancaAcaoCondicao : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodTipoCobrancaAcao { get; set; }
         public Nullable<short> CondicaoTipo { get; set; }
         public Nullable<short> CampoTipo { get; set; }
         public Nullable<short> OperadorLogicoTipo { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaTipoCobrancaAcao TabelaTipoCobrancaAcao { get; set; }
    }
}
