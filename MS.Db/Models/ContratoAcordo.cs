using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ContratoAcordo : BaseEntity
    {
        public ContratoAcordo()
        {
            this.ContratoParcelaAcordoes = new List<ContratoParcelaAcordo>();
        }

       //  public int Cod { get; set; }
         public Nullable<short> TipoAcordo { get; set; }
         public Nullable<int> CodUsuario { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string Observacao { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<ContratoParcelaAcordo> ContratoParcelaAcordoes { get; set; }
    }
}
