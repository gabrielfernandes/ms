using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ParametroAtendimento : BaseEntity
    {
        public ParametroAtendimento()
        {
            this.ParametroAtendimentoLogs = new List<ParametroAtendimentoLog>();
            this.ParametroAtendimentoProdutoes = new List<ParametroAtendimentoProduto>();
            this.ParametroAtendimentoRotas = new List<ParametroAtendimentoRota>();
        }

        //  public int Cod { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public Nullable<int> CodRegional { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual TabelaRegional TabelaRegional { get; set; }
        public virtual ICollection<ParametroAtendimentoLog> ParametroAtendimentoLogs { get; set; }
        public virtual ICollection<ParametroAtendimentoProduto> ParametroAtendimentoProdutoes { get; set; }
        public virtual ICollection<ParametroAtendimentoRota> ParametroAtendimentoRotas { get; set; }
    }
}
