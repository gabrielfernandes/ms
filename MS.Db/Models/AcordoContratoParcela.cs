using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class AcordoContratoParcela : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodAcordo { get; set; }
         public Nullable<int> CodContratoParcela { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual Acordo Acordo { get; set; }
        public virtual ContratoParcela ContratoParcela { get; set; }
    }
}
