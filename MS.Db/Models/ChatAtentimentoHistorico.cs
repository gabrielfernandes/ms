using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ChatAtentimentoHistorico : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodSintese { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public Nullable<System.DateTime> DataAgenda { get; set; }
         public string Observacao { get; set; }
         public Nullable<int> CodContrato { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ChatAtendimento ChatAtendimento { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
