using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaGrupo : BaseEntity
    {
        public TabelaGrupo()
        {
            this.TabelaGrupoClientes = new List<TabelaGrupoCliente>();
        }

       //  public int Cod { get; set; }
         public string Nome { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<bool> AcaoBloqueada { get; set; }
        public virtual ICollection<TabelaGrupoCliente> TabelaGrupoClientes { get; set; }
    }
}
