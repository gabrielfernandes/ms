using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ProspectCompromissoFinanceiro : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodProspect { get; set; }
         public Nullable<short> Tipo { get; set; }
         public Nullable<int> QtdPrestacaoRestante { get; set; }
         public Nullable<int> NumeroParcela { get; set; }
         public Nullable<decimal> Valor { get; set; }
         public string EmpresaCredora { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual PropostaProspect PropostaProspect { get; set; }
    }
}
