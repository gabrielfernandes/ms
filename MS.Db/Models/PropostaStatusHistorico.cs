using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class PropostaStatusHistorico : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodProposta { get; set; }
         public Nullable<short> Status { get; set; }
         public string Observacao { get; set; }
         public Nullable<int> CodUsuario { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual Proposta Proposta { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
