using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaParametroTaxa : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<bool> MultaElegivel { get; set; }
         public Nullable<decimal> MultaDe { get; set; }
         public Nullable<decimal> MultaAte { get; set; }
         public Nullable<bool> JurosElegivel { get; set; }
         public Nullable<decimal> JurosDe { get; set; }
         public Nullable<decimal> JurosAte { get; set; }
         public Nullable<bool> HonorarioPercentualElegivel { get; set; }
         public Nullable<decimal> HonorarioPercentualDe { get; set; }
         public Nullable<decimal> HonorarioPercentualAte { get; set; }
         public Nullable<bool> HonorarioValorElegivel { get; set; }
         public Nullable<decimal> HonorarioValorDe { get; set; }
         public Nullable<decimal> HonorarioValorAte { get; set; }
         public Nullable<bool> DescontoPercentualElegivel { get; set; }
         public Nullable<decimal> DescontoPercentualValor { get; set; }
         public Nullable<bool> JurosParcelaElegivel { get; set; }
         public Nullable<decimal> JurosParcelaValorDe { get; set; }
         public Nullable<decimal> JurosParcelaValorAte { get; set; }
         public Nullable<short> TipoCorrecao { get; set; }
         public Nullable<bool> ParcelaNaoMensaisElegivel { get; set; }
         public Nullable<bool> UtilizaJurosCompostoElegivel { get; set; }
         public Nullable<bool> CalculaJurosCorrecaoElegivel { get; set; }
         public Nullable<bool> CalculaMultaCorrecaoElegivel { get; set; }
         public Nullable<bool> CalculaMultaSobreJurosElegivel { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
    }
}
