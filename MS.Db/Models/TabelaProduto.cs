using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaProduto : BaseEntity
    {
        public TabelaProduto()
        {
            this.ParametroAtendimentoProdutoes = new List<ParametroAtendimentoProduto>();
            this.TabelaRegionalProdutoes = new List<TabelaRegionalProduto>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodRegional { get; set; }
         public string Produto { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string Descricao { get; set; }
        public virtual ICollection<ParametroAtendimentoProduto> ParametroAtendimentoProdutoes { get; set; }
        public virtual ICollection<TabelaRegionalProduto> TabelaRegionalProdutoes { get; set; }
    }
}
