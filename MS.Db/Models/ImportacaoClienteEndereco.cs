using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ImportacaoClienteEndereco : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodImportacao { get; set; }
         public string Contrato { get; set; }
         public string EnderecoCob { get; set; }
         public string NumeroCob { get; set; }
         public string ComplementoCob { get; set; }
         public string BairroCob { get; set; }
         public string CEPCob { get; set; }
         public string CidadeCob { get; set; }
         public string EstadoCob { get; set; }
         public string EnderecoObra { get; set; }
         public string NumeroObra { get; set; }
         public string ComplementoObra { get; set; }
         public string BairroObra { get; set; }
         public string CepObra { get; set; }
         public string CidadeObra { get; set; }
         public string EstadoObra { get; set; }
         public string Mensagem { get; set; }
         public Nullable<short> Status { get; set; }
         public Nullable<System.DateTime> DataProcessamento { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<int> CodContrato { get; set; }
         public Nullable<int> CodEnderecoCob { get; set; }
         public Nullable<int> CodEnderecoObra { get; set; }
         public string MensagemContrato { get; set; }
        public virtual Importacao Importacao { get; set; }
    }
}
