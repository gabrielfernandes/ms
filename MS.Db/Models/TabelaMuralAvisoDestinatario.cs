using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaMuralAvisoDestinatario : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodMuralAviso { get; set; }
         public Nullable<int> CodUsuario { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaMuralAviso TabelaMuralAviso { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
