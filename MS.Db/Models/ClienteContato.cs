using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ClienteContato : BaseEntity
    {
       //  public int Cod { get; set; }
         public string Contato { get; set; }
         public Nullable<short> Tipo { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<bool> ContatoHOT { get; set; }
         public Nullable<int> ContatoHOTCodUsuario { get; set; }
         public Nullable<System.DateTime> ContatoHOTData { get; set; }
         public string NomeContato { get; set; }
         public Nullable<int> CodImportacao { get; set; }
         public Nullable<int> CodTipoCadastro { get; set; }
         public string Descricao { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual TabelaTipoCadastro TabelaTipoCadastro { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
