using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TempImportacao : BaseEntity
    {
       //  public int Cod { get; set; }
         public string CodCliente { get; set; }
         public string Nome { get; set; }
         public string NumeroContrato { get; set; }
         public string Erro { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
    }
}
