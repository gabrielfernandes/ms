using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class LogMensagem : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<short> Tipo { get; set; }
         public string NomeDestinatario { get; set; }
         public string Celular { get; set; }
         public string Conteudo { get; set; }
         public Nullable<short> Status { get; set; }
         public Nullable<int> TentativaEnvio { get; set; }
         public string Mensagem { get; set; }
         public Nullable<System.DateTime> DataEnvio { get; set; }
         public string RetornoIntegracao { get; set; }
         public Nullable<short> StatusIntegracao { get; set; }
         public Nullable<System.DateTime> DataEnvioIntegracao { get; set; }
         public Nullable<System.DateTime> DataAgendamentoIntegracao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
         public Nullable<int> CodEnvioMassa { get; set; }
        public virtual LogEnvioMassa LogEnvioMassa { get; set; }
    }
}
