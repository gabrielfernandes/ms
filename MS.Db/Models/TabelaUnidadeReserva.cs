using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaUnidadeReserva : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodUnidade { get; set; }
         public Nullable<int> CodUnidadeStatusHistorico { get; set; }
         public Nullable<int> CodUsuario { get; set; }
         public Nullable<System.DateTime> DataReserva { get; set; }
         public Nullable<short> StatusReserva { get; set; }
         public string Nome { get; set; }
         public string Email { get; set; }
         public string Celular { get; set; }
         public string Telefone { get; set; }
         public string Observacao { get; set; }
         public Nullable<System.DateTime> DataExpiracao { get; set; }
         public string CPF { get; set; }
         public string CNPJ { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaUnidadeStatusHistorico TabelaUnidadeStatusHistorico { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
