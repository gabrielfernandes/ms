using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class RotaRegional : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodRota { get; set; }
         public Nullable<int> CodRegional { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual Rota Rota { get; set; }
        public virtual TabelaRegional TabelaRegional { get; set; }
    }
}
