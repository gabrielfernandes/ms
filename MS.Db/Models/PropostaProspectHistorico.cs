using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class PropostaProspectHistorico : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodPropostaProspect { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodSintese { get; set; }
         public string Observacao { get; set; }
         public Nullable<int> CodUsuario { get; set; }
        public virtual PropostaProspect PropostaProspect { get; set; }
        public virtual TabelaSintese TabelaSintese { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
