using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaAnaliseDocumentalAlternativa : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodAnaliseDocumental { get; set; }
         public string Alternativa { get; set; }
         public Nullable<decimal> Valor { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaAnaliseDocumental TabelaAnaliseDocumental { get; set; }
    }
}
