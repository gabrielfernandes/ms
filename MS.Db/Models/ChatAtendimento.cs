using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ChatAtendimento : BaseEntity
    {
        public ChatAtendimento()
        {
            this.ChatAtendimentoMensagems = new List<ChatAtendimentoMensagem>();
            this.ChatAtentimentoHistoricoes = new List<ChatAtentimentoHistorico>();
        }

        //  public int Cod { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        public Nullable<System.DateTime> DataAtendimento { get; set; }
        public Nullable<System.DateTime> DataEncerrado { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string IP { get; set; }
        public Nullable<int> CodResultado { get; set; }
        public Nullable<int> CodAtendimentoTipo { get; set; }
        public Nullable<int> CodProposta { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ChatAtendimentoResultado ChatAtendimentoResultado { get; set; }
        public virtual ChatAtendimentoTipo ChatAtendimentoTipo { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<ChatAtendimentoMensagem> ChatAtendimentoMensagems { get; set; }
        public virtual ICollection<ChatAtentimentoHistorico> ChatAtentimentoHistoricoes { get; set; }
    }
}
