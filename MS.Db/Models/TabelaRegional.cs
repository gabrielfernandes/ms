using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaRegional : BaseEntity
    {
        public TabelaRegional()
        {
            this.ParametroAtendimentoes = new List<ParametroAtendimento>();
            this.RotaRegionals = new List<RotaRegional>();
            this.TabelaEmpreendimentoes = new List<TabelaEmpreendimento>();
            this.TabelaRegionalGrupoes = new List<TabelaRegionalGrupo>();
            this.TabelaRegionalProdutoes = new List<TabelaRegionalProduto>();
            this.Usuarios = new List<Usuario>();
            this.UsuarioRegionals = new List<UsuarioRegional>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodConstrutora { get; set; }
         public Nullable<int> CodEndereco { get; set; }
         public string Regional { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string CodRegionalCliente { get; set; }
         public string Endereco { get; set; }
         public string Numero { get; set; }
         public string Complemento { get; set; }
         public string Cidade { get; set; }
         public string Bairro { get; set; }
         public string Estado { get; set; }
         public string Pais { get; set; }
         public string Telefone { get; set; }
         public string Email { get; set; }
         public string Responsavel { get; set; }
         public string CEP { get; set; }
        public Nullable<short> TipoRegiao { get; set; }
        public virtual ICollection<RotaRegional> RotaRegionals { get; set; }
        public virtual TabelaConstrutora TabelaConstrutora { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes { get; set; }
        public virtual TabelaEndereco TabelaEndereco { get; set; }
        public virtual ICollection<ParametroAtendimento> ParametroAtendimentoes { get; set; }
        public virtual ICollection<TabelaRegionalGrupo> TabelaRegionalGrupoes { get; set; }
        public virtual ICollection<TabelaRegionalProduto> TabelaRegionalProdutoes { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
        public virtual ICollection<UsuarioRegional> UsuarioRegionals { get; set; }
    }
}
