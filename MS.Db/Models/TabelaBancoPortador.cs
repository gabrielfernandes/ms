using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class TabelaBancoPortador : BaseEntity
    {
        public TabelaBancoPortador()
        {
            this.TabelaBancoPortadorInstrucaos = new List<TabelaBancoPortadorInstrucao>();
            this.TabelaBancoPortadorLogArquivos = new List<TabelaBancoPortadorLogArquivo>();
        }

        //  public int Cod { get; set; }
        public Nullable<int> CodBanco { get; set; }
        public string Portador { get; set; }
        public string Agencia { get; set; }
        public string AgenciaDigito { get; set; }
        public string Conta { get; set; }
        public string ContaDigito { get; set; }
        public Nullable<bool> Ativo { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        public string CNPJ { get; set; }
        public string CEP { get; set; }
        public string Endereco { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Bairro { get; set; }
        public string UF { get; set; }
        public string EnderecoNumero { get; set; }
        public string EnderecoComplemento { get; set; }
        public Nullable<short> BancoNumero { get; set; }
        public Nullable<short> DocumentoCodigoEspecie { get; set; }
        public Nullable<short> Carteira { get; set; }
        public string Companhia { get; set; }
        public Nullable<int> NossoNumero { get; set; }
        public Nullable<int> NossoNumeroAtual { get; set; }
        public Nullable<int> Lote { get; set; }
        public Nullable<int> LoteAtual { get; set; }
        public string CompanhiaFiscal { get; set; }
        public Nullable<int> CodRegionalGrupo { get; set; }
        public Nullable<int> CodBancoPortadorGrupo { get; set; }
        public Nullable<bool> Padrao { get; set; }

        public Nullable<decimal> JurosAoMesPorcentagem { get; set; }
        public Nullable<decimal> TaxaPorParcela { get; set; }
        public Nullable<decimal> MultaAtraso { get; set; }
        public Nullable<decimal> MoraAtrasoPorcetagemDia { get; set; }
        public Nullable<DateTime> DataInicialRegistroBoleto { get; set; }
        public Nullable<int> NumeroDiaRegistroBoleto { get; set; }
        public virtual TabelaBanco TabelaBanco { get; set; }
        public virtual TabelaRegionalGrupo TabelaRegionalGrupo { get; set; }
        public virtual ICollection<TabelaBancoPortadorInstrucao> TabelaBancoPortadorInstrucaos { get; set; }
        public virtual ICollection<TabelaBancoPortadorLogArquivo> TabelaBancoPortadorLogArquivos{ get; set; }
        public virtual TabelaBancoPortadorGrupo TabelaBancoPortadorGrupo { get; set; }
    }
}
