using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaCampanha : BaseEntity
    {
        public TabelaCampanha()
        {
            this.TabelaCampanhaAcaos = new List<TabelaCampanhaAcao>();
            this.TabelaCampanhaCondicaos = new List<TabelaCampanhaCondicao>();
            this.TabelaCampanhaEmails = new List<TabelaCampanhaEmail>();
            this.TabelaFases = new List<TabelaFase>();
        }

       //  public int Cod { get; set; }
         public string Campanha { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<System.DateTime> DataInicio { get; set; }
         public Nullable<System.DateTime> DataTerminio { get; set; }
         public string Produto { get; set; }
         public string Condicao { get; set; }
         public string Excecao { get; set; }
        public virtual ICollection<TabelaCampanhaAcao> TabelaCampanhaAcaos { get; set; }
        public virtual ICollection<TabelaCampanhaCondicao> TabelaCampanhaCondicaos { get; set; }
        public virtual ICollection<TabelaCampanhaEmail> TabelaCampanhaEmails { get; set; }
        public virtual ICollection<TabelaFase> TabelaFases { get; set; }
    }
}
