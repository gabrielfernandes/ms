using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoParcela : BaseEntity
    {
        public TabelaTipoParcela()
        {
            this.PropostaValores = new List<PropostaValore>();
            this.TabelaVendaFluxoes = new List<TabelaVendaFluxo>();
        }

       //  public int Cod { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string TipoParcela { get; set; }
         public Nullable<int> QtdMes { get; set; }
         public Nullable<short> TipoPeriodo { get; set; }
        public virtual ICollection<PropostaValore> PropostaValores { get; set; }
        public virtual ICollection<TabelaVendaFluxo> TabelaVendaFluxoes { get; set; }
    }
}
