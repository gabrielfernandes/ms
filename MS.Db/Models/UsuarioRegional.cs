using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class UsuarioRegional : BaseEntity
    {
       //  public int Cod { get; set; }
         public int CodUsuario { get; set; }
         public int CodRegional { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaRegional TabelaRegional { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
