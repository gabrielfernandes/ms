using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaVendaFluxo : BaseEntity
    {
       //  public int Cod { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodTabelaVenda { get; set; }
         public Nullable<int> CodTipoParcela { get; set; }
         public int Ordem { get; set; }
         public Nullable<decimal> PercentualMinimo { get; set; }
         public Nullable<decimal> Valor { get; set; }
        public virtual TabelaTipoParcela TabelaTipoParcela { get; set; }
        public virtual TabelaVenda TabelaVenda { get; set; }
    }
}
