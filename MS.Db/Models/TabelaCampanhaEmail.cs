using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaCampanhaEmail : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodCampanha { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<int> CodModelo { get; set; }
         public Nullable<bool> Excecao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodUsuario { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual TabelaCampanha TabelaCampanha { get; set; }
        public virtual TabelaModeloAcao TabelaModeloAcao { get; set; }
    }
}
