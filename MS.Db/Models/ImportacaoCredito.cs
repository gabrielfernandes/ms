using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ImportacaoCredito : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodImportacao { get; set; }
         public string NumeroDocumento { get; set; }
         public string NumeroCliente { get; set; }
         public string Descricao { get; set; }
         public string DataCredito { get; set; }
         public string ValorCredito { get; set; }
         public string Observacao { get; set; }
         public Nullable<short> Status { get; set; }
         public string Mensagem { get; set; }
         public Nullable<System.DateTime> DataProcessamento { get; set; }
         public Nullable<int> CodCliente { get; set; }
        public virtual Importacao Importacao { get; set; }
    }
}
