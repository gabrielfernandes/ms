using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaEmpresaVenda : BaseEntity
    {
        public TabelaEmpresaVenda()
        {
            this.Propostas = new List<Proposta>();
            this.Usuarios = new List<Usuario>();
        }

       //  public int Cod { get; set; }
         public string EmpresaVendas { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<Proposta> Propostas { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
