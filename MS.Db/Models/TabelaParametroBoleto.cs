using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaParametroBoleto : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodBanco { get; set; }
         public string Portador { get; set; }
         public Nullable<int> NumeroAgencia { get; set; }
         public Nullable<int> DigitoAgencia { get; set; }
         public Nullable<int> NumeroConta { get; set; }
         public Nullable<int> DigitoConta { get; set; }
         public Nullable<bool> Ativo { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
        public virtual TabelaBanco TabelaBanco { get; set; }
    }
}
