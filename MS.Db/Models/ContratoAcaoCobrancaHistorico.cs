using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ContratoAcaoCobrancaHistorico : BaseEntity
    {
        public ContratoAcaoCobrancaHistorico()
        {
            this.Historicoes = new List<Historico>();
        }
        //  public int Cod { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public Nullable<bool> EnvioAutomatico { get; set; }
        public Nullable<int> CodTipoCobrancaAcao { get; set; }
        public string Conteudo { get; set; }
        public Nullable<System.DateTime> DataEnvio { get; set; }
        public Nullable<System.DateTime> DataEnvioAgendado { get; set; }
        public string Status { get; set; }
        public Nullable<short> SituacaoTipo { get; set; }
        public Nullable<int> CodCliente { get; set; }
        public string TituloAcao { get; set; }
        public Nullable<int> CodParcela { get; set; }
        public Nullable<DateTime> DataAtendimentoAgendado { get; set; }
        public string CondicaoExecutada { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual TabelaTipoCobrancaAcao TabelaTipoCobrancaAcao { get; set; }
        public virtual ICollection<Historico> Historicoes { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
