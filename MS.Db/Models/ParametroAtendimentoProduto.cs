using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ParametroAtendimentoProduto : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodProduto { get; set; }
         public Nullable<int> CodParametroAtendimento { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ParametroAtendimento ParametroAtendimento { get; set; }
        public virtual TabelaProduto TabelaProduto { get; set; }
    }
}
