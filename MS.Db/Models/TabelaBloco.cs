using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaBloco : BaseEntity
    {
        public TabelaBloco()
        {
            this.TabelaUnidades = new List<TabelaUnidade>();
        }

       //  public int Cod { get; set; }
         public int CodEmpreend { get; set; }
         public string CodBlocoCliente { get; set; }
         public string NomeBloco { get; set; }
         public Nullable<int> QuantidadeUnidade { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<System.DateTime> DataHabitese { get; set; }
         public Nullable<System.DateTime> DataPromessaEntrega { get; set; }
        public virtual TabelaEmpreendimento TabelaEmpreendimento { get; set; }
        public virtual ICollection<TabelaUnidade> TabelaUnidades { get; set; }
    }
}
