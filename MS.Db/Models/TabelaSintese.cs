using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaSintese : BaseEntity
    {
        public TabelaSintese()
        {
            this.ClienteHistoricoes = new List<ClienteHistorico>();
            this.ContratoHistoricoes = new List<ContratoHistorico>();
            this.Historicoes = new List<Historico>();
            this.PropostaHistoricoes = new List<PropostaHistorico>();
            this.PropostaProspectHistoricoes = new List<PropostaProspectHistorico>();
            this.TabelaParametroAtendimentoes = new List<TabelaParametroAtendimento>();
            this.TabelaParametroSintesoesDe = new List<TabelaParametroSintese>();
            this.TabelaParametroSintesoesPara = new List<TabelaParametroSintese>();
            this.ContratoParcelaBoletoHistoricoes = new List<ContratoParcelaBoletoHistorico>();
        }

       //  public int Cod { get; set; }
         public int CodFase { get; set; }
         public Nullable<int> CodTipoCobranca { get; set; }
         public string Sintese { get; set; }
         public Nullable<int> Ordem { get; set; }
         public Nullable<int> Dias { get; set; }
         public string LoginCadastro { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<bool> TarefaAtendimento { get; set; }
         public Nullable<bool> AcaoBloqueada { get; set; }
         public string CodigoSinteseCliente { get; set; }
        public Nullable<bool> Justifica { get; set; }
        public virtual ICollection<ClienteHistorico> ClienteHistoricoes { get; set; }
        public virtual ICollection<ContratoHistorico> ContratoHistoricoes { get; set; }
        public virtual ICollection<Historico> Historicoes { get; set; }
        public virtual ICollection<PropostaHistorico> PropostaHistoricoes { get; set; }
        public virtual ICollection<PropostaProspectHistorico> PropostaProspectHistoricoes { get; set; }
        public virtual TabelaFase TabelaFase { get; set; }
        public virtual ICollection<TabelaParametroAtendimento> TabelaParametroAtendimentoes { get; set; }
        public virtual TabelaTipoCobranca TabelaTipoCobranca { get; set; }
        public virtual ICollection<TabelaParametroSintese> TabelaParametroSintesoesDe { get; set; }
        public virtual ICollection<TabelaParametroSintese> TabelaParametroSintesoesPara { get; set; }
        public virtual ICollection<ContratoParcelaBoletoHistorico> ContratoParcelaBoletoHistoricoes { get; set; }
    }
}
