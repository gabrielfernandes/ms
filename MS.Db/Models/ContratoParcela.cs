using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ContratoParcela : BaseEntity
    {
        public ContratoParcela()
        {
            this.AcordoContratoParcelas = new List<AcordoContratoParcela>();
            this.ContratoParcelaAcordoes = new List<ContratoParcelaAcordo>();
            this.ContratoParcelaBoletoes = new List<ContratoParcelaBoleto>();
            this.Historicoes = new List<Historico>();
        }

        //  public int Cod { get; set; }
        public Nullable<int> CodContrato { get; set; }
        public Nullable<int> CodNatureza { get; set; }
        public string FluxoPagamento { get; set; }
        public System.DateTime DataVencimento { get; set; }
        public Nullable<System.DateTime> DataPagamento { get; set; }
        public Nullable<decimal> ValorParcela { get; set; }
        public Nullable<decimal> ValorRecebido { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public Nullable<int> NaturezaCliente { get; set; }
        public Nullable<short> StatusParcela { get; set; }
        public Nullable<decimal> ValorAberto { get; set; }
        public string NumeroParcela { get; set; }
        public Nullable<decimal> Juros { get; set; }
        public Nullable<decimal> Multa { get; set; }
        public Nullable<decimal> ValorAtualizado { get; set; }
        public Nullable<System.DateTime> DataEmissao { get; set; }
        public Nullable<int> CodAcordo { get; set; }
        public string Obs { get; set; }
        public Nullable<short> FormaPagamento { get; set; }
        public Nullable<decimal> ValorRetornoBanco { get; set; }
        public string LogAlteracao { get; set; }
        public Nullable<int> CodImportacao { get; set; }
        public Nullable<System.DateTime> DataImportacao { get; set; }
        public Nullable<int> CodHistorico { get; set; }
        public string LinhaDigitavel { get; set; }
        public Nullable<int> CodImportacaoBoleto { get; set; }
        public Nullable<System.DateTime> DataDocumento { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string CEP { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string CodSinteseCliente { get; set; }
        public string NumeroNotaFiscal { get; set; }
        public string TipoFatura { get; set; }
        public Nullable<System.DateTime> DataFechamento { get; set; }
        public string InstrucaoPagamento { get; set; }
        public string RotaArea { get; set; }
        public Nullable<System.DateTime> DataEmissaoFatura { get; set; }
        public string Companhia { get; set; }
        public string CodRegionalCliente { get; set; }
        public Nullable<int> NossoNumero { get; set; }
        public string CodigoEmpresaCobranca { get; set; }
        public string CodCliente { get; set; }
        public Nullable<int> NumeroLinhaCliente { get; set; }
        public Nullable<decimal> Imposto { get; set; }
        public Nullable<decimal> ValorAbertoPercentual { get; set; }
        public Nullable<int> CodBoletoAprovado { get; set; }
        public string RegionalParcela { get; set; }
        public string ClienteParcela { get; set; }
        public string CompanhiaFiscal { get; set; }
        public string ClienteVIP { get; set; }
        public virtual ICollection<AcordoContratoParcela> AcordoContratoParcelas { get; set; }
        public virtual Contrato Contrato { get; set; }
        public virtual Historico Historico { get; set; }
        public virtual TabelaNatureza TabelaNatureza { get; set; }
        public virtual ICollection<ContratoParcelaAcordo> ContratoParcelaAcordoes { get; set; }
        public virtual ICollection<ContratoParcelaBoleto> ContratoParcelaBoletoes { get; set; }
        public virtual ICollection<Historico> Historicoes { get; set; }
    }
}
