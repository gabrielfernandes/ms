using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class TabelaParametroSintese : BaseEntity
    {
        public TabelaParametroSintese()
        {
        }

        //  public int Cod { get; set; }
        public Nullable<int> CodSinteseDe { get; set; }
        public Nullable<int> CodUsuario { get; set; }
        public Nullable<short> Etapa { get; set; }
        public Nullable<short> Tipo { get; set; }
        public string Observacao { get; set; }
        public Nullable<int> CodSintesePara { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaSintese TabelaSinteseDe { get; set; }
        public virtual TabelaSintese TabelaSintesePara { get; set; }
    }
}
