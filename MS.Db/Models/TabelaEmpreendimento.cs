using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaEmpreendimento : BaseEntity
    {
        public TabelaEmpreendimento()
        {
            this.TabelaBancoTipoOperacaoEmpreends = new List<TabelaBancoTipoOperacaoEmpreend>();
            this.TabelaBlocoes = new List<TabelaBloco>();
            this.TabelaEmpreendimentoDocumentoes = new List<TabelaEmpreendimentoDocumento>();
            this.TabelaEmpreendimentoFotoes = new List<TabelaEmpreendimentoFoto>();
            this.TabelaVendaEmpreendimentoes = new List<TabelaVendaEmpreendimento>();
        }

       //  public int Cod { get; set; }
         public int CodConstrutora { get; set; }
         public Nullable<int> CodRegional { get; set; }
         public string CodEmpreendimentoCliente { get; set; }
         public Nullable<int> CodUsuarioDonoDeNegocio { get; set; }
         public Nullable<int> CodUsuarioDonoDeCarteira { get; set; }
         public Nullable<int> CodUsuarioResponsavel { get; set; }
         public Nullable<int> CodBancoFinanciador { get; set; }
         public Nullable<int> CodEndereco { get; set; }
         public Nullable<int> CodBancoPiloto { get; set; }
         public string NomeEmpreendimento { get; set; }
         public Nullable<System.DateTime> DataCND { get; set; }
         public Nullable<System.DateTime> DataAverbacao { get; set; }
         public Nullable<System.DateTime> DataPastaMae { get; set; }
         public Nullable<System.DateTime> DataAssembleia { get; set; }
         public Nullable<System.DateTime> DataHabitese { get; set; }
         public Nullable<System.DateTime> DataMatricula { get; set; }
         public Nullable<System.DateTime> DataConclusaoConstrutacao { get; set; }
         public Nullable<System.DateTime> DataPrevCND { get; set; }
         public Nullable<System.DateTime> DataPrevAverbacao { get; set; }
         public Nullable<System.DateTime> DataPrevPastaMae { get; set; }
         public Nullable<System.DateTime> DataPrevAssembleia { get; set; }
         public Nullable<System.DateTime> DataPrevHabitese { get; set; }
         public Nullable<System.DateTime> DataPrevMatricula { get; set; }
         public Nullable<System.DateTime> DataPrevConclusaoConstrucao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string AndamentoObra { get; set; }
         public Nullable<int> CodTipoImovel { get; set; }
         public Nullable<int> CodCheckListNome { get; set; }
         public Nullable<int> CodCheckListEmpreendimento { get; set; }
        public virtual TabelaBanco TabelaBanco { get; set; }
        public virtual TabelaBanco TabelaBanco1 { get; set; }
        public virtual ICollection<TabelaBancoTipoOperacaoEmpreend> TabelaBancoTipoOperacaoEmpreends { get; set; }
        public virtual ICollection<TabelaBloco> TabelaBlocoes { get; set; }
        public virtual TabelaCheckListNome TabelaCheckListNome { get; set; }
        public virtual TabelaCheckListNome TabelaCheckListNome1 { get; set; }
        public virtual TabelaConstrutora TabelaConstrutora { get; set; }
        public virtual TabelaEndereco TabelaEndereco { get; set; }
        public virtual TabelaRegional TabelaRegional { get; set; }
        public virtual TabelaTipoImovel TabelaTipoImovel { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual Usuario Usuario1 { get; set; }
        public virtual Usuario Usuario2 { get; set; }
        public virtual ICollection<TabelaEmpreendimentoDocumento> TabelaEmpreendimentoDocumentoes { get; set; }
        public virtual ICollection<TabelaEmpreendimentoFoto> TabelaEmpreendimentoFotoes { get; set; }
        public virtual ICollection<TabelaVendaEmpreendimento> TabelaVendaEmpreendimentoes { get; set; }
    }
}
