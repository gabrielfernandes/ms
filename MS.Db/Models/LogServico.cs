using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class LogServico : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodTipoCobrancaAcao { get; set; }
         public Nullable<int> CodImportacao { get; set; }
         public Nullable<short> TipoServico { get; set; }
         public Nullable<short> TipoImportacao { get; set; }
         public string Arquivos { get; set; }
         public Nullable<System.DateTime> DataInicio { get; set; }
         public Nullable<System.DateTime> DataFim { get; set; }
         public string Mensagem { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
    }
}
