using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaEmpresaVendaUnidade : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodEmpresaVenda { get; set; }
         public Nullable<int> CodUnidade { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
    }
}
