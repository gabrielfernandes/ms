using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaGrupoCliente : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodGrupo { get; set; }
         public string CNPJ { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<bool> AcaoBloqueada { get; set; }
        public virtual TabelaGrupo TabelaGrupo { get; set; }
    }
}
