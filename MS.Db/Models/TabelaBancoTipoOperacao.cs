using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaBancoTipoOperacao : BaseEntity
    {
        public TabelaBancoTipoOperacao()
        {
            this.TabelaBancoTipoOperacaoEmpreends = new List<TabelaBancoTipoOperacaoEmpreend>();
        }

       //  public int Cod { get; set; }
         public Nullable<decimal> ValorVendaMinimo { get; set; }
         public Nullable<decimal> ValorVendaMaximo { get; set; }
         public Nullable<int> CodBanco { get; set; }
         public Nullable<decimal> ValorFinanciamentoMinimo { get; set; }
         public Nullable<decimal> ValorFiananciamentoMaximo { get; set; }
         public string NomeTipoOperacao { get; set; }
         public Nullable<decimal> PercentualComprometimentoRenda { get; set; }
         public Nullable<decimal> PercentualAvaliacao { get; set; }
         public Nullable<decimal> TaxaJuros { get; set; }
         public Nullable<int> PrazoMinimo { get; set; }
         public Nullable<int> PrazoMaximo { get; set; }
         public Nullable<int> IdadeMaxima { get; set; }
         public Nullable<System.DateTime> DataVigenciaDe { get; set; }
         public Nullable<System.DateTime> DataVigenciaAte { get; set; }
         public Nullable<int> CodTipoImovel { get; set; }
         public Nullable<int> SistemaAmortizacao { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaBanco TabelaBanco { get; set; }
        public virtual TabelaTipoImovel TabelaTipoImovel { get; set; }
        public virtual ICollection<TabelaBancoTipoOperacaoEmpreend> TabelaBancoTipoOperacaoEmpreends { get; set; }
    }
}
