using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ClienteDocumento : BaseEntity
    {
       //  public int Cod { get; set; }
         public string NumeroDocumento { get; set; }
         public Nullable<short> Tipo { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<System.DateTime> DataExpedicao { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}
