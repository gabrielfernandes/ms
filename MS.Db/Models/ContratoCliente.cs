using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ContratoCliente : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<int> CodContrato { get; set; }
         public Nullable<short> Tipo { get; set; }
         public Nullable<int> Ordem { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Contrato Contrato { get; set; }
    }
}
