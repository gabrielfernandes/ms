using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ClienteParcelaCredito : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodCliente { get; set; }
         public Nullable<int> CodContrato { get; set; }
         public string NumeroDocumento { get; set; }
         public string FluxoPagamento { get; set; }
         public Nullable<System.DateTime> DataPagamento { get; set; }
         public Nullable<decimal> ValorRecebido { get; set; }
         public string Observacao { get; set; }
         public Nullable<short> Status { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public string GuidImportacao { get; set; }
         public Nullable<int> CodImportacao { get; set; }
         public Nullable<System.DateTime> DataImportacao { get; set; }
        public Nullable<decimal> SaldoCredito { get; set; }
        public string Descricao { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Contrato Contrato { get; set; }
    }
}
