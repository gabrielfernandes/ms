using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaEndereco : BaseEntity
    {
        public TabelaEndereco()
        {
            this.ClienteEnderecoes = new List<ClienteEndereco>();
            this.TabelaConstrutoras = new List<TabelaConstrutora>();
            this.TabelaEmpreendimentoes = new List<TabelaEmpreendimento>();
            this.TabelaEscritorios = new List<TabelaEscritorio>();
            this.TabelaRegionals = new List<TabelaRegional>();
        }

       //  public int Cod { get; set; }
         public string Logradouro { get; set; }
         public string Endereco { get; set; }
         public string Numero { get; set; }
         public string Complemento { get; set; }
         public string Bairro { get; set; }
         public string Cidade { get; set; }
         public string Estado { get; set; }
         public string UF { get; set; }
         public string CEP { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<int> CodImportacao { get; set; }
         public Nullable<System.DateTime> DataImportacao { get; set; }
        public virtual ICollection<ClienteEndereco> ClienteEnderecoes { get; set; }
        public virtual ICollection<TabelaConstrutora> TabelaConstrutoras { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes { get; set; }
        public virtual ICollection<TabelaEscritorio> TabelaEscritorios { get; set; }
        public virtual ICollection<TabelaRegional> TabelaRegionals { get; set; }
    }
}
