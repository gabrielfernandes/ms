using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaMuralAviso : BaseEntity
    {
        public TabelaMuralAviso()
        {
            this.TabelaMuralAvisoDestinatarios = new List<TabelaMuralAvisoDestinatario>();
        }

       //  public int Cod { get; set; }
         public Nullable<System.DateTime> DataEnvio { get; set; }
         public Nullable<bool> EnviarEmail { get; set; }
         public string TituloAviso { get; set; }
         public string TextoAviso { get; set; }
         public Nullable<short> Tipo { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<TabelaMuralAvisoDestinatario> TabelaMuralAvisoDestinatarios { get; set; }
    }
}
