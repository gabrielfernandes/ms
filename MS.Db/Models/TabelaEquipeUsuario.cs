using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaEquipeUsuario : BaseEntity
    {
        public TabelaEquipeUsuario()
        {
            this.TabelaParametroAtendimentoUsuarios = new List<TabelaParametroAtendimentoUsuario>();
        }

       //  public int Cod { get; set; }
         public Nullable<int> CodEquipe { get; set; }
         public Nullable<int> CodUsuario { get; set; }
       //  public Nullable<System.DateTime> DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual TabelaEquipe TabelaEquipe { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<TabelaParametroAtendimentoUsuario> TabelaParametroAtendimentoUsuarios { get; set; }
    }
}
