using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaEstado : BaseEntity
    {
       //  public int Cod { get; set; }
         public string Estado { get; set; }
         public string Sigla { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
    }
}
