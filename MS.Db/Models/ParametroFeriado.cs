using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models
{
    public partial class ParametroFeriado : BaseEntity
    {
        public ParametroFeriado()
        {
        }
        
        //   public int Cod { get; set; }
        public Nullable<DateTime> Data { get; set; }
        public string Feriado { get; set; }
        public Nullable<bool> Ativo { get; set; }
        //  public System.DateTime DataCadastro { get; set; }
        //  public Nullable<System.DateTime> DataExcluido { get; set; }
        //  public Nullable<System.DateTime> DataAlteracao { get; set; }
    }
}
