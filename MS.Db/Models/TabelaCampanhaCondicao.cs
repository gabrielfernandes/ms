using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaCampanhaCondicao : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodCampanha { get; set; }
         public Nullable<short> Tipo { get; set; }
         public Nullable<short> Condicao { get; set; }
         public string Valor { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
         public Nullable<bool> Excecao { get; set; }
        public virtual TabelaCampanha TabelaCampanha { get; set; }
    }
}
