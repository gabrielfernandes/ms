using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class TabelaTipoImovel : BaseEntity
    {
        public TabelaTipoImovel()
        {
            this.TabelaBancoTipoOperacaos = new List<TabelaBancoTipoOperacao>();
            this.TabelaEmpreendimentoes = new List<TabelaEmpreendimento>();
        }

       //  public int Cod { get; set; }
         public string TipoImovel { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual ICollection<TabelaBancoTipoOperacao> TabelaBancoTipoOperacaos { get; set; }
        public virtual ICollection<TabelaEmpreendimento> TabelaEmpreendimentoes { get; set; }
    }
}
