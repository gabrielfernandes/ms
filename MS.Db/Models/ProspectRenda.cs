using System;
using System.Collections.Generic;

namespace Cobranca.Db.Models 
{
    public partial class ProspectRenda : BaseEntity
    {
       //  public int Cod { get; set; }
         public Nullable<int> CodProspect { get; set; }
         public Nullable<short> TipoRenda { get; set; }
         public string Cargo { get; set; }
         public Nullable<System.DateTime> DataAdmissao { get; set; }
         public string TelefoneComercial { get; set; }
         public Nullable<decimal> ValorBruto { get; set; }
         public string NomeEmpresa { get; set; }
         public string CNPJ { get; set; }
         public string Cep { get; set; }
         public Nullable<short> EnderecoTipo { get; set; }
         public string Endereco { get; set; }
         public string EnderecoNumero { get; set; }
         public string EnderecoComplmento { get; set; }
         public string Bairro { get; set; }
         public string Cidade { get; set; }
         public string UF { get; set; }
         public Nullable<short> DocumentoComprobatorio { get; set; }
       //  public System.DateTime DataCadastro { get; set; }
       //  public Nullable<System.DateTime> DataExcluido { get; set; }
       //  public Nullable<System.DateTime> DataAlteracao { get; set; }
        public virtual PropostaProspect PropostaProspect { get; set; }
    }
}
