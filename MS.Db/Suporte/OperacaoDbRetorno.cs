﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Suporte
{
    public class OperacaoDbRetorno
    {
        public bool OK { get; set; }
        public object Dados { get; set; }
        public string Mensagem { get; set; }

        public OperacaoDbRetorno()
        {
            OK = true;
        }

        public OperacaoDbRetorno(Exception ex)
        {
            OK = false;
            Dados = ex;
            Mensagem = ex.Message;
        }
    }

    public class ImportacaoRetorno
    {
        public OperacaoDbRetorno ImportCliente { get; set; }
        public OperacaoDbRetorno ImportClienteEndereco { get; set; }
        public OperacaoDbRetorno ImportParcela { get; set; }
        public OperacaoDbRetorno ImportBoleto { get; set; }
        public OperacaoDbRetorno ImportCredito { get; set; }
    }
}
