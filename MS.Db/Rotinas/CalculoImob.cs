﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cobranca.Db.Rotinas
{
    public static class CalculoImob
    {
        public static decimal CalcularCapacidadeFinanciamento(
            decimal valorRenda,
            DateTime dataNascimento,
            int idadeMaxima,
            int numeroParcelas,
            decimal taxaJuros,
            decimal percentualComprometimentoRenda,
            decimal percentualFinanciamentoRepasse)
        {

            if (valorRenda == 0)
                return 0;

            decimal valor = 0;
            int idade = CalcularIdade(dataNascimento);
            int prazoDisponivel = (idadeMaxima - idade) * 12;
            int prazoFinal = prazoDisponivel > numeroParcelas ? numeroParcelas : prazoDisponivel;
            decimal valorComprometimentoRenda = CalcularPercentualComprometimentoRenda(valorRenda, percentualComprometimentoRenda); ;

            decimal taxaJurosMensal = (taxaJuros / 100m) / 12m;
            decimal taxaAdministracao = 30m;
            decimal seguroMorteInvalidez = 0.531m / 100;//verificar a formula

            if (valorRenda > 2790)
            {
                valorComprometimentoRenda = valorComprometimentoRenda - taxaAdministracao;
            }
            valorComprometimentoRenda = valorComprometimentoRenda / (1m + seguroMorteInvalidez);
            valorComprometimentoRenda = valorComprometimentoRenda - taxaAdministracao;

            valor = valorComprometimentoRenda / (taxaJurosMensal + (1m / prazoFinal));
            if (percentualFinanciamentoRepasse > 0)
            {
                valor = valor * (percentualFinanciamentoRepasse / 100);
            }
            return valor;
        }

        public static decimal CalcularPercentualComprometimentoRenda(decimal valorRenda, decimal percentualComprometimentoRenda)
        {
            return valorRenda * (percentualComprometimentoRenda / 100);
        }

        public static int CalcularIdade(DateTime dataNascimento)
        {
            int anos = DateTime.Now.Year - dataNascimento.Year;
            if (DateTime.Now.Month < dataNascimento.Month || (DateTime.Now.Month == dataNascimento.Month && DateTime.Now.Day < dataNascimento.Day))
                anos--;
            return anos;
        }

        public static object CalcularSubsidioCAIXA(int qtdCompradores, decimal salario, decimal valorMaximoSubsidio)
        {
            if (salario <= 1800)
            {
                return valorMaximoSubsidio;
            }
            else if (salario < 2349.99m)
            {
                return valorMaximoSubsidio - ((valorMaximoSubsidio - 11200.0m) * (salario - 1800)) / 550m;
            }
            else if (salario < 2789.99m)
            {
                return 11200.0m - ((11200.0m - 4640.0m) * (salario - 2350)) / 440m;
            }
            else if (salario < 3274.99m)
            {
                return 4640.0m - ((4640.0m - 2405.0m) * (salario - 2790)) / 485m;
            }
            else
                return 0;
        }
    }
}
