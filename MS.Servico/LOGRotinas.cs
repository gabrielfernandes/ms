﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;

namespace Cobranca.Servico
{

    public static class LOGRotinas
    {


        public static void GravarLogTXT(string mensagem)
        {
            try
            {
                string caminhoPasta = ConfigurationSettings.AppSettings["CaminhoArquivoLog"];
                string caminhoArquivoLog = ConfigurationSettings.AppSettings["CaminhoArquivoLog"] + "(" + DateTime.Now.ToString("dd-MM-yyyy") + ")" + ConfigurationSettings.AppSettings["NomeArquivo"];

                try
                {

                    if (!Directory.Exists(caminhoPasta))
                        Directory.CreateDirectory(caminhoPasta);
                }
                catch { }

                FileStream fs = null;
                if (!File.Exists(caminhoArquivoLog))
                    fs = new FileStream(caminhoArquivoLog, FileMode.OpenOrCreate, FileAccess.Write);
                else if (File.Exists(caminhoArquivoLog) && File.GetLastWriteTime(caminhoArquivoLog) < DateTime.Today)
                    fs = new FileStream(caminhoArquivoLog, FileMode.OpenOrCreate, FileAccess.Write);
                else
                    fs = new FileStream(caminhoArquivoLog, FileMode.Append);

                StreamWriter sw = new StreamWriter(fs);

                sw.WriteLine(String.Format("{0} - {1}", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), mensagem));

                sw.Close();
                fs.Close();
            }
            catch { }
        }

        /// <summary>
        /// Método registrar o log no arquivo parametrizado no app.config
        /// </summary>
        /// <param name="LogServico">classe com Mensagem</param>
        /// <returns>static.</returns>
        public static LogServico GravarLogServico(LogServico _log)
        {
            try
            {
                //REGISTRA NO ARQUIVO TXT
                GravarLogTXT(_log.Mensagem);
                if (_log.DataInicio == DateTime.MinValue || _log.DataInicio == null) { _log.DataInicio = DateTime.Now; }
                //REGISTRA NO LOG EMAIL

                LogServico log = Activator.CreateInstance<LogServico>();
                log.CodTipoCobrancaAcao = _log.CodTipoCobrancaAcao;
                log.CodImportacao = _log.CodImportacao;

                #region tratar log importação
                var tp = (Enumeradores.TipoImportacao)(_log.TipoImportacao ?? 0);
                switch (tp)
                {
                    case Enumeradores.TipoImportacao.Cliente:
                        _log.Mensagem = $"Importação Cliente - {_log.Mensagem}";
                        break;
                    case Enumeradores.TipoImportacao.Parcela:
                        _log.Mensagem = $"Importação Parcela - {_log.Mensagem}";
                        break;
                    case Enumeradores.TipoImportacao.Credito:
                        _log.Mensagem = $"Importação Credito - {_log.Mensagem}";
                        break;
                    case Enumeradores.TipoImportacao.Boleto:
                        _log.Mensagem = $"Importação Boleto - {_log.Mensagem}";
                        break;
                    case Enumeradores.TipoImportacao.ClienteEndereco:
                        _log.Mensagem = $"Importação ClienteEndereco - {_log.Mensagem}";
                        break;
                    default:
                        break;
                }
                #endregion
                log.TipoServico = _log.TipoServico;
                log.TipoImportacao = _log.TipoImportacao;
                log.Arquivos = _log.Arquivos;
                log.DataInicio = _log.DataInicio;
                log.DataFim = _log.DataFim;
                log.Mensagem = _log.Mensagem;

                Repository<LogServico> _dbLogServico = new Repository<LogServico>();
                var entity =  _dbLogServico.CreateOrUpdate(log);
                entity.CodImportacao = null;
                System.Threading.Thread.Sleep(500);

                return entity;
            }
            catch (Exception ex)
            {
                GravarLogTXT($"Erro ao gravar historico: {ex.Message}");
            }
            return _log;
        }

        public static void GravarArquivo(string texto, string caminhoPasta, string caminhoArquivoLog)
        {
            try
            {
                caminhoArquivoLog = Path.Combine(caminhoPasta, caminhoArquivoLog);
                try
                {
                    if (!Directory.Exists(caminhoPasta))
                        Directory.CreateDirectory(caminhoPasta);
                }
                catch { }

                FileStream fs = null;
                if (!File.Exists(caminhoArquivoLog))
                    fs = new FileStream(caminhoArquivoLog, FileMode.OpenOrCreate, FileAccess.Write);
                else if (File.Exists(caminhoArquivoLog) && File.GetLastWriteTime(caminhoArquivoLog) < DateTime.Today)
                    fs = new FileStream(caminhoArquivoLog, FileMode.OpenOrCreate, FileAccess.Write);
                else
                    fs = new FileStream(caminhoArquivoLog, FileMode.Append);

                StreamWriter sw = new StreamWriter(fs);

                sw.WriteLine(texto);

                sw.Close();
                fs.Close();
            }
            catch(Exception ex) {
                throw new Exception("Erro ao escrever as linhas do arquivo", ex);
            }
        }
       public static void RemoveLinhasEmBranco(string caminhoPasta, string caminhoArquivoLog)
        {
            var tempFileName = Path.GetTempFileName();
            try
            {
                caminhoArquivoLog = Path.Combine(caminhoPasta, caminhoArquivoLog);

                using (var streamReader = new StreamReader(caminhoArquivoLog))
                using (var streamWriter = new StreamWriter(tempFileName))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        if (!string.IsNullOrWhiteSpace(line) || line != "")
                            streamWriter.WriteLine(line);
                    }
                    streamReader.Close();
                    streamWriter.Close();
                }
                File.Copy(tempFileName, caminhoArquivoLog, true);
               
            }
            finally
            {
                File.Delete(tempFileName);
            }
        }


    }
}
