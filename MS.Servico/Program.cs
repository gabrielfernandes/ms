﻿using Cobranca.Domain.ServicoContext.Handlers;
using Cobranca.Domain.ServicoContext.Repositories;
using Cobranca.Infra;
using Cobranca.Infra.ServicoContext;
using Cobranca.Infra.ServicoContext.Repositories;
using SimpleInjector;
using System;
using System.Configuration;
using System.Globalization;
using System.ServiceProcess;

namespace Cobranca.Servico
{

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //CONFIGURA A CULTURA DO SERVICO - DATA /VALOR ETC .... 
            var _language = ConfigurationManager.AppSettings["DefaultCulture"];
            if (String.IsNullOrEmpty(_language))
            {
                _language = "pt-BR";
            }
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo(_language);


            DIContainer.container = new Container();
            DIContainer.RegisterDependencies(DIContainer.container);


            //var container = new Container();
            //container.Register<DbContext>(Lifestyle.Singleton);
            //container.Register<IClienteEnderecoRepository, ClienteEnderecoRepository>(Lifestyle.Singleton);
            //container.Register<IContratoRepository, ContratoRepository>(Lifestyle.Singleton);
            //container.Register<ClienteEnderecoHandlers, ClienteEnderecoHandlers>(Lifestyle.Singleton);
            //container.Register<LogHandlers, LogHandlers>(Lifestyle.Singleton);
            //container.Register<ImportResultHandlers, ImportResultHandlers>(Lifestyle.Singleton);

            DIContainer.container.Verify();

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Service1(DIContainer.container.GetInstance<ClienteEnderecoRepository>(),
                    DIContainer.container.GetInstance<ClienteEnderecoHandlers>(),
                    DIContainer.container.GetInstance<LogHandlers>(),
                    DIContainer.container.GetInstance<ImportResultHandlers>()
                    )
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
