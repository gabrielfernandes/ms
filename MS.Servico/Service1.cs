﻿using Cobranca.Db.Models;
using Cobranca.Db.Repositorio;
using Cobranca.Db.Rotinas;
using Cobranca.Db.Suporte;
using Cobranca.Domain.ServicoContext.Handlers;
using Cobranca.Domain.ServicoContext.Commands.LogCommands.Inputs;
using Cobranca.Infra.ServicoContext.Repositories;
using Cobranca.Notificacao.EMAIL;
using Cobranca.Rotinas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Cobranca.Domain.ServicoContext.Commands.ClienteEnderecoCommands.Inputs;
using Cobranca.Domain.ServicoContext.Entities;
using Cobranca.Domain.ServicoContext.Commands.ImportacaoResultadoCommands;

namespace Cobranca.Servico
{
    public partial class Service1 : ServiceBase
    {
        public Repository<LogEmail> _dbLogEmail = new Repository<LogEmail>();
        public Repository<ContratoAcaoCobrancaHistorico> _dbContratoAcaoCobrancaHistorico = new Repository<ContratoAcaoCobrancaHistorico>();
        public Repository<Cliente> _dbCliente = new Repository<Cliente>();
        public Repository<ParametroPlataforma> _dbParametro = new Repository<ParametroPlataforma>();
        public Repository<TabelaBancoPortador> _dbParametroBoleto = new Repository<TabelaBancoPortador>();
        public Repository<ContratoParcelaBoleto> _dbBoleto = new Repository<ContratoParcelaBoleto>();
        public Cobranca.Data.Services.BoletoDapper _boletoDapper = new Cobranca.Data.Services.BoletoDapper();
        public Repository<TabelaBancoPortadorLogArquivo> _dbBancoPortadorLogArquivo = new Repository<TabelaBancoPortadorLogArquivo>();

        public Dados _qry = new Dados();
        private EmailRotinas _objEmailRotinas;
        private ParametroPlataforma _objParametro;
        private ReguaRotinas _objReguaRotinas = new ReguaRotinas();
        private DateTime _proximoProcessamentoDiarioData;
        private DateTime _proximoProcessamentoRemessaDiarioData;
        private DateTime _proximoProcessamentoExtracaoClassificacaoDiarioData;
        private string _proximoProcessamentoHora = "06:00", _proximoProcessamentoRemessaHora = "9:00", _proximoProcessamentoExtracaoClassificacao = "20:00";

        private bool _servicoQA = true;
        private bool _processaImportacao = true, _processaDiario = false, _processaRemessaDiario = false, _processaClassificacao = false, _processaEmail = false;
        private string _emailQA = String.Empty;//"rodrigo.correia@junix.com.br";//;gabriel@junix.com.br;alessandro.Raquella@otis.com;
        private string _emailErro = "gabriel@junix.com.br;rodrigo.correia@junix.com.br;Alessandro.Raquella@otis.com";
        private System.Timers.Timer _tm, _tmEmail;
        private System.Timers.Timer _tmDiario = new System.Timers.Timer(20000), _tmDiarioRemessa = new System.Timers.Timer(30000), _tmDiarioExtracaoClassificacao = new System.Timers.Timer(30000);
        private long _tamanhoArquvioAntesCliente = 0;

        Hashtable _tbMonitorArquivo = new Hashtable();

        //static Mutex mutexImportacao = new Mutex(false, "Servico_Importacao");
        static readonly object _lockImportacao = new object();
        private bool _lockTaken = false;

        RotinaImportacao importacao = new RotinaImportacao();
        string CaminhoSwap = ConfigurationSettings.AppSettings["CaminhoArquivoSwap"];


        public Service1()
        {
            InitializeComponent();
        }

        private readonly ClienteEnderecoRepository _clienteEnderecoRepository;
        private readonly ClienteEnderecoHandlers _clienteEnderecoHandlers;
        private readonly LogHandlers _logHandler;
        private readonly ImportResultHandlers _importResultHandlers;

        public Service1(
            ClienteEnderecoRepository clienteEnderecoRepository,
            ClienteEnderecoHandlers clienteEnderecoHandler,
            LogHandlers logHandler,
            ImportResultHandlers importResultHandlers)
        {
            _clienteEnderecoRepository = clienteEnderecoRepository;
            _clienteEnderecoHandlers = clienteEnderecoHandler;
            _logHandler = logHandler;
            _importResultHandlers = importResultHandlers;
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            LogServico _log = new LogServico();
            try
            {
                _objParametro = _dbParametro.FindAll().FirstOrDefault();
                if (_objParametro == null)
                {
                    _logHandler.Handle(new RegisterLogCommand("Parametro ausente.. Servico nao iniciado"));
                }
                else
                {
                    _logHandler.Handle(new RegisterLogCommand($"Servico iniciado ... {CultureInfo.CurrentCulture.Name}"));
                    _logHandler.Handle(new RegisterLogCommand($"Configurando o serviço : Importacao: {_processaImportacao} | Processo Diários: {_processaDiario}  | Remessa: {_processaRemessaDiario} | Classificação: { _processaClassificacao} | Emails: { _processaEmail}"));

                    _objEmailRotinas = new EmailRotinas(_objParametro.SMTPPorta, _objParametro.SMTPServidor, _objParametro.SMTPUsuario, _objParametro.SMTPSenha, _objParametro.fgSSL == true);

                    IniciarTimerImportacao();
                    IniciarTimerDiario();
                    IniciarTimerDiarioRemessa();
                    IniciarTimerDiarioExtracaoClassificacao();
                    IniciarTimerEmail();
                }

            }
            catch (Exception ex)
            {
                LOGRotinas.GravarLogTXT(ex.Message);
            }

        }

        private void IniciarTimerDiario()
        {
            #region configurar time diario

            var now = DateTime.Now;
            int hora = 6; //padrão
            int minuto = 00; //padrão

            var arrMinuto = _proximoProcessamentoHora.Split(':');

            if (arrMinuto.Length > 0)
            {
                minuto = DbRotinas.ConverterParaInt(arrMinuto[0]);
                minuto = DbRotinas.ConverterParaInt(arrMinuto[1]);
            }

            _proximoProcessamentoDiarioData = new DateTime(now.Year, now.Month, now.Day, hora, minuto, 0);
            #endregion

            _tmDiario = new System.Timers.Timer(300000);//5 minutos
            _tmDiario.Elapsed += new System.Timers.ElapsedEventHandler(ExecutarProcessamentoDiario);
            _tmDiario.Start();
        }
        private void IniciarTimerDiarioRemessa()
        {

            #region configurar time diario da remessa
            var now = DateTime.Now;
            int hora = 9; //padrão
            int minuto = 00; //padrão
            var arrMinuto = _proximoProcessamentoRemessaHora.Split(':');

            if (arrMinuto.Length > 0)
            {
                minuto = DbRotinas.ConverterParaInt(arrMinuto[0]);
                minuto = DbRotinas.ConverterParaInt(arrMinuto[1]);
            }
            _proximoProcessamentoRemessaDiarioData = new DateTime(now.Year, now.Month, now.Day, hora, minuto, 0);
            #endregion

            _tmDiarioRemessa = new System.Timers.Timer(180000);//3 minutos
            _tmDiarioRemessa.Elapsed += new System.Timers.ElapsedEventHandler(ExecutarProcessamentoDiarioRemessa);
            _tmDiarioRemessa.Start();
        }

        private void IniciarTimerDiarioExtracaoClassificacao()
        {
            #region configurar time diario da extração de classificações
            var now = DateTime.Now;
            int hora = 20; //padrão
            int minuto = 00; //padrão
            var arrMinuto = _proximoProcessamentoRemessaHora.Split(':');

            if (arrMinuto.Length > 0)
            {
                minuto = DbRotinas.ConverterParaInt(arrMinuto[0]);
                minuto = DbRotinas.ConverterParaInt(arrMinuto[1]);
            }
            _proximoProcessamentoExtracaoClassificacaoDiarioData = new DateTime(now.Year, now.Month, now.Day, hora, minuto, 0);
            #endregion

            _tmDiarioExtracaoClassificacao = new System.Timers.Timer(180000);//3 minutos
            _tmDiarioExtracaoClassificacao.Elapsed += new System.Timers.ElapsedEventHandler(ExecutarProcessamentoDiarioExportarClassificacao);
            _tmDiarioExtracaoClassificacao.Start();
        }

        private void IniciarTimerEmail()
        {
            _tmEmail = new System.Timers.Timer(10000);//10 segundos
            _tmEmail.Elapsed += new System.Timers.ElapsedEventHandler(ExecutarEmail);
            _tmEmail.Start();
        }

        private void IniciarTimerImportacao()
        {
            _tm = new System.Timers.Timer(60000);// 1 minuto
            _tm.Elapsed += new System.Timers.ElapsedEventHandler(ExecutarImportacao);
            _tm.Start();
        }

        private void ExecutarImportacao(object sender, ElapsedEventArgs e)
        {
            _tm.Stop();
            try
            {
                Monitor.Enter(_lockImportacao, ref _lockTaken);
                if (_processaImportacao)
                {
                    Importacao();
                }
            }
            catch (Exception ex)
            {
                _tm = new System.Timers.Timer(3600000);//1 HORA

                _logHandler.Handle(new RegisterLogCommand(ex));
                _logHandler.Handle(new RegisterLogCommand("Próxima tentativa sera após 60 minutos."));
                var _assunto = $"Importação - OTIS";
                var _conteudo = $"Descrição: {ex.Message} - {ex.InnerException}";
                EnviarLogPorEmail(_assunto, _conteudo);
            }
            finally
            {

                _tm.Start();
                // mutexImportacao = new Mutex(false, "Servico_Importacao");
                if (_lockTaken)
                {
                    Monitor.Exit(_lockImportacao);
                    _lockTaken = false;
                };
            }
        }

        private void ExecutarProcessamentoDiario(object sender, ElapsedEventArgs e)
        {
            //_tm.Stop();
            _tmDiario.Stop();
            //_tmDiarioRemessa.Stop();
            //_tmDiarioExtracaoClassificacao.Stop();

            try
            {
                if (DateTime.Now > _proximoProcessamentoDiarioData && _processaDiario)
                {
                    //VERIFICA SE EXISTE UMA IMPORTAÇÂO ESTA RODANDO.
                    if (_lockTaken)
                    {
                        LOGRotinas.GravarLogTXT("Processamento Diário - Existe um processo sendo executado tentaremos mais tarde ...");
                        _tmDiario.Start();
                        return;
                    }

                    var _log = new LogServico();
                    LOGRotinas.GravarLogTXT("Processamento Diário - Iniciando o processamento ...");
                    var opGerar = _objReguaRotinas.ExecutarRegua();
                    _log = new LogServico
                    {
                        TipoServico = (short)Enumeradores.TipoServico.Regua,
                        Mensagem = opGerar.Mensagem
                    };
                    LOGRotinas.GravarLogServico(_log);

                    var opEventosEmail = _objReguaRotinas.ProcesssarEventosEmail();
                    _log = new LogServico
                    {
                        TipoServico = (short)Enumeradores.TipoServico.Emails_Disparar,
                        Mensagem = opEventosEmail.Mensagem
                    };
                    LOGRotinas.GravarLogServico(_log);

                    CancelarSolicitacaoForaDoPrazo();
                    //VALORES DIARIOS
                    ProcesssarValoresDiarios();
                    _proximoProcessamentoDiarioData = _proximoProcessamentoDiarioData.AddDays(1);

                    _log = new LogServico
                    {
                        TipoServico = (short)Enumeradores.TipoServico.Finalizar_Servico,
                        Mensagem = $"Processamento Diário - Processo finalizado, proximo processamento {_proximoProcessamentoDiarioData:dd/MM/yyyy HH:mm}"
                    };
                    LOGRotinas.GravarLogServico(_log);
                }
            }
            catch (Exception ex)
            {
                LOGRotinas.GravarLogTXT("Erro processamento diario");
                LOGRotinas.GravarLogTXT(ex.Message);
            }
            finally
            {
                //_tm.Stop();
                //_tm.Start();
                _tmDiario.Start();
                //_tmDiarioRemessa.Start();
                //_tmDiarioExtracaoClassificacao.Start();
            }

        }
        private void ExecutarProcessamentoDiarioRemessa(object sender, ElapsedEventArgs e)
        {
            //_tm.Stop();
            //_tmDiario.Stop();
            _tmDiarioRemessa.Stop();
            //_tmDiarioExtracaoClassificacao.Stop();
            try
            {
                if (DateTime.Now > _proximoProcessamentoRemessaDiarioData && _processaRemessaDiario)
                {
                    TransmitirArquivoRemessa();
                    _proximoProcessamentoRemessaDiarioData = _proximoProcessamentoRemessaDiarioData.AddDays(1);
                    var _log = new LogServico
                    {
                        TipoServico = (short)Enumeradores.TipoServico.Finalizar_Servico,
                        Mensagem = $"Remessa - Processamento dos arquivos remessas finalizado, proximo processamento {_proximoProcessamentoRemessaDiarioData:dd/MM/yyyy HH:mm}"
                    };
                    LOGRotinas.GravarLogServico(_log);
                }
            }
            catch (Exception ex)
            {
                LOGRotinas.GravarLogTXT("Erro processamento diario");
                LOGRotinas.GravarLogTXT(ex.Message);
            }
            finally
            {
                //_tm.Stop();
                //_tm.Start();
                //_tmDiario.Start();
                _tmDiarioRemessa.Start();
                //_tmDiarioExtracaoClassificacao.Start();
            }
        }
        private void ExecutarProcessamentoDiarioExportarClassificacao(object sender, ElapsedEventArgs e)
        {
            //_tm.Stop();
            //_tmDiario.Stop();
            //_tmDiarioRemessa.Stop();
            _tmDiarioExtracaoClassificacao.Stop();
            try
            {
                if (DateTime.Now > _proximoProcessamentoExtracaoClassificacaoDiarioData && _processaClassificacao)
                {
                    var opEventosExtracao = ProcesssarExtracaoDeClassificacao();
                    _proximoProcessamentoExtracaoClassificacaoDiarioData = _proximoProcessamentoExtracaoClassificacaoDiarioData.AddDays(1);
                    var _log = new LogServico
                    {
                        TipoServico = (short)Enumeradores.TipoServico.Finalizar_Servico,
                        Mensagem = $"Exportação de Classificação - Processo finalizado, proximo processamento {_proximoProcessamentoExtracaoClassificacaoDiarioData:dd/MM/yyyy HH:mm}"
                    };
                    LOGRotinas.GravarLogServico(_log);
                }

            }
            catch (Exception ex)
            {
                LOGRotinas.GravarLogTXT($"Erro ao processar as classificações: {ex.Message}");
                LOGRotinas.GravarLogTXT(ex.Message);
            }
            finally
            {
                //_tm.Stop();
                //_tm.Start();
                //_tmDiario.Start();
                //_tmDiarioRemessa.Start();
                _tmDiarioExtracaoClassificacao.Start();
            }
        }
        private void ExecutarEmail(object sender, ElapsedEventArgs e)
        {
            _tmEmail.Stop();
            try
            {
                if (_processaEmail)
                {
                    EnvioEmail();
                }
            }
            catch (Exception ex)
            {
                LOGRotinas.GravarLogTXT("Erro Evento de Email");
                LOGRotinas.GravarLogTXT(ex.Message);
            }
            finally
            {
                _tmEmail.Start();
            }
        }
        private void EnvioEmail()
        {
            int count = 0;
            var all = _dbLogEmail.FindAll(x => x.Status == (short)Enumeradores.LogEmailStatus.AguardandoEnvio && (x.TentativaEnvio ?? 0) <= 3).ToList().Take(10);

            //aqui envia
            foreach (var item in all)
            {
                count++;

                int tentativas = item.TentativaEnvio ?? 0;

                item.TentativaEnvio = tentativas + 1;

                item.EmailDestinatario = item.EmailDestinatario;

                var NotificacaoRetorno = _objEmailRotinas.Enivar(new Notificacao.Email.EmailInfo()
                {
                    Assunto = item.Assunto,
                    Conteudo = item.Conteudo,
                    EmailRemetente = item.EmailRemetente,
                    EmailDestinatario = item.EmailDestinatario,
                    NomeDestinatario = item.NomeDestinatario,
                    NomeRemetente = item.NomeRemetente,
                    HTML = true
                });
                if (NotificacaoRetorno.OK)
                {
                    item.Status = (short)Enumeradores.LogEmailStatus.Enviado;
                }
                else
                {
                    item.Mensagem = NotificacaoRetorno.Mensagem;
                    item.Status = (short)Enumeradores.LogEmailStatus.FalhaNoEnvio;
                }

                _dbLogEmail.CreateOrUpdate(item);

            }
            if (count > 0)
            {
                LOGRotinas.GravarLogTXT($"{count} e-mails enviados.");
            }

        }
        private void Importacao()
        {
            ImportacaoRetorno retorno = new ImportacaoRetorno()
            {
                ImportCliente = new OperacaoDbRetorno() { OK = false },
                ImportClienteEndereco = new OperacaoDbRetorno() { OK = false },
                ImportParcela = new OperacaoDbRetorno() { OK = false },
                ImportCredito = new OperacaoDbRetorno() { OK = false },
                ImportBoleto = new OperacaoDbRetorno() { OK = false }
            };
            retorno.ImportCliente = ImportacaoCliente();
            if (retorno.ImportCliente.OK)
            {
                retorno.ImportParcela = ImportacaoParcela();
                retorno.ImportClienteEndereco = ImportacaoClienteEndereco();
                retorno.ImportCredito = ImportacaoCredito();
            }
            if (retorno.ImportParcela.OK)
            {
                retorno.ImportBoleto = ImportacaoBoleto();
            }
        }
        private bool MonitorarArquivoEmUso(FileInfo arquivo, Enumeradores.TipoImportacao tpImportacao)
        {
            if (!_tbMonitorArquivo.ContainsKey(arquivo.Name))
            {
                _tbMonitorArquivo.Add(arquivo.Name, arquivo.Length);
                return true;
            }
            else
            {
                long tamanhoAnterior = DbRotinas.ConverterParaLong(_tbMonitorArquivo[arquivo.Name]);
                if (arquivo.Length != tamanhoAnterior)
                {
                    _tbMonitorArquivo[arquivo.Name] = arquivo.Length;
                    _logHandler.Handle(new RegisterLogCommand($"Arquivo não está pronto, aguardando finalizar a cópia. Tamnho anterior e {tamanhoAnterior}bytes e o atual é {arquivo.Length}bytes."));
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }
        private OperacaoDbRetorno ImportacaoCliente()
        {
            LogServico _log = new LogServico();
            string CaminhoPendente = ConfigurationSettings.AppSettings["CaminhoArquivoClientePendente"];
            string CaminhoProcessado = ConfigurationSettings.AppSettings["CaminhoArquivoClienteProcessado"];
            string CaminhoSwapCliente = ConfigurationSettings.AppSettings["CaminhoArquivoClienteSwap"];

            try
            {
                DirectoryInfo pendente = new DirectoryInfo(CaminhoPendente);
                FileInfo[] Files = pendente.GetFiles();
                OperacaoDbRetorno retorno = new OperacaoDbRetorno();

                foreach (var arquivo in Files)
                {

                    if (MonitorarArquivoEmUso(arquivo, Enumeradores.TipoImportacao.Cliente)) continue;

                    if (!arquivo.IsReadOnly)
                    {

                        _log = new LogServico
                        {
                            TipoServico = (short)Enumeradores.TipoServico.Importacao,
                            Mensagem = $"Importacao Cliente - Arquivo encontrado de ({arquivo.Name}) que não esteja no modo somente leitura ...",
                            TipoImportacao = (short)Enumeradores.TipoImportacao.Cliente // (TIPO 1 = CLIENTE)
                        };
                        LOGRotinas.GravarLogServico(_log);

                        importacao.MoverFile(arquivo.Name, arquivo.FullName, CaminhoSwapCliente, "");
                        DirectoryInfo Swap = new DirectoryInfo(CaminhoSwapCliente);

                        _log = new LogServico
                        {
                            TipoServico = (short)Enumeradores.TipoServico.Importacao,
                            Mensagem = $"Arquivo foi movido para o diretório ({Swap.Name})",
                            TipoImportacao = (short)Enumeradores.TipoImportacao.Cliente // (TIPO 1 = CLIENTE)
                        };
                        LOGRotinas.GravarLogServico(_log);

                        FileInfo[] Arquivos = Swap.GetFiles();
                        foreach (var processar in Arquivos)
                        {
                            try
                            {
                                _log = new LogServico { TipoServico = _log.TipoServico, TipoImportacao = (short)Enumeradores.TipoImportacao.Cliente, Mensagem = $"Inicio do carregamento do arquivo de ({arquivo.Name}) para o banco!" };
                                _log = LOGRotinas.GravarLogServico(_log);

                                var CodRetorno = importacao
                                                    .CarregarArquivoParaBanco(processar.Name, processar.FullName, 1);//COLA NO BANCO OS DADOS DO ARQUIVO (TIPO 1 = CLIENTE)

                                _log = new LogServico
                                {
                                    TipoServico = _log.TipoServico,
                                    TipoImportacao = (short)Enumeradores.TipoImportacao.Cliente,
                                    Mensagem = $"Carregamento finalizado, processando arquivo ...",
                                    CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                };
                                _log = LOGRotinas.GravarLogServico(_log);

                                retorno = importacao
                                                .ProcessaCliente(DbRotinas.ConverterParaInt(CodRetorno));//PROCESSANDO

                                if (!retorno.OK)
                                {
                                    _log = new LogServico
                                    {
                                        TipoServico = _log.TipoServico,
                                        TipoImportacao = (short)Enumeradores.TipoImportacao.Cliente,
                                        Mensagem = $"Erro ao processar carga de parcelas erro:({retorno.Mensagem})",
                                        CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                    };
                                    LOGRotinas.GravarLogServico(_log);
                                }

                                importacao.MoverFile(processar.Name, processar.FullName, CaminhoProcessado, CodRetorno);//MOVE ARQUIVO PARA PASTA "PROCESSADOS"
                                _log = new LogServico
                                {
                                    TipoServico = _log.TipoServico,
                                    TipoImportacao = (short)Enumeradores.TipoImportacao.Cliente,
                                    Mensagem = $"Processo finalizado, movendo o arquivo para pasta processado!",
                                    CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                };
                                LOGRotinas.GravarLogServico(_log);
                                _tbMonitorArquivo.Remove(arquivo.Name);

                            }
                            catch (Exception ex)
                            {
                                string CaminhoServer = ConfigurationSettings.AppSettings["CaminhoArquivoError"];
                                if (!Directory.Exists(CaminhoServer))
                                    Directory.CreateDirectory(CaminhoServer);
                                string DestinoServidor = Path.Combine(CaminhoServer, $"Cliente{DateTime.Now:yyyyMMdd}_{DateTime.Now:HHmm}");
                                if (!Directory.Exists(DestinoServidor))
                                    Directory.CreateDirectory(DestinoServidor);

                                DirectoryInfo DirectoryI = new DirectoryInfo(Swap.FullName);
                                FileInfo[] Aqs = DirectoryI.GetFiles(arquivo.Name);
                                _tbMonitorArquivo.Remove(arquivo.Name);
                                foreach (var item in Aqs)
                                {
                                    ;
                                    importacao.MoverFile(item.Name, item.FullName, DestinoServidor, "");
                                }
                                throw;
                            }
                        }

                        _log = new LogServico { Mensagem = $"Processo Finalizado.", TipoServico = _log.TipoServico, TipoImportacao = _log.TipoImportacao };
                        LOGRotinas.GravarLogServico(_log);

                    }
                }
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Importacao Cliente - ", ex);
            }

        }
        private OperacaoDbRetorno ImportacaoClienteEndereco()
        {
            LogServico _log = new LogServico();
            string CaminhoPendente = ConfigurationSettings.AppSettings["CaminhoArquivoClienteEnderecoPendente"];
            string CaminhoProcessado = ConfigurationSettings.AppSettings["CaminhoArquivoClienteEnderecoProcessado"];
            string CaminhoSwapClienteEndereco = ConfigurationSettings.AppSettings["CaminhoArquivoClienteEnderecoSwap"];
            try
            {
                DirectoryInfo pendente = new DirectoryInfo(CreateClienteEnderecoCommand._caminhoPendente);
                FileInfo[] Files = pendente.GetFiles();
                OperacaoDbRetorno retorno = new OperacaoDbRetorno();

                foreach (var arquivo in Files)
                {
                    if (!arquivo.IsReadOnly)
                    {
                        //VERIFICAR SE O ARQUIVO ESTA CRESCENDO
                        if (MonitorarArquivoEmUso(arquivo, Enumeradores.TipoImportacao.ClienteEndereco)) continue;

                        //LOG
                        _logHandler.Handle(new RegisterLogCommand($"Arquivo ({arquivo.Name}) encontrado que não esteja no modo somente leitura!"));

                        //MOVENDO PARA PASTA SWAP
                        importacao.MoverFile(arquivo.Name, arquivo.FullName, CaminhoSwapClienteEndereco, "");
                        DirectoryInfo Swap = new DirectoryInfo(CaminhoSwapClienteEndereco);

                        //LOG
                        _logHandler.Handle(new RegisterLogCommand($"Arquivo foi movido para o diretório ({Swap.Name})"));

                        FileInfo[] Arquivos = Swap.GetFiles();
                        foreach (var processar in Arquivos)
                        {
                            try
                            {
                                //LOG
                                _logHandler.Handle(new RegisterLogCommand($"Inicio da leitura do arquivo ({arquivo.Name})"));

                                List<CreateClienteEnderecoCommand> Commands = new List<CreateClienteEnderecoCommand>();

                                //TRANSFORMAR ARQUIVO EM CLASSE
                                using (StreamReader sr = new StreamReader(processar.FullName, Encoding.Default, false, 512))
                                {
                                    string linhaHader = sr.ReadLine();// NAO NECESSITA DO HEADER

                                    var lines = sr.ReadToEnd().Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Skip(1);

                                    foreach (var item in lines)
                                    {
                                        try
                                        {
                                            var values = item.Split(';');
                                            var data = new CreateClienteEnderecoCommand
                                            {
                                                NumeroContrato = DbRotinas.Descriptografar(values[0].Trim()),
                                                An8Cob = values[1].Trim(),
                                                EnderecoCob = values[2].Trim(),
                                                NumeroCob = values[3].Trim(),
                                                ComplementoCob = values[4].Trim(),
                                                BairroCob = values[5].Trim(),
                                                CepCob = values[6].Trim(),
                                                CidadeCob = values[7].Trim(),
                                                EstadoCob = values[8].Trim(),
                                                An8Obra = values[9].Trim(),
                                                EnderecoObra = values[10].Trim(),
                                                NumeroObra = values[11].Trim(),
                                                ComplementoObra = values[12].Trim(),
                                                BairroObra = values[13].Trim(),
                                                CepObra = values[14].Trim(),
                                                CidadeObra = values[15].Trim(),
                                                EstadoObra = values[16].Trim(),
                                                MensagemContrato = values[17].Trim(),
                                                ConteudoOriginal = item
                                            };
                                            Commands.Add(data);
                                        }
                                        catch (Exception ex)
                                        {
                                            Commands.Add(new CreateClienteEnderecoCommand() { ImportStatus = "ERRO", ConteudoOriginal = item, ImportMensagem = $"{ex.Message} - {ex.InnerException}" });
                                        }
                                    }
                                }

                                _logHandler.Handle(new RegisterLogCommand($"Leitura Realizada, processando arquivo ({arquivo.Name})"));

                                //Processar o arquivo
                                var resultado = _clienteEnderecoHandlers.Handle(new ListCreateClienteEnderecoCommand(Commands));

                                //Criar Arquivo de Resultado
                                _importResultHandlers.Handle(new CreateResultFileCommand(processar.FullName, $"{processar.Name}Result.txt", (List<string>)resultado.Data));
                                
                                int CodRetorno = 1;
                                
                                //mover arquivo original
                                _importResultHandlers.Handle(new MoveResultCommand(processar.Name, processar.FullName, CaminhoProcessado, CodRetorno.ToString()));

                                //mover arquivo resultado
                                _importResultHandlers.Handle(new MoveResultCommand($"{processar.Name}Result.txt", processar.FullName, CaminhoProcessado, CodRetorno.ToString()));

                                //REMOVER arquivo do monitoramento
                                _tbMonitorArquivo.Remove(arquivo.Name);

                            }
                            catch (Exception)
                            {
                                #region Mover arquivo para Pasta com Erro
                                string CaminhoServer = ConfigurationSettings.AppSettings["CaminhoArquivoError"];
                                if (!Directory.Exists(CaminhoServer))
                                    Directory.CreateDirectory(CaminhoServer);
                                string DestinoServidor = Path.Combine(CaminhoServer, $"Endereco{DateTime.Now:yyyyMMdd}_{DateTime.Now:HHmm}");
                                if (!Directory.Exists(DestinoServidor))
                                    Directory.CreateDirectory(DestinoServidor);

                                DirectoryInfo DirectoryI = new DirectoryInfo(Swap.FullName);
                                FileInfo[] Aqs = DirectoryI.GetFiles(arquivo.Name);
                                _tbMonitorArquivo.Remove(arquivo.Name);
                                foreach (var item in Aqs)
                                {
                                    importacao.MoverFile(item.Name, item.FullName, DestinoServidor, "");
                                }
                                #endregion
                                throw;
                            }
                        }

                    }
                }
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Importacao Endereço - ", ex);
            }

        }
        private OperacaoDbRetorno ImportacaoParcela()
        {
            LogServico _log = new LogServico();
            string CaminhoPendente = ConfigurationSettings.AppSettings["CaminhoArquivoParcelaPendente"];
            string CaminhoProcessado = ConfigurationSettings.AppSettings["CaminhoArquivoParcelaProcessado"];
            string CaminhoSwapParcela = ConfigurationSettings.AppSettings["CaminhoArquivoParcelaSwap"];
            try
            {
                DirectoryInfo dinfo = new DirectoryInfo(CaminhoPendente);
                FileInfo[] Files = dinfo.GetFiles();
                OperacaoDbRetorno retorno = new OperacaoDbRetorno();

                int qtd = Files.Count();

                foreach (var arquivo in Files)
                {
                    if (!arquivo.IsReadOnly)
                    {
                        if (MonitorarArquivoEmUso(arquivo, Enumeradores.TipoImportacao.Parcela)) continue;

                        _log = new LogServico
                        {
                            TipoServico = (short)Enumeradores.TipoServico.Importacao,
                            Mensagem = $"Arquivo encontrado de contrato/parcela que não esteja somente leitura ({arquivo.Name})",
                            TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela // (TIPO 2 = CONTRATO/PARCELA)
                        };
                        LOGRotinas.GravarLogServico(_log);

                        importacao.MoverFile(arquivo.Name, arquivo.FullName, CaminhoSwapParcela, "");
                        DirectoryInfo Swap = new DirectoryInfo(CaminhoSwapParcela);
                        _log = new LogServico
                        {
                            TipoServico = (short)Enumeradores.TipoServico.Importacao,
                            Mensagem = $"Arquivo foi movido para o diretório ({Swap.Name})",
                            TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela // (TIPO 2 = CONTRATO/PARCELA)
                        };
                        LOGRotinas.GravarLogServico(_log);
                        FileInfo[] Arquivos = Swap.GetFiles();
                        foreach (var processar in Arquivos)
                        {
                            try
                            {
                                _log = new LogServico { TipoServico = _log.TipoServico, TipoImportacao = _log.TipoImportacao, Mensagem = $"Inicio do carregamento do arquivo de contrato/parcela para o banco ({arquivo.Name})" };
                                LOGRotinas.GravarLogServico(_log);
                                List<int> listaErroParcelas = new List<int>();
                                var CodRetorno = importacao
                                    .CarregarArquivoParaBanco(processar.Name, processar.FullName, 2, ref listaErroParcelas);//COLA NO BANCO OS DADOS DO ARQUIVO (TIPO 2 = CONTRATO/PARCELA)

                                _log = new LogServico
                                {
                                    TipoServico = _log.TipoServico,
                                    TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela,
                                    Mensagem = $"Carregamento finalizado. Processando arquivo de contrato/parcela ({arquivo.Name})",
                                    CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                };
                                LOGRotinas.GravarLogServico(_log);

                                if (listaErroParcelas.Any())
                                {
                                    string linhas = string.Join(",", listaErroParcelas);

                                    var logLihaErro = new LogServico
                                    {
                                        TipoServico = _log.TipoServico,
                                        TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela,
                                        Mensagem = $"Linhas nao processadas {linhas}.",
                                        CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                    };
                                    LOGRotinas.GravarLogServico(logLihaErro);
                                }

                                retorno = importacao.ProcessaParcela(DbRotinas.ConverterParaInt(CodRetorno));//PROCESSANDO

                                if (retorno.OK)
                                {
                                    #region Historico Parcela
                                    _log = new LogServico
                                    {
                                        TipoServico = _log.TipoServico,
                                        TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela,
                                        Mensagem = $"Trabalhando nos Historicos.",
                                        CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                    };
                                    LOGRotinas.GravarLogServico(_log);
                                    retorno = importacao.ProcessarHistoricoParcelaPorCodImportacao(DbRotinas.ConverterParaInt(CodRetorno));
                                    if (!retorno.OK)
                                    {
                                        _log = new LogServico
                                        {
                                            TipoServico = _log.TipoServico,
                                            TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela,
                                            Mensagem = $"{retorno.Mensagem}",
                                            CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                        };
                                        LOGRotinas.GravarLogServico(_log);
                                    }
                                    #endregion
                                    #region Computar as Informações
                                    retorno = importacao.ProcessarInformacaoParcelaPorCodImportacao(DbRotinas.ConverterParaInt(CodRetorno));
                                    if (!retorno.OK)
                                    {
                                        _log = new LogServico
                                        {
                                            TipoServico = _log.TipoServico,
                                            TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela,
                                            Mensagem = $"{retorno.Mensagem}",
                                            CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                        };
                                        LOGRotinas.GravarLogServico(_log);
                                    }
                                    #endregion

                                    _log = new LogServico
                                    {
                                        TipoServico = _log.TipoServico,
                                        TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela,
                                        Mensagem = $"Congelando os dados...",
                                        CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                    };
                                    LOGRotinas.GravarLogServico(_log);

                                    _qry.executarCongelamentoDiarioDados();
                                    _log = new LogServico
                                    {
                                        TipoServico = _log.TipoServico,
                                        TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela,
                                        Mensagem = $"Processo finalizado. Inicio do carregamento para a pasta processado ({arquivo.Name})",
                                        CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                    };
                                    LOGRotinas.GravarLogServico(_log);
                                }
                                else
                                {
                                    _log = new LogServico
                                    {
                                        TipoServico = _log.TipoServico,
                                        TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela,
                                        Mensagem = $"Erro ao processar carga de parcelas erro:({retorno.Mensagem})",
                                        CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                    };
                                    LOGRotinas.GravarLogServico(_log);
                                }

                                importacao.MoverFile(processar.Name, processar.FullName, CaminhoProcessado, CodRetorno);//MOVE ARQUIVO PARA PASTA "PROCESSADOS"
                                _log = new LogServico
                                {
                                    TipoServico = _log.TipoServico,
                                    TipoImportacao = (short)Enumeradores.TipoImportacao.Parcela,
                                    Mensagem = $"Processo finalizado. Movendo para a pasta processado o arquivo de contrato/parcela  ({ arquivo.Name})",
                                };
                                LOGRotinas.GravarLogServico(_log);
                                _tbMonitorArquivo.Remove(arquivo.Name);
                            }
                            catch (Exception ex)
                            {

                                string CaminhoServer = ConfigurationSettings.AppSettings["CaminhoArquivoError"];
                                if (!Directory.Exists(CaminhoServer))
                                    Directory.CreateDirectory(CaminhoServer);
                                string DestinoServidor = Path.Combine(CaminhoServer, $"Parcela{DateTime.Now:yyyyMMdd}_{DateTime.Now:HHmm}");
                                if (!Directory.Exists(DestinoServidor))
                                    Directory.CreateDirectory(DestinoServidor);

                                DirectoryInfo DirectoryI = new DirectoryInfo(Swap.FullName);
                                FileInfo[] Aqs = DirectoryI.GetFiles(arquivo.Name);
                                _tbMonitorArquivo.Remove(arquivo.Name);
                                foreach (var item in Aqs)
                                {
                                    importacao.MoverFile(item.Name, item.FullName, DestinoServidor, "");
                                }

                                throw;
                            }
                        }

                        _log = new LogServico { Mensagem = $"Processo Finalizado.", TipoServico = _log.TipoServico, TipoImportacao = _log.TipoImportacao };
                        LOGRotinas.GravarLogServico(_log);

                    }
                }
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Importacao Parcela - ", ex);
            }
        }
        private OperacaoDbRetorno ImportacaoCredito()
        {
            LogServico _log = new LogServico();
            string CaminhoPendente = ConfigurationSettings.AppSettings["CaminhoArquivoCreditoPendente"];
            string CaminhoProcessado = ConfigurationSettings.AppSettings["CaminhoArquivoCreditoProcessado"];
            string CaminhoSwapCredito = ConfigurationSettings.AppSettings["CaminhoArquivoCreditoSwap"];

            try
            {
                DirectoryInfo pendente = new DirectoryInfo(CaminhoPendente);
                FileInfo[] Files = pendente.GetFiles();
                OperacaoDbRetorno retorno = new OperacaoDbRetorno();

                foreach (var arquivo in Files)
                {
                    if (!arquivo.IsReadOnly)
                    {
                        if (MonitorarArquivoEmUso(arquivo, Enumeradores.TipoImportacao.Credito)) continue;

                        _log = new LogServico
                        {
                            TipoServico = (short)Enumeradores.TipoServico.Importacao,
                            Mensagem = $"Arquivo ({arquivo.Name}) encontrado que não esteja no modo somente leitura!",
                            TipoImportacao = (short)Enumeradores.TipoImportacao.Credito
                        };
                        LOGRotinas.GravarLogServico(_log);

                        importacao.MoverFile(arquivo.Name, arquivo.FullName, CaminhoSwapCredito, "");
                        DirectoryInfo Swap = new DirectoryInfo(CaminhoSwapCredito);
                        _log = new LogServico
                        {
                            Mensagem = $"Arquivo foi movido para o diretório ({Swap.Name})"
                        };
                        LOGRotinas.GravarLogServico(_log);
                        FileInfo[] Arquivos = Swap.GetFiles();
                        foreach (var processar in Arquivos)
                        {
                            try
                            {
                                _log = new LogServico { TipoServico = _log.TipoServico, TipoImportacao = (short)Enumeradores.TipoImportacao.Credito, Mensagem = $"Inicio do carregamento do arquivo para o banco de dados!" };
                                LOGRotinas.GravarLogServico(_log);
                                var CodRetorno = importacao.CarregarArquivoParaBanco(processar.Name, processar.FullName, 3);//COLA NO BANCO OS DADOS DO ARQUIVO (TIPO 3 = CREDITO)

                                _log = new LogServico
                                {
                                    TipoServico = _log.TipoImportacao,
                                    TipoImportacao = (short)Enumeradores.TipoImportacao.Credito,
                                    Mensagem = $"Carregamento finalizado, processando os Dados ... ",
                                    CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                };
                                LOGRotinas.GravarLogServico(_log);

                                retorno = importacao.ProcessaCredito(DbRotinas.ConverterParaInt(CodRetorno));//PROCESSANDO

                                if (!retorno.OK)
                                {
                                    _log = new LogServico
                                    {
                                        TipoServico = _log.TipoServico,
                                        TipoImportacao = (short)Enumeradores.TipoImportacao.Credito,
                                        Mensagem = $"Erro ao processar carga de credito erro:({retorno.Mensagem})",
                                        CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                    };
                                    LOGRotinas.GravarLogServico(_log);
                                }

                                importacao.MoverFile(processar.Name, processar.FullName, CaminhoProcessado, CodRetorno);//MOVE ARQUIVO PARA PASTA "PROCESSADOS"
                                _log = new LogServico
                                {
                                    TipoServico = _log.TipoServico,
                                    TipoImportacao = (short)Enumeradores.TipoImportacao.Credito,
                                    Mensagem = $"Processo finalizado. Movendo o arquivo ({ arquivo.Name}) para a pasta processado!",
                                };
                                LOGRotinas.GravarLogServico(_log);
                                _tbMonitorArquivo.Remove(arquivo.Name);
                            }
                            catch (Exception ex)
                            {
                                string CaminhoServer = ConfigurationSettings.AppSettings["CaminhoArquivoError"];
                                if (!Directory.Exists(CaminhoServer))
                                    Directory.CreateDirectory(CaminhoServer);
                                string DestinoServidor = Path.Combine(CaminhoServer, $"Credito{DateTime.Now:yyyyMMdd}_{DateTime.Now:HHmm}");
                                if (!Directory.Exists(DestinoServidor))
                                    Directory.CreateDirectory(DestinoServidor);

                                DirectoryInfo DirectoryI = new DirectoryInfo(Swap.FullName);
                                FileInfo[] Aqs = DirectoryI.GetFiles(arquivo.Name);
                                _tbMonitorArquivo.Remove(arquivo.Name);
                                foreach (var item in Aqs)
                                {
                                    importacao.MoverFile(item.Name, item.FullName, DestinoServidor, "");
                                }

                                throw new Exception(ex.Message);
                            }
                        }

                    }
                    _log = new LogServico { Mensagem = $"Processo Finalizado.", TipoServico = _log.TipoServico, TipoImportacao = _log.TipoImportacao };
                    LOGRotinas.GravarLogServico(_log);

                }
                return retorno;
            }
            catch (Exception ex)
            {

                throw new Exception("Importacao Credito - ", ex);
            }
        }
        private OperacaoDbRetorno ImportacaoBoleto()
        {
            LogServico _log = new LogServico();
            string CaminhoPendente = ConfigurationSettings.AppSettings["CaminhoArquivoBoletoPendente"];
            string CaminhoProcessado = ConfigurationSettings.AppSettings["CaminhoArquivoBoletoProcessado"];
            string CaminhoSwapBoleto = ConfigurationSettings.AppSettings["CaminhoArquivoBoletoSwap"];

            try
            {
                DirectoryInfo pendente = new DirectoryInfo(CaminhoPendente);
                FileInfo[] Files = pendente.GetFiles();
                OperacaoDbRetorno retorno = new OperacaoDbRetorno();

                foreach (var arquivo in Files)
                {
                    if (!arquivo.IsReadOnly)
                    {
                        if (MonitorarArquivoEmUso(arquivo, Enumeradores.TipoImportacao.Boleto)) continue;

                        _log = new LogServico
                        {
                            TipoServico = (short)Enumeradores.TipoServico.Importacao,
                            Mensagem = $"Arquivo ({arquivo.Name}) encontrado que não esteja no modo somente leitura!",
                            TipoImportacao = (short)Enumeradores.TipoImportacao.Boleto // (TIPO 4 = BOLETO)
                        };
                        LOGRotinas.GravarLogServico(_log);

                        importacao.MoverFile(arquivo.Name, arquivo.FullName, CaminhoSwapBoleto, "");
                        DirectoryInfo Swap = new DirectoryInfo(CaminhoSwapBoleto);
                        _log = new LogServico
                        {
                            Mensagem = $"Importacao Boleto - Arquivo foi movido para o diretório ({Swap.Name})"
                        };
                        LOGRotinas.GravarLogServico(_log);
                        FileInfo[] Arquivos = Swap.GetFiles();
                        foreach (var processar in Arquivos)
                        {
                            try
                            {
                                _log = new LogServico { TipoServico = _log.TipoServico, TipoImportacao = (short)Enumeradores.TipoImportacao.Boleto, Mensagem = $"Inicio do carregamento do arquivo para o banco de dados!" };
                                LOGRotinas.GravarLogServico(_log);
                                var CodRetorno = importacao.CarregarArquivoParaBanco(processar.Name, processar.FullName, 4);//COLA NO BANCO OS DADOS DO ARQUIVO (TIPO 4 = BOLETO)

                                _log = new LogServico
                                {
                                    TipoServico = _log.TipoServico,
                                    TipoImportacao = (short)Enumeradores.TipoImportacao.Boleto,
                                    Mensagem = $"Carregamento finalizado. Processando arquivo de boleto ({arquivo.Name})",
                                    CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                };
                                LOGRotinas.GravarLogServico(_log);
                                retorno = importacao.ProcessaBoleto(DbRotinas.ConverterParaInt(CodRetorno));//PROCESSANDO

                                if (!retorno.OK)
                                {
                                    _log = new LogServico
                                    {
                                        TipoServico = _log.TipoServico,
                                        TipoImportacao = (short)Enumeradores.TipoImportacao.Boleto,
                                        Mensagem = $"Erro ao processar carga de boleto erro:({retorno.Mensagem})",
                                        CodImportacao = DbRotinas.ConverterParaInt(CodRetorno)
                                    };
                                    LOGRotinas.GravarLogServico(_log);
                                }

                                importacao.MoverFile(processar.Name, processar.FullName, CaminhoProcessado, CodRetorno);//MOVE ARQUIVO PARA PASTA "PROCESSADOS"
                                _log = new LogServico
                                {
                                    TipoServico = _log.TipoServico,
                                    TipoImportacao = (short)Enumeradores.TipoImportacao.Boleto,
                                    Mensagem = $"Processo finalizado. Movendo para a pasta processado o arquivo de boleto  ({ arquivo.Name})"
                                };
                                LOGRotinas.GravarLogServico(_log);
                                _tbMonitorArquivo.Remove(arquivo.Name);

                            }
                            catch (Exception ex)
                            {
                                string CaminhoServer = ConfigurationSettings.AppSettings["CaminhoArquivoError"];
                                if (!Directory.Exists(CaminhoServer))
                                    Directory.CreateDirectory(CaminhoServer);
                                string DestinoServidor = Path.Combine(CaminhoServer, $"Boleto{DateTime.Now:yyyyMMdd}_{DateTime.Now:HHmm}");
                                if (!Directory.Exists(DestinoServidor))
                                    Directory.CreateDirectory(DestinoServidor);

                                DirectoryInfo DirectoryI = new DirectoryInfo(Swap.FullName);
                                FileInfo[] Aqs = DirectoryI.GetFiles(arquivo.Name);
                                _tbMonitorArquivo.Remove(arquivo.Name);
                                foreach (var item in Aqs)
                                {
                                    importacao.MoverFile(item.Name, item.FullName, DestinoServidor, "");
                                }
                                throw;
                            }
                        }
                    }

                }
                return retorno;
            }
            catch (Exception ex)
            {
                throw new Exception("Importacao Boleto - ", ex);
            }

        }

        #region REMESSA
        /// <summary>
        /// Método que processa os boletos gerando o arquivo remessa de cada companhia.
        /// </summary>
        private void TransmitirArquivoRemessa()
        {
            Rotinas.BoletoGerar.BoletoUtils BoletoRotina = new Rotinas.BoletoGerar.BoletoUtils();
            Rotinas.Service.RemessaService RemessaService = new Rotinas.Service.RemessaService();
            Repository<TabelaBancoPortadorGrupo> _repParamGrupo = new Repository<TabelaBancoPortadorGrupo>();
            string CaminhoSwap = ConfigurationSettings.AppSettings["CaminhoArquivoRemessaSwap"];
            string CaminhoBkp = ConfigurationSettings.AppSettings["CaminhoArquivoRemessaBKP"];
            string CaminhoServer = ConfigurationSettings.AppSettings["CaminhoArquivoRemessaServer"];
            var dateNow = DateTime.Now;
            DateTime periodoDe = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day);
            var _log = new LogServico
            {
                TipoServico = (short)Enumeradores.TipoServico.Boletos_remessa,
                Mensagem = $"Remessa - Inicio do processamento dos boletos.",
            };
            LOGRotinas.GravarLogServico(_log);
            try
            {
                Repository<Usuario> _repUsuario = new Repository<Usuario>();
                var usr = _repUsuario.All().Where(x => x.Login == "Servico").FirstOrDefault();

                var parametroGrupos = _repParamGrupo.All().Where(x => !x.DataExcluido.HasValue).ToList();
                List<int> arquivosIdsLog = new List<int>();
                Rotinas.BoletoGerar.BoletoUtils.RemessaRetorno op = new Rotinas.BoletoGerar.BoletoUtils.RemessaRetorno();
                foreach (var paramGrupo in parametroGrupos)
                {
                    if (paramGrupo.NumeroLoteAtual == null) paramGrupo.NumeroLoteAtual = paramGrupo.NumeroLote;

                    var response = RemessaService.CriarArquivoRemessa(periodoDe, dateNow, null, paramGrupo);
                    if (response.OK)
                    {
                        var nomeArquivo = paramGrupo.NomeArquivo;
                        nomeArquivo = nomeArquivo.Replace("@NUMERO_LOTE_ATUAL", DbRotinas.FormatCode(DbRotinas.ConverterParaString(paramGrupo.NumeroLoteAtual), "0", 8, true));
                        nomeArquivo = $"{nomeArquivo}.rem";
                        response.CodUsuario = usr.Cod;
                        response.NomeArquivo = nomeArquivo;
                        RemessaService.RegistrarLogRemessa(ref response);
                        RemessaService.RegistrarBoletosProcessados(ref response);
                        arquivosIdsLog.Add(response.CodLogArquivo);

                        _log = new LogServico
                        {
                            TipoServico = (short)Enumeradores.TipoServico.Boletos_remessa,
                            Mensagem = $"Remessa - Criando BKP."
                        };
                        LOGRotinas.GravarLogServico(_log);

                        if (!Directory.Exists(CaminhoBkp)) Directory.CreateDirectory(CaminhoBkp);
                        string DestinoBKP = Path.Combine(CaminhoBkp, nomeArquivo);
                        using (var sw = new StreamWriter(DestinoBKP, false, Encoding.UTF8))
                        {
                            sw.Write(Encoding.UTF8.GetString(response.Arquivo));
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _log = new LogServico { TipoServico = _log.TipoServico, Mensagem = $"Erro ao processar" };
                LOGRotinas.GravarLogServico(_log);
                LOGRotinas.GravarLogTXT($"Erro ao processar. Mensagem: {ex.Message}");

            }
        }
        #endregion

        /// <summary>
        /// Método para reprovar solicitações que o prazo para transmitir o boleto não podera ser feita. 
        /// ex: boleto para data de hoje pendente de aprovação não podera ser transmitido ao banco á tempo
        /// </summary>
        /// <param name="operadores"></param>
        /// <param name="tipoResolucao"></param>
        /// <returns>Classe de retorno {OK=True,Mensagem =  "",dados = null }.</returns>
        private OperacaoDbRetorno CancelarSolicitacaoForaDoPrazo()
        {
            Rotinas.BoletoGerar.BoletoUtils BoletoRotina = new Rotinas.BoletoGerar.BoletoUtils();
            DateTime DataDe = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0); ;
            OperacaoDbRetorno op = new OperacaoDbRetorno();
            var _log = new LogServico
            {
                TipoServico = (short)Enumeradores.TipoServico.Boletos_remessa,
                Mensagem = $"Boleto - verificando prazos para solicitações.",
            };
            LOGRotinas.GravarLogServico(_log);
            try
            {
                Repository<TabelaParametroSintese> _parametroSintese = new Repository<TabelaParametroSintese>();
                Repository<ContratoParcelaBoletoHistorico> _dbBoletoHistorico = new Repository<ContratoParcelaBoletoHistorico>();
                var sintesePrazoExpirou = (from p in _parametroSintese.All()
                                           where p.Etapa == (short)Enumeradores.EtapaBoleto.PrazoExpira
                                           select p.CodSintesePara)
                                       .FirstOrDefault();
                using (junix_cobrancaContext cx = new junix_cobrancaContext())
                {
                    var boletos = cx.ContratoParcelaBoletoes
                                    .Where(x =>
                                            !x.DataExcluido.HasValue
                                            && x.Status != (short)Enumeradores.StatusBoleto.Aprovado
                                            && x.Status != (short)Enumeradores.StatusBoleto.Reprovado
                                            //&& x.Status != (short)Enumeradores.StatusBoleto.Obsoleto
                                            && x.DataVencimento <= DataDe.Date
                                        );

                    foreach (var item in boletos)
                    {
                        ContratoParcelaBoletoHistorico history = Activator.CreateInstance<ContratoParcelaBoletoHistorico>();
                        history.DataCadastro = DateTime.Now;
                        history.CodBoleto = item.Cod;
                        history.CodSintese = sintesePrazoExpirou;
                        history.Observacao = $"O boleto solicitado com o vencimento em {item.DataVencimento:dd/MM/yyyy} não será registrado até a data!";
                        history = _dbBoletoHistorico.CreateOrUpdate(history);

                        item.CodHistorico = history.Cod;
                        item.Status = (short)Enumeradores.StatusBoleto.Reprovado;
                    }
                    cx.SaveChanges();

                }

                op.OK = true;
                _log = new LogServico
                {
                    TipoServico = _log.TipoServico,
                    Mensagem = $"Boletos - Todos os boletos foram verificados."
                };
                LOGRotinas.GravarLogServico(_log);

            }
            catch (Exception ex)
            {
                _log = new LogServico { TipoServico = _log.TipoServico, Mensagem = $"Erro ao processar" };
                LOGRotinas.GravarLogServico(_log);
                op = new OperacaoDbRetorno(ex);
                LOGRotinas.GravarLogTXT($"Erro ao processar. Mensagem: {ex.Message}");

            }
            return op;
        }


        #region Classificacao
        private OperacaoDbRetorno ProcesssarExtracaoDeClassificacao()
        {
            string CaminhoArquivo = ConfigurationSettings.AppSettings["ClassificacaoFaturaCaminho"];
            string nomeArquivo = ConfigurationSettings.AppSettings["ClassificacaoFaturaNomeArquivo"];
            OperacaoDbRetorno op = new OperacaoDbRetorno();
            var _log = new LogServico
            {
                TipoServico = (short)Enumeradores.TipoServico.Classificacao,
                Mensagem = $"Exportação de classificações - Processo Iniciado...",
            };
            LOGRotinas.GravarLogServico(_log);
            try
            {
                StringBuilder rows = GravarClassificacaoFaturas();
                LOGRotinas.GravarArquivo(rows.ToString(), CaminhoArquivo, nomeArquivo);
                LOGRotinas.RemoveLinhasEmBranco(CaminhoArquivo, nomeArquivo);
                gerarExtracaoBKP();

                op.OK = true;
                _log = new LogServico
                {
                    TipoServico = _log.TipoServico,
                    Mensagem = $"Exportação de classificações - Processo Concluído.",
                };
                LOGRotinas.GravarLogServico(_log);

            }
            catch (Exception ex)
            {
                _log = new LogServico { TipoServico = _log.TipoServico, Mensagem = $"Erro ao processar" };
                LOGRotinas.GravarLogServico(_log);
                op = new OperacaoDbRetorno(ex);
                LOGRotinas.GravarLogTXT($"Erro ao processar. Mensagem: {ex.Message}");

            }
            return op;
        }

        public void gerarExtracaoBKP()
        {
            try
            {
                string PastaFinal = "";
                string CaminhoArquivo = ConfigurationSettings.AppSettings["ClassificacaoFaturaCaminho"];
                string nomeArquivo = ConfigurationSettings.AppSettings["ClassificacaoFaturaNomeArquivo"];
                string CaminhoArquivoBKP = ConfigurationSettings.AppSettings["ClassificacaoFaturaCaminhoBKP"];
                string data = $"{DateTime.Now:yyyyMMdd}";

                var DestinoInicial = Path.Combine(CaminhoArquivo, nomeArquivo);

                if (!string.IsNullOrEmpty(data))
                    PastaFinal = Path.Combine(CaminhoArquivoBKP, data);
                else
                    PastaFinal = Path.Combine(CaminhoArquivoBKP);

                if (!Directory.Exists(PastaFinal))
                    Directory.CreateDirectory(PastaFinal);

                string DestinoFinal = Path.Combine(PastaFinal, nomeArquivo);
                File.Copy(DestinoInicial, DestinoFinal, true);
            }
            catch (Exception ex)
            {

                throw new Exception("Erro no processo de gerar bkp de extração: ", ex);
            }


        }

        private StringBuilder GravarClassificacaoFaturas()
        {
            StringBuilder row = new StringBuilder();
            try
            {
                DateTime DataDe = DbRotinas.ConverterParaDatetime(_proximoProcessamentoExtracaoClassificacaoDiarioData);
                DateTime DataAte = DbRotinas.ConverterParaDatetime(_proximoProcessamentoExtracaoClassificacaoDiarioData);
                TimeSpan time = new TimeSpan(19, 30, 0);
                var dtDe = DataDe.Date.AddDays(-1) + time;
                //var dtDe = DataDe.AddDays(-1);//POR DEFINIÇÂO O SERVIÇO RODA AS 20:00 E AS CLASSIFICACOES FORAM EXECUTAS NO DIA ANTERIOR
                var parcelas = _qry.ExtrairClassificacaoParcela(null, null, dtDe, DataAte);
                foreach (var item in parcelas)
                {
                    row.AppendLine($"{DbRotinas.Descriptografar(item.CodCliente)};{item.FluxoPagamento};{DbRotinas.Descriptografar(item.NumeroContrato)};{item.NumeroParcela};{item.CodRegionalCliente};{item.CodSinteseCliente};{item.NossoNumero};");
                }

                var _log = new LogServico
                {
                    TipoServico = (short)Enumeradores.TipoServico.Classificacao,
                    Mensagem = $"Exportação de classificações - {parcelas.Count} classificações encontradas ...",
                };
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao Processar as informações de classificações", ex);
            }

            return row;
        }
        #endregion

        private OperacaoDbRetorno ProcesssarValoresDiarios()
        {
            string CaminhoArquivo = ConfigurationSettings.AppSettings["ClassificacaoFaturaCaminho"];
            string nomeArquivo = ConfigurationSettings.AppSettings["ClassificacaoFaturaNomeArquivo"];
            OperacaoDbRetorno op = new OperacaoDbRetorno();
            var _log = new LogServico();
            try
            {
                _log = new LogServico
                {
                    TipoServico = _log.TipoServico,
                    Mensagem = $"Valores Diários - Atualizaçãos das informações foram iniciados.",
                };
                LOGRotinas.GravarLogServico(_log);
                op = _qry.ProcessarValoresDiarios();
                _log = new LogServico
                {
                    TipoServico = _log.TipoServico,
                    Mensagem = $"Valores Diários - Atualizaçãos das informações foram concluídas.",
                };
                LOGRotinas.GravarLogServico(_log);
            }
            catch (Exception ex)
            {
                _log = new LogServico { TipoServico = _log.TipoServico, Mensagem = $"Erro ao processar" };
                LOGRotinas.GravarLogServico(_log);
                op = new OperacaoDbRetorno(ex);
                LOGRotinas.GravarLogTXT($"Erro ao processar. Mensagem: {ex.Message}");

            }
            return op;
        }

        /// <summary>
        /// Método enviar email com erros no processar da importação
        /// </summary>
        /// <param name="LogServico">classe com Mensagem</param>
        /// <returns>static.</returns>
        private void EnviarLogPorEmail(string _assunto, string _conteudo)
        {
            try
            {


                var arr = _emailErro.Split(';');

                foreach (var item in arr)
                {
                    var _log = new LogEmail
                    {

                        Assunto = _assunto,
                        Conteudo = _conteudo,
                        EmailDestinatario = item,
                        NomeRemetente = "Servico OTIS",
                        Status = (short)Enumeradores.LogEmailStatus.AguardandoEnvio
                    };

                    if (!string.IsNullOrEmpty(item))
                        _dbLogEmail.CreateOrUpdate(_log);
                }


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        protected override void OnStop()
        {
            LOGRotinas.GravarLogTXT("Servico encerrado");
        }


    }
}

