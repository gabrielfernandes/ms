﻿
using Cobranca.Domain.ServicoContext.Handlers;
using Cobranca.Domain.ServicoContext.Repositories;
using Cobranca.Infra;
using Cobranca.Infra.ServicoContext.Repositories;
using SimpleInjector;

namespace Cobranca.Servico
{
    public static class DIContainer
    {
        public static Container container;

        public static void RegisterDependencies(Container containerToInject)
        {
            containerToInject.Register<DbContext>(Lifestyle.Scoped);

            //REPOSITORIEs
            containerToInject.Register<IClienteEnderecoRepository, ClienteEnderecoRepository>();
            containerToInject.Register<IContratoRepository, ContratoRepository>();

            //HANDLERs
            containerToInject.Register<ClienteEnderecoHandlers, ClienteEnderecoHandlers>();
            containerToInject.Register<LogHandlers, LogHandlers>();
            containerToInject.Register<ImportResultHandlers, ImportResultHandlers>();
        }
    }
}
